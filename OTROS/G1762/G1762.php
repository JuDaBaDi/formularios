
<?php date_default_timezone_set('America/Bogota'); ?>
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1762/G1762_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

	$PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

	if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1762_ConsInte__b as id, G1762_C31738 as camp2 , G1762_C31739 as camp1 FROM ".$BaseDatos.".G1762  WHERE G1762_Usuario = ".$idUsuario." ORDER BY G1762_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1762_ConsInte__b as id, G1762_C31738 as camp2 , G1762_C31739 as camp1 FROM ".$BaseDatos.".G1762  ORDER BY G1762_ConsInte__b DESC LIMIT 0, 50";
        }
    }else{
        $Zsql = "SELECT G1762_ConsInte__b as id, G1762_C31738 as camp2 , G1762_C31739 as camp1 FROM ".$BaseDatos.".G1762  ORDER BY G1762_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<?php
if(isset($_GET['user'])){


	$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
    $datoCampan = $res_Lsql_Campan->fetch_array();
    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

	$XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
	$nombre = $mysqli->query($XLsql);
	$nombreUsuario = NULL;
	//echo $XLsql;
	while ($key = $nombre->fetch_object()) {
	 	echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
	 	$nombreUsuario = $key->nombre;
	 	break;
	} 


	if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


					
		$data = array(	"strToken_t" => $_GET['token'], 
						"strIdGestion_t" => $_GET['id_gestion_cbx'],
						"strDatoPrincipal_t" => $nombreUsuario,
						"strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
		$data_string = json_encode($data);    

		$ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
		//especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                      
		); 
		//recogemos la respuesta
		$respuesta = curl_exec ($ch);
		//o el error, por si falla
		$error = curl_error($ch);
		//y finalmente cerramos curl
		//echo "Respuesta =>  ". $respuesta;
		//echo "<br/>Error => ".$error;
		//include "Log.class.php";
		//$log = new Log("log", "./Log/");
		//$log->insert($error, $respuesta, false, true, false);
		//echo "nada";
		curl_close ($ch);
	}
}else{
	echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";	
}
?>
<input type="hidden" id="CampoIdGestionCbx" value="<?php if(isset($_GET['id_gestion_cbx'])){ echo $_GET["id_gestion_cbx"];}else{echo "";}?>">
<?php if(isset($_GET['user'])){ ?>
<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="box">
			<div class="box-body">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
                            <th colspan="4">
                                Historico de gestiones
                            </th>
                        </tr>
						<tr>
							<th>Gesti&oacute;n</th>
							<th>Comentarios</th>
							<th>Fecha - hora</th>
							<th>Agente</th>
						</tr>
					</thead>
					<tbody>
						<?php

							$Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


							$res = $mysqli->query($Lsql);
							while($key = $res->fetch_object()){
								echo "<tr>";
								echo "<td>".($key->MONOEF_Texto_____b)."</td>";
								echo "<td>".$key->CONDIA_Observacio_b."</td>";
								echo "<td>".$key->CONDIA_Fecha_____b."</td>";
								echo "<td>".$key->USUARI_Nombre____b."</td>";
								echo "</tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?php } ?>

<div id="4395" >


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
			        <!-- lIBRETO O LABEL -->
			        <p style="text-align:justify;">Acueducto Metropolitano de B/manga, buenos días/tardes/noches le saluda [nombre_agente], en que le puedo colaborar?</p>
			        <!-- FIN LIBRETO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1762_C31966" id="LblG1762_C31966">ID LLAMADA</label>
			            <input type="text" class="form-control input-sm Numerico" value="<?php if(!isset($_GET["registroId"])){ $Lsql = "SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 31966"; $res = $mysqli->query($Lsql); $dato = $res->fetch_array(); echo ($dato["CONTADORES_Valor_b"] + 1); $XLsql = "UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = CONTADORES_Valor_b + 1 WHERE CONTADORES_ConsInte__PREGUN_b = 31966"; $mysqli->query($XLsql);}?>" readonly name="G1762_C31966" id="G1762_C31966" placeholder="ID LLAMADA">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>
            
        
            
            <div class="col-md-6 col-xs-6">
                <div class="form-group">
                    <label for="G1762_C34557" id="LblG1762_C34557">Teléfono(ANI)</label>   
                    <input type="text" class="form-control input-sm" id="G1762_C34514" value="<?php if(isset($_GET["ani"])){echo $_GET["ani"];} ?>" readonly name="G1762_C34514"  placeholder="telefono">
                </div>
            </div>                    

  
        </div>


</div>

<div id="4391" style='display:none;'>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1762_C31728" id="LblG1762_C31728">Agente</label>
			            <input type="text" class="form-control input-sm" id="G1762_C31728" value="<?php echo getNombreUser($token);?>" readonly name="G1762_C31728"  placeholder="Agente">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1762_C31729" id="LblG1762_C31729">Fecha</label>
			            <input type="text" class="form-control input-sm" id="G1762_C31729" value="<?php echo date('Y-m-d');?>" readonly name="G1762_C31729"  placeholder="Fecha">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1762_C31730" id="LblG1762_C31730">Hora</label>
			            <input type="text" class="form-control input-sm" id="G1762_C31730" value="<?php echo date('H:i:s');?>" readonly name="G1762_C31730"  placeholder="Hora">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1762_C31731" id="LblG1762_C31731">Campaña</label>
			            <input type="text" class="form-control input-sm" id="G1762_C31731" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÑA";}?>" readonly name="G1762_C31731"  placeholder="Campaña">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


</div>


<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4440">
                DATOS DEL SOLICITANTE
            </a>
        </h4>
    </div>
    <div id="s_4440" class="panel-collapse collapse in">
        <div class="box-body">
        
        
            <div class="row">


                <div class="col-md-6 col-xs-6">


                        <!-- CAMPO TIPO TEXTO -->
                        <div class="form-group">
                            <label for="G1762_C31736" id="LblG1762_C31736">NOMBRE DEL SOLICITANTE - CONTACTO</label>
                            <input type="text" class="form-control input-sm" id="G1762_C31736" value=""  name="G1762_C31736"  placeholder="NOMBRE DEL SOLICITANTE - CONTACTO">
                        </div>
                        <!-- FIN DEL CAMPO TIPO TEXTO -->

                </div>


                <div class="col-md-6 col-xs-6">


                        <!-- CAMPO TIPO TEXTO -->
                        <div class="form-group">
                            <label for="G1762_C31737" id="LblG1762_C31737">CEDULA</label>
                            <input type="text" class="form-control input-sm" id="G1762_C31737" value=""  name="G1762_C31737"  placeholder="CEDULA">
                        </div>
                        <!-- FIN DEL CAMPO TIPO TEXTO -->

                </div>


            </div>
            
            <div class="row">
            
                <div class="col-md-6 col-xs-6">


                        <!-- CAMPO TIPO TEXTO -->
                        <div class="form-group">
                            <label for="G1762_C31745" id="LblG1762_C31745">CELULAR</label>
                            <input type="text" class="form-control input-sm" id="G1762_C31745" value=""  name="G1762_C31745"  placeholder="CELULAR">
                        </div>
                        <!-- FIN DEL CAMPO TIPO TEXTO -->

                </div>            
            


                <div class="col-md-6 col-xs-6">


                        <!-- CAMPO TIPO TEXTO -->
                        <div class="form-group">
                            <label for="G1762_C31744" id="LblG1762_C31744">EMAIL</label>
                            <input type="text" class="form-control input-sm" id="G1762_C31744" value=""  name="G1762_C31744"  placeholder="EMAIL">
                        </div>
                        <!-- FIN DEL CAMPO TIPO TEXTO -->

                </div>


            </div>                                    
            
            

            <div class="row">


                <div class="col-md-6 col-xs-6">
                    <div class="form-group">
                        <label for="G1762_C34556" id="LblG1762_C34556">Propietario</label>
                        <select class="form-control input-sm" name="G1762_C34556" id="G1762_C34556">
                            <option value="0">SELECCIONE</option>
                            <?php
                            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 1880;";
                            $obj = $mysqli->query($Lsql);
                            while($obje = $obj->fetch_object()){
                                echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                            }          
                            ?>
                        </select>                
                    </div>
                
                </div>


                <div class="col-md-6 col-xs-6">

                    <div class="form-goup">
                        <label for="G1762_C34557" id="LblG1762_C34557">Medio de notificación</label>
                        <select class="form-control input-sm" name="G1762_C34557" id="G1762_C34557">
                            <option value="0">SELECCIONE</option>
                            <?php
                            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 1881;";
                            $obj = $mysqli->query($Lsql);
                            while($obje = $obj->fetch_object()){
                                echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                            }          
                            ?>
                        </select>                 
                    </div>

                </div>


            </div>


            <div class="row">


            </div>


        </div>
    </div>
</div>
<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4396">
                DATOS DEL SUSCRIPTOR
            </a>
        </h4>
    </div>
    <div id="s_4396" class="panel-collapse collapse in">
        <div class="box-body">


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1762_C31738" id="LblG1762_C31738">TELEFONO</label>
			            <input type="text" class="form-control input-sm" id="G1762_C31738" value=""  name="G1762_C31738"  placeholder="TELEFONO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1762_C31739" id="LblG1762_C31739">SUSCRIPTOR</label>
			            <input type="text" class="form-control input-sm" id="G1762_C31739" value=""  name="G1762_C31739"  placeholder="SUSCRIPTOR">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1762_C31740" id="LblG1762_C31740">DIRECCIÓN</label>
			            <input type="text" class="form-control input-sm" id="G1762_C31740" value=""  name="G1762_C31740"  placeholder="DIRECCIÓN">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1762_C31741" id="LblG1762_C31741">BARRIO</label>
			            <input type="text" class="form-control input-sm" id="G1762_C31741" value=""  name="G1762_C31741"  placeholder="BARRIO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1762_C31742" id="LblG1762_C31742">MUNICIPIO</label>
			            <input type="text" class="form-control input-sm" id="G1762_C31742" value=""  name="G1762_C31742"  placeholder="MUNICIPIO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1762_C31743" id="LblG1762_C31743">CICLO</label>
			            <input type="text" class="form-control input-sm" id="G1762_C31743" value=""  name="G1762_C31743"  placeholder="CICLO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1762_C31746" id="LblG1762_C31746">CODIGO MEDIDOR CONTROL</label>
			            <input type="text" class="form-control input-sm" id="G1762_C31746" value=""  name="G1762_C31746"  placeholder="CODIGO MEDIDOR CONTROL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1762_C31747" id="LblG1762_C31747">DIREECION ENVIO</label>
			            <input type="text" class="form-control input-sm" id="G1762_C31747" value=""  name="G1762_C31747"  placeholder="DIREECION ENVIO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1762_C31748" id="LblG1762_C31748">FECHA PAGO</label>
			            <input type="text" class="form-control input-sm Fecha" value=""  name="G1762_C31748" id="G1762_C31748" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>
  
        </div>


        </div>
    </div>
</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4399">
                TIPO DE LLAMADA
            </a>
        </h4>
    </div>
    <div id="s_4399" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1762_C31766" id="LblG1762_C31766">TIPO DE LLAMADA</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1762_C31766" id="G1762_C31766">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1703 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO MEMO -->
			        <div class="form-group">
			            <label for="G1762_C31767" id="LblG1762_C31767">DESCRIPCIÓN</label>
			            <textarea class="form-control input-sm" name="G1762_C31767" id="G1762_C31767"  value="" placeholder="DESCRIPCIÓN"></textarea>
			        </div>
			        <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1762_C31814" id="LblG1762_C31814">SUBCLASIFICIÓN</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1762_C31814" id="G1762_C31814">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1704 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1762_C31815" id="LblG1762_C31815"></label>
                        <div><a name="G1762_C31815" id="G1762_C31815" target="_blank"></a></div>
                        
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1762_C31816" id="LblG1762_C31816">TIPO QUEJA-DAÑOS</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1762_C31816" id="G1762_C31816">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1706 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1762_C31817" id="LblG1762_C31817"></label>
			            <div><a name="G1762_C31817" id="G1762_C31817" target="_blank"></a></div>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO MEMO -->
			        <div class="form-group">
			            <label for="G1762_C31968" id="LblG1762_C31968">GESTIÓN REALIZADA</label>
			            <textarea class="form-control input-sm" name="G1762_C31968" id="G1762_C31968"  value="" placeholder="GESTIÓN REALIZADA"></textarea>
			        </div>
			        <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div>

        </div>
    </div>
</div>
</div>


<div id="4408" >


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
			        <!-- lIBRETO O LABEL -->
			        <p style="text-align:justify;">Señor (a) recuerde que habló con [nombre_agente] que tenga buen día..</p>
			        <!-- FIN LIBRETO -->
  
            </div>

  
        </div>


</div>

<!-- SI ES MAESTRO - DETALLE CREO LAS TABS --> 

<hr/>
<div class="nav-tabs-custom">

    <ul class="nav nav-tabs">

        <li class="active">
            <a href="#tab_0" data-toggle="tab" id="tabs_click_0">HISTORICO BARRIOS</a>
        </li>

        <li class="">
            <a href="#tab_1" data-toggle="tab" id="tabs_click_1">CASOS</a>
        </li>

        <li class="">
            <a href="#tab_2" data-toggle="tab" id="tabs_click_2">ORDEN DE TRABAJO</a>
        </li>

        <li class="">
            <a href="#tab_3" data-toggle="tab" id="tabs_click_3">HISTORICO LLAMADAS</a>
        </li>

    </ul>


    <div class="tab-content">

        <div class="tab-pane active" id="tab_0"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless0" width="100%">
            </table>
            <div id="pagerDetalles0">
            </div> 
<!--            <button title="Crear HISTORICO BARRIOS" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_0"><i class="fa fa-plus"></i></button>-->
        </div>

        <div class="tab-pane " id="tab_1"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless1" width="100%">
            </table>
            <div id="pagerDetalles1">
            </div> 
            <button title="Crear CASOS" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_1"><i class="fa fa-plus"></i></button>
        </div>

        <div class="tab-pane " id="tab_2"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless2" width="100%">
            </table>
            <div id="pagerDetalles2">
            </div> 
            <button title="Crear ORDEN DE TRABAJO" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_2"><i class="fa fa-plus"></i></button>
        </div>

        <div class="tab-pane " id="tab_3"> 
            <table class="table table-hover table-bordered" id="tablaDatosDetalless3" width="100%">
            </table>
            <div id="pagerDetalles3">
            </div> 
        </div>

    </div>

</div>

<div class="row" style="background-color: #FAFAFA; ">
	<br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-9">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1762_C31723">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 1702;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
   	<div class="col-md-2 col-xs-3" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;" href='<?php echo $http ;?>/crm_php/Estacion_contact_center.php?token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?>&id_campana_crm=<?php echo $_GET['campana_crm'];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>&ani=busqueda_manual&busqueda_manual_forzada=true&dato_adicional_1=Llamada'>
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1762_C31723">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 1702;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G1762_C31724">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G1762_C31725" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G1762_C31726" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G1762_C31727" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<input type="hidden" name="orden" id="orden" value="">
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G1762/G1762_eventos.js"></script> 
<script type="text/javascript">
    //prueba iframe
  function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }


        // Listen to message from child window
        bindEvent(window, 'message', function (e) {
            $('#orden').val(e.data);
        });
    
    function llamarHijo(){
        setTimeout(function(){
           var formDatas = $('#FormularioDatos').serializeArray();
            var iframe = document.getElementById('frameContenedor'); 
            iframe.contentWindow.postMessage(formDatas, '*');                                 
        },2000);  
    }    
    $(function(){
        <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        	<?php if($_GET["yourfather"] != "NULL"){ ?>
        		$("#G1762_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        	<?php }else{ ?>
        		if(document.getElementById("G1762_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
        				$.ajax({
	                        url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
	                        type     : 'POST',
	                        data     : { q : <?php echo $_GET["idFather"]; ?> },
	                        type : 'post',
	                        success  : function(data){
	                            $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
	                    }});
        			}else{
        				$("#G1762_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
        			}
			<?php } ?>
        <?php } ?>
        
                        		<?php if (!isset($_GET["idFather"])) { ?>
		                        	 $("#add").click(function(){
							                $("#G1762_C31966").val("<?php if(!isset($_GET["registroId"])){ $Lsql = "SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 31966"; $res = $mysqli->query($Lsql); $dato = $res->fetch_array(); echo ($dato["CONTADORES_Valor_b"] + 1); $XLsql = "UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = CONTADORES_Valor_b + 1 WHERE CONTADORES_ConsInte__PREGUN_b = 31966"; $mysqli->query($XLsql);}?>");
							         });
					            <?php } ?>	
                        	
    	var meses = new Array(12);
    	meses[0] = "01";
    	meses[1] = "02";
    	meses[2] = "03";
    	meses[3] = "04";
    	meses[4] = "05";
    	meses[5] = "06";
    	meses[6] = "07";
    	meses[7] = "08";
    	meses[8] = "09";
    	meses[9] = "10";
    	meses[10] = "11";
    	meses[11] = "12";

    	var d = new Date();
        var h = d.getHours();
        var horas = (h < 10) ? '0' + h : h;
        var dia = d.getDate();
        var dias = (dia < 10) ? '0' + dia : dia;
        var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
        $("#FechaInicio").val(fechaInicial);
            

        //Esta es por si lo llaman en modo formulario de edicion LigthBox
        <?php if(isset($_GET['registroId'])){ ?>
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
            dataType : 'json',
            success  : function(data){
                //recorrer datos y enviarlos al formulario
                $.each(data, function(i, item) {
                    
            
            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            

            $.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
            

            $.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
            

            $.jgrid.gridUnload('#tablaDatosDetalless3'); //funcion Recargar 
            
 
                    $("#G1762_C31723").val(item.G1762_C31723).trigger("change"); 
 
                    $("#G1762_C31724").val(item.G1762_C31724).trigger("change"); 
 
                    $("#G1762_C31725").val(item.G1762_C31725);
 
                    $("#G1762_C31726").val(item.G1762_C31726);
 
                    $("#G1762_C31727").val(item.G1762_C31727);
 
                    $("#G1762_C31728").val(item.G1762_C31728);
 
                    $("#G1762_C31729").val(item.G1762_C31729);
 
                    $("#G1762_C31730").val(item.G1762_C31730);
 
                    $("#G1762_C31731").val(item.G1762_C31731);
   
                    if(item.G1762_C31735 == 1){
                        $("#G1762_C31735").attr('checked', true);
                    } 
 
                    $("#G1762_C31966").val(item.G1762_C31966);
 
                    $("#G1762_C31736").val(item.G1762_C31736);
 
                    $("#G1762_C31737").val(item.G1762_C31737);
 
                    $("#G1762_C31738").val(item.G1762_C31738);
 
                    $("#G1762_C31739").val(item.G1762_C31739);
 
                    $("#G1762_C31740").val(item.G1762_C31740);
 
                    $("#G1762_C31741").val(item.G1762_C31741);
 
                    $("#G1762_C31742").val(item.G1762_C31742);
 
                    $("#G1762_C31743").val(item.G1762_C31743);
 
                    $("#G1762_C31744").val(item.G1762_C31744);
 
                    $("#G1762_C31745").val(item.G1762_C31745);
 
                    $("#G1762_C31746").val(item.G1762_C31746);
 
                    $("#G1762_C31747").val(item.G1762_C31747);
 
                    $("#G1762_C31748").val(item.G1762_C31748);
 
                    $("#G1762_C31750").val(item.G1762_C31750).trigger("change"); 
 
                    $("#G1762_C31766").val(item.G1762_C31766).trigger("change"); 
 
                    $("#G1762_C31767").val(item.G1762_C31767);
 
                    $("#G1762_C31814").attr('option',item.G1762_C31814); 
 
                    $("#G1762_C31815").val(item.G1762_C31815);
 
                    $("#G1762_C31816").attr('option',item.G1762_C31816); 
 
                    $("#G1762_C31817").val(item.G1762_C31817); 
 
                    $("#G1762_C31968").val(item.G1762_C31968);
 
                    $("#G1762_C31799").val(item.G1762_C31799).trigger("change"); 
                    
                    $("#G1762_C34556").val(item.G1762_C34556).trigger("change"); 
                    
                    $("#G1762_C34556").val(item.G1762_C34557).trigger("change"); 
                    
                    $("#G1762_C34514").val(item.G1762_C34514);
   
                    if(item.G1762_C31800 == 1){
                        $("#G1762_C31800").attr('checked', true);
                    } 
			        cargarHijos_3($("#G1762_C31739").val());
					$("#h3mio").html(item.principal);

                });
                //Deshabilitar los campos 3

                //Habilitar todos los campos para edicion
                $('#FormularioDatos :input').each(function(){
                    $(this).attr('disabled', true);
                });

                              

                //Habilidar los botones de operacion, add, editar, eliminar
                $("#add").attr('disabled', false);
                $("#edit").attr('disabled', false);
                $("#delete").attr('disabled', false);

                //Desahabiliatra los botones de salvar y seleccionar_registro
                $("#cancel").attr('disabled', true);
                $("#Save").attr('disabled', true);
            } 
        });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

     	$("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        $("#btnLlamar_0").attr('padre', <?php echo $_GET['registroId'];?>);$("#btnLlamar_1").attr('padre', <?php echo $_GET['registroId'];?>);$("#btnLlamar_2").attr('padre', <?php echo $_GET['registroId'];?>);$("#btnLlamar_3").attr('padre', <?php echo $_GET['registroId'];?>);

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
        	/*$("#btnLlamar_0").attr('padre', <?php echo $_GET['user'];?>);$("#btnLlamar_1").attr('padre', <?php echo $_GET['user'];?>);$("#btnLlamar_2").attr('padre', <?php echo $_GET['user'];?>);$("#btnLlamar_3").attr('padre', <?php echo $_GET['user'];?>);
        	vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
        	idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            

            $.jgrid.gridUnload('#tablaDatosDetalless0');

            $.jgrid.gridUnload('#tablaDatosDetalless1');

            $.jgrid.gridUnload('#tablaDatosDetalless2');

            $.jgrid.gridUnload('#tablaDatosDetalless3');
            
    	$("#btnLlamar_0").attr('padre', $("#G1762_C31741").val());
    		var id_0 = $("#G1762_C31741").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
    		cargarHijos_0(id_0);
    	$("#btnLlamar_1").attr('padre', $("#G1762_C31739").val());
    		var id_1 = $("#G1762_C31739").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
    		cargarHijos_1(id_1);
    	$("#btnLlamar_2").attr('padre', $("#G1762_C31739").val());
    		var id_2 = $("#G1762_C31739").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
    		cargarHijos_2(id_2);
    	$("#btnLlamar_3").attr('padre', $("#G1762_C31739").val());
    		var id_3 = $("#G1762_C31739").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless3'); //funcion Recargar 
    		cargarHijos_3(id_3);
        });

        //Esta es la funcionalidad de los Tabs
        
 

        $("#tabs_click_0").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless0'); 
            $("#btnLlamar_0").attr('padre', $("#G1762_C31741").val());
    		var id_0 = $("#G1762_C31741").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
    		cargarHijos_0(id_0);
        });

        $("#btnLlamar_0").click(function( event ) {
            event.preventDefault(); 
            var padre = $("#G1762_C31741").val();
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1767&view=si&formaDetalle=si&formularioPadre=1762&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=31900<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                    $("#editarDatos").modal('show');
                    llamarHijo();
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var valido = 0;
                    
                    if (validado == '0') {
                    	var form = $("#FormularioDatos");
	                    //Se crean un array con los datos a enviar, apartir del formulario 
	                    var formData = new FormData($("#FormularioDatos")[0]);
	                    $.ajax({
	                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
	                        type: 'POST',
	                        data: formData,
	                        cache: false,
	                        contentType: false,
	                        processData: false,
	                        //una vez finalizado correctamente
	                        success: function(data){
	                            if(data){
	                                //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
	                                if($("#oper").val() == 'add'){
	                                    idTotal = data;
	                                }else{
	                                    idTotal= $("#hidId").val();
	                                }
	                                $("#hidId").val(idTotal);

	                                int_guardo = 1;
	                                $(".llamadores").attr('padre', idTotal);
	                                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1767&view=si&formaDetalle=si&formularioPadre=1762&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=31900&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
	                                $("#editarDatos").modal('show');
	                                $("#oper").val('edit');
                                    llamarHijo();

	                            }else{
	                                //Algo paso, hay un error
	                                alertify.error('Un error ha ocurrido');
	                            }                
	                        },
	                        //si ha ocurrido un error
	                        error: function(){
	                            after_save_error();
	                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
	                        }
	                    });
                    }
                }
            }else{

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1767&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1762&pincheCampo=31900&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');
                llamarHijo();
            }
        });

        $("#tabs_click_1").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless1'); 
            $("#btnLlamar_1").attr('padre', $("#G1762_C31739").val());
    		var id_1 = $("#G1762_C31739").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
    		cargarHijos_1(id_1);
        });

        $("#btnLlamar_1").click(function( event ) {
            event.preventDefault(); 
            var padre = $("#G1762_C31739").val();
            var id_llamada=$("#G1762_C31966").val();

           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1763&view=si&formaDetalle=si&id_llamada='+id_llamada+'&formularioPadre=1762&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=31827<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?><?php if(isset($_GET['campana_crm'])){ echo "&campana_crm=".$_GET['campana_crm']; }?>');
                    $("#editarDatos").modal('show');
                    llamarHijo();
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var valido = 0;
                    
                    if (validado == 0) {
                    	var form = $("#FormularioDatos");
	                    //Se crean un array con los datos a enviar, apartir del formulario 
	                    var formData = new FormData($("#FormularioDatos")[0]);
	                    $.ajax({
	                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
	                        type: 'POST',
	                        data: formData,
	                        cache: false,
	                        contentType: false,
	                        processData: false,
	                        //una vez finalizado correctamente
	                        success: function(data){
	                            if(data){
	                                //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
	                                if($("#oper").val() == 'add'){
	                                    idTotal = data;
	                                }else{
	                                    idTotal= $("#hidId").val();
	                                }
	                                $("#hidId").val(idTotal);

	                                int_guardo = 1;
	                                $(".llamadores").attr('padre', idTotal);
	                                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1763&view=si&formaDetalle=siid_llamada='+id_llamada+'&formularioPadre=1762&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=31827&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
	                                $("#editarDatos").modal('show');
	                                $("#oper").val('edit');
                                    llamarHijo();

	                            }else{
	                                //Algo paso, hay un error
	                                alertify.error('Un error ha ocurrido');
	                            }                
	                        },
	                        //si ha ocurrido un error
	                        error: function(){
	                            after_save_error();
	                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
	                        }
	                    });
                    }
                }
            }else{

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1763&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1762id_llamada='+id_llamada+'&pincheCampo=31827&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');
                llamarHijo();
            }
        });

        $("#tabs_click_2").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless2'); 
            $("#btnLlamar_2").attr('padre', $("#G1762_C31739").val());
    		var id_2 = $("#G1762_C31739").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
    		cargarHijos_2(id_2);
        });

        $("#btnLlamar_2").click(function( event ) {
            event.preventDefault(); 
            var padre = $("#G1762_C31739").val();
            


           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1772&view=si&formaDetalle=si&formularioPadre=1762&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=31993<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                    $("#editarDatos").modal('show');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var valido = 0;
                    
                    if (validado == '0') {
                    	var form = $("#FormularioDatos");
	                    //Se crean un array con los datos a enviar, apartir del formulario 
	                    var formData = new FormData($("#FormularioDatos")[0]);
	                    $.ajax({
	                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
	                        type: 'POST',
	                        data: formData,
	                        cache: false,
	                        contentType: false,
	                        processData: false,
	                        //una vez finalizado correctamente
	                        success: function(data){
	                            if(data){
	                                //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
	                                if($("#oper").val() == 'add'){
	                                    idTotal = data;
	                                }else{
	                                    idTotal= $("#hidId").val();
	                                }
	                                $("#hidId").val(idTotal);

	                                int_guardo = 1;
	                                $(".llamadores").attr('padre', idTotal);
	                                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1772&view=si&formaDetalle=si&formularioPadre=1762&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=31993&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
	                                $("#editarDatos").modal('show');
	                                $("#oper").val('edit');

	                            }else{
	                                //Algo paso, hay un error
	                                alertify.error('Un error ha ocurrido');
	                            }                
	                        },
	                        //si ha ocurrido un error
	                        error: function(){
	                            after_save_error();
	                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
	                        }
	                    });
                    }
                }
            }else{

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1772&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1762&pincheCampo=31993&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');
            }
        });

        $("#tabs_click_3").click(function(){ 
            $.jgrid.gridUnload('#tablaDatosDetalless3'); 
            $("#btnLlamar_3").attr('padre', $("#G1762_C31739").val());
    		var id_3 = $("#G1762_C31739").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless3'); //funcion Recargar 
    		cargarHijos_3(id_3);
        });

        $("#btnLlamar_3").click(function( event ) {
            event.preventDefault(); 
            var padre = $("#G1762_C31739").val();

           
            if($("#oper").val() == 'add'){
                if(before_save()){
                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1771&view=si&formaDetalle=si&formularioPadre=1762&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=31978<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                    $("#editarDatos").modal('show');
                }else{
                    before_save();
                    var d = new Date();
                    var h = d.getHours();
                    var horas = (h < 10) ? '0' + h : h;
                    var dia = d.getDate();
                    var dias = (dia < 10) ? '0' + dia : dia;
                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
                    $("#FechaFinal").val(fechaFinal);
                    
                    var valido = 0;
                    
                    if (validado == '0') {
                    	var form = $("#FormularioDatos");
	                    //Se crean un array con los datos a enviar, apartir del formulario 
	                    var formData = new FormData($("#FormularioDatos")[0]);
	                    $.ajax({
	                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
	                        type: 'POST',
	                        data: formData,
	                        cache: false,
	                        contentType: false,
	                        processData: false,
	                        //una vez finalizado correctamente
	                        success: function(data){
	                            if(data){
	                                //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
	                                if($("#oper").val() == 'add'){
	                                    idTotal = data;
	                                }else{
	                                    idTotal= $("#hidId").val();
	                                }
	                                $("#hidId").val(idTotal);

	                                int_guardo = 1;
	                                $(".llamadores").attr('padre', idTotal);
	                                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1771&view=si&formaDetalle=si&formularioPadre=1762&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=31978&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
	                                $("#editarDatos").modal('show');
	                                $("#oper").val('edit');

	                            }else{
	                                //Algo paso, hay un error
	                                alertify.error('Un error ha ocurrido');
	                            }                
	                        },
	                        //si ha ocurrido un error
	                        error: function(){
	                            after_save_error();
	                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
	                        }
	                    });
                    }
                }
            }else{

                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1771&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1762&pincheCampo=31978&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');
            }
        });
        //Select2 estos son los guiones
        


    $("#G1762_C31750").select2();

    $("#G1762_C31766").select2();

    $("#G1762_C31814").select2();

//    $("#G1762_C31815").select2();

    $("#G1762_C31816").select2();

//    $("#G1762_C31817").select2();

    $("#G1762_C31799").select2();
        //datepickers
        

        $("#G1762_C31725").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1762_C31748").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1762_C31726").wickedpicker(options);

        //Validaciones numeros Enteros
        

    	$("#G1762_C31966").numeric();
		        

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ¿Datos confirmados? 

    $("#G1762_C31750").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para TIPO DE LLAMADA 

    $("#G1762_C31766").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

		$.ajax({
			url    : '<?php echo $url_crud; ?>',
			type   : 'post',
			data   : { getListaHija : true , opcionID : '1704' , idPadre : $(this).val() },
			success : function(data){
				$("#G1762_C31814").html(data);
                $("#G1762_C31816").html('<option>Seleccione</option>');
                if($("#G1762_C31814").attr('option').length >0){
                    $("#G1762_C31814").val($("#G1762_C31814").attr('option')).trigger('change');    
                }
                
                
			}
		});
		
    });

    //function para SUBCLASIFICIÓN 

    $("#G1762_C31814").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

		$.ajax({
			url    : '<?php echo $url_crud; ?>',
			type   : 'post',
			data   : { getListaHija : true , opcionID : '1705' , link : 'si' ,idPadre : $(this).val() },
			success : function(data){
                $("#G1762_C31815").attr('href',data);
				$("#G1762_C31815").html(data);
                
			}
		});
        
		$.ajax({
			url    : '<?php echo $url_crud; ?>',
			type   : 'post',
			data   : { getListaHija : true , opcionID : '1706' , idPadre : $(this).val() },
			success : function(data){
				$("#G1762_C31816").html(data);
                if($("#G1762_C31816").attr('option').length >0){
                    $("#G1762_C31816").val($("#G1762_C31816").attr('option')).trigger('change');    
                }
                
			}
		});
		
    });

    //function para RESPUESTA 

    $("#G1762_C31815").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para TIPO QUEJA-DAÑOS 

    $("#G1762_C31816").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

		$.ajax({
			url    : '<?php echo $url_crud; ?>',
			type   : 'post',
			data   : { getListaHija : true , opcionID : '1707' , link : 'si' , idPadre : $(this).val() },
			success : function(data){
                $("#G1762_C31817").attr('href',data);
				$("#G1762_C31817").html(data);
			}
		});
		
    });

    //function para RESPUESTA  2 

    $("#G1762_C31817").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Origen llamada 

    $("#G1762_C31799").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });
        
    //function para Barrio 

    $("#G1762_C31741").change(function(){  
    	//Esto es para la busqueda al cambiar el campo de barrio
    	$('#tabs_click_0').click();

    });        
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
        	
        	var bol_respuesta = before_save();
        	var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
            	alertify.error("Es necesaria la tipificación!");
            	valido = 1;
            }

            if($(".reintento").val() == '2'){
            	if($(".TxtFechaReintento").val().length < 1){
            		alertify.error("Es necesario llenar la fecha de reintento!");
            		$(".TxtFechaReintento").focus();
            		valido = 1;
            	}

            	if($(".TxtHoraReintento").val().length < 1){
            		alertify.error("Es necesario llenar la hora de reintento!");
            		$(".TxtHoraReintento").focus();
            		valido = 1;
            	}
            }

            if(valido == '0'){
	        	if(bol_respuesta){            
		            var form = $("#FormularioDatos");
		            //Se crean un array con los datos a enviar, apartir del formulario 
		            var formData = new FormData($("#FormularioDatos")[0]);
		            $.ajax({
		               url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
		                type: 'POST',
		                data: formData,
		                cache: false,
		                contentType: false,
		                processData: false,
		                //una vez finalizado correctamente
		                success: function(data){
		                    if(data != '0'){
		                    	<?php if(!isset($_GET['campan'])){ ?>
			                        //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
			                        if($("#oper").val() == 'add'){
			                            idTotal = data;
			                        }else{
			                            idTotal= $("#hidId").val();
			                        }
			                       
			                        //Limpiar formulario
			                        form[0].reset();
			                        after_save();
			                        <?php if(isset($_GET['registroId'])){ ?>
			                        	var ID = <?=$_GET['registroId'];?>
			                        <?php }else{ ?>	
			                        	var ID = data
			                        <?php } ?>	
			                        $.ajax({
			                            url      : '<?=$url_crud;?>',
			                            type     : 'POST',
			                            data     : { CallDatos : 'SI', id : ID },
			                            dataType : 'json',
			                            success  : function(data){
			                                //recorrer datos y enviarlos al formulario
			                                $.each(data, function(i, item) {
		                                    

		                                    	$("#G1762_C31723").val(item.G1762_C31723).trigger("change"); 

		                                    	$("#G1762_C31724").val(item.G1762_C31724).trigger("change"); 
 
		                                    	$("#G1762_C31725").val(item.G1762_C31725);
 
		                                    	$("#G1762_C31726").val(item.G1762_C31726);
 
		                                    	$("#G1762_C31727").val(item.G1762_C31727);
 
		                                    	$("#G1762_C31728").val(item.G1762_C31728);
 
		                                    	$("#G1762_C31729").val(item.G1762_C31729);
 
		                                    	$("#G1762_C31730").val(item.G1762_C31730);
 
		                                    	$("#G1762_C31731").val(item.G1762_C31731);
      
			                                    if(item.G1762_C31735 == 1){
			                                       $("#G1762_C31735").attr('checked', true);
			                                    } 
 
		                                    	$("#G1762_C31966").val(item.G1762_C31966);
 
		                                    	$("#G1762_C31736").val(item.G1762_C31736);
 
		                                    	$("#G1762_C31737").val(item.G1762_C31737);
 
		                                    	$("#G1762_C31738").val(item.G1762_C31738);
 
		                                    	$("#G1762_C31739").val(item.G1762_C31739);
 
		                                    	$("#G1762_C31740").val(item.G1762_C31740);
 
		                                    	$("#G1762_C31741").val(item.G1762_C31741);
 
		                                    	$("#G1762_C31742").val(item.G1762_C31742);
 
		                                    	$("#G1762_C31743").val(item.G1762_C31743);
 
		                                    	$("#G1762_C31744").val(item.G1762_C31744);
 
		                                    	$("#G1762_C31745").val(item.G1762_C31745);
 
		                                    	$("#G1762_C31746").val(item.G1762_C31746);
 
		                                    	$("#G1762_C31747").val(item.G1762_C31747);
 
		                                    	$("#G1762_C31748").val(item.G1762_C31748);

		                                    	$("#G1762_C31750").val(item.G1762_C31750).trigger("change"); 

		                                    	$("#G1762_C31766").val(item.G1762_C31766).trigger("change"); 
 
		                                    	$("#G1762_C31767").val(item.G1762_C31767);

		                                    	$("#G1762_C31814").attr('option',item.G1762_C31814); 

		                                    	$("#G1762_C31815").val(item.G1762_C31815); 

		                                    	$("#G1762_C31816").attr('option',item.G1762_C31816); 

		                                    	$("#G1762_C31817").val(item.G1762_C31817); 
 
		                                    	$("#G1762_C31968").val(item.G1762_C31968);

		                                    	$("#G1762_C31799").val(item.G1762_C31799).trigger("change"); 
		                                    	
                                                $("#G1762_C34556").val(item.G1762_C34556).trigger("change"); 
                                                
                                                $("#G1762_C34556").val(item.G1762_C34557).trigger("change"); 
                                                
                                                $("#G1762_C34514").val(item.G1762_C34514);
      
			                                    if(item.G1762_C31800 == 1){
			                                       $("#G1762_C31800").attr('checked', true);
			                                    } 
		              							$("#h3mio").html(item.principal);
			                                });

			                                //Deshabilitar los campos 2

			                                //Habilitar todos los campos para edicion
			                                $('#FormularioDatos :input').each(function(){
			                                    $(this).attr('disabled', true);
			                                });

			                                //Habilidar los botones de operacion, add, editar, eliminar
			                                $("#add").attr('disabled', false);
			                                $("#edit").attr('disabled', false);
			                                $("#delete").attr('disabled', false);

			                                //Desahabiliatra los botones de salvar y seleccionar_registro
			                                $("#cancel").attr('disabled', true);
			                                $("#Save").attr('disabled', true);
			                            } 
			                        })
			                        $("#hidId").val(ID);  

		                        <?php }else{ 
		                        	if(!isset($_GET['formulario'])){
		                        ?>

		                        	$.ajax({
		                        		url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
		                        		type  : "post",
		                        		data  : formData,
		                    		 	cache: false,
					                    contentType: false,
					                    processData: false,
		                        		success : function(xt){
                                            borrarStorage($("#CampoIdGestionCbx").val());
		                        			console.log(xt);
                                            window.location.href = "quitar.php"; 
		                        		}
		                        	});
				                    
		                        <?php } 
		                        	}
		                        ?>            
		                    }else{
		                        //Algo paso, hay un error
		                        $("#Save").attr('disabled', false);
		                        alertify.error('Un error ha ocurrido y no pudimos guardar la información');
		                    }                
		                },
		                //si ha ocurrido un error
		                error: function(){
		                    after_save_error();
		                    $("#Save").attr('disabled', false);
		                    alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
		                }
		            });
          		}
          	}
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','Agente','Fecha','Hora','Campaña','ID LLAMADA','TELEFONO(ANI)','NOMBRE DEL SOLICITANTE - CONTACTO','CEDULA','TELEFONO','SUSCRIPTOR','DIRECCIÓN','BARRIO','MUNICIPIO','CICLO','EMAIL','PROPIETARIO','MEDIO DE NOTIFICACION','CELULAR','CODIGO MEDIDOR CONTROL','DIREECION ENVIO','FECHA PAGO','¿Datos confirmados?','TIPO DE LLAMADA','DESCRIPCIÓN','SUBCLASIFICIÓN','RESPUESTA','TIPO QUEJA-DAÑOS','RESPUESTA  2','GESTIÓN REALIZADA','Origen llamada'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

	                ,
	                { 
	                    name:'G1762_C31728', 
	                    index: 'G1762_C31728', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1762_C31729', 
	                    index: 'G1762_C31729', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1762_C31730', 
	                    index: 'G1762_C31730', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1762_C31731', 
	                    index: 'G1762_C31731', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
 
	                ,
	                {  
	                    name:'G1762_C31966', 
	                    index:'G1762_C31966', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1762_C31736', 
	                    index: 'G1762_C31736', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }	                
                    
                    ,
	                { 
	                    name:'G1762_C34514',
	                    index: 'G1762_C34514', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1762_C31737', 
	                    index: 'G1762_C31737', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1762_C31738', 
	                    index: 'G1762_C31738', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1762_C31739', 
	                    index: 'G1762_C31739', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1762_C31740', 
	                    index: 'G1762_C31740', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1762_C31741', 
	                    index: 'G1762_C31741', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1762_C31742', 
	                    index: 'G1762_C31742', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1762_C31743', 
	                    index: 'G1762_C31743', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1762_C31744', 
	                    index: 'G1762_C31744', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
                
	                ,
	                { 
	                    name:'G1762_C34556', 
	                    index:'G1762_C34556', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1880&campo=G1762_C34556'
	                    }
	                }                   
                
	                ,
	                { 
	                    name:'G1762_C34557', 
	                    index:'G1762_C34557', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1881&campo=G1762_C34557'
	                    }
	                }

	                ,
	                { 
	                    name:'G1762_C31745', 
	                    index: 'G1762_C31745', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1762_C31746', 
	                    index: 'G1762_C31746', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1762_C31747', 
	                    index: 'G1762_C31747', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                {  
	                    name:'G1762_C31748', 
	                    index:'G1762_C31748', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1762_C31750', 
	                    index:'G1762_C31750', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1412&campo=G1762_C31750'
	                    }
	                }

	                ,
	                { 
	                    name:'G1762_C31766', 
	                    index:'G1762_C31766', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1703&campo=G1762_C31766'
	                    }
	                }

	                ,
	                { 
	                    name:'G1762_C31767', 
	                    index:'G1762_C31767', 
	                    width:150, 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1762_C31814', 
	                    index:'G1762_C31814', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1704&campo=G1762_C31814'
	                    }
	                }

	                ,
	                { 
	                    name:'G1762_C31815', 
	                    index:'G1762_C31815', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1705&campo=G1762_C31815'
	                    }
	                }

	                ,
	                { 
	                    name:'G1762_C31816', 
	                    index:'G1762_C31816', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1706&campo=G1762_C31816'
	                    }
	                }

	                ,
	                { 
	                    name:'G1762_C31817', 
	                    index:'G1762_C31817', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1707&campo=G1762_C31817'
	                    }
	                }

	                ,
	                { 
	                    name:'G1762_C31968', 
	                    index:'G1762_C31968', 
	                    width:150, 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1762_C31799', 
	                    index:'G1762_C31799', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1424&campo=G1762_C31799'
	                    }
	                }
            ],
            
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1762_C31739',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            pager: "#pager",
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
	            ,subGrid: true,
	            subGridRowExpanded: function(subgrid_id, row_id) { 
	                // we pass two parameters 
	                // subgrid_id is a id of the div tag created whitin a table data 
	                // the id of this elemenet is a combination of the "sg_" + id of the row 
	                // the row_id is the id of the row 
	                // If we wan to pass additinal parameters to the url we can use 
	                // a method getRowData(row_id) - which returns associative array in type name-value 
	                // here we can easy construct the flowing 
	                $("#"+subgrid_id).html('');

                var subgrid_table_id_0, pager_id_0; 

                subgrid_table_id_0 = subgrid_id+"_t_0"; 

                pager_id_ = "p_"+subgrid_table_id_0; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_0+"' class='scroll'></table><div id='"+pager_id_0+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_0).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','ID LLAMADA','FECHA','HORA','CÓDIGO DE SUSCRIPTOR','DIRECCIÓN','BARRIO','NO. ORDEN DE TRABAJO','TIPO DE LLAMADA', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }
 
                        ,
                        {  
                            name:'G1767_C31887', 
                            index:'G1767_C31887', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).numeric();
                                }
                            }

                        }

                        ,
                        {  
                            name:'G1767_C31890', 
                            index:'G1767_C31890', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1767_C31889', 
                            index:'G1767_C31889', 
                            width:70 ,
                            editable: true ,
                            formatter: 'text', 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    //Timepicker
                                     var options = {  //hh:mm 24 hour format only, defaults to current time
                                        timeFormat: 'HH:mm:ss',
                                        interval: 5,
                                        minTime: '10',
                                        dynamic: false,
                                        dropdown: true,
                                        scrollbar: true
                                    }; 
                                    $(el).timepicker(options);
                    

                                }
                            }
                        }

                        ,
                        { 
                            name:'G1767_C31893', 
                            index: 'G1767_C31893', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1767_C31896', 
                            index: 'G1767_C31896', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1767_C31900', 
                            index: 'G1767_C31900', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1767_C31903', 
                            index: 'G1767_C31903', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1767_C31892', 
                            index: 'G1767_C31892', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
		                    editoptions:{ 
		                        dataInit: function(element) {                     
		                            $(element).val(id); 
		                        } 
		                    }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_0, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_0).jqGrid('navGrid',"#"+pager_id_0,{edit:false,add:false,del:false}) 

                var subgrid_table_id_1, pager_id_1; 

                subgrid_table_id_1 = subgrid_id+"_t_1"; 

                pager_id_ = "p_"+subgrid_table_id_1; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_1+"' class='scroll'></table><div id='"+pager_id_1+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_1).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_1=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','ID LLAMADA','NO. CASO','FECHA','HORA','TIPO DE CASO','SUB-TIPO','padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }
                        ,
                        {  
                            name:'G1763_C31824', 
                            index:'G1763_C31824', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,

                                    dataInit:function(el){
                                        $(el).numeric();
                                    }
                            }

                        }

                        ,
                        {  
                            name:'G1763_C31825', 
                            index:'G1763_C31825', 
                            width:80 ,
                            editable: true, 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,

                                    dataInit:function(el){
                                        $(el).numeric();
                                    }
                            }

                        }

                        ,
                        {  
                            name:'G1763_C31826', 
                            index:'G1763_C31826', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1763_C31829', 
                            index:'G1763_C31829', 
                            width:70 ,
                            editable: true ,
                            formatter: 'text', 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    //Timepicker
                                    var options = {  //hh:mm 24 hour format only, defaults to current time
                                        timeFormat: 'HH:mm:ss',
                                        interval: 5,
                                        minTime: '10',
                                        dynamic: false,
                                        dropdown: true,
                                        scrollbar: true
                                    }; 
                                    $(el).timepicker(options);
                                    $(".timepicker").css("z-index", 99999 );
                                }
                            }
                        }
                        
                        ,
                        {  
                            name:'G1763_C31835', 
                            index:'G1763_C31835', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1710&campo=G1763_C31835'
                            }
                        }
                        ,
                        {  
                            name:'G1763_C31858', 
                            index:'G1763_C31858', 
                            width:120 ,
                            editable: true, 
                            edittype:"select" , 
                            editoptions: {
                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1711&campo=G1763_C31858'
                            }
                        }                        
                       
                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
		                    editoptions:{ 
		                        dataInit: function(element) {                     
		                            $(element).val(id); 
		                        } 
		                    }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_1, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_1).jqGrid('navGrid',"#"+pager_id_1,{edit:false,add:false,del:false}) 

                var subgrid_table_id_2, pager_id_2; 

                subgrid_table_id_2 = subgrid_id+"_t_2"; 

                pager_id_ = "p_"+subgrid_table_id_2; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_2+"' class='scroll'></table><div id='"+pager_id_2+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_2).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_2=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','NÚMERO ORDEN DE TRABAJO','TIPO DE SERVICIO','FECHA DE SOLICITUD','OBSERVACIÓN SERVICIO','SECCIÓN','SUSCRIPTOR','HORA SOLICITUD', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                        ,
                        { 
                            name:'G1772_C31988', 
                            index: 'G1772_C31988', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1772_C31989', 
                            index: 'G1772_C31989', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1772_C31990', 
                            index: 'G1772_C31990', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1772_C31991', 
                            index:'G1772_C31991', 
                            width:150, 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1772_C31992', 
                            index: 'G1772_C31992', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1772_C31993', 
                            index: 'G1772_C31993', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1772_C31994', 
                            index:'G1772_C31994', 
                            width:70 ,
                            editable: true ,
                            formatter: 'text', 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    //Timepicker
                                     var options = {  //hh:mm 24 hour format only, defaults to current time
                                        timeFormat: 'HH:mm:ss',
                                        interval: 5,
                                        minTime: '10',
                                        dynamic: false,
                                        dropdown: true,
                                        scrollbar: true
                                    }; 
                                    $(el).timepicker(options);
                    

                                }
                            }
                        }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
		                    editoptions:{ 
		                        dataInit: function(element) {                     
		                            $(element).val(id); 
		                        } 
		                    }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_2, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_2).jqGrid('navGrid',"#"+pager_id_2,{edit:false,add:false,del:false}) 

                var subgrid_table_id_3, pager_id_3; 

                subgrid_table_id_3 = subgrid_id+"_t_3"; 

                pager_id_ = "p_"+subgrid_table_id_3; 

                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_3+"' class='scroll'></table><div id='"+pager_id_3+"' class='scroll'></div>"); 

                jQuery("#"+subgrid_table_id_3).jqGrid({ 
                    url:'<?=$url_crud;?>?callDatosSubgrilla_3=si&id='+row_id,
                    datatype: 'xml',
                    mtype: 'POST',
                    colNames:['id','ID LLAMADA','FECHA','HORA','CODIGO DE SUSCRIPTOR','DIRECCION','BARRIO','NUMERO ORDEN DE TRABAJO','TIPO DE LLAMADA', 'padre'],
                    colModel: [ 
                        {    
                            name:'providerUserId',
                            index:'providerUserId', 
                            width:100,editable:true, 
                            editrules:{
                                required:false, 
                                edithidden:true
                            },
                            hidden:true, 
                            editoptions:{ 
                                dataInit: function(element) {                     
                                    $(element).attr("readonly", "readonly"); 
                                } 
                            }
                        }

                        ,
                        { 
                            name:'G1771_C31975', 
                            index: 'G1771_C31975', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        {  
                            name:'G1771_C31976', 
                            index:'G1771_C31976', 
                            width:120 ,
                            editable: true ,
                            formatter: 'text', 
                            searchoptions: {
                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                            }, 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    $(el).datepicker({
                                        language: "es",
                                        autoclose: true,
                                        todayHighlight: true
                                    });
                                },
                                defaultValue: function(){
                                    var currentTime = new Date();
                                    var month = parseInt(currentTime.getMonth() + 1);
                                    month = month <= 9 ? "0"+month : month;
                                    var day = currentTime.getDate();
                                    day = day <= 9 ? "0"+day : day;
                                    var year = currentTime.getFullYear();
                                    return year+"-"+month + "-"+day;
                                }
                            }
                        }

                        ,
                        {  
                            name:'G1771_C31977', 
                            index:'G1771_C31977', 
                            width:70 ,
                            editable: true ,
                            formatter: 'text', 
                            editoptions:{
                                size:20,
                                dataInit:function(el){
                                    //Timepicker
                                     var options = {  //hh:mm 24 hour format only, defaults to current time
                                        timeFormat: 'HH:mm:ss',
                                        interval: 5,
                                        minTime: '10',
                                        dynamic: false,
                                        dropdown: true,
                                        scrollbar: true
                                    }; 
                                    $(el).timepicker(options);
                    

                                }
                            }
                        }

                        ,
                        { 
                            name:'G1771_C31978', 
                            index: 'G1771_C31978', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1771_C31979', 
                            index: 'G1771_C31979', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1771_C31980', 
                            index: 'G1771_C31980', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1771_C31981', 
                            index: 'G1771_C31981', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name:'G1771_C31982', 
                            index: 'G1771_C31982', 
                            width:160, 
                            resizable:false, 
                            sortable:true , 
                            editable: true 
                        }

                        ,
                        { 
                            name: 'Padre', 
                            index:'Padre', 
                            hidden: true , 
                            editable: false, 
                            editrules: { 
                                edithidden:true 
                            },
		                    editoptions:{ 
		                        dataInit: function(element) {                     
		                            $(element).val(id); 
		                        } 
		                    }
                        }
                    ], 
                    rowNum:20, 
                    pager: pager_id_3, 
                    sortname: 'num', 
                    sortorder: "asc",
                    height: '100%' 
                }); 

                jQuery("#"+subgrid_table_id_3).jqGrid('navGrid',"#"+pager_id_3,{edit:false,add:false,del:false}) 

            }, 
            subGridRowColapsed: function(subgrid_id, row_id) { 
                // this function is called before removing the data 
                //var subgrid_table_id; 
                //subgrid_table_id = subgrid_id+"_t"; 
                //jQuery("#"+subgrid_table_id).remove(); 
            }
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //SECCION  : Manipular Lista de Navegacion

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(x){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', Busqueda : x},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');
            $("#btnLlamar_0").attr('padre', id);$("#btnLlamar_1").attr('padre', id);$("#btnLlamar_2").attr('padre', id);$("#btnLlamar_3").attr('padre', id);
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
            

            $.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
            

            $.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
            

            $.jgrid.gridUnload('#tablaDatosDetalless3'); //funcion Recargar 
            
 
                    $("#G1762_C31723").val(item.G1762_C31723).trigger("change"); 
 
                    $("#G1762_C31724").val(item.G1762_C31724).trigger("change"); 

                        $("#G1762_C31725").val(item.G1762_C31725);

                        $("#G1762_C31726").val(item.G1762_C31726);

                        $("#G1762_C31727").val(item.G1762_C31727);

                        $("#G1762_C31728").val(item.G1762_C31728);

                        $("#G1762_C31729").val(item.G1762_C31729);

                        $("#G1762_C31730").val(item.G1762_C31730);

                        $("#G1762_C31731").val(item.G1762_C31731);
    
                        if(item.G1762_C31735 == 1){
                           $("#G1762_C31735").attr('checked', true);
                        } 

                        $("#G1762_C31966").val(item.G1762_C31966);

                        $("#G1762_C31736").val(item.G1762_C31736);

                        $("#G1762_C31737").val(item.G1762_C31737);

                        $("#G1762_C31738").val(item.G1762_C31738);

                        $("#G1762_C31739").val(item.G1762_C31739);

                        $("#G1762_C31740").val(item.G1762_C31740);

                        $("#G1762_C31741").val(item.G1762_C31741);

                        $("#G1762_C31742").val(item.G1762_C31742);

                        $("#G1762_C31743").val(item.G1762_C31743);

                        $("#G1762_C31744").val(item.G1762_C31744);

                        $("#G1762_C31745").val(item.G1762_C31745);

                        $("#G1762_C31746").val(item.G1762_C31746);

                        $("#G1762_C31747").val(item.G1762_C31747);

                        $("#G1762_C31748").val(item.G1762_C31748);
 
                        $("#G1762_C31750").val(item.G1762_C31750).trigger("change"); 

                        $("#G1762_C31766").val(item.G1762_C31766).trigger("change"); 

                        $("#G1762_C31767").val(item.G1762_C31767);

                        $("#G1762_C31814").attr('option',item.G1762_C31814); 

                        $("#G1762_C31815").val(item.G1762_C31815); 

                        $("#G1762_C31816").attr('option',item.G1762_C31816); 

                        $("#G1762_C31817").val(item.G1762_C31817); 

                        $("#G1762_C31968").val(item.G1762_C31968);

                        $("#G1762_C34514").val(item.G1762_C34514);

                        $("#G1762_C31799").val(item.G1762_C31799).trigger("change"); 
                        
                        $("#G1762_C34556").val(item.G1762_C34556).trigger("change"); 
                        
                        $("#G1762_C34557").val(item.G1762_C34557).trigger("change"); 
    
                        if(item.G1762_C31800 == 1){
                           $("#G1762_C31800").attr('checked', true);
                        } 
			            cargarHijos_3($("#G1762_C31739").val());
        				$("#h3mio").html(item.principal);
        				
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        

            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion descargar 

            $.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion descargar 

            $.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion descargar 

            $.jgrid.gridUnload('#tablaDatosDetalless3'); //funcion descargar 
    } 

    <?php } ?>


    

    function cargarHijos_0(id_0){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless0").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+id_0,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','ID LLAMADA','FECHA','HORA','CÓDIGO DE SUSCRIPTOR','DIRECCIÓN','BARRIO','NO. ORDEN DE TRABAJO','TIPO DE LLAMADA', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }
 
                ,
                {  
                    name:'G1767_C31887', 
                    index:'G1767_C31887', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }

                ,
                {  
                    name:'G1767_C31890', 
                    index:'G1767_C31890', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1767_C31889', 
                    index:'G1767_C31889', 
                    width:70 ,
                    editable: true ,
                    formatter: 'text', 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            //Timepicker
                            var options = {  //hh:mm 24 hour format only, defaults to current time
                                timeFormat: 'HH:mm:ss',
                                interval: 5,
                                minTime: '10',
                                dynamic: false,
                                dropdown: true,
                                scrollbar: true
                            }; 
                            $(el).timepicker(options);
                            $(".timepicker").css("z-index", 99999 );
                        }
                    }
                }

                ,
                { 
                    name:'G1767_C31893', 
                    index: 'G1767_C31893', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1767_C31896', 
                    index: 'G1767_C31896', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1767_C31900', 
                    index: 'G1767_C31900', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1767_C31903', 
                    index: 'G1767_C31903', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1767_C31892', 
                    index: 'G1767_C31892', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
                
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_0); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles0",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1767_C31887',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'HISTORICO BARRIOS',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_0=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1767&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=31900&dbclick=si&formularioPadre=1762<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless0").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_1(id_1){
        var id_llamada=$("#G1762_C31966").val();
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless1").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_1=si&id='+id_1,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','ID LLAMADA','NO. CASO','FECHA','HORA','TIPO','SUB-TIPO','CÓDIGO DE SUSCRIPTOR','ESTADO','FECHA DE CIERRE','padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }
 
                ,
                {  
                    name:'G1763_C31824', 
                    index:'G1763_C31824', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }
 
                ,
                {  
                    name:'G1763_C31825', 
                    index:'G1763_C31825', 
                    width:80 ,
                    editable: true, 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,

			        		dataInit:function(el){
    							$(el).numeric();
    						}
                    }

                }
                
                ,
                {  
                    name:'G1763_C31826', 
                    index:'G1763_C31826', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }
                
                ,
                {  
                    name:'G1763_C31829', 
                    index:'G1763_C31829', 
                    width:70 ,
                    editable: true ,
                    formatter: 'text', 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            //Timepicker
                            var options = {  //hh:mm 24 hour format only, defaults to current time
                                timeFormat: 'HH:mm:ss',
                                interval: 5,
                                minTime: '10',
                                dynamic: false,
                                dropdown: true,
                                scrollbar: true
                            }; 
                            $(el).timepicker(options);
                            $(".timepicker").css("z-index", 99999 );
                        }
                    }
                }
                
                ,
                {  
                    name:'G1763_C31835', 
                    index:'G1763_C31835', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1710&campo=G1763_C31835'
                    }
                }
                ,
                {  
                    name:'G1763_C31858', 
                    index:'G1763_C31858', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1711&campo=G1763_C31858'
                    }
                }
                
                ,
                { 
                    name:'G1763_C31827', 
                    index: 'G1763_C31827', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
                
                ,
                {  
                    name:'G1763_C31970', 
                    index:'G1763_C31970', 
                    width:120 ,
                    editable: true, 
                    edittype:"select" , 
                    editoptions: {
                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1725&campo=G1763_C31970'
                    }
                }

                ,
                {  
                    name:'G1763_C31971', 
                    index:'G1763_C31971', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }                

                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_1); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles1",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1763_C31824',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'CASOS',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_1=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,
            
            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1763&view=si&idllamada='+id_llamada+'&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=31827&formularioPadre=1762<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless1").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_2(id_2){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless2").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_2=si&id='+id_2,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','NÚMERO ORDEN DE TRABAJO','TIPO DE SERVICIO','FECHA DE SOLICITUD','OBSERVACIÓN SERVICIO','SECCIÓN','SUSCRIPTOR','HORA SOLICITUD','padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                ,
                { 
                    name:'G1772_C31988', 
                    index: 'G1772_C31988', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1772_C31989', 
                    index: 'G1772_C31989', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1772_C31990', 
                    index: 'G1772_C31990', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {
                    name:'G1772_C31991', 
                    index:'G1772_C31991', 
                    width:150, 
                    editable: true 
                }

                ,
                { 
                    name:'G1772_C31992', 
                    index: 'G1772_C31992', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1772_C31993', 
                    index: 'G1772_C31993', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1772_C31994', 
                    index:'G1772_C31994', 
                    width:70 ,
                    editable: true ,
                    formatter: 'text', 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            //Timepicker
                            var options = {  //hh:mm 24 hour format only, defaults to current time
                                timeFormat: 'HH:mm:ss',
                                interval: 5,
                                minTime: '10',
                                dynamic: false,
                                dropdown: true,
                                scrollbar: true
                            }; 
                            $(el).timepicker(options);
                            $(".timepicker").css("z-index", 99999 );
                        }
                    }
                }

                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_2); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles2",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1772_C31988',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'ORDEN DE TRABAJO',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_2=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId){
                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1772&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=31995&formularioPadre=1762<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless2").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function cargarHijos_3(id_3){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastSels;
        $("#tablaDatosDetalless3").jqGrid({
            url:'<?=$url_crud;?>?callDatosSubgrilla_3=si&id='+id_3,
            datatype: 'xml',
            mtype: 'POST',
            xmlReader: { 
                root:"rows", 
                row:"row",
                cell:"cell",
                id : "[asin]"
            },
            colNames:['id','ID LLAMADA','FECHA','HORA','CODIGO DE SUSCRIPTOR','DIRECCION','BARRIO','NUMERO ORDEN DE TRABAJO','TIPO DE LLAMADA', 'padre'],
            colModel:[

                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                ,
                { 
                    name:'G1771_C31975', 
                    index: 'G1771_C31975', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                {  
                    name:'G1771_C31976', 
                    index:'G1771_C31976', 
                    width:120 ,
                    editable: true ,
                    formatter: 'text', 
                    searchoptions: {
                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                    }, 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            $(el).datepicker({
                                language: "es",
                                autoclose: true,
                                todayHighlight: true
                            });
                        },
                        defaultValue: function(){
                            var currentTime = new Date();
                            var month = parseInt(currentTime.getMonth() + 1);
                            month = month <= 9 ? "0"+month : month;
                            var day = currentTime.getDate();
                            day = day <= 9 ? "0"+day : day;
                            var year = currentTime.getFullYear();
                            return year+"-"+month + "-"+day;
                        }
                    }
                }

                ,
                {  
                    name:'G1771_C31977', 
                    index:'G1771_C31977', 
                    width:70 ,
                    editable: true ,
                    formatter: 'text', 
                    editoptions:{
                        size:20,
                        dataInit:function(el){
                            //Timepicker
                            var options = {  //hh:mm 24 hour format only, defaults to current time
                                timeFormat: 'HH:mm:ss',
                                interval: 5,
                                minTime: '10',
                                dynamic: false,
                                dropdown: true,
                                scrollbar: true
                            }; 
                            $(el).timepicker(options);
                            $(".timepicker").css("z-index", 99999 );
                        }
                    }
                }

                ,
                { 
                    name:'G1771_C31978', 
                    index: 'G1771_C31978', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1771_C31979', 
                    index: 'G1771_C31979', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1771_C31980', 
                    index: 'G1771_C31980', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1771_C31981', 
                    index: 'G1771_C31981', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }

                ,
                { 
                    name:'G1771_C31982', 
                    index: 'G1771_C31982', 
                    width:160, 
                    resizable:false, 
                    sortable:true , 
                    editable: true 
                }
                ,
                { 
                    name: 'Padre', 
                    index:'Padre', 
                    hidden: true , 
                    editable: true, 
                    editrules: {
                        edithidden:true
                    },
                    editoptions:{ 
                        dataInit: function(element) {                     
                            $(element).val(id_3); 
                        } 
                    }
                }
            ],
            rowNum: 40,
            pager: "#pagerDetalles3",
            rowList: [40,80],
            sortable: true,
            sortname: 'G1771_C31975',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'HISTORICO LLAMADAS',
            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_3=si&usuario=<?php echo getIdentificacionUser($token);?>",
            height:'250px',
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastSels){
                    
                }
                lastSels = rowid;
            }
            ,

            ondblClickRow: function(rowId) {
                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1771&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=31978&dbclick=si&formularioPadre=1762<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
                $("#editarDatos").modal('show');

            }
        }); 

        $(window).bind('resize', function() {
            $("#tablaDatosDetalless3").setGridWidth($(window).width());
        }).trigger('resize');
    }

    function vamosRecargaLasGrillasPorfavor(id){
        
    	$("#btnLlamar_0").attr('padre', $("#G1762_C31741").val());
    		var id_0 = $("#G1762_C31741").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
    		cargarHijos_0(id_0);
    	$("#btnLlamar_1").attr('padre', $("#G1762_C31739").val());
    		var id_1 = $("#G1762_C31739").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
    		cargarHijos_1(id_1);
    	$("#btnLlamar_2").attr('padre', $("#G1762_C31739").val());
    		var id_2 = $("#G1762_C31739").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
    		cargarHijos_2(id_2);
    	$("#btnLlamar_3").attr('padre', $("#G1762_C31739").val());
    		var id_3 = $("#G1762_C31739").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless3'); //funcion Recargar 
    		cargarHijos_3(id_3);
    }
</script>
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
    guardarStorage($("#CampoIdGestionCbx").val(),"N/A","N/A");
    var EventChange=sessionStorage.getItem("gestiones");
    EventChange = JSON.parse(EventChange);
    setTimeout(function(){
        $.each(EventChange,function(i,item){
            if(typeof(item) == 'object'){
                if(item.hasOwnProperty('id_gestion') && item.id_gestion == $("#CampoIdGestionCbx").val()){
                    if(item.hasOwnProperty('ObjCliente')){
                        $.each(item.ObjCliente, function(c, camp){
                            if(camp.hasOwnProperty('type') && camp.type == 'SELECT'){
                                $("#"+camp.id).val(camp.value).trigger("change");
                            }
                        });
                    }
                }
            }
        });        
    },500) 
	$(document).ready(function() {
		<?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                		while($objDatos = $resultDatosSql->fetch_object()){ ?>
                        	document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
             		<?php  
             			}	
                    }
                    
                } */  
            }
        ?>
    	<?php if(isset($_GET['user'])){ ?>
        	
    	$("#btnLlamar_0").attr('padre', $("#G1762_C31741").val());
    		var id_0 = $("#G1762_C31741").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
    		cargarHijos_0(id_0);
    	$("#btnLlamar_1").attr('padre', $("#G1762_C31739").val());
    		var id_1 = $("#G1762_C31739").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless1'); //funcion Recargar 
    		cargarHijos_1(id_1);
    	$("#btnLlamar_2").attr('padre', $("#G1762_C31739").val());
    		var id_2 = $("#G1762_C31739").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless2'); //funcion Recargar 
    		cargarHijos_2(id_2);
    	$("#btnLlamar_3").attr('padre', $("#G1762_C31739").val());
    		var id_3 = $("#G1762_C31739").val();
    		$.jgrid.gridUnload('#tablaDatosDetalless3'); //funcion Recargar 
    		cargarHijos_3(id_3);
        	idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
		
	});
</script>
