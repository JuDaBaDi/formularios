<?php
    session_start();
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

        if(isset($_GET['EliminarTareaCancelada'])){

            $eliminar = "DELETE FROM ".$BaseDatos.".G1134 WHERE G1134_ConsInte__b = ".$_POST["id"];

            $eliminando = $mysqli->query($eliminar);

        }

      //Datos del formulario
      	if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1134_ConsInte__b, G1134_FechaInsercion , USUARI_Nombre____b,  G1134_CodigoMiembro  , G1134_PoblacionOrigen , G1134_EstadoDiligenciamiento ,  G1134_IdLlamada , G1134_C17457 as principal ,G1134_C20670,G1134_C17457,G1134_CodigoMiembro,G1134_C17458,G1134_C20333,G1134_C20671,G1134_C20672,G1134_C17459,G1134_C17460,G1134_C17461,G1134_C17462,G1134_C17463,G1134_C18912,G1134_C17464,G1134_C17465,G1134_C17466,G1134_C17467,G1134_C17468,G1134_C18913,G1134_C18914,G1134_C18915,G1134_C22657,G1134_C17469,G1134_C17470,G1134_C17471,G1134_C17472,G1134_C17473,G1134_C17474,G1134_C17475,G1134_C17476,G1134_C17477,G1134_C17478,G1134_C17479,G1134_C21449,G1134_C21450,G1134_C18344,G1134_C17481,G1134_C18345,G1134_C18346,G1134_C18347,G1134_C18348,G1134_C18349,G1134_C18350,G1134_C17482,G1134_C20956,G1134_C20957,G1134_C20958,G1134_C20959,G1134_C18916,G1134_C18917,G1134_C18918,G1134_C18919,G1134_C18920,G1134_C18921 FROM '.$BaseDatos.'.G1134 LEFT JOIN DYALOGOCRM_SISTEMA.USUARI ON USUARI_ConsInte__b =G1134_Usuario WHERE G1134_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1134_C20670'] = $key->G1134_C20670;

                $datos[$i]['G1134_CodigoMiembro'] = $key->G1134_CodigoMiembro;

                $datos[$i]['G1134_Usuario'] = $key->USUARI_Nombre____b;

                $datos[$i]['G1134_C17457'] = $key->G1134_C17457;

                $datos[$i]['G1134_C17458'] = $key->G1134_C17458;

                $datos[$i]['G1134_C20333'] = $key->G1134_C20333;

                $datos[$i]['G1134_C20671'] = $key->G1134_C20671;

                $datos[$i]['G1134_C20672'] = $key->G1134_C20672;

                $datos[$i]['G1134_C17459'] = $key->G1134_C17459;

                $datos[$i]['G1134_C17460'] = $key->G1134_C17460;

                $datos[$i]['G1134_C17461'] = $key->G1134_C17461;

                $datos[$i]['G1134_C17462'] = $key->G1134_C17462;

                $datos[$i]['G1134_C17463'] = $key->G1134_C17463;

                $datos[$i]['G1134_C18912'] = $key->G1134_C18912;

                $datos[$i]['G1134_C17464'] = $key->G1134_C17464;

                $datos[$i]['G1134_C17465'] = $key->G1134_C17465;

                $datos[$i]['G1134_C17466'] = $key->G1134_C17466;

                $datos[$i]['G1134_C17467'] = $key->G1134_C17467;

                $datos[$i]['G1134_C17468'] = $key->G1134_C17468;

                $datos[$i]['G1134_C18913'] = $key->G1134_C18913;

                $datos[$i]['G1134_C18914'] = explode(' ', $key->G1134_C18914)[0];

                $datos[$i]['G1134_C18915'] = explode(' ', $key->G1134_C18915)[0];

                $datos[$i]['G1134_C22657'] = $key->G1134_C22657;

                $datos[$i]['G1134_C17469'] = $key->G1134_C17469;

                $datos[$i]['G1134_C17470'] = $key->G1134_C17470;

                $datos[$i]['G1134_C17471'] = $key->G1134_C17471;

                $datos[$i]['G1134_C17472'] = $key->G1134_C17472;

                $datos[$i]['G1134_C17473'] = explode(' ', $key->G1134_C17473)[0];
  
                $hora = '';
                if(!is_null($key->G1134_C17474)){
                    $hora = explode(' ', $key->G1134_C17474)[1];
                }

                $datos[$i]['G1134_C17475'] = $key->G1134_C17475;

                $datos[$i]['G1134_C17476'] = $key->G1134_C17476;

                $datos[$i]['G1134_C17477'] = $key->G1134_C17477;

                $datos[$i]['G1134_C17478'] = $key->G1134_C17478;

                $datos[$i]['G1134_C17479'] = $key->G1134_C17479;

                $datos[$i]['G1134_C21449'] = $key->G1134_C21449;

                $datos[$i]['G1134_C21450'] = $key->G1134_C21450;

                $datos[$i]['G1134_C18344'] = $key->G1134_C18344;

                $datos[$i]['G1134_C17481'] = $key->G1134_C17481;

                $datos[$i]['G1134_C18345'] = $key->G1134_C18345;

                $datos[$i]['G1134_C18346'] = $key->G1134_C18346;

                $datos[$i]['G1134_C18347'] = $key->G1134_C18347;

                $datos[$i]['G1134_C18348'] = $key->G1134_C18348;

                $datos[$i]['G1134_C18349'] = explode(' ', $key->G1134_C18349)[0];

                $datos[$i]['G1134_C18350'] = $key->G1134_C18350;

                $datos[$i]['G1134_C17482'] = $key->G1134_C17482;

                $datos[$i]['G1134_C20956'] = explode(' ', $key->G1134_C20956)[0];
  
                $hora = '';
                if(!is_null($key->G1134_C20957)){
                    $hora = explode(' ', $key->G1134_C20957)[1];
                }

                $datos[$i]['G1134_C20958'] = explode(' ', $key->G1134_C20958)[0];
  
                $hora = '';
                if(!is_null($key->G1134_C20959)){
                    $hora = explode(' ', $key->G1134_C20959)[1];
                }

                $datos[$i]['G1134_C18916'] = $key->G1134_C18916;

                $datos[$i]['G1134_C18917'] = $key->G1134_C18917;

                $datos[$i]['G1134_C18918'] = $key->G1134_C18918;

                $datos[$i]['G1134_C18919'] = $key->G1134_C18919;

                $datos[$i]['G1134_C18920'] = $key->G1134_C18920;

                $datos[$i]['G1134_C18921'] = $key->G1134_C18921;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        /*Esto es para traer el historico del BackOffice*/
        if(isset($_POST['DameHistorico'])){

        	if(isset($_POST['campana_crm']) && $_POST['campana_crm'] != 0){
        		$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConfDinam_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST['campana_crm'];
	            $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	            $datoCampan = $res_Lsql_Campan->fetch_array();
	            $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	            $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	            $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];


	            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA_BACKOFFICE JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b LEFT JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_POST['campana_crm']." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_POST['user_codigo_mien']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";

                // echo $Lsql; die();

	            $res = $mysqli->query($Lsql);
	            while($key = $res->fetch_object()){
	                echo "<tr>";
	                echo "<td>".$key->MONOEF_Texto_____b."</td>";
	                echo "<td>".$key->CONDIA_Observacio_b."</td>";
	                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
	                echo "<td>".$key->USUARI_Nombre____b."</td>";
	                echo "</tr>";
	            }
        	}
            
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1134_ConsInte__b as id,  G1134_C17457 as camp1 , G1134_C17459 as camp2 , LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1134 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1134_C17470  WHERE TRUE ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " AND ( G1134_C17457 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1134_C17459 like '%".$_POST['Busqueda']."%' ) ";
            }

            if(!is_null($_POST['CodMiembro']) && $_POST['CodMiembro'] != ''){
                $Lsql .= " AND G1134_CodigoMiembro = ". $_POST['CodMiembro'] ." ";
            }
            
			if(!is_null($_POST['G1134_C20670']) && $_POST['G1134_C20670'] != ''){
				$Lsql .= " AND G1134_C20670 LIKE '%". $_POST['G1134_C20670'] ."%' ";
			}
			if(!is_null($_POST['G1134_C17457']) && $_POST['G1134_C17457'] != ''){
				$Lsql .= " AND G1134_C17457 LIKE '%". $_POST['G1134_C17457'] ."%' ";
			}
			if(!is_null($_POST['G1134_C17458']) && $_POST['G1134_C17458'] != ''){
				$Lsql .= " AND G1134_C17458 LIKE '%". $_POST['G1134_C17458'] ."%' ";
			}
			if(!is_null($_POST['G1134_C17459']) && $_POST['G1134_C17459'] != ''){
				$Lsql .= " AND G1134_C17459 LIKE '%". $_POST['G1134_C17459'] ."%' ";
			}

            $Lsql .= " ORDER BY field (G1134_C17470,14264,14265,14266,14267,14268), G1134_FechaInsercion DESC  LIMIT 0, 50 "; 
            
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){

            	$color = '';
                $strIconoBackOffice = '';
                if($key->estado == 'En gestión' || $key->estado == 'En gestión por devolución'){
                    $color = 'color:blue;';
                    $strIconoBackOffice = '1';
                }else if($key->estado == 'Cerrada'){
                    $color = 'color:green;';
                    $strIconoBackOffice = '2';
                }else if($key->estado == 'Devuelta'){
                    $color = 'color:red;';
                    $strIconoBackOffice = '3';
                }else if($key->estado == 'Sin gestión'){
                    $color = 'color:orange;';
                    $strIconoBackOffice = '4';
                }

                $datos[$i]['camp1'] = mb_strtoupper(($key->camp1));
                $datos[$i]['camp2'] = mb_strtolower(($key->camp2));
                $datos[$i]['estado'] = $strIconoBackOffice;
                $datos[$i]['color'] = $color;
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
	    	$Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID']." ORDER BY LISOPC_Nombre____b ASC";
	    	$res = $mysqli->query($Lsql);
	    	echo "<option value='0'>Seleccione</option>";
	    	while($key = $res->fetch_object()){
	    		echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
	    	}
	    }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        

        if(isset($_GET['MostrarCombo_Guion_G1134_C18918'])){
            echo '<select class="form-control input-sm"  name="G1134_C18918" id="G1134_C18918">';
            echo '<option >Buscar</option>';
            echo '</select>';
        }

        if(isset($_GET['CallDatosCombo_Guion_G1134_C18918'])){
            $Ysql = "SELECT G1188_ConsInte__b as id,  G1188_C18652 as text FROM ".$BaseDatos.".G1188 WHERE G1188_C18652 LIKE '%".$_POST['q']."%'";
            $guion = $mysqli->query($Ysql);
            $i = 0;
            $datos = array();
            while($obj = $guion->fetch_object()){
                $datos[$i]['id'] = $obj->id;
                $datos[$i]['text'] = $obj->text;
                $i++;
            } 
            echo json_encode($datos);
        }

        if(isset($_POST['dameValoresCamposDinamicos_Guion_G1134_C18918'])){
             $Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos.".G1188 WHERE G1188_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_G1134_C18918'];
            $res = $mysqli->query($Lsql);
            $data = array();
            $i = 0;
            while ($key = $res->fetch_object()) {
	            
                $i++;
            }
            
            echo json_encode($data);
        }
        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1134");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1134_ConsInte__b, G1134_FechaInsercion , G1134_Usuario ,  G1134_CodigoMiembro  , G1134_PoblacionOrigen , G1134_EstadoDiligenciamiento ,  G1134_IdLlamada , G1134_C17457 as principal ,G1134_C20670,G1134_C17457,G1134_C17458,G1134_C20333,G1134_C20671,G1134_C20672,G1134_C17459,G1134_C17460,G1134_C17461,G1134_C17462,G1134_C17463,G1134_C18912,G1134_C17464,G1134_C17465,G1134_C17466,G1134_C17467,G1134_C17468, a.LISOPC_Nombre____b as G1134_C18913,G1134_C18914,G1134_C18915,G1134_C22657, b.LISOPC_Nombre____b as G1134_C17469, c.LISOPC_Nombre____b as G1134_C17470,G1134_C17471,G1134_C17472,G1134_C17473,G1134_C17474,G1134_C17475,G1134_C17476,G1134_C17477,G1134_C17478,G1134_C17479,G1134_C21449,G1134_C21450, d.LISOPC_Nombre____b as G1134_C18344, e.LISOPC_Nombre____b as G1134_C17481, f.LISOPC_Nombre____b as G1134_C18345,G1134_C18346, g.LISOPC_Nombre____b as G1134_C18347,G1134_C18348,G1134_C18349,G1134_C18350,G1134_C17482,G1134_C20956,G1134_C20957,G1134_C20958,G1134_C20959, h.LISOPC_Nombre____b as G1134_C18916, i.LISOPC_Nombre____b as G1134_C18917,G1134_C18919,G1134_C18920,G1134_C18921 FROM '.$BaseDatos.'.G1134 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1134_C18913 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1134_C17469 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1134_C17470 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1134_C18344 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1134_C17481 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1134_C18345 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G1134_C18347 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G1134_C18916 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G1134_C18917 LEFT JOIN '.$BaseDatos.'.G1188 ON G1188_ConsInte__b  =  G1134_C18918';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1134_C17474)){
                    $hora_a = explode(' ', $fila->G1134_C17474)[1];
                }

                $hora_b = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1134_C20957)){
                    $hora_b = explode(' ', $fila->G1134_C20957)[1];
                }

                $hora_c = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1134_C20959)){
                    $hora_c = explode(' ', $fila->G1134_C20959)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1134_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1134_ConsInte__b , ($fila->G1134_C20670) , ($fila->G1134_C17457) , ($fila->G1134_C17458) , ($fila->G1134_C20333) , ($fila->G1134_C20671) , ($fila->G1134_C20672) , ($fila->G1134_C17459) , ($fila->G1134_C17460) , ($fila->G1134_C17461) , ($fila->G1134_C17462) , ($fila->G1134_C17463) , ($fila->G1134_C18912) , ($fila->G1134_C17464) , ($fila->G1134_C17465) , ($fila->G1134_C17466) , ($fila->G1134_C17467) , ($fila->G1134_C17468) , ($fila->G1134_C18913) , explode(' ', $fila->G1134_C18914)[0] , explode(' ', $fila->G1134_C18915)[0] , ($fila->G1134_C22657) , ($fila->G1134_C17469) , ($fila->G1134_C17470) , ($fila->G1134_C17471) , ($fila->G1134_C17472) , explode(' ', $fila->G1134_C17473)[0] , $hora_a , ($fila->G1134_C17475) , ($fila->G1134_C17476) , ($fila->G1134_C17477) , ($fila->G1134_C17478) , ($fila->G1134_C17479) , ($fila->G1134_C21449) , ($fila->G1134_C21450) , ($fila->G1134_C18344) , ($fila->G1134_C17481) , ($fila->G1134_C18345) , ($fila->G1134_C18346) , ($fila->G1134_C18347) , ($fila->G1134_C18348) , explode(' ', $fila->G1134_C18349)[0] , ($fila->G1134_C18350) , ($fila->G1134_C17482) , explode(' ', $fila->G1134_C20956)[0] , $hora_b , explode(' ', $fila->G1134_C20958)[0] , $hora_c , ($fila->G1134_C18916) , ($fila->G1134_C18917) , ($fila->G1134_C18919) , ($fila->G1134_C18920) , ($fila->G1134_C18921) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1134 WHERE G1134_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G1134_ConsInte__b as id,  G1134_C17457 as camp1 , G1134_C17459 as camp2, G1134_C17470 as estado  FROM '.$BaseDatos.'.G1134 ORDER BY field(G1134_C17470,14264,14265,14266,14267,14268) LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);


            while($obj = $result->fetch_object()){
                
                $color = '';
                $strIconoBackOffice = '';
                if($obj->estado == '14266' || $obj->estado == '14265'){
                    $color = 'color:blue;';
                    $strIconoBackOffice = 'En gestión';
                }else if($obj->estado == '14267'){
                    $color = 'color:green;';
                    $strIconoBackOffice = 'Cerrada';
                }else if($obj->estado == '14268'){
                    $color = 'color:red;';
                    $strIconoBackOffice = 'Devuelta';
                }
                else if($obj->estado == '14264'){
                    $color = 'color:orange;';
                    $strIconoBackOffice = 'Sin gestión';
                }
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."<span style='position: relative;right: 2px;float: right;font-size:10px;".$color."'>".$strIconoBackOffice."</i></p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1134 SET "; 
  
            if(isset($_POST["G1134_C20670"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C20670 = '".$_POST["G1134_C20670"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C17457"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17457 = '".$_POST["G1134_C17457"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C17458"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17458 = '".$_POST["G1134_C17458"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C20333"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C20333 = '".$_POST["G1134_C20333"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C20671"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C20671 = '".$_POST["G1134_C20671"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C20672"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C20672 = '".$_POST["G1134_C20672"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C17459"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17459 = '".$_POST["G1134_C17459"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C17460"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17460 = '".$_POST["G1134_C17460"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C17461"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17461 = '".$_POST["G1134_C17461"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C17462"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17462 = '".$_POST["G1134_C17462"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C17463"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17463 = '".$_POST["G1134_C17463"]."'";
                $validar = 1;
            }
             
  
            $G1134_C18912 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1134_C18912"])){
                if($_POST["G1134_C18912"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C18912 = $_POST["G1134_C18912"];
                    $LsqlU .= $separador." G1134_C18912 = ".$G1134_C18912."";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1134_C17464"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17464 = '".$_POST["G1134_C17464"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C17465"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17465 = '".$_POST["G1134_C17465"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C17466"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17466 = '".$_POST["G1134_C17466"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C17467"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17467 = '".$_POST["G1134_C17467"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C17468"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17468 = '".$_POST["G1134_C17468"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C18913"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C18913 = '".$_POST["G1134_C18913"]."'";
                $validar = 1;
            }
             
 
            $G1134_C18914 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1134_C18914"])){    
                if($_POST["G1134_C18914"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1134_C18914"]);
                    if(count($tieneHora) > 1){
                    	$G1134_C18914 = "'".$_POST["G1134_C18914"]."'";
                    }else{
                    	$G1134_C18914 = "'".str_replace(' ', '',$_POST["G1134_C18914"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1134_C18914 = ".$G1134_C18914;
                    $validar = 1;
                }
            }
 
            $G1134_C18915 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1134_C18915"])){    
                if($_POST["G1134_C18915"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1134_C18915"]);
                    if(count($tieneHora) > 1){
                    	$G1134_C18915 = "'".$_POST["G1134_C18915"]."'";
                    }else{
                    	$G1134_C18915 = "'".str_replace(' ', '',$_POST["G1134_C18915"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1134_C18915 = ".$G1134_C18915;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1134_C22657"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C22657 = '".$_POST["G1134_C22657"]."'";
                $validar = 1;
            }
             
 
            $G1134_C17469 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1134_C17469 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1134_C17469 = ".$G1134_C17469;
                    $validar = 1;

                    
                }
            }
 
            $G1134_C17470 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1134_C17470 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1134_C17470 = ".$G1134_C17470;
                    $validar = 1;
                }
            }
  
            $G1134_C17471 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1134_C17471"])){
                if($_POST["G1134_C17471"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C17471 = $_POST["G1134_C17471"];
                    $LsqlU .= $separador." G1134_C17471 = ".$G1134_C17471."";
                    $validar = 1;
                }
            }
  
            $G1134_C17472 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1134_C17472"])){
                if($_POST["G1134_C17472"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C17472 = $_POST["G1134_C17472"];
                    $LsqlU .= $separador." G1134_C17472 = ".$G1134_C17472."";
                    $validar = 1;
                }
            }
 
            $G1134_C17475 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1134_C17475 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1134_C17475 = ".$G1134_C17475;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1134_C17476"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17476 = '".$_POST["G1134_C17476"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C17477"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17477 = '".$_POST["G1134_C17477"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C17478"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17478 = '".$_POST["G1134_C17478"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C17479"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17479 = '".$_POST["G1134_C17479"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C21449"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C21449 = '".$_POST["G1134_C21449"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C21450"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C21450 = '".$_POST["G1134_C21450"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C18344"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C18344 = '".$_POST["G1134_C18344"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C17481"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17481 = '".$_POST["G1134_C17481"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C18345"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C18345 = '".$_POST["G1134_C18345"]."'";
                $validar = 1;
            }
             
  
            $G1134_C18346 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1134_C18346"])){
                if($_POST["G1134_C18346"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C18346 = $_POST["G1134_C18346"];
                    $LsqlU .= $separador." G1134_C18346 = ".$G1134_C18346."";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1134_C18347"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C18347 = '".$_POST["G1134_C18347"]."'";
                $validar = 1;
            }
             
  
            $G1134_C18348 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1134_C18348"])){
                if($_POST["G1134_C18348"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C18348 = $_POST["G1134_C18348"];
                    $LsqlU .= $separador." G1134_C18348 = ".$G1134_C18348."";
                    $validar = 1;
                }
            }
 
            $G1134_C18349 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1134_C18349"])){    
                if($_POST["G1134_C18349"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1134_C18349"]);
                    if(count($tieneHora) > 1){
                    	$G1134_C18349 = "'".$_POST["G1134_C18349"]."'";
                    }else{
                    	$G1134_C18349 = "'".str_replace(' ', '',$_POST["G1134_C18349"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1134_C18349 = ".$G1134_C18349;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1134_C18350"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C18350 = '".$_POST["G1134_C18350"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C17482"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C17482 = '".$_POST["G1134_C17482"]."'";
                $validar = 1;
            }
             
 
            $G1134_C20956 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1134_C20956"])){    
                if($_POST["G1134_C20956"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1134_C20956"]);
                    if(count($tieneHora) > 1){
                    	$G1134_C20956 = "'".$_POST["G1134_C20956"]."'";
                    }else{
                    	$G1134_C20956 = "'".str_replace(' ', '',$_POST["G1134_C20956"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1134_C20956 = ".$G1134_C20956;
                    $validar = 1;
                }
            }
  
            // $G1134_C20957 = NULL;
            // //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            // if(isset($_POST["G1134_C20957"])){   
            //     if($_POST["G1134_C20957"] != '' && $_POST["G1134_C20957"] != 'undefined' && $_POST["G1134_C20957"] != 'null'){
            //         $separador = "";
            //         $fecha = date('Y-m-d');
            //         if($validar == 1){
            //             $separador = ",";
            //         }

            //         $G1134_C20957 = "'".$fecha." ".str_replace(' ', '',$_POST["G1134_C20957"])."'";
            //         $LsqlU .= $separador." G1134_C20957 = ".$G1134_C20957."";
            //         $validar = 1;
            //     }
            // }
 
            // $G1134_C20958 = NULL;
            // //este es de tipo date hay que preguntar si esta vacia o no
            // if(isset($_POST["G1134_C20958"])){    
            //     if($_POST["G1134_C20958"] != ''){
            //         $separador = "";
            //         if($validar == 1){
            //             $separador = ",";
            //         }

            //         $tieneHora = explode(' ' , $_POST["G1134_C20958"]);
            //         if(count($tieneHora) > 1){
            //         	$G1134_C20958 = "'".$_POST["G1134_C20958"]."'";
            //         }else{
            //         	$G1134_C20958 = "'".str_replace(' ', '',$_POST["G1134_C20958"])." 00:00:00'";
            //         }


            //         $LsqlU .= $separador." G1134_C20958 = ".$G1134_C20958;
            //         $validar = 1;
            //     }
            // }
  
            // $G1134_C20959 = NULL;
            // //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            // if(isset($_POST["G1134_C20959"])){   
            //     if($_POST["G1134_C20959"] != '' && $_POST["G1134_C20959"] != 'undefined' && $_POST["G1134_C20959"] != 'null'){
            //         $separador = "";
            //         $fecha = date('Y-m-d');
            //         if($validar == 1){
            //             $separador = ",";
            //         }

            //         $G1134_C20959 = "'".$fecha." ".str_replace(' ', '',$_POST["G1134_C20959"])."'";
            //         $LsqlU .= $separador." G1134_C20959 = ".$G1134_C20959."";
            //         $validar = 1;
            //     }
            // }
  
            if(isset($_POST["G1134_C18916"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C18916 = '".$_POST["G1134_C18916"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C18917"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C18917 = '".$_POST["G1134_C18917"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1134_C18918"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C18918 = '".$_POST["G1134_C18918"]."'";
                $validar = 1;
            }
             
  
            $G1134_C18919 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1134_C18919"])){
                if($_POST["G1134_C18919"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C18919 = $_POST["G1134_C18919"];
                    $LsqlU .= $separador." G1134_C18919 = ".$G1134_C18919."";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1134_C18920"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_C18920 = '".$_POST["G1134_C18920"]."'";
                $validar = 1;
            }
             
  
            $G1134_C18921 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1134_C18921"])){
                if($_POST["G1134_C18921"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1134_C18921 = $_POST["G1134_C18921"];
                    $LsqlU .= $separador." G1134_C18921 = ".$G1134_C18921."";
                    $validar = 1;
                }
            }

            

			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1134_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $validar = 1;
			}
             if(isset($_GET['ValAgen'])){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }                
                        $LsqlU .= $separador."G1134_Usuario = '".$_GET['ValAgen']."'";
                        $validar = 1;
                    }

			if(isset($_POST['oper'])){
                $Lsql = $LsqlU." WHERE G1134_ConsInte__b =".$_POST["id"]; 
            }

            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {

                	if(isset($_POST["campana_crm"]) && $_POST["campana_crm"] != 0){
                		/*Ahor ainsertamos en CONDIA BACKOFICE*/
	                    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b , CAMPAN_ActPobGui_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST["campana_crm"];
	                    //echo $Lsql_Campan;
	                    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	                    $datoCampan = $res_Lsql_Campan->fetch_array();
	                    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	                    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	                    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	                    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
	                    $intActualiza_oNo = $datoCampan['CAMPAN_ActPobGui_b']; 

	                    /* PARA SABER SI ACTUALIZAMOS O NO*/
	                    if($intActualiza_oNo == '-1'){
	                        /* toca hacer actualizacion desde Script */
	                        
	                        $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_POST["campana_crm"];
	                        $resultcampSql = $mysqli->query($campSql);
	                        $Lsql = 'UPDATE '.$BaseDatos.'.'.$str_Pobla_Campan.' , '.$BaseDatos.'.G'.$int_Guion_Campan.' SET ';
	                        $i=0;
	                        while($key = $resultcampSql->fetch_object()){
	                            $validoparaedicion = false;
	                            $valorScript = $key->CAMINC_NomCamGui_b;

	                            $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE Field = '".$key->CAMINC_NomCamPob_b."'";

	                            $resultShow = $mysqli->query($LsqlShow);
	                            if($resultShow->num_rows === 0){
	                                //comentario el campo no existe
	                                $validoparaedicion = false;
	                            }else{
	                                $validoparaedicion = true;
	                            } 

	                            $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE Field = '".$key->CAMINC_NomCamGui_b."'";
	                            //echo $LsqlShow;
	                            $resultShow = $mysqli->query($LsqlShow);
	                            if($resultShow->num_rows === 0 ){
	                                //comentario el campo no existe
	                                $validoparaedicion = false;
	                            }else{
	                                $validoparaedicion = true;
	                            } 

	                            $LsqlPAsaNull = "SELECT ".$key->CAMINC_NomCamGui_b." as Campo_valido FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE  G".$int_Guion_Campan.'_ConsInte__b = '.$_POST["id"];
	                            $LsqlRes = $mysqli->query($LsqlPAsaNull);
	                            if($LsqlRes){
	                                $sata = $LsqlRes->fetch_array();
	                                if($sata['Campo_valido'] != '' && $sata['Campo_valido'] != null){

	                                }else{
	                                    $valorScript = 'NULL';
	                                }
	                            }

	                            if($validoparaedicion){
	                                if($i == 0){
	                                    $Lsql .= $key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                                }else{
	                                    $Lsql .= " , ".$key->CAMINC_NomCamPob_b . ' = '.$valorScript;
	                                }
	                                $i++;    
	                            }
	                            
	                        } 
	                        $Lsql .= ' WHERE  G'.$int_Guion_Campan.'_ConsInte__b = '.$_POST["id"].' AND G'.$int_Guion_Campan.'_CodigoMiembro = '.$str_Pobla_Campan.'_ConsInte__b'; 
	                        //echo "Esta ".$Lsql;
	                        if($mysqli->query($Lsql) === TRUE ){

	                        }else{
	                            echo "NO SE ACTALIZO LA BASE DE DATOS ".$mysqli->error;
	                        }
	                    }

	                    /* AHora toca insertar en Condia */
	                    $fecha_gestion = date('Y-m-d H:i:s');
	                    $CondiaSql = "INSERT INTO ".$BaseDatos_systema.".CONDIA_BACKOFFICE (
	                        CONDIA_IndiEfec__b, 
	                        CONDIA_TipNo_Efe_b, 
	                        CONDIA_ConsInte__MONOEF_b, 
	                        CONDIA_TiemDura__b, 
	                        CONDIA_Fecha_____b, 
	                        CONDIA_ConsInte__CAMPAN_b, 
	                        CONDIA_ConsInte__USUARI_b, 
	                        CONDIA_ConsInte__GUION__Gui_b, 
	                        CONDIA_ConsInte__GUION__Pob_b, 
	                        CONDIA_ConsInte__MUESTR_b, 
	                        CONDIA_CodiMiem__b, 
	                        CONDIA_Observacio_b) 
	                        VALUES (
	                        '".$_POST['tipificacion']."', 
	                        '".$_POST['reintento']."',
	                        '".$_POST['MonoEf']."',
	                        '".$fecha_gestion."',
	                        '".$fecha_gestion."',
	                        '".$_POST["campana_crm"]."',
	                        '".$_SESSION['IDENTIFICACION']."',
	                        '".$int_Guion_Campan."',
	                        '".$int_Pobla_Camp_2."',
	                        '".$int_Muest_Campan."',
	                        '".$_POST["id"]."',
	                        '".$_POST['textAreaComentarios']."'
	                    )";

	                    echo $CondiaSql;
	                    if($mysqli->query($CondiaSql) === true){

	                    }else{
	                        echo "Error insertando Condia => ".$mysqli->error;
	                    }

	                    include '../funcion_Devolver_tarea.php';
                    	devolverTarea($int_Guion_Campan, $_POST['tipificacion'], $_POST['reintento'], $_POST['id']);
                	}

                	
                	
                    echo $_POST["id"];
                } else {
                	echo '0';
                    //echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }
        }
    }
  

  
?>
