
	<?php date_default_timezone_set('America/Bogota'); ?>
	<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
	    <div class="modal-dialog" style="width:95%;">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
	                <h4 class="modal-title">Edicion</h4>
	            </div>
	            <div class="modal-body">
	                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
	                  
	                </iframe>
	            </div>
	        </div>
	    </div>
	</div>
	<?php
	   //SECCION : Definicion urls
	   $url_crud = "formularios/G1008/G1008_CRUD.php";
	   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

		$PEOBUS_Escritur__b = 1 ;
	    $PEOBUS_Adiciona__b = 1 ;
	    $PEOBUS_Borrar____b = 1 ;

		if(!isset($_GET['view'])){
	        $idUsuario = getIdentificacionUser($token);
	        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
	        $query = $mysqli->query($peobus);
	        $PEOBUS_VeRegPro__b = 0 ;
	        
	        while ($key =  $query->fetch_object()) {
	            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
	            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
	            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
	            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
	        }

	        if($PEOBUS_VeRegPro__b != 0){
	            $Zsql = "SELECT G1008_ConsInte__b as id, G1008_C14041 as camp2 , G1008_C14042 as camp1 FROM ".$BaseDatos.".G1008  WHERE G1008_Usuario = ".$idUsuario." ORDER BY G1008_ConsInte__b DESC LIMIT 0, 50";
	        }else{
	            $Zsql = "SELECT G1008_ConsInte__b as id, G1008_C14041 as camp2 , G1008_C14042 as camp1 FROM ".$BaseDatos.".G1008  ORDER BY G1008_ConsInte__b DESC LIMIT 0, 50";
	        }
	    }else{
	        $Zsql = "SELECT G1008_ConsInte__b as id, G1008_C14041 as camp2 , G1008_C14042 as camp1 FROM ".$BaseDatos.".G1008  ORDER BY G1008_ConsInte__b DESC LIMIT 0, 50";
	    }

	   $result = $mysqli->query($Zsql);

	?>
	<?php include(__DIR__ ."/../cabecera.php");?>

	<?php
	if(isset($_GET['user'])){


		$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
	    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	    $datoCampan = $res_Lsql_Campan->fetch_array();
	    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
	    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


	    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
	    $resLsql = $mysqli->query($getPrincipales);
	    //echo $getPrincipales;
	    $dato = $resLsql->fetch_array();

		$XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
		$nombre = $mysqli->query($XLsql);
		$nombreUsuario = NULL;
		//echo $XLsql;
		while ($key = $nombre->fetch_object()) {
		 	echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
		 	$nombreUsuario = $key->nombre;
		 	break;
		} 


		if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


						
			$data = array(	"strToken_t" => $_GET['token'], 
							"strIdGestion_t" => $_GET['id_gestion_cbx'],
							"strDatoPrincipal_t" => $nombreUsuario,
							"strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
			$data_string = json_encode($data);    

			$ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
			//especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
	        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
	        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			    'Content-Type: application/json',                                                                                
			    'Content-Length: ' . strlen($data_string))                                                                      
			); 
			//recogemos la respuesta
			$respuesta = curl_exec ($ch);
			//o el error, por si falla
			$error = curl_error($ch);
			//y finalmente cerramos curl
			//echo "Respuesta =>  ". $respuesta;
			//echo "<br/>Error => ".$error;
			//include "Log.class.php";
			//$log = new Log("log", "./Log/");
			//$log->insert($error, $respuesta, false, true, false);
			//echo "nada";
			curl_close ($ch);
		}
	}else{
		echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";	
	}
	?>

	<?php if(isset($_GET['user'])){ ?>
	<div class="row">
		<div class="col-md-12 col-xs-12">
			<div class="box">
				<div class="box-body">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
	                            <th colspan="4">
	                                Historico de gestiones
	                            </th>
	                        </tr>
							<tr>
								<th>Gesti&oacute;n</th>
								<th>Comentarios</th>
								<th>Fecha - hora</th>
								<th>Agente</th>
							</tr>
						</thead>
						<tbody>
							<?php

								$Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


								$res = $mysqli->query($Lsql);
								while($key = $res->fetch_object()){
									echo "<tr>";
									echo "<td>".($key->MONOEF_Texto_____b)."</td>";
									echo "<td>".$key->CONDIA_Observacio_b."</td>";
									echo "<td>".$key->CONDIA_Fecha_____b."</td>";
									echo "<td>".$key->USUARI_Nombre____b."</td>";
									echo "</tr>";
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>

	<div class="panel box box-primary" >
	    <div class="box-header with-border">
	        <h4 class="box-title">
	            <a data-toggle="collapse" data-parent="#accordion" href="#s_1645">
	                CONVERSACION
	            </a>
	        </h4>
	    </div>
	    <div id="s_1645" class="panel-collapse collapse in">
	        <div class="box-body">

	        <div class="row">
	        

	            <div class="col-md-6 col-xs-6">

	  
				        <!-- lIBRETO O LABEL -->
				        <p style="text-align:justify;">Buenas tardes Sr XXXX habla con XXXXX asesor comercial de fincaraiz.com.co el portal digital.</p>
				        <!-- FIN LIBRETO -->
	  
	            </div>


	            <div class="col-md-6 col-xs-6">

	  
				        <!-- lIBRETO O LABEL -->
				        <p style="text-align:justify;">El motivo de la llamada es para ayudarlo mejorar la publicación de su inmueble y sabemos que se interesó en… </p>
				        <!-- FIN LIBRETO -->
	  
	            </div>

	  
	        </div>


	        <div class="row">
	        

	            <div class="col-md-6 col-xs-6">

	  
				        <!-- lIBRETO O LABEL -->
				        <p style="text-align:justify;">Logró realizar la compra de este producto?</p>
				        <!-- FIN LIBRETO -->
	  
	            </div>


	            <div class="col-md-6 col-xs-6">

	  
				        <!-- lIBRETO O LABEL -->
				        <p style="text-align:justify;">Puedo acompañarlo a realizar su pago. Recuerde que nuestros medios de pago son: Pin por Baloto o Efecty, tarjeta débito, tarjeta crédito o consignación. Cual le queda más fácil?</p>
				        <!-- FIN LIBRETO -->
	  
	            </div>

	  
	        </div>


	        <div class="row">
	        

	            <div class="col-md-6 col-xs-6">

	  
				        <!-- lIBRETO O LABEL -->
				        <p style="text-align:justify;">(Importante indagar sobre la razón por la que no pudo concretar la compra)</p>
				        <!-- FIN LIBRETO -->
	  
	            </div>


	        </div>


	        </div>
	    </div>
	</div>

	<div id="1641" >


	        <div class="row">
	        

	            <div class="col-md-6 col-xs-6">

	  
				        <!-- CAMPO TIPO FECHA -->
				        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
				        <div class="form-group">
				            <label for="G1008_C14035" id="LblG1008_C14035">FECHA</label>
				            <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G1008_C14035'])) {
		                		echo $_GET['G1008_C14035'];
		                	} ?>"  name="G1008_C14035" id="G1008_C14035" placeholder="YYYY-MM-DD">
				        </div>
				        <!-- FIN DEL CAMPO TIPO FECHA-->
	  
	            </div>


	            <div class="col-md-6 col-xs-6">

	 
				        <!-- CAMPO TIPO TEXTO -->
				        <div class="form-group">
				            <label for="G1008_C14036" id="LblG1008_C14036">CODIGO AVISO</label>
				            <input type="text" class="form-control input-sm" id="G1008_C14036" value="<?php if (isset($_GET['G1008_C14036'])) {
		                		echo $_GET['G1008_C14036'];
		                	} ?>"  name="G1008_C14036"  placeholder="CODIGO AVISO">
				        </div>
				        <!-- FIN DEL CAMPO TIPO TEXTO -->
	  
	            </div>

	  
	        </div>


	        <div class="row">
	        

	            <div class="col-md-6 col-xs-6">

	 
				        <!-- CAMPO TIPO TEXTO -->
				        <div class="form-group">
				            <label for="G1008_C14037" id="LblG1008_C14037">CONCEPTO</label>
				            <input type="text" class="form-control input-sm" id="G1008_C14037" value="<?php if (isset($_GET['G1008_C14037'])) {
		                		echo $_GET['G1008_C14037'];
		                	} ?>"  name="G1008_C14037"  placeholder="CONCEPTO">
				        </div>
				        <!-- FIN DEL CAMPO TIPO TEXTO -->
	  
	            </div>


	            <div class="col-md-6 col-xs-6">

	 
				        <!-- CAMPO TIPO TEXTO -->
				        <div class="form-group">
				            <label for="G1008_C14038" id="LblG1008_C14038">TIPO INMUEBLE</label>
				            <input type="text" class="form-control input-sm" id="G1008_C14038" value="<?php if (isset($_GET['G1008_C14038'])) {
		                		echo $_GET['G1008_C14038'];
		                	} ?>"  name="G1008_C14038"  placeholder="TIPO INMUEBLE">
				        </div>
				        <!-- FIN DEL CAMPO TIPO TEXTO -->
	  
	            </div>

	  
	        </div>


	        <div class="row">
	        

	            <div class="col-md-6 col-xs-6">

	 
				        <!-- CAMPO TIPO TEXTO -->
				        <div class="form-group">
				            <label for="G1008_C14039" id="LblG1008_C14039">TIPO OFERTA</label>
				            <input type="text" class="form-control input-sm" id="G1008_C14039" value="<?php if (isset($_GET['G1008_C14039'])) {
		                		echo $_GET['G1008_C14039'];
		                	} ?>"  name="G1008_C14039"  placeholder="TIPO OFERTA">
				        </div>
				        <!-- FIN DEL CAMPO TIPO TEXTO -->
	  
	            </div>


	            <div class="col-md-6 col-xs-6">

	 
				        <!-- CAMPO TIPO TEXTO -->
				        <div class="form-group">
				            <label for="G1008_C14040" id="LblG1008_C14040">CIUDAD</label>
				            <input type="text" class="form-control input-sm" id="G1008_C14040" value="<?php if (isset($_GET['G1008_C14040'])) {
		                		echo $_GET['G1008_C14040'];
		                	} ?>"  name="G1008_C14040"  placeholder="CIUDAD">
				        </div>
				        <!-- FIN DEL CAMPO TIPO TEXTO -->
	  
	            </div>

	  
	        </div>


	        <div class="row">
	        

	            <div class="col-md-6 col-xs-6">

	 
				        <!-- CAMPO TIPO TEXTO -->
				        <div class="form-group">
				            <label for="G1008_C14041" id="LblG1008_C14041">CODIGO CLIENTE</label>
				            <input type="text" class="form-control input-sm" id="G1008_C14041" value="<?php if (isset($_GET['G1008_C14041'])) {
		                		echo $_GET['G1008_C14041'];
		                	} ?>"  name="G1008_C14041"  placeholder="CODIGO CLIENTE">
				        </div>
				        <!-- FIN DEL CAMPO TIPO TEXTO -->
	  
	            </div>


	            <div class="col-md-6 col-xs-6">

	 
				        <!-- CAMPO TIPO TEXTO -->
				        <div class="form-group">
				            <label for="G1008_C14042" id="LblG1008_C14042">CLIENTE</label>
				            <input type="text" class="form-control input-sm" id="G1008_C14042" value="<?php if (isset($_GET['G1008_C14042'])) {
		                		echo $_GET['G1008_C14042'];
		                	} ?>"  name="G1008_C14042"  placeholder="CLIENTE">
				        </div>
				        <!-- FIN DEL CAMPO TIPO TEXTO -->
	  
	            </div>

	  
	        </div>


	        <div class="row">
	        

	            <div class="col-md-6 col-xs-6">

	 
				        <!-- CAMPO TIPO TEXTO -->
				        <div class="form-group">
				            <label for="G1008_C14043" id="LblG1008_C14043">TELEFONO 1</label>
				            <input type="text" class="form-control input-sm" id="G1008_C14043" value="<?php if (isset($_GET['G1008_C14043'])) {
		                		echo $_GET['G1008_C14043'];
		                	} ?>"  name="G1008_C14043"  placeholder="TELEFONO 1">
				        </div>
				        <!-- FIN DEL CAMPO TIPO TEXTO -->
	  
	            </div>


	            <div class="col-md-6 col-xs-6">

	 
				        <!-- CAMPO TIPO TEXTO -->
				        <div class="form-group">
				            <label for="G1008_C14044" id="LblG1008_C14044">TELEFONO 2</label>
				            <input type="text" class="form-control input-sm" id="G1008_C14044" value="<?php if (isset($_GET['G1008_C14044'])) {
		                		echo $_GET['G1008_C14044'];
		                	} ?>"  name="G1008_C14044"  placeholder="TELEFONO 2">
				        </div>
				        <!-- FIN DEL CAMPO TIPO TEXTO -->
	  
	            </div>

	  
	        </div>


	        <div class="row">
	        

	            <div class="col-md-6 col-xs-6">

	 
				        <!-- CAMPO TIPO TEXTO -->
				        <div class="form-group">
				            <label for="G1008_C14045" id="LblG1008_C14045">EMAIL</label>
				            <input type="text" class="form-control input-sm" id="G1008_C14045" value="<?php if (isset($_GET['G1008_C14045'])) {
		                		echo $_GET['G1008_C14045'];
		                	} ?>"  name="G1008_C14045"  placeholder="EMAIL">
				        </div>
				        <!-- FIN DEL CAMPO TIPO TEXTO -->
	  
	            </div>


	            <div class="col-md-6 col-xs-6">

	 
				        <!-- CAMPO TIPO TEXTO -->
				        <div class="form-group">
				            <label for="G1008_C14046" id="LblG1008_C14046">DEPARTAMENTO</label>
				            <input type="text" class="form-control input-sm" id="G1008_C14046" value="<?php if (isset($_GET['G1008_C14046'])) {
		                		echo $_GET['G1008_C14046'];
		                	} ?>"  name="G1008_C14046"  placeholder="DEPARTAMENTO">
				        </div>
				        <!-- FIN DEL CAMPO TIPO TEXTO -->
	  
	            </div>

	  
	        </div>


	        <div class="row">
	        

	            <div class="col-md-6 col-xs-6">

	  
				        <!-- CAMPO TIPO MEMO -->
				        <div class="form-group">
				            <label for="G1008_C14047" id="LblG1008_C14047">OBSERVACIONES CONTACTO EFECTIVO</label>
				            <textarea class="form-control input-sm" name="G1008_C14047" id="G1008_C14047"  value="<?php if (isset($_GET['G1008_C14047'])) {
		                		echo $_GET['G1008_C14047'];
		                	} ?>" placeholder="OBSERVACIONES CONTACTO EFECTIVO"></textarea>
				        </div>
				        <!-- FIN DEL CAMPO TIPO MEMO -->
	  
	            </div>


	        </div>


	</div>

	<div id="1643" style='display:none;'>


	        <div class="row">
	        

	            <div class="col-md-12 col-xs-12">

	 
				        <!-- CAMPO TIPO TEXTO -->
				        <div class="form-group">
				            <label for="G1008_C14053" id="LblG1008_C14053">Agente</label>
				            <input type="text" class="form-control input-sm" id="G1008_C14053" value="<?php echo getNombreUser($token);?>" readonly name="G1008_C14053"  placeholder="Agente">
				        </div>
				        <!-- FIN DEL CAMPO TIPO TEXTO -->
	  
	            </div>

	  
	        </div>


	        <div class="row">
	        

	            <div class="col-md-12 col-xs-12">

	 
				        <!-- CAMPO TIPO TEXTO -->
				        <div class="form-group">
				            <label for="G1008_C14054" id="LblG1008_C14054">Fecha</label>
				            <input type="text" class="form-control input-sm" id="G1008_C14054" value="<?php echo date('Y-m-d');?>" readonly name="G1008_C14054"  placeholder="Fecha">
				        </div>
				        <!-- FIN DEL CAMPO TIPO TEXTO -->
	  
	            </div>

	  
	        </div>


	        <div class="row">
	        

	            <div class="col-md-12 col-xs-12">

	 
				        <!-- CAMPO TIPO TEXTO -->
				        <div class="form-group">
				            <label for="G1008_C14055" id="LblG1008_C14055">Hora</label>
				            <input type="text" class="form-control input-sm" id="G1008_C14055" value="<?php echo date('H:i:s');?>" readonly name="G1008_C14055"  placeholder="Hora">
				        </div>
				        <!-- FIN DEL CAMPO TIPO TEXTO -->
	  
	            </div>

	  
	        </div>


	        <div class="row">
	        

	            <div class="col-md-12 col-xs-12">

	 
				        <!-- CAMPO TIPO TEXTO -->
				        <div class="form-group">
				            <label for="G1008_C14056" id="LblG1008_C14056">Campaña</label>
				            <input type="text" class="form-control input-sm" id="G1008_C14056" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
	                $resCampa = $mysqli->query($cmapa);
	                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÑA";}?>" readonly name="G1008_C14056"  placeholder="Campaña">
				        </div>
				        <!-- FIN DEL CAMPO TIPO TEXTO -->
	  
	            </div>

	  
	        </div>


	</div>

	<div id="1642" >


	</div>

	<div class="panel box box-primary" >
	    <div class="box-header with-border">
	        <h4 class="box-title">
	            <a data-toggle="collapse" data-parent="#accordion" href="#s_1917">
	                TIPIFICACIONES AT
	            </a>
	        </h4>
	    </div>
	    <div id="s_1917" class="panel-collapse collapse in">
	        <div class="box-body">

	        <div class="row">
	        

	            <div class="col-md-12 col-xs-12">


				        <!-- CAMPO DE TIPO LISTA -->
				        <div class="form-group">
				            <label for="G1008_C15543" id="LblG1008_C15543">GESTION</label>
				            <select class="form-control input-sm select2"  style="width: 100%;" name="G1008_C15543" id="G1008_C15543">
				                <option value="0">Seleccione</option>
				                <?php
				                    /*
				                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
				                    */
				                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 747 ORDER BY LISOPC_Nombre____b ASC";

				                    $obj = $mysqli->query($Lsql);
				                    while($obje = $obj->fetch_object()){
				                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

				                    }    
				                    
				                ?>
				            </select>
				        </div>
				        <!-- FIN DEL CAMPO TIPO LISTA -->
	  
	            </div>

	  
	        </div>


	        <div class="row">
	        

	            <div class="col-md-12 col-xs-12">


				        <!-- CAMPO DE TIPO LISTA -->
				        <div class="form-group">
				            <label for="G1008_C15544" id="LblG1008_C15544">TIPIFICACION 1</label>
				            <select class="form-control input-sm select2"  style="width: 100%;" name="G1008_C15544" id="G1008_C15544">
				                <option value="0">Seleccione</option>
				                <?php
				                    /*
				                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
				                    */
				                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 748 ORDER BY LISOPC_Nombre____b ASC";

				                    $obj = $mysqli->query($Lsql);
				                    while($obje = $obj->fetch_object()){
				                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

				                    }    
				                    
				                ?>
				            </select>
				        </div>
				        <!-- FIN DEL CAMPO TIPO LISTA -->
	  
	            </div>

	  
	        </div>


	        <div class="row">
	        

	            <div class="col-md-12 col-xs-12">


				        <!-- CAMPO DE TIPO LISTA -->
				        <div class="form-group">
				            <label for="G1008_C15545" id="LblG1008_C15545">TIPIFICACION 2</label>
				            <select class="form-control input-sm select2"  style="width: 100%;" name="G1008_C15545" id="G1008_C15545">
				                <option value="0">Seleccione</option>
				                <?php
				                    /*
				                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
				                    */
				                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 743 ORDER BY LISOPC_Nombre____b ASC";

				                    $obj = $mysqli->query($Lsql);
				                    while($obje = $obj->fetch_object()){
				                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

				                    }    
				                    
				                ?>
				            </select>
				        </div>
				        <!-- FIN DEL CAMPO TIPO LISTA -->
	  
	            </div>

	  
	        </div>


	        </div>
	    </div>
	</div>

	<div class="panel box box-primary" >
	    <div class="box-header with-border">
	        <h4 class="box-title">
	            <a data-toggle="collapse" data-parent="#accordion" href="#s_2056">
	                GESTIÓN EN OTRAS BASES DE DATOS
	            </a>
	        </h4>
	    </div>
	    <div id="s_2056" class="panel-collapse collapse in">
	        <div class="box-body">

	        <div class="row">
	        

	            <div class="col-md-6 col-xs-6">

	  
	            </div>


	        </div>


	        </div>
	    </div>
	</div>

	<!-- SI ES MAESTRO - DETALLE CREO LAS TABS --> 

	<hr/>
	<div class="nav-tabs-custom">

	    <ul class="nav nav-tabs">

	        <li class="active">
	            <a href="#tab_0" data-toggle="tab" id="tabs_click_0">GESTIÓN</a>
	        </li>

	    </ul>


	    <div class="tab-content">

	        <div class="tab-pane active" id="tab_0"> 
	            <table class="table table-hover table-bordered" id="tablaDatosDetalless0" width="100%">
	            </table>
	            <div id="pagerDetalles0">
	            </div> 
	            <button title="Crear GESTIÓN" class="btn btn-primary btn-sm llamadores" padre="'<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' " id="btnLlamar_0"><i class="fa fa-plus"></i></button>
	        </div>

	    </div>

	</div>

	<div class="row" style="background-color: #FAFAFA; ">
		<br/>
	    <?php if(isset($_GET['user'])){ ?>
	    <div class="col-md-10 col-xs-9">
	        <div class="form-group">
	            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1008_C14048">
	                <option value="0">Tipificaci&oacute;n</option>
	                <?php
	                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
	                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
	                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 669;";
	                $obj = $mysqli->query($Lsql);
	                while($obje = $obj->fetch_object()){
	                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

	                }          
	                ?>
	            </select>
	            
	            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
	            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
	            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
	            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
	            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
	            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
	            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
	        </div>
	    </div>
	   	<div class="col-md-2 col-xs-3" style="text-align: center;">
	        <button class="btn btn-primary btn-block" id="Save" type="button">
	            Cerrar Gesti&oacute;n
	        </button>
	        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
	            <u>Cambiar registro</u>
	        </a>
	    </div>
	    <?php }else{ ?>
	    <div class="col-md-12 col-xs-12">
	        <div class="form-group">
	            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1008_C14048">
	                <option value="0">Tipificaci&oacute;n</option>
	                <?php
	                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
	                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
	                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 669;";
	                $obj = $mysqli->query($Lsql);
	                while($obje = $obj->fetch_object()){
	                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

	                }            
	                ?>
	            </select>
	            
	            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
	            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
	            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
	            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
	            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
	            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
	            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
	        </div>
	    </div>
	    <?php } ?>
	</div>
	<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
	    <div class="col-md-4 col-xs-4">
	        <div class="form-group">
	            <select class="form-control input-sm reintento" name="reintento" id="G1008_C14049">
	                <option value="0">Reintento</option>
	                <option value="1">REINTENTO AUTOMATICO</option>
	                <option value="2">AGENDADO</option>
	                <option value="3">NO REINTENTAR</option>
	            </select>     
	        </div>
	    </div>
	    <div class="col-md-4 col-xs-4">
	        <div class="form-group">
	            <input type="text" name="TxtFechaReintento" id="G1008_C14050" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
	        </div>
	    </div>
	    <div class="col-md-4 col-xs-4" style="text-align: left;">
	        <div class="form-group">
	            <input type="text" name="TxtHoraReintento" id="G1008_C14051" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
	        </div>
	    </div>
	</div>
	<div class="row" style="background-color: #FAFAFA;">
	    <div class="col-md-12 col-xs-12">
	        <div class="form-group">
	            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G1008_C14052" placeholder="Observaciones"></textarea>
	        </div>
	    </div>
	</div>
	<!-- SECCION : PAGINAS INCLUIDAS -->
	<?php include(__DIR__ ."/../pies.php");?>
	<script type="text/javascript" src="formularios/G1008/G1008_eventos.js"></script> 
	<script type="text/javascript">
	    $(function(){
		//JDBD - Esta seccion es solo para la interaccion con el formulario Padre
		/////////////////////////////////////////////////////////////////////////
		<?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
			<?php if($_GET["yourfather"] != "NULL"){ ?>
				$("#G1008_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
			<?php }else{ ?>
				if(document.getElementById("G1008_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
					$.ajax({
						url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
						type     : 'POST',
						data     : { q : <?php echo $_GET["idFather"]; ?> },
						success  : function(data){
							$("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
						}
					});
				}else{
					$("#G1008_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
				}
			<?php } ?>
		<?php } ?>
		/////////////////////////////////////////////////////////////////////////
		<?php if (!isset($_GET["view"])) {?>
			$("#add").click(function(){
								
			});
		<?php } ?>;	
		var meses = new Array(12);
		meses[0] = "01";
		meses[1] = "02";
		meses[2] = "03";
		meses[3] = "04";
		meses[4] = "05";
		meses[5] = "06";
		meses[6] = "07";
		meses[7] = "08";
		meses[8] = "09";
		meses[9] = "10";
		meses[10] = "11";
		meses[11] = "12";

		var d = new Date();
		var h = d.getHours();
		var horas = (h < 10) ? '0' + h : h;
		var dia = d.getDate();
		var dias = (dia < 10) ? '0' + dia : dia;
		var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
		$("#FechaInicio").val(fechaInicial);
	            

		//Esta es por si lo llaman en modo formulario de edicion LigthBox
		<?php if(isset($_GET['registroId'])){ ?>
		$.ajax({
			url      : '<?=$url_crud;?>',
			type     : 'POST',
			data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
			dataType : 'json',
			success  : function(data){
				//recorrer datos y enviarlos al formulario
				$.each(data, function(i, item) {
	                    

	            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
	             
					$("#G1008_C14035").val(item.G1008_C14035); 
					$("#G1008_C14036").val(item.G1008_C14036); 
					$("#G1008_C14037").val(item.G1008_C14037); 
					$("#G1008_C14038").val(item.G1008_C14038); 
					$("#G1008_C14039").val(item.G1008_C14039); 
					$("#G1008_C14040").val(item.G1008_C14040); 
					$("#G1008_C14041").val(item.G1008_C14041); 
					$("#G1008_C14042").val(item.G1008_C14042); 
					$("#G1008_C14043").val(item.G1008_C14043); 
					$("#G1008_C14044").val(item.G1008_C14044); 
					$("#G1008_C14045").val(item.G1008_C14045); 
					$("#G1008_C14046").val(item.G1008_C14046); 
					$("#G1008_C14047").val(item.G1008_C14047); 
					$("#G1008_C14048").val(item.G1008_C14048).trigger("change");  
					$("#G1008_C14049").val(item.G1008_C14049).trigger("change");  
					$("#G1008_C14050").val(item.G1008_C14050); 
					$("#G1008_C14051").val(item.G1008_C14051); 
					$("#G1008_C14052").val(item.G1008_C14052); 
					$("#G1008_C14053").val(item.G1008_C14053); 
					$("#G1008_C14054").val(item.G1008_C14054); 
					$("#G1008_C14055").val(item.G1008_C14055); 
					$("#G1008_C14056").val(item.G1008_C14056);   
					if(item.G1008_C14057 == 1){
						$("#G1008_C14057").attr('checked', true);
					}    
					if(item.G1008_C14058 == 1){
						$("#G1008_C14058").attr('checked', true);
					}    
					if(item.G1008_C14084 == 1){
						$("#G1008_C14084").attr('checked', true);
					}    
					if(item.G1008_C14085 == 1){
						$("#G1008_C14085").attr('checked', true);
					}    
					if(item.G1008_C14086 == 1){
						$("#G1008_C14086").attr('checked', true);
					}  
					$("#G1008_C15543").val(item.G1008_C15543).trigger("change");  
					$("#G1008_C15544").attr("opt",item.G1008_C15544);  
					$("#G1008_C15545").attr("opt",item.G1008_C15545); 
					
					$("#h3mio").html(item.principal);

				});

				//Deshabilitar los campos 3

				//Habilitar todos los campos para edicion
				$('#FormularioDatos :input').each(function(){
					$(this).attr('disabled', true);
				});              

				//Habilidar los botones de operacion, add, editar, eliminar
				$("#add").attr('disabled', false);
				$("#edit").attr('disabled', false);
				$("#delete").attr('disabled', false);

				//Desahabiliatra los botones de salvar y seleccionar_registro
				$("#cancel").attr('disabled', true);
				$("#Save").attr('disabled', true);
			} 
		});

	        $("#hidId").val(<?php echo $_GET['registroId'];?>);
	        idTotal = <?php echo $_GET['registroId'];?>;

	     	$("#TxtFechaReintento").attr('disabled', true);
	        $("#TxtHoraReintento").attr('disabled', true); 
	        $("#btnLlamar_0").attr('padre', <?php echo $_GET['registroId'];?>);

	        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

	        <?php } ?>

	        <?php if(isset($_GET['user'])){ ?>
	        	/*$("#btnLlamar_0").attr('padre', <?php echo $_GET['user'];?>);
	        	vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
	        	idTotal = <?php echo $_GET['user'];?>; */
	        <?php } ?>

	        $("#refrescarGrillas").click(function(){
	            

	            $.jgrid.gridUnload('#tablaDatosDetalless0');
	            
	    	$("#btnLlamar_0").attr('padre', $("#G1008_C14041").val());
	    		var id_0 = $("#G1008_C14041").val();
	    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
	    		cargarHijos_0(id_0);
	        });

	        //Esta es la funcionalidad de los Tabs
	        
	 

	        $("#tabs_click_0").click(function(){ 
	            $.jgrid.gridUnload('#tablaDatosDetalless0'); 
	            $("#btnLlamar_0").attr('padre', $("#G1008_C14041").val());
	    		var id_0 = $("#G1008_C14041").val();
	    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
	    		cargarHijos_0(id_0);
	        });

	        $("#btnLlamar_0").click(function( event ) {
	            event.preventDefault(); 
	            var padre = $("#G1008_C14041").val();
	            


	           
	            if($("#oper").val() == 'add'){
	                if(before_save()){
	                    $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1101&view=si&formaDetalle=si&formularioPadre=1008&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=16723<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
	                    $("#editarDatos").modal('show');
	                }else{
	                    before_save();
	                    var d = new Date();
	                    var h = d.getHours();
	                    var horas = (h < 10) ? '0' + h : h;
	                    var dia = d.getDate();
	                    var dias = (dia < 10) ? '0' + dia : dia;
	                    var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
	                    $("#FechaFinal").val(fechaFinal);
	                    
	                    var valido = 0;
	                    

	    		if($("#G1008_C15543").prop("selectedIndex")==0 || $("#G1008_C15543").prop("selectedIndex") == -1){
	    			alertify.error('GESTION debe ser diligenciado');
	    			$("#G1008_C15543").closest(".form-group").addClass("has-error");
	    			valido = 1;
	    		}

	    		if($("#G1008_C15544").prop("selectedIndex")==0 || $("#G1008_C15544").prop("selectedIndex") == -1){
	    			alertify.error('TIPIFICACION 1 debe ser diligenciado');
	    			$("#G1008_C15544").closest(".form-group").addClass("has-error");
	    			valido = 1;
	    		}
	                    if (validado == '0') {
	                    	var form = $("#FormularioDatos");
		                    //Se crean un array con los datos a enviar, apartir del formulario 
		                    var formData = new FormData($("#FormularioDatos")[0]);
		                    $.ajax({
		                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
		                        type: 'POST',
		                        data: formData,
		                        cache: false,
		                        contentType: false,
		                        processData: false,
		                        //una vez finalizado correctamente
		                        success: function(data){
		                            if(data){
		                                //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
		                                if($("#oper").val() == 'add'){
		                                    idTotal = data;
		                                }else{
		                                    idTotal= $("#hidId").val();
		                                }
		                                $("#hidId").val(idTotal);

		                                int_guardo = 1;
		                                $(".llamadores").attr('padre', idTotal);
		                                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1101&view=si&formaDetalle=si&formularioPadre=1008&idFather='+idTotal+'&yourfather='+ padre +'&pincheCampo=16723&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
		                                $("#editarDatos").modal('show');
		                                $("#oper").val('edit');

		                            }else{
		                                //Algo paso, hay un error
		                                alertify.error('Un error ha ocurrido');
		                            }                
		                        },
		                        //si ha ocurrido un error
		                        error: function(){
		                            after_save_error();
		                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
		                        }
		                    });
	                    }
	                }
	            }else{

	                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1101&view=si&idFather='+idTotal+'&yourfather='+ padre +'&formaDetalle=si&formularioPadre=1008&pincheCampo=16723&action=add<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
	                $("#editarDatos").modal('show');
	            }
	        });
	        //Select2 estos son los guiones
	        


	    $("#G1008_C15543").select2();

	    $("#G1008_C15544").select2();

	    $("#G1008_C15545").select2();
	        //datepickers
	        

	        $("#G1008_C14035").datepicker({
	            language: "es",
	            autoclose: true,
	            todayHighlight: true
	        });

	        $("#G1008_C14050").datepicker({
	            language: "es",
	            autoclose: true,
	            todayHighlight: true
	        });

	        //Timepickers
	        


	        //Timepicker
	        var options = { //hh:mm 24 hour format only, defaults to current time
	            twentyFour: true, //Display 24 hour format, defaults to false
	            title: 'Hora Agenda', //The Wickedpicker's title,
	            showSeconds: true, //Whether or not to show seconds,
	            secondsInterval: 1, //Change interval for seconds, defaults to 1
	            minutesInterval: 1, //Change interval for minutes, defaults to 1
	            beforeShow: null, //A function to be called before the Wickedpicker is shown
	            show: null, //A function to be called when the Wickedpicker is shown
	            clearable: false, //Make the picker's input clearable (has clickable "x")
	        }; 
	        $("#G1008_C14051").wickedpicker(options);

	        //Validaciones numeros Enteros
	        


	        //Validaciones numeros Decimales
	        


	        /* Si son d formulas */
	        


	        //Si tienen dependencias

	        


	    //function para GESTION 

	    $("#G1008_C15543").change(function(){  
	    	//Esto es la parte de las listas dependientes
	    	

			$.ajax({
				url    : '<?php echo $url_crud; ?>',
				type   : 'post',
				data   : { getListaHija : true , opcionID : '748' , idPadre : $(this).val() },
				success : function(data){
					var optG1008_C15544 = $("#G1008_C15544").attr("opt");
					$("#G1008_C15544").html(data);
					if (optG1008_C15544 != null) {
						$("#G1008_C15544").val(optG1008_C15544).trigger("change");
					}
				}
			});
			
	    });

	    //function para TIPIFICACION 1 

	    $("#G1008_C15544").change(function(){  
	    	//Esto es la parte de las listas dependientes
	    	

			$.ajax({
				url    : '<?php echo $url_crud; ?>',
				type   : 'post',
				data   : { getListaHija : true , opcionID : '743' , idPadre : $(this).val() },
				success : function(data){
					var optG1008_C15545 = $("#G1008_C15545").attr("opt");
					$("#G1008_C15545").html(data);
					if (optG1008_C15545 != null) {
						$("#G1008_C15545").val(optG1008_C15545).trigger("change");
					}
				}
			});
			
	    });

	    //function para TIPIFICACION 2 

	    $("#G1008_C15545").change(function(){  
	    	//Esto es la parte de las listas dependientes
	    	

	    });
	        
	        //Funcionalidad del botob guardar
	        


	        $("#Save").click(function(){
	        	
	        	var bol_respuesta = before_save();
	        	var d = new Date();
	            var h = d.getHours();
	            var horas = (h < 10) ? '0' + h : h;
	            var dia = d.getDate();
	            var dias = (dia < 10) ? '0' + dia : dia;
	            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
	            $("#FechaFinal").val(fechaFinal);
	            var valido = 0;
	            

	    		if($("#G1008_C15543").prop("selectedIndex")==0 || $("#G1008_C15543").prop("selectedIndex") == -1){
	    			alertify.error('GESTION debe ser diligenciado');
	    			$("#G1008_C15543").closest(".form-group").addClass("has-error");
	    			valido = 1;
	    		}

	    		if($("#G1008_C15544").prop("selectedIndex")==0 || $("#G1008_C15544").prop("selectedIndex") == -1){
	    			alertify.error('TIPIFICACION 1 debe ser diligenciado');
	    			$("#G1008_C15544").closest(".form-group").addClass("has-error");
	    			valido = 1;
	    		}
	            if($(".tipificacion").val() == '0'){
	            	alertify.error("Es necesaria la tipificación!");
	            	valido = 1;
	            }

	            if($(".reintento").val() == '2'){
	            	if($(".TxtFechaReintento").val().length < 1){
	            		alertify.error("Es necesario llenar la fecha de reintento!");
	            		$(".TxtFechaReintento").focus();
	            		valido = 1;
	            	}

	            	if($(".TxtHoraReintento").val().length < 1){
	            		alertify.error("Es necesario llenar la hora de reintento!");
	            		$(".TxtHoraReintento").focus();
	            		valido = 1;
	            	}
	            }

	            if(valido == '0'){
		        	if(bol_respuesta){            
			            var form = $("#FormularioDatos");
			            //Se crean un array con los datos a enviar, apartir del formulario 
			            var formData = new FormData($("#FormularioDatos")[0]);
			            $.ajax({
			               url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
			                type: 'POST',
			                data: formData,
			                cache: false,
			                contentType: false,
			                processData: false,
			                //una vez finalizado correctamente
			                success: function(data){
			                    if(data != '0'){
			                    	<?php if(!isset($_GET['campan'])){ ?>
				                        //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
				                        if($("#oper").val() == 'add'){
				                            idTotal = data;
				                        }else{
				                            idTotal= $("#hidId").val();
				                        }
				                       
				                        //Limpiar formulario
				                        form[0].reset();
				                        after_save();
				                        <?php if(isset($_GET['registroId'])){ ?>
				                        	var ID = <?=$_GET['registroId'];?>
				                        <?php }else{ ?>	
				                        	var ID = data
				                        <?php } ?>	
				                        $.ajax({
				                            url      : '<?=$url_crud;?>',
				                            type     : 'POST',
				                            data     : { CallDatos : 'SI', id : ID },
				                            dataType : 'json',
				                            success  : function(data){
				                                //recorrer datos y enviarlos al formulario
				                                $.each(data, function(i, item) {
			                                    
	 
			                                    	$("#G1008_C14035").val(item.G1008_C14035);
	 
			                                    	$("#G1008_C14036").val(item.G1008_C14036);
	 
			                                    	$("#G1008_C14037").val(item.G1008_C14037);
	 
			                                    	$("#G1008_C14038").val(item.G1008_C14038);
	 
			                                    	$("#G1008_C14039").val(item.G1008_C14039);
	 
			                                    	$("#G1008_C14040").val(item.G1008_C14040);
	 
			                                    	$("#G1008_C14041").val(item.G1008_C14041);
	 
			                                    	$("#G1008_C14042").val(item.G1008_C14042);
	 
			                                    	$("#G1008_C14043").val(item.G1008_C14043);
	 
			                                    	$("#G1008_C14044").val(item.G1008_C14044);
	 
			                                    	$("#G1008_C14045").val(item.G1008_C14045);
	 
			                                    	$("#G1008_C14046").val(item.G1008_C14046);
	 
			                                    	$("#G1008_C14047").val(item.G1008_C14047);
	 
	                    $("#G1008_C14048").val(item.G1008_C14048).trigger("change"); 
	 
	                    $("#G1008_C14049").val(item.G1008_C14049).trigger("change"); 
	 
			                                    	$("#G1008_C14050").val(item.G1008_C14050);
	 
			                                    	$("#G1008_C14051").val(item.G1008_C14051);
	 
			                                    	$("#G1008_C14052").val(item.G1008_C14052);
	 
			                                    	$("#G1008_C14053").val(item.G1008_C14053);
	 
			                                    	$("#G1008_C14054").val(item.G1008_C14054);
	 
			                                    	$("#G1008_C14055").val(item.G1008_C14055);
	 
			                                    	$("#G1008_C14056").val(item.G1008_C14056);
	      
				                                    if(item.G1008_C14057 == 1){
				                                       $("#G1008_C14057").attr('checked', true);
				                                    } 
	      
				                                    if(item.G1008_C14058 == 1){
				                                       $("#G1008_C14058").attr('checked', true);
				                                    } 
	      
				                                    if(item.G1008_C14084 == 1){
				                                       $("#G1008_C14084").attr('checked', true);
				                                    } 
	      
				                                    if(item.G1008_C14085 == 1){
				                                       $("#G1008_C14085").attr('checked', true);
				                                    } 
	      
				                                    if(item.G1008_C14086 == 1){
				                                       $("#G1008_C14086").attr('checked', true);
				                                    } 
	 
	                    $("#G1008_C15543").val(item.G1008_C15543).trigger("change"); 
	 
	                    $("#G1008_C15544").attr("opt",item.G1008_C15544); 
	 
	                    $("#G1008_C15545").attr("opt",item.G1008_C15545); 
			              							$("#h3mio").html(item.principal);
				                                });

				                                //Deshabilitar los campos 2

				                                //Habilitar todos los campos para edicion
				                                $('#FormularioDatos :input').each(function(){
				                                    $(this).attr('disabled', true);
				                                });

				                                //Habilidar los botones de operacion, add, editar, eliminar
				                                $("#add").attr('disabled', false);
				                                $("#edit").attr('disabled', false);
				                                $("#delete").attr('disabled', false);

				                                //Desahabiliatra los botones de salvar y seleccionar_registro
				                                $("#cancel").attr('disabled', true);
				                                $("#Save").attr('disabled', true);
				                            } 
				                        })
				                        $("#hidId").val(ID);  

			                        <?php }else{ 
			                        	if(!isset($_GET['formulario'])){
			                        ?>

			                        	$.ajax({
			                        		url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
			                        		type  : "post",
			                        		data  : formData,
			                    		 	cache: false,
						                    contentType: false,
						                    processData: false,
			                        		success : function(xt){
			                        			console.log(xt);
			                        			window.location.href = "quitar.php";
			                        		}
			                        	});
			                        	
					
			                        <?php } 
			                        	}
			                        ?>            
			                    }else{
			                        //Algo paso, hay un error
			                        $("#Save").attr('disabled', false);
			                        alertify.error('Un error ha ocurrido y no pudimos guardar la información');
			                    }                
			                },
			                //si ha ocurrido un error
			                error: function(){
			                    after_save_error();
			                    $("#Save").attr('disabled', false);
			                    alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
			                }
			            });
	          		}
	          	}
	        });
	    });

	        //funcionalidad del boton Gestion botonCerrarErronea
	        




	    <?php if(!isset($_GET['view'])) { ?>
	    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
	    //Cargar datos de la hoja de datos
	    function cargar_hoja_datos(){
	        $.jgrid.defaults.width = '1225';
	        $.jgrid.defaults.height = '650';
	        $.jgrid.defaults.responsive = true;
	        $.jgrid.defaults.styleUI = 'Bootstrap';
	        var lastsel2;
	        $("#tablaDatos").jqGrid({
	            url:'<?=$url_crud;?>?CallDatosJson=si',
	            datatype: 'json',
	            mtype: 'POST',
	            colNames:['id','FECHA','CODIGO AVISO','CONCEPTO','TIPO INMUEBLE','TIPO OFERTA','CIUDAD','CODIGO CLIENTE','CLIENTE','TELEFONO 1','TELEFONO 2','EMAIL','DEPARTAMENTO','OBSERVACIONES CONTACTO EFECTIVO','Agente','Fecha','Hora','Campaña','GESTION','TIPIFICACION 1','TIPIFICACION 2'],
	            colModel:[
	                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
	                {
	                    name:'providerUserId',
	                    index:'providerUserId', 
	                    width:100,
	                    editable:true, 
	                    editrules:{
	                        required:false, 
	                        edithidden:true
	                    },
	                    hidden:true, 
	                    editoptions:{ 
	                        dataInit: function(element) {                     
	                          $(element).attr("readonly", "readonly"); 
	                        } 
	                    }
	                }

		                ,
		                {  
		                    name:'G1008_C14035', 
		                    index:'G1008_C14035', 
		                    width:120 ,
		                    editable: true ,
		                    formatter: 'text', 
		                    searchoptions: {
		                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
		                    }, 
		                    editoptions:{
		                        size:20,
		                        dataInit:function(el){
		                            $(el).datepicker({
		                                language: "es",
		                                autoclose: true,
		                                todayHighlight: true
		                            });
		                        },
		                        defaultValue: function(){
		                            var currentTime = new Date();
		                            var month = parseInt(currentTime.getMonth() + 1);
		                            month = month <= 9 ? "0"+month : month;
		                            var day = currentTime.getDate();
		                            day = day <= 9 ? "0"+day : day;
		                            var year = currentTime.getFullYear();
		                            return year+"-"+month + "-"+day;
		                        }
		                    }
		                }

		                ,
		                { 
		                    name:'G1008_C14036', 
		                    index: 'G1008_C14036', 
		                    width:160, 
		                    resizable:false, 
		                    sortable:true , 
		                    editable: true 
		                }

		                ,
		                { 
		                    name:'G1008_C14037', 
		                    index: 'G1008_C14037', 
		                    width:160, 
		                    resizable:false, 
		                    sortable:true , 
		                    editable: true 
		                }

		                ,
		                { 
		                    name:'G1008_C14038', 
		                    index: 'G1008_C14038', 
		                    width:160, 
		                    resizable:false, 
		                    sortable:true , 
		                    editable: true 
		                }

		                ,
		                { 
		                    name:'G1008_C14039', 
		                    index: 'G1008_C14039', 
		                    width:160, 
		                    resizable:false, 
		                    sortable:true , 
		                    editable: true 
		                }

		                ,
		                { 
		                    name:'G1008_C14040', 
		                    index: 'G1008_C14040', 
		                    width:160, 
		                    resizable:false, 
		                    sortable:true , 
		                    editable: true 
		                }

		                ,
		                { 
		                    name:'G1008_C14041', 
		                    index: 'G1008_C14041', 
		                    width:160, 
		                    resizable:false, 
		                    sortable:true , 
		                    editable: true 
		                }

		                ,
		                { 
		                    name:'G1008_C14042', 
		                    index: 'G1008_C14042', 
		                    width:160, 
		                    resizable:false, 
		                    sortable:true , 
		                    editable: true 
		                }

		                ,
		                { 
		                    name:'G1008_C14043', 
		                    index: 'G1008_C14043', 
		                    width:160, 
		                    resizable:false, 
		                    sortable:true , 
		                    editable: true 
		                }

		                ,
		                { 
		                    name:'G1008_C14044', 
		                    index: 'G1008_C14044', 
		                    width:160, 
		                    resizable:false, 
		                    sortable:true , 
		                    editable: true 
		                }

		                ,
		                { 
		                    name:'G1008_C14045', 
		                    index: 'G1008_C14045', 
		                    width:160, 
		                    resizable:false, 
		                    sortable:true , 
		                    editable: true 
		                }

		                ,
		                { 
		                    name:'G1008_C14046', 
		                    index: 'G1008_C14046', 
		                    width:160, 
		                    resizable:false, 
		                    sortable:true , 
		                    editable: true 
		                }

		                ,
		                { 
		                    name:'G1008_C14047', 
		                    index:'G1008_C14047', 
		                    width:150, 
		                    editable: true 
		                }

		                ,
		                { 
		                    name:'G1008_C14053', 
		                    index: 'G1008_C14053', 
		                    width:160, 
		                    resizable:false, 
		                    sortable:true , 
		                    editable: true 
		                }

		                ,
		                { 
		                    name:'G1008_C14054', 
		                    index: 'G1008_C14054', 
		                    width:160, 
		                    resizable:false, 
		                    sortable:true , 
		                    editable: true 
		                }

		                ,
		                { 
		                    name:'G1008_C14055', 
		                    index: 'G1008_C14055', 
		                    width:160, 
		                    resizable:false, 
		                    sortable:true , 
		                    editable: true 
		                }

		                ,
		                { 
		                    name:'G1008_C14056', 
		                    index: 'G1008_C14056', 
		                    width:160, 
		                    resizable:false, 
		                    sortable:true , 
		                    editable: true 
		                }

		                ,
		                { 
		                    name:'G1008_C15543', 
		                    index:'G1008_C15543', 
		                    width:120 ,
		                    editable: true, 
		                    edittype:"select" , 
		                    editoptions: {
		                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=747&campo=G1008_C15543'
		                    }
		                }

		                ,
		                { 
		                    name:'G1008_C15544', 
		                    index:'G1008_C15544', 
		                    width:120 ,
		                    editable: true, 
		                    edittype:"select" , 
		                    editoptions: {
		                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=748&campo=G1008_C15544'
		                    }
		                }

		                ,
		                { 
		                    name:'G1008_C15545', 
		                    index:'G1008_C15545', 
		                    width:120 ,
		                    editable: true, 
		                    edittype:"select" , 
		                    editoptions: {
		                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=743&campo=G1008_C15545'
		                    }
		                }
	            ],
	            pager: "#pager" ,
	            beforeSelectRow: function(rowid){
	                if(rowid && rowid!==lastsel2){
	                    
	                }
	                lastsel2=rowid;
	            },
	            rowNum: 50,
	            rowList:[50,100],
	            loadonce: false,
	            sortable: true,
	            sortname: 'G1008_C14042',
	            sortorder: 'asc',
	            viewrecords: true,
	            caption: 'PRUEBAS',
	            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
	            autowidth: true
	            
		            ,subGrid: true,
		            subGridRowExpanded: function(subgrid_id, row_id) { 
		                // we pass two parameters 
		                // subgrid_id is a id of the div tag created whitin a table data 
		                // the id of this elemenet is a combination of the "sg_" + id of the row 
		                // the row_id is the id of the row 
		                // If we wan to pass additinal parameters to the url we can use 
		                // a method getRowData(row_id) - which returns associative array in type name-value 
		                // here we can easy construct the flowing 
		                $("#"+subgrid_id).html('');

	                var subgrid_table_id_0, pager_id_0; 

	                subgrid_table_id_0 = subgrid_id+"_t_0"; 

	                pager_id_ = "p_"+subgrid_table_id_0; 

	                $("#"+subgrid_id).append("<table id='"+subgrid_table_id_0+"' class='scroll'></table><div id='"+pager_id_0+"' class='scroll'></div>"); 

	                jQuery("#"+subgrid_table_id_0).jqGrid({ 
	                    url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+row_id,
	                    datatype: 'xml',
	                    mtype: 'POST',
	                    colNames:['id','ID CLIENTE','GESTIÓN','TIPIFICACION 1','TIPIFICACION 2','FECHA DE TIPIFICACION ','HORA TIPIFICACION','AGENTE ','OBSERVACIONES ','CAMPAÑA','FECHA AGENDA ','HORA AGENDA ','ID DY', 'padre'],
	                    colModel: [ 
	                        {    
	                            name:'providerUserId',
	                            index:'providerUserId', 
	                            width:100,editable:true, 
	                            editrules:{
	                                required:false, 
	                                edithidden:true
	                            },
	                            hidden:true, 
	                            editoptions:{ 
	                                dataInit: function(element) {                     
	                                    $(element).attr("readonly", "readonly"); 
	                                } 
	                            }
	                        }

	                        ,
	                        { 
	                            name:'G1101_C16723', 
	                            index: 'G1101_C16723', 
	                            width:160, 
	                            resizable:false, 
	                            sortable:true , 
	                            editable: true 
	                        }

	                        ,
	                        {  
	                            name:'G1101_C16715', 
	                            index:'G1101_C16715', 
	                            width:120 ,
	                            editable: true, 
	                            edittype:"select" , 
	                            editoptions: {
	                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=747&campo=G1101_C16715'
	                            }
	                        }

	                        ,
	                        {  
	                            name:'G1101_C16719', 
	                            index:'G1101_C16719', 
	                            width:120 ,
	                            editable: true, 
	                            edittype:"select" , 
	                            editoptions: {
	                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=748&campo=G1101_C16719'
	                            }
	                        }

	                        ,
	                        {  
	                            name:'G1101_C16729', 
	                            index:'G1101_C16729', 
	                            width:120 ,
	                            editable: true, 
	                            edittype:"select" , 
	                            editoptions: {
	                                dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=743&campo=G1101_C16729'
	                            }
	                        }

	                        ,
	                        {  
	                            name:'G1101_C16717', 
	                            index:'G1101_C16717', 
	                            width:120 ,
	                            editable: true ,
	                            formatter: 'text', 
	                            searchoptions: {
	                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                            }, 
	                            editoptions:{
	                                size:20,
	                                dataInit:function(el){
	                                    $(el).datepicker({
	                                        language: "es",
	                                        autoclose: true,
	                                        todayHighlight: true
	                                    });
	                                },
	                                defaultValue: function(){
	                                    var currentTime = new Date();
	                                    var month = parseInt(currentTime.getMonth() + 1);
	                                    month = month <= 9 ? "0"+month : month;
	                                    var day = currentTime.getDate();
	                                    day = day <= 9 ? "0"+day : day;
	                                    var year = currentTime.getFullYear();
	                                    return year+"-"+month + "-"+day;
	                                }
	                            }
	                        },
	                        {  
	                            name:'G1101_C30105', 
	                            index:'G1101_C30105', 
	                            width:70 ,
	                            editable: true ,
	                            formatter: 'text', 
	                            editoptions:{
	                                size:20,
	                                dataInit:function(el){
	                                    //Timepicker
	                                     var options = {  //hh:mm 24 hour format only, defaults to current time
	                                        timeFormat: 'HH:mm:ss',
	                                        interval: 5,
	                                        minTime: '10',
	                                        dynamic: false,
	                                        dropdown: true,
	                                        scrollbar: true
	                                    }; 
	                                    $(el).timepicker(options);
	                    

	                                }
	                            }
	                        }

	                        ,
	                        { 
	                            name:'G1101_C16718', 
	                            index: 'G1101_C16718', 
	                            width:160, 
	                            resizable:false, 
	                            sortable:true , 
	                            editable: true 
	                        }

	                        ,
	                        { 
	                            name:'G1101_C16716', 
	                            index:'G1101_C16716', 
	                            width:150, 
	                            editable: true 
	                        }

	                        ,
	                        { 
	                            name:'G1101_C16720', 
	                            index: 'G1101_C16720', 
	                            width:160, 
	                            resizable:false, 
	                            sortable:true , 
	                            editable: true 
	                        }

	                        ,
	                        {  
	                            name:'G1101_C16721', 
	                            index:'G1101_C16721', 
	                            width:120 ,
	                            editable: true ,
	                            formatter: 'text', 
	                            searchoptions: {
	                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                            }, 
	                            editoptions:{
	                                size:20,
	                                dataInit:function(el){
	                                    $(el).datepicker({
	                                        language: "es",
	                                        autoclose: true,
	                                        todayHighlight: true
	                                    });
	                                },
	                                defaultValue: function(){
	                                    var currentTime = new Date();
	                                    var month = parseInt(currentTime.getMonth() + 1);
	                                    month = month <= 9 ? "0"+month : month;
	                                    var day = currentTime.getDate();
	                                    day = day <= 9 ? "0"+day : day;
	                                    var year = currentTime.getFullYear();
	                                    return year+"-"+month + "-"+day;
	                                }
	                            }
	                        }

	                        ,
	                        {  
	                            name:'G1101_C16722', 
	                            index:'G1101_C16722', 
	                            width:70 ,
	                            editable: true ,
	                            formatter: 'text', 
	                            editoptions:{
	                                size:20,
	                                dataInit:function(el){
	                                    //Timepicker
	                                     var options = {  //hh:mm 24 hour format only, defaults to current time
	                                        timeFormat: 'HH:mm:ss',
	                                        interval: 5,
	                                        minTime: '10',
	                                        dynamic: false,
	                                        dropdown: true,
	                                        scrollbar: true
	                                    }; 
	                                    $(el).timepicker(options);
	                    

	                                }
	                            }
	                        }
	 
	                        ,
	                        {  
	                            name:'G1101_C16730', 
	                            index:'G1101_C16730', 
	                            width:80 ,
	                            editable: true, 
	                            searchoptions: {
	                                sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                            }, 
	                            editoptions:{
	                                size:20,
	                                dataInit:function(el){
	                                    $(el).numeric();
	                                }
	                            }

	                        }

	                        ,
	                        { 
	                            name: 'Padre', 
	                            index:'Padre', 
	                            hidden: true , 
	                            editable: false, 
	                            editrules: { 
	                                edithidden:true 
	                            },
			                    editoptions:{ 
			                        dataInit: function(element) {                     
			                            $(element).val(id); 
			                        } 
			                    }
	                        }
	                    ], 
	                    rowNum:20, 
	                    pager: pager_id_0, 
	                    sortname: 'num', 
	                    sortorder: "asc",
	                    height: '100%' 
	                }); 

	                jQuery("#"+subgrid_table_id_0).jqGrid('navGrid',"#"+pager_id_0,{edit:false,add:false,del:false}) 

	            }, 
	            subGridRowColapsed: function(subgrid_id, row_id) { 
	                // this function is called before removing the data 
	                //var subgrid_table_id; 
	                //subgrid_table_id = subgrid_id+"_t"; 
	                //jQuery("#"+subgrid_table_id).remove(); 
	            }
	        });

	        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
	        $('#tablaDatos').inlineNav('#pager',
	        // the buttons to appear on the toolbar of the grid
	        { 
	            edit: true, 
	            add: true, 
	            cancel: true,
	            editParams: {
	                keys: true,
	            },
	            addParams: {
	                keys: true
	            }
	        });
	      
	        //para cuando se Maximice o minimize la pantalla.
	        $(window).bind('resize', function() {
	            $("#tablaDatos").setGridWidth($(window).width());
	        }).trigger('resize'); 
	    }

	    //SECCION  : Manipular Lista de Navegacion

	    //buscar registro en la Lista de navegacion
	    function llenar_lista_navegacion(x){
	        var tr = '';
	        $.ajax({
	            url      : '<?=$url_crud;?>',
	            type     : 'POST',
	            data     : { CallDatosJson : 'SI', Busqueda : x},
	            dataType : 'json',
	            success  : function(data){
	                //Cargar la lista con los datos obtenidos en la consulta
	                $.each(data, function(i, item) {
	                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
	                    tr += "<td>";
	                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
	                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
	                    tr += "</td>";
	                    tr += "</tr>";
	                });
	                $("#tablaScroll").html(tr);
	                //aplicar funcionalidad a la Lista de navegacion
	                busqueda_lista_navegacion();

	                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
	                if ( $("#"+idTotal).length > 0) {
	                    $("#"+idTotal).click();   
	                    $("#"+idTotal).addClass('active'); 
	                }else{
	                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
	                    $(".CargarDatos :first").click();
	                }

	            } 
	        });
	    }

	    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
	    function busqueda_lista_navegacion(){

	        $(".CargarDatos").click(function(){
	            //remover todas las clases activas de la lista de navegacion
	            $(".CargarDatos").each(function(){
	                $(this).removeClass('active');
	            });
	            
	            //add la clase activa solo ala celda que le dimos click.
	            $(this).addClass('active');
	              
	              
	            var id = $(this).attr('id');
	            $("#btnLlamar_0").attr('padre', id);
	            //buscar los datos
	            $.ajax({
	                url      : '<?=$url_crud;?>',
	                type     : 'POST',
	                data     : { CallDatos : 'SI', id : id },
	                dataType : 'json',
	                success  : function(data){
	                    //recorrer datos y enviarlos al formulario
	                    $.each(data, function(i, item) {
	                        

	            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
	            

	                        $("#G1008_C14035").val(item.G1008_C14035);

	                        $("#G1008_C14036").val(item.G1008_C14036);

	                        $("#G1008_C14037").val(item.G1008_C14037);

	                        $("#G1008_C14038").val(item.G1008_C14038);

	                        $("#G1008_C14039").val(item.G1008_C14039);

	                        $("#G1008_C14040").val(item.G1008_C14040);

	                        $("#G1008_C14041").val(item.G1008_C14041);

	                        $("#G1008_C14042").val(item.G1008_C14042);

	                        $("#G1008_C14043").val(item.G1008_C14043);

	                        $("#G1008_C14044").val(item.G1008_C14044);

	                        $("#G1008_C14045").val(item.G1008_C14045);

	                        $("#G1008_C14046").val(item.G1008_C14046);

	                        $("#G1008_C14047").val(item.G1008_C14047);
	 
	                    $("#G1008_C14048").val(item.G1008_C14048).trigger("change"); 
	 
	                    $("#G1008_C14049").val(item.G1008_C14049).trigger("change"); 

	                        $("#G1008_C14050").val(item.G1008_C14050);

	                        $("#G1008_C14051").val(item.G1008_C14051);

	                        $("#G1008_C14052").val(item.G1008_C14052);

	                        $("#G1008_C14053").val(item.G1008_C14053);

	                        $("#G1008_C14054").val(item.G1008_C14054);

	                        $("#G1008_C14055").val(item.G1008_C14055);

	                        $("#G1008_C14056").val(item.G1008_C14056);
	    
	                        if(item.G1008_C14057 == 1){
	                           $("#G1008_C14057").attr('checked', true);
	                        } 
	    
	                        if(item.G1008_C14058 == 1){
	                           $("#G1008_C14058").attr('checked', true);
	                        } 
	    
	                        if(item.G1008_C14084 == 1){
	                           $("#G1008_C14084").attr('checked', true);
	                        } 
	    
	                        if(item.G1008_C14085 == 1){
	                           $("#G1008_C14085").attr('checked', true);
	                        } 
	    
	                        if(item.G1008_C14086 == 1){
	                           $("#G1008_C14086").attr('checked', true);
	                        } 
	 
	                    $("#G1008_C15543").val(item.G1008_C15543).trigger("change"); 
	 
	                    $("#G1008_C15544").attr("opt",item.G1008_C15544); 
	 
	                    $("#G1008_C15545").attr("opt",item.G1008_C15545); 
				            
	        				$("#h3mio").html(item.principal);
	        				
	                    });

	                    //Deshabilitar los campos

	                    //Habilitar todos los campos para edicion
	                    $('#FormularioDatos :input').each(function(){
	                        $(this).attr('disabled', true);
	                    });

	                    //Habilidar los botones de operacion, add, editar, eliminar
	                    $("#add").attr('disabled', false);
	                    $("#edit").attr('disabled', false);
	                    $("#delete").attr('disabled', false);

	                    //Desahabiliatra los botones de salvar y seleccionar_registro
	                    $("#cancel").attr('disabled', true);
	                    $("#Save").attr('disabled', true);
	                } 
	            });

	            $("#hidId").val(id);
	            idTotal = $("#hidId").val();
	        });
	    }

	    function seleccionar_registro(){
	        //Seleccinar loos registros de la Lista de navegacion, 
	        if ( $("#"+idTotal).length > 0) {
	            $("#"+idTotal).click();   
	            $("#"+idTotal).addClass('active'); 
	            idTotal = 0;
	        }else{
	            $(".CargarDatos :first").click();
	        } 
	        

	            $.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion descargar 
	    } 

	    <?php } ?>


	    

	    function cargarHijos_0(id_0){
	        $.jgrid.defaults.width = '1225';
	        $.jgrid.defaults.height = '650';
	        $.jgrid.defaults.responsive = true;
	        $.jgrid.defaults.styleUI = 'Bootstrap';
	        var lastSels;
	        $("#tablaDatosDetalless0").jqGrid({
	            url:'<?=$url_crud;?>?callDatosSubgrilla_0=si&id='+id_0,
	            datatype: 'xml',
	            mtype: 'POST',
	            xmlReader: { 
	                root:"rows", 
	                row:"row",
	                cell:"cell",
	                id : "[asin]"
	            },
	            colNames:['id','ID CLIENTE','GESTIÓN','TIPIFICACION 1','TIPIFICACION 2','FECHA DE TIPIFICACION ','HORA TIPIFICACION','AGENTE ','OBSERVACIONES ','CAMPAÑA','FECHA AGENDA ','HORA AGENDA ','ID DY', 'padre'],
	            colModel:[

	                {
	                    name:'providerUserId',
	                    index:'providerUserId', 
	                    width:100,editable:true, 
	                    editrules:{
	                        required:false, 
	                        edithidden:true
	                    },
	                    hidden:true, 
	                    editoptions:{ 
	                        dataInit: function(element) {                     
	                            $(element).attr("readonly", "readonly"); 
	                        } 
	                    }
	                }

	                ,
	                { 
	                    name:'G1101_C16723', 
	                    index: 'G1101_C16723', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                {  
	                    name:'G1101_C16715', 
	                    index:'G1101_C16715', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=747&campo=G1101_C16715'
	                    }
	                }

	                ,
	                {  
	                    name:'G1101_C16719', 
	                    index:'G1101_C16719', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=748&campo=G1101_C16719'
	                    }
	                }

	                ,
	                {  
	                    name:'G1101_C16729', 
	                    index:'G1101_C16729', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=743&campo=G1101_C16729'
	                    }
	                }

	                ,
	                {  
	                    name:'G1101_C16717', 
	                    index:'G1101_C16717', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                },

					{  
                        name:'G1101_C30105', 
                        index:'G1101_C30105', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                 var options = {  //hh:mm 24 hour format only, defaults to current time
                                    timeFormat: 'HH:mm:ss',
                                    interval: 5,
                                    minTime: '10',
                                    dynamic: false,
                                    dropdown: true,
                                    scrollbar: true
                                }; 
                                $(el).timepicker(options);
                

                            }
                        }
	                }

	                ,
	                { 
	                    name:'G1101_C16718', 
	                    index: 'G1101_C16718', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                {
	                    name:'G1101_C16716', 
	                    index:'G1101_C16716', 
	                    width:150, 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1101_C16720', 
	                    index: 'G1101_C16720', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                {  
	                    name:'G1101_C16721', 
	                    index:'G1101_C16721', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                {  
	                    name:'G1101_C16722', 
	                    index:'G1101_C16722', 
	                    width:70 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            //Timepicker
	                            var options = {  //hh:mm 24 hour format only, defaults to current time
	                                timeFormat: 'HH:mm:ss',
	                                interval: 5,
	                                minTime: '10',
	                                dynamic: false,
	                                dropdown: true,
	                                scrollbar: true
	                            }; 
	                            $(el).timepicker(options);
	                            $(".timepicker").css("z-index", 99999 );
	                        }
	                    }
	                }
	 
	                ,
	                {  
	                    name:'G1101_C16730', 
	                    index:'G1101_C16730', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,

				        		dataInit:function(el){
	    							$(el).numeric();
	    						}
	                    }

	                }
	                ,
	                { 
	                    name: 'Padre', 
	                    index:'Padre', 
	                    hidden: true , 
	                    editable: true, 
	                    editrules: {
	                        edithidden:true
	                    },
	                    editoptions:{ 
	                        dataInit: function(element) {                     
	                            $(element).val(id_0); 
	                        } 
	                    }
	                }
	            ],
	            rowNum: 40,
	            pager: "#pagerDetalles0",
	            rowList: [40,80],
	            sortable: true,
	            sortname: 'G1101_C16723',
	            sortorder: 'asc',
	            viewrecords: true,
	            caption: 'GESTIÓN',
	            editurl:"<?=$url_crud;?>?insertarDatosSubgrilla_0=si&usuario=<?php echo getIdentificacionUser($token);?>",
	            height:'250px',
	            beforeSelectRow: function(rowid){
	                if(rowid && rowid!==lastSels){
	                    
	                }
	                lastSels = rowid;
	            }
	            ,

	            ondblClickRow: function(rowId) {
	                $("#frameContenedor").attr('src', 'https://<?php echo $_SERVER["HTTP_HOST"];?>/crm_php/new_index.php?formulario=1101&view=si&registroId='+ rowId +'&formaDetalle=si&yourfather='+ idTotal +'&pincheCampo=16723&formularioPadre=1008<?php if(isset($_GET['token'])){ echo "&token=".$_GET['token']; }?>');
	                $("#editarDatos").modal('show');

	            }
	        }); 

	        $(window).bind('resize', function() {
	            $("#tablaDatosDetalless0").setGridWidth($(window).width());
	        }).trigger('resize');
	    }

	    function vamosRecargaLasGrillasPorfavor(id){
	        
	    	$("#btnLlamar_0").attr('padre', $("#G1008_C14041").val());
	    		var id_0 = $("#G1008_C14041").val();
	    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
	    		cargarHijos_0(id_0);
	    }
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			<?php
	            if(isset($campSql)){
	                //recorro la campaña para tener los datos que necesito
	                /*$resultcampSql = $mysqli->query($campSql);
	                while($key = $resultcampSql->fetch_object()){
	                    

	                    //consulta de datos del usuario
	                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

	                    //echo $DatosSql;
	                    //recorro la tabla de donde necesito los datos
	                    $resultDatosSql = $mysqli->query($DatosSql);
	                    if($resultDatosSql){
	                		while($objDatos = $resultDatosSql->fetch_object()){ ?>
	                        	document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
	             		<?php  
	             			}	
	                    }
	                    
	                } */  
	            }
	        ?>
	    	<?php if(isset($_GET['user'])){ ?>
	        	
	    	$("#btnLlamar_0").attr('padre', $("#G1008_C14041").val());
	    		var id_0 = $("#G1008_C14041").val();
	    		$.jgrid.gridUnload('#tablaDatosDetalless0'); //funcion Recargar 
	    		cargarHijos_0(id_0);
	        	idTotal = <?php echo $_GET['user'];?>; 
	        <?php } ?>
			
		});
	</script>
