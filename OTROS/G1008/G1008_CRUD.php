<?php
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1008_ConsInte__b, G1008_FechaInsercion , G1008_Usuario ,  G1008_CodigoMiembro  , G1008_PoblacionOrigen , G1008_EstadoDiligenciamiento ,  G1008_IdLlamada , G1008_C14042 as principal ,G1008_C14035,G1008_C14036,G1008_C14037,G1008_C14038,G1008_C14039,G1008_C14040,G1008_C14041,G1008_C14042,G1008_C14043,G1008_C14044,G1008_C14045,G1008_C14046,G1008_C14047,G1008_C14048,G1008_C14049,G1008_C14050,G1008_C14051,G1008_C14052,G1008_C14053,G1008_C14054,G1008_C14055,G1008_C14056,G1008_C15543,G1008_C15544,G1008_C15545 FROM '.$BaseDatos.'.G1008 WHERE G1008_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1008_C14035'] = explode(' ', $key->G1008_C14035)[0];

                $datos[$i]['G1008_C14036'] = $key->G1008_C14036;

                $datos[$i]['G1008_C14037'] = $key->G1008_C14037;

                $datos[$i]['G1008_C14038'] = $key->G1008_C14038;

                $datos[$i]['G1008_C14039'] = $key->G1008_C14039;

                $datos[$i]['G1008_C14040'] = $key->G1008_C14040;

                $datos[$i]['G1008_C14041'] = $key->G1008_C14041;

                $datos[$i]['G1008_C14042'] = $key->G1008_C14042;

                $datos[$i]['G1008_C14043'] = $key->G1008_C14043;

                $datos[$i]['G1008_C14044'] = $key->G1008_C14044;

                $datos[$i]['G1008_C14045'] = $key->G1008_C14045;

                $datos[$i]['G1008_C14046'] = $key->G1008_C14046;

                $datos[$i]['G1008_C14047'] = $key->G1008_C14047;

                $datos[$i]['G1008_C14048'] = $key->G1008_C14048;

                $datos[$i]['G1008_C14049'] = $key->G1008_C14049;

                $datos[$i]['G1008_C14050'] = explode(' ', $key->G1008_C14050)[0];
  
                $hora = '';
                if(!is_null($key->G1008_C14051)){
                    $hora = explode(' ', $key->G1008_C14051)[1];
                }

                $datos[$i]['G1008_C14051'] = $hora;

                $datos[$i]['G1008_C14052'] = $key->G1008_C14052;

                $datos[$i]['G1008_C14053'] = $key->G1008_C14053;

                $datos[$i]['G1008_C14054'] = $key->G1008_C14054;

                $datos[$i]['G1008_C14055'] = $key->G1008_C14055;

                $datos[$i]['G1008_C14056'] = $key->G1008_C14056;

                $datos[$i]['G1008_C15543'] = $key->G1008_C15543;

                $datos[$i]['G1008_C15544'] = $key->G1008_C15544;

                $datos[$i]['G1008_C15545'] = $key->G1008_C15545;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1008_ConsInte__b as id,  G1008_C14041 as camp2 , G1008_C14042 as camp1 ";
            $Lsql .= " FROM ".$BaseDatos.".G1008 ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " WHERE G1008_C14041 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1008_C14042 like '%".$_POST['Busqueda']."%' ";
            }
            $Lsql .= " ORDER BY G1008_ConsInte__b DESC LIMIT 0, 50 "; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
	    	$Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
	    	$res = $mysqli->query($Lsql);
	    	echo "<option value='0'>Seleccione</option>";
	    	while($key = $res->fetch_object()){
	    		echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
	    	}
	    }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1008");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1008_ConsInte__b, G1008_FechaInsercion , G1008_Usuario ,  G1008_CodigoMiembro  , G1008_PoblacionOrigen , G1008_EstadoDiligenciamiento ,  G1008_IdLlamada , G1008_C14042 as principal ,G1008_C14035,G1008_C14036,G1008_C14037,G1008_C14038,G1008_C14039,G1008_C14040,G1008_C14041,G1008_C14042,G1008_C14043,G1008_C14044,G1008_C14045,G1008_C14046,G1008_C14047, a.LISOPC_Nombre____b as G1008_C14048, b.LISOPC_Nombre____b as G1008_C14049,G1008_C14050,G1008_C14051,G1008_C14052,G1008_C14053,G1008_C14054,G1008_C14055,G1008_C14056, c.LISOPC_Nombre____b as G1008_C15543, d.LISOPC_Nombre____b as G1008_C15544, e.LISOPC_Nombre____b as G1008_C15545 FROM '.$BaseDatos.'.G1008 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1008_C14048 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1008_C14049 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1008_C15543 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1008_C15544 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1008_C15545';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1008_C14051)){
                    $hora_a = explode(' ', $fila->G1008_C14051)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1008_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1008_ConsInte__b , explode(' ', $fila->G1008_C14035)[0] , ($fila->G1008_C14036) , ($fila->G1008_C14037) , ($fila->G1008_C14038) , ($fila->G1008_C14039) , ($fila->G1008_C14040) , ($fila->G1008_C14041) , ($fila->G1008_C14042) , ($fila->G1008_C14043) , ($fila->G1008_C14044) , ($fila->G1008_C14045) , ($fila->G1008_C14046) , ($fila->G1008_C14047) , ($fila->G1008_C14048) , ($fila->G1008_C14049) , explode(' ', $fila->G1008_C14050)[0] , $hora_a , ($fila->G1008_C14052) , ($fila->G1008_C14053) , ($fila->G1008_C14054) , ($fila->G1008_C14055) , ($fila->G1008_C14056) , ($fila->G1008_C15543) , ($fila->G1008_C15544) , ($fila->G1008_C15545) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1008 WHERE G1008_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G1008_ConsInte__b as id,  G1008_C14041 as camp2 , G1008_C14042 as camp1  FROM '.$BaseDatos.'.G1008 ORDER BY G1008_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1008 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1008(";
            $LsqlV = " VALUES ("; 
            // variable que actualiza la bd caducados G1061
            $LsqlUcad = "UPDATE ".$BaseDatos.".G1061 SET "; 
            // variable que actualiza la bd frios G1059
            $LsqlUfri = "UPDATE ".$BaseDatos.".G1059 SET ";
 
            $G1008_C14035 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1008_C14035"])){    
                if($_POST["G1008_C14035"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1008_C14035"]);
                    if(count($tieneHora) > 1){
                    	$G1008_C14035 = "'".$_POST["G1008_C14035"]."'";
                    }else{
                    	$G1008_C14035 = "'".str_replace(' ', '',$_POST["G1008_C14035"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1008_C14035 = ".$G1008_C14035;
                    $LsqlI .= $separador." G1008_C14035";
                    $LsqlV .= $separador.$G1008_C14035;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1008_C14036"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14036 = '".$_POST["G1008_C14036"]."'";
                $LsqlI .= $separador."G1008_C14036";
                $LsqlV .= $separador."'".$_POST["G1008_C14036"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14037"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14037 = '".$_POST["G1008_C14037"]."'";
                $LsqlI .= $separador."G1008_C14037";
                $LsqlV .= $separador."'".$_POST["G1008_C14037"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14038"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14038 = '".$_POST["G1008_C14038"]."'";
                $LsqlI .= $separador."G1008_C14038";
                $LsqlV .= $separador."'".$_POST["G1008_C14038"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14039"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14039 = '".$_POST["G1008_C14039"]."'";
                $LsqlI .= $separador."G1008_C14039";
                $LsqlV .= $separador."'".$_POST["G1008_C14039"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14040"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14040 = '".$_POST["G1008_C14040"]."'";
                $LsqlI .= $separador."G1008_C14040";
                $LsqlV .= $separador."'".$_POST["G1008_C14040"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14041"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14041 = '".$_POST["G1008_C14041"]."'";
                $LsqlI .= $separador."G1008_C14041";
                $LsqlV .= $separador."'".$_POST["G1008_C14041"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14042"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14042 = '".$_POST["G1008_C14042"]."'";
                $LsqlI .= $separador."G1008_C14042";
                $LsqlV .= $separador."'".$_POST["G1008_C14042"]."'";

                $LsqlUcad .= " G1061_C15267 = '".$_POST["G1008_C14042"]."'";
                $LsqlUfri .= " G1059_C15217 = '".$_POST["G1008_C14042"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14043"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14043 = '".$_POST["G1008_C14043"]."'";
                $LsqlI .= $separador."G1008_C14043";
                $LsqlV .= $separador."'".$_POST["G1008_C14043"]."'";
                $LsqlUcad .= $separador."G1061_C15269 = '".$_POST["G1008_C14043"]."'";
                $LsqlUfri .= $separador."G1059_C15218 = '".$_POST["G1008_C14043"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14044"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14044 = '".$_POST["G1008_C14044"]."'";
                $LsqlI .= $separador."G1008_C14044";
                $LsqlV .= $separador."'".$_POST["G1008_C14044"]."'";
                $LsqlUcad .= $separador."G1061_C15270 = '".$_POST["G1008_C14044"]."'";
                $LsqlUfri .= $separador."G1059_C15219 = '".$_POST["G1008_C14044"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14045"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14045 = '".$_POST["G1008_C14045"]."'";
                $LsqlI .= $separador."G1008_C14045";
                $LsqlV .= $separador."'".$_POST["G1008_C14045"]."'";
                $LsqlUcad .= $separador."G1061_C15271 = '".$_POST["G1008_C14045"]."'";
                $LsqlUfri .= $separador."G1059_C15220 = '".$_POST["G1008_C14045"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14046"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14046 = '".$_POST["G1008_C14046"]."'";
                $LsqlI .= $separador."G1008_C14046";
                $LsqlV .= $separador."'".$_POST["G1008_C14046"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14047"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14047 = '".$_POST["G1008_C14047"]."'";
                $LsqlI .= $separador."G1008_C14047";
                $LsqlV .= $separador."'".$_POST["G1008_C14047"]."'";
                $validar = 1;
            }
             
 
            $G1008_C14048 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1008_C14048 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1008_C14048 = ".$G1008_C14048;
                    $LsqlI .= $separador." G1008_C14048";
                    $LsqlV .= $separador.$G1008_C14048;
                    $validar = 1;

                    
                }
            }
 
            $G1008_C14049 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1008_C14049 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1008_C14049 = ".$G1008_C14049;
                    $LsqlI .= $separador." G1008_C14049";
                    $LsqlV .= $separador.$G1008_C14049;
                    $validar = 1;
                }
            }
 
            $G1008_C14050 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1008_C14050 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1008_C14050 = ".$G1008_C14050;
                    $LsqlI .= $separador." G1008_C14050";
                    $LsqlV .= $separador.$G1008_C14050;
                    $validar = 1;
                }
            }
 
            $G1008_C14051 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1008_C14051 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1008_C14051 = ".$G1008_C14051;
                    $LsqlI .= $separador." G1008_C14051";
                    $LsqlV .= $separador.$G1008_C14051;
                    $validar = 1;
                }
            }
 
            $G1008_C14052 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1008_C14052 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1008_C14052 = ".$G1008_C14052;
                    $LsqlI .= $separador." G1008_C14052";
                    $LsqlV .= $separador.$G1008_C14052;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1008_C14053"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14053 = '".$_POST["G1008_C14053"]."'";
                $LsqlI .= $separador."G1008_C14053";
                $LsqlV .= $separador."'".$_POST["G1008_C14053"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14054"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14054 = '".$_POST["G1008_C14054"]."'";
                $LsqlI .= $separador."G1008_C14054";
                $LsqlV .= $separador."'".$_POST["G1008_C14054"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14055"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14055 = '".$_POST["G1008_C14055"]."'";
                $LsqlI .= $separador."G1008_C14055";
                $LsqlV .= $separador."'".$_POST["G1008_C14055"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14056"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14056 = '".$_POST["G1008_C14056"]."'";
                $LsqlI .= $separador."G1008_C14056";
                $LsqlV .= $separador."'".$_POST["G1008_C14056"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14057"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14057 = '".$_POST["G1008_C14057"]."'";
                $LsqlI .= $separador."G1008_C14057";
                $LsqlV .= $separador."'".$_POST["G1008_C14057"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14058"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14058 = '".$_POST["G1008_C14058"]."'";
                $LsqlI .= $separador."G1008_C14058";
                $LsqlV .= $separador."'".$_POST["G1008_C14058"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14084"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14084 = '".$_POST["G1008_C14084"]."'";
                $LsqlI .= $separador."G1008_C14084";
                $LsqlV .= $separador."'".$_POST["G1008_C14084"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14085"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14085 = '".$_POST["G1008_C14085"]."'";
                $LsqlI .= $separador."G1008_C14085";
                $LsqlV .= $separador."'".$_POST["G1008_C14085"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C14086"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C14086 = '".$_POST["G1008_C14086"]."'";
                $LsqlI .= $separador."G1008_C14086";
                $LsqlV .= $separador."'".$_POST["G1008_C14086"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C15543"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C15543 = '".$_POST["G1008_C15543"]."'";
                $LsqlI .= $separador."G1008_C15543";
                $LsqlV .= $separador."'".$_POST["G1008_C15543"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C15544"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C15544 = '".$_POST["G1008_C15544"]."'";
                $LsqlI .= $separador."G1008_C15544";
                $LsqlV .= $separador."'".$_POST["G1008_C15544"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1008_C15545"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_C15545 = '".$_POST["G1008_C15545"]."'";
                $LsqlI .= $separador."G1008_C15545";
                $LsqlV .= $separador."'".$_POST["G1008_C15545"]."'";
                $validar = 1;
            }
             

			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1008_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1008_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
			}


			$padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1008_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
			if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1008_Usuario , G1008_FechaInsercion, G1008_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1008_ConsInte__b =".$_POST["id"]; 
                    if(isset($_POST["G1008_C14041"]) && $_POST["G1008_C14041"]!= 0 && $_POST["G1008_C14041"]!=""){
                        $LsqlUfrios = $LsqlUfri." WHERE G1059_C15216 = ".$_POST["G1008_C14041"];
                        $LsqlUcaducados = $LsqlUcad." WHERE G1061_C15266 = ".$_POST["G1008_C14041"];
                        $upFrios = $mysqli->query($LsqlUfrios);
                        $upCaducados = $mysqli->query($LsqlUcaducados);
                    }

                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1008 WHERE G1008_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){

                    	$UltimoID = $mysqli->insert_id;

                        //JDBD - Insertamos la gestion en el subformulario.
                        $strSQLInsertG1101_t = "INSERT INTO ".$BaseDatos.".G1101 (G1101_C16729,G1101_C16719,G1101_C16716,G1101_C16723,G1101_C16722,G1101_C30105,G1101_C16715,G1101_C16717,G1101_C16721,G1101_C16720,G1101_C16718) SELECT G1008_C15545,G1008_C15544,G1008_C14052,G1008_C14041,G1008_C14051,G1008_FechaInsercion,G1008_C15543,G1008_C14035,G1008_C14050,'ATORADOS' AS campana,G1008_C14053 FROM ".$BaseDatos.".G1008 WHERE G1008_ConsInte__b = ".$UltimoID;

                        $mysqli->query($strSQLInsertG1101_t);

                    	echo $UltimoID; 

                	}else{
                		if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                		echo "1";    		
                	}

                	

                } else {
                	echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1101_ConsInte__b, G1101_C16723, b.LISOPC_Nombre____b as  G1101_C16715, c.LISOPC_Nombre____b as  G1101_C16719, d.LISOPC_Nombre____b as  G1101_C16729, G1101_C16717, G1101_C30105, G1101_C16718, G1101_C16716, G1101_C16720, G1101_C16721, G1101_C16722, G1101_C16730 FROM ".$BaseDatos.".G1101  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b =  G1101_C16715 LEFT JOIN ".$BaseDatos_systema.".LISOPC as c ON c.LISOPC_ConsInte__b =  G1101_C16719 LEFT JOIN ".$BaseDatos_systema.".LISOPC as d ON d.LISOPC_ConsInte__b =  G1101_C16729 ";

        $SQL .= " WHERE G1101_C16723 = '".$numero."'"; 

        $SQL .= " ORDER BY G1101_C16717 DESC";
/*        echo $SQL;*/

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1101_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1101_ConsInte__b)."</cell>"; 
            

            echo "<cell>". ($fila->G1101_C16723)."</cell>";

            echo "<cell>". ($fila->G1101_C16715)."</cell>";

            echo "<cell>". ($fila->G1101_C16719)."</cell>";

            echo "<cell>". ($fila->G1101_C16729)."</cell>";

            if($fila->G1101_C16717 != ''){
                echo "<cell>". explode(' ', $fila->G1101_C16717)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1101_C30105 != ''){
                echo "<cell>". explode(' ', $fila->G1101_C30105)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1101_C16718)."</cell>";

            echo "<cell><![CDATA[". ($fila->G1101_C16716)."]]></cell>";

            echo "<cell>". ($fila->G1101_C16720)."</cell>";

            if($fila->G1101_C16721 != ''){
                echo "<cell>". explode(' ', $fila->G1101_C16721)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1101_C16722 != ''){
                echo "<cell>". explode(' ', $fila->G1101_C16722)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". $fila->G1101_C16730."</cell>"; 
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1101 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1101(";
            $LsqlV = " VALUES ("; 
 
            if(isset($_POST["G1101_C16715"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16715 = '".$_POST["G1101_C16715"]."'";
                $LsqlI .= $separador."G1101_C16715";
                $LsqlV .= $separador."'".$_POST["G1101_C16715"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1101_C16719"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16719 = '".$_POST["G1101_C16719"]."'";
                $LsqlI .= $separador."G1101_C16719";
                $LsqlV .= $separador."'".$_POST["G1101_C16719"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1101_C16729"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16729 = '".$_POST["G1101_C16729"]."'";
                $LsqlI .= $separador."G1101_C16729";
                $LsqlV .= $separador."'".$_POST["G1101_C16729"]."'";
                $validar = 1;
            }

            $G1101_C16717 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1101_C16717"])){    
                if($_POST["G1101_C16717"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1101_C16717 = "'".str_replace(' ', '',$_POST["G1101_C16717"])." 00:00:00'";
                    $LsqlU .= $separador." G1101_C16717 = ".$G1101_C16717;
                    $LsqlI .= $separador." G1101_C16717";
                    $LsqlV .= $separador.$G1101_C16717;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1101_C16718"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16718 = '".$_POST["G1101_C16718"]."'";
                $LsqlI .= $separador."G1101_C16718";
                $LsqlV .= $separador."'".$_POST["G1101_C16718"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
  

            if(isset($_POST["G1101_C16716"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16716 = '".$_POST["G1101_C16716"]."'";
                $LsqlI .= $separador."G1101_C16716";
                $LsqlV .= $separador."'".$_POST["G1101_C16716"]."'";
                $validar = 1;
            }
                                                                           
 
                                                                         
            if(isset($_POST["G1101_C16720"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16720 = '".$_POST["G1101_C16720"]."'";
                $LsqlI .= $separador."G1101_C16720";
                $LsqlV .= $separador."'".$_POST["G1101_C16720"]."'";
                $validar = 1;
            }
                                                                          
                                                                           

            $G1101_C16721 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1101_C16721"])){    
                if($_POST["G1101_C16721"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1101_C16721 = "'".str_replace(' ', '',$_POST["G1101_C16721"])." 00:00:00'";
                    $LsqlU .= $separador." G1101_C16721 = ".$G1101_C16721;
                    $LsqlI .= $separador." G1101_C16721";
                    $LsqlV .= $separador.$G1101_C16721;
                    $validar = 1;
                }
            }
 
            $G1101_C16722 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1101_C16722"])){    
                if($_POST["G1101_C16722"] != '' && $_POST["G1101_C16722"] != 'undefined' && $_POST["G1101_C16722"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1101_C16722 = "'".$fecha." ".str_replace(' ', '',$_POST["G1101_C16722"])."'";
                    $LsqlU .= $separador."  G1101_C16722 = ".$G1101_C16722."";
                    $LsqlI .= $separador."  G1101_C16722";
                    $LsqlV .= $separador.$G1101_C16722;
                    $validar = 1;
                }
            }
 
            $G1101_C16730= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1101_C16730"])){    
                if($_POST["G1101_C16730"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1101_C16730 = $_POST["G1101_C16730"];
                    $LsqlU .= $separador." G1101_C16730 = '".$G1101_C16730."'";
                    $LsqlI .= $separador." G1101_C16730";
                    $LsqlV .= $separador."'".$G1101_C16730."'";
                    $validar = 1;
                }
            }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1101_C16723 = $numero;
                    $LsqlU .= ", G1101_C16723 = ".$G1101_C16723."";
                    $LsqlI .= ", G1101_C16723";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1101_Usuario ,  G1101_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1101_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1101 WHERE  G1101_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
