<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1875_ConsInte__b, G1875_FechaInsercion , G1875_Usuario ,  G1875_CodigoMiembro  , G1875_PoblacionOrigen , G1875_EstadoDiligenciamiento ,  G1875_IdLlamada , G1875_C34468 as principal ,G1875_C34468,G1875_C34469,G1875_C34470,G1875_C34457,G1875_C34458,G1875_C34459,G1875_C34460,G1875_C34461,G1875_C34462,G1875_C34463,G1875_C34464,G1875_C34465,G1875_C34466,G1875_C34467,G1875_C34471,G1875_C34472,G1875_C34473,G1875_C34474,G1875_C34475,G1875_C34639,G1875_C34640,G1875_C34641,G1875_C34642,G1875_C34476,G1875_C34643,G1875_C34477,G1875_C34480,G1875_C34478,G1875_C34479,G1875_C34481,G1875_C34486,G1875_C34482,G1875_C34485,G1875_C34483,G1875_C34484,G1875_C34487,G1875_C34488,G1875_C34489,G1875_C34645,G1875_C34646,G1875_C34490,G1875_C34491,G1875_C34492,G1875_C34493,G1875_C34636,G1875_C34637,G1875_C34638,G1875_C34496,G1875_C34494,G1875_C34495,G1875_C34501,G1875_C34502,G1875_C34570 FROM '.$BaseDatos.'.G1875 WHERE G1875_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1875_C34468'] = $key->G1875_C34468;

                $datos[$i]['G1875_C34469'] = $key->G1875_C34469;

                $datos[$i]['G1875_C34470'] = $key->G1875_C34470;

                $datos[$i]['G1875_C34457'] = $key->G1875_C34457;

                $datos[$i]['G1875_C34458'] = $key->G1875_C34458;

                $datos[$i]['G1875_C34459'] = explode(' ', $key->G1875_C34459)[0];
  
                $hora = '';
                if(!is_null($key->G1875_C34460)){
                    $hora = explode(' ', $key->G1875_C34460)[1];
                }

                $datos[$i]['G1875_C34460'] = $hora;

                $datos[$i]['G1875_C34461'] = $key->G1875_C34461;

                $datos[$i]['G1875_C34462'] = $key->G1875_C34462;

                $datos[$i]['G1875_C34463'] = $key->G1875_C34463;

                $datos[$i]['G1875_C34464'] = $key->G1875_C34464;

                $datos[$i]['G1875_C34465'] = $key->G1875_C34465;

                $datos[$i]['G1875_C34466'] = $key->G1875_C34466;

                $datos[$i]['G1875_C34467'] = $key->G1875_C34467;

                $datos[$i]['G1875_C34471'] = $key->G1875_C34471;

                $datos[$i]['G1875_C34472'] = $key->G1875_C34472;

                $datos[$i]['G1875_C34473'] = $key->G1875_C34473;

                $datos[$i]['G1875_C34474'] = $key->G1875_C34474;

                $datos[$i]['G1875_C34475'] = $key->G1875_C34475;

                $datos[$i]['G1875_C34639'] = $key->G1875_C34639;

                $datos[$i]['G1875_C34640'] = $key->G1875_C34640;

                $datos[$i]['G1875_C34641'] = $key->G1875_C34641;

                $datos[$i]['G1875_C34642'] = $key->G1875_C34642;

                $datos[$i]['G1875_C34476'] = $key->G1875_C34476;

                $datos[$i]['G1875_C34643'] = $key->G1875_C34643;

                $datos[$i]['G1875_C34477'] = $key->G1875_C34477;

                $datos[$i]['G1875_C34480'] = $key->G1875_C34480;

                $datos[$i]['G1875_C34478'] = $key->G1875_C34478;

                $datos[$i]['G1875_C34479'] = $key->G1875_C34479;

                $datos[$i]['G1875_C34481'] = $key->G1875_C34481;

                $datos[$i]['G1875_C34486'] = $key->G1875_C34486;

                $datos[$i]['G1875_C34482'] = $key->G1875_C34482;

                $datos[$i]['G1875_C34485'] = $key->G1875_C34485;

                $datos[$i]['G1875_C34483'] = $key->G1875_C34483;

                $datos[$i]['G1875_C34484'] = $key->G1875_C34484;

                $datos[$i]['G1875_C34487'] = $key->G1875_C34487;

                $datos[$i]['G1875_C34488'] = $key->G1875_C34488;

                $datos[$i]['G1875_C34489'] = $key->G1875_C34489;

                $datos[$i]['G1875_C34645'] = $key->G1875_C34645;

                $datos[$i]['G1875_C34646'] = $key->G1875_C34646;

                $datos[$i]['G1875_C34490'] = $key->G1875_C34490;

                $datos[$i]['G1875_C34491'] = $key->G1875_C34491;

                $datos[$i]['G1875_C34492'] = $key->G1875_C34492;

                $datos[$i]['G1875_C34493'] = $key->G1875_C34493;

                $datos[$i]['G1875_C34636'] = $key->G1875_C34636;

                $datos[$i]['G1875_C34637'] = $key->G1875_C34637;

                $datos[$i]['G1875_C34638'] = $key->G1875_C34638;

                $datos[$i]['G1875_C34496'] = $key->G1875_C34496;

                $datos[$i]['G1875_C34494'] = explode(' ', $key->G1875_C34494)[0];
  
                $hora = '';
                if(!is_null($key->G1875_C34495)){
                    $hora = explode(' ', $key->G1875_C34495)[1];
                }

                $datos[$i]['G1875_C34495'] = $hora;

                $datos[$i]['G1875_C34501'] = $key->G1875_C34501;

                $datos[$i]['G1875_C34502'] = $key->G1875_C34502;

                $datos[$i]['G1875_C34570'] = $key->G1875_C34570;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1875";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1875_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1875_ConsInte__b as id,  G1875_C34468 as camp1 , b.LISOPC_Nombre____b as camp2 
                     FROM ".$BaseDatos.".G1875  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1875_C34469 WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1875_ConsInte__b as id,  G1875_C34468 as camp1 , b.LISOPC_Nombre____b as camp2  
                    FROM ".$BaseDatos.".G1875  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1875_C34469 JOIN ".$BaseDatos.".G1875_M".$_POST['muestra']." ON G1875_ConsInte__b = G1875_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1875_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1875_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1875_C34468 LIKE '%".$B."%' OR G1875_C34469 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1875_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1875");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1875_ConsInte__b, G1875_FechaInsercion , G1875_Usuario ,  G1875_CodigoMiembro  , G1875_PoblacionOrigen , G1875_EstadoDiligenciamiento ,  G1875_IdLlamada , G1875_C34468 as principal ,G1875_C34468, a.LISOPC_Nombre____b as G1875_C34469,G1875_C34470, b.LISOPC_Nombre____b as G1875_C34457, c.LISOPC_Nombre____b as G1875_C34458,G1875_C34459,G1875_C34460,G1875_C34461,G1875_C34462,G1875_C34463,G1875_C34464,G1875_C34465,G1875_C34466,G1875_C34467,G1875_C34471,G1875_C34472,G1875_C34473,G1875_C34474,G1875_C34475,G1875_C34639,G1875_C34640,G1875_C34641,G1875_C34642, d.LISOPC_Nombre____b as G1875_C34476,G1875_C34643,G1875_C34477,G1875_C34480,G1875_C34478,G1875_C34479,G1875_C34481,G1875_C34486,G1875_C34482,G1875_C34485,G1875_C34483,G1875_C34484,G1875_C34487,G1875_C34488,G1875_C34489, e.LISOPC_Nombre____b as G1875_C34645, f.LISOPC_Nombre____b as G1875_C34646,G1875_C34490,G1875_C34491,G1875_C34492,G1875_C34493,G1875_C34636,G1875_C34637,G1875_C34638,G1875_C34496,G1875_C34494,G1875_C34495, g.LISOPC_Nombre____b as G1875_C34501, h.LISOPC_Nombre____b as G1875_C34502,G1875_C34570 FROM '.$BaseDatos.'.G1875 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1875_C34469 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1875_C34457 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1875_C34458 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1875_C34476 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1875_C34645 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1875_C34646 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G1875_C34501 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G1875_C34502';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1875_C34460)){
                    $hora_a = explode(' ', $fila->G1875_C34460)[1];
                }

                $hora_b = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1875_C34495)){
                    $hora_b = explode(' ', $fila->G1875_C34495)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1875_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1875_ConsInte__b , ($fila->G1875_C34468) , ($fila->G1875_C34469) , ($fila->G1875_C34470) , ($fila->G1875_C34457) , ($fila->G1875_C34458) , explode(' ', $fila->G1875_C34459)[0] , $hora_a , ($fila->G1875_C34461) , ($fila->G1875_C34462) , ($fila->G1875_C34463) , ($fila->G1875_C34464) , ($fila->G1875_C34465) , ($fila->G1875_C34466) , ($fila->G1875_C34467) , ($fila->G1875_C34471) , ($fila->G1875_C34472) , ($fila->G1875_C34473) , ($fila->G1875_C34474) , ($fila->G1875_C34475) , ($fila->G1875_C34639) , ($fila->G1875_C34640) , ($fila->G1875_C34641) , ($fila->G1875_C34642) , ($fila->G1875_C34476) , ($fila->G1875_C34643) , ($fila->G1875_C34477) , ($fila->G1875_C34480) , ($fila->G1875_C34478) , ($fila->G1875_C34479) , ($fila->G1875_C34481) , ($fila->G1875_C34486) , ($fila->G1875_C34482) , ($fila->G1875_C34485) , ($fila->G1875_C34483) , ($fila->G1875_C34484) , ($fila->G1875_C34487) , ($fila->G1875_C34488) , ($fila->G1875_C34489) , ($fila->G1875_C34645) , ($fila->G1875_C34646) , ($fila->G1875_C34490) , ($fila->G1875_C34491) , ($fila->G1875_C34492) , ($fila->G1875_C34493) , ($fila->G1875_C34636) , ($fila->G1875_C34637) , ($fila->G1875_C34638) , ($fila->G1875_C34496) , explode(' ', $fila->G1875_C34494)[0] , $hora_b , ($fila->G1875_C34501) , ($fila->G1875_C34502) , ($fila->G1875_C34570) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1875 WHERE G1875_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1875";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1875_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1875_ConsInte__b as id,  G1875_C34468 as camp1 , b.LISOPC_Nombre____b as camp2  FROM '.$BaseDatos.'.G1875 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1875_ConsInte__b as id,  G1875_C34468 as camp1 , b.LISOPC_Nombre____b as camp2  
                    FROM ".$BaseDatos.".G1875  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1875_C34469 JOIN ".$BaseDatos.".G1875_M".$_POST['muestra']." ON G1875_ConsInte__b = G1875_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1875_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1875_C34468 LIKE "%'.$B.'%" OR G1875_C34469 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1875_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1875 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1875(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1875_C34468"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34468 = '".$_POST["G1875_C34468"]."'";
                $LsqlI .= $separador."G1875_C34468";
                $LsqlV .= $separador."'".$_POST["G1875_C34468"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34469"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34469 = '".$_POST["G1875_C34469"]."'";
                $LsqlI .= $separador."G1875_C34469";
                $LsqlV .= $separador."'".$_POST["G1875_C34469"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34470"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34470 = '".$_POST["G1875_C34470"]."'";
                $LsqlI .= $separador."G1875_C34470";
                $LsqlV .= $separador."'".$_POST["G1875_C34470"]."'";
                $validar = 1;
            }
             
 
            $G1875_C34457 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1875_C34457 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1875_C34457 = ".$G1875_C34457;
                    $LsqlI .= $separador." G1875_C34457";
                    $LsqlV .= $separador.$G1875_C34457;
                    $validar = 1;

                    
                }
            }
 
            $G1875_C34458 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1875_C34458 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1875_C34458 = ".$G1875_C34458;
                    $LsqlI .= $separador." G1875_C34458";
                    $LsqlV .= $separador.$G1875_C34458;
                    $validar = 1;
                }
            }
 
            $G1875_C34459 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1875_C34459 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1875_C34459 = ".$G1875_C34459;
                    $LsqlI .= $separador." G1875_C34459";
                    $LsqlV .= $separador.$G1875_C34459;
                    $validar = 1;
                }
            }
 
            $G1875_C34460 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1875_C34460 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1875_C34460 = ".$G1875_C34460;
                    $LsqlI .= $separador." G1875_C34460";
                    $LsqlV .= $separador.$G1875_C34460;
                    $validar = 1;
                }
            }
 
            $G1875_C34461 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1875_C34461 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1875_C34461 = ".$G1875_C34461;
                    $LsqlI .= $separador." G1875_C34461";
                    $LsqlV .= $separador.$G1875_C34461;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1875_C34462"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34462 = '".$_POST["G1875_C34462"]."'";
                $LsqlI .= $separador."G1875_C34462";
                $LsqlV .= $separador."'".$_POST["G1875_C34462"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34463"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34463 = '".$_POST["G1875_C34463"]."'";
                $LsqlI .= $separador."G1875_C34463";
                $LsqlV .= $separador."'".$_POST["G1875_C34463"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34464"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34464 = '".$_POST["G1875_C34464"]."'";
                $LsqlI .= $separador."G1875_C34464";
                $LsqlV .= $separador."'".$_POST["G1875_C34464"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34465"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34465 = '".$_POST["G1875_C34465"]."'";
                $LsqlI .= $separador."G1875_C34465";
                $LsqlV .= $separador."'".$_POST["G1875_C34465"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34466"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34466 = '".$_POST["G1875_C34466"]."'";
                $LsqlI .= $separador."G1875_C34466";
                $LsqlV .= $separador."'".$_POST["G1875_C34466"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34467"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34467 = '".$_POST["G1875_C34467"]."'";
                $LsqlI .= $separador."G1875_C34467";
                $LsqlV .= $separador."'".$_POST["G1875_C34467"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34471"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34471 = '".$_POST["G1875_C34471"]."'";
                $LsqlI .= $separador."G1875_C34471";
                $LsqlV .= $separador."'".$_POST["G1875_C34471"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34472"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34472 = '".$_POST["G1875_C34472"]."'";
                $LsqlI .= $separador."G1875_C34472";
                $LsqlV .= $separador."'".$_POST["G1875_C34472"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34473"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34473 = '".$_POST["G1875_C34473"]."'";
                $LsqlI .= $separador."G1875_C34473";
                $LsqlV .= $separador."'".$_POST["G1875_C34473"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34474"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34474 = '".$_POST["G1875_C34474"]."'";
                $LsqlI .= $separador."G1875_C34474";
                $LsqlV .= $separador."'".$_POST["G1875_C34474"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34475"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34475 = '".$_POST["G1875_C34475"]."'";
                $LsqlI .= $separador."G1875_C34475";
                $LsqlV .= $separador."'".$_POST["G1875_C34475"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34639"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34639 = '".$_POST["G1875_C34639"]."'";
                $LsqlI .= $separador."G1875_C34639";
                $LsqlV .= $separador."'".$_POST["G1875_C34639"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34640"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34640 = '".$_POST["G1875_C34640"]."'";
                $LsqlI .= $separador."G1875_C34640";
                $LsqlV .= $separador."'".$_POST["G1875_C34640"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34641"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34641 = '".$_POST["G1875_C34641"]."'";
                $LsqlI .= $separador."G1875_C34641";
                $LsqlV .= $separador."'".$_POST["G1875_C34641"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34642"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34642 = '".$_POST["G1875_C34642"]."'";
                $LsqlI .= $separador."G1875_C34642";
                $LsqlV .= $separador."'".$_POST["G1875_C34642"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34476"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34476 = '".$_POST["G1875_C34476"]."'";
                $LsqlI .= $separador."G1875_C34476";
                $LsqlV .= $separador."'".$_POST["G1875_C34476"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34643"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34643 = '".$_POST["G1875_C34643"]."'";
                $LsqlI .= $separador."G1875_C34643";
                $LsqlV .= $separador."'".$_POST["G1875_C34643"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34477"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34477 = '".$_POST["G1875_C34477"]."'";
                $LsqlI .= $separador."G1875_C34477";
                $LsqlV .= $separador."'".$_POST["G1875_C34477"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34480"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34480 = '".$_POST["G1875_C34480"]."'";
                $LsqlI .= $separador."G1875_C34480";
                $LsqlV .= $separador."'".$_POST["G1875_C34480"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34478"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34478 = '".$_POST["G1875_C34478"]."'";
                $LsqlI .= $separador."G1875_C34478";
                $LsqlV .= $separador."'".$_POST["G1875_C34478"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34479"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34479 = '".$_POST["G1875_C34479"]."'";
                $LsqlI .= $separador."G1875_C34479";
                $LsqlV .= $separador."'".$_POST["G1875_C34479"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34481"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34481 = '".$_POST["G1875_C34481"]."'";
                $LsqlI .= $separador."G1875_C34481";
                $LsqlV .= $separador."'".$_POST["G1875_C34481"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34486"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34486 = '".$_POST["G1875_C34486"]."'";
                $LsqlI .= $separador."G1875_C34486";
                $LsqlV .= $separador."'".$_POST["G1875_C34486"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34482"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34482 = '".$_POST["G1875_C34482"]."'";
                $LsqlI .= $separador."G1875_C34482";
                $LsqlV .= $separador."'".$_POST["G1875_C34482"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34485"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34485 = '".$_POST["G1875_C34485"]."'";
                $LsqlI .= $separador."G1875_C34485";
                $LsqlV .= $separador."'".$_POST["G1875_C34485"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34483"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34483 = '".$_POST["G1875_C34483"]."'";
                $LsqlI .= $separador."G1875_C34483";
                $LsqlV .= $separador."'".$_POST["G1875_C34483"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34484"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34484 = '".$_POST["G1875_C34484"]."'";
                $LsqlI .= $separador."G1875_C34484";
                $LsqlV .= $separador."'".$_POST["G1875_C34484"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34487"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34487 = '".$_POST["G1875_C34487"]."'";
                $LsqlI .= $separador."G1875_C34487";
                $LsqlV .= $separador."'".$_POST["G1875_C34487"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34488"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34488 = '".$_POST["G1875_C34488"]."'";
                $LsqlI .= $separador."G1875_C34488";
                $LsqlV .= $separador."'".$_POST["G1875_C34488"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34489"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34489 = '".$_POST["G1875_C34489"]."'";
                $LsqlI .= $separador."G1875_C34489";
                $LsqlV .= $separador."'".$_POST["G1875_C34489"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34645"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34645 = '".$_POST["G1875_C34645"]."'";
                $LsqlI .= $separador."G1875_C34645";
                $LsqlV .= $separador."'".$_POST["G1875_C34645"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34646"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34646 = '".$_POST["G1875_C34646"]."'";
                $LsqlI .= $separador."G1875_C34646";
                $LsqlV .= $separador."'".$_POST["G1875_C34646"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34490"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34490 = '".$_POST["G1875_C34490"]."'";
                $LsqlI .= $separador."G1875_C34490";
                $LsqlV .= $separador."'".$_POST["G1875_C34490"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34491"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34491 = '".$_POST["G1875_C34491"]."'";
                $LsqlI .= $separador."G1875_C34491";
                $LsqlV .= $separador."'".$_POST["G1875_C34491"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34492"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34492 = '".$_POST["G1875_C34492"]."'";
                $LsqlI .= $separador."G1875_C34492";
                $LsqlV .= $separador."'".$_POST["G1875_C34492"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34493"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34493 = '".$_POST["G1875_C34493"]."'";
                $LsqlI .= $separador."G1875_C34493";
                $LsqlV .= $separador."'".$_POST["G1875_C34493"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34636"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34636 = '".$_POST["G1875_C34636"]."'";
                $LsqlI .= $separador."G1875_C34636";
                $LsqlV .= $separador."'".$_POST["G1875_C34636"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34637"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34637 = '".$_POST["G1875_C34637"]."'";
                $LsqlI .= $separador."G1875_C34637";
                $LsqlV .= $separador."'".$_POST["G1875_C34637"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34638"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34638 = '".$_POST["G1875_C34638"]."'";
                $LsqlI .= $separador."G1875_C34638";
                $LsqlV .= $separador."'".$_POST["G1875_C34638"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34496"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34496 = '".$_POST["G1875_C34496"]."'";
                $LsqlI .= $separador."G1875_C34496";
                $LsqlV .= $separador."'".$_POST["G1875_C34496"]."'";
                $validar = 1;
            }
             
 
            $G1875_C34494 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1875_C34494"])){    
                if($_POST["G1875_C34494"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1875_C34494"]);
                    if(count($tieneHora) > 1){
                        $G1875_C34494 = "'".$_POST["G1875_C34494"]."'";
                    }else{
                        $G1875_C34494 = "'".str_replace(' ', '',$_POST["G1875_C34494"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1875_C34494 = ".$G1875_C34494;
                    $LsqlI .= $separador." G1875_C34494";
                    $LsqlV .= $separador.$G1875_C34494;
                    $validar = 1;
                }
            }
  
            $G1875_C34495 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1875_C34495"])){   
                if($_POST["G1875_C34495"] != '' && $_POST["G1875_C34495"] != 'undefined' && $_POST["G1875_C34495"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1875_C34495 = "'".$fecha." ".str_replace(' ', '',$_POST["G1875_C34495"])."'";
                    $LsqlU .= $separador." G1875_C34495 = ".$G1875_C34495."";
                    $LsqlI .= $separador." G1875_C34495";
                    $LsqlV .= $separador.$G1875_C34495;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1875_C34501"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34501 = '".$_POST["G1875_C34501"]."'";
                $LsqlI .= $separador."G1875_C34501";
                $LsqlV .= $separador."'".$_POST["G1875_C34501"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34502"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34502 = '".$_POST["G1875_C34502"]."'";
                $LsqlI .= $separador."G1875_C34502";
                $LsqlV .= $separador."'".$_POST["G1875_C34502"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1875_C34570"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_C34570 = '".$_POST["G1875_C34570"]."'";
                $LsqlI .= $separador."G1875_C34570";
                $LsqlV .= $separador."'".$_POST["G1875_C34570"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1875_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G1875_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1875_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1875_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1875_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1875_Usuario , G1875_FechaInsercion, G1875_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1875_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1875 WHERE G1875_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }else{
                            $UltimoID = "1";
                        }        
                    }

                    if (isset($_POST["G1875_C34496"])) {
                        $strSQLInsertCita_t = "INSERT INTO ".$BaseDatos.".G1874 (G1874_FechaInsercion,G1874_C34453";
                        $strSQLValue_t = " VALUES (NOW(),".$_POST["G1875_C34496"];

                        if (isset($_POST["G1875_C34494"]) && $_POST["G1875_C34494"] != "") {

                            $strSQLInsertCita_t .= ",G1874_C34454";
                            $strSQLValue_t .= ",'".$_POST["G1875_C34494"]."'";

                        }

                        if (isset($_POST["G1875_C34495"]) && $_POST["G1875_C34495"] != "") {

                            $strHora_t = "'".date("Y-m-d")." ".str_replace(' ', '',$_POST["G1875_C34495"])."'";

                            $strSQLInsertCita_t .= ",G1874_C34455";
                            $strSQLValue_t .= ",".$strHora_t;
                        }

                        $strSQLInsertCita_t .= ")";

                        $strSQLValue_t .= ")";

                        $strSQLInsertCita_t .= $strSQLValue_t;

                        $mysqli->query($strSQLInsertCita_t);
                    }



                    echo $UltimoID;

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }           

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1874_ConsInte__b, a.LISOPC_Nombre____b as  G1874_C34453, G1874_C34454, G1874_C34455, G1874_C34456 FROM ".$BaseDatos.".G1874  LEFT JOIN ".$BaseDatos_systema.".LISOPC as a ON a.LISOPC_ConsInte__b =  G1874_C34453 ";

        $SQL .= " WHERE G1874_C34453 = '".$numero."'"; 

        $SQL .= " ORDER BY G1874_C34453";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1874_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1874_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G1874_C34453)."</cell>";

                if($fila->G1874_C34454 != ''){
                    echo "<cell>". explode(' ', $fila->G1874_C34454)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                if($fila->G1874_C34455 != ''){
                    echo "<cell>". explode(' ', $fila->G1874_C34455)[1]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G1874_C34456)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1874 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1874(";
            $LsqlV = " VALUES ("; 

                $G1874_C34454 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G1874_C34454"])){    
                    if($_POST["G1874_C34454"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G1874_C34454 = "'".str_replace(' ', '',$_POST["G1874_C34454"])." 00:00:00'";
                        $LsqlU .= $separador." G1874_C34454 = ".$G1874_C34454;
                        $LsqlI .= $separador." G1874_C34454";
                        $LsqlV .= $separador.$G1874_C34454;
                        $validar = 1;
                    }
                }
 
                $G1874_C34455 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
                if(isset($_POST["G1874_C34455"])){    
                    if($_POST["G1874_C34455"] != '' && $_POST["G1874_C34455"] != 'undefined' && $_POST["G1874_C34455"] != 'null'){
                        $separador = "";
                        $fecha = date('Y-m-d');
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G1874_C34455 = "'".$fecha." ".str_replace(' ', '',$_POST["G1874_C34455"])."'";
                        $LsqlU .= $separador."  G1874_C34455 = ".$G1874_C34455."";
                        $LsqlI .= $separador."  G1874_C34455";
                        $LsqlV .= $separador.$G1874_C34455;
                        $validar = 1;
                    }
                }
 

                if(isset($_POST["G1874_C34456"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1874_C34456 = '".$_POST["G1874_C34456"]."'";
                    $LsqlI .= $separador."G1874_C34456";
                    $LsqlV .= $separador."'".$_POST["G1874_C34456"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1874_C34453 = $numero;
                    $LsqlU .= ", G1874_C34453 = ".$G1874_C34453."";
                    $LsqlI .= ", G1874_C34453";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1874_Usuario ,  G1874_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1874_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1874 WHERE  G1874_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
