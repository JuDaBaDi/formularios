<?php 
    /*
        Document   : index
        Created on : 2020-06-12 14:42:09
        Author     : Nicolas y su poderoso generador, La gloria sea Para DIOS 
        Url 	   : id = MTg3NQ==  
    */
    $url_crud =  "formularios/G1875/G1875_CRUD_web.php";
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulario de contacto</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
    
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link rel="shortcut icon" href="assets/img/logo_dyalogo_mail.png"/>
        <style>
            
            .hed{
           
                font-family: 'Sansita One', cursive; 
                color:white;
            }

            .hed2{
                text-align:center;
                font-family: 'Sansita One', cursive; 
                font-size:25px; 
                color:#019CDE;
                margin-top: -9px;
            }
            .font_2 {
                font: normal normal normal 17px/1.4em Spinnaker,sans-serif;
                text-align:center;
            }
            
            .redonder {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                -webkit-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
            }

            [class^='select2'] {
                border-radius: 0px !important;
            }
        </style>
    </head>
    <?php  
        echo '<body class="hold-transition" >';
    ?>
    
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" >
                <div class="login-box">
                    <div class="login-logo hed">
                        <img src="assets/img/logo_dyalogo_mail.png"  alt="Dyalogo">
                    </div><!-- /.login-logo -->
                    <div class="login-box-body">
                        <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
                        <form action="formularios/G1875/G1875_CRUD_web.php" method="post" id="formLogin">
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34468" id="LblG1875_C34468">Nombres y Apellidos</label>
								<input type="text" class="form-control input-sm" id="G1875_C34468" value=""  name="G1875_C34468"  placeholder="Nombres y Apellidos">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1875_C34469" id="LblG1875_C34469">¿Qué Persona se Está Comunicando?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1875_C34469" id="G1875_C34469">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1924 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34470" id="LblG1875_C34470">Número de contacto</label>
								<input type="text" class="form-control input-sm" id="G1875_C34470" value=""  name="G1875_C34470"  placeholder="Número de contacto">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34466" id="LblG1875_C34466">Número de Identificación del estudiante</label>
								<input type="text" class="form-control input-sm" id="G1875_C34466" value=""  name="G1875_C34466"  placeholder="Número de Identificación del estudiante">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34467" id="LblG1875_C34467">¿En qué grado se encuentra el estudiante?</label>
								<input type="text" class="form-control input-sm" id="G1875_C34467" value=""  name="G1875_C34467"  placeholder="¿En qué grado se encuentra el estudiante?">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34471" id="LblG1875_C34471">Nombres y Apellidos del Estudiante que Representa</label>
								<input type="text" class="form-control input-sm" id="G1875_C34471" value=""  name="G1875_C34471"  placeholder="Nombres y Apellidos del Estudiante que Representa">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34472" id="LblG1875_C34472">¿En qué colegio se encuentra el estudiante?</label>
								<input type="text" class="form-control input-sm" id="G1875_C34472" value=""  name="G1875_C34472"  placeholder="¿En qué colegio se encuentra el estudiante?">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34473" id="LblG1875_C34473">Número Celular de familiar</label>
								<input type="text" class="form-control input-sm" id="G1875_C34473" value=""  name="G1875_C34473"  placeholder="Número Celular de familiar">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34474" id="LblG1875_C34474">Teléfono Adicional de familiar</label>
								<input type="text" class="form-control input-sm" id="G1875_C34474" value=""  name="G1875_C34474"  placeholder="Teléfono Adicional de familiar">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34475" id="LblG1875_C34475">Correo Electrónico de familiar</label>
								<input type="text" class="form-control input-sm" id="G1875_C34475" value=""  name="G1875_C34475"  placeholder="Correo Electrónico de familiar">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34639" id="LblG1875_C34639">Edad del estudiante</label>
								<input type="text" class="form-control input-sm" id="G1875_C34639" value=""  name="G1875_C34639"  placeholder="Edad del estudiante">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34640" id="LblG1875_C34640">Ciudad</label>
								<input type="text" class="form-control input-sm" id="G1875_C34640" value=""  name="G1875_C34640"  placeholder="Ciudad">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34641" id="LblG1875_C34641">Localidad</label>
								<input type="text" class="form-control input-sm" id="G1875_C34641" value=""  name="G1875_C34641"  placeholder="Localidad">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34642" id="LblG1875_C34642">Barrio</label>
								<input type="text" class="form-control input-sm" id="G1875_C34642" value=""  name="G1875_C34642"  placeholder="Barrio">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1875_C34476" id="LblG1875_C34476">Tiene autorización o está acompañado de un adulto </label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1875_C34476" id="G1875_C34476">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1925 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34643" id="LblG1875_C34643">Edad</label>
								<input type="text" class="form-control input-sm" id="G1875_C34643" value=""  name="G1875_C34643"  placeholder="Edad">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34477" id="LblG1875_C34477">Colegio Donde Estudia</label>
								<input type="text" class="form-control input-sm" id="G1875_C34477" value=""  name="G1875_C34477"  placeholder="Colegio Donde Estudia">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34480" id="LblG1875_C34480">Grado Que Está Cursando</label>
								<input type="text" class="form-control input-sm" id="G1875_C34480" value=""  name="G1875_C34480"  placeholder="Grado Que Está Cursando">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34478" id="LblG1875_C34478">Ciudad del Estudiante</label>
								<input type="text" class="form-control input-sm" id="G1875_C34478" value=""  name="G1875_C34478"  placeholder="Ciudad del Estudiante">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34479" id="LblG1875_C34479">Barrio del Estudiante</label>
								<input type="text" class="form-control input-sm" id="G1875_C34479" value=""  name="G1875_C34479"  placeholder="Barrio del Estudiante">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34481" id="LblG1875_C34481">Número de Identificación.</label>
								<input type="text" class="form-control input-sm" id="G1875_C34481" value=""  name="G1875_C34481"  placeholder="Número de Identificación.">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34486" id="LblG1875_C34486">Barrio.</label>
								<input type="text" class="form-control input-sm" id="G1875_C34486" value=""  name="G1875_C34486"  placeholder="Barrio.">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34482" id="LblG1875_C34482">Colegio donde trabaja.</label>
								<input type="text" class="form-control input-sm" id="G1875_C34482" value=""  name="G1875_C34482"  placeholder="Colegio donde trabaja.">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34485" id="LblG1875_C34485">Ciudad.</label>
								<input type="text" class="form-control input-sm" id="G1875_C34485" value=""  name="G1875_C34485"  placeholder="Ciudad.">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34483" id="LblG1875_C34483">Nivel que está enseñando este año.</label>
								<input type="text" class="form-control input-sm" id="G1875_C34483" value=""  name="G1875_C34483"  placeholder="Nivel que está enseñando este año.">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34484" id="LblG1875_C34484">Área que está enseñando este año.</label>
								<input type="text" class="form-control input-sm" id="G1875_C34484" value=""  name="G1875_C34484"  placeholder="Área que está enseñando este año.">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34487" id="LblG1875_C34487">Número Celular.</label>
								<input type="text" class="form-control input-sm" id="G1875_C34487" value=""  name="G1875_C34487"  placeholder="Número Celular.">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34488" id="LblG1875_C34488">Teléfono Adicional.</label>
								<input type="text" class="form-control input-sm" id="G1875_C34488" value=""  name="G1875_C34488"  placeholder="Teléfono Adicional.">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34489" id="LblG1875_C34489">Correo Electrónico.</label>
								<input type="text" class="form-control input-sm" id="G1875_C34489" value=""  name="G1875_C34489"  placeholder="Correo Electrónico.">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1875_C34645" id="LblG1875_C34645">¿Es Docente de un Establecimiento Educativo Oficial?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1875_C34645" id="G1875_C34645">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1925 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1875_C34646" id="LblG1875_C34646">¿Tiene nombramiento en propiedad o provisional?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1875_C34646" id="G1875_C34646">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1925 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34490" id="LblG1875_C34490">Número de Identificación de Otro</label>
								<input type="text" class="form-control input-sm" id="G1875_C34490" value=""  name="G1875_C34490"  placeholder="Número de Identificación de Otro">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34491" id="LblG1875_C34491">Correo Electrónico de Otro</label>
								<input type="text" class="form-control input-sm" id="G1875_C34491" value=""  name="G1875_C34491"  placeholder="Correo Electrónico de Otro">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34492" id="LblG1875_C34492">Número Celular de Otro</label>
								<input type="text" class="form-control input-sm" id="G1875_C34492" value=""  name="G1875_C34492"  placeholder="Número Celular de Otro">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34493" id="LblG1875_C34493">Teléfono Adicional de Otro</label>
								<input type="text" class="form-control input-sm" id="G1875_C34493" value=""  name="G1875_C34493"  placeholder="Teléfono Adicional de Otro">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34636" id="LblG1875_C34636">Ciudad de Otro</label>
								<input type="text" class="form-control input-sm" id="G1875_C34636" value=""  name="G1875_C34636"  placeholder="Ciudad de Otro">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34637" id="LblG1875_C34637">Localidad de Otro</label>
								<input type="text" class="form-control input-sm" id="G1875_C34637" value=""  name="G1875_C34637"  placeholder="Localidad de Otro">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1875_C34638" id="LblG1875_C34638">Barrio de Otro</label>
								<input type="text" class="form-control input-sm" id="G1875_C34638" value=""  name="G1875_C34638"  placeholder="Barrio de Otro">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1875_C34501" id="LblG1875_C34501">TIPIFICACION</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1875_C34501" id="G1875_C34501">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1929 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1875_C34502" id="LblG1875_C34502">SUBTIPIFICACION</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1875_C34502" id="G1875_C34502">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1930 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="G1875_C34570" id="LblG1875_C34570">OBSERVACIONES AGENTE</label>
                                <textarea class="form-control input-sm" name="G1875_C34570" id="G1875_C34570"  value="" placeholder="OBSERVACIONES AGENTE"></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->

                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1875_C34496" id="LblG1875_C34496">CONSULTOR</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1875_C34496" id="G1875_C34496">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1927 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1875_C34496" id="respuesta_LblG1875_C34496">Respuesta</label>
                        <textarea id="respuesta_G1875_C34496" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1875_C34494" id="LblG1875_C34494">FECHA DE CITA</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G1875_C34494" id="G1875_C34494" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIMEPICKER -->
                            <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1875_C34495" id="LblG1875_C34495">HORA DE LA CITA</label>
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm Hora"  name="G1875_C34495" id="G1875_C34495" placeholder="HH:MM:SS" >
                                    <div class="input-group-addon" id="TMP_G1875_C34495">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->

                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="id" id="hidId" value='<?php if(isset($_GET['u'])){ echo $_GET['u']; }else{ echo "0"; } ?>'>
                            <input type="hidden" name="oper" id="oper" value='add'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formpadre" id="formpadre" value='<?php if(isset($_GET['formularioPadre'])){ echo $_GET['formularioPadre']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formhijo" id="formhijo" value='<?php if(isset($_GET['formulario'])){ echo $_GET['formulario']; }else{ echo "0"; }?>' >
                            <input type= "hidden" name="campana" id="campana" value="<?php if(isset($_GET['camp'])){ echo base64_decode($_GET['camp']); }else{ echo "0"; }?>">
                            <div class="row">
                                <div class="col-xs-2">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-xs-8">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar datos</button>
                                </div><!-- /.col -->
                            </div>
                        </form>
                    </div><!-- /.login-box-body -->
                </div><!-- /.login-box -->
            </div>
        </div>


        <!-- jQuery 2.2.3 -->
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>  
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/numeric.js"></script>

        <script type="text/javascript" src="formularios/G1875/G1875_eventos.js"></script>
        <script type="text/javascript">
            $.validator.setDefaults({
                submitHandler: function() { 
                     $("#formLogin").submit();
                }
            });

            $(function(){

                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };

                //str_Select2 estos son los guiones
                


                //datepickers
                

            $("#G1875_C34459").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G1875_C34494").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

                //Timepickers
                


            //Timepicker
            $("#G1875_C34460").timepicker({
                'timeFormat': 'H:i:s',
                'minTime': '08:00:00',
                'maxTime': '17:00:00',
                'setTime': '08:00:00',
                'step'  : '5',
                'showDuration': true
            });

            //Timepicker
            $("#G1875_C34495").timepicker({
                'timeFormat': 'H:i:s',
                'minTime': '08:00:00',
                'maxTime': '17:00:00',
                'setTime': '08:00:00',
                'step'  : '5',
                'showDuration': true
            });

                //Validaciones numeros Enteros
                


                //Validaciones numeros Decimales
               


               //Si tiene dependencias
               


    //function para ¿Qué Persona se Está Comunicando? 

    $("#G1875_C34469").change(function(){ 
            $("#G1875_C34457").prop('disabled', false);
        
            $("#G1875_C34458").prop('disabled', false);
        
            $("#G1875_C34459").prop('disabled', false);
        
            $("#G1875_C34460").prop('disabled', false);
        
            $("#G1875_C34461").prop('disabled', false);
        
            if($(this).val() == '169807'){
            $("#G1875_C34476").prop('disabled', true); 
          
            $("#G1875_C34477").prop('disabled', true); 
          
            $("#G1875_C34478").prop('disabled', true); 
          
            $("#G1875_C34479").prop('disabled', true); 
          
            $("#G1875_C34480").prop('disabled', true); 
          
            $("#G1875_C34481").prop('disabled', true); 
          
            $("#G1875_C34482").prop('disabled', true); 
          
            $("#G1875_C34483").prop('disabled', true); 
          
            $("#G1875_C34484").prop('disabled', true); 
          
            $("#G1875_C34485").prop('disabled', true); 
          
            $("#G1875_C34486").prop('disabled', true); 
          
            $("#G1875_C34487").prop('disabled', true); 
          
            $("#G1875_C34488").prop('disabled', true); 
          
            $("#G1875_C34489").prop('disabled', true); 
          
            $("#G1875_C34490").prop('disabled', true); 
          
            $("#G1875_C34491").prop('disabled', true); 
          
            $("#G1875_C34492").prop('disabled', true); 
          
            $("#G1875_C34493").prop('disabled', true); 
          
            }
        
            if($(this).val() == '169808'){
            $("#G1875_C34466").prop('disabled', true); 
          
            $("#G1875_C34467").prop('disabled', true); 
          
            $("#G1875_C34471").prop('disabled', true); 
          
            $("#G1875_C34472").prop('disabled', true); 
          
            $("#G1875_C34473").prop('disabled', true); 
          
            $("#G1875_C34474").prop('disabled', true); 
          
            $("#G1875_C34475").prop('disabled', true); 
          
            $("#G1875_C34481").prop('disabled', true); 
          
            $("#G1875_C34482").prop('disabled', true); 
          
            $("#G1875_C34483").prop('disabled', true); 
          
            $("#G1875_C34484").prop('disabled', true); 
          
            $("#G1875_C34485").prop('disabled', true); 
          
            $("#G1875_C34486").prop('disabled', true); 
          
            $("#G1875_C34487").prop('disabled', true); 
          
            $("#G1875_C34488").prop('disabled', true); 
          
            $("#G1875_C34489").prop('disabled', true); 
          
            $("#G1875_C34490").prop('disabled', true); 
          
            $("#G1875_C34491").prop('disabled', true); 
          
            $("#G1875_C34492").prop('disabled', true); 
          
            $("#G1875_C34493").prop('disabled', true); 
          
            }
        
            if($(this).val() == '169809'){
            $("#G1875_C34466").prop('disabled', true); 
          
            $("#G1875_C34467").prop('disabled', true); 
          
            $("#G1875_C34471").prop('disabled', true); 
          
            $("#G1875_C34472").prop('disabled', true); 
          
            $("#G1875_C34473").prop('disabled', true); 
          
            $("#G1875_C34474").prop('disabled', true); 
          
            $("#G1875_C34475").prop('disabled', true); 
          
            $("#G1875_C34476").prop('disabled', true); 
          
            $("#G1875_C34477").prop('disabled', true); 
          
            $("#G1875_C34478").prop('disabled', true); 
          
            $("#G1875_C34479").prop('disabled', true); 
          
            $("#G1875_C34480").prop('disabled', true); 
          
            $("#G1875_C34490").prop('disabled', true); 
          
            $("#G1875_C34491").prop('disabled', true); 
          
            $("#G1875_C34492").prop('disabled', true); 
          
            $("#G1875_C34493").prop('disabled', true); 
          
            }
        
            if($(this).val() == '169810'){
            $("#G1875_C34466").prop('disabled', true); 
          
            $("#G1875_C34467").prop('disabled', true); 
          
            $("#G1875_C34471").prop('disabled', true); 
          
            $("#G1875_C34472").prop('disabled', true); 
          
            $("#G1875_C34473").prop('disabled', true); 
          
            $("#G1875_C34474").prop('disabled', true); 
          
            $("#G1875_C34475").prop('disabled', true); 
          
            $("#G1875_C34476").prop('disabled', true); 
          
            $("#G1875_C34477").prop('disabled', true); 
          
            $("#G1875_C34478").prop('disabled', true); 
          
            $("#G1875_C34479").prop('disabled', true); 
          
            $("#G1875_C34480").prop('disabled', true); 
          
            $("#G1875_C34490").prop('disabled', true); 
          
            $("#G1875_C34491").prop('disabled', true); 
          
            $("#G1875_C34492").prop('disabled', true); 
          
            $("#G1875_C34493").prop('disabled', true); 
          
            }
        
            if($(this).val() == '169811'){
            $("#G1875_C34466").prop('disabled', true); 
          
            $("#G1875_C34467").prop('disabled', true); 
          
            $("#G1875_C34471").prop('disabled', true); 
          
            $("#G1875_C34472").prop('disabled', true); 
          
            $("#G1875_C34473").prop('disabled', true); 
          
            $("#G1875_C34474").prop('disabled', true); 
          
            $("#G1875_C34475").prop('disabled', true); 
          
            $("#G1875_C34476").prop('disabled', true); 
          
            $("#G1875_C34477").prop('disabled', true); 
          
            $("#G1875_C34478").prop('disabled', true); 
          
            $("#G1875_C34479").prop('disabled', true); 
          
            $("#G1875_C34480").prop('disabled', true); 
          
            $("#G1875_C34490").prop('disabled', true); 
          
            $("#G1875_C34491").prop('disabled', true); 
          
            $("#G1875_C34492").prop('disabled', true); 
          
            $("#G1875_C34493").prop('disabled', true); 
          
            }
        
            if($(this).val() == '169812'){
            $("#G1875_C34466").prop('disabled', true); 
          
            $("#G1875_C34467").prop('disabled', true); 
          
            $("#G1875_C34471").prop('disabled', true); 
          
            $("#G1875_C34472").prop('disabled', true); 
          
            $("#G1875_C34473").prop('disabled', true); 
          
            $("#G1875_C34474").prop('disabled', true); 
          
            $("#G1875_C34475").prop('disabled', true); 
          
            $("#G1875_C34476").prop('disabled', true); 
          
            $("#G1875_C34477").prop('disabled', true); 
          
            $("#G1875_C34478").prop('disabled', true); 
          
            $("#G1875_C34479").prop('disabled', true); 
          
            $("#G1875_C34480").prop('disabled', true); 
          
            $("#G1875_C34490").prop('disabled', true); 
          
            $("#G1875_C34491").prop('disabled', true); 
          
            $("#G1875_C34492").prop('disabled', true); 
          
            $("#G1875_C34493").prop('disabled', true); 
          
            }
        
            if($(this).val() == '169813'){
            $("#G1875_C34466").prop('disabled', true); 
          
            $("#G1875_C34467").prop('disabled', true); 
          
            $("#G1875_C34471").prop('disabled', true); 
          
            $("#G1875_C34472").prop('disabled', true); 
          
            $("#G1875_C34473").prop('disabled', true); 
          
            $("#G1875_C34474").prop('disabled', true); 
          
            $("#G1875_C34475").prop('disabled', true); 
          
            $("#G1875_C34476").prop('disabled', true); 
          
            $("#G1875_C34477").prop('disabled', true); 
          
            $("#G1875_C34478").prop('disabled', true); 
          
            $("#G1875_C34479").prop('disabled', true); 
          
            $("#G1875_C34480").prop('disabled', true); 
          
            $("#G1875_C34481").prop('disabled', true); 
          
            $("#G1875_C34482").prop('disabled', true); 
          
            $("#G1875_C34483").prop('disabled', true); 
          
            $("#G1875_C34484").prop('disabled', true); 
          
            $("#G1875_C34485").prop('disabled', true); 
          
            $("#G1875_C34486").prop('disabled', true); 
          
            $("#G1875_C34487").prop('disabled', true); 
          
            $("#G1875_C34488").prop('disabled', true); 
          
            $("#G1875_C34489").prop('disabled', true); 
          
            $("#G1875_C34636").prop('disabled', true); 
          
            $("#G1875_C34637").prop('disabled', true); 
          
            $("#G1875_C34638").prop('disabled', true); 
          
            }
         
        //Esto es la parte de las listas dependientes
        

    });

    //function para Tiene autorización o está acompañado de un adulto  

    $("#G1875_C34476").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Es Docente de un Establecimiento Educativo Oficial? 

    $("#G1875_C34645").change(function(){ 
            $("#G1875_C34457").prop('disabled', false);
        
            $("#G1875_C34458").prop('disabled', false);
        
            $("#G1875_C34459").prop('disabled', false);
        
            $("#G1875_C34460").prop('disabled', false);
        
            $("#G1875_C34461").prop('disabled', false);
        
            if($(this).val() == '169814'){
            $("#G1875_C34646").prop('disabled', true); 
          
            }
         
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Tiene nombramiento en propiedad o provisional? 

    $("#G1875_C34646").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPIFICACION 

    $("#G1875_C34501").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '1930' , idPadre : $(this).val() },
            success : function(data){
                $("#G1875_C34502").html(data);
            }
        });
        
    });

    //function para SUBTIPIFICACION 

    $("#G1875_C34502").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para CONSULTOR 

    $("#G1875_C34496").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
                

               <?php
                    if(isset($_GET['result'])){
                        if($_GET['result'] ==  1){
                ?>
                        swal({
                            title: "Exito!",
                            text: "Recibimos su solicitud, pronto estaremos en contacto",
                            type: "success",
                            confirmButtonText: "Ok"
                        },function(){

                        });
                        
                        
                <?php   }else{ ?>
                            swal({
                                title: "Error!",
                                text: 'Ocurrio un error, intenta mas tarde',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }
                    }
                ?>
            });
        </script>
        <Script type="text/javascript">
            $(document).ready(function() {
                <?php
                $campana = base64_decode($_GET['camp']);
                $Guion = 0;//id de la campaña
                $tabla = 0;// $_GET['u'];//ide del usuario
                $Lsql = "SELECT CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConsInte__GUION__Pob_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$campana;

                $result = $mysqli->query($Lsql);
                while($obj = $result->fetch_object()){
                    $Guion = $obj->CAMPAN_ConsInte__GUION__Gui_b;
                    $tabla = $obj->CAMPAN_ConsInte__GUION__Pob_b;
                } 
                //SELECT de la camic
                $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b, CAMINC_ConsInte__CAMPO_Gui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$campana;
                
                $resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    
                    //Pregfuntar por el tipo de dato
                    $Lsql = "SELECT PREGUN_Tipo______b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__b = ".$key->CAMINC_ConsInte__CAMPO_Gui_b;
                    $res = $mysqli->query($Lsql);
                    $datos = $res->fetch_array();


                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['u'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ 
                            if(!is_null($objDatos->campo) && $objDatos->campo != ''){

                                if($datos['PREGUN_Tipo______b'] != '8'){
                            ?>
                                    document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                            <?php  
                                }else{
                                    if($objDatos->campo == '1'){
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , true);";
                                    }else{
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , false);";
                                    }
                                    
                                } 
                            }
                        }
                    }
                    
                }
                ?>
            });
        </script>
        
