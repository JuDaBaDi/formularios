<?php 
    /*
        Document   : index
        Created on : 2018-03-15 18:37:10
        Author     : Jose David y su poderoso generador, La gloria sea Para DIOS 
    */
    $url_crud =  "formularios/G734/G734_CRUD.php";
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulario de contacto</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
    
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link rel="shortcut icon" href="assets/img/logo_dyalogo_mail.png"/>
        <style>
            
            .hed{
           
                font-family: 'Sansita One', cursive; 
                color:white;
            }

            .hed2{
                text-align:center;
                font-family: 'Sansita One', cursive; 
                font-size:25px; 
                color:#019CDE;
                margin-top: -9px;
            }
            .font_2 {
                font: normal normal normal 14px/1.4em Spinnaker,sans-serif;
                color:#3D9BE9;
            }
            
            .redonder {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                -webkit-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
            }

            [class^='select2'] {
                border-radius: 0px !important;
            }
        </style>
    </head>
    <body class="hold-transition" >
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" >
                <div class="login-box">
                    <?php if(!isset($_GET['aceptaTerminos'])){ ?>
                    <div class="login-logo hed">
                        <img src="formularios/G734/1.jpeg"  alt="ISEC" width="100%">
                    </div><!-- /.login-logo -->
                    <div class="login-box-body redonder">
                        <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
                        
                        <form action="formularios/G734/G734_CRUD_web.php" method="post" id="formLogin">

        
 
                            <!-- CAMPO TIPO TEXTO -->
                            <div class="form-group">
                                <label for="G734_C9681" id="LblG734_C9681">Nombre del evento</label>
                                <input type="text" class="form-control input-sm" id="G734_C9681" value=""  name="G734_C9681" required  placeholder="Nombre del evento">
                            </div>
                            <!-- FIN DEL CAMPO TIPO TEXTO -->
 
                            <!-- CAMPO TIPO TEXTO -->
                            <div class="form-group">
                                <label for="G734_C9682" id="LblG734_C9682">Nombre de la empresa</label>
                                <input type="text" class="form-control input-sm" id="G734_C9682" value=""  name="G734_C9682" required  placeholder="Nombre de la empresa">
                            </div>
                            <!-- FIN DEL CAMPO TIPO TEXTO -->
 
                            <!-- CAMPO TIPO TEXTO -->
                            <div class="form-group">
                                <label for="G734_C9683" id="LblG734_C9683">Nombre contacto</label>
                                <input type="text" class="form-control input-sm" id="G734_C9683" value=""  name="G734_C9683" required placeholder="Nombre contacto">
                            </div>
                            <!-- FIN DEL CAMPO TIPO TEXTO -->
 
                            <!-- CAMPO TIPO TEXTO -->
                            <div class="form-group">
                                <label for="G734_C9684" id="LblG734_C9684">Cargo</label>
                                <input type="text" class="form-control input-sm" id="G734_C9684" value=""  name="G734_C9684" required  placeholder="Cargo">
                            </div>
                            <!-- FIN DEL CAMPO TIPO TEXTO -->
 
                            <!-- CAMPO TIPO TEXTO -->
                            <div class="form-group">
                                <label for="G734_C9685" id="LblG734_C9685">Correo electronico</label>
                                <input type="email" class="form-control input-sm" id="G734_C9685" value=""  name="G734_C9685" required  placeholder="Corro electronico">
                            </div>
                            <!-- FIN DEL CAMPO TIPO TEXTO -->
 
                            <!-- CAMPO TIPO ENTERO -->
                            <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G734_C9686" id="LblG734_C9686">Teléfono</label>
                                <input type="text" class="form-control input-sm Numerico" value=""  name="G734_C9686" id="G734_C9686" maxlength="10" required placeholder="Teléfono">
                            </div>
                            <!-- FIN DEL CAMPO TIPO ENTERO -->


                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="id" id="hidId" value='0'>
                            <input type="hidden" name="oper" id="oper" value='add'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formpadre" id="formpadre" value='<?php if(isset($_GET['formularioPadre'])){ echo $_GET['formularioPadre']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formhijo" id="formhijo" value='<?php if(isset($_GET['formulario'])){ echo $_GET['formulario']; }else{ echo "0"; }?>' >

                            <div class="row">
                                <div class="col-xs-2">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-xs-8">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar datos</button>
                                </div><!-- /.col -->
                            </div>
                        </form>
                        
                    </div><!-- /.login-box-body -->
                   
                    <?php 
                        }else{ 
                            echo '<div class="login-logo hed">
                                    <img src="formularios/G734/1.jpeg"  alt="ISEC" width="100%">
                                </div><!-- /.login-logo -->';

                            if($_GET['aceptaTerminos'] == 'acepto'){
                                $numero_form = base64_decode($_GET['cons']);

                                $Lsql = "UPDATE ".$BaseDatos.".G734 SET G734_C9687 = 'ACEPTA' WHERE G734_ConsInte__b =".$numero_form;
                                if ($mysqli->query($Lsql) === TRUE) {
                                    /* Acepto toca meterlo en la muestra  G626_M285*/

                                    $InsertMuestra = "INSERT INTO  ".$BaseDatos.".G626_M285 ( G626_M285_CoInMiPo__b ,  G626_M285_NumeInte__b, G626_M285_Estado____b) VALUES (".$numero_form.", 0 , 0);";
                                    $mysqli->query($InsertMuestra);  
                    

                                    /* Ahora toca mandar un corre a esta persona a   */
                                    $Lsql = "SELECT * FROM ".$BaseDatos.".G734 WHERE G734_ConsInte__b =".$numero_form;
                                    $resLsql = $mysqli->query($Lsql);
                                    $datoArray = $resLsql->fetch_array();
                       $strCuerpo_t = '<html><body><font face="arial" size="3">
                       <img src="http://customers.dyalogodev.com/crm_php/formularios/G734/2.jpeg" style="width:30%;">

                       <p>Reciba un cordial saludo</p>
                       <p>NOMBRES : '.$datoArray['G734_C9683'].'</p>
                       <p>CORREO  : '.$datoArray['G734_C9685'].'</p>
                       <p>TELEFONO: '.$datoArray['G734_C9686'].'</p>
                       <p>EVENTO  : '.$datoArray['G734_C9681'].'</p>
                       <p>CARGO   : '.$datoArray['G734_C9684'].'</p>
    </font></body></html>';

        
                                    $email_user = "crmisec@isec.com.co";
                                    $email_password = "80192428";
                                    $the_subject = "CLIENTE AUTORIZA TRATAMIENTO DATOS";

                                    
                                    $from_name = "Notificaciones";

                                    require "PHPMailer/class.phpmailer.php";
                                    require "PHPMailer/class.smtp.php";

                                    $phpmailer = new PHPMailer(); 
                                    $phpmailer->CharSet = 'UTF-8';
                                    // ---------- datos de la cuenta de Gmail -------------------------------
                                    $phpmailer->Username = $email_user;
                                    $phpmailer->Password = $email_password; 
                                    //-----------------------------------------------------------------------
                                    // $phpmailer->SMTPDebug = 1;
                                    $phpmailer->SMTPSecure = 'ssl';
                                    $phpmailer->Host = "smtp.gmail.com"; // GMail
                                    $phpmailer->Port = 465;
                                    $phpmailer->IsSMTP(); // use SMTP
                                    $phpmailer->SMTPAuth = true;
                                    $phpmailer->setFrom($phpmailer->Username,$from_name);
                                    $phpmailer->AddAddress('andres.garzon@isec.com.co');
                                    
                                    $phpmailer->Subject = $the_subject; 
                                    $phpmailer->Body = $strCuerpo_t;
                                    $phpmailer->IsHTML(true);   
                                    if (!$phpmailer->Send()){
                                        echo $phpmailer->ErrorInfo." \n";
                                    } else {
                                       // echo "EMAIL OK \n";
                                        //echo "********************************************* \n";
                                    }
                                    
                                    echo "<div class=\"alert alert-info\">Gracias por aceptar nuestra <a href='#'>Politica de tratamiento de datos</a>, nos pondremos en contacto pronto!</div>";



                                }
                                
                            }else{
                                $numero_form = base64_decode($_GET['cons']);
                                $Lsql = "UPDATE ".$BaseDatos.".G734 SET G734_C9683 = '#ELIMINADO#' , G734_C9684= '#ELIMINADO#', G734_C9685 = '#ELIMINADO#' , G734_C9686 = '#ELIMINADO#' , G734_C9682 = '#ELIMINADO#' , G734_C9687 = 'NO ACEPTA' WHERE G734_ConsInte__b =".$numero_form;
                                if ($mysqli->query($Lsql) === TRUE) {
                                    echo "<div class=\"alert alert-info\">Hemos eliminado su información de la base de datos.</div>";
                                }
                            }

                        } 
                    ?>
                </div><!-- /.login-box -->
            </div>
        </div>


        <!-- jQuery 2.2.3 -->
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>  
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/numeric.js"></script>

        <script type="text/javascript" src="formularios/G734/G734_eventos.js"></script>
        <script type="text/javascript">
            $.validator.setDefaults({
                submitHandler: function() { 
                     $("#formLogin").submit();
                }
            });

            $(function(){

                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };

                //str_Select2 estos son los guiones
                


                //datepickers
                

                //Timepickers
                


                //Validaciones numeros Enteros
                

                $("#G734_C9686").numeric();

                $("#G734_C9686").change(function(){
                    $(".alert").remove();
                    if($(this).val().length > 8){
                        /* Es un celular */
                        var cadena= $(this).val();
                        var fstChar = cadena.charAt(0);
                        if(fstChar != '3'){
                            $("#G734_C9686").parent().after('<div class="alert alert-warning">El celular debe iniciar por 3</div>');
                            $("#G734_C9686").val('');
                        }else{
                            /* si ya inicia por cero entonces toca hacer las otras validaciones */
                            
                            if($(this).val().length < 9){
                                $("#G734_C9686").parent().after('<div class="alert alert-warning">El celular debe tener 10 digitos</div>');
                                $("#G734_C9686").val('');
                            }    
                        }
                    }else{
                        /* es un numero de teléfono */
                        if($(this).val().length < 7){
                            $("#G734_C9686").parent().after('<div class="alert alert-warning">El teléfono debe tener entre 7 y 8 digitos</div>');
                            $("#G734_C9686").val('');
                        }
                    }
                });
            

                //Validaciones numeros Decimales
               
                <?php
                    if(isset($_GET['result'])){
                        if($_GET['result'] ==  1){
                ?>
                        swal({
                            title: "Exito!",
                            text: "Contacto guardado con exito!",
                            type: "success",
                            confirmButtonText: "Ok"
                        });
                        
                        
                <?php   }else{ ?>
                            swal({
                                title: "Error!",
                                text: 'Ocurrio un error, intenta mas tarde',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }
                    }
                ?>
            });
        </script>
        
