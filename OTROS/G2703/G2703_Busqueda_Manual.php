
<?php
	$http = "http://".$_SERVER["HTTP_HOST"];
	if (isset($_SERVER['HTTPS'])) {
	    $http = "https://".$_SERVER["HTTP_HOST"];
	}
	if(isset($_GET["accion_manual"])){
		$idMonoef=-19;
		$texto = "Busqueda No Aplica";
	}else{
		$idMonoef =-18;
		$texto = "No Suministra Información";
	}
?>

<script type="text/javascript">
	function autofitIframe(id){
		if (!window.opera && document.all && document.getElementById){
			id.style.height=id.contentWindow.document.body.scrollHeight;
		} else if(document.getElementById) {
			id.style.height=id.contentDocument.body.scrollHeight+"px";
		}
	}
</script>

<link rel="stylesheet" href="assets/plugins/WinPicker/dist/wickedpicker.min.css">
<script type="text/javascript" src="assets/plugins/WinPicker/dist/wickedpicker.min.js"></script>

<div class="row" id="buscador">
	<div class="col-md-1">&nbsp;</div>
	<div class="col-md-10">
		<div class="row">
			<form id="formId">
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2703_C52907" id="LblG2703_C52907">CEDULA TITULAR</label>
					    <input type="text" class="form-control input-sm" id="G2703_C52907" name="G2703_C52907"  placeholder="CEDULA TITULAR">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2703_C52842" id="LblG2703_C52842">NOMBRE DEL TITULAR</label>
					    <input type="text" class="form-control input-sm" id="G2703_C52842" name="G2703_C52842"  placeholder="NOMBRE DEL TITULAR">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2703_C52850" id="LblG2703_C52850">TELEFONO 1 DEL TITULAR</label>
					    <input type="text" class="form-control input-sm" id="G2703_C52850" name="G2703_C52850"  placeholder="TELEFONO 1 DEL TITULAR">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2703_C52851" id="LblG2703_C52851">TELEFONO 2 DEL TITULAR</label>
					    <input type="text" class="form-control input-sm" id="G2703_C52851" name="G2703_C52851"  placeholder="TELEFONO 2 DEL TITULAR">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2703_C52852" id="LblG2703_C52852">TELEFONO 3 DEL TITULAR</label>
					    <input type="text" class="form-control input-sm" id="G2703_C52852" name="G2703_C52852"  placeholder="TELEFONO 3 DEL TITULAR">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2703_C52853" id="LblG2703_C52853">TELEFONO 4 DEL TITULAR</label>
					    <input type="text" class="form-control input-sm" id="G2703_C52853" name="G2703_C52853"  placeholder="TELEFONO 4 DEL TITULAR">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G2703_C52854" id="LblG2703_C52854">TELEFONO 5 DEL TITULAR</label>
					    <input type="text" class="form-control input-sm" id="G2703_C52854" name="G2703_C52854"  placeholder="TELEFONO 5 DEL TITULAR">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 	
			</form>
		</div>
	</div>
	<div class="col-md-1">&nbsp;</div>
</div>
<div class="row" id="botones">
	<div class="col-md-6 col-xs-6">
		<button class="btn btn-block btn-danger" id="btnCancelar" type="button"><?php echo $texto; ?></button>
	</div>
	<div class="col-md-6 col-xs-6">
		<button class="btn btn-block btn-success" id="btnBuscar" type="button"><i class="fa fa-search"></i>Buscar en toda la base de datos</button>
	</div>
</div>
<br/>
<div class="row" id="resulados">
	<div class="col-md-12 col-xs-12" id="resultadosBusqueda">

	</div>
</div>
<div class="row">
	<div class="col-md-12" id="gestiones">
		<iframe id="frameContenedor" src="" style="width: 100%; height: 2500px;"  marginheight="0" marginwidth="0" noresize  frameborder="0" onload="autofitIframe(this);">
              
        </iframe>
	</div>
</div>
<input type="hidden" id="CampoIdGestionCbx" value="<?php echo $_GET["id_gestion_cbx"];?>">
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>');
<?php 
    $id_campan='';
    if(isset($_GET['id_campana_crm']) && $_GET['id_campana_crm'] != '0'){
        $id_campan='&id_campana_crm='.$_GET['id_campana_crm'];
    } 
?>

    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }


    // Listen to message from child window
    bindEvent(window, 'message', function (e) {
        if(Array.isArray(e.data)){
            console.log(e.data);
            var keys=Object.keys(e.data[0].camposivr);
            var key=0;
            $.each(e.data[0].camposivr, function(i, valor){
                if($("#"+keys[key]).length > 0){
                    $("#"+keys[key]).val(valor); 
                }
                key++;
            });
            buscarRegistros(e.data);
//          $("#btnBuscar").click();
        }
        if(typeof(e.data)== 'object'){
            parent.postMessage(e.data, '*');
        }
    });
    
	$(function(){
    
        //DATEPICKER
        
        
        //TIMEPICKER
        
        var ObjDataGestion= new Object();
        ObjDataGestion.server         = '<?php echo $http; ?>';
        ObjDataGestion.canal          = '<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal bq_manual"; }?>';
        ObjDataGestion.token          = '<?php echo $_GET["token"];?>';
        ObjDataGestion.predictiva     = '<?php if(isset($_GET['predictiva'])) { echo $_GET['predictiva']; } else{ echo 'null'; }?>';
        ObjDataGestion.consinte       = '<?php if(isset($_GET['consinte'])) { echo $_GET['consinte']; } else{ echo 'null'; }?>';
        ObjDataGestion.id_campana_crm = '<?php if(isset($_GET['id_campana_crm'])) { echo $_GET['id_campana_crm']; } else{ echo 'null'; }?>';
        ObjDataGestion.sentido        = '<?php if(isset($_GET['sentido'])) { echo $_GET['sentido']; } else{ echo 'null'; }?>';
            $("#btnBuscar").click(function(){
            var valido=0;
            
            
            if(valido==0){
                var datos = $("#formId").serialize();
                $.ajax({
                    url     	: 'formularios/G2703/G2703_Funciones_Busqueda_Manual.php?action=GET_DATOS<?=$id_campan?>&agente=<?=getIdentificacionUser($_GET["token"])?>',
                    type		: 'post',
                    dataType	: 'json',
                    data		: datos,
                    success 	: function(datosq){
                        if(datosq[0].cantidad_registros > 1){
                            buscarRegistros(datosq);
                        }else if(datosq[0].cantidad_registros == 1){
                            $("#buscador").hide();
                            $("#botones").hide();
                            $("#resulados").hide();
                            var id = datosq[0].registros[0].G2703_ConsInte__b;
                            if(datosq[0].registros[0].id_muestra == null){
                                $.ajax({
                                    url     	: 'formularios/generados/PHP_Ejecutar.php?action=ADD_MUESTRA&campana_crm=<?php echo $_GET['id_campana_crm'];?>',
                                    type		: 'post',
                                    data        : {id:id},
                                    success 	: function(data){
                                        <?php if(isset($_GET['token'])) { ?>
                                            guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>',id,ObjDataGestion);
                                            $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal ani"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>');
                                        <?php } ?>
                                    }
                                });
                            }else{
                                <?php if(isset($_GET['token'])) { ?>
                                    guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>',id,ObjDataGestion);
                                    $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal ani"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>');
                                <?php } ?>                        
                            }
                        }else{
                            adicionarRegistro(2,datosq[0].mensaje);
                        }
                    }
                });
            }
		});

		$("\#btnCancelar").click(function(){
            borrarStorage('<?php echo $_GET["id_gestion_cbx"]?>');
			window.location = 'formularios/generados/PHP_Cerrar_Cancelar.php?canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal ani"; } ?>&token=<?php echo $_GET['token'];?>&id_gestion_cbx=<?php echo $_GET['id_gestion_cbx'];?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?><?php echo "&idMonoef=".$idMonoef;?><?php echo "&tiempoInicio=".date("Y-m-d H:i:s");?>';
		});
	});

    function buscarRegistros(datosq){
        var ObjDataGestion= new Object();
        ObjDataGestion.server         = '<?php echo $http; ?>';
        ObjDataGestion.canal          = '<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal bq_manual"; }?>';
        ObjDataGestion.token          = '<?php echo $_GET["token"];?>';
        ObjDataGestion.predictiva     = '<?php if(isset($_GET['predictiva'])) { echo $_GET['predictiva']; } else{ echo 'null'; }?>';
        ObjDataGestion.consinte       = '<?php if(isset($_GET['consinte'])) { echo $_GET['consinte']; } else{ echo 'null'; }?>';
        ObjDataGestion.id_campana_crm = '<?php if(isset($_GET['id_campana_crm'])) { echo $_GET['id_campana_crm']; } else{ echo 'null'; }?>';
        ObjDataGestion.sentido        = '<?php if(isset($_GET['sentido'])) { echo $_GET['sentido']; } else{ echo 'null'; }?>';    
        var valores = null;
        var tabla_a_mostrar = '<div class="box box-default">'+
        '<div class="box-header">'+
            '<h3 class="box-title">RESULTADOS DE LA BUSQUEDA</h3>'+
        '</div>'+
        '<div class="box-body">'+
            '<table class="table table-hover table-bordered" style="width:100%;">';
        tabla_a_mostrar += '<thead>';
        tabla_a_mostrar += '<tr>';
        tabla_a_mostrar += ' <th>CEDULA TITULAR</th><th>NOMBRE DEL TITULAR</th><th>TELEFONO 1 DEL TITULAR</th><th>TELEFONO 2 DEL TITULAR</th><th>TELEFONO 3 DEL TITULAR</th><th>TELEFONO 4 DEL TITULAR</th><th>TELEFONO 5 DEL TITULAR</th> ';
        tabla_a_mostrar += '</tr>';
        tabla_a_mostrar += '</thead>';
        tabla_a_mostrar += '<tbody>';
        $.each(datosq[0].registros, function(i, item) {
            tabla_a_mostrar += '<tr idMuestra="'+ item.id_muestra +'" ConsInte="'+ item.G2703_ConsInte__b +'" class="EditRegistro">';
            tabla_a_mostrar += '<td>'+ item.G2703_C52907 +'</td><td>'+ item.G2703_C52842 +'</td><td>'+ item.G2703_C52850 +'</td><td>'+ item.G2703_C52851 +'</td><td>'+ item.G2703_C52852 +'</td><td>'+ item.G2703_C52853 +'</td><td>'+ item.G2703_C52854 +'</td>';
            tabla_a_mostrar += '</tr>';
        });
        tabla_a_mostrar += '</tbody>';
        tabla_a_mostrar += '</table></div></div>';

        $("#resultadosBusqueda").html(tabla_a_mostrar);

        $(".EditRegistro").dblclick(function(){
            var id = $(this).attr("ConsInte");
            var muestra=$(this).attr("idMuestra");
            swal({
                html : true,
                title: "Información - Dyalogo CRM",
                text: 'Esta seguro de editar este registro?',
                type: "warning",
                confirmButtonText: "Editar registro",
                cancelButtonText : "No Editar registro",
                showCancelButton : true,
                closeOnConfirm : true
            },
                function(isconfirm){
                    if(isconfirm){
                        $("#buscador").hide();
                        $("#botones").hide();
                        $("#resulados").hide();
                        if(muestra == 'null'){
                            $.ajax({
                                url     	: 'formularios/generados/PHP_Ejecutar.php?action=ADD_MUESTRA&campana_crm=<?php echo $_GET['id_campana_crm'];?>',
                                type		: 'post',
                                data        : {id:id},
                                success 	: function(data){
                                    <?php if(isset($_GET['token'])) { ?>
                                        guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>',id,ObjDataGestion);
                                        $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal ani"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>');
                                    <?php } ?>
                                }
                            });
                        }else{
                            <?php if(isset($_GET['token'])) { ?>
                                guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>',id,ObjDataGestion);
                                $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal ani"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>');
                            <?php } ?>                        
                        }
                    }else{
                        $("#buscador").show();
                        $("#botones").show();
                        $("#resulados").show();
                    }
                });
            });    
    }
    
	function adicionarRegistro(tipo,mensaje){
        var ObjDataGestion= new Object();
        ObjDataGestion.server         = '<?php echo $http; ?>';
        ObjDataGestion.canal          = '<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal bq_manual"; }?>';
        ObjDataGestion.token          = '<?php echo $_GET["token"];?>';
        ObjDataGestion.predictiva     = '<?php if(isset($_GET['predictiva'])) { echo $_GET['predictiva']; } else{ echo 'null'; }?>';
        ObjDataGestion.consinte       = '<?php if(isset($_GET['consinte'])) { echo $_GET['consinte']; } else{ echo 'null'; }?>';
        ObjDataGestion.id_campana_crm = '<?php if(isset($_GET['id_campana_crm'])) { echo $_GET['id_campana_crm']; } else{ echo 'null'; }?>';
        ObjDataGestion.sentido        = '<?php if(isset($_GET['sentido'])) { echo $_GET['sentido']; } else{ echo 'null'; }?>';
    
        if(mensaje == null ){
            if(tipo == "1"){
                mensaje = "Desea adicionar un registro";
            }
            if(tipo == "2"){
                mensaje = "No se encontraron datos, desea adicionar un registro";
            }
            swal({
                html : true,
                title: "Información - Dyalogo CRM",
                text: mensaje,
                type: "warning",
                 confirmButtonText: "Adicionar registro",
                cancelButtonText : "Hacer otra busqueda",
                showCancelButton : true,
                closeOnConfirm : true
            },
            function(isconfirm){
                $("#buscador").hide();
                $("#botones").hide();
                $("#resulados").hide();
                if(isconfirm){
                    //JDBD - obtenemos los valores de la busqueda manual para enviarselos al formulario
                    var G2706_C52919 = $("#G2703_C52907").val();
			var G2706_C52920 = $("#G2703_C52842").val();
			var G2706_C52928 = $("#G2703_C52850").val();
			var G2706_C52929 = $("#G2703_C52851").val();
			var G2706_C52930 = $("#G2703_C52852").val();
			var G2706_C52931 = $("#G2703_C52853").val();
			var G2706_C52932 = $("#G2703_C52854").val();
                    $.ajax({
                    url     	: 'formularios/generados/PHP_Ejecutar.php?action=ADD&campana_crm=<?php echo $_GET['id_campana_crm'];?>',
                    type		: 'post',
                    dataType	: 'json',
                    success 	: function(numeroIdnuevo){
                        <?php if(isset($_GET['token'])){ ?>
                        guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>',numeroIdnuevo,ObjDataGestion);
                        $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ numeroIdnuevo +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal ani"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>&nuevoregistro=true&G2706_C52919='+G2706_C52919+'&G2706_C52920='+G2706_C52920+'&G2706_C52928='+G2706_C52928+'&G2706_C52929='+G2706_C52929+'&G2706_C52930='+G2706_C52930+'&G2706_C52931='+G2706_C52931+'&G2706_C52932='+G2706_C52932);
                        <?php } ?>
                    }
                });
                }else{
                    limpiar();
                }
            });
        }else{
            swal("", mensaje, "warning");
        }
	}
	function limpiar(){
		$("#buscador").show();
		$("#buscador :input").each(function(){
			$(this).val("");
		});
		$("#resultadosBusqueda").html("");
		$("#botones").show();
		$("#resulados").show();
	}
</script>
