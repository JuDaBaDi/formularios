<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2703_ConsInte__b, G2703_FechaInsercion , G2703_Usuario ,  G2703_CodigoMiembro  , G2703_PoblacionOrigen , G2703_EstadoDiligenciamiento ,  G2703_IdLlamada , G2703_C52864 as principal ,G2703_C52907,G2703_C52842,G2703_C52843,G2703_C52844,G2703_C52845,G2703_C52846,G2703_C52847,G2703_C52947,G2703_C52848,G2703_C52849,G2703_C52948,G2703_C52850,G2703_C52851,G2703_C52852,G2703_C52853,G2703_C52854,G2703_C52951,G2703_C56101,G2703_C52838,G2703_C52839,G2703_C52840,G2703_C52855,G2703_C52856,G2703_C52857,G2703_C52858,G2703_C52859,G2703_C52860,G2703_C52861,G2703_C52862,G2703_C52863,G2703_C52864,G2703_C52865,G2703_C52866,G2703_C52867 FROM '.$BaseDatos.'.G2703 WHERE G2703_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2703_C52907'] = $key->G2703_C52907;

                $datos[$i]['G2703_C52842'] = $key->G2703_C52842;

                $datos[$i]['G2703_C52843'] = $key->G2703_C52843;

                $datos[$i]['G2703_C52844'] = $key->G2703_C52844;

                $datos[$i]['G2703_C52845'] = $key->G2703_C52845;

                $datos[$i]['G2703_C52846'] = $key->G2703_C52846;

                $datos[$i]['G2703_C52847'] = $key->G2703_C52847;

                $datos[$i]['G2703_C52947'] = $key->G2703_C52947;

                $datos[$i]['G2703_C52848'] = $key->G2703_C52848;

                $datos[$i]['G2703_C52849'] = $key->G2703_C52849;

                $datos[$i]['G2703_C52948'] = $key->G2703_C52948;

                $datos[$i]['G2703_C52850'] = $key->G2703_C52850;

                $datos[$i]['G2703_C52851'] = $key->G2703_C52851;

                $datos[$i]['G2703_C52852'] = $key->G2703_C52852;

                $datos[$i]['G2703_C52853'] = $key->G2703_C52853;

                $datos[$i]['G2703_C52854'] = $key->G2703_C52854;

                $datos[$i]['G2703_C52951'] = $key->G2703_C52951;

                $datos[$i]['G2703_C56101'] = $key->G2703_C56101;

                $datos[$i]['G2703_C52838'] = $key->G2703_C52838;

                $datos[$i]['G2703_C52839'] = $key->G2703_C52839;

                $datos[$i]['G2703_C52840'] = $key->G2703_C52840;

                $datos[$i]['G2703_C52855'] = $key->G2703_C52855;

                $datos[$i]['G2703_C52856'] = $key->G2703_C52856;

                $datos[$i]['G2703_C52857'] = $key->G2703_C52857;

                $datos[$i]['G2703_C52858'] = $key->G2703_C52858;

                $datos[$i]['G2703_C52859'] = $key->G2703_C52859;

                $datos[$i]['G2703_C52860'] = $key->G2703_C52860;

                $datos[$i]['G2703_C52861'] = $key->G2703_C52861;

                $datos[$i]['G2703_C52862'] = $key->G2703_C52862;

                $datos[$i]['G2703_C52863'] = $key->G2703_C52863;

                $datos[$i]['G2703_C52864'] = $key->G2703_C52864;

                $datos[$i]['G2703_C52865'] = $key->G2703_C52865;

                $datos[$i]['G2703_C52866'] = $key->G2703_C52866;

                $datos[$i]['G2703_C52867'] = $key->G2703_C52867;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2703";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2703_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2703_ConsInte__b as id,  G2703_C52842 as camp2 , G2703_C52864 as camp1 
                     FROM ".$BaseDatos.".G2703  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2703_ConsInte__b as id,  G2703_C52842 as camp2 , G2703_C52864 as camp1  
                    FROM ".$BaseDatos.".G2703  JOIN ".$BaseDatos.".G2703_M".$_POST['muestra']." ON G2703_ConsInte__b = G2703_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2703_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2703_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2703_C52842 LIKE '%".$B."%' OR G2703_C52864 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2703_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2703");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2703_ConsInte__b, G2703_FechaInsercion , G2703_Usuario ,  G2703_CodigoMiembro  , G2703_PoblacionOrigen , G2703_EstadoDiligenciamiento ,  G2703_IdLlamada , G2703_C52864 as principal ,G2703_C52907,G2703_C52842,G2703_C52843,G2703_C52844,G2703_C52845,G2703_C52846,G2703_C52847,G2703_C52947,G2703_C52848,G2703_C52849,G2703_C52948,G2703_C52850,G2703_C52851,G2703_C52852,G2703_C52853,G2703_C52854,G2703_C52951,G2703_C52838,G2703_C52839, a.LISOPC_Nombre____b as G2703_C52840, b.LISOPC_Nombre____b as G2703_C52855, c.LISOPC_Nombre____b as G2703_C52856, d.LISOPC_Nombre____b as G2703_C52857,G2703_C52858,G2703_C52859,G2703_C52860,G2703_C52861,G2703_C52862,G2703_C52863,G2703_C52864,G2703_C52865,G2703_C52866,G2703_C52867 FROM '.$BaseDatos.'.G2703 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2703_C52840 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2703_C52855 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2703_C52856 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2703_C52857';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G2703_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2703_ConsInte__b , ($fila->G2703_C52907) , ($fila->G2703_C52842) , ($fila->G2703_C52843) , ($fila->G2703_C52844) , ($fila->G2703_C52845) , ($fila->G2703_C52846) , ($fila->G2703_C52847) , ($fila->G2703_C52947) , ($fila->G2703_C52848) , ($fila->G2703_C52849) , ($fila->G2703_C52948) , ($fila->G2703_C52850) , ($fila->G2703_C52851) , ($fila->G2703_C52852) , ($fila->G2703_C52853) , ($fila->G2703_C52854) , ($fila->G2703_C52951) , ($fila->G2703_C52838) , ($fila->G2703_C52839) , ($fila->G2703_C52840) , ($fila->G2703_C52855) , ($fila->G2703_C52856) , ($fila->G2703_C52857) , ($fila->G2703_C52858) , ($fila->G2703_C52859) , ($fila->G2703_C52860) , ($fila->G2703_C52861) , ($fila->G2703_C52862) , ($fila->G2703_C52863) , ($fila->G2703_C52864) , ($fila->G2703_C52865) , ($fila->G2703_C52866) , ($fila->G2703_C52867) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2703 WHERE G2703_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2703";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2703_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2703_ConsInte__b as id,  G2703_C52842 as camp2 , G2703_C52864 as camp1  FROM '.$BaseDatos.'.G2703 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2703_ConsInte__b as id,  G2703_C52842 as camp2 , G2703_C52864 as camp1  
                    FROM ".$BaseDatos.".G2703  JOIN ".$BaseDatos.".G2703_M".$_POST['muestra']." ON G2703_ConsInte__b = G2703_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2703_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2703_C52842 LIKE "%'.$B.'%" OR G2703_C52864 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2703_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2703 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2703(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2703_C52907"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52907 = '".$_POST["G2703_C52907"]."'";
                $LsqlI .= $separador."G2703_C52907";
                $LsqlV .= $separador."'".$_POST["G2703_C52907"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52842"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52842 = '".$_POST["G2703_C52842"]."'";
                $LsqlI .= $separador."G2703_C52842";
                $LsqlV .= $separador."'".$_POST["G2703_C52842"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52843"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52843 = '".$_POST["G2703_C52843"]."'";
                $LsqlI .= $separador."G2703_C52843";
                $LsqlV .= $separador."'".$_POST["G2703_C52843"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52844"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52844 = '".$_POST["G2703_C52844"]."'";
                $LsqlI .= $separador."G2703_C52844";
                $LsqlV .= $separador."'".$_POST["G2703_C52844"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52845"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52845 = '".$_POST["G2703_C52845"]."'";
                $LsqlI .= $separador."G2703_C52845";
                $LsqlV .= $separador."'".$_POST["G2703_C52845"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52846"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52846 = '".$_POST["G2703_C52846"]."'";
                $LsqlI .= $separador."G2703_C52846";
                $LsqlV .= $separador."'".$_POST["G2703_C52846"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52847"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52847 = '".$_POST["G2703_C52847"]."'";
                $LsqlI .= $separador."G2703_C52847";
                $LsqlV .= $separador."'".$_POST["G2703_C52847"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52947"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52947 = '".$_POST["G2703_C52947"]."'";
                $LsqlI .= $separador."G2703_C52947";
                $LsqlV .= $separador."'".$_POST["G2703_C52947"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52848"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52848 = '".$_POST["G2703_C52848"]."'";
                $LsqlI .= $separador."G2703_C52848";
                $LsqlV .= $separador."'".$_POST["G2703_C52848"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52849"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52849 = '".$_POST["G2703_C52849"]."'";
                $LsqlI .= $separador."G2703_C52849";
                $LsqlV .= $separador."'".$_POST["G2703_C52849"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52948"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52948 = '".$_POST["G2703_C52948"]."'";
                $LsqlI .= $separador."G2703_C52948";
                $LsqlV .= $separador."'".$_POST["G2703_C52948"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52850"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52850 = '".$_POST["G2703_C52850"]."'";
                $LsqlI .= $separador."G2703_C52850";
                $LsqlV .= $separador."'".$_POST["G2703_C52850"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52851"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52851 = '".$_POST["G2703_C52851"]."'";
                $LsqlI .= $separador."G2703_C52851";
                $LsqlV .= $separador."'".$_POST["G2703_C52851"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52852"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52852 = '".$_POST["G2703_C52852"]."'";
                $LsqlI .= $separador."G2703_C52852";
                $LsqlV .= $separador."'".$_POST["G2703_C52852"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52853"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52853 = '".$_POST["G2703_C52853"]."'";
                $LsqlI .= $separador."G2703_C52853";
                $LsqlV .= $separador."'".$_POST["G2703_C52853"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52854"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52854 = '".$_POST["G2703_C52854"]."'";
                $LsqlI .= $separador."G2703_C52854";
                $LsqlV .= $separador."'".$_POST["G2703_C52854"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52951"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52951 = '".$_POST["G2703_C52951"]."'";
                $LsqlI .= $separador."G2703_C52951";
                $LsqlV .= $separador."'".$_POST["G2703_C52951"]."'";
                $validar = 1;
            }

            if(isset($_POST["G2703_C56101"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C56101 = '".$_POST["G2703_C56101"]."'";
                $LsqlI .= $separador."G2703_C56101";
                $LsqlV .= $separador."'".$_POST["G2703_C56101"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52838"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52838 = '".$_POST["G2703_C52838"]."'";
                $LsqlI .= $separador."G2703_C52838";
                $LsqlV .= $separador."'".$_POST["G2703_C52838"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52839"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52839 = '".$_POST["G2703_C52839"]."'";
                $LsqlI .= $separador."G2703_C52839";
                $LsqlV .= $separador."'".$_POST["G2703_C52839"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52840"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52840 = '".$_POST["G2703_C52840"]."'";
                $LsqlI .= $separador."G2703_C52840";
                $LsqlV .= $separador."'".$_POST["G2703_C52840"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52855"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52855 = '".$_POST["G2703_C52855"]."'";
                $LsqlI .= $separador."G2703_C52855";
                $LsqlV .= $separador."'".$_POST["G2703_C52855"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52856"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52856 = '".$_POST["G2703_C52856"]."'";
                $LsqlI .= $separador."G2703_C52856";
                $LsqlV .= $separador."'".$_POST["G2703_C52856"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52857"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52857 = '".$_POST["G2703_C52857"]."'";
                $LsqlI .= $separador."G2703_C52857";
                $LsqlV .= $separador."'".$_POST["G2703_C52857"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52858"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52858 = '".$_POST["G2703_C52858"]."'";
                $LsqlI .= $separador."G2703_C52858";
                $LsqlV .= $separador."'".$_POST["G2703_C52858"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52859"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52859 = '".$_POST["G2703_C52859"]."'";
                $LsqlI .= $separador."G2703_C52859";
                $LsqlV .= $separador."'".$_POST["G2703_C52859"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52860"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52860 = '".$_POST["G2703_C52860"]."'";
                $LsqlI .= $separador."G2703_C52860";
                $LsqlV .= $separador."'".$_POST["G2703_C52860"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52861"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52861 = '".$_POST["G2703_C52861"]."'";
                $LsqlI .= $separador."G2703_C52861";
                $LsqlV .= $separador."'".$_POST["G2703_C52861"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52862"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52862 = '".$_POST["G2703_C52862"]."'";
                $LsqlI .= $separador."G2703_C52862";
                $LsqlV .= $separador."'".$_POST["G2703_C52862"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52863"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52863 = '".$_POST["G2703_C52863"]."'";
                $LsqlI .= $separador."G2703_C52863";
                $LsqlV .= $separador."'".$_POST["G2703_C52863"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52864"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52864 = '".$_POST["G2703_C52864"]."'";
                $LsqlI .= $separador."G2703_C52864";
                $LsqlV .= $separador."'".$_POST["G2703_C52864"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52865"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52865 = '".$_POST["G2703_C52865"]."'";
                $LsqlI .= $separador."G2703_C52865";
                $LsqlV .= $separador."'".$_POST["G2703_C52865"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52866"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52866 = '".$_POST["G2703_C52866"]."'";
                $LsqlI .= $separador."G2703_C52866";
                $LsqlV .= $separador."'".$_POST["G2703_C52866"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2703_C52867"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_C52867 = '".$_POST["G2703_C52867"]."'";
                $LsqlI .= $separador."G2703_C52867";
                $LsqlV .= $separador."'".$_POST["G2703_C52867"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2703_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2703_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2703_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2703_Usuario , G2703_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2703_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2703 WHERE G2703_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2703 SET G2703_UltiGest__b =-14, G2703_GesMasImp_b =-14, G2703_TipoReintentoUG_b =0, G2703_TipoReintentoGMI_b =0, G2703_EstadoUG_b =-14, G2703_EstadoGMI_b =-14, G2703_CantidadIntentos =0, G2703_CantidadIntentosGMI_b =0 WHERE G2703_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G2704_ConsInte__b, G2704_C52885, G2704_C52871, G2704_C52872, G2704_C52873, G2704_C52874, G2704_C52875, G2704_C52876, G2704_C52877, G2704_C52878, G2704_C52879, G2704_C52880, G2704_C52881, G2704_C52882, G2704_C52883, G2704_C52884, G2704_C52886 FROM ".$BaseDatos.".G2704  ";

        $SQL .= " WHERE G2704_C52871 = '".$numero."'"; 

        $SQL .= " ORDER BY G2704_C52885";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2704_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2704_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G2704_C52885)."</cell>";

                echo "<cell>". ($fila->G2704_C52871)."</cell>";

                echo "<cell>". ($fila->G2704_C52872)."</cell>";

                echo "<cell>". ($fila->G2704_C52873)."</cell>";

                echo "<cell>". ($fila->G2704_C52874)."</cell>";

                echo "<cell>". ($fila->G2704_C52875)."</cell>";

                echo "<cell>". ($fila->G2704_C52876)."</cell>";

                echo "<cell>". ($fila->G2704_C52877)."</cell>";

                echo "<cell>". ($fila->G2704_C52878)."</cell>";

                echo "<cell>". ($fila->G2704_C52879)."</cell>";

                echo "<cell>". ($fila->G2704_C52880)."</cell>";

                echo "<cell>". ($fila->G2704_C52881)."</cell>";

                echo "<cell>". ($fila->G2704_C52882)."</cell>";

                echo "<cell>". ($fila->G2704_C52883)."</cell>";

                echo "<cell>". ($fila->G2704_C52884)."</cell>";

                echo "<cell>". ($fila->G2704_C52886)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2704 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2704(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G2704_C52885"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52885 = '".$_POST["G2704_C52885"]."'";
                    $LsqlI .= $separador."G2704_C52885";
                    $LsqlV .= $separador."'".$_POST["G2704_C52885"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52872"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52872 = '".$_POST["G2704_C52872"]."'";
                    $LsqlI .= $separador."G2704_C52872";
                    $LsqlV .= $separador."'".$_POST["G2704_C52872"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52873"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52873 = '".$_POST["G2704_C52873"]."'";
                    $LsqlI .= $separador."G2704_C52873";
                    $LsqlV .= $separador."'".$_POST["G2704_C52873"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52874"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52874 = '".$_POST["G2704_C52874"]."'";
                    $LsqlI .= $separador."G2704_C52874";
                    $LsqlV .= $separador."'".$_POST["G2704_C52874"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52875"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52875 = '".$_POST["G2704_C52875"]."'";
                    $LsqlI .= $separador."G2704_C52875";
                    $LsqlV .= $separador."'".$_POST["G2704_C52875"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52876"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52876 = '".$_POST["G2704_C52876"]."'";
                    $LsqlI .= $separador."G2704_C52876";
                    $LsqlV .= $separador."'".$_POST["G2704_C52876"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52877"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52877 = '".$_POST["G2704_C52877"]."'";
                    $LsqlI .= $separador."G2704_C52877";
                    $LsqlV .= $separador."'".$_POST["G2704_C52877"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52878"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52878 = '".$_POST["G2704_C52878"]."'";
                    $LsqlI .= $separador."G2704_C52878";
                    $LsqlV .= $separador."'".$_POST["G2704_C52878"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52879"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52879 = '".$_POST["G2704_C52879"]."'";
                    $LsqlI .= $separador."G2704_C52879";
                    $LsqlV .= $separador."'".$_POST["G2704_C52879"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52880"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52880 = '".$_POST["G2704_C52880"]."'";
                    $LsqlI .= $separador."G2704_C52880";
                    $LsqlV .= $separador."'".$_POST["G2704_C52880"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52881"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52881 = '".$_POST["G2704_C52881"]."'";
                    $LsqlI .= $separador."G2704_C52881";
                    $LsqlV .= $separador."'".$_POST["G2704_C52881"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52882"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52882 = '".$_POST["G2704_C52882"]."'";
                    $LsqlI .= $separador."G2704_C52882";
                    $LsqlV .= $separador."'".$_POST["G2704_C52882"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52883"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52883 = '".$_POST["G2704_C52883"]."'";
                    $LsqlI .= $separador."G2704_C52883";
                    $LsqlV .= $separador."'".$_POST["G2704_C52883"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52884"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52884 = '".$_POST["G2704_C52884"]."'";
                    $LsqlI .= $separador."G2704_C52884";
                    $LsqlV .= $separador."'".$_POST["G2704_C52884"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52886"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52886 = '".$_POST["G2704_C52886"]."'";
                    $LsqlI .= $separador."G2704_C52886";
                    $LsqlV .= $separador."'".$_POST["G2704_C52886"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2704_C52871 = $numero;
                    $LsqlU .= ", G2704_C52871 = ".$G2704_C52871."";
                    $LsqlI .= ", G2704_C52871";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2704_Usuario ,  G2704_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2704_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2704 WHERE  G2704_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
