<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

      if (isset($_GET["traerVehiculos"])) {

        $intNit_t = $_POST["intNit_t"];
        $strPlaca_t = $_POST["strPlaca_t"];
        $arrVehiculos_t = [];

        $strSQLVehiculos_t = "SELECT G2264_ConsInte__b AS id, (CASE WHEN G2264_C44246 IS NULL THEN '' ELSE G2264_C44246 END) AS placa, (CASE WHEN G2264_C44249 IS NULL THEN '' ELSE G2264_C44249 END) AS ultimoIngreso, (CASE WHEN G2264_C44256 IS NULL THEN '' ELSE G2264_C44256 END) AS fechaAgenda, (CASE WHEN G2264_C44257 IS NULL THEN '' ELSE G2264_C44257 END) AS horaAgenda, G2264_C44247 as linea FROM ".$BaseDatos.".G2264 WHERE G2264_C44234 = ".$intNit_t." AND G2264_C44246 != '".$strPlaca_t."'";

        $resSQLVehiculos_t = $mysqli->query($strSQLVehiculos_t);

        $i = 0;
        if ($resSQLVehiculos_t->num_rows > 0) {
            while ($row = $resSQLVehiculos_t->fetch_object()) {
                $arrVehiculos_t[$i]["id"] = $row->id;
                $arrVehiculos_t[$i]["placa"] = $row->placa;
                $arrVehiculos_t[$i]["ultimoIngreso"] = $row->ultimoIngreso;
                $arrVehiculos_t[$i]["fechaAgenda"] = $row->fechaAgenda;
                $arrVehiculos_t[$i]["horaAgenda"] = $row->horaAgenda;
                $arrVehiculos_t[$i]["linea"] = $row->linea;

                $i ++;
            }
        }

        echo json_encode($arrVehiculos_t);

      }
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2265_ConsInte__b, G2265_FechaInsercion , G2265_Usuario ,  G2265_CodigoMiembro  , G2265_PoblacionOrigen , G2265_EstadoDiligenciamiento ,  G2265_IdLlamada , G2265_C44274 as principal ,G2265_C44271,G2265_C44272,G2265_C44273,G2265_C44274,G2265_C44275,G2265_C44276,G2265_C44277,G2265_C44278,G2265_C44279,G2265_C44280,G2265_C44281,G2265_C44282,G2265_C44283,G2265_C44284,G2265_C44285,G2265_C44260,G2265_C44261,G2265_C44262,G2265_C44263,G2265_C44264,G2265_C44265,G2265_C44266,G2265_C44267,G2265_C44268,G2265_C44286,G2265_C44287,G2265_C44288,G2265_C44289,G2265_C44290,G2265_C44291,G2265_C44292,G2265_C44293,G2265_C44294,G2265_C44295,G2265_C44296,G2265_C44297,G2265_C44298,G2265_C44299 FROM '.$BaseDatos.'.G2265 WHERE G2265_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2265_C44271'] = $key->G2265_C44271;

                $datos[$i]['G2265_C44272'] = $key->G2265_C44272;

                $datos[$i]['G2265_C44273'] = $key->G2265_C44273;

                $datos[$i]['G2265_C44274'] = $key->G2265_C44274;

                $datos[$i]['G2265_C44275'] = $key->G2265_C44275;

                $datos[$i]['G2265_C44276'] = $key->G2265_C44276;

                $datos[$i]['G2265_C44277'] = $key->G2265_C44277;

                $datos[$i]['G2265_C44278'] = $key->G2265_C44278;

                $datos[$i]['G2265_C44279'] = $key->G2265_C44279;

                $datos[$i]['G2265_C44280'] = $key->G2265_C44280;

                $datos[$i]['G2265_C44281'] = $key->G2265_C44281;

                $datos[$i]['G2265_C44282'] = $key->G2265_C44282;

                $datos[$i]['G2265_C44283'] = $key->G2265_C44283;

                $datos[$i]['G2265_C44284'] = $key->G2265_C44284;

                $datos[$i]['G2265_C44285'] = $key->G2265_C44285;

                $datos[$i]['G2265_C44260'] = $key->G2265_C44260;

                $datos[$i]['G2265_C44261'] = $key->G2265_C44261;

                $datos[$i]['G2265_C44262'] = explode(' ', $key->G2265_C44262)[0];
  
                $hora = '';
                if(!is_null($key->G2265_C44263)){
                    $hora = explode(' ', $key->G2265_C44263)[1];
                }

                $datos[$i]['G2265_C44263'] = $hora;

                $datos[$i]['G2265_C44264'] = $key->G2265_C44264;

                $datos[$i]['G2265_C44265'] = $key->G2265_C44265;

                $datos[$i]['G2265_C44266'] = $key->G2265_C44266;

                $datos[$i]['G2265_C44267'] = $key->G2265_C44267;

                $datos[$i]['G2265_C44268'] = $key->G2265_C44268;

                $datos[$i]['G2265_C44286'] = $key->G2265_C44286;

                $datos[$i]['G2265_C44287'] = $key->G2265_C44287;

                $datos[$i]['G2265_C44288'] = $key->G2265_C44288;

                $datos[$i]['G2265_C44289'] = explode(' ', $key->G2265_C44289)[0];

                $datos[$i]['G2265_C44290'] = $key->G2265_C44290;

                $datos[$i]['G2265_C44291'] = $key->G2265_C44291;

                $datos[$i]['G2265_C44292'] = $key->G2265_C44292;

                $datos[$i]['G2265_C44293'] = $key->G2265_C44293;

                $datos[$i]['G2265_C44294'] = $key->G2265_C44294;

                $datos[$i]['G2265_C44295'] = $key->G2265_C44295;

                $datos[$i]['G2265_C44296'] = explode(' ', $key->G2265_C44296)[0];
  
                $hora = '';
                if(!is_null($key->G2265_C44297)){
                    $hora = explode(' ', $key->G2265_C44297)[1];
                }

                $datos[$i]['G2265_C44297'] = $hora;

                $datos[$i]['G2265_C44298'] = $key->G2265_C44298;

                $datos[$i]['G2265_C44299'] = $key->G2265_C44299;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $PEOBUS = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2265";

            $PEOBUS = $mysqli->query($PEOBUS);
            $PEOBUS = $PEOBUS->fetch_array();

            if ($PEOBUS["reg"] != 0) {
                $regProp = " AND G2265_Usuario = ".$_POST["idUsuario"]." ";
            }else{
                $regProp = "";
            }

            //JDBD valores filtros.
            $B = $_POST["B"];
            $A = $_POST["A"];
            $T = $_POST["T"];
            $F = $_POST["F"];
            $E = $_POST["E"];


            $Lsql = "SELECT G2265_ConsInte__b as id,  G2265_C44271 as camp2 , G2265_C44274 as camp1 
                     FROM ".$BaseDatos.".G2265  WHERE TRUE ".$regProp;

            if ($B != "" && $B != NULL) {
                $Lsql .= " AND (G2265_C44271 LIKE '%".$B."%' OR G2265_C44274 LIKE '%".$B."%') ";
            }
            if ($A != 0 && $A != -1 && $A != NULL) {
                $Lsql .= " AND G2265_Usuario = ".$A." ";
            }
            if ($T != 0 && $T != -1 && $T != NULL) {
                $Lsql .= " AND G2265_C44260 = ".$T." ";
            }
            if ($F != "" && $F != NULL) {
                $Lsql .= " AND DATE_FORMAT(G2265_FechaInsercion,'%Y-%m-%d') = '".$F."' ";
            }
            if ($E != 0 && $E != -1 && $E != NULL) {
                if ($E == -203) {
                    $Lsql .= " AND G2265_C = -203 OR G2265_C = '' OR G2265_C IS NULL ";
                }else{
                    $Lsql .= " AND G2265_C = ".$E." "; 
                }
            }


            $Lsql .= " ORDER BY G2265_ConsInte__b DESC LIMIT 0, 50 "; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2265");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2265_ConsInte__b, G2265_FechaInsercion , G2265_Usuario ,  G2265_CodigoMiembro  , G2265_PoblacionOrigen , G2265_EstadoDiligenciamiento ,  G2265_IdLlamada , G2265_C44274 as principal ,G2265_C44271, a.LISOPC_Nombre____b as G2265_C44272,G2265_C44273,G2265_C44274,G2265_C44275,G2265_C44276,G2265_C44277,G2265_C44278, b.LISOPC_Nombre____b as G2265_C44279, c.LISOPC_Nombre____b as G2265_C44280,G2265_C44281, d.LISOPC_Nombre____b as G2265_C44282, e.LISOPC_Nombre____b as G2265_C44283,G2265_C44284,G2265_C44285, f.LISOPC_Nombre____b as G2265_C44260, g.LISOPC_Nombre____b as G2265_C44261,G2265_C44262,G2265_C44263,G2265_C44264,G2265_C44265,G2265_C44266,G2265_C44267,G2265_C44268,G2265_C44286,G2265_C44287,G2265_C44288,G2265_C44289, h.LISOPC_Nombre____b as G2265_C44290, i.LISOPC_Nombre____b as G2265_C44291, j.LISOPC_Nombre____b as G2265_C44292,G2265_C44293,G2265_C44294,G2265_C44295,G2265_C44296,G2265_C44297,G2265_C44298,G2265_C44299 FROM '.$BaseDatos.'.G2265 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2265_C44272 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2265_C44279 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2265_C44280 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2265_C44282 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2265_C44283 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G2265_C44260 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G2265_C44261 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G2265_C44290 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G2265_C44291 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G2265_C44292';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2265_C44263)){
                    $hora_a = explode(' ', $fila->G2265_C44263)[1];
                }

                $hora_b = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2265_C44297)){
                    $hora_b = explode(' ', $fila->G2265_C44297)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2265_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2265_ConsInte__b , ($fila->G2265_C44271) , ($fila->G2265_C44272) , ($fila->G2265_C44273) , ($fila->G2265_C44274) , ($fila->G2265_C44275) , ($fila->G2265_C44276) , ($fila->G2265_C44277) , ($fila->G2265_C44278) , ($fila->G2265_C44279) , ($fila->G2265_C44280) , ($fila->G2265_C44281) , ($fila->G2265_C44282) , ($fila->G2265_C44283) , ($fila->G2265_C44284) , ($fila->G2265_C44285) , ($fila->G2265_C44260) , ($fila->G2265_C44261) , explode(' ', $fila->G2265_C44262)[0] , $hora_a , ($fila->G2265_C44264) , ($fila->G2265_C44265) , ($fila->G2265_C44266) , ($fila->G2265_C44267) , ($fila->G2265_C44268) , ($fila->G2265_C44286) , ($fila->G2265_C44287) , ($fila->G2265_C44288) , explode(' ', $fila->G2265_C44289)[0] , ($fila->G2265_C44290) , ($fila->G2265_C44291) , ($fila->G2265_C44292) , ($fila->G2265_C44293) , ($fila->G2265_C44294) , ($fila->G2265_C44295) , explode(' ', $fila->G2265_C44296)[0] , $hora_b , ($fila->G2265_C44298) , ($fila->G2265_C44299) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2265 WHERE G2265_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            $PEOBUS = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2265";

            $PEOBUS = $mysqli->query($PEOBUS);
            $PEOBUS = $PEOBUS->fetch_array();

            if ($PEOBUS["reg"] != 0) {
                $regProp = ' AND G2265_Usuario = '.$_POST["idUsuario"].' ';
            }else{
                $regProp = '';
            }

            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $A = 0;
            $T = 0;
            $F = "";
            $B = "";
            $E = 0;

            if (isset($_POST["A"])) {
                $A = $_POST["A"];
            }
            if (isset($_POST["T"])) {
                $T = $_POST["T"];
            }
            if (isset($_POST["F"])) {
                $F = $_POST["F"];
            }
            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            if (isset($_POST["E"])) {
                $E = $_POST["E"];
            }

            $Zsql = 'SELECT  G2265_ConsInte__b as id,  G2265_C44271 as camp2 , G2265_C44274 as camp1  FROM '.$BaseDatos.'.G2265 WHERE TRUE'.$regProp; 

            if ($A != 0) {
                $Zsql .= ' AND G2265_Usuario = '.$A.' ';
            }

            if ($T != 0) {
                $Zsql .= ' AND G2265_C44260 = '.$T.' ';
            }

            if ($F != "") {
                $Zsql .= ' AND DATE_FORMAT(G2265_FechaInsercion,"%Y-%m-%d") = "'.$F.'" ';
            }

            if ($B != "") {
                $Zsql .= ' AND (G2265_C44271 LIKE "%'.$B.'%" OR G2265_C44274 LIKE "%'.$B.'%") ';
            }

            if ($E != 0) {
                if ($E == -203) {
                    $Zsql .= ' AND G2265_C = -203 OR G2265_C = "" OR G2265_C IS NULL ';
                }else{
                    $Zsql .= ' AND G2265_C = '.$E.'  ';
                }
            }

            $Zsql .= ' ORDER BY G2265_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2265 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2265(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2265_C44271"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44271 = '".$_POST["G2265_C44271"]."'";
                $LsqlI .= $separador."G2265_C44271";
                $LsqlV .= $separador."'".$_POST["G2265_C44271"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44272"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44272 = '".$_POST["G2265_C44272"]."'";
                $LsqlI .= $separador."G2265_C44272";
                $LsqlV .= $separador."'".$_POST["G2265_C44272"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44273"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44273 = '".$_POST["G2265_C44273"]."'";
                $LsqlI .= $separador."G2265_C44273";
                $LsqlV .= $separador."'".$_POST["G2265_C44273"]."'";
                $validar = 1;
            }
             
  
            $G2265_C44274 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2265_C44274"])){
                if($_POST["G2265_C44274"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2265_C44274 = $_POST["G2265_C44274"];
                    $LsqlU .= $separador." G2265_C44274 = '".$G2265_C44274."'";
                    $LsqlI .= $separador." G2265_C44274";
                    $LsqlV .= $separador."'".$G2265_C44274."'";
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2265_C44275"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44275 = '".$_POST["G2265_C44275"]."'";
                $LsqlI .= $separador."G2265_C44275";
                $LsqlV .= $separador."'".$_POST["G2265_C44275"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44276"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44276 = '".$_POST["G2265_C44276"]."'";
                $LsqlI .= $separador."G2265_C44276";
                $LsqlV .= $separador."'".$_POST["G2265_C44276"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44277"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44277 = '".$_POST["G2265_C44277"]."'";
                $LsqlI .= $separador."G2265_C44277";
                $LsqlV .= $separador."'".$_POST["G2265_C44277"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44278"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44278 = '".$_POST["G2265_C44278"]."'";
                $LsqlI .= $separador."G2265_C44278";
                $LsqlV .= $separador."'".$_POST["G2265_C44278"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44279"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44279 = '".$_POST["G2265_C44279"]."'";
                $LsqlI .= $separador."G2265_C44279";
                $LsqlV .= $separador."'".$_POST["G2265_C44279"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44280"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44280 = '".$_POST["G2265_C44280"]."'";
                $LsqlI .= $separador."G2265_C44280";
                $LsqlV .= $separador."'".$_POST["G2265_C44280"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44281"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44281 = '".$_POST["G2265_C44281"]."'";
                $LsqlI .= $separador."G2265_C44281";
                $LsqlV .= $separador."'".$_POST["G2265_C44281"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44282"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44282 = '".$_POST["G2265_C44282"]."'";
                $LsqlI .= $separador."G2265_C44282";
                $LsqlV .= $separador."'".$_POST["G2265_C44282"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44283"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44283 = '".$_POST["G2265_C44283"]."'";
                $LsqlI .= $separador."G2265_C44283";
                $LsqlV .= $separador."'".$_POST["G2265_C44283"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44284"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44284 = '".$_POST["G2265_C44284"]."'";
                $LsqlI .= $separador."G2265_C44284";
                $LsqlV .= $separador."'".$_POST["G2265_C44284"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44285"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44285 = '".$_POST["G2265_C44285"]."'";
                $LsqlI .= $separador."G2265_C44285";
                $LsqlV .= $separador."'".$_POST["G2265_C44285"]."'";
                $validar = 1;
            }
             
 
            $G2265_C44260 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2265_C44260 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2265_C44260 = ".$G2265_C44260;
                    $LsqlI .= $separador." G2265_C44260";
                    $LsqlV .= $separador.$G2265_C44260;
                    $validar = 1;

                    
                }
            }
 
            $G2265_C44261 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2265_C44261 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2265_C44261 = ".$G2265_C44261;
                    $LsqlI .= $separador." G2265_C44261";
                    $LsqlV .= $separador.$G2265_C44261;
                    $validar = 1;
                }
            }
 
            $G2265_C44262 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2265_C44262 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2265_C44262 = ".$G2265_C44262;
                    $LsqlI .= $separador." G2265_C44262";
                    $LsqlV .= $separador.$G2265_C44262;
                    $validar = 1;
                }
            }
 
            $G2265_C44263 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2265_C44263 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2265_C44263 = ".$G2265_C44263;
                    $LsqlI .= $separador." G2265_C44263";
                    $LsqlV .= $separador.$G2265_C44263;
                    $validar = 1;
                }
            }
 
            $G2265_C44264 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2265_C44264 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2265_C44264 = ".$G2265_C44264;
                    $LsqlI .= $separador." G2265_C44264";
                    $LsqlV .= $separador.$G2265_C44264;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2265_C44265"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44265 = '".$_POST["G2265_C44265"]."'";
                $LsqlI .= $separador."G2265_C44265";
                $LsqlV .= $separador."'".$_POST["G2265_C44265"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44266"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44266 = '".$_POST["G2265_C44266"]."'";
                $LsqlI .= $separador."G2265_C44266";
                $LsqlV .= $separador."'".$_POST["G2265_C44266"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44267"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44267 = '".$_POST["G2265_C44267"]."'";
                $LsqlI .= $separador."G2265_C44267";
                $LsqlV .= $separador."'".$_POST["G2265_C44267"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44268"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44268 = '".$_POST["G2265_C44268"]."'";
                $LsqlI .= $separador."G2265_C44268";
                $LsqlV .= $separador."'".$_POST["G2265_C44268"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44286"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44286 = '".$_POST["G2265_C44286"]."'";
                $LsqlI .= $separador."G2265_C44286";
                $LsqlV .= $separador."'".$_POST["G2265_C44286"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44287"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44287 = '".$_POST["G2265_C44287"]."'";
                $LsqlI .= $separador."G2265_C44287";
                $LsqlV .= $separador."'".$_POST["G2265_C44287"]."'";
                $validar = 1;
            }
             
  
            $G2265_C44288 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2265_C44288"])){
                if($_POST["G2265_C44288"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2265_C44288 = $_POST["G2265_C44288"];
                    $LsqlU .= $separador." G2265_C44288 = '".$G2265_C44288."'";
                    $LsqlI .= $separador." G2265_C44288";
                    $LsqlV .= $separador."'".$G2265_C44288."'";
                    $validar = 1;
                }
            }
 
            $G2265_C44289 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2265_C44289"])){    
                if($_POST["G2265_C44289"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2265_C44289"]);
                    if(count($tieneHora) > 1){
                        $G2265_C44289 = "'".$_POST["G2265_C44289"]."'";
                    }else{
                        $G2265_C44289 = "'".str_replace(' ', '',$_POST["G2265_C44289"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2265_C44289 = ".$G2265_C44289;
                    $LsqlI .= $separador." G2265_C44289";
                    $LsqlV .= $separador.$G2265_C44289;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2265_C44290"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44290 = '".$_POST["G2265_C44290"]."'";
                $LsqlI .= $separador."G2265_C44290";
                $LsqlV .= $separador."'".$_POST["G2265_C44290"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44291"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44291 = '".$_POST["G2265_C44291"]."'";
                $LsqlI .= $separador."G2265_C44291";
                $LsqlV .= $separador."'".$_POST["G2265_C44291"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44292"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44292 = '".$_POST["G2265_C44292"]."'";
                $LsqlI .= $separador."G2265_C44292";
                $LsqlV .= $separador."'".$_POST["G2265_C44292"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44293"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44293 = '".$_POST["G2265_C44293"]."'";
                $LsqlI .= $separador."G2265_C44293";
                $LsqlV .= $separador."'".$_POST["G2265_C44293"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44294"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44294 = '".$_POST["G2265_C44294"]."'";
                $LsqlI .= $separador."G2265_C44294";
                $LsqlV .= $separador."'".$_POST["G2265_C44294"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44295"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44295 = '".$_POST["G2265_C44295"]."'";
                $LsqlI .= $separador."G2265_C44295";
                $LsqlV .= $separador."'".$_POST["G2265_C44295"]."'";
                $validar = 1;
            }
             
 
            $G2265_C44296 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2265_C44296"])){    
                if($_POST["G2265_C44296"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2265_C44296"]);
                    if(count($tieneHora) > 1){
                        $G2265_C44296 = "'".$_POST["G2265_C44296"]."'";
                    }else{
                        $G2265_C44296 = "'".str_replace(' ', '',$_POST["G2265_C44296"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2265_C44296 = ".$G2265_C44296;
                    $LsqlI .= $separador." G2265_C44296";
                    $LsqlV .= $separador.$G2265_C44296;
                    $validar = 1;
                }
            }
  
            $G2265_C44297 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G2265_C44297"])){   
                if($_POST["G2265_C44297"] != '' && $_POST["G2265_C44297"] != 'undefined' && $_POST["G2265_C44297"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2265_C44297 = "'".$fecha." ".str_replace(' ', '',$_POST["G2265_C44297"])."'";
                    $LsqlU .= $separador." G2265_C44297 = ".$G2265_C44297."";
                    $LsqlI .= $separador." G2265_C44297";
                    $LsqlV .= $separador.$G2265_C44297;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2265_C44298"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44298 = '".$_POST["G2265_C44298"]."'";
                $LsqlI .= $separador."G2265_C44298";
                $LsqlV .= $separador."'".$_POST["G2265_C44298"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2265_C44299"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_C44299 = '".$_POST["G2265_C44299"]."'";
                $LsqlI .= $separador."G2265_C44299";
                $LsqlV .= $separador."'".$_POST["G2265_C44299"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_TipNo_Efe_b, MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $reintento = $dataMonoEf->MONOEF_TipNo_Efe_b;
                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2265_C44261 = ".$reintento;
                    $LsqlI .= $separador."G2265_C44261";
                    $LsqlV .= $separador.$reintento;
                    $validar = 1;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2265_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2265_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2265_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2265_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2265_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2265_Usuario , G2265_FechaInsercion, G2265_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2265_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2265 WHERE G2265_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {

                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }          
                    }

                    if (isset($_POST["totalVehiculos"])) {
                        
                        $intCantVe_t = (INT)$_POST["totalVehiculos"];

                        for ($i=1; $i <= $intCantVe_t; $i++) { 
                            
                            $intIdVehiculo_t = $_POST["IdVehiculo".$i];
                            $strFechaAgenda_t = $_POST["fechaAgenda".$i];
                            $strHoraAgenda_t = $_POST["horaAgenda".$i];

                            if ($strFechaAgenda_t != "" && $strHoraAgenda_t != "") {

                                $strSQLUpdateMuestra_t = "UPDATE ".$BaseDatos.".G2264_M1587 SET G2264_M1587_Estado____b = 2, G2264_M1587_FecHorAge_b = '".$strFechaAgenda_t." ".str_replace(" ", "",$strHoraAgenda_t)."' WHERE G2264_M1587_CoInMiPo__b = ".$intIdVehiculo_t;

                                $strSQLUpdateBase_t = "UPDATE ".$BaseDatos.".G2264 SET G2264_C44256 = '".$strFechaAgenda_t." ".str_replace(" ", "",$strHoraAgenda_t)."', G2264_C44257 = '".$strFechaAgenda_t." ".str_replace(" ", "",$strHoraAgenda_t)."' WHERE G2264_ConsInte__b = ".$intIdVehiculo_t;

                                $mysqli->query($strSQLUpdateMuestra_t);
                                $mysqli->query($strSQLUpdateBase_t);
                            }
                            

                        }

                    }

                    echo $UltimoID;


                    

                } else {
                    echo '0';
                }
            }        

        }
    }
  

  
?>
