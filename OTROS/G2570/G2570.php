
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G2570/G2570_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G2570_ConsInte__b as id, G2570_C50125 as camp2 , G2570_C50126 as camp1 FROM ".$BaseDatos.".G2570  WHERE G2570_Usuario = ".$idUsuario." ORDER BY G2570_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G2570_ConsInte__b as id, G2570_C50125 as camp2 , G2570_C50126 as camp1 FROM ".$BaseDatos.".G2570  ORDER BY G2570_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G2570_ConsInte__b as id, G2570_C50125 as camp2 , G2570_C50126 as camp1 FROM ".$BaseDatos.".G2570 JOIN ".$BaseDatos.".G2570_M".$resultEstpas->muestr." ON G2570_ConsInte__b = G2570_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G2570_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G2570_ConsInte__b as id, G2570_C50125 as camp2 , G2570_C50126 as camp1 FROM ".$BaseDatos.".G2570 JOIN ".$BaseDatos.".G2570_M".$resultEstpas->muestr." ON G2570_ConsInte__b = G2570_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G2570_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G2570_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G2570_ConsInte__b as id, G2570_C50125 as camp2 , G2570_C50126 as camp1 FROM ".$BaseDatos.".G2570  ORDER BY G2570_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  class="panel box box-primary" id="7571" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_7571c">
                CO-DEUDOR
            </a>
        </h4>
        
    </div>
    <div id="s_7571c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50125" id="LblG2570_C50125">OPERACION</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50125" value="<?php if (isset($_GET['G2570_C50125'])) {
                            echo $_GET['G2570_C50125'];
                        } ?>" readonly name="G2570_C50125"  placeholder="OPERACION">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50126" id="LblG2570_C50126">CEDULA CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50126" value="<?php if (isset($_GET['G2570_C50126'])) {
                            echo $_GET['G2570_C50126'];
                        } ?>" readonly name="G2570_C50126"  placeholder="CEDULA CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50127" id="LblG2570_C50127">NOMBRE CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50127" value="<?php if (isset($_GET['G2570_C50127'])) {
                            echo $_GET['G2570_C50127'];
                        } ?>" readonly name="G2570_C50127"  placeholder="NOMBRE CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50128" id="LblG2570_C50128">CIUDAD RESIDENCIAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50128" value="<?php if (isset($_GET['G2570_C50128'])) {
                            echo $_GET['G2570_C50128'];
                        } ?>" readonly name="G2570_C50128"  placeholder="CIUDAD RESIDENCIAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50129" id="LblG2570_C50129">CIUDAD LABORAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50129" value="<?php if (isset($_GET['G2570_C50129'])) {
                            echo $_GET['G2570_C50129'];
                        } ?>" readonly name="G2570_C50129"  placeholder="CIUDAD LABORAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50130" id="LblG2570_C50130">CIUDAD COMERCIAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50130" value="<?php if (isset($_GET['G2570_C50130'])) {
                            echo $_GET['G2570_C50130'];
                        } ?>" readonly name="G2570_C50130"  placeholder="CIUDAD COMERCIAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50131" id="LblG2570_C50131">DR. RESIDENCIAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50131" value="<?php if (isset($_GET['G2570_C50131'])) {
                            echo $_GET['G2570_C50131'];
                        } ?>" readonly name="G2570_C50131"  placeholder="DR. RESIDENCIAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50132" id="LblG2570_C50132">DR: LABORAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50132" value="<?php if (isset($_GET['G2570_C50132'])) {
                            echo $_GET['G2570_C50132'];
                        } ?>" readonly name="G2570_C50132"  placeholder="DR: LABORAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50133" id="LblG2570_C50133">DR: COMERCIAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50133" value="<?php if (isset($_GET['G2570_C50133'])) {
                            echo $_GET['G2570_C50133'];
                        } ?>" readonly name="G2570_C50133"  placeholder="DR: COMERCIAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50134" id="LblG2570_C50134">BARRIO RESIDENCIAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50134" value="<?php if (isset($_GET['G2570_C50134'])) {
                            echo $_GET['G2570_C50134'];
                        } ?>" readonly name="G2570_C50134"  placeholder="BARRIO RESIDENCIAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50135" id="LblG2570_C50135">BARRIO LABORAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50135" value="<?php if (isset($_GET['G2570_C50135'])) {
                            echo $_GET['G2570_C50135'];
                        } ?>" readonly name="G2570_C50135"  placeholder="BARRIO LABORAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50139" id="LblG2570_C50139">TEL. COMERCIAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50139" value="<?php if (isset($_GET['G2570_C50139'])) {
                            echo $_GET['G2570_C50139'];
                        } ?>" readonly name="G2570_C50139"  placeholder="TEL. COMERCIAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50140" id="LblG2570_C50140">CELULAR CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50140" value="<?php if (isset($_GET['G2570_C50140'])) {
                            echo $_GET['G2570_C50140'];
                        } ?>" readonly name="G2570_C50140"  placeholder="CELULAR CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50136" id="LblG2570_C50136">BARRIO COMERCIAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50136" value="<?php if (isset($_GET['G2570_C50136'])) {
                            echo $_GET['G2570_C50136'];
                        } ?>" readonly name="G2570_C50136"  placeholder="BARRIO COMERCIAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50137" id="LblG2570_C50137">TEL. RESIDENCIAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50137" value="<?php if (isset($_GET['G2570_C50137'])) {
                            echo $_GET['G2570_C50137'];
                        } ?>" readonly name="G2570_C50137"  placeholder="TEL. RESIDENCIAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50138" id="LblG2570_C50138">TEL. LABORAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50138" value="<?php if (isset($_GET['G2570_C50138'])) {
                            echo $_GET['G2570_C50138'];
                        } ?>" readonly name="G2570_C50138"  placeholder="TEL. LABORAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="7572" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50122" id="LblG2570_C50122">ORIGEN_DY_WF</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50122" value="<?php if (isset($_GET['G2570_C50122'])) {
                            echo $_GET['G2570_C50122'];
                        } ?>" readonly name="G2570_C50122"  placeholder="ORIGEN_DY_WF">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50123" id="LblG2570_C50123">OPTIN_DY_WF</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50123" value="<?php if (isset($_GET['G2570_C50123'])) {
                            echo $_GET['G2570_C50123'];
                        } ?>" readonly name="G2570_C50123"  placeholder="OPTIN_DY_WF">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2570_C50124" id="LblG2570_C50124">ESTADO_DY</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G2570_C50124" id="G2570_C50124">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2882 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="7677" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_7677c">
                ACTUALIZACION DATOS CO-DEUDOR
            </a>
        </h4>
        
    </div>
    <div id="s_7677c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50661" id="LblG2570_C50661">A.CEDULA CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50661" value="<?php if (isset($_GET['G2570_C50661'])) {
                            echo $_GET['G2570_C50661'];
                        } ?>"  name="G2570_C50661"  placeholder="A.CEDULA CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50662" id="LblG2570_C50662">A.NOMBRE CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50662" value="<?php if (isset($_GET['G2570_C50662'])) {
                            echo $_GET['G2570_C50662'];
                        } ?>"  name="G2570_C50662"  placeholder="A.NOMBRE CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50663" id="LblG2570_C50663">A.CIUDAD RESIDENCIAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50663" value="<?php if (isset($_GET['G2570_C50663'])) {
                            echo $_GET['G2570_C50663'];
                        } ?>"  name="G2570_C50663"  placeholder="A.CIUDAD RESIDENCIAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50664" id="LblG2570_C50664">A.CIUDAD LABORAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50664" value="<?php if (isset($_GET['G2570_C50664'])) {
                            echo $_GET['G2570_C50664'];
                        } ?>"  name="G2570_C50664"  placeholder="A.CIUDAD LABORAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50665" id="LblG2570_C50665">A.CIUDAD COMERCIAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50665" value="<?php if (isset($_GET['G2570_C50665'])) {
                            echo $_GET['G2570_C50665'];
                        } ?>"  name="G2570_C50665"  placeholder="A.CIUDAD COMERCIAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50666" id="LblG2570_C50666">A.DR. RESIDENCIAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50666" value="<?php if (isset($_GET['G2570_C50666'])) {
                            echo $_GET['G2570_C50666'];
                        } ?>"  name="G2570_C50666"  placeholder="A.DR. RESIDENCIAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50667" id="LblG2570_C50667">A.DR: LABORAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50667" value="<?php if (isset($_GET['G2570_C50667'])) {
                            echo $_GET['G2570_C50667'];
                        } ?>"  name="G2570_C50667"  placeholder="A.DR: LABORAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50668" id="LblG2570_C50668">A.DR: COMERCIAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50668" value="<?php if (isset($_GET['G2570_C50668'])) {
                            echo $_GET['G2570_C50668'];
                        } ?>"  name="G2570_C50668"  placeholder="A.DR: COMERCIAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50669" id="LblG2570_C50669">A.BARRIO RESIDENCIAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50669" value="<?php if (isset($_GET['G2570_C50669'])) {
                            echo $_GET['G2570_C50669'];
                        } ?>"  name="G2570_C50669"  placeholder="A.BARRIO RESIDENCIAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50670" id="LblG2570_C50670">A.BARRIO LABORAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50670" value="<?php if (isset($_GET['G2570_C50670'])) {
                            echo $_GET['G2570_C50670'];
                        } ?>"  name="G2570_C50670"  placeholder="A.BARRIO LABORAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50671" id="LblG2570_C50671">A.TEL. COMERCIAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50671" value="<?php if (isset($_GET['G2570_C50671'])) {
                            echo $_GET['G2570_C50671'];
                        } ?>"  name="G2570_C50671"  placeholder="A.TEL. COMERCIAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50672" id="LblG2570_C50672">A.CELULAR CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50672" value="<?php if (isset($_GET['G2570_C50672'])) {
                            echo $_GET['G2570_C50672'];
                        } ?>"  name="G2570_C50672"  placeholder="A.CELULAR CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50673" id="LblG2570_C50673">A.BARRIO COMERCIAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50673" value="<?php if (isset($_GET['G2570_C50673'])) {
                            echo $_GET['G2570_C50673'];
                        } ?>"  name="G2570_C50673"  placeholder="A.BARRIO COMERCIAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50674" id="LblG2570_C50674">A.TEL. RESIDENCIAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50674" value="<?php if (isset($_GET['G2570_C50674'])) {
                            echo $_GET['G2570_C50674'];
                        } ?>"  name="G2570_C50674"  placeholder="A.TEL. RESIDENCIAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G2570_C50675" id="LblG2570_C50675">A.TEL. LABORAL CO.</label>
                        <input type="text" class="form-control input-sm" id="G2570_C50675" value="<?php if (isset($_GET['G2570_C50675'])) {
                            echo $_GET['G2570_C50675'];
                        } ?>"  name="G2570_C50675"  placeholder="A.TEL. LABORAL CO.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G2570/G2570_eventos.js"></script> 
<script type="text/javascript">

    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la sección de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G2570_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G2570_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G2570_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G2570_C50125").val(item.G2570_C50125); 
                $("#G2570_C50126").val(item.G2570_C50126); 
                $("#G2570_C50127").val(item.G2570_C50127); 
                $("#G2570_C50128").val(item.G2570_C50128); 
                $("#G2570_C50129").val(item.G2570_C50129); 
                $("#G2570_C50130").val(item.G2570_C50130); 
                $("#G2570_C50131").val(item.G2570_C50131); 
                $("#G2570_C50132").val(item.G2570_C50132); 
                $("#G2570_C50133").val(item.G2570_C50133); 
                $("#G2570_C50134").val(item.G2570_C50134); 
                $("#G2570_C50135").val(item.G2570_C50135); 
                $("#G2570_C50139").val(item.G2570_C50139); 
                $("#G2570_C50140").val(item.G2570_C50140); 
                $("#G2570_C50136").val(item.G2570_C50136); 
                $("#G2570_C50137").val(item.G2570_C50137); 
                $("#G2570_C50138").val(item.G2570_C50138); 
                $("#G2570_C50122").val(item.G2570_C50122); 
                $("#G2570_C50123").val(item.G2570_C50123); 
                $("#G2570_C50124").val(item.G2570_C50124).trigger("change");  
                $("#G2570_C50661").val(item.G2570_C50661); 
                $("#G2570_C50662").val(item.G2570_C50662); 
                $("#G2570_C50663").val(item.G2570_C50663); 
                $("#G2570_C50664").val(item.G2570_C50664); 
                $("#G2570_C50665").val(item.G2570_C50665); 
                $("#G2570_C50666").val(item.G2570_C50666); 
                $("#G2570_C50667").val(item.G2570_C50667); 
                $("#G2570_C50668").val(item.G2570_C50668); 
                $("#G2570_C50669").val(item.G2570_C50669); 
                $("#G2570_C50670").val(item.G2570_C50670); 
                $("#G2570_C50671").val(item.G2570_C50671); 
                $("#G2570_C50672").val(item.G2570_C50672); 
                $("#G2570_C50673").val(item.G2570_C50673); 
                $("#G2570_C50674").val(item.G2570_C50674); 
                $("#G2570_C50675").val(item.G2570_C50675);
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G2570_C50124").select2();
        //datepickers
        

        //Timepickers
        


        //Validaciones numeros Enteros
        


        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO_DY 

    $("#G2570_C50124").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificación!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                var sendMessage = function (msg) {
                                    // Make sure you are sending a string, and to stringify JSON
                                    window.parent.postMessage(msg, '*');
                                };
                                sendMessage('Cierrame');
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G2570_C50125").val(item.G2570_C50125);
 
                                                $("#G2570_C50126").val(item.G2570_C50126);
 
                                                $("#G2570_C50127").val(item.G2570_C50127);
 
                                                $("#G2570_C50128").val(item.G2570_C50128);
 
                                                $("#G2570_C50129").val(item.G2570_C50129);
 
                                                $("#G2570_C50130").val(item.G2570_C50130);
 
                                                $("#G2570_C50131").val(item.G2570_C50131);
 
                                                $("#G2570_C50132").val(item.G2570_C50132);
 
                                                $("#G2570_C50133").val(item.G2570_C50133);
 
                                                $("#G2570_C50134").val(item.G2570_C50134);
 
                                                $("#G2570_C50135").val(item.G2570_C50135);
 
                                                $("#G2570_C50139").val(item.G2570_C50139);
 
                                                $("#G2570_C50140").val(item.G2570_C50140);
 
                                                $("#G2570_C50136").val(item.G2570_C50136);
 
                                                $("#G2570_C50137").val(item.G2570_C50137);
 
                                                $("#G2570_C50138").val(item.G2570_C50138);
 
                                                $("#G2570_C50122").val(item.G2570_C50122);
 
                                                $("#G2570_C50123").val(item.G2570_C50123);
 
                    $("#G2570_C50124").val(item.G2570_C50124).trigger("change"); 
 
                                                $("#G2570_C50661").val(item.G2570_C50661);
 
                                                $("#G2570_C50662").val(item.G2570_C50662);
 
                                                $("#G2570_C50663").val(item.G2570_C50663);
 
                                                $("#G2570_C50664").val(item.G2570_C50664);
 
                                                $("#G2570_C50665").val(item.G2570_C50665);
 
                                                $("#G2570_C50666").val(item.G2570_C50666);
 
                                                $("#G2570_C50667").val(item.G2570_C50667);
 
                                                $("#G2570_C50668").val(item.G2570_C50668);
 
                                                $("#G2570_C50669").val(item.G2570_C50669);
 
                                                $("#G2570_C50670").val(item.G2570_C50670);
 
                                                $("#G2570_C50671").val(item.G2570_C50671);
 
                                                $("#G2570_C50672").val(item.G2570_C50672);
 
                                                $("#G2570_C50673").val(item.G2570_C50673);
 
                                                $("#G2570_C50674").val(item.G2570_C50674);
 
                                                $("#G2570_C50675").val(item.G2570_C50675);
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
                                        type  : "post",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            console.log(xt);
                                            window.location.href = "quitar.php";
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la información');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','OPERACION','CEDULA CO.','NOMBRE CO.','CIUDAD RESIDENCIAL CO.','CIUDAD LABORAL CO.','CIUDAD COMERCIAL CO.','DR. RESIDENCIAL CO.','DR: LABORAL CO.','DR: COMERCIAL CO.','BARRIO RESIDENCIAL CO.','BARRIO LABORAL CO.','TEL. COMERCIAL CO.','CELULAR CO.','BARRIO COMERCIAL CO.','TEL. RESIDENCIAL CO.','TEL. LABORAL CO.','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY','A.CEDULA CO.','A.NOMBRE CO.','A.CIUDAD RESIDENCIAL CO.','A.CIUDAD LABORAL CO.','A.CIUDAD COMERCIAL CO.','A.DR. RESIDENCIAL CO.','A.DR: LABORAL CO.','A.DR: COMERCIAL CO.','A.BARRIO RESIDENCIAL CO.','A.BARRIO LABORAL CO.','A.TEL. COMERCIAL CO.','A.CELULAR CO.','A.BARRIO COMERCIAL CO.','A.TEL. RESIDENCIAL CO.','A.TEL. LABORAL CO.'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G2570_C50125', 
                        index: 'G2570_C50125', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50126', 
                        index: 'G2570_C50126', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50127', 
                        index: 'G2570_C50127', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50128', 
                        index: 'G2570_C50128', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50129', 
                        index: 'G2570_C50129', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50130', 
                        index: 'G2570_C50130', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50131', 
                        index: 'G2570_C50131', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50132', 
                        index: 'G2570_C50132', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50133', 
                        index: 'G2570_C50133', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50134', 
                        index: 'G2570_C50134', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50135', 
                        index: 'G2570_C50135', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50139', 
                        index: 'G2570_C50139', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50140', 
                        index: 'G2570_C50140', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50136', 
                        index: 'G2570_C50136', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50137', 
                        index: 'G2570_C50137', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50138', 
                        index: 'G2570_C50138', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50122', 
                        index: 'G2570_C50122', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50123', 
                        index: 'G2570_C50123', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50124', 
                        index:'G2570_C50124', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=2882&campo=G2570_C50124'
                        }
                    }

                    ,
                    { 
                        name:'G2570_C50661', 
                        index: 'G2570_C50661', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50662', 
                        index: 'G2570_C50662', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50663', 
                        index: 'G2570_C50663', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50664', 
                        index: 'G2570_C50664', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50665', 
                        index: 'G2570_C50665', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50666', 
                        index: 'G2570_C50666', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50667', 
                        index: 'G2570_C50667', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50668', 
                        index: 'G2570_C50668', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50669', 
                        index: 'G2570_C50669', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50670', 
                        index: 'G2570_C50670', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50671', 
                        index: 'G2570_C50671', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50672', 
                        index: 'G2570_C50672', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50673', 
                        index: 'G2570_C50673', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50674', 
                        index: 'G2570_C50674', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G2570_C50675', 
                        index: 'G2570_C50675', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G2570_C50126',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario;?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario;?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G2570_C50125").val(item.G2570_C50125);

                        $("#G2570_C50126").val(item.G2570_C50126);

                        $("#G2570_C50127").val(item.G2570_C50127);

                        $("#G2570_C50128").val(item.G2570_C50128);

                        $("#G2570_C50129").val(item.G2570_C50129);

                        $("#G2570_C50130").val(item.G2570_C50130);

                        $("#G2570_C50131").val(item.G2570_C50131);

                        $("#G2570_C50132").val(item.G2570_C50132);

                        $("#G2570_C50133").val(item.G2570_C50133);

                        $("#G2570_C50134").val(item.G2570_C50134);

                        $("#G2570_C50135").val(item.G2570_C50135);

                        $("#G2570_C50139").val(item.G2570_C50139);

                        $("#G2570_C50140").val(item.G2570_C50140);

                        $("#G2570_C50136").val(item.G2570_C50136);

                        $("#G2570_C50137").val(item.G2570_C50137);

                        $("#G2570_C50138").val(item.G2570_C50138);

                        $("#G2570_C50122").val(item.G2570_C50122);

                        $("#G2570_C50123").val(item.G2570_C50123);
 
                    $("#G2570_C50124").val(item.G2570_C50124).trigger("change"); 

                        $("#G2570_C50661").val(item.G2570_C50661);

                        $("#G2570_C50662").val(item.G2570_C50662);

                        $("#G2570_C50663").val(item.G2570_C50663);

                        $("#G2570_C50664").val(item.G2570_C50664);

                        $("#G2570_C50665").val(item.G2570_C50665);

                        $("#G2570_C50666").val(item.G2570_C50666);

                        $("#G2570_C50667").val(item.G2570_C50667);

                        $("#G2570_C50668").val(item.G2570_C50668);

                        $("#G2570_C50669").val(item.G2570_C50669);

                        $("#G2570_C50670").val(item.G2570_C50670);

                        $("#G2570_C50671").val(item.G2570_C50671);

                        $("#G2570_C50672").val(item.G2570_C50672);

                        $("#G2570_C50673").val(item.G2570_C50673);

                        $("#G2570_C50674").val(item.G2570_C50674);

                        $("#G2570_C50675").val(item.G2570_C50675);
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
