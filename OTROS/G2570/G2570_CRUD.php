<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2570_ConsInte__b, G2570_FechaInsercion , G2570_Usuario ,  G2570_CodigoMiembro  , G2570_PoblacionOrigen , G2570_EstadoDiligenciamiento ,  G2570_IdLlamada , G2570_C50126 as principal ,G2570_C50125,G2570_C50126,G2570_C50127,G2570_C50128,G2570_C50129,G2570_C50130,G2570_C50131,G2570_C50132,G2570_C50133,G2570_C50134,G2570_C50135,G2570_C50139,G2570_C50140,G2570_C50136,G2570_C50137,G2570_C50138,G2570_C50122,G2570_C50123,G2570_C50124,G2570_C50661,G2570_C50662,G2570_C50663,G2570_C50664,G2570_C50665,G2570_C50666,G2570_C50667,G2570_C50668,G2570_C50669,G2570_C50670,G2570_C50671,G2570_C50672,G2570_C50673,G2570_C50674,G2570_C50675 FROM '.$BaseDatos.'.G2570 WHERE G2570_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2570_C50125'] = $key->G2570_C50125;

                $datos[$i]['G2570_C50126'] = $key->G2570_C50126;

                $datos[$i]['G2570_C50127'] = $key->G2570_C50127;

                $datos[$i]['G2570_C50128'] = $key->G2570_C50128;

                $datos[$i]['G2570_C50129'] = $key->G2570_C50129;

                $datos[$i]['G2570_C50130'] = $key->G2570_C50130;

                $datos[$i]['G2570_C50131'] = $key->G2570_C50131;

                $datos[$i]['G2570_C50132'] = $key->G2570_C50132;

                $datos[$i]['G2570_C50133'] = $key->G2570_C50133;

                $datos[$i]['G2570_C50134'] = $key->G2570_C50134;

                $datos[$i]['G2570_C50135'] = $key->G2570_C50135;

                $datos[$i]['G2570_C50139'] = $key->G2570_C50139;

                $datos[$i]['G2570_C50140'] = $key->G2570_C50140;

                $datos[$i]['G2570_C50136'] = $key->G2570_C50136;

                $datos[$i]['G2570_C50137'] = $key->G2570_C50137;

                $datos[$i]['G2570_C50138'] = $key->G2570_C50138;

                $datos[$i]['G2570_C50122'] = $key->G2570_C50122;

                $datos[$i]['G2570_C50123'] = $key->G2570_C50123;

                $datos[$i]['G2570_C50124'] = $key->G2570_C50124;

                $datos[$i]['G2570_C50661'] = $key->G2570_C50661;

                $datos[$i]['G2570_C50662'] = $key->G2570_C50662;

                $datos[$i]['G2570_C50663'] = $key->G2570_C50663;

                $datos[$i]['G2570_C50664'] = $key->G2570_C50664;

                $datos[$i]['G2570_C50665'] = $key->G2570_C50665;

                $datos[$i]['G2570_C50666'] = $key->G2570_C50666;

                $datos[$i]['G2570_C50667'] = $key->G2570_C50667;

                $datos[$i]['G2570_C50668'] = $key->G2570_C50668;

                $datos[$i]['G2570_C50669'] = $key->G2570_C50669;

                $datos[$i]['G2570_C50670'] = $key->G2570_C50670;

                $datos[$i]['G2570_C50671'] = $key->G2570_C50671;

                $datos[$i]['G2570_C50672'] = $key->G2570_C50672;

                $datos[$i]['G2570_C50673'] = $key->G2570_C50673;

                $datos[$i]['G2570_C50674'] = $key->G2570_C50674;

                $datos[$i]['G2570_C50675'] = $key->G2570_C50675;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2570";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2570_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2570_ConsInte__b as id,  G2570_C50125 as camp2 , G2570_C50126 as camp1 
                     FROM ".$BaseDatos.".G2570  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2570_ConsInte__b as id,  G2570_C50125 as camp2 , G2570_C50126 as camp1  
                    FROM ".$BaseDatos.".G2570  JOIN ".$BaseDatos.".G2570_M".$_POST['muestra']." ON G2570_ConsInte__b = G2570_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2570_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2570_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2570_C50125 LIKE '%".$B."%' OR G2570_C50126 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2570_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2570");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2570_ConsInte__b, G2570_FechaInsercion , G2570_Usuario ,  G2570_CodigoMiembro  , G2570_PoblacionOrigen , G2570_EstadoDiligenciamiento ,  G2570_IdLlamada , G2570_C50126 as principal ,G2570_C50125,G2570_C50126,G2570_C50127,G2570_C50128,G2570_C50129,G2570_C50130,G2570_C50131,G2570_C50132,G2570_C50133,G2570_C50134,G2570_C50135,G2570_C50139,G2570_C50140,G2570_C50136,G2570_C50137,G2570_C50138,G2570_C50122,G2570_C50123, a.LISOPC_Nombre____b as G2570_C50124,G2570_C50661,G2570_C50662,G2570_C50663,G2570_C50664,G2570_C50665,G2570_C50666,G2570_C50667,G2570_C50668,G2570_C50669,G2570_C50670,G2570_C50671,G2570_C50672,G2570_C50673,G2570_C50674,G2570_C50675 FROM '.$BaseDatos.'.G2570 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2570_C50124';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G2570_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2570_ConsInte__b , ($fila->G2570_C50125) , ($fila->G2570_C50126) , ($fila->G2570_C50127) , ($fila->G2570_C50128) , ($fila->G2570_C50129) , ($fila->G2570_C50130) , ($fila->G2570_C50131) , ($fila->G2570_C50132) , ($fila->G2570_C50133) , ($fila->G2570_C50134) , ($fila->G2570_C50135) , ($fila->G2570_C50139) , ($fila->G2570_C50140) , ($fila->G2570_C50136) , ($fila->G2570_C50137) , ($fila->G2570_C50138) , ($fila->G2570_C50122) , ($fila->G2570_C50123) , ($fila->G2570_C50124) , ($fila->G2570_C50661) , ($fila->G2570_C50662) , ($fila->G2570_C50663) , ($fila->G2570_C50664) , ($fila->G2570_C50665) , ($fila->G2570_C50666) , ($fila->G2570_C50667) , ($fila->G2570_C50668) , ($fila->G2570_C50669) , ($fila->G2570_C50670) , ($fila->G2570_C50671) , ($fila->G2570_C50672) , ($fila->G2570_C50673) , ($fila->G2570_C50674) , ($fila->G2570_C50675) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2570 WHERE G2570_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2570";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2570_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2570_ConsInte__b as id,  G2570_C50125 as camp2 , G2570_C50126 as camp1  FROM '.$BaseDatos.'.G2570 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2570_ConsInte__b as id,  G2570_C50125 as camp2 , G2570_C50126 as camp1  
                    FROM ".$BaseDatos.".G2570  JOIN ".$BaseDatos.".G2570_M".$_POST['muestra']." ON G2570_ConsInte__b = G2570_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2570_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2570_C50125 LIKE "%'.$B.'%" OR G2570_C50126 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2570_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2570 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2570(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2570_C50125"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50125 = '".$_POST["G2570_C50125"]."'";
                $LsqlI .= $separador."G2570_C50125";
                $LsqlV .= $separador."'".$_POST["G2570_C50125"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50126"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50126 = '".$_POST["G2570_C50126"]."'";
                $LsqlI .= $separador."G2570_C50126";
                $LsqlV .= $separador."'".$_POST["G2570_C50126"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50127"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50127 = '".$_POST["G2570_C50127"]."'";
                $LsqlI .= $separador."G2570_C50127";
                $LsqlV .= $separador."'".$_POST["G2570_C50127"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50128"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50128 = '".$_POST["G2570_C50128"]."'";
                $LsqlI .= $separador."G2570_C50128";
                $LsqlV .= $separador."'".$_POST["G2570_C50128"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50129"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50129 = '".$_POST["G2570_C50129"]."'";
                $LsqlI .= $separador."G2570_C50129";
                $LsqlV .= $separador."'".$_POST["G2570_C50129"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50130"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50130 = '".$_POST["G2570_C50130"]."'";
                $LsqlI .= $separador."G2570_C50130";
                $LsqlV .= $separador."'".$_POST["G2570_C50130"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50131"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50131 = '".$_POST["G2570_C50131"]."'";
                $LsqlI .= $separador."G2570_C50131";
                $LsqlV .= $separador."'".$_POST["G2570_C50131"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50132"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50132 = '".$_POST["G2570_C50132"]."'";
                $LsqlI .= $separador."G2570_C50132";
                $LsqlV .= $separador."'".$_POST["G2570_C50132"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50133"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50133 = '".$_POST["G2570_C50133"]."'";
                $LsqlI .= $separador."G2570_C50133";
                $LsqlV .= $separador."'".$_POST["G2570_C50133"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50134"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50134 = '".$_POST["G2570_C50134"]."'";
                $LsqlI .= $separador."G2570_C50134";
                $LsqlV .= $separador."'".$_POST["G2570_C50134"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50135"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50135 = '".$_POST["G2570_C50135"]."'";
                $LsqlI .= $separador."G2570_C50135";
                $LsqlV .= $separador."'".$_POST["G2570_C50135"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50139"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50139 = '".$_POST["G2570_C50139"]."'";
                $LsqlI .= $separador."G2570_C50139";
                $LsqlV .= $separador."'".$_POST["G2570_C50139"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50140"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50140 = '".$_POST["G2570_C50140"]."'";
                $LsqlI .= $separador."G2570_C50140";
                $LsqlV .= $separador."'".$_POST["G2570_C50140"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50136"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50136 = '".$_POST["G2570_C50136"]."'";
                $LsqlI .= $separador."G2570_C50136";
                $LsqlV .= $separador."'".$_POST["G2570_C50136"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50137"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50137 = '".$_POST["G2570_C50137"]."'";
                $LsqlI .= $separador."G2570_C50137";
                $LsqlV .= $separador."'".$_POST["G2570_C50137"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50138"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50138 = '".$_POST["G2570_C50138"]."'";
                $LsqlI .= $separador."G2570_C50138";
                $LsqlV .= $separador."'".$_POST["G2570_C50138"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50122"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50122 = '".$_POST["G2570_C50122"]."'";
                $LsqlI .= $separador."G2570_C50122";
                $LsqlV .= $separador."'".$_POST["G2570_C50122"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50123"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50123 = '".$_POST["G2570_C50123"]."'";
                $LsqlI .= $separador."G2570_C50123";
                $LsqlV .= $separador."'".$_POST["G2570_C50123"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50124"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50124 = '".$_POST["G2570_C50124"]."'";
                $LsqlI .= $separador."G2570_C50124";
                $LsqlV .= $separador."'".$_POST["G2570_C50124"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50661"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50661 = '".$_POST["G2570_C50661"]."'";
                $LsqlI .= $separador."G2570_C50661";
                $LsqlV .= $separador."'".$_POST["G2570_C50661"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50662"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50662 = '".$_POST["G2570_C50662"]."'";
                $LsqlI .= $separador."G2570_C50662";
                $LsqlV .= $separador."'".$_POST["G2570_C50662"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50663"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50663 = '".$_POST["G2570_C50663"]."'";
                $LsqlI .= $separador."G2570_C50663";
                $LsqlV .= $separador."'".$_POST["G2570_C50663"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50664"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50664 = '".$_POST["G2570_C50664"]."'";
                $LsqlI .= $separador."G2570_C50664";
                $LsqlV .= $separador."'".$_POST["G2570_C50664"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50665"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50665 = '".$_POST["G2570_C50665"]."'";
                $LsqlI .= $separador."G2570_C50665";
                $LsqlV .= $separador."'".$_POST["G2570_C50665"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50666"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50666 = '".$_POST["G2570_C50666"]."'";
                $LsqlI .= $separador."G2570_C50666";
                $LsqlV .= $separador."'".$_POST["G2570_C50666"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50667"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50667 = '".$_POST["G2570_C50667"]."'";
                $LsqlI .= $separador."G2570_C50667";
                $LsqlV .= $separador."'".$_POST["G2570_C50667"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50668"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50668 = '".$_POST["G2570_C50668"]."'";
                $LsqlI .= $separador."G2570_C50668";
                $LsqlV .= $separador."'".$_POST["G2570_C50668"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50669"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50669 = '".$_POST["G2570_C50669"]."'";
                $LsqlI .= $separador."G2570_C50669";
                $LsqlV .= $separador."'".$_POST["G2570_C50669"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50670"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50670 = '".$_POST["G2570_C50670"]."'";
                $LsqlI .= $separador."G2570_C50670";
                $LsqlV .= $separador."'".$_POST["G2570_C50670"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50671"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50671 = '".$_POST["G2570_C50671"]."'";
                $LsqlI .= $separador."G2570_C50671";
                $LsqlV .= $separador."'".$_POST["G2570_C50671"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50672"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50672 = '".$_POST["G2570_C50672"]."'";
                $LsqlI .= $separador."G2570_C50672";
                $LsqlV .= $separador."'".$_POST["G2570_C50672"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50673"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50673 = '".$_POST["G2570_C50673"]."'";
                $LsqlI .= $separador."G2570_C50673";
                $LsqlV .= $separador."'".$_POST["G2570_C50673"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50674"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50674 = '".$_POST["G2570_C50674"]."'";
                $LsqlI .= $separador."G2570_C50674";
                $LsqlV .= $separador."'".$_POST["G2570_C50674"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2570_C50675"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_C50675 = '".$_POST["G2570_C50675"]."'";
                $LsqlI .= $separador."G2570_C50675";
                $LsqlV .= $separador."'".$_POST["G2570_C50675"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2570_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2570_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2570_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2570_Usuario , G2570_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2570_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2570 WHERE G2570_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2570 SET G2570_UltiGest__b =-14, G2570_GesMasImp_b =-14, G2570_TipoReintentoUG_b =0, G2570_TipoReintentoGMI_b =0, G2570_EstadoUG_b =-14, G2570_EstadoGMI_b =-14, G2570_CantidadIntentos =0, G2570_CantidadIntentosGMI_b =0 WHERE G2570_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
