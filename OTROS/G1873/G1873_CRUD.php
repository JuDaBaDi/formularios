<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1873_ConsInte__b, G1873_FechaInsercion , G1873_Usuario ,  G1873_CodigoMiembro  , G1873_PoblacionOrigen , G1873_EstadoDiligenciamiento ,  G1873_IdLlamada , G1873_C34421 as principal ,G1873_C34421,G1873_C34422,G1873_C34436,G1873_C34418,G1873_C34419,G1873_C34420,G1873_C34424,G1873_C34427,G1873_C34425,G1873_C34426,G1873_C34428,G1873_C34429,G1873_C34430,G1873_C34647,G1873_C34648,G1873_C34649,G1873_C34650,G1873_C34431,G1873_C34651,G1873_C34434,G1873_C34435,G1873_C34432,G1873_C34433,G1873_C34437,G1873_C34442,G1873_C34440,G1873_C34441,G1873_C34438,G1873_C34439,G1873_C34443,G1873_C34444,G1873_C34445,G1873_C34652,G1873_C34653,G1873_C34446,G1873_C34449,G1873_C34447,G1873_C34448,G1873_C34498,G1873_C34499,G1873_C34500,G1873_C34532,G1873_C34533,G1873_C34571,G1873_C34534,G1873_C34535,G1873_C34572 FROM '.$BaseDatos.'.G1873 WHERE G1873_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1873_C34421'] = $key->G1873_C34421;

                $datos[$i]['G1873_C34422'] = $key->G1873_C34422;

                $datos[$i]['G1873_C34436'] = $key->G1873_C34436;

                $datos[$i]['G1873_C34418'] = $key->G1873_C34418;

                $datos[$i]['G1873_C34419'] = $key->G1873_C34419;

                $datos[$i]['G1873_C34420'] = $key->G1873_C34420;

                $datos[$i]['G1873_C34424'] = $key->G1873_C34424;

                $datos[$i]['G1873_C34427'] = $key->G1873_C34427;

                $datos[$i]['G1873_C34425'] = $key->G1873_C34425;

                $datos[$i]['G1873_C34426'] = $key->G1873_C34426;

                $datos[$i]['G1873_C34428'] = $key->G1873_C34428;

                $datos[$i]['G1873_C34429'] = $key->G1873_C34429;

                $datos[$i]['G1873_C34430'] = $key->G1873_C34430;

                $datos[$i]['G1873_C34647'] = $key->G1873_C34647;

                $datos[$i]['G1873_C34648'] = $key->G1873_C34648;

                $datos[$i]['G1873_C34649'] = $key->G1873_C34649;

                $datos[$i]['G1873_C34650'] = $key->G1873_C34650;

                $datos[$i]['G1873_C34431'] = $key->G1873_C34431;

                $datos[$i]['G1873_C34651'] = $key->G1873_C34651;

                $datos[$i]['G1873_C34434'] = $key->G1873_C34434;

                $datos[$i]['G1873_C34435'] = $key->G1873_C34435;

                $datos[$i]['G1873_C34432'] = $key->G1873_C34432;

                $datos[$i]['G1873_C34433'] = $key->G1873_C34433;

                $datos[$i]['G1873_C34437'] = $key->G1873_C34437;

                $datos[$i]['G1873_C34442'] = $key->G1873_C34442;

                $datos[$i]['G1873_C34440'] = $key->G1873_C34440;

                $datos[$i]['G1873_C34441'] = $key->G1873_C34441;

                $datos[$i]['G1873_C34438'] = $key->G1873_C34438;

                $datos[$i]['G1873_C34439'] = $key->G1873_C34439;

                $datos[$i]['G1873_C34443'] = $key->G1873_C34443;

                $datos[$i]['G1873_C34444'] = $key->G1873_C34444;

                $datos[$i]['G1873_C34445'] = $key->G1873_C34445;

                $datos[$i]['G1873_C34652'] = $key->G1873_C34652;

                $datos[$i]['G1873_C34653'] = $key->G1873_C34653;

                $datos[$i]['G1873_C34446'] = $key->G1873_C34446;

                $datos[$i]['G1873_C34449'] = $key->G1873_C34449;

                $datos[$i]['G1873_C34447'] = $key->G1873_C34447;

                $datos[$i]['G1873_C34448'] = $key->G1873_C34448;

                $datos[$i]['G1873_C34498'] = explode(' ', $key->G1873_C34498)[0];
  
                $hora = '';
                if(!is_null($key->G1873_C34499)){
                    $hora = explode(' ', $key->G1873_C34499)[1];
                }

                $datos[$i]['G1873_C34499'] = $hora;

                $datos[$i]['G1873_C34500'] = $key->G1873_C34500;

                $datos[$i]['G1873_C34532'] = $key->G1873_C34532;

                $datos[$i]['G1873_C34533'] = $key->G1873_C34533;

                $datos[$i]['G1873_C34571'] = $key->G1873_C34571;

                $datos[$i]['G1873_C34534'] = $key->G1873_C34534;

                $datos[$i]['G1873_C34535'] = $key->G1873_C34535;

                $datos[$i]['G1873_C34572'] = $key->G1873_C34572;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1873";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1873_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1873_ConsInte__b as id,  G1873_C34421 as camp1 , b.LISOPC_Nombre____b as camp2 
                     FROM ".$BaseDatos.".G1873  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1873_C34422 WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1873_ConsInte__b as id,  G1873_C34421 as camp1 , b.LISOPC_Nombre____b as camp2  
                    FROM ".$BaseDatos.".G1873  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1873_C34422 JOIN ".$BaseDatos.".G1873_M".$_POST['muestra']." ON G1873_ConsInte__b = G1873_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1873_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1873_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1873_C34421 LIKE '%".$B."%' OR G1873_C34422 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1873_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1873");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1873_ConsInte__b, G1873_FechaInsercion , G1873_Usuario ,  G1873_CodigoMiembro  , G1873_PoblacionOrigen , G1873_EstadoDiligenciamiento ,  G1873_IdLlamada , G1873_C34421 as principal ,G1873_C34421, a.LISOPC_Nombre____b as G1873_C34422,G1873_C34436,G1873_C34418,G1873_C34419, b.LISOPC_Nombre____b as G1873_C34420,G1873_C34424,G1873_C34427,G1873_C34425,G1873_C34426,G1873_C34428,G1873_C34429,G1873_C34430,G1873_C34647,G1873_C34648,G1873_C34649,G1873_C34650, c.LISOPC_Nombre____b as G1873_C34431,G1873_C34651,G1873_C34434,G1873_C34435,G1873_C34432,G1873_C34433,G1873_C34437,G1873_C34442,G1873_C34440,G1873_C34441,G1873_C34438,G1873_C34439,G1873_C34443,G1873_C34444,G1873_C34445, d.LISOPC_Nombre____b as G1873_C34652, e.LISOPC_Nombre____b as G1873_C34653,G1873_C34446,G1873_C34449,G1873_C34447,G1873_C34448,G1873_C34498,G1873_C34499, f.LISOPC_Nombre____b as G1873_C34500, g.LISOPC_Nombre____b as G1873_C34532, h.LISOPC_Nombre____b as G1873_C34533,G1873_C34571, i.LISOPC_Nombre____b as G1873_C34534, j.LISOPC_Nombre____b as G1873_C34535,G1873_C34572 FROM '.$BaseDatos.'.G1873 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1873_C34422 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1873_C34420 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1873_C34431 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1873_C34652 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1873_C34653 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1873_C34500 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G1873_C34532 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G1873_C34533 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as i ON i.LISOPC_ConsInte__b =  G1873_C34534 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as j ON j.LISOPC_ConsInte__b =  G1873_C34535';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1873_C34499)){
                    $hora_a = explode(' ', $fila->G1873_C34499)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1873_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1873_ConsInte__b , ($fila->G1873_C34421) , ($fila->G1873_C34422) , ($fila->G1873_C34436) , ($fila->G1873_C34418) , ($fila->G1873_C34419) , ($fila->G1873_C34420) , ($fila->G1873_C34424) , ($fila->G1873_C34427) , ($fila->G1873_C34425) , ($fila->G1873_C34426) , ($fila->G1873_C34428) , ($fila->G1873_C34429) , ($fila->G1873_C34430) , ($fila->G1873_C34647) , ($fila->G1873_C34648) , ($fila->G1873_C34649) , ($fila->G1873_C34650) , ($fila->G1873_C34431) , ($fila->G1873_C34651) , ($fila->G1873_C34434) , ($fila->G1873_C34435) , ($fila->G1873_C34432) , ($fila->G1873_C34433) , ($fila->G1873_C34437) , ($fila->G1873_C34442) , ($fila->G1873_C34440) , ($fila->G1873_C34441) , ($fila->G1873_C34438) , ($fila->G1873_C34439) , ($fila->G1873_C34443) , ($fila->G1873_C34444) , ($fila->G1873_C34445) , ($fila->G1873_C34652) , ($fila->G1873_C34653) , ($fila->G1873_C34446) , ($fila->G1873_C34449) , ($fila->G1873_C34447) , ($fila->G1873_C34448) , explode(' ', $fila->G1873_C34498)[0] , $hora_a , ($fila->G1873_C34500) , ($fila->G1873_C34532) , ($fila->G1873_C34533) , ($fila->G1873_C34571) , ($fila->G1873_C34534) , ($fila->G1873_C34535) , ($fila->G1873_C34572) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1873 WHERE G1873_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1873";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1873_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1873_ConsInte__b as id,  G1873_C34421 as camp1 , b.LISOPC_Nombre____b as camp2  FROM '.$BaseDatos.'.G1873 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b = G1873_C34422 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1873_ConsInte__b as id,  G1873_C34421 as camp1 , b.LISOPC_Nombre____b as camp2  
                    FROM ".$BaseDatos.".G1873  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1873_C34422 JOIN ".$BaseDatos.".G1873_M".$_POST['muestra']." ON G1873_ConsInte__b = G1873_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1873_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1873_C34421 LIKE "%'.$B.'%" OR G1873_C34422 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1873_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1873 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1873(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1873_C34421"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34421 = '".$_POST["G1873_C34421"]."'";
                $LsqlI .= $separador."G1873_C34421";
                $LsqlV .= $separador."'".$_POST["G1873_C34421"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34422"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34422 = '".$_POST["G1873_C34422"]."'";
                $LsqlI .= $separador."G1873_C34422";
                $LsqlV .= $separador."'".$_POST["G1873_C34422"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34436"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34436 = '".$_POST["G1873_C34436"]."'";
                $LsqlI .= $separador."G1873_C34436";
                $LsqlV .= $separador."'".$_POST["G1873_C34436"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34418"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34418 = '".$_POST["G1873_C34418"]."'";
                $LsqlI .= $separador."G1873_C34418";
                $LsqlV .= $separador."'".$_POST["G1873_C34418"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34419"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34419 = '".$_POST["G1873_C34419"]."'";
                $LsqlI .= $separador."G1873_C34419";
                $LsqlV .= $separador."'".$_POST["G1873_C34419"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34420"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34420 = '".$_POST["G1873_C34420"]."'";
                $LsqlI .= $separador."G1873_C34420";
                $LsqlV .= $separador."'".$_POST["G1873_C34420"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34424"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34424 = '".$_POST["G1873_C34424"]."'";
                $LsqlI .= $separador."G1873_C34424";
                $LsqlV .= $separador."'".$_POST["G1873_C34424"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34427"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34427 = '".$_POST["G1873_C34427"]."'";
                $LsqlI .= $separador."G1873_C34427";
                $LsqlV .= $separador."'".$_POST["G1873_C34427"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34425"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34425 = '".$_POST["G1873_C34425"]."'";
                $LsqlI .= $separador."G1873_C34425";
                $LsqlV .= $separador."'".$_POST["G1873_C34425"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34426"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34426 = '".$_POST["G1873_C34426"]."'";
                $LsqlI .= $separador."G1873_C34426";
                $LsqlV .= $separador."'".$_POST["G1873_C34426"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34428"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34428 = '".$_POST["G1873_C34428"]."'";
                $LsqlI .= $separador."G1873_C34428";
                $LsqlV .= $separador."'".$_POST["G1873_C34428"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34429"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34429 = '".$_POST["G1873_C34429"]."'";
                $LsqlI .= $separador."G1873_C34429";
                $LsqlV .= $separador."'".$_POST["G1873_C34429"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34430"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34430 = '".$_POST["G1873_C34430"]."'";
                $LsqlI .= $separador."G1873_C34430";
                $LsqlV .= $separador."'".$_POST["G1873_C34430"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34647"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34647 = '".$_POST["G1873_C34647"]."'";
                $LsqlI .= $separador."G1873_C34647";
                $LsqlV .= $separador."'".$_POST["G1873_C34647"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34648"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34648 = '".$_POST["G1873_C34648"]."'";
                $LsqlI .= $separador."G1873_C34648";
                $LsqlV .= $separador."'".$_POST["G1873_C34648"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34649"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34649 = '".$_POST["G1873_C34649"]."'";
                $LsqlI .= $separador."G1873_C34649";
                $LsqlV .= $separador."'".$_POST["G1873_C34649"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34650"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34650 = '".$_POST["G1873_C34650"]."'";
                $LsqlI .= $separador."G1873_C34650";
                $LsqlV .= $separador."'".$_POST["G1873_C34650"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34431"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34431 = '".$_POST["G1873_C34431"]."'";
                $LsqlI .= $separador."G1873_C34431";
                $LsqlV .= $separador."'".$_POST["G1873_C34431"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34651"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34651 = '".$_POST["G1873_C34651"]."'";
                $LsqlI .= $separador."G1873_C34651";
                $LsqlV .= $separador."'".$_POST["G1873_C34651"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34434"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34434 = '".$_POST["G1873_C34434"]."'";
                $LsqlI .= $separador."G1873_C34434";
                $LsqlV .= $separador."'".$_POST["G1873_C34434"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34435"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34435 = '".$_POST["G1873_C34435"]."'";
                $LsqlI .= $separador."G1873_C34435";
                $LsqlV .= $separador."'".$_POST["G1873_C34435"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34432"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34432 = '".$_POST["G1873_C34432"]."'";
                $LsqlI .= $separador."G1873_C34432";
                $LsqlV .= $separador."'".$_POST["G1873_C34432"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34433"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34433 = '".$_POST["G1873_C34433"]."'";
                $LsqlI .= $separador."G1873_C34433";
                $LsqlV .= $separador."'".$_POST["G1873_C34433"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34437"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34437 = '".$_POST["G1873_C34437"]."'";
                $LsqlI .= $separador."G1873_C34437";
                $LsqlV .= $separador."'".$_POST["G1873_C34437"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34442"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34442 = '".$_POST["G1873_C34442"]."'";
                $LsqlI .= $separador."G1873_C34442";
                $LsqlV .= $separador."'".$_POST["G1873_C34442"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34440"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34440 = '".$_POST["G1873_C34440"]."'";
                $LsqlI .= $separador."G1873_C34440";
                $LsqlV .= $separador."'".$_POST["G1873_C34440"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34441"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34441 = '".$_POST["G1873_C34441"]."'";
                $LsqlI .= $separador."G1873_C34441";
                $LsqlV .= $separador."'".$_POST["G1873_C34441"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34438"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34438 = '".$_POST["G1873_C34438"]."'";
                $LsqlI .= $separador."G1873_C34438";
                $LsqlV .= $separador."'".$_POST["G1873_C34438"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34439"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34439 = '".$_POST["G1873_C34439"]."'";
                $LsqlI .= $separador."G1873_C34439";
                $LsqlV .= $separador."'".$_POST["G1873_C34439"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34443"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34443 = '".$_POST["G1873_C34443"]."'";
                $LsqlI .= $separador."G1873_C34443";
                $LsqlV .= $separador."'".$_POST["G1873_C34443"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34444"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34444 = '".$_POST["G1873_C34444"]."'";
                $LsqlI .= $separador."G1873_C34444";
                $LsqlV .= $separador."'".$_POST["G1873_C34444"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34445"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34445 = '".$_POST["G1873_C34445"]."'";
                $LsqlI .= $separador."G1873_C34445";
                $LsqlV .= $separador."'".$_POST["G1873_C34445"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34652"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34652 = '".$_POST["G1873_C34652"]."'";
                $LsqlI .= $separador."G1873_C34652";
                $LsqlV .= $separador."'".$_POST["G1873_C34652"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34653"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34653 = '".$_POST["G1873_C34653"]."'";
                $LsqlI .= $separador."G1873_C34653";
                $LsqlV .= $separador."'".$_POST["G1873_C34653"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34446"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34446 = '".$_POST["G1873_C34446"]."'";
                $LsqlI .= $separador."G1873_C34446";
                $LsqlV .= $separador."'".$_POST["G1873_C34446"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34449"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34449 = '".$_POST["G1873_C34449"]."'";
                $LsqlI .= $separador."G1873_C34449";
                $LsqlV .= $separador."'".$_POST["G1873_C34449"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34447"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34447 = '".$_POST["G1873_C34447"]."'";
                $LsqlI .= $separador."G1873_C34447";
                $LsqlV .= $separador."'".$_POST["G1873_C34447"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34448"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34448 = '".$_POST["G1873_C34448"]."'";
                $LsqlI .= $separador."G1873_C34448";
                $LsqlV .= $separador."'".$_POST["G1873_C34448"]."'";
                $validar = 1;
            }
             
 
            $G1873_C34498 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1873_C34498"])){    
                if($_POST["G1873_C34498"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1873_C34498"]);
                    if(count($tieneHora) > 1){
                        $G1873_C34498 = "'".$_POST["G1873_C34498"]."'";
                    }else{
                        $G1873_C34498 = "'".str_replace(' ', '',$_POST["G1873_C34498"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1873_C34498 = ".$G1873_C34498;
                    $LsqlI .= $separador." G1873_C34498";
                    $LsqlV .= $separador.$G1873_C34498;
                    $validar = 1;
                }
            }
  
            $G1873_C34499 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1873_C34499"])){   
                if($_POST["G1873_C34499"] != '' && $_POST["G1873_C34499"] != 'undefined' && $_POST["G1873_C34499"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1873_C34499 = "'".$fecha." ".str_replace(' ', '',$_POST["G1873_C34499"])."'";
                    $LsqlU .= $separador." G1873_C34499 = ".$G1873_C34499."";
                    $LsqlI .= $separador." G1873_C34499";
                    $LsqlV .= $separador.$G1873_C34499;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1873_C34500"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34500 = '".$_POST["G1873_C34500"]."'";
                $LsqlI .= $separador."G1873_C34500";
                $LsqlV .= $separador."'".$_POST["G1873_C34500"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34532"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34532 = '".$_POST["G1873_C34532"]."'";
                $LsqlI .= $separador."G1873_C34532";
                $LsqlV .= $separador."'".$_POST["G1873_C34532"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34533"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34533 = '".$_POST["G1873_C34533"]."'";
                $LsqlI .= $separador."G1873_C34533";
                $LsqlV .= $separador."'".$_POST["G1873_C34533"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34571"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34571 = '".$_POST["G1873_C34571"]."'";
                $LsqlI .= $separador."G1873_C34571";
                $LsqlV .= $separador."'".$_POST["G1873_C34571"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34534"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34534 = '".$_POST["G1873_C34534"]."'";
                $LsqlI .= $separador."G1873_C34534";
                $LsqlV .= $separador."'".$_POST["G1873_C34534"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34535"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34535 = '".$_POST["G1873_C34535"]."'";
                $LsqlI .= $separador."G1873_C34535";
                $LsqlV .= $separador."'".$_POST["G1873_C34535"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1873_C34572"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_C34572 = '".$_POST["G1873_C34572"]."'";
                $LsqlI .= $separador."G1873_C34572";
                $LsqlV .= $separador."'".$_POST["G1873_C34572"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1873_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1873_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1873_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1873_Usuario , G1873_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1873_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1873 WHERE G1873_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G1873 SET G1873_UltiGest__b =-14, G1873_GesMasImp_b =-14, G1873_TipoReintentoUG_b =0, G1873_TipoReintentoGMI_b =0, G1873_EstadoUG_b =-14, G1873_EstadoGMI_b =-14, G1873_CantidadIntentos =0, G1873_CantidadIntentosGMI_b =0 WHERE G1873_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
