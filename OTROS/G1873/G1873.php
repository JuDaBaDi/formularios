
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1873/G1873_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1873_ConsInte__b as id, G1873_C34421 as camp1 , b.LISOPC_Nombre____b as camp2 FROM ".$BaseDatos.".G1873  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1873_C34422 WHERE G1873_Usuario = ".$idUsuario." ORDER BY G1873_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1873_ConsInte__b as id, G1873_C34421 as camp1 , b.LISOPC_Nombre____b as camp2 FROM ".$BaseDatos.".G1873  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1873_C34422 ORDER BY G1873_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G1873_ConsInte__b as id, G1873_C34421 as camp1 , b.LISOPC_Nombre____b as camp2 FROM ".$BaseDatos.".G1873 LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1873_C34422 JOIN ".$BaseDatos.".G1873_M".$resultEstpas->muestr." ON G1873_ConsInte__b = G1873_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G1873_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G1873_ConsInte__b as id, G1873_C34421 as camp1 , b.LISOPC_Nombre____b as camp2 FROM ".$BaseDatos.".G1873 LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1873_C34422 JOIN ".$BaseDatos.".G1873_M".$resultEstpas->muestr." ON G1873_ConsInte__b = G1873_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G1873_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G1873_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G1873_ConsInte__b as id, G1873_C34421 as camp1 , b.LISOPC_Nombre____b as camp2 FROM ".$BaseDatos.".G1873  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b = G1873_C34422 ORDER BY G1873_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  class="panel box box-primary" id="4881" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4881c">
                DATOS BASICOS
            </a>
        </h4>
        
    </div>
    <div id="s_4881c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34421" id="LblG1873_C34421">Nombres y Apellidos</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34421" value="<?php if (isset($_GET['G1873_C34421'])) {
                            echo $_GET['G1873_C34421'];
                        } ?>"  name="G1873_C34421"  placeholder="Nombres y Apellidos">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1873_C34422" id="LblG1873_C34422">¿Qué Persona se Está Comunicando?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1873_C34422" id="G1873_C34422">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1924 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-4 col-xs-4">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34436" id="LblG1873_C34436">Número de contacto</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34436" value="<?php if (isset($_GET['G1873_C34436'])) {
                            echo $_GET['G1873_C34436'];
                        } ?>"  name="G1873_C34436"  placeholder="Número de contacto">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="4882" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34418" id="LblG1873_C34418">ORIGEN_DY_WF</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34418" value="<?php if (isset($_GET['G1873_C34418'])) {
                            echo $_GET['G1873_C34418'];
                        } ?>" readonly name="G1873_C34418"  placeholder="ORIGEN_DY_WF">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34419" id="LblG1873_C34419">OPTIN_DY_WF</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34419" value="<?php if (isset($_GET['G1873_C34419'])) {
                            echo $_GET['G1873_C34419'];
                        } ?>" readonly name="G1873_C34419"  placeholder="OPTIN_DY_WF">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1873_C34420" id="LblG1873_C34420">ESTADO_DY</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1873_C34420" id="G1873_C34420">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1923 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="4883" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4883c">
                VISTA PARA LLAMADAS DE FAMILIAR
            </a>
        </h4>
        
    </div>
    <div id="s_4883c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34424" id="LblG1873_C34424">Número de Identificación de familiar</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34424" value="<?php if (isset($_GET['G1873_C34424'])) {
                            echo $_GET['G1873_C34424'];
                        } ?>"  name="G1873_C34424"  placeholder="Número de Identificación de familiar">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34427" id="LblG1873_C34427">¿En qué grado se encuentra el estudiante?</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34427" value="<?php if (isset($_GET['G1873_C34427'])) {
                            echo $_GET['G1873_C34427'];
                        } ?>"  name="G1873_C34427"  placeholder="¿En qué grado se encuentra el estudiante?">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34425" id="LblG1873_C34425">Nombres y Apellidos del Estudiante que Representa</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34425" value="<?php if (isset($_GET['G1873_C34425'])) {
                            echo $_GET['G1873_C34425'];
                        } ?>"  name="G1873_C34425"  placeholder="Nombres y Apellidos del Estudiante que Representa">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34426" id="LblG1873_C34426">¿En qué colegio se encuentra el estudiante?</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34426" value="<?php if (isset($_GET['G1873_C34426'])) {
                            echo $_GET['G1873_C34426'];
                        } ?>"  name="G1873_C34426"  placeholder="¿En qué colegio se encuentra el estudiante?">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34428" id="LblG1873_C34428">Número Celular de familiar</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34428" value="<?php if (isset($_GET['G1873_C34428'])) {
                            echo $_GET['G1873_C34428'];
                        } ?>"  name="G1873_C34428"  placeholder="Número Celular de familiar">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34429" id="LblG1873_C34429">Teléfono Adicional de familiar</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34429" value="<?php if (isset($_GET['G1873_C34429'])) {
                            echo $_GET['G1873_C34429'];
                        } ?>"  name="G1873_C34429"  placeholder="Teléfono Adicional de familiar">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34430" id="LblG1873_C34430">Correo Electrónico de familiar</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34430" value="<?php if (isset($_GET['G1873_C34430'])) {
                            echo $_GET['G1873_C34430'];
                        } ?>"  name="G1873_C34430"  placeholder="Correo Electrónico de familiar">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34647" id="LblG1873_C34647">Edad del estudiante</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34647" value="<?php if (isset($_GET['G1873_C34647'])) {
                            echo $_GET['G1873_C34647'];
                        } ?>"  name="G1873_C34647"  placeholder="Edad del estudiante">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34648" id="LblG1873_C34648">Ciudad</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34648" value="<?php if (isset($_GET['G1873_C34648'])) {
                            echo $_GET['G1873_C34648'];
                        } ?>"  name="G1873_C34648"  placeholder="Ciudad">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34649" id="LblG1873_C34649">Localidad</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34649" value="<?php if (isset($_GET['G1873_C34649'])) {
                            echo $_GET['G1873_C34649'];
                        } ?>"  name="G1873_C34649"  placeholder="Localidad">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34650" id="LblG1873_C34650">Barrio</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34650" value="<?php if (isset($_GET['G1873_C34650'])) {
                            echo $_GET['G1873_C34650'];
                        } ?>"  name="G1873_C34650"  placeholder="Barrio">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="4884" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4884c">
                VISTA PARA LLAMADAS DE ESTUDIANTE
            </a>
        </h4>
        
    </div>
    <div id="s_4884c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1873_C34431" id="LblG1873_C34431">Tiene autorización o está acompañado de un adulto</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1873_C34431" id="G1873_C34431">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1925 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34651" id="LblG1873_C34651">Edad</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34651" value="<?php if (isset($_GET['G1873_C34651'])) {
                            echo $_GET['G1873_C34651'];
                        } ?>"  name="G1873_C34651"  placeholder="Edad">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34434" id="LblG1873_C34434">Colegio Donde Estudia</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34434" value="<?php if (isset($_GET['G1873_C34434'])) {
                            echo $_GET['G1873_C34434'];
                        } ?>"  name="G1873_C34434"  placeholder="Colegio Donde Estudia">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34435" id="LblG1873_C34435">Grado Que Está Cursando</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34435" value="<?php if (isset($_GET['G1873_C34435'])) {
                            echo $_GET['G1873_C34435'];
                        } ?>"  name="G1873_C34435"  placeholder="Grado Que Está Cursando">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34432" id="LblG1873_C34432">Ciudad del Estudiante</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34432" value="<?php if (isset($_GET['G1873_C34432'])) {
                            echo $_GET['G1873_C34432'];
                        } ?>"  name="G1873_C34432"  placeholder="Ciudad del Estudiante">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34433" id="LblG1873_C34433">Barriodel Estudiante</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34433" value="<?php if (isset($_GET['G1873_C34433'])) {
                            echo $_GET['G1873_C34433'];
                        } ?>"  name="G1873_C34433"  placeholder="Barriodel Estudiante">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="4885" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4885c">
                VISTA PARA LLAMADAS DE DOCENTE, RECTOR, COORDINADOR, ORIENTADOR
            </a>
        </h4>
        
    </div>
    <div id="s_4885c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34437" id="LblG1873_C34437">Número de Identificación.</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34437" value="<?php if (isset($_GET['G1873_C34437'])) {
                            echo $_GET['G1873_C34437'];
                        } ?>"  name="G1873_C34437"  placeholder="Número de Identificación.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34442" id="LblG1873_C34442">Barrio.</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34442" value="<?php if (isset($_GET['G1873_C34442'])) {
                            echo $_GET['G1873_C34442'];
                        } ?>"  name="G1873_C34442"  placeholder="Barrio.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34440" id="LblG1873_C34440">Colegio donde trabaja.</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34440" value="<?php if (isset($_GET['G1873_C34440'])) {
                            echo $_GET['G1873_C34440'];
                        } ?>"  name="G1873_C34440"  placeholder="Colegio donde trabaja.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34441" id="LblG1873_C34441">Ciudad.</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34441" value="<?php if (isset($_GET['G1873_C34441'])) {
                            echo $_GET['G1873_C34441'];
                        } ?>"  name="G1873_C34441"  placeholder="Ciudad.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34438" id="LblG1873_C34438">Nivel que está enseñando este año.</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34438" value="<?php if (isset($_GET['G1873_C34438'])) {
                            echo $_GET['G1873_C34438'];
                        } ?>"  name="G1873_C34438"  placeholder="Nivel que está enseñando este año.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34439" id="LblG1873_C34439">Área que está enseñando este año.</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34439" value="<?php if (isset($_GET['G1873_C34439'])) {
                            echo $_GET['G1873_C34439'];
                        } ?>"  name="G1873_C34439"  placeholder="Área que está enseñando este año.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34443" id="LblG1873_C34443">Número Celular.</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34443" value="<?php if (isset($_GET['G1873_C34443'])) {
                            echo $_GET['G1873_C34443'];
                        } ?>"  name="G1873_C34443"  placeholder="Número Celular.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34444" id="LblG1873_C34444">Teléfono Adicional.</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34444" value="<?php if (isset($_GET['G1873_C34444'])) {
                            echo $_GET['G1873_C34444'];
                        } ?>"  name="G1873_C34444"  placeholder="Teléfono Adicional.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34445" id="LblG1873_C34445">Correo Electrónico.</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34445" value="<?php if (isset($_GET['G1873_C34445'])) {
                            echo $_GET['G1873_C34445'];
                        } ?>"  name="G1873_C34445"  placeholder="Correo Electrónico.">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1873_C34652" id="LblG1873_C34652">¿Es Docente de un Establecimiento Educativo Oficial?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1873_C34652" id="G1873_C34652">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1925 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1873_C34653" id="LblG1873_C34653">¿Tiene nombramiento en propiedad o provisional?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1873_C34653" id="G1873_C34653">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1925 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="4886" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4886c">
                VISTA PARA LLAMADAS DE OTROS
            </a>
        </h4>
        
    </div>
    <div id="s_4886c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34446" id="LblG1873_C34446">Número de Identificación de Otro</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34446" value="<?php if (isset($_GET['G1873_C34446'])) {
                            echo $_GET['G1873_C34446'];
                        } ?>"  name="G1873_C34446"  placeholder="Número de Identificación de Otro">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34449" id="LblG1873_C34449">Correo Electrónico de Otro</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34449" value="<?php if (isset($_GET['G1873_C34449'])) {
                            echo $_GET['G1873_C34449'];
                        } ?>"  name="G1873_C34449"  placeholder="Correo Electrónico de Otro">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34447" id="LblG1873_C34447">Número Celular de Otro</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34447" value="<?php if (isset($_GET['G1873_C34447'])) {
                            echo $_GET['G1873_C34447'];
                        } ?>"  name="G1873_C34447"  placeholder="Número Celular de Otro">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1873_C34448" id="LblG1873_C34448">Teléfono Adicional de Otro</label>
                        <input type="text" class="form-control input-sm" id="G1873_C34448" value="<?php if (isset($_GET['G1873_C34448'])) {
                            echo $_GET['G1873_C34448'];
                        } ?>"  name="G1873_C34448"  placeholder="Teléfono Adicional de Otro">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="4898" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4898c">
                AGENDAMIENTO DE CITA
            </a>
        </h4>
        
    </div>
    <div id="s_4898c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO FECHA -->
                    <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                    <div class="form-group">
                        <label for="G1873_C34498" id="LblG1873_C34498">FECHA DE CITA</label>
                        <input type="text" class="form-control input-sm Fecha" value="<?php if (isset($_GET['G1873_C34498'])) {
                            echo $_GET['G1873_C34498'];
                        } ?>"  name="G1873_C34498" id="G1873_C34498" placeholder="YYYY-MM-DD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIMEPICKER -->
                    <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                    <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label for="G1873_C34499" id="LblG1873_C34499">HORA DE CITA</label>
                            <div class="input-group">
                                <input type="text" class="form-control input-sm Hora"  name="G1873_C34499" id="G1873_C34499" placeholder="HH:MM:SS" >
                                <div class="input-group-addon" id="TMP_G1873_C34499">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                    </div>
                    <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1873_C34500" id="LblG1873_C34500">CONSULTOR</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1873_C34500" id="G1873_C34500">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1927 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="4908" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4908c">
                TIPIFICACION AGENTE
            </a>
        </h4>
        
    </div>
    <div id="s_4908c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1873_C34532" id="LblG1873_C34532">TIPIFICACION AGENTE </label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1873_C34532" id="G1873_C34532">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1929 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1873_C34533" id="LblG1873_C34533">SUBTIPIFICACION AGENTE</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1873_C34533" id="G1873_C34533">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1930 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G1873_C34571" id="LblG1873_C34571">OBSERVACIONES AGENTE</label>
                        <textarea class="form-control input-sm" name="G1873_C34571" id="G1873_C34571"  value="<?php if (isset($_GET['G1873_C34571'])) {
                            echo $_GET['G1873_C34571'];
                        } ?>" placeholder="OBSERVACIONES AGENTE"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="4909" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4909c">
                TIPIFICACION BACKOFFICE
            </a>
        </h4>
        
    </div>
    <div id="s_4909c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1873_C34534" id="LblG1873_C34534">TIPIFICACION BACKOFFICE</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1873_C34534" id="G1873_C34534">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1933 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1873_C34535" id="LblG1873_C34535">SUBTIPIFICACION BACKOFFICE</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1873_C34535" id="G1873_C34535">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1934 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G1873_C34572" id="LblG1873_C34572">OBSERVACIONES BACKOFFICE</label>
                        <textarea class="form-control input-sm" name="G1873_C34572" id="G1873_C34572"  value="<?php if (isset($_GET['G1873_C34572'])) {
                            echo $_GET['G1873_C34572'];
                        } ?>" placeholder="OBSERVACIONES BACKOFFICE"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G1873/G1873_eventos.js"></script> 
<script type="text/javascript">
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la sección de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G1873_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G1873_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G1873_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G1873_C34421").val(item.G1873_C34421); 
                $("#G1873_C34422").val(item.G1873_C34422).trigger("change");  
                $("#G1873_C34436").val(item.G1873_C34436); 
                $("#G1873_C34418").val(item.G1873_C34418); 
                $("#G1873_C34419").val(item.G1873_C34419); 
                $("#G1873_C34420").val(item.G1873_C34420).trigger("change");  
                $("#G1873_C34424").val(item.G1873_C34424); 
                $("#G1873_C34427").val(item.G1873_C34427); 
                $("#G1873_C34425").val(item.G1873_C34425); 
                $("#G1873_C34426").val(item.G1873_C34426); 
                $("#G1873_C34428").val(item.G1873_C34428); 
                $("#G1873_C34429").val(item.G1873_C34429); 
                $("#G1873_C34430").val(item.G1873_C34430); 
                $("#G1873_C34647").val(item.G1873_C34647); 
                $("#G1873_C34648").val(item.G1873_C34648); 
                $("#G1873_C34649").val(item.G1873_C34649); 
                $("#G1873_C34650").val(item.G1873_C34650); 
                $("#G1873_C34431").val(item.G1873_C34431).trigger("change");  
                $("#G1873_C34651").val(item.G1873_C34651); 
                $("#G1873_C34434").val(item.G1873_C34434); 
                $("#G1873_C34435").val(item.G1873_C34435); 
                $("#G1873_C34432").val(item.G1873_C34432); 
                $("#G1873_C34433").val(item.G1873_C34433); 
                $("#G1873_C34437").val(item.G1873_C34437); 
                $("#G1873_C34442").val(item.G1873_C34442); 
                $("#G1873_C34440").val(item.G1873_C34440); 
                $("#G1873_C34441").val(item.G1873_C34441); 
                $("#G1873_C34438").val(item.G1873_C34438); 
                $("#G1873_C34439").val(item.G1873_C34439); 
                $("#G1873_C34443").val(item.G1873_C34443); 
                $("#G1873_C34444").val(item.G1873_C34444); 
                $("#G1873_C34445").val(item.G1873_C34445); 
                $("#G1873_C34652").val(item.G1873_C34652).trigger("change");  
                $("#G1873_C34653").val(item.G1873_C34653).trigger("change");  
                $("#G1873_C34446").val(item.G1873_C34446); 
                $("#G1873_C34449").val(item.G1873_C34449); 
                $("#G1873_C34447").val(item.G1873_C34447); 
                $("#G1873_C34448").val(item.G1873_C34448); 
                $("#G1873_C34498").val(item.G1873_C34498); 
                $("#G1873_C34499").val(item.G1873_C34499); 
                $("#G1873_C34500").val(item.G1873_C34500).trigger("change");  
                $("#G1873_C34532").val(item.G1873_C34532).trigger("change");  
                $("#G1873_C34533").attr("opt",item.G1873_C34533);  
                $("#G1873_C34571").val(item.G1873_C34571); 
                $("#G1873_C34534").val(item.G1873_C34534).trigger("change");  
                $("#G1873_C34535").attr("opt",item.G1873_C34535);  
                $("#G1873_C34572").val(item.G1873_C34572);
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G1873_C34422").select2();

    $("#G1873_C34420").select2();

    $("#G1873_C34431").select2();

    $("#G1873_C34652").select2();

    $("#G1873_C34653").select2();

    $("#G1873_C34500").select2();

    $("#G1873_C34532").select2();

    $("#G1873_C34533").select2();

    $("#G1873_C34534").select2();

    $("#G1873_C34535").select2();
        //datepickers
        

        $("#G1873_C34498").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'HORA DE CITA', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1873_C34499").wickedpicker(options);

        //Validaciones numeros Enteros
        


        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ¿Qué Persona se Está Comunicando? 

    $("#G1873_C34422").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ESTADO_DY 

    $("#G1873_C34420").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Tiene autorización o está acompañado de un adulto 

    $("#G1873_C34431").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Es Docente de un Establecimiento Educativo Oficial? 

    $("#G1873_C34652").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Tiene nombramiento en propiedad o provisional? 

    $("#G1873_C34653").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para CONSULTOR 

    $("#G1873_C34500").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPIFICACION AGENTE  

    $("#G1873_C34532").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '1930' , idPadre : $(this).val() },
            success : function(data){
                var optG1873_C34533 = $("#G1873_C34533").attr("opt");
                $("#G1873_C34533").html(data);
                if (optG1873_C34533 != null) {
                    $("#G1873_C34533").val(optG1873_C34533).trigger("change");
                }
            }
        });
        
    });

    //function para SUBTIPIFICACION AGENTE 

    $("#G1873_C34533").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para TIPIFICACION BACKOFFICE 

    $("#G1873_C34534").change(function(){  
        //Esto es la parte de las listas dependientes
        

        $.ajax({
            url    : '<?php echo $url_crud; ?>',
            type   : 'post',
            data   : { getListaHija : true , opcionID : '1934' , idPadre : $(this).val() },
            success : function(data){
                var optG1873_C34535 = $("#G1873_C34535").attr("opt");
                $("#G1873_C34535").html(data);
                if (optG1873_C34535 != null) {
                    $("#G1873_C34535").val(optG1873_C34535).trigger("change");
                }
            }
        });
        
    });

    //function para SUBTIPIFICACION BACKOFFICE 

    $("#G1873_C34535").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificación!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G1873_C34421").val(item.G1873_C34421);
 
                    $("#G1873_C34422").val(item.G1873_C34422).trigger("change"); 
 
                                                $("#G1873_C34436").val(item.G1873_C34436);
 
                                                $("#G1873_C34418").val(item.G1873_C34418);
 
                                                $("#G1873_C34419").val(item.G1873_C34419);
 
                    $("#G1873_C34420").val(item.G1873_C34420).trigger("change"); 
 
                                                $("#G1873_C34424").val(item.G1873_C34424);
 
                                                $("#G1873_C34427").val(item.G1873_C34427);
 
                                                $("#G1873_C34425").val(item.G1873_C34425);
 
                                                $("#G1873_C34426").val(item.G1873_C34426);
 
                                                $("#G1873_C34428").val(item.G1873_C34428);
 
                                                $("#G1873_C34429").val(item.G1873_C34429);
 
                                                $("#G1873_C34430").val(item.G1873_C34430);
 
                                                $("#G1873_C34647").val(item.G1873_C34647);
 
                                                $("#G1873_C34648").val(item.G1873_C34648);
 
                                                $("#G1873_C34649").val(item.G1873_C34649);
 
                                                $("#G1873_C34650").val(item.G1873_C34650);
 
                    $("#G1873_C34431").val(item.G1873_C34431).trigger("change"); 
 
                                                $("#G1873_C34651").val(item.G1873_C34651);
 
                                                $("#G1873_C34434").val(item.G1873_C34434);
 
                                                $("#G1873_C34435").val(item.G1873_C34435);
 
                                                $("#G1873_C34432").val(item.G1873_C34432);
 
                                                $("#G1873_C34433").val(item.G1873_C34433);
 
                                                $("#G1873_C34437").val(item.G1873_C34437);
 
                                                $("#G1873_C34442").val(item.G1873_C34442);
 
                                                $("#G1873_C34440").val(item.G1873_C34440);
 
                                                $("#G1873_C34441").val(item.G1873_C34441);
 
                                                $("#G1873_C34438").val(item.G1873_C34438);
 
                                                $("#G1873_C34439").val(item.G1873_C34439);
 
                                                $("#G1873_C34443").val(item.G1873_C34443);
 
                                                $("#G1873_C34444").val(item.G1873_C34444);
 
                                                $("#G1873_C34445").val(item.G1873_C34445);
 
                    $("#G1873_C34652").val(item.G1873_C34652).trigger("change"); 
 
                    $("#G1873_C34653").val(item.G1873_C34653).trigger("change"); 
 
                                                $("#G1873_C34446").val(item.G1873_C34446);
 
                                                $("#G1873_C34449").val(item.G1873_C34449);
 
                                                $("#G1873_C34447").val(item.G1873_C34447);
 
                                                $("#G1873_C34448").val(item.G1873_C34448);
 
                                                $("#G1873_C34498").val(item.G1873_C34498);
 
                                                $("#G1873_C34499").val(item.G1873_C34499);
 
                    $("#G1873_C34500").val(item.G1873_C34500).trigger("change"); 
 
                    $("#G1873_C34532").val(item.G1873_C34532).trigger("change"); 
 
                    $("#G1873_C34533").attr("opt",item.G1873_C34533); 
 
                                                $("#G1873_C34571").val(item.G1873_C34571);
 
                    $("#G1873_C34534").val(item.G1873_C34534).trigger("change"); 
 
                    $("#G1873_C34535").attr("opt",item.G1873_C34535); 
 
                                                $("#G1873_C34572").val(item.G1873_C34572);
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
                                        type  : "post",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            console.log(xt);
                                            window.location.href = "quitar.php";
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la información');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','Nombres y Apellidos','¿Qué Persona se Está Comunicando?','Número de contacto','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY','Número de Identificación de familiar','¿En qué grado se encuentra el estudiante?','Nombres y Apellidos del Estudiante que Representa','¿En qué colegio se encuentra el estudiante?','Número Celular de familiar','Teléfono Adicional de familiar','Correo Electrónico de familiar','Edad del estudiante','Ciudad','Localidad','Barrio','Tiene autorización o está acompañado de un adulto','Edad','Colegio Donde Estudia','Grado Que Está Cursando','Ciudad del Estudiante','Barriodel Estudiante','Número de Identificación.','Barrio.','Colegio donde trabaja.','Ciudad.','Nivel que está enseñando este año.','Área que está enseñando este año.','Número Celular.','Teléfono Adicional.','Correo Electrónico.','¿Es Docente de un Establecimiento Educativo Oficial?','¿Tiene nombramiento en propiedad o provisional?','Número de Identificación de Otro','Correo Electrónico de Otro','Número Celular de Otro','Teléfono Adicional de Otro','FECHA DE CITA','HORA DE CITA','CONSULTOR','TIPIFICACION AGENTE ','SUBTIPIFICACION AGENTE','OBSERVACIONES AGENTE','TIPIFICACION BACKOFFICE','SUBTIPIFICACION BACKOFFICE','OBSERVACIONES BACKOFFICE'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G1873_C34421', 
                        index: 'G1873_C34421', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34422', 
                        index:'G1873_C34422', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1924&campo=G1873_C34422'
                        }
                    }

                    ,
                    { 
                        name:'G1873_C34436', 
                        index: 'G1873_C34436', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34418', 
                        index: 'G1873_C34418', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34419', 
                        index: 'G1873_C34419', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34420', 
                        index:'G1873_C34420', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1923&campo=G1873_C34420'
                        }
                    }

                    ,
                    { 
                        name:'G1873_C34424', 
                        index: 'G1873_C34424', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34427', 
                        index: 'G1873_C34427', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34425', 
                        index: 'G1873_C34425', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34426', 
                        index: 'G1873_C34426', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34428', 
                        index: 'G1873_C34428', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34429', 
                        index: 'G1873_C34429', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34430', 
                        index: 'G1873_C34430', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34647', 
                        index: 'G1873_C34647', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34648', 
                        index: 'G1873_C34648', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34649', 
                        index: 'G1873_C34649', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34650', 
                        index: 'G1873_C34650', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34431', 
                        index:'G1873_C34431', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1925&campo=G1873_C34431'
                        }
                    }

                    ,
                    { 
                        name:'G1873_C34651', 
                        index: 'G1873_C34651', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34434', 
                        index: 'G1873_C34434', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34435', 
                        index: 'G1873_C34435', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34432', 
                        index: 'G1873_C34432', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34433', 
                        index: 'G1873_C34433', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34437', 
                        index: 'G1873_C34437', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34442', 
                        index: 'G1873_C34442', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34440', 
                        index: 'G1873_C34440', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34441', 
                        index: 'G1873_C34441', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34438', 
                        index: 'G1873_C34438', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34439', 
                        index: 'G1873_C34439', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34443', 
                        index: 'G1873_C34443', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34444', 
                        index: 'G1873_C34444', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34445', 
                        index: 'G1873_C34445', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34652', 
                        index:'G1873_C34652', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1925&campo=G1873_C34652'
                        }
                    }

                    ,
                    { 
                        name:'G1873_C34653', 
                        index:'G1873_C34653', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1925&campo=G1873_C34653'
                        }
                    }

                    ,
                    { 
                        name:'G1873_C34446', 
                        index: 'G1873_C34446', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34449', 
                        index: 'G1873_C34449', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34447', 
                        index: 'G1873_C34447', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34448', 
                        index: 'G1873_C34448', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    {  
                        name:'G1873_C34498', 
                        index:'G1873_C34498', 
                        width:120 ,
                        editable: true ,
                        formatter: 'text', 
                        searchoptions: {
                            sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
                        }, 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                $(el).datepicker({
                                    language: "es",
                                    autoclose: true,
                                    todayHighlight: true
                                });
                            },
                            defaultValue: function(){
                                var currentTime = new Date();
                                var month = parseInt(currentTime.getMonth() + 1);
                                month = month <= 9 ? "0"+month : month;
                                var day = currentTime.getDate();
                                day = day <= 9 ? "0"+day : day;
                                var year = currentTime.getFullYear();
                                return year+"-"+month + "-"+day;
                            }
                        }
                    }

                    ,
                    {  
                        name:'G1873_C34499', 
                        index:'G1873_C34499', 
                        width:70 ,
                        editable: true ,
                        formatter: 'text', 
                        editoptions:{
                            size:20,
                            dataInit:function(el){
                                //Timepicker
                                var options = { 
                                    now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
                                    twentyFour: true, //Display 24 hour format, defaults to false
                                    title: 'HORA DE CITA', //The Wickedpicker's title,
                                    showSeconds: true, //Whether or not to show seconds,
                                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                                    show: null, //A function to be called when the Wickedpicker is shown
                                    clearable: false, //Make the picker's input clearable (has clickable "x")
                                }; 
                                $(el).wickedpicker(options);
                            }
                        }
                    }

                    ,
                    { 
                        name:'G1873_C34500', 
                        index:'G1873_C34500', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1927&campo=G1873_C34500'
                        }
                    }

                    ,
                    { 
                        name:'G1873_C34532', 
                        index:'G1873_C34532', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1929&campo=G1873_C34532'
                        }
                    }

                    ,
                    { 
                        name:'G1873_C34533', 
                        index:'G1873_C34533', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1930&campo=G1873_C34533'
                        }
                    }

                    ,
                    { 
                        name:'G1873_C34571', 
                        index:'G1873_C34571', 
                        width:150, 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1873_C34534', 
                        index:'G1873_C34534', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1933&campo=G1873_C34534'
                        }
                    }

                    ,
                    { 
                        name:'G1873_C34535', 
                        index:'G1873_C34535', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1934&campo=G1873_C34535'
                        }
                    }

                    ,
                    { 
                        name:'G1873_C34572', 
                        index:'G1873_C34572', 
                        width:150, 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1873_C34421',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario;?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario;?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G1873_C34421").val(item.G1873_C34421);
 
                    $("#G1873_C34422").val(item.G1873_C34422).trigger("change"); 

                        $("#G1873_C34436").val(item.G1873_C34436);

                        $("#G1873_C34418").val(item.G1873_C34418);

                        $("#G1873_C34419").val(item.G1873_C34419);
 
                    $("#G1873_C34420").val(item.G1873_C34420).trigger("change"); 

                        $("#G1873_C34424").val(item.G1873_C34424);

                        $("#G1873_C34427").val(item.G1873_C34427);

                        $("#G1873_C34425").val(item.G1873_C34425);

                        $("#G1873_C34426").val(item.G1873_C34426);

                        $("#G1873_C34428").val(item.G1873_C34428);

                        $("#G1873_C34429").val(item.G1873_C34429);

                        $("#G1873_C34430").val(item.G1873_C34430);

                        $("#G1873_C34647").val(item.G1873_C34647);

                        $("#G1873_C34648").val(item.G1873_C34648);

                        $("#G1873_C34649").val(item.G1873_C34649);

                        $("#G1873_C34650").val(item.G1873_C34650);
 
                    $("#G1873_C34431").val(item.G1873_C34431).trigger("change"); 

                        $("#G1873_C34651").val(item.G1873_C34651);

                        $("#G1873_C34434").val(item.G1873_C34434);

                        $("#G1873_C34435").val(item.G1873_C34435);

                        $("#G1873_C34432").val(item.G1873_C34432);

                        $("#G1873_C34433").val(item.G1873_C34433);

                        $("#G1873_C34437").val(item.G1873_C34437);

                        $("#G1873_C34442").val(item.G1873_C34442);

                        $("#G1873_C34440").val(item.G1873_C34440);

                        $("#G1873_C34441").val(item.G1873_C34441);

                        $("#G1873_C34438").val(item.G1873_C34438);

                        $("#G1873_C34439").val(item.G1873_C34439);

                        $("#G1873_C34443").val(item.G1873_C34443);

                        $("#G1873_C34444").val(item.G1873_C34444);

                        $("#G1873_C34445").val(item.G1873_C34445);
 
                    $("#G1873_C34652").val(item.G1873_C34652).trigger("change"); 
 
                    $("#G1873_C34653").val(item.G1873_C34653).trigger("change"); 

                        $("#G1873_C34446").val(item.G1873_C34446);

                        $("#G1873_C34449").val(item.G1873_C34449);

                        $("#G1873_C34447").val(item.G1873_C34447);

                        $("#G1873_C34448").val(item.G1873_C34448);

                        $("#G1873_C34498").val(item.G1873_C34498);

                        $("#G1873_C34499").val(item.G1873_C34499);
 
                    $("#G1873_C34500").val(item.G1873_C34500).trigger("change"); 
 
                    $("#G1873_C34532").val(item.G1873_C34532).trigger("change"); 
 
                    $("#G1873_C34533").attr("opt",item.G1873_C34533); 

                        $("#G1873_C34571").val(item.G1873_C34571);
 
                    $("#G1873_C34534").val(item.G1873_C34534).trigger("change"); 
 
                    $("#G1873_C34535").attr("opt",item.G1873_C34535); 

                        $("#G1873_C34572").val(item.G1873_C34572);
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
