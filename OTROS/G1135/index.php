<?php 
    /*
        Document   : index
        Created on : 2019-03-01 12:54:16
        Author     : Jose David y su poderoso generador, La gloria sea Para DIOS 
        Url 	   : id = MTEzNQ==  
    */
    $url_crud =  "formularios/G1135/G1135_CRUD_web.php";
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulario de contacto</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
    
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link rel="shortcut icon" href="assets/img/logo_dyalogo_mail.png"/>
        <style>
            
            .hed{
           
                font-family: 'Sansita One', cursive; 
                color:white;
            }

            .hed2{
                text-align:center;
                font-family: 'Sansita One', cursive; 
                font-size:25px; 
                color:#019CDE;
                margin-top: -9px;
            }
            .font_2 {
                font: normal normal normal 17px/1.4em Spinnaker,sans-serif;
                text-align:center;
            }
            
            .redonder {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                -webkit-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
            }

            [class^='select2'] {
                border-radius: 0px !important;
            }
        </style>
    </head>
    <?php  
        echo '<body class="hold-transition" >';
    ?>
    
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" >
                <div class="login-box">
                    <div class="login-logo hed">
                        <img src="assets/img/logo_dyalogo_mail.png"  alt="Dyalogo">
                    </div><!-- /.login-logo -->
                    <div class="login-box-body">
                        <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
                        <form action="formularios/G1135/G1135_CRUD_web.php" method="post" id="formLogin">
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C20667" id="LblG1135_C20667">Código Cliente </label>
								<input type="text" class="form-control input-sm" id="G1135_C20667" value="" disabled name="G1135_C20667"  placeholder="Código Cliente ">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C17483" id="LblG1135_C17483">RAZON SOCIAL</label>
								<input type="text" class="form-control input-sm" id="G1135_C17483" value="" disabled name="G1135_C17483"  placeholder="RAZON SOCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C17484" id="LblG1135_C17484">NOMBRE COMERCIAL</label>
								<input type="text" class="form-control input-sm" id="G1135_C17484" value="" disabled name="G1135_C17484"  placeholder="NOMBRE COMERCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C17485" id="LblG1135_C17485">CONTACTO COMERCIAL</label>
								<input type="text" class="form-control input-sm" id="G1135_C17485" value="" disabled name="G1135_C17485"  placeholder="CONTACTO COMERCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C20668" id="LblG1135_C20668">ID Contacto Comercial </label>
								<input type="text" class="form-control input-sm" id="G1135_C20668" value="" disabled name="G1135_C20668"  placeholder="ID Contacto Comercial ">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C20669" id="LblG1135_C20669">Cuidad de expedición </label>
								<input type="text" class="form-control input-sm" id="G1135_C20669" value="" disabled name="G1135_C20669"  placeholder="Cuidad de expedición ">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C17486" id="LblG1135_C17486">CEDULA NIT</label>
								<input type="text" class="form-control input-sm" id="G1135_C17486" value="" disabled name="G1135_C17486"  placeholder="CEDULA NIT">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C17487" id="LblG1135_C17487">TEL 1</label>
								<input type="text" class="form-control input-sm" id="G1135_C17487" value="" disabled name="G1135_C17487"  placeholder="TEL 1">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C17488" id="LblG1135_C17488">TEL 2</label>
								<input type="text" class="form-control input-sm" id="G1135_C17488" value="" disabled name="G1135_C17488"  placeholder="TEL 2">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C17489" id="LblG1135_C17489">TEL 3</label>
								<input type="text" class="form-control input-sm" id="G1135_C17489" value="" disabled name="G1135_C17489"  placeholder="TEL 3">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C18901" id="LblG1135_C18901">CIUDAD</label>
								<input type="text" class="form-control input-sm" id="G1135_C18901" value="" disabled name="G1135_C18901"  placeholder="CIUDAD">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C17490" id="LblG1135_C17490">DEPARTAMENTO</label>
								<input type="text" class="form-control input-sm" id="G1135_C17490" value="" disabled name="G1135_C17490"  placeholder="DEPARTAMENTO">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C17491" id="LblG1135_C17491">MAIL</label>
								<input type="text" class="form-control input-sm" id="G1135_C17491" value="" disabled name="G1135_C17491"  placeholder="MAIL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C17492" id="LblG1135_C17492">DIRECCION</label>
								<input type="text" class="form-control input-sm" id="G1135_C17492" value="" disabled name="G1135_C17492"  placeholder="DIRECCION">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C17493" id="LblG1135_C17493">FECHA DE CARGUE</label>
								<input type="text" class="form-control input-sm" id="G1135_C17493" value="" disabled name="G1135_C17493"  placeholder="FECHA DE CARGUE">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C17494" id="LblG1135_C17494">CLASIFICACION</label>
								<input type="text" class="form-control input-sm" id="G1135_C17494" value="" disabled name="G1135_C17494"  placeholder="CLASIFICACION">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C18902" id="LblG1135_C18902">CUPO CLIENTE</label>
								<input type="text" class="form-control input-sm" id="G1135_C18902" value="" disabled name="G1135_C18902"  placeholder="CUPO CLIENTE">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1135_C18903" id="LblG1135_C18903">ESTADO CLIENTE</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18903" id="G1135_C18903">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 865 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1135_C18904" id="LblG1135_C18904">FECHA ACTIVACIÓN</label>
                                <input type="text" class="form-control input-sm Fecha" value="" disabled name="G1135_C18904" id="G1135_C18904" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIMEPICKER -->
                            <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1135_C20954" id="LblG1135_C20954">HORA DE ACTIVACIÓN </label>
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm Hora" disabled name="G1135_C20954" id="G1135_C20954" placeholder="HH:MM:SS" >
                                    <div class="input-group-addon" id="TMP_G1135_C20954">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1135_C18905" id="LblG1135_C18905">FECHA VENCIMIENTO</label>
                                <input type="text" class="form-control input-sm Fecha" value="" disabled name="G1135_C18905" id="G1135_C18905" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C22656" id="LblG1135_C22656">Agente-call</label>
								<input type="text" class="form-control input-sm" id="G1135_C22656" value="" disabled name="G1135_C22656"  placeholder="Agente-call">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1135_C18906" id="LblG1135_C18906">¿Es ó ha sido cliente de Fincaraiz?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18906" id="G1135_C18906">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 827 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1135_C18907" id="LblG1135_C18907">¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18907" id="G1135_C18907">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 827 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
                            <?php 
                            $str_Lsql = "SELECT  G1188_ConsInte__b as id  FROM ".$BaseDatos_systema.".G1188";
                            ?>
                            <!-- CAMPO DE TIPO GUION -->
                            <div class="form-group">
                                <label for="G1135_C18908" id="LblG1135_C18908">¿Desde que ciudad realiza la administración de los inmuebles?</label>
                                <select class="form-control input-sm str_Select2" style="width: 100%;"  name="G1135_C18908" id="G1135_C18908">
                                    <option></option>
                                    <?php
                                        /*
                                            SE RECORRE LA CONSULTA QUE TRAE LOS CAMPOS DEL GUIÓN
                                        */
                                        $combo = $mysqli->query($str_Lsql);
                                        while($obj = $combo->fetch_object()){
                                            echo "<option value='".$obj->id."' dinammicos='0'></option>";

                                        }    
                                        
                                    ?>
                                </select>
                            </div>
                            <!-- FIN DEL CAMPO TIPO LISTA -->
 
                            <!-- CAMPO TIPO ENTERO -->
                            <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1135_C18909" id="LblG1135_C18909">¿Cuántos inmuebles USADOS tiene en su inventario?</label>
                                <input type="text" class="form-control input-sm Numerico" value="" disabled name="G1135_C18909" id="G1135_C18909" placeholder="¿Cuántos inmuebles USADOS tiene en su inventario?">
                            </div>
                            <!-- FIN DEL CAMPO TIPO ENTERO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1135_C18910" id="LblG1135_C18910">Cupo autorizado</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18910" id="G1135_C18910">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 977 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
                            <!-- CAMPO TIPO ENTERO -->
                            <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1135_C18911" id="LblG1135_C18911">¿Cuántos inmuebles NUEVOS tiene en su inventario?</label>
                                <input type="text" class="form-control input-sm Numerico" value="" disabled name="G1135_C18911" id="G1135_C18911" placeholder="¿Cuántos inmuebles NUEVOS tiene en su inventario?">
                            </div>
                            <!-- FIN DEL CAMPO TIPO ENTERO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C21447" id="LblG1135_C21447">Cupo Usado </label>
								<input type="text" class="form-control input-sm" id="G1135_C21447" value=""  name="G1135_C21447"  placeholder="Cupo Usado ">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C21448" id="LblG1135_C21448">Cupo Nuevo</label>
								<input type="text" class="form-control input-sm" id="G1135_C21448" value=""  name="G1135_C21448"  placeholder="Cupo Nuevo">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1135_C18645" id="LblG1135_C18645">Tipo de cliente</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18645" id="G1135_C18645">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 912 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Lectura de contrato</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1135_C17507" id="LblG1135_C17507">Fecha lectura de contrato</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G1135_C17507" id="G1135_C17507" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIMEPICKER -->
                            <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1135_C17508" id="LblG1135_C17508">Hora de lectura de contrato</label>
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm Hora"  name="G1135_C17508" id="G1135_C17508" placeholder="HH:MM:SS" >
                                    <div class="input-group-addon" id="TMP_G1135_C17508">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1135_C17509" id="LblG1135_C17509">Extensión</label>
								<input type="text" class="form-control input-sm" id="G1135_C17509" value=""  name="G1135_C17509"  placeholder="Extensión">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1135_C18646" id="LblG1135_C18646">Pack adquirido</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18646" id="G1135_C18646">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 978 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1135_C18647" id="LblG1135_C18647">Cupo</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18647" id="G1135_C18647">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 977 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1135_C18648" id="LblG1135_C18648">Tiempo</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18648" id="G1135_C18648">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 979 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1135_C18649" id="LblG1135_C18649">Descuento aplicado</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18649" id="G1135_C18649">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 980 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1135_C18650" id="LblG1135_C18650">Fecha de activación</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G1135_C18650" id="G1135_C18650" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIMEPICKER -->
                            <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1135_C20955" id="LblG1135_C20955">Hora de activación </label>
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm Hora"  name="G1135_C20955" id="G1135_C20955" placeholder="HH:MM:SS" >
                                    <div class="input-group-addon" id="TMP_G1135_C20955">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
  
                            <!-- CAMPO TIPO FECHA -->
                            <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G1135_C18651" id="LblG1135_C18651">Fecha fin</label>
                                <input type="text" class="form-control input-sm Fecha" value=""  name="G1135_C18651" id="G1135_C18651" placeholder="YYYY-MM-DD">
                            </div>
                            <!-- FIN DEL CAMPO TIPO FECHA-->
  
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="G1135_C17512" id="LblG1135_C17512">Observación</label>
                                <textarea class="form-control input-sm" name="G1135_C17512" id="G1135_C17512"  value="" placeholder="Observación"></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->

                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="id" id="hidId" value='<?php if(isset($_GET['u'])){ echo $_GET['u']; }else{ echo "0"; } ?>'>
                            <input type="hidden" name="oper" id="oper" value='add'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formpadre" id="formpadre" value='<?php if(isset($_GET['formularioPadre'])){ echo $_GET['formularioPadre']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formhijo" id="formhijo" value='<?php if(isset($_GET['formulario'])){ echo $_GET['formulario']; }else{ echo "0"; }?>' >
                            <input type= "hidden" name="campana" id="campana" value="<?php if(isset($_GET['camp'])){ echo base64_decode($_GET['camp']); }else{ echo "0"; }?>">
                            <div class="row">
                                <div class="col-xs-2">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-xs-8">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar datos</button>
                                </div><!-- /.col -->
                            </div>
                        </form>
                    </div><!-- /.login-box-body -->
                </div><!-- /.login-box -->
            </div>
        </div>


        <!-- jQuery 2.2.3 -->
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>  
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/numeric.js"></script>

        <script type="text/javascript" src="formularios/G1135/G1135_eventos.js"></script>
        <script type="text/javascript">
            $.validator.setDefaults({
                submitHandler: function() { 
                     $("#formLogin").submit();
                }
            });

            $(function(){

                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };

                //str_Select2 estos son los guiones
                


        $("#G1135_C18908").select2({ 
            templateResult: function(data) {
                var r = data.text.split('|');
                var $result = $(
                    '<div class="row">' +
                        
                    '</div>'
                );
                return $result;
            },
            templateSelection : function(data){
                var r = data.text.split('|');
                return r[0];
            }
        });

        $("#G1135_C18908").change(function(){
            var valores = $("#G1135_C18908 option:selected").text();
            var campos = $("#G1135_C18908 option:selected").attr("dinammicos");
            var r = valores.split('|');
            if(r.length > 1){

                var c = campos.split('|');
                for(i = 1; i < r.length; i++){
                    if(!$("#"+c[i]).is("select")) {
                    // the input field is not a select
                        $("#"+c[i]).val(r[i]);
                    }else{
                        var change = r[i].replace(' ', ''); 
                        $("#"+c[i]).val(change).change();
                    }
                    
                }
            }
        });

                //datepickers
                

            $("#G1135_C18904").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G1135_C18905").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G1135_C17499").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G1135_C17507").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G1135_C18650").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

            $("#G1135_C18651").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

                //Timepickers
                


            //Timepicker
            $("#G1135_C20954").timepicker({
                'timeFormat': 'H:i:s',
                'minTime': '08:00:00',
                'maxTime': '17:00:00',
                'setTime': '08:00:00',
                'step'  : '5',
                'showDuration': true
            });

            //Timepicker
            $("#G1135_C17500").timepicker({
                'timeFormat': 'H:i:s',
                'minTime': '08:00:00',
                'maxTime': '17:00:00',
                'setTime': '08:00:00',
                'step'  : '5',
                'showDuration': true
            });

            //Timepicker
            $("#G1135_C17508").timepicker({
                'timeFormat': 'H:i:s',
                'minTime': '08:00:00',
                'maxTime': '17:00:00',
                'setTime': '08:00:00',
                'step'  : '5',
                'showDuration': true
            });

            //Timepicker
            $("#G1135_C20955").timepicker({
                'timeFormat': 'H:i:s',
                'minTime': '08:00:00',
                'maxTime': '17:00:00',
                'setTime': '08:00:00',
                'step'  : '5',
                'showDuration': true
            });

                //Validaciones numeros Enteros
                

            $("#G1135_C17497").numeric();
            
            $("#G1135_C17498").numeric();
            
            $("#G1135_C18909").numeric();
            
            $("#G1135_C18911").numeric();
            

                //Validaciones numeros Decimales
               


               //Si tiene dependencias
               


    //function para ESTADO CLIENTE 

    $("#G1135_C18903").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Es ó ha sido cliente de Fincaraiz? 

    $("#G1135_C18906").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz? 

    $("#G1135_C18907").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Cupo autorizado 

    $("#G1135_C18910").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Tipo de cliente 

    $("#G1135_C18645").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Pack adquirido 

    $("#G1135_C18646").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Cupo 

    $("#G1135_C18647").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Tiempo 

    $("#G1135_C18648").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para Descuento aplicado 

    $("#G1135_C18649").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
                

               <?php
                    if(isset($_GET['result'])){
                        if($_GET['result'] ==  1){
                ?>
                        swal({
                            title: "Exito!",
                            text: "Recibimos su solicitud, pronto estaremos en contacto",
                            type: "success",
                            confirmButtonText: "Ok"
                        },function(){

                        });
                        
                        
                <?php   }else{ ?>
                            swal({
                                title: "Error!",
                                text: 'Ocurrio un error, intenta mas tarde',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }
                    }
                ?>
            });
        </script>
        <Script type="text/javascript">
            $(document).ready(function() {
                <?php
                $campana = base64_decode($_GET['camp']);
                $Guion = 0;//id de la campaña
                $tabla = 0;// $_GET['u'];//ide del usuario
                $Lsql = "SELECT CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConsInte__GUION__Pob_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$campana;

                $result = $mysqli->query($Lsql);
                while($obj = $result->fetch_object()){
                    $Guion = $obj->CAMPAN_ConsInte__GUION__Gui_b;
                    $tabla = $obj->CAMPAN_ConsInte__GUION__Pob_b;
                } 
                //SELECT de la camic
                $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b, CAMINC_ConsInte__CAMPO_Gui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$campana;
                
                $resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    
                    //Pregfuntar por el tipo de dato
                    $Lsql = "SELECT PREGUN_Tipo______b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__b = ".$key->CAMINC_ConsInte__CAMPO_Gui_b;
                    $res = $mysqli->query($Lsql);
                    $datos = $res->fetch_array();


                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['u'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ 
                            if(!is_null($objDatos->campo) && $objDatos->campo != ''){

                                if($datos['PREGUN_Tipo______b'] != '8'){
                            ?>
                                    document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                            <?php  
                                }else{
                                    if($objDatos->campo == '1'){
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , true);";
                                    }else{
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , false);";
                                    }
                                    
                                } 
                            }
                        }
                    }
                    
                }
                ?>
            });
        </script>
        
