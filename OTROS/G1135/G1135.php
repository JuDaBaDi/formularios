
<?php date_default_timezone_set('America/Bogota'); ?>
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edición</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1135/G1135_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

	$PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

	if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1135_ConsInte__b as id, G1135_C17483 as camp1 , G1135_C17485 as camp2, LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1135 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1135_C17496  WHERE G1135_Usuario = ".$idUsuario." ORDER BY field (G1135_C17496,14272,14273,14274,14275,14276), G1135_FechaInsercion DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1135_ConsInte__b as id, G1135_C17483 as camp1 , G1135_C17485 as camp2, LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1135 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1135_C17496  ORDER BY field (G1135_C17496,14272,14273,14274,14275,14276), G1135_FechaInsercion DESC LIMIT 0, 50";
        }
    }else{
        $Zsql = "SELECT G1135_ConsInte__b as id, G1135_C17483 as camp1 , G1135_C17485 as camp2,  LISOPC_Nombre____b as estado FROM ".$BaseDatos.".G1135 JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G1135_C17496  ORDER BY field (G1135_C17496,14272,14273,14274,14275,14276), G1135_FechaInsercion DESC LIMIT 0, 50";
    }


   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>

<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="box box-primary box-solid">
			<div class="box-header">
                <h3 class="box-title">Historico de gestiones</h3>
            </div>
			<div class="box-body">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Gesti&oacute;n</th>
							<th>Comentarios</th>
							<th>Fecha - hora</th>
							<th>Agente</th>
						</tr>
					</thead>
					<tbody id="tablaGestiones">
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="2182" >


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C20667" id="LblG1135_C20667">Código Cliente </label>
			            <input type="text" class="form-control input-sm" id="G1135_C20667" value="" readonly name="G1135_C20667"  placeholder="Código Cliente ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_CodigoMiembro" id="LblG1135_CodigoMiembro">CODIGO MIEMBRO</label>
			            <input type="text" class="form-control input-sm" id="G1135_CodigoMiembro" value="" readonly name="G1135_CodigoMiembro"  placeholder="CODIGO MIEMBRO">
			        </div>

  
            </div>


            <div class="col-md-6 col-xs-6">

 					<div class="form-group">
			            <label for="G1135_Usuario" id="LblG1135_Usuario">AGENTE-CALL</label>
			            <input type="text" style="color:#0897B0" class="form-control input-sm" id="G1135_Usuario" value="" readonly name="G1135_Usuario"  placeholder="NOMBRE AGENTE">
			            

			        </div>

			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C17483" id="LblG1135_C17483">RAZON SOCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1135_C17483" value="" readonly name="G1135_C17483"  placeholder="RAZON SOCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C17484" id="LblG1135_C17484">NOMBRE COMERCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1135_C17484" value="" readonly name="G1135_C17484"  placeholder="NOMBRE COMERCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C17485" id="LblG1135_C17485">CONTACTO COMERCIAL</label>
			            <input type="text" class="form-control input-sm" id="G1135_C17485" value="" readonly name="G1135_C17485"  placeholder="CONTACTO COMERCIAL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>



        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C20668" id="LblG1135_C20668">ID Contacto Comercial </label>
			            <input type="text" class="form-control input-sm" id="G1135_C20668" value="" readonly name="G1135_C20668"  placeholder="ID Contacto Comercial ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C20669" id="LblG1135_C20669">Cuidad de expedición </label>
			            <input type="text" class="form-control input-sm" id="G1135_C20669" value="" readonly name="G1135_C20669"  placeholder="Cuidad de expedición ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C17486" id="LblG1135_C17486">CEDULA NIT</label>
			            <input type="text" class="form-control input-sm" id="G1135_C17486" value="" readonly name="G1135_C17486"  placeholder="CEDULA NIT">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C17487" id="LblG1135_C17487">TEL 1</label>
			            <input type="text" class="form-control input-sm" id="G1135_C17487" value="" readonly name="G1135_C17487"  placeholder="TEL 1">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C17488" id="LblG1135_C17488">TEL 2</label>
			            <input type="text" class="form-control input-sm" id="G1135_C17488" value="" readonly name="G1135_C17488"  placeholder="TEL 2">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C17489" id="LblG1135_C17489">TEL 3</label>
			            <input type="text" class="form-control input-sm" id="G1135_C17489" value="" readonly name="G1135_C17489"  placeholder="TEL 3">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C18901" id="LblG1135_C18901">CIUDAD</label>
			            <input type="text" class="form-control input-sm" id="G1135_C18901" value="" readonly name="G1135_C18901"  placeholder="CIUDAD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C17490" id="LblG1135_C17490">DEPARTAMENTO</label>
			            <input type="text" class="form-control input-sm" id="G1135_C17490" value="" readonly name="G1135_C17490"  placeholder="DEPARTAMENTO">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C17491" id="LblG1135_C17491">MAIL</label>
			            <input type="text" class="form-control input-sm" id="G1135_C17491" value="" readonly name="G1135_C17491"  placeholder="MAIL">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C17492" id="LblG1135_C17492">DIRECCION</label>
			            <input type="text" class="form-control input-sm" id="G1135_C17492" value="" readonly name="G1135_C17492"  placeholder="DIRECCION">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C17493" id="LblG1135_C17493">FECHA DE CARGUE</label>
			            <input type="text" class="form-control input-sm" id="G1135_C17493" value="" readonly name="G1135_C17493"  placeholder="FECHA DE CARGUE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C17494" id="LblG1135_C17494">CLASIFICACION</label>
			            <input type="text" class="form-control input-sm" id="G1135_C17494" value="" readonly name="G1135_C17494"  placeholder="CLASIFICACION">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C18902" id="LblG1135_C18902">CUPO CLIENTE</label>
			            <input type="text" class="form-control input-sm" id="G1135_C18902" value="" readonly name="G1135_C18902"  placeholder="CUPO CLIENTE">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1135_C18903" id="LblG1135_C18903">ESTADO CLIENTE</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18903" readonly id="G1135_C18903">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 865 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1135_C18904" id="LblG1135_C18904">FECHA ACTIVACIÓN</label>
			            <input type="text" class="form-control input-sm Fecha" value="" readonly name="G1135_C18904" id="G1135_C18904" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIMEPICKER -->
			        <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
			        <div class="bootstrap-timepicker">
			            <div class="form-group">
			                <label for="G1135_C20954" id="LblG1135_C20954">HORA DE ACTIVACIÓN </label>
			                <div class="input-group">
			                    <input type="text" class="form-control input-sm Hora" readonly name="G1135_C20954" id="G1135_C20954" placeholder="HH:MM:SS" >
			                    <div class="input-group-addon" id="TMP_G1135_C20954">
			                        <i class="fa fa-clock-o"></i>
			                    </div>
			                </div>
			                <!-- /.input group -->
			            </div>
			            <!-- /.form group -->
			        </div>
			        <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			 
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->

			        <div class="form-group">
			            <label for="G1135_C18905" id="LblG1135_C18905">FECHA VENCIMIENTO</label>
			            <input type="text" class="form-control input-sm Fecha" value="" readonly name="G1135_C18905" id="G1135_C18905" placeholder="YYYY-MM-DD">
			        </div>			        



			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


  
        </div>


		


</div>

<div id="2184" style='display:none;'>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C17502" id="LblG1135_C17502">Agente</label>
			            <input type="text" class="form-control input-sm" id="G1135_C17502" value="<?php echo getNombreUser($token);?>" readonly name="G1135_C17502"  placeholder="Agente">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C17503" id="LblG1135_C17503">Fecha</label>
			            <input type="text" class="form-control input-sm" id="G1135_C17503" value="<?php echo date('Y-m-d');?>" readonly name="G1135_C17503"  placeholder="Fecha">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C17504" id="LblG1135_C17504">Hora</label>
			            <input type="text" class="form-control input-sm" id="G1135_C17504" value="<?php echo date('H:i:s');?>" readonly name="G1135_C17504"  placeholder="Hora">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C17505" id="LblG1135_C17505">Campaña</label>
			            <input type="text" class="form-control input-sm" id="G1135_C17505" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"];}else{
                	echo "SIN CAMPAÑA";}?>" readonly name="G1135_C17505"  placeholder="Campaña">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2403">
                DATOS DE PERFIL
            </a>
        </h4>
    </div>
    <div id="s_2403" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1135_C18906" id="LblG1135_C18906">¿Es ó ha sido cliente de Fincaraiz?</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18906" readonly id="G1135_C18906">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 827 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1135_C18907" id="LblG1135_C18907">¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18907" readonly id="G1135_C18907">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 827 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			       
			        <!-- CAMPO DE TIPO GUION -->
			        <div class="form-group">
			            <label for="G1135_C18908" id="LblG1135_C18908">¿Desde que ciudad realiza la administración de los inmuebles?</label>
			            <select class="form-control input-sm select2" style="width: 100%;" readonly  name="G1135_C18908" id="G1135_C18908">
			                <option value="0" id="opcionVacia">Seleccione</option>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1135_C18909" id="LblG1135_C18909">¿Cuántos inmuebles USADOS tiene en su inventario?</label>
			            <input type="text" class="form-control input-sm Numerico" value="" readonly name="G1135_C18909" id="G1135_C18909" placeholder="¿Cuántos inmuebles USADOS tiene en su inventario?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1135_C18910" id="LblG1135_C18910">Cupo autorizado</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18910" readonly id="G1135_C18910">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 977 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1135_C18911" id="LblG1135_C18911">¿Cuántos inmuebles NUEVOS tiene en su inventario?</label>
			            <input type="text" class="form-control input-sm Numerico" value="" readonly name="G1135_C18911" id="G1135_C18911" placeholder="¿Cuántos inmuebles NUEVOS tiene en su inventario?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>

  
        </div>


        </div>
    </div>
</div>

<div class="panel box box-primary" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_2186">
                Datos tarea
            </a>
        </h4>
    </div>
    <div id="s_2186" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C21447" id="LblG1135_C21447">Cupo Usado </label>
			            <input type="text" class="form-control input-sm" id="G1135_C21447" value="" disabled="disabled"  name="G1135_C21447"  placeholder="Cupo Usado ">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C21448" id="LblG1135_C21448">Cupo Nuevo</label>
			            <input type="text" class="form-control input-sm" id="G1135_C21448" value="" disabled="disabled" name="G1135_C21448"  placeholder="Cupo Nuevo">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1135_C18645" id="LblG1135_C18645">Tipo de cliente</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18645" disabled="disabled" id="G1135_C18645">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 912 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- lIBRETO O LABEL -->
			        <p style="text-align:justify;">Lectura de contrato</p>
			        <!-- FIN LIBRETO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1135_C17507" id="LblG1135_C17507">Fecha lectura de contrato</label>
			            <input type="text" class="form-control input-sm Fecha" value=""  name="G1135_C17507" disabled="disabled" id="G1135_C17507" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIMEPICKER -->
			        <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
			        <div class="bootstrap-timepicker">
			            <div class="form-group">
			                <label for="G1135_C17508" id="LblG1135_C17508">Hora de lectura de contrato</label>
			                <div class="input-group">
			                    <input type="text" class="form-control input-sm Hora"  name="G1135_C17508" disabled="disabled" id="G1135_C17508" placeholder="HH:MM:SS" >
			                    <div class="input-group-addon" id="TMP_G1135_C17508">
			                        <i class="fa fa-clock-o"></i>
			                    </div>
			                </div>
			                <!-- /.input group -->
			            </div>
			            <!-- /.form group -->
			        </div>
			        <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G1135_C17509" id="LblG1135_C17509">Extensión</label>
			            <input type="text" class="form-control input-sm" id="G1135_C17509" value="" disabled="disabled" name="G1135_C17509"  placeholder="Extensión">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1135_C18646" id="LblG1135_C18646">Pack adquirido</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18646" disabled="disabled" id="G1135_C18646">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 978 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1135_C18647" id="LblG1135_C18647">Cupo</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18647" disabled="disabled" id="G1135_C18647">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 977 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1135_C18648" id="LblG1135_C18648">Tiempo</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18648" disabled="disabled" id="G1135_C18648">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 979 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G1135_C18649" id="LblG1135_C18649">Descuento aplicado</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G1135_C18649" disabled="disabled" id="G1135_C18649">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 980 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1135_C18650" id="LblG1135_C18650">Fecha de activación</label>
			            <input type="text" class="form-control input-sm Fecha" value=""  name="G1135_C18650" disabled="disabled" id="G1135_C18650" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIMEPICKER -->
			        <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
			        <div class="bootstrap-timepicker">
			            <div class="form-group">
			                <label for="G1135_C20955" id="LblG1135_C20955">Hora de activación </label>
			                <div class="input-group">
			                    <input type="text" class="form-control input-sm Hora"  name="G1135_C20955" disabled="disabled" id="G1135_C20955" placeholder="HH:MM:SS" >
			                    <div class="input-group-addon" id="TMP_G1135_C20955">
			                        <i class="fa fa-clock-o"></i>
			                    </div>
			                </div>
			                <!-- /.input group -->
			            </div>
			            <!-- /.form group -->
			        </div>
			        <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div>


            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G1135_C18651" id="LblG1135_C18651">Fecha fin</label>
			            <input type="text" class="form-control input-sm Fecha" value="" disabled="disabled" name="G1135_C18651" id="G1135_C18651" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
			        <!-- CAMPO TIPO MEMO -->
			        <div class="form-group">
			            <label for="G1135_C17512" id="LblG1135_C17512">Observación</label>
			            <textarea class="form-control input-sm" name="G1135_C17512" id="G1135_C17512"  value="" disabled="disabled" placeholder="Observación"></textarea>
			        </div>
			        <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div>


        </div>


        </div>
    </div>
</div>

<div id="2183" >


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

  
            </div>


            <div class="col-md-6 col-xs-6">

  
            </div>

  
        </div>


</div>

<input type="hidden" name="campana_crm" id="campana_crm" value="<?php if(isset($_GET["campana_crm"])){echo $_GET["campana_crm"];}else{ echo "567";}?>">
<div class="row" style="background-color: #FAFAFA; ">
	<br/>
    <div class="col-md-5 col-xs-5">
        <div class="form-group">
       		<label for="G1135_C17495">Tipificaci&oacute;n</label>
            <select class="form-control input-sm tipificacionBackOffice" name="tipificacion" id="G1135_C17495">
                <option value="0">Seleccione</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 851;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-5 col-xs-5">
        <div class="form-group">
        	<label for="G1135_C17496">Estado de la tarea</label>
            <select class="form-control input-sm reintento" name="reintento" id="G1135_C17496">
                <option value="0">Seleccione</option>
                <?php
                	$Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC 
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 852;";
	                $obj = $mysqli->query($Lsql);
	                while($obje = $obj->fetch_object()){
	                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";
	                }      
                ?>
            </select>     
        </div>
    </div>
    <div class="col-md-2 col-xs-2" style="text-align: center;">
    	<label for="G1135_C17496" style="visibility:hidden;">Estado de la tarea</label>
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Guardar Gesti&oacute;n
        </button>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G1135_C17501" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/pies.php");?>
<script type="text/javascript" src="formularios/G1135/G1135_eventos.js"></script> 
<script type="text/javascript">
    $(function(){

    	var meses = new Array(12);
    	meses[0] = "01";
    	meses[1] = "02";
    	meses[2] = "03";
    	meses[3] = "04";
    	meses[4] = "05";
    	meses[5] = "06";
    	meses[6] = "07";
    	meses[7] = "08";
    	meses[8] = "09";
    	meses[9] = "10";
    	meses[10] = "11";
    	meses[11] = "12";

    	var d = new Date();
        var h = d.getHours();
        var horas = (h < 10) ? '0' + h : h;
        var dia = d.getDate();
        var dias = (dia < 10) ? '0' + dia : dia;
        var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
        $("#FechaInicio").val(fechaInicial);
            

        //Esta es por si lo llaman en modo formulario de edicion LigthBox
        <?php if(isset($_GET['registroId'])){ ?>
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
            dataType : 'json',
            success  : function(data){
                //recorrer datos y enviarlos al formulario
                $.each(data, function(i, item) {
                    
 
                    $("#G1135_C20667").val(item.G1135_C20667);

                    $("#G1135_CodigoMiembro").val(item.G1135_CodigoMiembro);

                    $("#G1135_Usuario").val(item.G1135_Usuario);
 
                    $("#G1135_C17483").val(item.G1135_C17483);
 
                    $("#G1135_C17484").val(item.G1135_C17484);
 
                    $("#G1135_C17485").val(item.G1135_C17485);
 
                    $("#G1135_C20668").val(item.G1135_C20668);
 
                    $("#G1135_C20669").val(item.G1135_C20669);
 
                    $("#G1135_C17486").val(item.G1135_C17486);
 
                    $("#G1135_C17487").val(item.G1135_C17487);
 
                    $("#G1135_C17488").val(item.G1135_C17488);
 
                    $("#G1135_C17489").val(item.G1135_C17489);
 
                    $("#G1135_C18901").val(item.G1135_C18901);
 
                    $("#G1135_C17490").val(item.G1135_C17490);
 
                    $("#G1135_C17491").val(item.G1135_C17491);
 
                    $("#G1135_C17492").val(item.G1135_C17492);
 
                    $("#G1135_C17493").val(item.G1135_C17493);
 
                    $("#G1135_C17494").val(item.G1135_C17494);
 
                    $("#G1135_C18902").val(item.G1135_C18902);
 
                    $("#G1135_C18903").val(item.G1135_C18903);
 
                    $("#G1135_C18904").val(item.G1135_C18904);
 
                    $("#G1135_C20954").val(item.G1135_C20954);
 
                    $("#G1135_C18905").val(item.G1135_C18905);
 
                    $("#G1135_C22656").val(item.G1135_C22656);
 
                    $("#G1135_C17495").val(item.G1135_C17495);
 
                    $("#G1135_C17496").val(item.G1135_C17496);
 
                    $("#G1135_C17497").val(item.G1135_C17497);
 
                    $("#G1135_C17498").val(item.G1135_C17498);
 
                    $("#G1135_C17499").val(item.G1135_C17499);
 
                    $("#G1135_C17500").val(item.G1135_C17500);
 
                    $("#G1135_C17501").val(item.G1135_C17501);
 
                    $("#G1135_C17502").val(item.G1135_C17502);
 
                    $("#G1135_C17503").val(item.G1135_C17503);
 
                    $("#G1135_C17504").val(item.G1135_C17504);
 
                    $("#G1135_C17505").val(item.G1135_C17505);
 
                    $("#G1135_C21447").val(item.G1135_C21447);
 
                    $("#G1135_C21448").val(item.G1135_C21448);
 
                    $("#G1135_C18645").val(item.G1135_C18645);
   
                    if(item.G1135_C17506 == 1){
                        $("#G1135_C17506").attr('checked', true);
                    } 
 
                    $("#G1135_C17507").val(item.G1135_C17507);
 
                    $("#G1135_C17508").val(item.G1135_C17508);
 
                    $("#G1135_C17509").val(item.G1135_C17509);
 
                    $("#G1135_C18646").val(item.G1135_C18646);
 
                    $("#G1135_C18647").val(item.G1135_C18647);
 
                    $("#G1135_C18648").val(item.G1135_C18648);
 
                    $("#G1135_C18649").val(item.G1135_C18649);
 
                    $("#G1135_C18650").val(item.G1135_C18650);
 
                    $("#G1135_C20955").val(item.G1135_C20955);
 
                    $("#G1135_C18651").val(item.G1135_C18651);
 
                    $("#G1135_C17512").val(item.G1135_C17512);
 
                    $("#G1135_C18906").val(item.G1135_C18906);
 
                    $("#G1135_C18907").val(item.G1135_C18907);
 
                    $("#G1135_C18908").val(item.G1135_C18908);
 
                    $("#G1135_C18908").val(item.G1135_C18908).trigger("change"); 
 
                    $("#G1135_C18909").val(item.G1135_C18909);
 
                    $("#G1135_C18910").val(item.G1135_C18910);
 
                    $("#G1135_C18911").val(item.G1135_C18911);

					$("#h3mio").html(item.principal);

                });
                //Deshabilitar los campos

                //Habilitar todos los campos para edicion
                $('#FormularioDatos :input').each(function(){
                    $(this).attr('disabled', true);
                });

                              

                //Habilidar los botones de operacion, add, editar, eliminar
                $("#add").attr('disabled', false);
                $("#edit").attr('disabled', false);
                $("#delete").attr('disabled', false);

                //Desahabiliatra los botones de salvar y seleccionar_registro
                $("#cancel").attr('disabled', true);
                $("#Save").attr('disabled', true);
            } 
        });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

     	$("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
        	/*
        	vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
        	idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G1135_C18645").select2();

    $("#G1135_C18646").select2();

    $("#G1135_C18647").select2();

    $("#G1135_C18648").select2();

    $("#G1135_C18649").select2();
        //datepickers
        

        $("#G1135_C18904").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1135_C18905").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1135_C17499").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1135_C17507").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1135_C18650").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G1135_C18651").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'HORA DE ACTIVACIÓN ', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1135_C20954").wickedpicker(options);

        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1135_C17500").wickedpicker(options);

        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora de lectura de contrato', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1135_C17508").wickedpicker(options);

        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora de activación ', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1135_C20955").wickedpicker(options);

        //Validaciones numeros Enteros
        

    	$("#G1135_C17497").numeric();
		        
    	$("#G1135_C17498").numeric();
		        
    	$("#G1135_C18909").numeric();
		        
    	$("#G1135_C18911").numeric();
		        

        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO CLIENTE 

    $("#G1135_C18903").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para ¿Es ó ha sido cliente de Fincaraiz? 

    $("#G1135_C18906").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para ¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz? 

    $("#G1135_C18907").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Cupo autorizado 

    $("#G1135_C18910").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Tipo de cliente 

    $("#G1135_C18645").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Pack adquirido 

    $("#G1135_C18646").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Cupo 

    $("#G1135_C18647").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Tiempo 

    $("#G1135_C18648").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });

    //function para Descuento aplicado 

    $("#G1135_C18649").change(function(){  
    	//Esto es la parte de las listas dependientes
    	

    });


        $("#G1135_C17495").change(function(){
        	var id = $(this).attr('id');
            var valor = $("#"+ id +" option:selected").attr('efecividad');
            var monoef = $("#"+ id +" option:selected").attr('monoef');
            var TipNoEF = $("#"+ id +" option:selected").attr('TipNoEF');
            var cambio = $("#"+ id +" option:selected").attr('cambio');
            var importancia = $("#"+ id + " option:selected").attr('importancia');
            var contacto = $("#"+id+" option:selected").attr('contacto');
            $(".reintento").val(TipNoEF).change();
            $("#Efectividad").val(valor);
            $("#MonoEf").val(monoef);
            $("#TipNoEF").val(TipNoEF);
            $("#MonoEfPeso").val(importancia);
            $("#ContactoMonoEf").val(contacto);
    	});
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
        	$("#Save").attr('disabled', true);
        	var bol_respuesta = before_save();
        	var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
            	alertify.error("Es necesaria la tipificación!");
            	valido = 1;
            }

            if($(".reintento").val() == '2'){
            	if($(".TxtFechaReintento").val().length < 1){
            		alertify.error("Es necesario llenar la fecha de reintento!");
            		$(".TxtFechaReintento").focus();
            		valido = 1;
            	}

            	if($(".TxtHoraReintento").val().length < 1){
            		alertify.error("Es necesario llenar la hora de reintento!");
            		$(".TxtHoraReintento").focus();
            		valido = 1;
            	}
            }

            if(valido == '0'){
	        	if(bol_respuesta){            
		            var form = $("#FormularioDatos");
		            //Se crean un array con los datos a enviar, apartir del formulario 
		            var formData = new FormData($("#FormularioDatos")[0]);
		            if($("#G1135_C17495 option:selected").text() == 'Devuelta'){
	                	formData.append("tareaDevuelta","SI");
	                }
		            $.ajax({
		               url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "567"; } ?>',  
		                type: 'POST',
		                data: formData,
		                cache: false,
		                contentType: false,
		                processData: false,
		                //una vez finalizado correctamente
		                success: function(data){
		                    if(data != '0'){
		                    	$("#Save").attr('disabled', false);
			                    alertify.success('Información guardada con exito');   
		                    }else{
		                        //Algo paso, hay un error
		                        $("#Save").attr('disabled', false);
		                        alertify.error('Un error ha ocurrido y no pudimos guardar la información');
		                    }                
		                },
		                //si ha ocurrido un error
		                error: function(){
		                    after_save_error();
		                    $("#Save").attr('disabled', false);
		                    alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
		                }
		            });
          		}
          	}
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','Código Cliente ','RAZON SOCIAL','NOMBRE COMERCIAL','CONTACTO COMERCIAL','ID Contacto Comercial ','Cuidad de expedición ','CEDULA NIT','TEL 1','TEL 2','TEL 3','CIUDAD','DEPARTAMENTO','MAIL','DIRECCION','FECHA DE CARGUE','CLASIFICACION','CUPO CLIENTE','ESTADO CLIENTE','FECHA ACTIVACIÓN','HORA DE ACTIVACIÓN ','FECHA VENCIMIENTO','Agente-call','PASO_ID','REGISTRO_ID','Agente','Fecha','Hora','Campaña','Cupo Usado ','Cupo Nuevo','Tipo de cliente','Fecha lectura de contrato','Hora de lectura de contrato','Extensión','Pack adquirido','Cupo','Tiempo','Descuento aplicado','Fecha de activación','Hora de activación ','Fecha fin','Observación','¿Es ó ha sido cliente de Fincaraiz?','¿Lleva algún proceso ó tiene alguna propuesta con otro asesor de Fincaraiz?','¿Desde que ciudad realiza la administración de los inmuebles?','¿Cuántos inmuebles USADOS tiene en su inventario?','Cupo autorizado','¿Cuántos inmuebles NUEVOS tiene en su inventario?'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

	                ,
	                { 
	                    name:'G1135_C20667', 
	                    index: 'G1135_C20667', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_CodigoMiembro', 
	                    index: 'G1135_CodigoMiembro', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_Usuario', 
	                    index: 'G1135_Usuario', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,


	                { 
	                    name:'G1135_C17483', 
	                    index: 'G1135_C17483', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C17484', 
	                    index: 'G1135_C17484', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C17485', 
	                    index: 'G1135_C17485', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C20668', 
	                    index: 'G1135_C20668', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C20669', 
	                    index: 'G1135_C20669', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C17486', 
	                    index: 'G1135_C17486', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C17487', 
	                    index: 'G1135_C17487', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C17488', 
	                    index: 'G1135_C17488', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C17489', 
	                    index: 'G1135_C17489', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C18901', 
	                    index: 'G1135_C18901', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C17490', 
	                    index: 'G1135_C17490', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C17491', 
	                    index: 'G1135_C17491', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C17492', 
	                    index: 'G1135_C17492', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C17493', 
	                    index: 'G1135_C17493', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C17494', 
	                    index: 'G1135_C17494', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C18902', 
	                    index: 'G1135_C18902', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C18903', 
	                    index:'G1135_C18903', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=865&campo=G1135_C18903'
	                    }
	                }

	                ,
	                {  
	                    name:'G1135_C18904', 
	                    index:'G1135_C18904', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                {  
	                    name:'G1135_C20954', 
	                    index:'G1135_C20954', 
	                    width:70 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            //Timepicker
	                            var options = { 
	                                now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
	                                twentyFour: true, //Display 24 hour format, defaults to false
	                                title: 'HORA DE ACTIVACIÓN ', //The Wickedpicker's title,
	                                showSeconds: true, //Whether or not to show seconds,
	                                secondsInterval: 1, //Change interval for seconds, defaults to 1
	                                minutesInterval: 1, //Change interval for minutes, defaults to 1
	                                beforeShow: null, //A function to be called before the Wickedpicker is shown
	                                show: null, //A function to be called when the Wickedpicker is shown
	                                clearable: false, //Make the picker's input clearable (has clickable "x")
	                            }; 
	                            $(el).wickedpicker(options);
	                        }
	                    }
	                }

	                ,
	                {  
	                    name:'G1135_C18905', 
	                    index:'G1135_C18905', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1135_C22656', 
	                    index: 'G1135_C22656', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
 
	                ,
	                {  
	                    name:'G1135_C17497', 
	                    index:'G1135_C17497', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }
 
	                ,
	                {  
	                    name:'G1135_C17498', 
	                    index:'G1135_C17498', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1135_C17502', 
	                    index: 'G1135_C17502', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C17503', 
	                    index: 'G1135_C17503', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C17504', 
	                    index: 'G1135_C17504', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C17505', 
	                    index: 'G1135_C17505', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C21447', 
	                    index: 'G1135_C21447', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C21448', 
	                    index: 'G1135_C21448', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C18645', 
	                    index:'G1135_C18645', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=912&campo=G1135_C18645'
	                    }
	                }

	                ,
	                {  
	                    name:'G1135_C17507', 
	                    index:'G1135_C17507', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                {  
	                    name:'G1135_C17508', 
	                    index:'G1135_C17508', 
	                    width:70 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            //Timepicker
	                            var options = { 
	                                now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
	                                twentyFour: true, //Display 24 hour format, defaults to false
	                                title: 'Hora de lectura de contrato', //The Wickedpicker's title,
	                                showSeconds: true, //Whether or not to show seconds,
	                                secondsInterval: 1, //Change interval for seconds, defaults to 1
	                                minutesInterval: 1, //Change interval for minutes, defaults to 1
	                                beforeShow: null, //A function to be called before the Wickedpicker is shown
	                                show: null, //A function to be called when the Wickedpicker is shown
	                                clearable: false, //Make the picker's input clearable (has clickable "x")
	                            }; 
	                            $(el).wickedpicker(options);
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1135_C17509', 
	                    index: 'G1135_C17509', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C18646', 
	                    index:'G1135_C18646', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=978&campo=G1135_C18646'
	                    }
	                }

	                ,
	                { 
	                    name:'G1135_C18647', 
	                    index:'G1135_C18647', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=977&campo=G1135_C18647'
	                    }
	                }

	                ,
	                { 
	                    name:'G1135_C18648', 
	                    index:'G1135_C18648', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=979&campo=G1135_C18648'
	                    }
	                }

	                ,
	                { 
	                    name:'G1135_C18649', 
	                    index:'G1135_C18649', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=980&campo=G1135_C18649'
	                    }
	                }

	                ,
	                {  
	                    name:'G1135_C18650', 
	                    index:'G1135_C18650', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                {  
	                    name:'G1135_C20955', 
	                    index:'G1135_C20955', 
	                    width:70 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            //Timepicker
	                            var options = { 
	                                now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
	                                twentyFour: true, //Display 24 hour format, defaults to false
	                                title: 'Hora de activación ', //The Wickedpicker's title,
	                                showSeconds: true, //Whether or not to show seconds,
	                                secondsInterval: 1, //Change interval for seconds, defaults to 1
	                                minutesInterval: 1, //Change interval for minutes, defaults to 1
	                                beforeShow: null, //A function to be called before the Wickedpicker is shown
	                                show: null, //A function to be called when the Wickedpicker is shown
	                                clearable: false, //Make the picker's input clearable (has clickable "x")
	                            }; 
	                            $(el).wickedpicker(options);
	                        }
	                    }
	                }

	                ,
	                {  
	                    name:'G1135_C18651', 
	                    index:'G1135_C18651', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G1135_C17512', 
	                    index:'G1135_C17512', 
	                    width:150, 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G1135_C18906', 
	                    index:'G1135_C18906', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1135_C18906'
	                    }
	                }

	                ,
	                { 
	                    name:'G1135_C18907', 
	                    index:'G1135_C18907', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=827&campo=G1135_C18907'
	                    }
	                }

	                ,
	                { 
	                    name:'G1135_C18908', 
	                    index:'G1135_C18908', 
	                    width:300 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosCombo_Guion_G1135_C18908=si',
	                        dataInit:function(el){
	                        	$(el).select2();
	                            /*$(el).select2({ 
	                                templateResult: function(data) {
	                                    var r = data.text.split('|');
	                                    var row = '<div class="row">';
	                                    var totalRows = 12 / r.length;
	                                    for(i= 0; i < r.length; i++){
	                                        row += '<div class="col-md-'+ Math.round(totalRows) +'">' + r[i] + '</div>';
	                                    }
	                                    row += '</div>';
	                                    var $result = $(row);
	                                    return $result;
	                                },
	                                templateSelection : function(data){
	                                    var r = data.text.split('|');
	                                    return r[0];
	                                }
	                            });*/
	                            $(el).change(function(){
	                                var valores = $(el + " option:selected").attr("llenadores");
	                                var campos =  $(el + " option:selected").attr("dinammicos");
	                                var r = valores.split('|');
	                                if(r.length > 1){

	                                    var c = campos.split('|');
	                                    for(i = 1; i < r.length; i++){
	                                        $("#"+ rowid +"_"+c[i]).val(r[i]);
	                                    }
	                                }
	                            });
	                        }
	                    }
	                }
 
	                ,
	                {  
	                    name:'G1135_C18909', 
	                    index:'G1135_C18909', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G1135_C18910', 
	                    index:'G1135_C18910', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=977&campo=G1135_C18910'
	                    }
	                }
 
	                ,
	                {  
	                    name:'G1135_C18911', 
	                    index:'G1135_C18911', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    

                    $("#"+ rowid +"_G1135_C18908").change(function(){
                        var valores = $("#"+ rowid +"_G1135_C18908 option:selected").attr("llenadores");
                        var campos = $("#"+ rowid +"_G1135_C18908 option:selected").attr("dinammicos");
                        var r = valores.split('|');

                        if(r.length > 1){

                            var c = campos.split('|');
                            for(i = 1; i < r.length; i++){
                                if(!$("#"+c[i]).is("select")) {
                                // the input field is not a select
                                    $("#"+ rowid +"_"+c[i]).val(r[i]);
                                }else{
                                    var change = r[i].replace(' ', ''); 
                                    $("#"+ rowid +"_"+c[i]).val(change).change();
                                }
                                
                            }
                        }
                    });
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1135_C17483',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //SECCION  : Manipular Lista de Navegacion

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(x){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', Busqueda : x  , G1135_C20667 : $("#busq_G1135_C20667").val() , G1135_C17483 : $("#busq_G1135_C17483").val() , G1135_C17484 : $("#busq_G1135_C17484").val() , G1135_C17486 : $("#busq_G1135_C17486").val(), CodMiembro : $("#busq_G1135_CodigoMiembro").val() },
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    var strIconoBackOffice = '';
                    if(data[i].estado == '1'){
                        strIconoBackOffice = 'En gestión';
                    }else if(data[i].estado == '2'){
                        strIconoBackOffice = 'Cerrada';
                    }else if(data[i].estado == '3'){
                        strIconoBackOffice = 'Devuelta';
                    }else if(data[i].estado == '4'){
                        strIconoBackOffice = 'Sin gestión';
                    }
                    

                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"<span style='position: relative;right: 2px;float: right;font-size:10px;"+ data[i].color+ "'>"+strIconoBackOffice+"</span></p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');
            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G1135_C20667").val(item.G1135_C20667);

                        $("#G1135_CodigoMiembro").val(item.G1135_CodigoMiembro);

                        $("#G1135_Usuario").val(item.G1135_Usuario);

                        $("#G1135_C17483").val(item.G1135_C17483);

                        $("#G1135_C17484").val(item.G1135_C17484);

                        $("#G1135_C17485").val(item.G1135_C17485);

                        $("#G1135_C20668").val(item.G1135_C20668);

                        $("#G1135_C20669").val(item.G1135_C20669);

                        $("#G1135_C17486").val(item.G1135_C17486);

                        $("#G1135_C17487").val(item.G1135_C17487);

                        $("#G1135_C17488").val(item.G1135_C17488);

                        $("#G1135_C17489").val(item.G1135_C17489);

                        $("#G1135_C18901").val(item.G1135_C18901);

                        $("#G1135_C17490").val(item.G1135_C17490);

                        $("#G1135_C17491").val(item.G1135_C17491);

                        $("#G1135_C17492").val(item.G1135_C17492);

                        $("#G1135_C17493").val(item.G1135_C17493);

                        $("#G1135_C17494").val(item.G1135_C17494);

                        $("#G1135_C18902").val(item.G1135_C18902);

                        $("#G1135_C18903").val(item.G1135_C18903);
 
        	            $("#G1135_C18903").val(item.G1135_C18903).trigger("change"); 

                        $("#G1135_C18904").val(item.G1135_C18904);

                        $("#G1135_C20954").val(item.G1135_C20954);

                        $("#G1135_C18905").val(item.G1135_C18905);

                        $("#G1135_C22656").val(item.G1135_C22656);

                        $("#G1135_C17495").val(item.G1135_C17495);
 
        	            $("#G1135_C17495").val(item.G1135_C17495).trigger("change"); 

                        $("#G1135_C17496").val(item.G1135_C17496);
 
        	            $("#G1135_C17496").val(item.G1135_C17496).trigger("change"); 

                        $("#G1135_C17497").val(item.G1135_C17497);

                        $("#G1135_C17498").val(item.G1135_C17498);

                        $("#G1135_C17499").val(item.G1135_C17499);

                        $("#G1135_C17500").val(item.G1135_C17500);

                        $("#G1135_C17501").val(item.G1135_C17501);

                        $("#G1135_C17502").val(item.G1135_C17502);

                        $("#G1135_C17503").val(item.G1135_C17503);

                        $("#G1135_C17504").val(item.G1135_C17504);

                        $("#G1135_C17505").val(item.G1135_C17505);

                        $("#G1135_C21447").val(item.G1135_C21447);

                        $("#G1135_C21448").val(item.G1135_C21448);

                        $("#G1135_C18645").val(item.G1135_C18645);
 
        	            $("#G1135_C18645").val(item.G1135_C18645).trigger("change"); 
    
                        if(item.G1135_C17506 == 1){
                           $("#G1135_C17506").attr('checked', true);
                        } 

                        $("#G1135_C17507").val(item.G1135_C17507);

                        $("#G1135_C17508").val(item.G1135_C17508);

                        $("#G1135_C17509").val(item.G1135_C17509);

                        $("#G1135_C18646").val(item.G1135_C18646);
 
        	            $("#G1135_C18646").val(item.G1135_C18646).trigger("change"); 

                        $("#G1135_C18647").val(item.G1135_C18647);
 
        	            $("#G1135_C18647").val(item.G1135_C18647).trigger("change"); 

                        $("#G1135_C18648").val(item.G1135_C18648);
 
        	            $("#G1135_C18648").val(item.G1135_C18648).trigger("change"); 

                        $("#G1135_C18649").val(item.G1135_C18649);
 
        	            $("#G1135_C18649").val(item.G1135_C18649).trigger("change"); 

                        $("#G1135_C18650").val(item.G1135_C18650);

                        $("#G1135_C20955").val(item.G1135_C20955);

                        $("#G1135_C18651").val(item.G1135_C18651);

                        $("#G1135_C17512").val(item.G1135_C17512);

                        $("#G1135_C18906").val(item.G1135_C18906);
 
        	            $("#G1135_C18906").val(item.G1135_C18906).trigger("change"); 

                        $("#G1135_C18907").val(item.G1135_C18907);
 
        	            $("#G1135_C18907").val(item.G1135_C18907).trigger("change"); 

                        $("#G1135_C18908").val(item.G1135_C18908);
 
        	            $("#G1135_C18908").val(item.G1135_C18908).trigger("change"); 

                        $("#G1135_C18909").val(item.G1135_C18909);

                        $("#G1135_C18910").val(item.G1135_C18910);
 
        	            $("#G1135_C18910").val(item.G1135_C18910).trigger("change"); 

                        $("#G1135_C18911").val(item.G1135_C18911);
        				$("#h3mio").html(item.principal);
        				
                    });

               
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();

            $.ajax({
	        	url   : '<?php echo $url_crud; ?>',
	        	type  : 'post',
	        	data  : { DameHistorico : 'si', user_codigo_mien : idTotal, campana_crm : '<?php if(isset($_GET['campana_crm'])) { echo $_GET['campana_crm']; } else { echo "567"; } ?>' },
	        	dataType : 'html',
	        	success : function(data){
	        		$("#tablaGestiones").html('');
	        		$("#tablaGestiones").html(data);
	        		
	        	}

	    	});
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
	$(document).ready(function() {
		<?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                		while($objDatos = $resultDatosSql->fetch_object()){ ?>
                        	document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
             		<?php  
             			}	
                    }
                    
                } */  
            }
        ?>
        $('#G1135_CodigoMiembro').val('<?php echo $_GET['user']?>');		
    	<?php if(isset($_GET['user'])){ ?>
        	
    		vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user']; ?>');
    		        <?php } ?>

    		        $('#G1122_C17138').val('<?php echo $_GET['user']?>');		
    	<?php if(isset($_GET['user'])){ ?>
        	
    		vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user']; ?>');
    		        <?php } ?>
    	<?php if(isset($_GET['user'])){ ?>
        	
        	idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
		
	});
</script>
