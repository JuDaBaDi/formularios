<?php 
    /*
        Document   : index
        Created on : 2020-05-23 11:38:45
        Author     : Nicolas y su poderoso generador, La gloria sea Para DIOS 
        Url 	   : id = MjU3MQ==  
    */
    $url_crud =  "formularios/G2571/G2571_CRUD_web.php";
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulario de contacto</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
    
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link rel="shortcut icon" href="assets/img/logo_dyalogo_mail.png"/>
        <style>
            
            .hed{
           
                font-family: 'Sansita One', cursive; 
                color:white;
            }

            .hed2{
                text-align:center;
                font-family: 'Sansita One', cursive; 
                font-size:25px; 
                color:#019CDE;
                margin-top: -9px;
            }
            .font_2 {
                font: normal normal normal 17px/1.4em Spinnaker,sans-serif;
                text-align:center;
            }
            
            .redonder {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                -webkit-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
            }

            [class^='select2'] {
                border-radius: 0px !important;
            }
        </style>
    </head>
    <?php  
        echo '<body class="hold-transition" >';
    ?>
    
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" >
                <div class="login-box">
                    <div class="login-logo hed">
                        <img src="assets/img/logo_dyalogo_mail.png"  alt="Dyalogo">
                    </div><!-- /.login-logo -->
                    <div class="login-box-body">
                        <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
                        <form action="formularios/G2571/G2571_CRUD_web.php" method="post" id="formLogin">
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50152" id="LblG2571_C50152">NOMBRE</label>
								<input type="text" class="form-control input-sm" id="G2571_C50152" value="" disabled name="G2571_C50152"  placeholder="NOMBRE">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50153" id="LblG2571_C50153">CEDULA</label>
								<input type="text" class="form-control input-sm" id="G2571_C50153" value="" disabled name="G2571_C50153"  placeholder="CEDULA">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50154" id="LblG2571_C50154">CIUDAD RESIDENCIAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50154" value="" disabled name="G2571_C50154"  placeholder="CIUDAD RESIDENCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50155" id="LblG2571_C50155">CIUDAD LABORAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50155" value="" disabled name="G2571_C50155"  placeholder="CIUDAD LABORAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50156" id="LblG2571_C50156">CIUDAD COMERCIAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50156" value="" disabled name="G2571_C50156"  placeholder="CIUDAD COMERCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50157" id="LblG2571_C50157">DR. RESIDENCIAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50157" value="" disabled name="G2571_C50157"  placeholder="DR. RESIDENCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50158" id="LblG2571_C50158">DR: LABORAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50158" value="" disabled name="G2571_C50158"  placeholder="DR: LABORAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50159" id="LblG2571_C50159">DR: COMERCIAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50159" value="" disabled name="G2571_C50159"  placeholder="DR: COMERCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50160" id="LblG2571_C50160">BARRIO RESIDENCIAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50160" value="" disabled name="G2571_C50160"  placeholder="BARRIO RESIDENCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50161" id="LblG2571_C50161">BARRIO LABORAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50161" value="" disabled name="G2571_C50161"  placeholder="BARRIO LABORAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50162" id="LblG2571_C50162">BARRIO COMERCIAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50162" value="" disabled name="G2571_C50162"  placeholder="BARRIO COMERCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50163" id="LblG2571_C50163">TEL. RESIDENCIAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50163" value="" disabled name="G2571_C50163"  placeholder="TEL. RESIDENCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50164" id="LblG2571_C50164">TEL. LABORAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50164" value="" disabled name="G2571_C50164"  placeholder="TEL. LABORAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50165" id="LblG2571_C50165">TEL. COMERCIAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50165" value="" disabled name="G2571_C50165"  placeholder="TEL. COMERCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50166" id="LblG2571_C50166">CELULAR</label>
								<input type="text" class="form-control input-sm" id="G2571_C50166" value="" disabled name="G2571_C50166"  placeholder="CELULAR">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50635" id="LblG2571_C50635">A.NOMBRE</label>
								<input type="text" class="form-control input-sm" id="G2571_C50635" value=""  name="G2571_C50635"  placeholder="A.NOMBRE">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50636" id="LblG2571_C50636">A.CEDULA</label>
								<input type="text" class="form-control input-sm" id="G2571_C50636" value=""  name="G2571_C50636"  placeholder="A.CEDULA">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50637" id="LblG2571_C50637">A.CIUDAD RESIDENCIAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50637" value=""  name="G2571_C50637"  placeholder="A.CIUDAD RESIDENCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50638" id="LblG2571_C50638">A.CIUDAD LABORAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50638" value=""  name="G2571_C50638"  placeholder="A.CIUDAD LABORAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50639" id="LblG2571_C50639">A.CIUDAD COMERCIAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50639" value=""  name="G2571_C50639"  placeholder="A.CIUDAD COMERCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50640" id="LblG2571_C50640">A.DR. RESIDENCIAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50640" value=""  name="G2571_C50640"  placeholder="A.DR. RESIDENCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50641" id="LblG2571_C50641">A.DR: LABORAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50641" value=""  name="G2571_C50641"  placeholder="A.DR: LABORAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50642" id="LblG2571_C50642">A.DR: COMERCIAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50642" value=""  name="G2571_C50642"  placeholder="A.DR: COMERCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50643" id="LblG2571_C50643">A.BARRIO RESIDENCIAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50643" value=""  name="G2571_C50643"  placeholder="A.BARRIO RESIDENCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50644" id="LblG2571_C50644">A.BARRIO LABORAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50644" value=""  name="G2571_C50644"  placeholder="A.BARRIO LABORAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50645" id="LblG2571_C50645">A.BARRIO COMERCIAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50645" value=""  name="G2571_C50645"  placeholder="A.BARRIO COMERCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50646" id="LblG2571_C50646">A.TEL. RESIDENCIAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50646" value=""  name="G2571_C50646"  placeholder="A.TEL. RESIDENCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50647" id="LblG2571_C50647">A.TEL. LABORAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50647" value=""  name="G2571_C50647"  placeholder="A.TEL. LABORAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50648" id="LblG2571_C50648">A.TEL. COMERCIAL</label>
								<input type="text" class="form-control input-sm" id="G2571_C50648" value=""  name="G2571_C50648"  placeholder="A.TEL. COMERCIAL">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50649" id="LblG2571_C50649">A.CELULAR</label>
								<input type="text" class="form-control input-sm" id="G2571_C50649" value=""  name="G2571_C50649"  placeholder="A.CELULAR">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2571_C50650" id="LblG2571_C50650">ESTADO ACTUAL</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2571_C50650" id="G2571_C50650">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 2575 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50651" id="LblG2571_C50651">LUGAR DE TRABAJO</label>
								<input type="text" class="form-control input-sm" id="G2571_C50651" value=""  name="G2571_C50651"  placeholder="LUGAR DE TRABAJO">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50652" id="LblG2571_C50652">CARGO O ACTIVIDAD ECONÓMICA</label>
								<input type="text" class="form-control input-sm" id="G2571_C50652" value=""  name="G2571_C50652"  placeholder="CARGO O ACTIVIDAD ECONÓMICA">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50653" id="LblG2571_C50653">NOMBRE DE LA EMPRESA</label>
								<input type="text" class="form-control input-sm" id="G2571_C50653" value=""  name="G2571_C50653"  placeholder="NOMBRE DE LA EMPRESA">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50654" id="LblG2571_C50654">DIRECCION ACTUALIZADA</label>
								<input type="text" class="form-control input-sm" id="G2571_C50654" value=""  name="G2571_C50654"  placeholder="DIRECCION ACTUALIZADA">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2571_C50655" id="LblG2571_C50655">TELEFONO WHATSAPP</label>
								<input type="text" class="form-control input-sm" id="G2571_C50655" value=""  name="G2571_C50655"  placeholder="TELEFONO WHATSAPP">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="id" id="hidId" value='<?php if(isset($_GET['u'])){ echo $_GET['u']; }else{ echo "0"; } ?>'>
                            <input type="hidden" name="oper" id="oper" value='add'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formpadre" id="formpadre" value='<?php if(isset($_GET['formularioPadre'])){ echo $_GET['formularioPadre']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formhijo" id="formhijo" value='<?php if(isset($_GET['formulario'])){ echo $_GET['formulario']; }else{ echo "0"; }?>' >
                            <input type= "hidden" name="campana" id="campana" value="<?php if(isset($_GET['camp'])){ echo base64_decode($_GET['camp']); }else{ echo "0"; }?>">
                            <div class="row">
                                <div class="col-xs-2">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-xs-8">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar datos</button>
                                </div><!-- /.col -->
                            </div>
                        </form>
                    </div><!-- /.login-box-body -->
                </div><!-- /.login-box -->
            </div>
        </div>


        <!-- jQuery 2.2.3 -->
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>  
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/numeric.js"></script>

        <script type="text/javascript" src="formularios/G2571/G2571_eventos.js"></script>
        <script type="text/javascript">
            $.validator.setDefaults({
                submitHandler: function() { 
                     $("#formLogin").submit();
                }
            });

            $(function(){

                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };

                //str_Select2 estos son los guiones
                


                //datepickers
                

            $("#G2571_C50143").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

                //Timepickers
                


            //Timepicker
            $("#G2571_C50144").timepicker({
                'timeFormat': 'H:i:s',
                'minTime': '08:00:00',
                'maxTime': '17:00:00',
                'setTime': '08:00:00',
                'step'  : '5',
                'showDuration': true
            });

                //Validaciones numeros Enteros
                


                //Validaciones numeros Decimales
               


               //Si tiene dependencias
               


    //function para ESTADO ACTUAL 

    $("#G2571_C50650").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
                

               <?php
                    if(isset($_GET['result'])){
                        if($_GET['result'] ==  1){
                ?>
                        swal({
                            title: "Exito!",
                            text: "Recibimos su solicitud, pronto estaremos en contacto",
                            type: "success",
                            confirmButtonText: "Ok"
                        },function(){

                        });
                        
                        
                <?php   }else{ ?>
                            swal({
                                title: "Error!",
                                text: 'Ocurrio un error, intenta mas tarde',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }
                    }
                ?>
            });
        </script>
        <Script type="text/javascript">
            $(document).ready(function() {
                <?php
                $campana = base64_decode($_GET['camp']);
                $Guion = 0;//id de la campaña
                $tabla = 0;// $_GET['u'];//ide del usuario
                $Lsql = "SELECT CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConsInte__GUION__Pob_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$campana;

                $result = $mysqli->query($Lsql);
                while($obj = $result->fetch_object()){
                    $Guion = $obj->CAMPAN_ConsInte__GUION__Gui_b;
                    $tabla = $obj->CAMPAN_ConsInte__GUION__Pob_b;
                } 
                //SELECT de la camic
                $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b, CAMINC_ConsInte__CAMPO_Gui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$campana;
                
                $resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    
                    //Pregfuntar por el tipo de dato
                    $Lsql = "SELECT PREGUN_Tipo______b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__b = ".$key->CAMINC_ConsInte__CAMPO_Gui_b;
                    $res = $mysqli->query($Lsql);
                    $datos = $res->fetch_array();


                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['u'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ 
                            if(!is_null($objDatos->campo) && $objDatos->campo != ''){

                                if($datos['PREGUN_Tipo______b'] != '8'){
                            ?>
                                    document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                            <?php  
                                }else{
                                    if($objDatos->campo == '1'){
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , true);";
                                    }else{
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , false);";
                                    }
                                    
                                } 
                            }
                        }
                    }
                    
                }
                ?>
            });
        </script>
        
