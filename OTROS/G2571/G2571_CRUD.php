<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2571_ConsInte__b, G2571_FechaInsercion , G2571_Usuario ,  G2571_CodigoMiembro  , G2571_PoblacionOrigen , G2571_EstadoDiligenciamiento ,  G2571_IdLlamada , G2571_C50152 as principal ,G2571_C50152,G2571_C50153,G2571_C50154,G2571_C50155,G2571_C50156,G2571_C50157,G2571_C50158,G2571_C50159,G2571_C50160,G2571_C50161,G2571_C50162,G2571_C50163,G2571_C50164,G2571_C50165,G2571_C50166,G2571_C50141,G2571_C50142,G2571_C50143,G2571_C50144,G2571_C50145,G2571_C50146,G2571_C50147,G2571_C50148,G2571_C50149,G2571_C50635,G2571_C50636,G2571_C50637,G2571_C50638,G2571_C50639,G2571_C50640,G2571_C50641,G2571_C50642,G2571_C50643,G2571_C50644,G2571_C50645,G2571_C50646,G2571_C50647,G2571_C50648,G2571_C50649,G2571_C50650,G2571_C50651,G2571_C50652,G2571_C50653,G2571_C50654,G2571_C50655 FROM '.$BaseDatos.'.G2571 WHERE G2571_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2571_C50152'] = $key->G2571_C50152;

                $datos[$i]['G2571_C50153'] = $key->G2571_C50153;

                $datos[$i]['G2571_C50154'] = $key->G2571_C50154;

                $datos[$i]['G2571_C50155'] = $key->G2571_C50155;

                $datos[$i]['G2571_C50156'] = $key->G2571_C50156;

                $datos[$i]['G2571_C50157'] = $key->G2571_C50157;

                $datos[$i]['G2571_C50158'] = $key->G2571_C50158;

                $datos[$i]['G2571_C50159'] = $key->G2571_C50159;

                $datos[$i]['G2571_C50160'] = $key->G2571_C50160;

                $datos[$i]['G2571_C50161'] = $key->G2571_C50161;

                $datos[$i]['G2571_C50162'] = $key->G2571_C50162;

                $datos[$i]['G2571_C50163'] = $key->G2571_C50163;

                $datos[$i]['G2571_C50164'] = $key->G2571_C50164;

                $datos[$i]['G2571_C50165'] = $key->G2571_C50165;

                $datos[$i]['G2571_C50166'] = $key->G2571_C50166;

                $datos[$i]['G2571_C50141'] = $key->G2571_C50141;

                $datos[$i]['G2571_C50142'] = $key->G2571_C50142;

                $datos[$i]['G2571_C50143'] = explode(' ', $key->G2571_C50143)[0];
  
                $hora = '';
                if(!is_null($key->G2571_C50144)){
                    $hora = explode(' ', $key->G2571_C50144)[1];
                }

                $datos[$i]['G2571_C50144'] = $hora;

                $datos[$i]['G2571_C50145'] = $key->G2571_C50145;

                $datos[$i]['G2571_C50146'] = $key->G2571_C50146;

                $datos[$i]['G2571_C50147'] = $key->G2571_C50147;

                $datos[$i]['G2571_C50148'] = $key->G2571_C50148;

                $datos[$i]['G2571_C50149'] = $key->G2571_C50149;

                $datos[$i]['G2571_C50635'] = $key->G2571_C50635;

                $datos[$i]['G2571_C50636'] = $key->G2571_C50636;

                $datos[$i]['G2571_C50637'] = $key->G2571_C50637;

                $datos[$i]['G2571_C50638'] = $key->G2571_C50638;

                $datos[$i]['G2571_C50639'] = $key->G2571_C50639;

                $datos[$i]['G2571_C50640'] = $key->G2571_C50640;

                $datos[$i]['G2571_C50641'] = $key->G2571_C50641;

                $datos[$i]['G2571_C50642'] = $key->G2571_C50642;

                $datos[$i]['G2571_C50643'] = $key->G2571_C50643;

                $datos[$i]['G2571_C50644'] = $key->G2571_C50644;

                $datos[$i]['G2571_C50645'] = $key->G2571_C50645;

                $datos[$i]['G2571_C50646'] = $key->G2571_C50646;

                $datos[$i]['G2571_C50647'] = $key->G2571_C50647;

                $datos[$i]['G2571_C50648'] = $key->G2571_C50648;

                $datos[$i]['G2571_C50649'] = $key->G2571_C50649;

                $datos[$i]['G2571_C50650'] = $key->G2571_C50650;

                $datos[$i]['G2571_C50651'] = $key->G2571_C50651;

                $datos[$i]['G2571_C50652'] = $key->G2571_C50652;

                $datos[$i]['G2571_C50653'] = $key->G2571_C50653;

                $datos[$i]['G2571_C50654'] = $key->G2571_C50654;

                $datos[$i]['G2571_C50655'] = $key->G2571_C50655;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2571";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2571_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2571_ConsInte__b as id,  G2571_C50152 as camp1 , G2571_C50153 as camp2 
                     FROM ".$BaseDatos.".G2571  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2571_ConsInte__b as id,  G2571_C50152 as camp1 , G2571_C50153 as camp2  
                    FROM ".$BaseDatos.".G2571  JOIN ".$BaseDatos.".G2571_M".$_POST['muestra']." ON G2571_ConsInte__b = G2571_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2571_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2571_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2571_C50152 LIKE '%".$B."%' OR G2571_C50153 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2571_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2571");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2571_ConsInte__b, G2571_FechaInsercion , G2571_Usuario ,  G2571_CodigoMiembro  , G2571_PoblacionOrigen , G2571_EstadoDiligenciamiento ,  G2571_IdLlamada , G2571_C50152 as principal ,G2571_C50152,G2571_C50153,G2571_C50154,G2571_C50155,G2571_C50156,G2571_C50157,G2571_C50158,G2571_C50159,G2571_C50160,G2571_C50161,G2571_C50162,G2571_C50163,G2571_C50164,G2571_C50165,G2571_C50166, a.LISOPC_Nombre____b as G2571_C50141, b.LISOPC_Nombre____b as G2571_C50142,G2571_C50143,G2571_C50144,G2571_C50145,G2571_C50146,G2571_C50147,G2571_C50148,G2571_C50149,G2571_C50635,G2571_C50636,G2571_C50637,G2571_C50638,G2571_C50639,G2571_C50640,G2571_C50641,G2571_C50642,G2571_C50643,G2571_C50644,G2571_C50645,G2571_C50646,G2571_C50647,G2571_C50648,G2571_C50649, c.LISOPC_Nombre____b as G2571_C50650,G2571_C50651,G2571_C50652,G2571_C50653,G2571_C50654,G2571_C50655 FROM '.$BaseDatos.'.G2571 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2571_C50141 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2571_C50142 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2571_C50650';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2571_C50144)){
                    $hora_a = explode(' ', $fila->G2571_C50144)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2571_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2571_ConsInte__b , ($fila->G2571_C50152) , ($fila->G2571_C50153) , ($fila->G2571_C50154) , ($fila->G2571_C50155) , ($fila->G2571_C50156) , ($fila->G2571_C50157) , ($fila->G2571_C50158) , ($fila->G2571_C50159) , ($fila->G2571_C50160) , ($fila->G2571_C50161) , ($fila->G2571_C50162) , ($fila->G2571_C50163) , ($fila->G2571_C50164) , ($fila->G2571_C50165) , ($fila->G2571_C50166) , ($fila->G2571_C50141) , ($fila->G2571_C50142) , explode(' ', $fila->G2571_C50143)[0] , $hora_a , ($fila->G2571_C50145) , ($fila->G2571_C50146) , ($fila->G2571_C50147) , ($fila->G2571_C50148) , ($fila->G2571_C50149) , ($fila->G2571_C50635) , ($fila->G2571_C50636) , ($fila->G2571_C50637) , ($fila->G2571_C50638) , ($fila->G2571_C50639) , ($fila->G2571_C50640) , ($fila->G2571_C50641) , ($fila->G2571_C50642) , ($fila->G2571_C50643) , ($fila->G2571_C50644) , ($fila->G2571_C50645) , ($fila->G2571_C50646) , ($fila->G2571_C50647) , ($fila->G2571_C50648) , ($fila->G2571_C50649) , ($fila->G2571_C50650) , ($fila->G2571_C50651) , ($fila->G2571_C50652) , ($fila->G2571_C50653) , ($fila->G2571_C50654) , ($fila->G2571_C50655) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2571 WHERE G2571_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2571";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2571_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2571_ConsInte__b as id,  G2571_C50152 as camp1 , G2571_C50153 as camp2  FROM '.$BaseDatos.'.G2571 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2571_ConsInte__b as id,  G2571_C50152 as camp1 , G2571_C50153 as camp2  
                    FROM ".$BaseDatos.".G2571  JOIN ".$BaseDatos.".G2571_M".$_POST['muestra']." ON G2571_ConsInte__b = G2571_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2571_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2571_C50152 LIKE "%'.$B.'%" OR G2571_C50153 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2571_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2571 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2571(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2571_C50152"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50152 = '".$_POST["G2571_C50152"]."'";
                $LsqlI .= $separador."G2571_C50152";
                $LsqlV .= $separador."'".$_POST["G2571_C50152"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50153"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50153 = '".$_POST["G2571_C50153"]."'";
                $LsqlI .= $separador."G2571_C50153";
                $LsqlV .= $separador."'".$_POST["G2571_C50153"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50154"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50154 = '".$_POST["G2571_C50154"]."'";
                $LsqlI .= $separador."G2571_C50154";
                $LsqlV .= $separador."'".$_POST["G2571_C50154"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50155"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50155 = '".$_POST["G2571_C50155"]."'";
                $LsqlI .= $separador."G2571_C50155";
                $LsqlV .= $separador."'".$_POST["G2571_C50155"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50156"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50156 = '".$_POST["G2571_C50156"]."'";
                $LsqlI .= $separador."G2571_C50156";
                $LsqlV .= $separador."'".$_POST["G2571_C50156"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50157"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50157 = '".$_POST["G2571_C50157"]."'";
                $LsqlI .= $separador."G2571_C50157";
                $LsqlV .= $separador."'".$_POST["G2571_C50157"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50158"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50158 = '".$_POST["G2571_C50158"]."'";
                $LsqlI .= $separador."G2571_C50158";
                $LsqlV .= $separador."'".$_POST["G2571_C50158"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50159"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50159 = '".$_POST["G2571_C50159"]."'";
                $LsqlI .= $separador."G2571_C50159";
                $LsqlV .= $separador."'".$_POST["G2571_C50159"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50160"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50160 = '".$_POST["G2571_C50160"]."'";
                $LsqlI .= $separador."G2571_C50160";
                $LsqlV .= $separador."'".$_POST["G2571_C50160"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50161"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50161 = '".$_POST["G2571_C50161"]."'";
                $LsqlI .= $separador."G2571_C50161";
                $LsqlV .= $separador."'".$_POST["G2571_C50161"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50162"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50162 = '".$_POST["G2571_C50162"]."'";
                $LsqlI .= $separador."G2571_C50162";
                $LsqlV .= $separador."'".$_POST["G2571_C50162"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50163"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50163 = '".$_POST["G2571_C50163"]."'";
                $LsqlI .= $separador."G2571_C50163";
                $LsqlV .= $separador."'".$_POST["G2571_C50163"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50164"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50164 = '".$_POST["G2571_C50164"]."'";
                $LsqlI .= $separador."G2571_C50164";
                $LsqlV .= $separador."'".$_POST["G2571_C50164"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50165"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50165 = '".$_POST["G2571_C50165"]."'";
                $LsqlI .= $separador."G2571_C50165";
                $LsqlV .= $separador."'".$_POST["G2571_C50165"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50166"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50166 = '".$_POST["G2571_C50166"]."'";
                $LsqlI .= $separador."G2571_C50166";
                $LsqlV .= $separador."'".$_POST["G2571_C50166"]."'";
                $validar = 1;
            }
             
 
            $G2571_C50141 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2571_C50141 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2571_C50141 = ".$G2571_C50141;
                    $LsqlI .= $separador." G2571_C50141";
                    $LsqlV .= $separador.$G2571_C50141;
                    $validar = 1;

                    
                }
            }
 
            $G2571_C50142 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2571_C50142 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2571_C50142 = ".$G2571_C50142;
                    $LsqlI .= $separador." G2571_C50142";
                    $LsqlV .= $separador.$G2571_C50142;
                    $validar = 1;
                }
            }
 
            $G2571_C50143 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2571_C50143 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2571_C50143 = ".$G2571_C50143;
                    $LsqlI .= $separador." G2571_C50143";
                    $LsqlV .= $separador.$G2571_C50143;
                    $validar = 1;
                }
            }
 
            $G2571_C50144 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2571_C50144 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2571_C50144 = ".$G2571_C50144;
                    $LsqlI .= $separador." G2571_C50144";
                    $LsqlV .= $separador.$G2571_C50144;
                    $validar = 1;
                }
            }
 
            $G2571_C50145 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2571_C50145 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2571_C50145 = ".$G2571_C50145;
                    $LsqlI .= $separador." G2571_C50145";
                    $LsqlV .= $separador.$G2571_C50145;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2571_C50146"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50146 = '".$_POST["G2571_C50146"]."'";
                $LsqlI .= $separador."G2571_C50146";
                $LsqlV .= $separador."'".$_POST["G2571_C50146"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50147"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50147 = '".$_POST["G2571_C50147"]."'";
                $LsqlI .= $separador."G2571_C50147";
                $LsqlV .= $separador."'".$_POST["G2571_C50147"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50148"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50148 = '".$_POST["G2571_C50148"]."'";
                $LsqlI .= $separador."G2571_C50148";
                $LsqlV .= $separador."'".$_POST["G2571_C50148"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50149"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50149 = '".$_POST["G2571_C50149"]."'";
                $LsqlI .= $separador."G2571_C50149";
                $LsqlV .= $separador."'".$_POST["G2571_C50149"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50635"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50635 = '".$_POST["G2571_C50635"]."'";
                $LsqlI .= $separador."G2571_C50635";
                $LsqlV .= $separador."'".$_POST["G2571_C50635"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50636"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50636 = '".$_POST["G2571_C50636"]."'";
                $LsqlI .= $separador."G2571_C50636";
                $LsqlV .= $separador."'".$_POST["G2571_C50636"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50637"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50637 = '".$_POST["G2571_C50637"]."'";
                $LsqlI .= $separador."G2571_C50637";
                $LsqlV .= $separador."'".$_POST["G2571_C50637"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50638"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50638 = '".$_POST["G2571_C50638"]."'";
                $LsqlI .= $separador."G2571_C50638";
                $LsqlV .= $separador."'".$_POST["G2571_C50638"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50639"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50639 = '".$_POST["G2571_C50639"]."'";
                $LsqlI .= $separador."G2571_C50639";
                $LsqlV .= $separador."'".$_POST["G2571_C50639"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50640"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50640 = '".$_POST["G2571_C50640"]."'";
                $LsqlI .= $separador."G2571_C50640";
                $LsqlV .= $separador."'".$_POST["G2571_C50640"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50641"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50641 = '".$_POST["G2571_C50641"]."'";
                $LsqlI .= $separador."G2571_C50641";
                $LsqlV .= $separador."'".$_POST["G2571_C50641"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50642"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50642 = '".$_POST["G2571_C50642"]."'";
                $LsqlI .= $separador."G2571_C50642";
                $LsqlV .= $separador."'".$_POST["G2571_C50642"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50643"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50643 = '".$_POST["G2571_C50643"]."'";
                $LsqlI .= $separador."G2571_C50643";
                $LsqlV .= $separador."'".$_POST["G2571_C50643"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50644"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50644 = '".$_POST["G2571_C50644"]."'";
                $LsqlI .= $separador."G2571_C50644";
                $LsqlV .= $separador."'".$_POST["G2571_C50644"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50645"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50645 = '".$_POST["G2571_C50645"]."'";
                $LsqlI .= $separador."G2571_C50645";
                $LsqlV .= $separador."'".$_POST["G2571_C50645"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50646"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50646 = '".$_POST["G2571_C50646"]."'";
                $LsqlI .= $separador."G2571_C50646";
                $LsqlV .= $separador."'".$_POST["G2571_C50646"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50647"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50647 = '".$_POST["G2571_C50647"]."'";
                $LsqlI .= $separador."G2571_C50647";
                $LsqlV .= $separador."'".$_POST["G2571_C50647"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50648"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50648 = '".$_POST["G2571_C50648"]."'";
                $LsqlI .= $separador."G2571_C50648";
                $LsqlV .= $separador."'".$_POST["G2571_C50648"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50649"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50649 = '".$_POST["G2571_C50649"]."'";
                $LsqlI .= $separador."G2571_C50649";
                $LsqlV .= $separador."'".$_POST["G2571_C50649"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50650"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50650 = '".$_POST["G2571_C50650"]."'";
                $LsqlI .= $separador."G2571_C50650";
                $LsqlV .= $separador."'".$_POST["G2571_C50650"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50651"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50651 = '".$_POST["G2571_C50651"]."'";
                $LsqlI .= $separador."G2571_C50651";
                $LsqlV .= $separador."'".$_POST["G2571_C50651"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50652"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50652 = '".$_POST["G2571_C50652"]."'";
                $LsqlI .= $separador."G2571_C50652";
                $LsqlV .= $separador."'".$_POST["G2571_C50652"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50653"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50653 = '".$_POST["G2571_C50653"]."'";
                $LsqlI .= $separador."G2571_C50653";
                $LsqlV .= $separador."'".$_POST["G2571_C50653"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50654"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50654 = '".$_POST["G2571_C50654"]."'";
                $LsqlI .= $separador."G2571_C50654";
                $LsqlV .= $separador."'".$_POST["G2571_C50654"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2571_C50655"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_C50655 = '".$_POST["G2571_C50655"]."'";
                $LsqlI .= $separador."G2571_C50655";
                $LsqlV .= $separador."'".$_POST["G2571_C50655"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2571_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2571_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2571_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2571_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2571_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2571_Usuario , G2571_FechaInsercion, G2571_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2571_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2571 WHERE G2571_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G2569_ConsInte__b, G2569_C50112, G2569_C50111, G2569_C50113, G2569_C50114, G2569_C50115, G2569_C50116, G2569_C50117, G2569_C50118, G2569_C50119, G2569_C50120, G2569_C50121 FROM ".$BaseDatos.".G2569  ";

        $SQL .= " WHERE G2569_C50111 = '".$numero."'"; 

        $SQL .= " ORDER BY G2569_C50112";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2569_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2569_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G2569_C50112)."</cell>";

                echo "<cell>". ($fila->G2569_C50111)."</cell>";

                echo "<cell>". ($fila->G2569_C50113)."</cell>";

                echo "<cell>". ($fila->G2569_C50114)."</cell>";

                echo "<cell>". ($fila->G2569_C50115)."</cell>";

                if($fila->G2569_C50116 != ''){
                    echo "<cell>". explode(' ', $fila->G2569_C50116)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G2569_C50117)."</cell>";

                echo "<cell>". ($fila->G2569_C50118)."</cell>";

                echo "<cell>". ($fila->G2569_C50119)."</cell>";

                echo "<cell>". ($fila->G2569_C50120)."</cell>";

                echo "<cell>". ($fila->G2569_C50121)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2569 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2569(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G2569_C50112"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2569_C50112 = '".$_POST["G2569_C50112"]."'";
                    $LsqlI .= $separador."G2569_C50112";
                    $LsqlV .= $separador."'".$_POST["G2569_C50112"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2569_C50113"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2569_C50113 = '".$_POST["G2569_C50113"]."'";
                    $LsqlI .= $separador."G2569_C50113";
                    $LsqlV .= $separador."'".$_POST["G2569_C50113"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2569_C50114"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2569_C50114 = '".$_POST["G2569_C50114"]."'";
                    $LsqlI .= $separador."G2569_C50114";
                    $LsqlV .= $separador."'".$_POST["G2569_C50114"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2569_C50115"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2569_C50115 = '".$_POST["G2569_C50115"]."'";
                    $LsqlI .= $separador."G2569_C50115";
                    $LsqlV .= $separador."'".$_POST["G2569_C50115"]."'";
                    $validar = 1;
                }

                                                                               

                $G2569_C50116 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2569_C50116"])){    
                    if($_POST["G2569_C50116"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2569_C50116 = "'".str_replace(' ', '',$_POST["G2569_C50116"])." 00:00:00'";
                        $LsqlU .= $separador." G2569_C50116 = ".$G2569_C50116;
                        $LsqlI .= $separador." G2569_C50116";
                        $LsqlV .= $separador.$G2569_C50116;
                        $validar = 1;
                    }
                }
 

                if(isset($_POST["G2569_C50117"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2569_C50117 = '".$_POST["G2569_C50117"]."'";
                    $LsqlI .= $separador."G2569_C50117";
                    $LsqlV .= $separador."'".$_POST["G2569_C50117"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2569_C50118"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2569_C50118 = '".$_POST["G2569_C50118"]."'";
                    $LsqlI .= $separador."G2569_C50118";
                    $LsqlV .= $separador."'".$_POST["G2569_C50118"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2569_C50119"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2569_C50119 = '".$_POST["G2569_C50119"]."'";
                    $LsqlI .= $separador."G2569_C50119";
                    $LsqlV .= $separador."'".$_POST["G2569_C50119"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2569_C50120"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2569_C50120 = '".$_POST["G2569_C50120"]."'";
                    $LsqlI .= $separador."G2569_C50120";
                    $LsqlV .= $separador."'".$_POST["G2569_C50120"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2569_C50121"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2569_C50121 = '".$_POST["G2569_C50121"]."'";
                    $LsqlI .= $separador."G2569_C50121";
                    $LsqlV .= $separador."'".$_POST["G2569_C50121"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2569_C50111 = $numero;
                    $LsqlU .= ", G2569_C50111 = ".$G2569_C50111."";
                    $LsqlI .= ", G2569_C50111";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2569_Usuario ,  G2569_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2569_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2569 WHERE  G2569_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
