<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1904_ConsInte__b, G1904_FechaInsercion , G1904_Usuario ,  G1904_CodigoMiembro  , G1904_PoblacionOrigen , G1904_EstadoDiligenciamiento ,  G1904_IdLlamada , G1904_C34933 as principal ,G1904_C34933,G1904_C34930,G1904_C34931,G1904_C34932,G1904_C34934,G1904_C34935,G1904_C34936,G1904_C34937,G1904_C34938,G1904_C34939,G1904_C34940,G1904_C34941,G1904_C35121,G1904_C34943,G1904_C34944,G1904_C34990,G1904_C35097,G1904_C35098,G1904_C34945,G1904_C34946,G1904_C34991,G1904_C34993,G1904_C34992,G1904_C34994,G1904_C34947,G1904_C34995,G1904_C34996,G1904_C34948,G1904_C34951,G1904_C34952,G1904_C34949,G1904_C34950,G1904_C34997,G1904_C34998,G1904_C34999,G1904_C35000,G1904_C35001,G1904_C35002 FROM '.$BaseDatos.'.G1904 WHERE G1904_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1904_C34933'] = $key->G1904_C34933;

                $datos[$i]['G1904_C34930'] = $key->G1904_C34930;

                $datos[$i]['G1904_C34931'] = $key->G1904_C34931;

                $datos[$i]['G1904_C34932'] = $key->G1904_C34932;

                $datos[$i]['G1904_C34934'] = $key->G1904_C34934;

                $datos[$i]['G1904_C34935'] = $key->G1904_C34935;

                $datos[$i]['G1904_C34936'] = $key->G1904_C34936;

                $datos[$i]['G1904_C34937'] = $key->G1904_C34937;

                $datos[$i]['G1904_C34938'] = $key->G1904_C34938;

                $datos[$i]['G1904_C34939'] = $key->G1904_C34939;

                $datos[$i]['G1904_C34940'] = $key->G1904_C34940;

                $datos[$i]['G1904_C34941'] = $key->G1904_C34941;

                $datos[$i]['G1904_C35121'] = $key->G1904_C35121;

                $datos[$i]['G1904_C34943'] = $key->G1904_C34943;

                $datos[$i]['G1904_C34944'] = $key->G1904_C34944;

                $datos[$i]['G1904_C34990'] = $key->G1904_C34990;

                $datos[$i]['G1904_C35097'] = $key->G1904_C35097;

                $datos[$i]['G1904_C35098'] = $key->G1904_C35098;

                $datos[$i]['G1904_C34945'] = $key->G1904_C34945;

                $datos[$i]['G1904_C34946'] = $key->G1904_C34946;

                $datos[$i]['G1904_C34991'] = $key->G1904_C34991;

                $datos[$i]['G1904_C34993'] = $key->G1904_C34993;

                $datos[$i]['G1904_C34992'] = $key->G1904_C34992;

                $datos[$i]['G1904_C34994'] = $key->G1904_C34994;

                $datos[$i]['G1904_C34947'] = $key->G1904_C34947;

                $datos[$i]['G1904_C34995'] = $key->G1904_C34995;

                $datos[$i]['G1904_C34996'] = $key->G1904_C34996;

                $datos[$i]['G1904_C34948'] = $key->G1904_C34948;

                $datos[$i]['G1904_C34951'] = $key->G1904_C34951;

                $datos[$i]['G1904_C34952'] = $key->G1904_C34952;

                $datos[$i]['G1904_C34949'] = $key->G1904_C34949;

                $datos[$i]['G1904_C34950'] = $key->G1904_C34950;

                $datos[$i]['G1904_C34997'] = $key->G1904_C34997;

                $datos[$i]['G1904_C34998'] = $key->G1904_C34998;

                $datos[$i]['G1904_C34999'] = $key->G1904_C34999;

                $datos[$i]['G1904_C35000'] = $key->G1904_C35000;

                $datos[$i]['G1904_C35001'] = $key->G1904_C35001;

                $datos[$i]['G1904_C35002'] = $key->G1904_C35002;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1904";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1904_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1904_ConsInte__b as id,  G1904_C34933 as camp1 , G1904_C34935 as camp2 
                     FROM ".$BaseDatos.".G1904  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1904_ConsInte__b as id,  G1904_C34933 as camp1 , G1904_C34935 as camp2  
                    FROM ".$BaseDatos.".G1904  JOIN ".$BaseDatos.".G1904_M".$_POST['muestra']." ON G1904_ConsInte__b = G1904_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1904_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1904_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1904_C34933 LIKE '%".$B."%' OR G1904_C34935 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1904_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1904");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1904_ConsInte__b, G1904_FechaInsercion , G1904_Usuario ,  G1904_CodigoMiembro  , G1904_PoblacionOrigen , G1904_EstadoDiligenciamiento ,  G1904_IdLlamada , G1904_C34933 as principal ,G1904_C34933,G1904_C34930,G1904_C34931, a.LISOPC_Nombre____b as G1904_C34932,G1904_C34934,G1904_C34935,G1904_C34936,G1904_C34937,G1904_C34938,G1904_C34939,G1904_C34940,G1904_C34941,G1904_C35121,G1904_C34943,G1904_C34944,G1904_C34990,G1904_C35097,G1904_C35098,G1904_C34945,G1904_C34946, b.LISOPC_Nombre____b as G1904_C34991,G1904_C34993,G1904_C34992,G1904_C34994,G1904_C34947,G1904_C34995,G1904_C34996,G1904_C34948,G1904_C34951,G1904_C34952,G1904_C34949,G1904_C34950, c.LISOPC_Nombre____b as G1904_C34997, d.LISOPC_Nombre____b as G1904_C34998, e.LISOPC_Nombre____b as G1904_C34999,G1904_C35000,G1904_C35001,G1904_C35002 FROM '.$BaseDatos.'.G1904 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1904_C34932 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1904_C34991 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1904_C34997 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1904_C34998 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1904_C34999';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G1904_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1904_ConsInte__b , ($fila->G1904_C34933) , ($fila->G1904_C34930) , ($fila->G1904_C34931) , ($fila->G1904_C34932) , ($fila->G1904_C34934) , ($fila->G1904_C34935) , ($fila->G1904_C34936) , ($fila->G1904_C34937) , ($fila->G1904_C34938) , ($fila->G1904_C34939) , ($fila->G1904_C34940) , ($fila->G1904_C34941) , ($fila->G1904_C35121) , ($fila->G1904_C34943) , ($fila->G1904_C34944) , ($fila->G1904_C34990) , ($fila->G1904_C35097) , ($fila->G1904_C35098) , ($fila->G1904_C34945) , ($fila->G1904_C34946) , ($fila->G1904_C34991) , ($fila->G1904_C34993) , ($fila->G1904_C34992) , ($fila->G1904_C34994) , ($fila->G1904_C34947) , ($fila->G1904_C34995) , ($fila->G1904_C34996) , ($fila->G1904_C34948) , ($fila->G1904_C34951) , ($fila->G1904_C34952) , ($fila->G1904_C34949) , ($fila->G1904_C34950) , ($fila->G1904_C34997) , ($fila->G1904_C34998) , ($fila->G1904_C34999) , ($fila->G1904_C35000) , ($fila->G1904_C35001) , ($fila->G1904_C35002) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1904 WHERE G1904_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1904";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1904_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1904_ConsInte__b as id,  G1904_C34933 as camp1 , G1904_C34935 as camp2  FROM '.$BaseDatos.'.G1904 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1904_ConsInte__b as id,  G1904_C34933 as camp1 , G1904_C34935 as camp2  
                    FROM ".$BaseDatos.".G1904  JOIN ".$BaseDatos.".G1904_M".$_POST['muestra']." ON G1904_ConsInte__b = G1904_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1904_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1904_C34933 LIKE "%'.$B.'%" OR G1904_C34935 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1904_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1904 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1904(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1904_C34933"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34933 = '".$_POST["G1904_C34933"]."'";
                $LsqlI .= $separador."G1904_C34933";
                $LsqlV .= $separador."'".$_POST["G1904_C34933"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34930"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34930 = '".$_POST["G1904_C34930"]."'";
                $LsqlI .= $separador."G1904_C34930";
                $LsqlV .= $separador."'".$_POST["G1904_C34930"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34931"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34931 = '".$_POST["G1904_C34931"]."'";
                $LsqlI .= $separador."G1904_C34931";
                $LsqlV .= $separador."'".$_POST["G1904_C34931"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34932"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34932 = '".$_POST["G1904_C34932"]."'";
                $LsqlI .= $separador."G1904_C34932";
                $LsqlV .= $separador."'".$_POST["G1904_C34932"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34934"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34934 = '".$_POST["G1904_C34934"]."'";
                $LsqlI .= $separador."G1904_C34934";
                $LsqlV .= $separador."'".$_POST["G1904_C34934"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34935"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34935 = '".$_POST["G1904_C34935"]."'";
                $LsqlI .= $separador."G1904_C34935";
                $LsqlV .= $separador."'".$_POST["G1904_C34935"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34936"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34936 = '".$_POST["G1904_C34936"]."'";
                $LsqlI .= $separador."G1904_C34936";
                $LsqlV .= $separador."'".$_POST["G1904_C34936"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34937"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34937 = '".$_POST["G1904_C34937"]."'";
                $LsqlI .= $separador."G1904_C34937";
                $LsqlV .= $separador."'".$_POST["G1904_C34937"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34938"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34938 = '".$_POST["G1904_C34938"]."'";
                $LsqlI .= $separador."G1904_C34938";
                $LsqlV .= $separador."'".$_POST["G1904_C34938"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34939"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34939 = '".$_POST["G1904_C34939"]."'";
                $LsqlI .= $separador."G1904_C34939";
                $LsqlV .= $separador."'".$_POST["G1904_C34939"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34940"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34940 = '".$_POST["G1904_C34940"]."'";
                $LsqlI .= $separador."G1904_C34940";
                $LsqlV .= $separador."'".$_POST["G1904_C34940"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34941"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34941 = '".$_POST["G1904_C34941"]."'";
                $LsqlI .= $separador."G1904_C34941";
                $LsqlV .= $separador."'".$_POST["G1904_C34941"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C35121"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C35121 = '".$_POST["G1904_C35121"]."'";
                $LsqlI .= $separador."G1904_C35121";
                $LsqlV .= $separador."'".$_POST["G1904_C35121"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34943"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34943 = '".$_POST["G1904_C34943"]."'";
                $LsqlI .= $separador."G1904_C34943";
                $LsqlV .= $separador."'".$_POST["G1904_C34943"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34944"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34944 = '".$_POST["G1904_C34944"]."'";
                $LsqlI .= $separador."G1904_C34944";
                $LsqlV .= $separador."'".$_POST["G1904_C34944"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34990"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34990 = '".$_POST["G1904_C34990"]."'";
                $LsqlI .= $separador."G1904_C34990";
                $LsqlV .= $separador."'".$_POST["G1904_C34990"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C35097"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C35097 = '".$_POST["G1904_C35097"]."'";
                $LsqlI .= $separador."G1904_C35097";
                $LsqlV .= $separador."'".$_POST["G1904_C35097"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C35098"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C35098 = '".$_POST["G1904_C35098"]."'";
                $LsqlI .= $separador."G1904_C35098";
                $LsqlV .= $separador."'".$_POST["G1904_C35098"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34945"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34945 = '".$_POST["G1904_C34945"]."'";
                $LsqlI .= $separador."G1904_C34945";
                $LsqlV .= $separador."'".$_POST["G1904_C34945"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34946"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34946 = '".$_POST["G1904_C34946"]."'";
                $LsqlI .= $separador."G1904_C34946";
                $LsqlV .= $separador."'".$_POST["G1904_C34946"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34991"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34991 = '".$_POST["G1904_C34991"]."'";
                $LsqlI .= $separador."G1904_C34991";
                $LsqlV .= $separador."'".$_POST["G1904_C34991"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34993"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34993 = '".$_POST["G1904_C34993"]."'";
                $LsqlI .= $separador."G1904_C34993";
                $LsqlV .= $separador."'".$_POST["G1904_C34993"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34992"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34992 = '".$_POST["G1904_C34992"]."'";
                $LsqlI .= $separador."G1904_C34992";
                $LsqlV .= $separador."'".$_POST["G1904_C34992"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34994"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34994 = '".$_POST["G1904_C34994"]."'";
                $LsqlI .= $separador."G1904_C34994";
                $LsqlV .= $separador."'".$_POST["G1904_C34994"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34947"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34947 = '".$_POST["G1904_C34947"]."'";
                $LsqlI .= $separador."G1904_C34947";
                $LsqlV .= $separador."'".$_POST["G1904_C34947"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34995"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34995 = '".$_POST["G1904_C34995"]."'";
                $LsqlI .= $separador."G1904_C34995";
                $LsqlV .= $separador."'".$_POST["G1904_C34995"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34996"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34996 = '".$_POST["G1904_C34996"]."'";
                $LsqlI .= $separador."G1904_C34996";
                $LsqlV .= $separador."'".$_POST["G1904_C34996"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34948"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34948 = '".$_POST["G1904_C34948"]."'";
                $LsqlI .= $separador."G1904_C34948";
                $LsqlV .= $separador."'".$_POST["G1904_C34948"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34951"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34951 = '".$_POST["G1904_C34951"]."'";
                $LsqlI .= $separador."G1904_C34951";
                $LsqlV .= $separador."'".$_POST["G1904_C34951"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34952"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34952 = '".$_POST["G1904_C34952"]."'";
                $LsqlI .= $separador."G1904_C34952";
                $LsqlV .= $separador."'".$_POST["G1904_C34952"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34949"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34949 = '".$_POST["G1904_C34949"]."'";
                $LsqlI .= $separador."G1904_C34949";
                $LsqlV .= $separador."'".$_POST["G1904_C34949"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34950"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34950 = '".$_POST["G1904_C34950"]."'";
                $LsqlI .= $separador."G1904_C34950";
                $LsqlV .= $separador."'".$_POST["G1904_C34950"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34997"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34997 = '".$_POST["G1904_C34997"]."'";
                $LsqlI .= $separador."G1904_C34997";
                $LsqlV .= $separador."'".$_POST["G1904_C34997"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34998"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34998 = '".$_POST["G1904_C34998"]."'";
                $LsqlI .= $separador."G1904_C34998";
                $LsqlV .= $separador."'".$_POST["G1904_C34998"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C34999"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C34999 = '".$_POST["G1904_C34999"]."'";
                $LsqlI .= $separador."G1904_C34999";
                $LsqlV .= $separador."'".$_POST["G1904_C34999"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C35000"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C35000 = '".$_POST["G1904_C35000"]."'";
                $LsqlI .= $separador."G1904_C35000";
                $LsqlV .= $separador."'".$_POST["G1904_C35000"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C35001"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C35001 = '".$_POST["G1904_C35001"]."'";
                $LsqlI .= $separador."G1904_C35001";
                $LsqlV .= $separador."'".$_POST["G1904_C35001"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1904_C35002"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_C35002 = '".$_POST["G1904_C35002"]."'";
                $LsqlI .= $separador."G1904_C35002";
                $LsqlV .= $separador."'".$_POST["G1904_C35002"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1904_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1904_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1904_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1904_Usuario , G1904_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1904_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1904 WHERE G1904_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G1904 SET G1904_UltiGest__b =-14, G1904_GesMasImp_b =-14, G1904_TipoReintentoUG_b =0, G1904_TipoReintentoGMI_b =0, G1904_EstadoUG_b =-14, G1904_EstadoGMI_b =-14, G1904_CantidadIntentos =0, G1904_CantidadIntentosGMI_b =0 WHERE G1904_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
