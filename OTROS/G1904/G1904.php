
<?php date_default_timezone_set('America/Bogota'); ?>

<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1904/G1904_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1904_ConsInte__b as id, G1904_C34933 as camp1 , G1904_C34935 as camp2 FROM ".$BaseDatos.".G1904  WHERE G1904_Usuario = ".$idUsuario." ORDER BY G1904_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1904_ConsInte__b as id, G1904_C34933 as camp1 , G1904_C34935 as camp2 FROM ".$BaseDatos.".G1904  ORDER BY G1904_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G1904_ConsInte__b as id, G1904_C34933 as camp1 , G1904_C34935 as camp2 FROM ".$BaseDatos.".G1904 JOIN ".$BaseDatos.".G1904_M".$resultEstpas->muestr." ON G1904_ConsInte__b = G1904_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G1904_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G1904_ConsInte__b as id, G1904_C34933 as camp1 , G1904_C34935 as camp2 FROM ".$BaseDatos.".G1904 JOIN ".$BaseDatos.".G1904_M".$resultEstpas->muestr." ON G1904_ConsInte__b = G1904_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G1904_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G1904_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G1904_ConsInte__b as id, G1904_C34933 as camp1 , G1904_C34935 as camp2 FROM ".$BaseDatos.".G1904  ORDER BY G1904_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<div  class="panel box box-primary" id="4992" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4992c">
                INFORMACIÓN DEL REGISTRO
            </a>
        </h4>
        
    </div>
    <div id="s_4992c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34933" id="LblG1904_C34933">ID REGISTRO</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34933" value="<?php if (isset($_GET['G1904_C34933'])) {
                            echo $_GET['G1904_C34933'];
                        } ?>"  name="G1904_C34933"  placeholder="ID REGISTRO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="4993" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34930" id="LblG1904_C34930">ORIGEN_DY_WF</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34930" value="<?php if (isset($_GET['G1904_C34930'])) {
                            echo $_GET['G1904_C34930'];
                        } ?>" readonly name="G1904_C34930"  placeholder="ORIGEN_DY_WF">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34931" id="LblG1904_C34931">OPTIN_DY_WF</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34931" value="<?php if (isset($_GET['G1904_C34931'])) {
                            echo $_GET['G1904_C34931'];
                        } ?>" readonly name="G1904_C34931"  placeholder="OPTIN_DY_WF">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1904_C34932" id="LblG1904_C34932">ESTADO_DY</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1904_C34932" id="G1904_C34932">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1968 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="4994" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4994c">
                INFORMACION DEL ESTUDIANTE
            </a>
        </h4>
        
    </div>
    <div id="s_4994c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34934" id="LblG1904_C34934">ZONA SDP</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34934" value="<?php if (isset($_GET['G1904_C34934'])) {
                            echo $_GET['G1904_C34934'];
                        } ?>"  name="G1904_C34934"  placeholder="ZONA SDP">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34935" id="LblG1904_C34935">NOMBRE DEL ESTUDIANTE</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34935" value="<?php if (isset($_GET['G1904_C34935'])) {
                            echo $_GET['G1904_C34935'];
                        } ?>"  name="G1904_C34935"  placeholder="NOMBRE DEL ESTUDIANTE">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34936" id="LblG1904_C34936">TIPO DE DOCUMENTO</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34936" value="<?php if (isset($_GET['G1904_C34936'])) {
                            echo $_GET['G1904_C34936'];
                        } ?>"  name="G1904_C34936"  placeholder="TIPO DE DOCUMENTO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34937" id="LblG1904_C34937">NÚMERO DE IDENTIFICACIÓN</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34937" value="<?php if (isset($_GET['G1904_C34937'])) {
                            echo $_GET['G1904_C34937'];
                        } ?>"  name="G1904_C34937"  placeholder="NÚMERO DE IDENTIFICACIÓN">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34938" id="LblG1904_C34938">CICLO EDUCATIVO</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34938" value="<?php if (isset($_GET['G1904_C34938'])) {
                            echo $_GET['G1904_C34938'];
                        } ?>"  name="G1904_C34938"  placeholder="CICLO EDUCATIVO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34939" id="LblG1904_C34939">GRADO ACTUAL</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34939" value="<?php if (isset($_GET['G1904_C34939'])) {
                            echo $_GET['G1904_C34939'];
                        } ?>"  name="G1904_C34939"  placeholder="GRADO ACTUAL">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34940" id="LblG1904_C34940">FECHA DE NACIMIENTO</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34940" value="<?php if (isset($_GET['G1904_C34940'])) {
                            echo $_GET['G1904_C34940'];
                        } ?>"  name="G1904_C34940"  placeholder="FECHA DE NACIMIENTO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34941" id="LblG1904_C34941">COLEGIO</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34941" value="<?php if (isset($_GET['G1904_C34941'])) {
                            echo $_GET['G1904_C34941'];
                        } ?>"  name="G1904_C34941"  placeholder="COLEGIO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C35121" id="LblG1904_C35121">NOMBRE ESTABLECIMIENTO EDUCATIVO</label>
                        <input type="text" class="form-control input-sm" id="G1904_C35121" value="<?php if (isset($_GET['G1904_C35121'])) {
                            echo $_GET['G1904_C35121'];
                        } ?>"  name="G1904_C35121"  placeholder="NOMBRE ESTABLECIMIENTO EDUCATIVO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="4995" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4995c">
                INFORMACION DEL ACUDIENTE
            </a>
        </h4>
        
    </div>
    <div id="s_4995c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34943" id="LblG1904_C34943">DIRECCION DE RESIDENCIA ACUDIENTE</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34943" value="<?php if (isset($_GET['G1904_C34943'])) {
                            echo $_GET['G1904_C34943'];
                        } ?>"  name="G1904_C34943"  placeholder="DIRECCION DE RESIDENCIA ACUDIENTE">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34944" id="LblG1904_C34944">NOMBRE DEL ACUDIENTE</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34944" value="<?php if (isset($_GET['G1904_C34944'])) {
                            echo $_GET['G1904_C34944'];
                        } ?>"  name="G1904_C34944"  placeholder="NOMBRE DEL ACUDIENTE">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34990" id="LblG1904_C34990">Persona Autorizada para recibir</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34990" value="<?php if (isset($_GET['G1904_C34990'])) {
                            echo $_GET['G1904_C34990'];
                        } ?>"  name="G1904_C34990"  placeholder="Persona Autorizada para recibir">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C35097" id="LblG1904_C35097">Parentezco del acudiente</label>
                        <input type="text" class="form-control input-sm" id="G1904_C35097" value="<?php if (isset($_GET['G1904_C35097'])) {
                            echo $_GET['G1904_C35097'];
                        } ?>"  name="G1904_C35097"  placeholder="Parentezco del acudiente">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C35098" id="LblG1904_C35098">Parentezco de quien recibe</label>
                        <input type="text" class="form-control input-sm" id="G1904_C35098" value="<?php if (isset($_GET['G1904_C35098'])) {
                            echo $_GET['G1904_C35098'];
                        } ?>"  name="G1904_C35098"  placeholder="Parentezco de quien recibe">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="4996" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4996c">
                INFORMACION DE CONTACTO
            </a>
        </h4>
        
    </div>
    <div id="s_4996c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34945" id="LblG1904_C34945">LUGAR DE ENTREGA</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34945" value="<?php if (isset($_GET['G1904_C34945'])) {
                            echo $_GET['G1904_C34945'];
                        } ?>"  name="G1904_C34945"  placeholder="LUGAR DE ENTREGA">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34946" id="LblG1904_C34946">DIRECCION DE RESIDENCIA DEL ESTUDIANTE</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34946" value="<?php if (isset($_GET['G1904_C34946'])) {
                            echo $_GET['G1904_C34946'];
                        } ?>"  name="G1904_C34946"  placeholder="DIRECCION DE RESIDENCIA DEL ESTUDIANTE">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1904_C34991" id="LblG1904_C34991">¿Se actualizo la dirección?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1904_C34991" id="G1904_C34991">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1970 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34993" id="LblG1904_C34993">Especificaciones de Dirección (Torre, manzana etc)</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34993" value="<?php if (isset($_GET['G1904_C34993'])) {
                            echo $_GET['G1904_C34993'];
                        } ?>"  name="G1904_C34993"  placeholder="Especificaciones de Dirección (Torre, manzana etc)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34992" id="LblG1904_C34992">DIRECCION ACTUALIZADA ESTUDIANTE</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34992" value="<?php if (isset($_GET['G1904_C34992'])) {
                            echo $_GET['G1904_C34992'];
                        } ?>"  name="G1904_C34992"  placeholder="DIRECCION ACTUALIZADA ESTUDIANTE">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34994" id="LblG1904_C34994">CIUDAD</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34994" value="<?php if (isset($_GET['G1904_C34994'])) {
                            echo $_GET['G1904_C34994'];
                        } ?>"  name="G1904_C34994"  placeholder="CIUDAD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34947" id="LblG1904_C34947">LOCALIDAD</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34947" value="<?php if (isset($_GET['G1904_C34947'])) {
                            echo $_GET['G1904_C34947'];
                        } ?>"  name="G1904_C34947"  placeholder="LOCALIDAD">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34995" id="LblG1904_C34995">BARRIO</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34995" value="<?php if (isset($_GET['G1904_C34995'])) {
                            echo $_GET['G1904_C34995'];
                        } ?>"  name="G1904_C34995"  placeholder="BARRIO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34996" id="LblG1904_C34996">Dirección de Trabajo del Acudiente (solo si aplica)</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34996" value="<?php if (isset($_GET['G1904_C34996'])) {
                            echo $_GET['G1904_C34996'];
                        } ?>"  name="G1904_C34996"  placeholder="Dirección de Trabajo del Acudiente (solo si aplica)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34948" id="LblG1904_C34948">CORREO ELECTRONICO</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34948" value="<?php if (isset($_GET['G1904_C34948'])) {
                            echo $_GET['G1904_C34948'];
                        } ?>"  name="G1904_C34948"  placeholder="CORREO ELECTRONICO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34951" id="LblG1904_C34951">TELÉFONO CELULAR 1</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34951" value="<?php if (isset($_GET['G1904_C34951'])) {
                            echo $_GET['G1904_C34951'];
                        } ?>"  name="G1904_C34951"  placeholder="TELÉFONO CELULAR 1">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34952" id="LblG1904_C34952">TELÉFONO CELULAR  2</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34952" value="<?php if (isset($_GET['G1904_C34952'])) {
                            echo $_GET['G1904_C34952'];
                        } ?>"  name="G1904_C34952"  placeholder="TELÉFONO CELULAR  2">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34949" id="LblG1904_C34949">TELÉFONO 1 FIJO</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34949" value="<?php if (isset($_GET['G1904_C34949'])) {
                            echo $_GET['G1904_C34949'];
                        } ?>"  name="G1904_C34949"  placeholder="TELÉFONO 1 FIJO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C34950" id="LblG1904_C34950">TELÉFONO 2 FIJO</label>
                        <input type="text" class="form-control input-sm" id="G1904_C34950" value="<?php if (isset($_GET['G1904_C34950'])) {
                            echo $_GET['G1904_C34950'];
                        } ?>"  name="G1904_C34950"  placeholder="TELÉFONO 2 FIJO">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5004" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5004c">
                INFORMACION ADICIONAL
            </a>
        </h4>
        
    </div>
    <div id="s_5004c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1904_C34997" id="LblG1904_C34997">¿Cuentan con conexión a internet en el hogar?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1904_C34997" id="G1904_C34997">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1970 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1904_C34998" id="LblG1904_C34998">¿El/la estudiante cuenta con acceso a herramientas tecnológicas como computador o tablet?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1904_C34998" id="G1904_C34998">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1970 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5005" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5005c">
                AUTORIZACION PROTECCION DE DATOS PERSONALES
            </a>
        </h4>
        
    </div>
    <div id="s_5005c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1904_C34999" id="LblG1904_C34999">¿Confirma usted que la información brindada es la que corresponde al Acudiente del Estudiante (Nombres y Apellidos del Estudiante) y autoriza al tratamiento de los datos para vincularlos en la inscripción, seguimiento y envío de material del Programa Aprende en Casa con MALOKA de acuerdo a nuestra Política de Tratamiento de Datos Personales?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1904_C34999" id="G1904_C34999">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1970 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C35000" id="LblG1904_C35000">Nombre de la Persona que Actualizo y Confirmo los datos</label>
                        <input type="text" class="form-control input-sm" id="G1904_C35000" value="<?php if (isset($_GET['G1904_C35000'])) {
                            echo $_GET['G1904_C35000'];
                        } ?>"  name="G1904_C35000"  placeholder="Nombre de la Persona que Actualizo y Confirmo los datos">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1904_C35001" id="LblG1904_C35001">Parentesco</label>
                        <input type="text" class="form-control input-sm" id="G1904_C35001" value="<?php if (isset($_GET['G1904_C35001'])) {
                            echo $_GET['G1904_C35001'];
                        } ?>"  name="G1904_C35001"  placeholder="Parentesco">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G1904_C35002" id="LblG1904_C35002">OBSERVACIONES</label>
                        <textarea class="form-control input-sm" name="G1904_C35002" id="G1904_C35002"  value="<?php if (isset($_GET['G1904_C35002'])) {
                            echo $_GET['G1904_C35002'];
                        } ?>" placeholder="OBSERVACIONES"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G1904/G1904_eventos.js"></script> 
<script type="text/javascript">
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la sección de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
          
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G1904_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G1904_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G1904_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G1904_C34933").val(item.G1904_C34933); 
                $("#G1904_C34930").val(item.G1904_C34930); 
                $("#G1904_C34931").val(item.G1904_C34931); 
                $("#G1904_C34932").val(item.G1904_C34932).trigger("change");  
                $("#G1904_C34934").val(item.G1904_C34934); 
                $("#G1904_C34935").val(item.G1904_C34935); 
                $("#G1904_C34936").val(item.G1904_C34936); 
                $("#G1904_C34937").val(item.G1904_C34937); 
                $("#G1904_C34938").val(item.G1904_C34938); 
                $("#G1904_C34939").val(item.G1904_C34939); 
                $("#G1904_C34940").val(item.G1904_C34940); 
                $("#G1904_C34941").val(item.G1904_C34941); 
                $("#G1904_C35121").val(item.G1904_C35121); 
                $("#G1904_C34943").val(item.G1904_C34943); 
                $("#G1904_C34944").val(item.G1904_C34944); 
                $("#G1904_C34990").val(item.G1904_C34990); 
                $("#G1904_C35097").val(item.G1904_C35097); 
                $("#G1904_C35098").val(item.G1904_C35098); 
                $("#G1904_C34945").val(item.G1904_C34945); 
                $("#G1904_C34946").val(item.G1904_C34946); 
                $("#G1904_C34991").val(item.G1904_C34991).trigger("change");  
                $("#G1904_C34993").val(item.G1904_C34993); 
                $("#G1904_C34992").val(item.G1904_C34992); 
                $("#G1904_C34994").val(item.G1904_C34994); 
                $("#G1904_C34947").val(item.G1904_C34947); 
                $("#G1904_C34995").val(item.G1904_C34995); 
                $("#G1904_C34996").val(item.G1904_C34996); 
                $("#G1904_C34948").val(item.G1904_C34948); 
                $("#G1904_C34951").val(item.G1904_C34951); 
                $("#G1904_C34952").val(item.G1904_C34952); 
                $("#G1904_C34949").val(item.G1904_C34949); 
                $("#G1904_C34950").val(item.G1904_C34950); 
                $("#G1904_C34997").val(item.G1904_C34997).trigger("change");  
                $("#G1904_C34998").val(item.G1904_C34998).trigger("change");  
                $("#G1904_C34999").val(item.G1904_C34999).trigger("change");  
                $("#G1904_C35000").val(item.G1904_C35000); 
                $("#G1904_C35001").val(item.G1904_C35001); 
                $("#G1904_C35002").val(item.G1904_C35002);
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G1904_C34932").select2();

    $("#G1904_C34991").select2();

    $("#G1904_C34997").select2();

    $("#G1904_C34998").select2();

    $("#G1904_C34999").select2();
        //datepickers
        

        //Timepickers
        


        //Validaciones numeros Enteros
        


        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para ESTADO_DY 

    $("#G1904_C34932").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Se actualizo la dirección? 

    $("#G1904_C34991").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Cuentan con conexión a internet en el hogar? 

    $("#G1904_C34997").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿El/la estudiante cuenta con acceso a herramientas tecnológicas como computador o tablet? 

    $("#G1904_C34998").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Confirma usted que la información brindada es la que corresponde al Acudiente del Estudiante (Nombres y Apellidos del Estudiante) y autoriza al tratamiento de los datos para vincularlos en la inscripción, seguimiento y envío de material del Programa Aprende en Casa con MALOKA de acuerdo a nuestra Política de Tratamiento de Datos Personales? 

    $("#G1904_C34999").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificación!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $("#Save").attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G1904_C34933").val(item.G1904_C34933);
 
                                                $("#G1904_C34930").val(item.G1904_C34930);
 
                                                $("#G1904_C34931").val(item.G1904_C34931);
 
                    $("#G1904_C34932").val(item.G1904_C34932).trigger("change"); 
 
                                                $("#G1904_C34934").val(item.G1904_C34934);
 
                                                $("#G1904_C34935").val(item.G1904_C34935);
 
                                                $("#G1904_C34936").val(item.G1904_C34936);
 
                                                $("#G1904_C34937").val(item.G1904_C34937);
 
                                                $("#G1904_C34938").val(item.G1904_C34938);
 
                                                $("#G1904_C34939").val(item.G1904_C34939);
 
                                                $("#G1904_C34940").val(item.G1904_C34940);
 
                                                $("#G1904_C34941").val(item.G1904_C34941);
 
                                                $("#G1904_C35121").val(item.G1904_C35121);
 
                                                $("#G1904_C34943").val(item.G1904_C34943);
 
                                                $("#G1904_C34944").val(item.G1904_C34944);
 
                                                $("#G1904_C34990").val(item.G1904_C34990);
 
                                                $("#G1904_C35097").val(item.G1904_C35097);
 
                                                $("#G1904_C35098").val(item.G1904_C35098);
 
                                                $("#G1904_C34945").val(item.G1904_C34945);
 
                                                $("#G1904_C34946").val(item.G1904_C34946);
 
                    $("#G1904_C34991").val(item.G1904_C34991).trigger("change"); 
 
                                                $("#G1904_C34993").val(item.G1904_C34993);
 
                                                $("#G1904_C34992").val(item.G1904_C34992);
 
                                                $("#G1904_C34994").val(item.G1904_C34994);
 
                                                $("#G1904_C34947").val(item.G1904_C34947);
 
                                                $("#G1904_C34995").val(item.G1904_C34995);
 
                                                $("#G1904_C34996").val(item.G1904_C34996);
 
                                                $("#G1904_C34948").val(item.G1904_C34948);
 
                                                $("#G1904_C34951").val(item.G1904_C34951);
 
                                                $("#G1904_C34952").val(item.G1904_C34952);
 
                                                $("#G1904_C34949").val(item.G1904_C34949);
 
                                                $("#G1904_C34950").val(item.G1904_C34950);
 
                    $("#G1904_C34997").val(item.G1904_C34997).trigger("change"); 
 
                    $("#G1904_C34998").val(item.G1904_C34998).trigger("change"); 
 
                    $("#G1904_C34999").val(item.G1904_C34999).trigger("change"); 
 
                                                $("#G1904_C35000").val(item.G1904_C35000);
 
                                                $("#G1904_C35001").val(item.G1904_C35001);
 
                                                $("#G1904_C35002").val(item.G1904_C35002);
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/generados/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?>',
                                        type  : "post",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            console.log(xt);
                                            window.location.href = "quitar.php";
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la información');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','ID REGISTRO','ORIGEN_DY_WF','OPTIN_DY_WF','ESTADO_DY','ZONA SDP','NOMBRE DEL ESTUDIANTE','TIPO DE DOCUMENTO','NÚMERO DE IDENTIFICACIÓN','CICLO EDUCATIVO','GRADO ACTUAL','FECHA DE NACIMIENTO','COLEGIO','NOMBRE ESTABLECIMIENTO EDUCATIVO','DIRECCION DE RESIDENCIA ACUDIENTE','NOMBRE DEL ACUDIENTE','Persona Autorizada para recibir','Parentezco del acudiente','Parentezco de quien recibe','LUGAR DE ENTREGA','DIRECCION DE RESIDENCIA DEL ESTUDIANTE','¿Se actualizo la dirección?','Especificaciones de Dirección (Torre, manzana etc)','DIRECCION ACTUALIZADA ESTUDIANTE','CIUDAD','LOCALIDAD','BARRIO','Dirección de Trabajo del Acudiente (solo si aplica)','CORREO ELECTRONICO','TELÉFONO CELULAR 1','TELÉFONO CELULAR  2','TELÉFONO 1 FIJO','TELÉFONO 2 FIJO','¿Cuentan con conexión a internet en el hogar?','¿El/la estudiante cuenta con acceso a herramientas tecnológicas como computador o tablet?','¿Confirma usted que la información brindada es la que corresponde al Acudiente del Estudiante (Nombres y Apellidos del Estudiante) y autoriza al tratamiento de los datos para vincularlos en la inscripción, seguimiento y envío de material del Programa Aprende en Casa con MALOKA de acuerdo a nuestra Política de Tratamiento de Datos Personales?','Nombre de la Persona que Actualizo y Confirmo los datos','Parentesco','OBSERVACIONES'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G1904_C34933', 
                        index: 'G1904_C34933', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34930', 
                        index: 'G1904_C34930', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34931', 
                        index: 'G1904_C34931', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34932', 
                        index:'G1904_C34932', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1968&campo=G1904_C34932'
                        }
                    }

                    ,
                    { 
                        name:'G1904_C34934', 
                        index: 'G1904_C34934', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34935', 
                        index: 'G1904_C34935', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34936', 
                        index: 'G1904_C34936', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34937', 
                        index: 'G1904_C34937', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34938', 
                        index: 'G1904_C34938', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34939', 
                        index: 'G1904_C34939', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34940', 
                        index: 'G1904_C34940', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34941', 
                        index: 'G1904_C34941', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C35121', 
                        index: 'G1904_C35121', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34943', 
                        index: 'G1904_C34943', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34944', 
                        index: 'G1904_C34944', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34990', 
                        index: 'G1904_C34990', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C35097', 
                        index: 'G1904_C35097', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C35098', 
                        index: 'G1904_C35098', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34945', 
                        index: 'G1904_C34945', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34946', 
                        index: 'G1904_C34946', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34991', 
                        index:'G1904_C34991', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1970&campo=G1904_C34991'
                        }
                    }

                    ,
                    { 
                        name:'G1904_C34993', 
                        index: 'G1904_C34993', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34992', 
                        index: 'G1904_C34992', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34994', 
                        index: 'G1904_C34994', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34947', 
                        index: 'G1904_C34947', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34995', 
                        index: 'G1904_C34995', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34996', 
                        index: 'G1904_C34996', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34948', 
                        index: 'G1904_C34948', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34951', 
                        index: 'G1904_C34951', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34952', 
                        index: 'G1904_C34952', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34949', 
                        index: 'G1904_C34949', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34950', 
                        index: 'G1904_C34950', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C34997', 
                        index:'G1904_C34997', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1970&campo=G1904_C34997'
                        }
                    }

                    ,
                    { 
                        name:'G1904_C34998', 
                        index:'G1904_C34998', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1970&campo=G1904_C34998'
                        }
                    }

                    ,
                    { 
                        name:'G1904_C34999', 
                        index:'G1904_C34999', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1970&campo=G1904_C34999'
                        }
                    }

                    ,
                    { 
                        name:'G1904_C35000', 
                        index: 'G1904_C35000', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C35001', 
                        index: 'G1904_C35001', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1904_C35002', 
                        index:'G1904_C35002', 
                        width:150, 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1904_C34933',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario;?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario;?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G1904_C34933").val(item.G1904_C34933);

                        $("#G1904_C34930").val(item.G1904_C34930);

                        $("#G1904_C34931").val(item.G1904_C34931);
 
                    $("#G1904_C34932").val(item.G1904_C34932).trigger("change"); 

                        $("#G1904_C34934").val(item.G1904_C34934);

                        $("#G1904_C34935").val(item.G1904_C34935);

                        $("#G1904_C34936").val(item.G1904_C34936);

                        $("#G1904_C34937").val(item.G1904_C34937);

                        $("#G1904_C34938").val(item.G1904_C34938);

                        $("#G1904_C34939").val(item.G1904_C34939);

                        $("#G1904_C34940").val(item.G1904_C34940);

                        $("#G1904_C34941").val(item.G1904_C34941);

                        $("#G1904_C35121").val(item.G1904_C35121);

                        $("#G1904_C34943").val(item.G1904_C34943);

                        $("#G1904_C34944").val(item.G1904_C34944);

                        $("#G1904_C34990").val(item.G1904_C34990);

                        $("#G1904_C35097").val(item.G1904_C35097);

                        $("#G1904_C35098").val(item.G1904_C35098);

                        $("#G1904_C34945").val(item.G1904_C34945);

                        $("#G1904_C34946").val(item.G1904_C34946);
 
                    $("#G1904_C34991").val(item.G1904_C34991).trigger("change"); 

                        $("#G1904_C34993").val(item.G1904_C34993);

                        $("#G1904_C34992").val(item.G1904_C34992);

                        $("#G1904_C34994").val(item.G1904_C34994);

                        $("#G1904_C34947").val(item.G1904_C34947);

                        $("#G1904_C34995").val(item.G1904_C34995);

                        $("#G1904_C34996").val(item.G1904_C34996);

                        $("#G1904_C34948").val(item.G1904_C34948);

                        $("#G1904_C34951").val(item.G1904_C34951);

                        $("#G1904_C34952").val(item.G1904_C34952);

                        $("#G1904_C34949").val(item.G1904_C34949);

                        $("#G1904_C34950").val(item.G1904_C34950);
 
                    $("#G1904_C34997").val(item.G1904_C34997).trigger("change"); 
 
                    $("#G1904_C34998").val(item.G1904_C34998).trigger("change"); 
 
                    $("#G1904_C34999").val(item.G1904_C34999).trigger("change"); 

                        $("#G1904_C35000").val(item.G1904_C35000);

                        $("#G1904_C35001").val(item.G1904_C35001);

                        $("#G1904_C35002").val(item.G1904_C35002);
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
