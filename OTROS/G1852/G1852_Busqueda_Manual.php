
<?php
	$http = "http://".$_SERVER["HTTP_HOST"];
	if (isset($_SERVER['HTTPS'])) {
	    $http = "https://".$_SERVER["HTTP_HOST"];
	}
	if(isset($_GET["accion_manual"])){
		$idMonoef=-19;
		$texto = "Busqueda No Aplica";
	}else{
		$idMonoef =-18;
		$texto = "No Suministra Información";
	}
?>

<script type="text/javascript">
	function autofitIframe(id){
		if (!window.opera && document.all && document.getElementById){
			id.style.height=id.contentWindow.document.body.scrollHeight;
		} else if(document.getElementById) {
			id.style.height=id.contentDocument.body.scrollHeight+"px";
		}
	}
</script>

<link rel="stylesheet" href="assets/plugins/WinPicker/dist/wickedpicker.min.css">
<script type="text/javascript" src="assets/plugins/WinPicker/dist/wickedpicker.min.js"></script>

<div class="row" id="buscador">
	<div class="col-md-1">&nbsp;</div>
	<div class="col-md-10">
		<div class="row">
			<form id="formId">
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G1852_C30590" id="LblG1852_C30590">CEDULA</label>
					    <input type="text" class="form-control input-sm" id="G1852_C30590" name="G1852_C30590"  placeholder="CEDULA">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 
				<!-- CAMPO TIPO TEXTO -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G1852_C30534" id="LblG1852_C30534">EMAIL</label>
					    <input type="email" class="form-control input-sm" id="G1852_C30534" name="G1852_C30534"  placeholder="EMAIL">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO TEXTO -->
 	
				<!-- CAMPO TIPO FECHA -->
				<!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
				<div class="col-xs-4">
					<div class="form-group">
					    <label for="G1852_C30537" id="LblG1852_C30537">FECHA</label>
					    <input type="text" class="form-control input-sm Fecha"  name="G1852_C30537" id="G1852_C30537" placeholder="YYYY-MM-DD">
					</div>
				</div>
				<!-- FIN DEL CAMPO TIPO FECHA-->
 	
				<!-- CAMPO TIMEPICKER -->
				<!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
				<div class="col-xs-4">
					<div class="bootstrap-timepicker">
					    <div class="form-group">
					        <label for="G1852_C30538" id="LblG1852_C30538">HORA</label>
					        <div class="input-group">
					            <input type="text" class="form-control input-sm Hora"  name="G1852_C30538" id="G1852_C30538" placeholder="HH:MM:SS" >
					            <div class="input-group-addon">
					                <i class="fa fa-clock-o"></i>
					            </div>
					        </div>
					        <!-- /.input group -->
					    </div>
					    <!-- /.form group -->
					</div>
				</div>
				<!-- FIN DEL CAMPO TIMEPICKER -->
 	
			</form>
		</div>
	</div>
	<div class="col-md-1">&nbsp;</div>
</div>
<div class="row" id="botones">
	<div class="col-md-6 col-xs-6">
		<button class="btn btn-block btn-danger" id="btnCancelar" type="button"><?php echo $texto; ?></button>
	</div>
	<div class="col-md-6 col-xs-6">
		<button class="btn btn-block btn-success" id="btnBuscar" type="button"><i class="fa fa-search"></i>Buscar en toda la base de datos</button>
	</div>
</div>
<br/>
<div class="row" id="resulados">
	<div class="col-md-12 col-xs-12" id="resultadosBusqueda">

	</div>
</div>
<div class="row">
	<div class="col-md-12" id="gestiones">
		<iframe id="frameContenedor" src="" style="width: 100%; height: 2500px;"  marginheight="0" marginwidth="0" noresize  frameborder="0" onload="autofitIframe(this);">
              
        </iframe>
	</div>
</div>
<input type="hidden" id="CampoIdGestionCbx" value="<?php echo $_GET["id_gestion_cbx"];?>">
<script type="text/javascript" src="formularios/generados/funcioneslocalstorage.js"></script>
<script type="text/javascript">
guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>');
<?php 
    $id_campan='';
    if(isset($_GET['id_campana_crm']) && $_GET['id_campana_crm'] != '0'){
        $id_campan='&id_campana_crm='.$_GET['id_campana_crm'];
    } 
?>

    function bindEvent(element, eventName, eventHandler) {
        if (element.addEventListener) {
            element.addEventListener(eventName, eventHandler, false);
        } else if (element.attachEvent) {
            element.attachEvent('on' + eventName, eventHandler);
        }
    }


    // Listen to message from child window
    bindEvent(window, 'message', function (e) {
        if(Array.isArray(e.data)){
            console.log(e.data);
            var keys=Object.keys(e.data[0].camposivr);
            var key=0;
            $.each(e.data[0].camposivr, function(i, valor){
                if($("#"+keys[key]).length > 0){
                    $("#"+keys[key]).val(valor); 
                }
                key++;
            });
            buscarRegistros(e.data);
//          $("#btnBuscar").click();
        }
    });
    
	$(function(){
    
        //DATEPICKER
        
                $("#G1852_C30537").datepicker({
                    language: "es",
                    autoclose: true,
                    todayHighlight: true,
                    format:"yyyy-mm-dd"
                });
                
        
        //TIMEPICKER
        
                var options = { //hh:mm 24 hour format only, defaults to current time
                    twentyFour: true, //Display 24 hour format, defaults to false
                    title: 'HORA', //The Wickedpicker's title,
                    showSeconds: true, //Whether or not to show seconds,
                    secondsInterval: 1, //Change interval for seconds, defaults to 1
                    minutesInterval: 1, //Change interval for minutes, defaults to 1
                    beforeShow: null, //A function to be called before the Wickedpicker is shown
                    show: null, //A function to be called when the Wickedpicker is shown
                    clearable: false, //Make the picker's input clearable (has clickable "x")
                }; 
                $("#G1852_C30538").wickedpicker(options);
                $("#G1852_C30538").val("");
                
        var ObjDataGestion= new Object();
        ObjDataGestion.server         = '<?php echo $http; ?>';
        ObjDataGestion.canal          = '<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal bq_manual"; }?>';
        ObjDataGestion.token          = '<?php echo $_GET["token"];?>';
        ObjDataGestion.predictiva     = '<?php if(isset($_GET['predictiva'])) { echo $_GET['predictiva']; } else{ echo 'null'; }?>';
        ObjDataGestion.consinte       = '<?php if(isset($_GET['consinte'])) { echo $_GET['consinte']; } else{ echo 'null'; }?>';
        ObjDataGestion.id_campana_crm = '<?php if(isset($_GET['id_campana_crm'])) { echo $_GET['id_campana_crm']; } else{ echo 'null'; }?>';
        ObjDataGestion.sentido        = '<?php if(isset($_GET['sentido'])) { echo $_GET['sentido']; } else{ echo 'null'; }?>';
            $("#btnBuscar").click(function(){
            var valido=0;
            
                if($('#G1852_C30534').val() != ''){                
                    var escorreo=/^[^@\s]+@[^@\.\s]+(\.[^@\.\s]+)+$/;         
                    if (!(escorreo.test($('#G1852_C30534').val()))) { 
                        alertify.error('Digite un correo valido');
                        $('#G1852_C30534').focus();
                        valido=1;
                    }
                }
                    
            
            if(valido==0){
                var datos = $("#formId").serialize();
                $.ajax({
                    url     	: 'formularios/G1852/G1852_Funciones_Busqueda_Manual.php?action=GET_DATOS<?=$id_campan?>&agente=<?=getIdentificacionUser($_GET["token"])?>',
                    type		: 'post',
                    dataType	: 'json',
                    data		: datos,
                    success 	: function(datosq){
                        if(datosq[0].cantidad_registros > 1){
                            buscarRegistros(datosq);
                        }else if(datosq[0].cantidad_registros == 1){
                            $("#buscador").hide();
                            $("#botones").hide();
                            $("#resulados").hide();
                            var id = datosq[0].registros[0].G1852_ConsInte__b;
                            if(datosq[0].registros[0].id_muestra == null){
                                $.ajax({
                                    url     	: 'formularios/generados/PHP_Ejecutar.php?action=ADD_MUESTRA&campana_crm=<?php echo $_GET['id_campana_crm'];?>',
                                    type		: 'post',
                                    data        : {id:id},
                                    success 	: function(data){
                                        <?php if(isset($_GET['token'])) { ?>
                                            guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>',id,ObjDataGestion);
                                            $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal ani"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>');
                                        <?php } ?>
                                    }
                                });
                            }else{
                                <?php if(isset($_GET['token'])) { ?>
                                    guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>',id,ObjDataGestion);
                                    $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal ani"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>');
                                <?php } ?>                        
                            }
                        }else{
                            adicionarRegistro(2,datosq[0].mensaje);
                        }
                    }
                });
            }
		});

		$("\#btnCancelar").click(function(){
            borrarStorage('<?php echo $_GET["id_gestion_cbx"]?>');
			window.location = 'formularios/generados/PHP_Cerrar_Cancelar.php?canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal ani"; } ?>&token=<?php echo $_GET['token'];?>&id_gestion_cbx=<?php echo $_GET['id_gestion_cbx'];?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?><?php echo "&idMonoef=".$idMonoef;?><?php echo "&tiempoInicio=".date("Y-m-d H:i:s");?>';
		});
	});

    function buscarRegistros(datosq){
        var ObjDataGestion= new Object();
        ObjDataGestion.server         = '<?php echo $http; ?>';
        ObjDataGestion.canal          = '<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal bq_manual"; }?>';
        ObjDataGestion.token          = '<?php echo $_GET["token"];?>';
        ObjDataGestion.predictiva     = '<?php if(isset($_GET['predictiva'])) { echo $_GET['predictiva']; } else{ echo 'null'; }?>';
        ObjDataGestion.consinte       = '<?php if(isset($_GET['consinte'])) { echo $_GET['consinte']; } else{ echo 'null'; }?>';
        ObjDataGestion.id_campana_crm = '<?php if(isset($_GET['id_campana_crm'])) { echo $_GET['id_campana_crm']; } else{ echo 'null'; }?>';
        ObjDataGestion.sentido        = '<?php if(isset($_GET['sentido'])) { echo $_GET['sentido']; } else{ echo 'null'; }?>';    
        var valores = null;
        var tabla_a_mostrar = '<div class="box box-default">'+
        '<div class="box-header">'+
            '<h3 class="box-title">RESULTADOS DE LA BUSQUEDA</h3>'+
        '</div>'+
        '<div class="box-body">'+
            '<table class="table table-hover table-bordered" style="width:100%;">';
        tabla_a_mostrar += '<thead>';
        tabla_a_mostrar += '<tr>';
        tabla_a_mostrar += ' <th>CEDULA</th><th>EMAIL</th><th>FECHA</th><th>HORA</th> ';
        tabla_a_mostrar += '</tr>';
        tabla_a_mostrar += '</thead>';
        tabla_a_mostrar += '<tbody>';
        $.each(datosq[0].registros, function(i, item) {
            tabla_a_mostrar += '<tr idMuestra="'+ item.id_muestra +'" ConsInte="'+ item.G1852_ConsInte__b +'" class="EditRegistro">';
            tabla_a_mostrar += '<td>'+ item.G1852_C30590 +'</td><td>'+ item.G1852_C30534 +'</td><td>'+ item.G1852_C30537 +'</td><td>'+ item.G1852_C30538 +'</td>';
            tabla_a_mostrar += '</tr>';
        });
        tabla_a_mostrar += '</tbody>';
        tabla_a_mostrar += '</table></div></div>';

        $("#resultadosBusqueda").html(tabla_a_mostrar);

        $(".EditRegistro").dblclick(function(){
            var id = $(this).attr("ConsInte");
            var muestra=$(this).attr("idMuestra");
            swal({
                html : true,
                title: "Información - Dyalogo CRM",
                text: 'Esta seguro de editar este registro?',
                type: "warning",
                confirmButtonText: "Editar registro",
                cancelButtonText : "No Editar registro",
                showCancelButton : true,
                closeOnConfirm : true
            },
                function(isconfirm){
                    if(isconfirm){
                        $("#buscador").hide();
                        $("#botones").hide();
                        $("#resulados").hide();
                        if(muestra == 'null'){
                            $.ajax({
                                url     	: 'formularios/generados/PHP_Ejecutar.php?action=ADD_MUESTRA&campana_crm=<?php echo $_GET['id_campana_crm'];?>',
                                type		: 'post',
                                data        : {id:id},
                                success 	: function(data){
                                    <?php if(isset($_GET['token'])) { ?>
                                        guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>',id,ObjDataGestion);
                                        $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal ani"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>');
                                    <?php } ?>
                                }
                            });
                        }else{
                            <?php if(isset($_GET['token'])) { ?>
                                guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>',id,ObjDataGestion);
                                $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ id +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal ani"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>');
                            <?php } ?>                        
                        }
                    }else{
                        $("#buscador").show();
                        $("#botones").show();
                        $("#resulados").show();
                    }
                });
            });    
    }
    
	function adicionarRegistro(tipo,mensaje){
        var ObjDataGestion= new Object();
        ObjDataGestion.server         = '<?php echo $http; ?>';
        ObjDataGestion.canal          = '<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal bq_manual"; }?>';
        ObjDataGestion.token          = '<?php echo $_GET["token"];?>';
        ObjDataGestion.predictiva     = '<?php if(isset($_GET['predictiva'])) { echo $_GET['predictiva']; } else{ echo 'null'; }?>';
        ObjDataGestion.consinte       = '<?php if(isset($_GET['consinte'])) { echo $_GET['consinte']; } else{ echo 'null'; }?>';
        ObjDataGestion.id_campana_crm = '<?php if(isset($_GET['id_campana_crm'])) { echo $_GET['id_campana_crm']; } else{ echo 'null'; }?>';
        ObjDataGestion.sentido        = '<?php if(isset($_GET['sentido'])) { echo $_GET['sentido']; } else{ echo 'null'; }?>';
    
        if(mensaje == null ){
            if(tipo == "1"){
                mensaje = "Desea adicionar un registro";
            }
            if(tipo == "2"){
                mensaje = "No se encontraron datos, desea adicionar un registro";
            }
            swal({
                html : true,
                title: "Información - Dyalogo CRM",
                text: mensaje,
                type: "warning",
                 confirmButtonText: "Adicionar registro",
                cancelButtonText : "Hacer otra busqueda",
                showCancelButton : true,
                closeOnConfirm : true
            },
            function(isconfirm){
                $("#buscador").hide();
                $("#botones").hide();
                $("#resulados").hide();
                if(isconfirm){
                    //JDBD - obtenemos los valores de la busqueda manual para enviarselos al formulario
                    var G1855_C30592 = $("#G1852_C30590").val();
			var G1856_C30584 = $("#G1852_C30590").val();
			var G1856_C30570 = $("#G1852_C30534").val();
			var G1855_C30556 = $("#G1852_C30534").val();
			var G1856_C30573 = $("#G1852_C30537").val();
			var G1855_C30559 = $("#G1852_C30537").val();
			var G1856_C30581 = $("#G1852_C30537").val();
			var G1856_C30574 = $("#G1852_C30538").val();
			var G1855_C30560 = $("#G1852_C30538").val();
                    $.ajax({
                    url     	: 'formularios/generados/PHP_Ejecutar.php?action=ADD&campana_crm=<?php echo $_GET['id_campana_crm'];?>',
                    type		: 'post',
                    dataType	: 'json',
                    success 	: function(numeroIdnuevo){
                        <?php if(isset($_GET['token'])){ ?>
                        guardarStorage('<?php echo $_GET["id_gestion_cbx"]?>',numeroIdnuevo,ObjDataGestion);
                        $("#frameContenedor").attr('src', '<?php echo $http ;?>/crm_php/Estacion_contact_center.php?campan=true&user='+ numeroIdnuevo +'&view=si&canal=<?php if (isset($_GET["canal"])) { echo $_GET["canal"]; }else{ echo "sin canal ani"; } ?>&token=<?php echo $_GET["token"];?>&id_gestion_cbx=<?php echo $_GET["id_gestion_cbx"];?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva']; }?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_campana_crm'])) { echo "&campana_crm=".$_GET['id_campana_crm']; }?><?php if(isset($_GET['sentido'])) { echo "&sentido=".$_GET['sentido']; }?>&nuevoregistro=true&G1855_C30592='+G1855_C30592+'&G1856_C30584='+G1856_C30584+'&G1856_C30570='+G1856_C30570+'&G1855_C30556='+G1855_C30556+'&G1856_C30573='+G1856_C30573+'&G1855_C30559='+G1855_C30559+'&G1856_C30581='+G1856_C30581+'&G1856_C30574='+G1856_C30574+'&G1855_C30560='+G1855_C30560);
                        <?php } ?>
                    }
                });
                }else{
                    limpiar();
                }
            });
        }else{
            swal("", mensaje, "warning");
        }
	}
	function limpiar(){
		$("#buscador").show();
		$("#buscador :input").each(function(){
			$(this).val("");
		});
		$("#resultadosBusqueda").html("");
		$("#botones").show();
		$("#resulados").show();
	}
</script>
