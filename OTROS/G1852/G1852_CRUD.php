<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (isset($_GET["adjunto"])) {
            $archivo=$_GET["adjunto"];
            $extensiones = explode(".", $archivo);
            // $ext = ["pdf","jpeg","jpg","png"];
            // if (in_array(end($extensiones),$ext)) {
                if (is_file("/Dyalogo/tmp/G1852/".$archivo)) {
                    $size = strlen("/Dyalogo/tmp/G1852/".$archivo);

                    if ($size>0) {
                        $nombre=basename("/Dyalogo/tmp/G1852/".$archivo);
                        $tamaño = filesize("/Dyalogo/tmp/G1852/".$archivo);
                        header("Content-Description: File Transfer");
                        header("Content-type: application/force-download");
                        header("Content-disposition: attachment; filename=".$nombre);
                        header("Content-Transfer-Encoding: binary");
                        header("Expires: 0");
                        header("Cache-Control: must-revalidate");
                        header("Pragma: public");
                        header("Content-Length: " . $tamaño);
                        ob_clean();
                        flush();
                        readfile("/Dyalogo/tmp/G1852/".$archivo);
                    }            
                }else{ 
                    // header("Location:" . getenv("HTTP_REFERER"));
                    echo "<h1>EL ARCHIVO NO EXISTE O LO HAN BORRADO...<br> !!VUELVE ATRAS</h1>";
                }            
            // }else{
                // header("Location:" . getenv("HTTP_REFERER"));
                // echo "Este archivo no se puede descargar.";
            // }

    }
        
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1852_ConsInte__b, G1852_FechaInsercion , G1852_Usuario ,  G1852_CodigoMiembro  , G1852_PoblacionOrigen , G1852_EstadoDiligenciamiento ,  G1852_IdLlamada , G1852_C30590 as principal ,G1852_C30590,G1852_C30591,G1852_C30532,G1852_C30533,G1852_C30534,G1852_C30535,G1852_C30536,G1852_C30537,G1852_C30538,G1852_C30539,G1852_C30542,G1852_C30601,G1852_C30523,G1852_C30524,G1852_C30525 FROM '.$BaseDatos.'.G1852 WHERE G1852_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1852_C30590'] = $key->G1852_C30590;

                $datos[$i]['G1852_C30591'] = $key->G1852_C30591;

                $datos[$i]['G1852_C30532'] = $key->G1852_C30532;

                $datos[$i]['G1852_C30533'] = $key->G1852_C30533;

                $datos[$i]['G1852_C30534'] = $key->G1852_C30534;

                $datos[$i]['G1852_C30535'] = $key->G1852_C30535;

                $datos[$i]['G1852_C30536'] = $key->G1852_C30536;

                $datos[$i]['G1852_C30537'] = explode(' ', $key->G1852_C30537)[0];
  
                $hora = '';
                if(!is_null($key->G1852_C30538)){
                    $hora = explode(' ', $key->G1852_C30538)[1];
                }

                $datos[$i]['G1852_C30538'] = $hora;

                $datos[$i]['G1852_C30539'] = $key->G1852_C30539;

                $datos[$i]['G1852_C30542'] = $key->G1852_C30542;

                $datos[$i]['G1852_C30601'] = $key->G1852_C30601;

                $datos[$i]['G1852_C30523'] = $key->G1852_C30523;

                $datos[$i]['G1852_C30524'] = $key->G1852_C30524;

                $datos[$i]['G1852_C30525'] = $key->G1852_C30525;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1852";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1852_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1852_ConsInte__b as id,  G1852_C30590 as camp1 , G1852_C30591 as camp2 
                     FROM ".$BaseDatos.".G1852  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1852_ConsInte__b as id,  G1852_C30590 as camp1 , G1852_C30591 as camp2  
                    FROM ".$BaseDatos.".G1852  JOIN ".$BaseDatos.".G1852_M".$_POST['muestra']." ON G1852_ConsInte__b = G1852_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1852_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1852_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1852_C30590 LIKE '%".$B."%' OR G1852_C30591 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1852_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1852");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1852_ConsInte__b, G1852_FechaInsercion , G1852_Usuario ,  G1852_CodigoMiembro  , G1852_PoblacionOrigen , G1852_EstadoDiligenciamiento ,  G1852_IdLlamada , G1852_C30590 as principal ,G1852_C30590,G1852_C30591,G1852_C30532,G1852_C30533,G1852_C30534,G1852_C30535,G1852_C30536,G1852_C30537,G1852_C30538, a.LISOPC_Nombre____b as G1852_C30539,G1852_C30542,G1852_C30601,G1852_C30523,G1852_C30524, b.LISOPC_Nombre____b as G1852_C30525 FROM '.$BaseDatos.'.G1852 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1852_C30539 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1852_C30525';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1852_C30538)){
                    $hora_a = explode(' ', $fila->G1852_C30538)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1852_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1852_ConsInte__b , ($fila->G1852_C30590) , ($fila->G1852_C30591) , ($fila->G1852_C30532) , ($fila->G1852_C30533) , ($fila->G1852_C30534) , ($fila->G1852_C30535) , ($fila->G1852_C30536) , explode(' ', $fila->G1852_C30537)[0] , $hora_a , ($fila->G1852_C30539) , ($fila->G1852_C30542) , ($fila->G1852_C30601) , ($fila->G1852_C30523) , ($fila->G1852_C30524) , ($fila->G1852_C30525) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1852 WHERE G1852_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1852";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1852_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1852_ConsInte__b as id,  G1852_C30590 as camp1 , G1852_C30591 as camp2  FROM '.$BaseDatos.'.G1852 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1852_ConsInte__b as id,  G1852_C30590 as camp1 , G1852_C30591 as camp2  
                    FROM ".$BaseDatos.".G1852  JOIN ".$BaseDatos.".G1852_M".$_POST['muestra']." ON G1852_ConsInte__b = G1852_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1852_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1852_C30590 LIKE "%'.$B.'%" OR G1852_C30591 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1852_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1852 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1852(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1852_C30590"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1852_C30590 = '".$_POST["G1852_C30590"]."'";
                $LsqlI .= $separador."G1852_C30590";
                $LsqlV .= $separador."'".$_POST["G1852_C30590"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1852_C30591"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1852_C30591 = '".$_POST["G1852_C30591"]."'";
                $LsqlI .= $separador."G1852_C30591";
                $LsqlV .= $separador."'".$_POST["G1852_C30591"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1852_C30532"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1852_C30532 = '".$_POST["G1852_C30532"]."'";
                $LsqlI .= $separador."G1852_C30532";
                $LsqlV .= $separador."'".$_POST["G1852_C30532"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1852_C30533"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1852_C30533 = '".$_POST["G1852_C30533"]."'";
                $LsqlI .= $separador."G1852_C30533";
                $LsqlV .= $separador."'".$_POST["G1852_C30533"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1852_C30534"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1852_C30534 = '".$_POST["G1852_C30534"]."'";
                $LsqlI .= $separador."G1852_C30534";
                $LsqlV .= $separador."'".$_POST["G1852_C30534"]."'";
                $validar = 1;
            }
             
  
            $G1852_C30535 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1852_C30535"])){
                if($_POST["G1852_C30535"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1852_C30535 = $_POST["G1852_C30535"];
                    $LsqlU .= $separador." G1852_C30535 = ".$G1852_C30535."";
                    $LsqlI .= $separador." G1852_C30535";
                    $LsqlV .= $separador.$G1852_C30535;
                    $validar = 1;
                }
            }
  
            $G1852_C30536 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1852_C30536"])){
                if($_POST["G1852_C30536"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1852_C30536 = $_POST["G1852_C30536"];
                    $LsqlU .= $separador." G1852_C30536 = ".$G1852_C30536."";
                    $LsqlI .= $separador." G1852_C30536";
                    $LsqlV .= $separador.$G1852_C30536;
                    $validar = 1;
                }
            }
 
            $G1852_C30537 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1852_C30537"])){    
                if($_POST["G1852_C30537"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1852_C30537"]);
                    if(count($tieneHora) > 1){
                        $G1852_C30537 = "'".$_POST["G1852_C30537"]."'";
                    }else{
                        $G1852_C30537 = "'".str_replace(' ', '',$_POST["G1852_C30537"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1852_C30537 = ".$G1852_C30537;
                    $LsqlI .= $separador." G1852_C30537";
                    $LsqlV .= $separador.$G1852_C30537;
                    $validar = 1;
                }
            }
  
            $G1852_C30538 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1852_C30538"])){   
                if($_POST["G1852_C30538"] != '' && $_POST["G1852_C30538"] != 'undefined' && $_POST["G1852_C30538"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1852_C30538 = "'".$fecha." ".str_replace(' ', '',$_POST["G1852_C30538"])."'";
                    $LsqlU .= $separador." G1852_C30538 = ".$G1852_C30538."";
                    $LsqlI .= $separador." G1852_C30538";
                    $LsqlV .= $separador.$G1852_C30538;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1852_C30539"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1852_C30539 = '".$_POST["G1852_C30539"]."'";
                $LsqlI .= $separador."G1852_C30539";
                $LsqlV .= $separador."'".$_POST["G1852_C30539"]."'";
                $validar = 1;
            }
             
  
            $G1852_C30542 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G1852_C30542"])){
                if($_POST["G1852_C30542"] == 'Yes'){
                    $G1852_C30542 = 1;
                }else if($_POST["G1852_C30542"] == 'off'){
                    $G1852_C30542 = 0;
                }else if($_POST["G1852_C30542"] == 'on'){
                    $G1852_C30542 = 1;
                }else if($_POST["G1852_C30542"] == 'No'){
                    $G1852_C30542 = 1;
                }else{
                    $G1852_C30542 = $_POST["G1852_C30542"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G1852_C30542 = ".$G1852_C30542."";
                $LsqlI .= $separador." G1852_C30542";
                $LsqlV .= $separador.$G1852_C30542;

                $validar = 1;
            }
  
            if (isset($_FILES["FG1852_C30601"]["tmp_name"])) {
                $destinoFile = "/Dyalogo/tmp/G1852/";
                $fechUp = date("Y-m-d_H:i:s");
                if (!file_exists("/Dyalogo/tmp/G1852")){
                    mkdir("/Dyalogo/tmp/G1852", 0777);
                }
                if ($_FILES["FG1852_C30601"]["size"] != 0) {
                    $G1852_C30601 = $_FILES["FG1852_C30601"]["tmp_name"];
                    $nG1852_C30601 = $fechUp."_".$_FILES["FG1852_C30601"]["name"];
                    $rutaFinal = $destinoFile.$nG1852_C30601;
                    if (is_uploaded_file($G1852_C30601)) {
                        move_uploaded_file($G1852_C30601, $rutaFinal);
                    }

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1852_C30601 = '".$nG1852_C30601."'";
                    $LsqlI .= $separador."G1852_C30601";
                    $LsqlV .= $separador."'".$nG1852_C30601."'";
                    $validar = 1;
                }
            }
            
  
            if(isset($_POST["G1852_C30523"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1852_C30523 = '".$_POST["G1852_C30523"]."'";
                $LsqlI .= $separador."G1852_C30523";
                $LsqlV .= $separador."'".$_POST["G1852_C30523"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1852_C30524"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1852_C30524 = '".$_POST["G1852_C30524"]."'";
                $LsqlI .= $separador."G1852_C30524";
                $LsqlV .= $separador."'".$_POST["G1852_C30524"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1852_C30525"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1852_C30525 = '".$_POST["G1852_C30525"]."'";
                $LsqlI .= $separador."G1852_C30525";
                $LsqlV .= $separador."'".$_POST["G1852_C30525"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1852_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1852_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1852_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1852_Usuario , G1852_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1852_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1852 WHERE G1852_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G1852 SET G1852_UltiGest__b =-14, G1852_GesMasImp_b =-14, G1852_TipoReintentoUG_b =0, G1852_TipoReintentoGMI_b =0, G1852_EstadoUG_b =-14, G1852_EstadoGMI_b =-14, G1852_CantidadIntentos =0, G1852_CantidadIntentosGMI_b =0 WHERE G1852_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
