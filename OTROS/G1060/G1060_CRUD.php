<?php
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1060_ConsInte__b, G1060_FechaInsercion , G1060_Usuario ,  G1060_CodigoMiembro  , G1060_PoblacionOrigen , G1060_EstadoDiligenciamiento ,  G1060_IdLlamada , G1060_C15234 as principal ,G1060_C15260,G1060_C15238,G1060_C15234,G1060_C15233,G1060_C15239,G1060_C15240,G1060_C15241,G1060_C15235,G1060_C15236,G1060_C15242,G1060_C15243,G1060_C15244,G1060_C15237,G1060_C15245,G1060_C15261,G1060_C15247,G1060_C15248,G1060_C15249,G1060_C15250,G1060_C15251,G1060_C15252,G1060_C15253,G1060_C15254,G1060_C15255,G1060_C15377,G1060_C15378,G1060_C15379 FROM '.$BaseDatos.'.G1060 WHERE G1060_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1060_C15260'] = explode(' ', $key->G1060_C15260)[0];

                $datos[$i]['G1060_C15238'] = $key->G1060_C15238;

                $datos[$i]['G1060_C15234'] = $key->G1060_C15234;

                $datos[$i]['G1060_C15233'] = $key->G1060_C15233;

                $datos[$i]['G1060_C15239'] = $key->G1060_C15239;

                $datos[$i]['G1060_C15240'] = $key->G1060_C15240;

                $datos[$i]['G1060_C15241'] = $key->G1060_C15241;

                $datos[$i]['G1060_C15235'] = $key->G1060_C15235;

                $datos[$i]['G1060_C15236'] = $key->G1060_C15236;

                $datos[$i]['G1060_C15242'] = $key->G1060_C15242;

                $datos[$i]['G1060_C15243'] = $key->G1060_C15243;

                $datos[$i]['G1060_C15244'] = $key->G1060_C15244;

                $datos[$i]['G1060_C15237'] = $key->G1060_C15237;

                $datos[$i]['G1060_C15245'] = $key->G1060_C15245;

                $datos[$i]['G1060_C15261'] = $key->G1060_C15261;

                $datos[$i]['G1060_C15247'] = $key->G1060_C15247;

                $datos[$i]['G1060_C15248'] = $key->G1060_C15248;

                $datos[$i]['G1060_C15249'] = explode(' ', $key->G1060_C15249)[0];
  
                $hora = '';
                if(!is_null($key->G1060_C15250)){
                    $hora = explode(' ', $key->G1060_C15250)[1];
                }

                $datos[$i]['G1060_C15250'] = $hora;

                $datos[$i]['G1060_C15251'] = $key->G1060_C15251;

                $datos[$i]['G1060_C15252'] = $key->G1060_C15252;

                $datos[$i]['G1060_C15253'] = $key->G1060_C15253;

                $datos[$i]['G1060_C15254'] = $key->G1060_C15254;

                $datos[$i]['G1060_C15255'] = $key->G1060_C15255;

                $datos[$i]['G1060_C15377'] = $key->G1060_C15377;

                $datos[$i]['G1060_C15378'] = $key->G1060_C15378;

                $datos[$i]['G1060_C15379'] = $key->G1060_C15379;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G1060_ConsInte__b as id,  G1060_C15234 as camp1 , G1060_C15233 as camp2 ";
            $Lsql .= " FROM ".$BaseDatos.".G1060 ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " WHERE G1060_C15234 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G1060_C15233 like '%".$_POST['Busqueda']."%' ";
            }
            $Lsql .= " ORDER BY G1060_ConsInte__b DESC LIMIT 0, 50 "; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
	    	$Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
	    	$res = $mysqli->query($Lsql);
	    	echo "<option value='0'>Seleccione</option>";
	    	while($key = $res->fetch_object()){
	    		echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
	    	}
	    }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1060");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1060_ConsInte__b, G1060_FechaInsercion , G1060_Usuario ,  G1060_CodigoMiembro  , G1060_PoblacionOrigen , G1060_EstadoDiligenciamiento ,  G1060_IdLlamada , G1060_C15234 as principal ,G1060_C15260,G1060_C15238,G1060_C15234,G1060_C15233,G1060_C15239,G1060_C15240,G1060_C15241,G1060_C15235,G1060_C15236,G1060_C15242,G1060_C15243,G1060_C15244,G1060_C15237,G1060_C15245,G1060_C15261, a.LISOPC_Nombre____b as G1060_C15247, b.LISOPC_Nombre____b as G1060_C15248,G1060_C15249,G1060_C15250,G1060_C15251,G1060_C15252,G1060_C15253,G1060_C15254,G1060_C15255, c.LISOPC_Nombre____b as G1060_C15377, d.LISOPC_Nombre____b as G1060_C15378, e.LISOPC_Nombre____b as G1060_C15379 FROM '.$BaseDatos.'.G1060 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1060_C15247 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1060_C15248 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1060_C15377 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1060_C15378 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1060_C15379';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1060_C15250)){
                    $hora_a = explode(' ', $fila->G1060_C15250)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1060_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1060_ConsInte__b , explode(' ', $fila->G1060_C15260)[0] , ($fila->G1060_C15238) , ($fila->G1060_C15234) , ($fila->G1060_C15233) , ($fila->G1060_C15239) , ($fila->G1060_C15240) , ($fila->G1060_C15241) , ($fila->G1060_C15235) , ($fila->G1060_C15236) , ($fila->G1060_C15242) , ($fila->G1060_C15243) , ($fila->G1060_C15244) , ($fila->G1060_C15237) , ($fila->G1060_C15245) , ($fila->G1060_C15261) , ($fila->G1060_C15247) , ($fila->G1060_C15248) , explode(' ', $fila->G1060_C15249)[0] , $hora_a , ($fila->G1060_C15251) , ($fila->G1060_C15252) , ($fila->G1060_C15253) , ($fila->G1060_C15254) , ($fila->G1060_C15255) , ($fila->G1060_C15377) , ($fila->G1060_C15378) , ($fila->G1060_C15379) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1060 WHERE G1060_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G1060_ConsInte__b as id,  G1060_C15234 as camp1 , G1060_C15233 as camp2  FROM '.$BaseDatos.'.G1060 ORDER BY G1060_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1060 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1060(";
            $LsqlUato = "UPDATE ".$BaseDatos.".G1007 SET ";
            $LsqlUcadu = "UPDATE ".$BaseDatos.".G1061 SET ";
            $LsqlV = " VALUES ("; 
 
            $G1060_C15260 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1060_C15260"])){    
                if($_POST["G1060_C15260"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1060_C15260"]);
                    if(count($tieneHora) > 1){
                    	$G1060_C15260 = "'".$_POST["G1060_C15260"]."'";
                    }else{
                    	$G1060_C15260 = "'".str_replace(' ', '',$_POST["G1060_C15260"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1060_C15260 = ".$G1060_C15260;
                    $LsqlI .= $separador." G1060_C15260";
                    $LsqlV .= $separador.$G1060_C15260;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1060_C15238"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15238 = '".$_POST["G1060_C15238"]."'";
                $LsqlI .= $separador."G1060_C15238";
                $LsqlV .= $separador."'".$_POST["G1060_C15238"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15234"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15234 = '".$_POST["G1060_C15234"]."'";
                $LsqlI .= $separador."G1060_C15234";
                $LsqlV .= $separador."'".$_POST["G1060_C15234"]."'";
                $LsqlUcadu .="G1061_C15267 = '".$_POST["G1060_C15234"]."'";
                $LsqlUato .="G1007_C14027 = '".$_POST["G1060_C15234"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15233"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15233 = '".$_POST["G1060_C15233"]."'";
                $LsqlI .= $separador."G1060_C15233";
                $LsqlV .= $separador."'".$_POST["G1060_C15233"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15239"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15239 = '".$_POST["G1060_C15239"]."'";
                $LsqlI .= $separador."G1060_C15239";
                $LsqlV .= $separador."'".$_POST["G1060_C15239"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15240"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15240 = '".$_POST["G1060_C15240"]."'";
                $LsqlI .= $separador."G1060_C15240";
                $LsqlV .= $separador."'".$_POST["G1060_C15240"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15241"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15241 = '".$_POST["G1060_C15241"]."'";
                $LsqlI .= $separador."G1060_C15241";
                $LsqlV .= $separador."'".$_POST["G1060_C15241"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15235"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15235 = '".$_POST["G1060_C15235"]."'";
                $LsqlI .= $separador."G1060_C15235";
                $LsqlV .= $separador."'".$_POST["G1060_C15235"]."'";
                $LsqlUcadu .= $separador."G1061_C15269 = '".$_POST["G1060_C15235"]."'";
                $LsqlUato .= $separador."G1007_C14028 = '".$_POST["G1060_C15235"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15236"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15236 = '".$_POST["G1060_C15236"]."'";
                $LsqlI .= $separador."G1060_C15236";
                $LsqlV .= $separador."'".$_POST["G1060_C15236"]."'";
                $LsqlUcadu .= $separador."G1061_C15270 = '".$_POST["G1060_C15236"]."'";
                $LsqlUato .= $separador."G1007_C14029 = '".$_POST["G1060_C15236"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15242"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15242 = '".$_POST["G1060_C15242"]."'";
                $LsqlI .= $separador."G1060_C15242";
                $LsqlV .= $separador."'".$_POST["G1060_C15242"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15243"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15243 = '".$_POST["G1060_C15243"]."'";
                $LsqlI .= $separador."G1060_C15243";
                $LsqlV .= $separador."'".$_POST["G1060_C15243"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15244"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15244 = '".$_POST["G1060_C15244"]."'";
                $LsqlI .= $separador."G1060_C15244";
                $LsqlV .= $separador."'".$_POST["G1060_C15244"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15237"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15237 = '".$_POST["G1060_C15237"]."'";
                $LsqlI .= $separador."G1060_C15237";
                $LsqlV .= $separador."'".$_POST["G1060_C15237"]."'";
                $LsqlUcadu .= $separador."G1061_C15271 = '".$_POST["G1060_C15237"]."'";
                $LsqlUato .= $separador."G1007_C14030 = '".$_POST["G1060_C15237"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15245"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15245 = '".$_POST["G1060_C15245"]."'";
                $LsqlI .= $separador."G1060_C15245";
                $LsqlV .= $separador."'".$_POST["G1060_C15245"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15261"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15261 = '".$_POST["G1060_C15261"]."'";
                $LsqlI .= $separador."G1060_C15261";
                $LsqlV .= $separador."'".$_POST["G1060_C15261"]."'";
                $validar = 1;
            }
             
 
            $G1060_C15247 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1060_C15247 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1060_C15247 = ".$G1060_C15247;
                    $LsqlI .= $separador." G1060_C15247";
                    $LsqlV .= $separador.$G1060_C15247;
                    $validar = 1;

                    
                }
            }
 
            $G1060_C15248 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1060_C15248 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1060_C15248 = ".$G1060_C15248;
                    $LsqlI .= $separador." G1060_C15248";
                    $LsqlV .= $separador.$G1060_C15248;
                    $validar = 1;
                }
            }
 
            $G1060_C15249 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1060_C15249 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1060_C15249 = ".$G1060_C15249;
                    $LsqlI .= $separador." G1060_C15249";
                    $LsqlV .= $separador.$G1060_C15249;
                    $validar = 1;
                }
            }
 
            $G1060_C15250 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1060_C15250 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1060_C15250 = ".$G1060_C15250;
                    $LsqlI .= $separador." G1060_C15250";
                    $LsqlV .= $separador.$G1060_C15250;
                    $validar = 1;
                }
            }
 
            $G1060_C15251 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1060_C15251 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1060_C15251 = ".$G1060_C15251;
                    $LsqlI .= $separador." G1060_C15251";
                    $LsqlV .= $separador.$G1060_C15251;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1060_C15252"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15252 = '".$_POST["G1060_C15252"]."'";
                $LsqlI .= $separador."G1060_C15252";
                $LsqlV .= $separador."'".$_POST["G1060_C15252"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15253"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15253 = '".$_POST["G1060_C15253"]."'";
                $LsqlI .= $separador."G1060_C15253";
                $LsqlV .= $separador."'".$_POST["G1060_C15253"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15254"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15254 = '".$_POST["G1060_C15254"]."'";
                $LsqlI .= $separador."G1060_C15254";
                $LsqlV .= $separador."'".$_POST["G1060_C15254"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15255"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15255 = '".$_POST["G1060_C15255"]."'";
                $LsqlI .= $separador."G1060_C15255";
                $LsqlV .= $separador."'".$_POST["G1060_C15255"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15256"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15256 = '".$_POST["G1060_C15256"]."'";
                $LsqlI .= $separador."G1060_C15256";
                $LsqlV .= $separador."'".$_POST["G1060_C15256"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15257"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15257 = '".$_POST["G1060_C15257"]."'";
                $LsqlI .= $separador."G1060_C15257";
                $LsqlV .= $separador."'".$_POST["G1060_C15257"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15342"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15342 = '".$_POST["G1060_C15342"]."'";
                $LsqlI .= $separador."G1060_C15342";
                $LsqlV .= $separador."'".$_POST["G1060_C15342"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15343"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15343 = '".$_POST["G1060_C15343"]."'";
                $LsqlI .= $separador."G1060_C15343";
                $LsqlV .= $separador."'".$_POST["G1060_C15343"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15344"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15344 = '".$_POST["G1060_C15344"]."'";
                $LsqlI .= $separador."G1060_C15344";
                $LsqlV .= $separador."'".$_POST["G1060_C15344"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15345"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15345 = '".$_POST["G1060_C15345"]."'";
                $LsqlI .= $separador."G1060_C15345";
                $LsqlV .= $separador."'".$_POST["G1060_C15345"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15346"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15346 = '".$_POST["G1060_C15346"]."'";
                $LsqlI .= $separador."G1060_C15346";
                $LsqlV .= $separador."'".$_POST["G1060_C15346"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15347"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15347 = '".$_POST["G1060_C15347"]."'";
                $LsqlI .= $separador."G1060_C15347";
                $LsqlV .= $separador."'".$_POST["G1060_C15347"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15377"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15377 = '".$_POST["G1060_C15377"]."'";
                $LsqlI .= $separador."G1060_C15377";
                $LsqlV .= $separador."'".$_POST["G1060_C15377"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15378"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15378 = '".$_POST["G1060_C15378"]."'";
                $LsqlI .= $separador."G1060_C15378";
                $LsqlV .= $separador."'".$_POST["G1060_C15378"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1060_C15379"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_C15379 = '".$_POST["G1060_C15379"]."'";
                $LsqlI .= $separador."G1060_C15379";
                $LsqlV .= $separador."'".$_POST["G1060_C15379"]."'";
                $validar = 1;
            }
             

			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1060_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1060_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
			}


			$padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1060_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
			if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1060_Usuario , G1060_FechaInsercion, G1060_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1060_ConsInte__b =".$_POST["id"]; 
                    if (isset($_POST["G1060_C15233"]) && $_POST["G1060_C15233"]!= 0 && $_POST["G1060_C15233"]!=null) {
                        $LsqlUpAtora = $LsqlUato." WHERE G1007_C14026 = ".$_POST["G1060_C15233"];
                        $LsqlUpCaduca = $LsqlUcadu." WHERE G1061_C15266 = ".$_POST["G1060_C15233"];
                        $uAtora = $mysqli->query($LsqlUpAtora);
                        $uCaduca = $mysqli->query($LsqlUpCaduca);
                    }
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1060 WHERE G1060_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {

                    if($_POST["oper"] == 'add' ){

                    	$UltimoID = $mysqli->insert_id;

                        //JDBD - Insertamos la gestion en el subformulario.
                        $strSQLInsertG1101_t = "INSERT INTO ".$BaseDatos.".G1101 (G1101_C16729,G1101_C16719,G1101_C16716,G1101_C16723,G1101_C16722,G1101_C30105,G1101_C16715,G1101_C16717,G1101_C16721,G1101_C16720,G1101_C16718) SELECT G1060_C15379,G1060_C15378,G1060_C15251,G1060_C15233,G1060_C15250,G1060_FechaInsercion,G1060_C15377,G1060_C15253,G1060_C15249,'FRIOS' AS campana,G1060_C15252 FROM ".$BaseDatos.".G1060 WHERE G1060_ConsInte__b = ".$UltimoID;

                        $mysqli->query($strSQLInsertG1101_t);

                    	echo $UltimoID;


                	}else{
                		if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                		echo "1";    		
                	}

                	

                } else {
                	echo '0';
                   // echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1101_ConsInte__b, G1101_C16723, b.LISOPC_Nombre____b as  G1101_C16715, c.LISOPC_Nombre____b as  G1101_C16719, d.LISOPC_Nombre____b as  G1101_C16729, G1101_C16717, G1101_C16718, G1101_C16716, G1101_C16720, G1101_C30105, G1101_C16721, G1101_C16722, G1101_C16730 FROM ".$BaseDatos.".G1101  LEFT JOIN ".$BaseDatos_systema.".LISOPC as b ON b.LISOPC_ConsInte__b =  G1101_C16715 LEFT JOIN ".$BaseDatos_systema.".LISOPC as c ON c.LISOPC_ConsInte__b =  G1101_C16719 LEFT JOIN ".$BaseDatos_systema.".LISOPC as d ON d.LISOPC_ConsInte__b =  G1101_C16729 ";

        $SQL .= " WHERE G1101_C16723 = '".$numero."'"; 

        $SQL .= " ORDER BY G1101_C16717 DESC";

        // echo $SQL; die();
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1101_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1101_ConsInte__b)."</cell>"; 
            

            echo "<cell>". ($fila->G1101_C16723)."</cell>";

            echo "<cell>". ($fila->G1101_C16715)."</cell>";

            echo "<cell>". ($fila->G1101_C16719)."</cell>";

            echo "<cell>". ($fila->G1101_C16729)."</cell>";

            if($fila->G1101_C16717 != ''){
                echo "<cell>". explode(' ', $fila->G1101_C16717)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1101_C30105 != ''){
                echo "<cell>". explode(' ', $fila->G1101_C30105)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". ($fila->G1101_C16718)."</cell>";

            echo "<cell><![CDATA[". ($fila->G1101_C16716)."]]></cell>";

            echo "<cell>". ($fila->G1101_C16720)."</cell>";

            if($fila->G1101_C16721 != ''){
                echo "<cell>". explode(' ', $fila->G1101_C16721)[0]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            if($fila->G1101_C16722 != ''){
                echo "<cell>". explode(' ', $fila->G1101_C16722)[1]."</cell>";
            }else{
                echo "<cell></cell>";
            }

            echo "<cell>". $fila->G1101_C16730."</cell>"; 
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1101 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1101(";
            $LsqlV = " VALUES ("; 
 
            if(isset($_POST["G1101_C16715"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16715 = '".$_POST["G1101_C16715"]."'";
                $LsqlI .= $separador."G1101_C16715";
                $LsqlV .= $separador."'".$_POST["G1101_C16715"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1101_C16719"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16719 = '".$_POST["G1101_C16719"]."'";
                $LsqlI .= $separador."G1101_C16719";
                $LsqlV .= $separador."'".$_POST["G1101_C16719"]."'";
                $validar = 1;
            }
 
            if(isset($_POST["G1101_C16729"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16729 = '".$_POST["G1101_C16729"]."'";
                $LsqlI .= $separador."G1101_C16729";
                $LsqlV .= $separador."'".$_POST["G1101_C16729"]."'";
                $validar = 1;
            }

            $G1101_C16717 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1101_C16717"])){    
                if($_POST["G1101_C16717"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1101_C16717 = "'".str_replace(' ', '',$_POST["G1101_C16717"])." 00:00:00'";
                    $LsqlU .= $separador." G1101_C16717 = ".$G1101_C16717;
                    $LsqlI .= $separador." G1101_C16717";
                    $LsqlV .= $separador.$G1101_C16717;
                    $validar = 1;
                }
            }
 
                                                                         
            if(isset($_POST["G1101_C16718"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16718 = '".$_POST["G1101_C16718"]."'";
                $LsqlI .= $separador."G1101_C16718";
                $LsqlV .= $separador."'".$_POST["G1101_C16718"]."'";
                $validar = 1;
            }
                                                                          
                                                                           
  

            if(isset($_POST["G1101_C16716"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16716 = '".$_POST["G1101_C16716"]."'";
                $LsqlI .= $separador."G1101_C16716";
                $LsqlV .= $separador."'".$_POST["G1101_C16716"]."'";
                $validar = 1;
            }
                                                                           
 
                                                                         
            if(isset($_POST["G1101_C16720"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1101_C16720 = '".$_POST["G1101_C16720"]."'";
                $LsqlI .= $separador."G1101_C16720";
                $LsqlV .= $separador."'".$_POST["G1101_C16720"]."'";
                $validar = 1;
            }
                                                                          
                                                                           

            $G1101_C16721 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1101_C16721"])){    
                if($_POST["G1101_C16721"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1101_C16721 = "'".str_replace(' ', '',$_POST["G1101_C16721"])." 00:00:00'";
                    $LsqlU .= $separador." G1101_C16721 = ".$G1101_C16721;
                    $LsqlI .= $separador." G1101_C16721";
                    $LsqlV .= $separador.$G1101_C16721;
                    $validar = 1;
                }
            }
 
            $G1101_C16722 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1101_C16722"])){    
                if($_POST["G1101_C16722"] != '' && $_POST["G1101_C16722"] != 'undefined' && $_POST["G1101_C16722"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1101_C16722 = "'".$fecha." ".str_replace(' ', '',$_POST["G1101_C16722"])."'";
                    $LsqlU .= $separador."  G1101_C16722 = ".$G1101_C16722."";
                    $LsqlI .= $separador."  G1101_C16722";
                    $LsqlV .= $separador.$G1101_C16722;
                    $validar = 1;
                }
            }
 
            $G1101_C16730= NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1101_C16730"])){    
                if($_POST["G1101_C16730"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1101_C16730 = $_POST["G1101_C16730"];
                    $LsqlU .= $separador." G1101_C16730 = '".$G1101_C16730."'";
                    $LsqlI .= $separador." G1101_C16730";
                    $LsqlV .= $separador."'".$G1101_C16730."'";
                    $validar = 1;
                }
            }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1101_C16723 = $numero;
                    $LsqlU .= ", G1101_C16723 = ".$G1101_C16723."";
                    $LsqlI .= ", G1101_C16723";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1101_Usuario ,  G1101_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1101_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1101 WHERE  G1101_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
