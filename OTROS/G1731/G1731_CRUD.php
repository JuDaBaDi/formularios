<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (isset($_GET["adjunto"])) {
            $archivo=$_GET["adjunto"];
            $extensiones = explode(".", $archivo);
            // $ext = ["pdf","jpeg","jpg","png"];
            // if (in_array(end($extensiones),$ext)) {
                if (is_file("/Dyalogo/tmp/G1731/".$archivo)) {
                    $size = strlen("/Dyalogo/tmp/G1731/".$archivo);

                    if ($size>0) {
                        $nombre=basename("/Dyalogo/tmp/G1731/".$archivo);
                        $tamaño = filesize("/Dyalogo/tmp/G1731/".$archivo);
                        header("Content-Description: File Transfer");
                        header("Content-type: application/force-download");
                        header("Content-disposition: attachment; filename=".$nombre);
                        header("Content-Transfer-Encoding: binary");
                        header("Expires: 0");
                        header("Cache-Control: must-revalidate");
                        header("Pragma: public");
                        header("Content-Length: " . $tamaño);
                        ob_clean();
                        flush();
                        readfile("/Dyalogo/tmp/G1731/".$archivo);
                    }            
                }else{ 
                    // header("Location:" . getenv("HTTP_REFERER"));
                    echo "<h1>EL ARCHIVO NO EXISTE O LO HAN BORRADO...<br> !!VUELVE ATRAS</h1>";
                }            
            // }else{
                // header("Location:" . getenv("HTTP_REFERER"));
                // echo "Este archivo no se puede descargar.";
            // }

    }
        
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1731_ConsInte__b, G1731_FechaInsercion , G1731_Usuario ,  G1731_CodigoMiembro  , G1731_PoblacionOrigen , G1731_EstadoDiligenciamiento ,  G1731_IdLlamada , G1731_C29086 as principal ,G1731_C29086,G1731_C29087,G1731_C29088,G1731_C29089,G1731_C29090,G1731_C29091,G1731_C29092,G1731_C29093,G1731_C30453,G1731_C30596,G1731_C29094,G1731_C29095,G1731_C29096,G1731_C29097,G1731_C29098,G1731_C29099,G1731_C29100,G1731_C29101,G1731_C29102 FROM '.$BaseDatos.'.G1731 WHERE G1731_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1731_C29086'] = explode(' ', $key->G1731_C29086)[0];

                $datos[$i]['G1731_C29087'] = $key->G1731_C29087;

                $datos[$i]['G1731_C29088'] = $key->G1731_C29088;

                $datos[$i]['G1731_C29089'] = $key->G1731_C29089;

                $datos[$i]['G1731_C29090'] = $key->G1731_C29090;

                $datos[$i]['G1731_C29091'] = $key->G1731_C29091;

                $datos[$i]['G1731_C29092'] = $key->G1731_C29092;

                $datos[$i]['G1731_C29093'] = $key->G1731_C29093;

                $datos[$i]['G1731_C30453'] = $key->G1731_C30453;

                $datos[$i]['G1731_C30596'] = $key->G1731_C30596;

                $datos[$i]['G1731_C29094'] = $key->G1731_C29094;

                $datos[$i]['G1731_C29095'] = $key->G1731_C29095;

                $datos[$i]['G1731_C29096'] = explode(' ', $key->G1731_C29096)[0];
  
                $hora = '';
                if(!is_null($key->G1731_C29097)){
                    $hora = explode(' ', $key->G1731_C29097)[1];
                }

                $datos[$i]['G1731_C29097'] = $hora;

                $datos[$i]['G1731_C29098'] = $key->G1731_C29098;

                $datos[$i]['G1731_C29099'] = $key->G1731_C29099;

                $datos[$i]['G1731_C29100'] = $key->G1731_C29100;

                $datos[$i]['G1731_C29101'] = $key->G1731_C29101;

                $datos[$i]['G1731_C29102'] = $key->G1731_C29102;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1731";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1731_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1731_ConsInte__b as id,  G1731_C29086 as camp1 , G1731_C29087 as camp2 
                     FROM ".$BaseDatos.".G1731  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1731_ConsInte__b as id,  G1731_C29086 as camp1 , G1731_C29087 as camp2  
                    FROM ".$BaseDatos.".G1731  JOIN ".$BaseDatos.".G1731_M".$_POST['muestra']." ON G1731_ConsInte__b = G1731_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1731_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1731_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1731_C29086 LIKE '%".$B."%' OR G1731_C29087 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1731_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1731");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1731_ConsInte__b, G1731_FechaInsercion , G1731_Usuario ,  G1731_CodigoMiembro  , G1731_PoblacionOrigen , G1731_EstadoDiligenciamiento ,  G1731_IdLlamada , G1731_C29086 as principal ,G1731_C29086,G1731_C29087,G1731_C29088,G1731_C29089,G1731_C29090, a.LISOPC_Nombre____b as G1731_C29091,G1731_C29092,G1731_C29093,G1731_C30453,G1731_C30596, b.LISOPC_Nombre____b as G1731_C29094, c.LISOPC_Nombre____b as G1731_C29095,G1731_C29096,G1731_C29097,G1731_C29098,G1731_C29099,G1731_C29100,G1731_C29101,G1731_C29102 FROM '.$BaseDatos.'.G1731 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1731_C29091 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1731_C29094 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1731_C29095';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1731_C29097)){
                    $hora_a = explode(' ', $fila->G1731_C29097)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1731_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1731_ConsInte__b , explode(' ', $fila->G1731_C29086)[0] , ($fila->G1731_C29087) , ($fila->G1731_C29088) , ($fila->G1731_C29089) , ($fila->G1731_C29090) , ($fila->G1731_C29091) , ($fila->G1731_C29092) , ($fila->G1731_C29093) , ($fila->G1731_C30453) , ($fila->G1731_C30596) , ($fila->G1731_C29094) , ($fila->G1731_C29095) , explode(' ', $fila->G1731_C29096)[0] , $hora_a , ($fila->G1731_C29098) , ($fila->G1731_C29099) , ($fila->G1731_C29100) , ($fila->G1731_C29101) , ($fila->G1731_C29102) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1731 WHERE G1731_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1731";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1731_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1731_ConsInte__b as id,  G1731_C29086 as camp1 , G1731_C29087 as camp2  FROM '.$BaseDatos.'.G1731 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1731_ConsInte__b as id,  G1731_C29086 as camp1 , G1731_C29087 as camp2  
                    FROM ".$BaseDatos.".G1731  JOIN ".$BaseDatos.".G1731_M".$_POST['muestra']." ON G1731_ConsInte__b = G1731_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1731_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1731_C29086 LIKE "%'.$B.'%" OR G1731_C29087 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1731_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1731 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1731(";
            $LsqlV = " VALUES ("; 
 
            $G1731_C29086 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1731_C29086"])){    
                if($_POST["G1731_C29086"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1731_C29086"]);
                    if(count($tieneHora) > 1){
                        $G1731_C29086 = "'".$_POST["G1731_C29086"]."'";
                    }else{
                        $G1731_C29086 = "'".str_replace(' ', '',$_POST["G1731_C29086"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1731_C29086 = ".$G1731_C29086;
                    $LsqlI .= $separador." G1731_C29086";
                    $LsqlV .= $separador.$G1731_C29086;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1731_C29087"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1731_C29087 = '".$_POST["G1731_C29087"]."'";
                $LsqlI .= $separador."G1731_C29087";
                $LsqlV .= $separador."'".$_POST["G1731_C29087"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1731_C29088"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1731_C29088 = '".$_POST["G1731_C29088"]."'";
                $LsqlI .= $separador."G1731_C29088";
                $LsqlV .= $separador."'".$_POST["G1731_C29088"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1731_C29089"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1731_C29089 = '".$_POST["G1731_C29089"]."'";
                $LsqlI .= $separador."G1731_C29089";
                $LsqlV .= $separador."'".$_POST["G1731_C29089"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1731_C29090"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1731_C29090 = '".$_POST["G1731_C29090"]."'";
                $LsqlI .= $separador."G1731_C29090";
                $LsqlV .= $separador."'".$_POST["G1731_C29090"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1731_C29091"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1731_C29091 = '".$_POST["G1731_C29091"]."'";
                $LsqlI .= $separador."G1731_C29091";
                $LsqlV .= $separador."'".$_POST["G1731_C29091"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1731_C29092"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1731_C29092 = '".$_POST["G1731_C29092"]."'";
                $LsqlI .= $separador."G1731_C29092";
                $LsqlV .= $separador."'".$_POST["G1731_C29092"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1731_C29093"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1731_C29093 = '".$_POST["G1731_C29093"]."'";
                $LsqlI .= $separador."G1731_C29093";
                $LsqlV .= $separador."'".$_POST["G1731_C29093"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1731_C30453"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1731_C30453 = '".$_POST["G1731_C30453"]."'";
                $LsqlI .= $separador."G1731_C30453";
                $LsqlV .= $separador."'".$_POST["G1731_C30453"]."'";
                $validar = 1;
            }
             
  
            if (isset($_FILES["FG1731_C30596"]["tmp_name"])) {
                $destinoFile = "/Dyalogo/tmp/G1731/";
                $fechUp = date("Y-m-d_H:i:s");
                if (!file_exists("/Dyalogo/tmp/G1731")){
                    mkdir("/Dyalogo/tmp/G1731", 0777);
                }
                if ($_FILES["FG1731_C30596"]["size"] != 0) {
                    $G1731_C30596 = $_FILES["FG1731_C30596"]["tmp_name"];
                    $nG1731_C30596 = $fechUp."_".$_FILES["FG1731_C30596"]["name"];
                    $rutaFinal = $destinoFile.$nG1731_C30596;
                    if (is_uploaded_file($G1731_C30596)) {
                        move_uploaded_file($G1731_C30596, $rutaFinal);
                    }

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1731_C30596 = '".$nG1731_C30596."'";
                    $LsqlI .= $separador."G1731_C30596";
                    $LsqlV .= $separador."'".$nG1731_C30596."'";
                    $validar = 1;
                }
            }
            
 
            $G1731_C29094 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1731_C29094 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1731_C29094 = ".$G1731_C29094;
                    $LsqlI .= $separador." G1731_C29094";
                    $LsqlV .= $separador.$G1731_C29094;
                    $validar = 1;

                    
                }
            }
 
            $G1731_C29095 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1731_C29095 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1731_C29095 = ".$G1731_C29095;
                    $LsqlI .= $separador." G1731_C29095";
                    $LsqlV .= $separador.$G1731_C29095;
                    $validar = 1;
                }
            }
 
            $G1731_C29096 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1731_C29096 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1731_C29096 = ".$G1731_C29096;
                    $LsqlI .= $separador." G1731_C29096";
                    $LsqlV .= $separador.$G1731_C29096;
                    $validar = 1;
                }
            }
 
            $G1731_C29097 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1731_C29097 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1731_C29097 = ".$G1731_C29097;
                    $LsqlI .= $separador." G1731_C29097";
                    $LsqlV .= $separador.$G1731_C29097;
                    $validar = 1;
                }
            }
 
            $G1731_C29098 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1731_C29098 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1731_C29098 = ".$G1731_C29098;
                    $LsqlI .= $separador." G1731_C29098";
                    $LsqlV .= $separador.$G1731_C29098;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1731_C29099"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1731_C29099 = '".$_POST["G1731_C29099"]."'";
                $LsqlI .= $separador."G1731_C29099";
                $LsqlV .= $separador."'".$_POST["G1731_C29099"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1731_C29100"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1731_C29100 = '".$_POST["G1731_C29100"]."'";
                $LsqlI .= $separador."G1731_C29100";
                $LsqlV .= $separador."'".$_POST["G1731_C29100"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1731_C29101"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1731_C29101 = '".$_POST["G1731_C29101"]."'";
                $LsqlI .= $separador."G1731_C29101";
                $LsqlV .= $separador."'".$_POST["G1731_C29101"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1731_C29102"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1731_C29102 = '".$_POST["G1731_C29102"]."'";
                $LsqlI .= $separador."G1731_C29102";
                $LsqlV .= $separador."'".$_POST["G1731_C29102"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1731_C29103"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1731_C29103 = '".$_POST["G1731_C29103"]."'";
                $LsqlI .= $separador."G1731_C29103";
                $LsqlV .= $separador."'".$_POST["G1731_C29103"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1731_C29104"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1731_C29104 = '".$_POST["G1731_C29104"]."'";
                $LsqlI .= $separador."G1731_C29104";
                $LsqlV .= $separador."'".$_POST["G1731_C29104"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1731_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G1731_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1731_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1731_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1731_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1731_Usuario , G1731_FechaInsercion, G1731_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1731_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1731 WHERE G1731_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1609_ConsInte__b, G1609_C28077, G1609_C28076 FROM ".$BaseDatos.".G1609  ";

        $SQL .= " WHERE G1609_C28077 = '".$numero."'"; 

        $SQL .= " ORDER BY G1609_C28077";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1609_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1609_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G1609_C28077)."</cell>";

                echo "<cell>". ($fila->G1609_C28076)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_1"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G1530_ConsInte__b, G1530_C27157, G1530_C27158, G1530_C27154, G1530_C27155, e.LISOPC_Nombre____b as  G1530_C27156 FROM ".$BaseDatos.".G1530  LEFT JOIN ".$BaseDatos_systema.".LISOPC as e ON e.LISOPC_ConsInte__b =  G1530_C27156 ";

        $SQL .= " WHERE G1530_C27157 = '".$numero."'"; 

        $SQL .= " ORDER BY G1530_C27157";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G1530_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G1530_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G1530_C27157)."</cell>";

                echo "<cell>". ($fila->G1530_C27158)."</cell>";

                echo "<cell>". ($fila->G1530_C27154)."</cell>";

                echo "<cell>". ($fila->G1530_C27155)."</cell>";

                echo "<cell>". ($fila->G1530_C27156)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1609 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1609(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G1609_C28076"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1609_C28076 = '".$_POST["G1609_C28076"]."'";
                    $LsqlI .= $separador."G1609_C28076";
                    $LsqlV .= $separador."'".$_POST["G1609_C28076"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1609_C28077 = $numero;
                    $LsqlU .= ", G1609_C28077 = ".$G1609_C28077."";
                    $LsqlI .= ", G1609_C28077";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1609_Usuario ,  G1609_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1609_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1609 WHERE  G1609_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_1"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1530 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1530(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G1530_C27158"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1530_C27158 = '".$_POST["G1530_C27158"]."'";
                    $LsqlI .= $separador."G1530_C27158";
                    $LsqlV .= $separador."'".$_POST["G1530_C27158"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G1530_C27154"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1530_C27154 = '".$_POST["G1530_C27154"]."'";
                    $LsqlI .= $separador."G1530_C27154";
                    $LsqlV .= $separador."'".$_POST["G1530_C27154"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G1530_C27155"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1530_C27155 = '".$_POST["G1530_C27155"]."'";
                    $LsqlI .= $separador."G1530_C27155";
                    $LsqlV .= $separador."'".$_POST["G1530_C27155"]."'";
                    $validar = 1;
                }

                                                                               
 
                if(isset($_POST["G1530_C27156"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1530_C27156 = '".$_POST["G1530_C27156"]."'";
                    $LsqlI .= $separador."G1530_C27156";
                    $LsqlV .= $separador."'".$_POST["G1530_C27156"]."'";
                    $validar = 1;
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G1530_C27157 = $numero;
                    $LsqlU .= ", G1530_C27157 = ".$G1530_C27157."";
                    $LsqlI .= ", G1530_C27157";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G1530_Usuario ,  G1530_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1530_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G1530 WHERE  G1530_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
