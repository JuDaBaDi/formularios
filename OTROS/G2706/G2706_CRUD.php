<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2706_ConsInte__b, G2706_FechaInsercion , G2706_Usuario ,  G2706_CodigoMiembro  , G2706_PoblacionOrigen , G2706_EstadoDiligenciamiento ,  G2706_IdLlamada , G2706_C52919 as principal ,G2706_C52919,G2706_C52920,G2706_C52921,G2706_C52922,G2706_C52923,G2706_C52924,G2706_C52925,G2706_C52949,G2706_C52926,G2706_C52927,G2706_C52950,G2706_C52928,G2706_C52929,G2706_C52930,G2706_C52931,G2706_C52932,G2706_C52952,G2706_C52908,G2706_C52909,G2706_C52910,G2706_C52911,G2706_C52912,G2706_C52913,G2706_C52914,G2706_C52915,G2706_C52916,G2706_C52917,G2706_C52918,G2706_C52933,G2706_C52934,G2706_C52935,G2706_C52936,G2706_C52937,G2706_C52938,G2706_C52939,G2706_C52940,G2706_C52941,G2706_C52942,G2706_C52943,G2706_C56102 FROM '.$BaseDatos.'.G2706 WHERE G2706_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2706_C52919'] = $key->G2706_C52919;

                $datos[$i]['G2706_C52920'] = $key->G2706_C52920;

                $datos[$i]['G2706_C52921'] = $key->G2706_C52921;

                $datos[$i]['G2706_C52922'] = $key->G2706_C52922;

                $datos[$i]['G2706_C52923'] = $key->G2706_C52923;

                $datos[$i]['G2706_C52924'] = $key->G2706_C52924;

                $datos[$i]['G2706_C52925'] = $key->G2706_C52925;

                $datos[$i]['G2706_C52949'] = $key->G2706_C52949;

                $datos[$i]['G2706_C52926'] = $key->G2706_C52926;

                $datos[$i]['G2706_C52927'] = $key->G2706_C52927;

                $datos[$i]['G2706_C52950'] = $key->G2706_C52950;

                $datos[$i]['G2706_C52928'] = $key->G2706_C52928;

                $datos[$i]['G2706_C52929'] = $key->G2706_C52929;

                $datos[$i]['G2706_C56102'] = $key->G2706_C56102;

                $datos[$i]['G2706_C52930'] = $key->G2706_C52930;

                $datos[$i]['G2706_C52931'] = $key->G2706_C52931;

                $datos[$i]['G2706_C52932'] = $key->G2706_C52932;

                $datos[$i]['G2706_C52952'] = $key->G2706_C52952;

                $datos[$i]['G2706_C52908'] = $key->G2706_C52908;

                $datos[$i]['G2706_C52909'] = $key->G2706_C52909;

                $datos[$i]['G2706_C52910'] = explode(' ', $key->G2706_C52910)[0];
  
                $hora = '';
                if(!is_null($key->G2706_C52911)){
                    $hora = explode(' ', $key->G2706_C52911)[1];
                }

                $datos[$i]['G2706_C52911'] = $hora;

                $datos[$i]['G2706_C52912'] = $key->G2706_C52912;

                $datos[$i]['G2706_C52913'] = $key->G2706_C52913;

                $datos[$i]['G2706_C52914'] = $key->G2706_C52914;

                $datos[$i]['G2706_C52915'] = $key->G2706_C52915;

                $datos[$i]['G2706_C52916'] = $key->G2706_C52916;

                $datos[$i]['G2706_C52917'] = $key->G2706_C52917;

                $datos[$i]['G2706_C52918'] = $key->G2706_C52918;

                $datos[$i]['G2706_C52933'] = $key->G2706_C52933;

                $datos[$i]['G2706_C52934'] = $key->G2706_C52934;

                $datos[$i]['G2706_C52935'] = $key->G2706_C52935;

                $datos[$i]['G2706_C52936'] = $key->G2706_C52936;

                $datos[$i]['G2706_C52937'] = $key->G2706_C52937;

                $datos[$i]['G2706_C52938'] = $key->G2706_C52938;

                $datos[$i]['G2706_C52939'] = $key->G2706_C52939;

                $datos[$i]['G2706_C52940'] = $key->G2706_C52940;

                $datos[$i]['G2706_C52941'] = $key->G2706_C52941;

                $datos[$i]['G2706_C52942'] = $key->G2706_C52942;

                $datos[$i]['G2706_C52943'] = $key->G2706_C52943;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2706";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2706_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2706_ConsInte__b as id,  G2706_C52919 as camp1 , G2706_C52920 as camp2 
                     FROM ".$BaseDatos.".G2706  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2706_ConsInte__b as id,  G2706_C52919 as camp1 , G2706_C52920 as camp2  
                    FROM ".$BaseDatos.".G2706  JOIN ".$BaseDatos.".G2706_M".$_POST['muestra']." ON G2706_ConsInte__b = G2706_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2706_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2706_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2706_C52919 LIKE '%".$B."%' OR G2706_C52920 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2706_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2706");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2706_ConsInte__b, G2706_FechaInsercion , G2706_Usuario ,  G2706_CodigoMiembro  , G2706_PoblacionOrigen , G2706_EstadoDiligenciamiento ,  G2706_IdLlamada , G2706_C52919 as principal ,G2706_C52919,G2706_C52920,G2706_C52921,G2706_C52922,G2706_C52923,G2706_C52924,G2706_C52925,G2706_C52949,G2706_C52926,G2706_C52927,G2706_C52950,G2706_C52928,G2706_C52929,G2706_C52930,G2706_C52931,G2706_C52932,G2706_C52952, a.LISOPC_Nombre____b as G2706_C52908, b.LISOPC_Nombre____b as G2706_C52909,G2706_C52910,G2706_C52911,G2706_C52912,G2706_C52913,G2706_C52914,G2706_C52915,G2706_C52916, c.LISOPC_Nombre____b as G2706_C52917, d.LISOPC_Nombre____b as G2706_C52918, e.LISOPC_Nombre____b as G2706_C52933,G2706_C52934,G2706_C52935,G2706_C52936,G2706_C52937,G2706_C52938,G2706_C52939,G2706_C52940,G2706_C52941,G2706_C52942,G2706_C52943 FROM '.$BaseDatos.'.G2706 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2706_C52908 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2706_C52909 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2706_C52917 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2706_C52918 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2706_C52933';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2706_C52911)){
                    $hora_a = explode(' ', $fila->G2706_C52911)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2706_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2706_ConsInte__b , ($fila->G2706_C52919) , ($fila->G2706_C52920) , ($fila->G2706_C52921) , ($fila->G2706_C52922) , ($fila->G2706_C52923) , ($fila->G2706_C52924) , ($fila->G2706_C52925) , ($fila->G2706_C52949) , ($fila->G2706_C52926) , ($fila->G2706_C52927) , ($fila->G2706_C52950) , ($fila->G2706_C52928) , ($fila->G2706_C52929) , ($fila->G2706_C52930) , ($fila->G2706_C52931) , ($fila->G2706_C52932) , ($fila->G2706_C52952) , ($fila->G2706_C52908) , ($fila->G2706_C52909) , explode(' ', $fila->G2706_C52910)[0] , $hora_a , ($fila->G2706_C52912) , ($fila->G2706_C52913) , ($fila->G2706_C52914) , ($fila->G2706_C52915) , ($fila->G2706_C52916) , ($fila->G2706_C52917) , ($fila->G2706_C52918) , ($fila->G2706_C52933) , ($fila->G2706_C52934) , ($fila->G2706_C52935) , ($fila->G2706_C52936) , ($fila->G2706_C52937) , ($fila->G2706_C52938) , ($fila->G2706_C52939) , ($fila->G2706_C52940) , ($fila->G2706_C52941) , ($fila->G2706_C52942) , ($fila->G2706_C52943) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2706 WHERE G2706_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2706";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2706_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2706_ConsInte__b as id,  G2706_C52919 as camp1 , G2706_C52920 as camp2  FROM '.$BaseDatos.'.G2706 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2706_ConsInte__b as id,  G2706_C52919 as camp1 , G2706_C52920 as camp2  
                    FROM ".$BaseDatos.".G2706  JOIN ".$BaseDatos.".G2706_M".$_POST['muestra']." ON G2706_ConsInte__b = G2706_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2706_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2706_C52919 LIKE "%'.$B.'%" OR G2706_C52920 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2706_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2706 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2706(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2706_C52919"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52919 = '".$_POST["G2706_C52919"]."'";
                $LsqlI .= $separador."G2706_C52919";
                $LsqlV .= $separador."'".$_POST["G2706_C52919"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52920"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52920 = '".$_POST["G2706_C52920"]."'";
                $LsqlI .= $separador."G2706_C52920";
                $LsqlV .= $separador."'".$_POST["G2706_C52920"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52921"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52921 = '".$_POST["G2706_C52921"]."'";
                $LsqlI .= $separador."G2706_C52921";
                $LsqlV .= $separador."'".$_POST["G2706_C52921"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52922"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52922 = '".$_POST["G2706_C52922"]."'";
                $LsqlI .= $separador."G2706_C52922";
                $LsqlV .= $separador."'".$_POST["G2706_C52922"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52923"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52923 = '".$_POST["G2706_C52923"]."'";
                $LsqlI .= $separador."G2706_C52923";
                $LsqlV .= $separador."'".$_POST["G2706_C52923"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52924"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52924 = '".$_POST["G2706_C52924"]."'";
                $LsqlI .= $separador."G2706_C52924";
                $LsqlV .= $separador."'".$_POST["G2706_C52924"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52925"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52925 = '".$_POST["G2706_C52925"]."'";
                $LsqlI .= $separador."G2706_C52925";
                $LsqlV .= $separador."'".$_POST["G2706_C52925"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52949"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52949 = '".$_POST["G2706_C52949"]."'";
                $LsqlI .= $separador."G2706_C52949";
                $LsqlV .= $separador."'".$_POST["G2706_C52949"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52926"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52926 = '".$_POST["G2706_C52926"]."'";
                $LsqlI .= $separador."G2706_C52926";
                $LsqlV .= $separador."'".$_POST["G2706_C52926"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52927"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52927 = '".$_POST["G2706_C52927"]."'";
                $LsqlI .= $separador."G2706_C52927";
                $LsqlV .= $separador."'".$_POST["G2706_C52927"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52950"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52950 = '".$_POST["G2706_C52950"]."'";
                $LsqlI .= $separador."G2706_C52950";
                $LsqlV .= $separador."'".$_POST["G2706_C52950"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52928"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52928 = '".$_POST["G2706_C52928"]."'";
                $LsqlI .= $separador."G2706_C52928";
                $LsqlV .= $separador."'".$_POST["G2706_C52928"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52929"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52929 = '".$_POST["G2706_C52929"]."'";
                $LsqlI .= $separador."G2706_C52929";
                $LsqlV .= $separador."'".$_POST["G2706_C52929"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52930"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52930 = '".$_POST["G2706_C52930"]."'";
                $LsqlI .= $separador."G2706_C52930";
                $LsqlV .= $separador."'".$_POST["G2706_C52930"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52931"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52931 = '".$_POST["G2706_C52931"]."'";
                $LsqlI .= $separador."G2706_C52931";
                $LsqlV .= $separador."'".$_POST["G2706_C52931"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52932"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52932 = '".$_POST["G2706_C52932"]."'";
                $LsqlI .= $separador."G2706_C52932";
                $LsqlV .= $separador."'".$_POST["G2706_C52932"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52952"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52952 = '".$_POST["G2706_C52952"]."'";
                $LsqlI .= $separador."G2706_C52952";
                $LsqlV .= $separador."'".$_POST["G2706_C52952"]."'";
                $validar = 1;
            }

            if(isset($_POST["G2706_C56102"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C56102 = '".$_POST["G2706_C56102"]."'";
                $LsqlI .= $separador."G2706_C56102";
                $LsqlV .= $separador."'".$_POST["G2706_C56102"]."'";
                $validar = 1;
            }
             
 
            $G2706_C52908 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2706_C52908 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2706_C52908 = ".$G2706_C52908;
                    $LsqlI .= $separador." G2706_C52908";
                    $LsqlV .= $separador.$G2706_C52908;
                    $validar = 1;

                    
                }
            }
 
            $G2706_C52909 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2706_C52909 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2706_C52909 = ".$G2706_C52909;
                    $LsqlI .= $separador." G2706_C52909";
                    $LsqlV .= $separador.$G2706_C52909;
                    $validar = 1;
                }
            }
 
            $G2706_C52910 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2706_C52910 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2706_C52910 = ".$G2706_C52910;
                    $LsqlI .= $separador." G2706_C52910";
                    $LsqlV .= $separador.$G2706_C52910;
                    $validar = 1;
                }
            }
 
            $G2706_C52911 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2706_C52911 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2706_C52911 = ".$G2706_C52911;
                    $LsqlI .= $separador." G2706_C52911";
                    $LsqlV .= $separador.$G2706_C52911;
                    $validar = 1;
                }
            }
 
            $G2706_C52912 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2706_C52912 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2706_C52912 = ".$G2706_C52912;
                    $LsqlI .= $separador." G2706_C52912";
                    $LsqlV .= $separador.$G2706_C52912;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2706_C52913"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52913 = '".$_POST["G2706_C52913"]."'";
                $LsqlI .= $separador."G2706_C52913";
                $LsqlV .= $separador."'".$_POST["G2706_C52913"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52914"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52914 = '".$_POST["G2706_C52914"]."'";
                $LsqlI .= $separador."G2706_C52914";
                $LsqlV .= $separador."'".$_POST["G2706_C52914"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52915"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52915 = '".$_POST["G2706_C52915"]."'";
                $LsqlI .= $separador."G2706_C52915";
                $LsqlV .= $separador."'".$_POST["G2706_C52915"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52916"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52916 = '".$_POST["G2706_C52916"]."'";
                $LsqlI .= $separador."G2706_C52916";
                $LsqlV .= $separador."'".$_POST["G2706_C52916"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52917"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52917 = '".$_POST["G2706_C52917"]."'";
                $LsqlI .= $separador."G2706_C52917";
                $LsqlV .= $separador."'".$_POST["G2706_C52917"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52918"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52918 = '".$_POST["G2706_C52918"]."'";
                $LsqlI .= $separador."G2706_C52918";
                $LsqlV .= $separador."'".$_POST["G2706_C52918"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52933"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52933 = '".$_POST["G2706_C52933"]."'";
                $LsqlI .= $separador."G2706_C52933";
                $LsqlV .= $separador."'".$_POST["G2706_C52933"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52934"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52934 = '".$_POST["G2706_C52934"]."'";
                $LsqlI .= $separador."G2706_C52934";
                $LsqlV .= $separador."'".$_POST["G2706_C52934"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52935"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52935 = '".$_POST["G2706_C52935"]."'";
                $LsqlI .= $separador."G2706_C52935";
                $LsqlV .= $separador."'".$_POST["G2706_C52935"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52936"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52936 = '".$_POST["G2706_C52936"]."'";
                $LsqlI .= $separador."G2706_C52936";
                $LsqlV .= $separador."'".$_POST["G2706_C52936"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52937"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52937 = '".$_POST["G2706_C52937"]."'";
                $LsqlI .= $separador."G2706_C52937";
                $LsqlV .= $separador."'".$_POST["G2706_C52937"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52938"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52938 = '".$_POST["G2706_C52938"]."'";
                $LsqlI .= $separador."G2706_C52938";
                $LsqlV .= $separador."'".$_POST["G2706_C52938"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52939"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52939 = '".$_POST["G2706_C52939"]."'";
                $LsqlI .= $separador."G2706_C52939";
                $LsqlV .= $separador."'".$_POST["G2706_C52939"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52940"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52940 = '".$_POST["G2706_C52940"]."'";
                $LsqlI .= $separador."G2706_C52940";
                $LsqlV .= $separador."'".$_POST["G2706_C52940"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52941"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52941 = '".$_POST["G2706_C52941"]."'";
                $LsqlI .= $separador."G2706_C52941";
                $LsqlV .= $separador."'".$_POST["G2706_C52941"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52942"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52942 = '".$_POST["G2706_C52942"]."'";
                $LsqlI .= $separador."G2706_C52942";
                $LsqlV .= $separador."'".$_POST["G2706_C52942"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2706_C52943"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_C52943 = '".$_POST["G2706_C52943"]."'";
                $LsqlI .= $separador."G2706_C52943";
                $LsqlV .= $separador."'".$_POST["G2706_C52943"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2706_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2706_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2706_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2706_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2706_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2706_Usuario , G2706_FechaInsercion, G2706_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2706_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2706 WHERE G2706_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G2704_ConsInte__b, G2704_C52885, G2704_C52871, G2704_C52872, G2704_C52873, G2704_C52874, G2704_C52875, G2704_C52876, G2704_C52877, G2704_C52878, G2704_C52879, G2704_C52880, G2704_C52881, G2704_C52882, G2704_C52883, G2704_C52884, G2704_C52886 FROM ".$BaseDatos.".G2704  ";

        $SQL .= " WHERE G2704_C52871 = '".$numero."'"; 

        $SQL .= " ORDER BY G2704_C52885";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2704_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2704_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G2704_C52885)."</cell>";

                echo "<cell>". ($fila->G2704_C52871)."</cell>";

                echo "<cell>". ($fila->G2704_C52872)."</cell>";

                echo "<cell>". ($fila->G2704_C52873)."</cell>";

                echo "<cell>". ($fila->G2704_C52874)."</cell>";

                echo "<cell>". ($fila->G2704_C52875)."</cell>";

                echo "<cell>". ($fila->G2704_C52876)."</cell>";

                echo "<cell>". ($fila->G2704_C52877)."</cell>";

                echo "<cell>". ($fila->G2704_C52878)."</cell>";

                echo "<cell>". ($fila->G2704_C52879)."</cell>";

                echo "<cell>". ($fila->G2704_C52880)."</cell>";

                echo "<cell>". ($fila->G2704_C52881)."</cell>";

                echo "<cell>". ($fila->G2704_C52882)."</cell>";

                echo "<cell>". ($fila->G2704_C52883)."</cell>";

                echo "<cell>". ($fila->G2704_C52884)."</cell>";

                echo "<cell>". ($fila->G2704_C52886)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2704 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2704(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G2704_C52885"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52885 = '".$_POST["G2704_C52885"]."'";
                    $LsqlI .= $separador."G2704_C52885";
                    $LsqlV .= $separador."'".$_POST["G2704_C52885"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52872"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52872 = '".$_POST["G2704_C52872"]."'";
                    $LsqlI .= $separador."G2704_C52872";
                    $LsqlV .= $separador."'".$_POST["G2704_C52872"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52873"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52873 = '".$_POST["G2704_C52873"]."'";
                    $LsqlI .= $separador."G2704_C52873";
                    $LsqlV .= $separador."'".$_POST["G2704_C52873"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52874"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52874 = '".$_POST["G2704_C52874"]."'";
                    $LsqlI .= $separador."G2704_C52874";
                    $LsqlV .= $separador."'".$_POST["G2704_C52874"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52875"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52875 = '".$_POST["G2704_C52875"]."'";
                    $LsqlI .= $separador."G2704_C52875";
                    $LsqlV .= $separador."'".$_POST["G2704_C52875"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52876"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52876 = '".$_POST["G2704_C52876"]."'";
                    $LsqlI .= $separador."G2704_C52876";
                    $LsqlV .= $separador."'".$_POST["G2704_C52876"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52877"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52877 = '".$_POST["G2704_C52877"]."'";
                    $LsqlI .= $separador."G2704_C52877";
                    $LsqlV .= $separador."'".$_POST["G2704_C52877"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52878"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52878 = '".$_POST["G2704_C52878"]."'";
                    $LsqlI .= $separador."G2704_C52878";
                    $LsqlV .= $separador."'".$_POST["G2704_C52878"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52879"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52879 = '".$_POST["G2704_C52879"]."'";
                    $LsqlI .= $separador."G2704_C52879";
                    $LsqlV .= $separador."'".$_POST["G2704_C52879"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52880"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52880 = '".$_POST["G2704_C52880"]."'";
                    $LsqlI .= $separador."G2704_C52880";
                    $LsqlV .= $separador."'".$_POST["G2704_C52880"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52881"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52881 = '".$_POST["G2704_C52881"]."'";
                    $LsqlI .= $separador."G2704_C52881";
                    $LsqlV .= $separador."'".$_POST["G2704_C52881"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52882"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52882 = '".$_POST["G2704_C52882"]."'";
                    $LsqlI .= $separador."G2704_C52882";
                    $LsqlV .= $separador."'".$_POST["G2704_C52882"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52883"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52883 = '".$_POST["G2704_C52883"]."'";
                    $LsqlI .= $separador."G2704_C52883";
                    $LsqlV .= $separador."'".$_POST["G2704_C52883"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52884"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52884 = '".$_POST["G2704_C52884"]."'";
                    $LsqlI .= $separador."G2704_C52884";
                    $LsqlV .= $separador."'".$_POST["G2704_C52884"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2704_C52886"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2704_C52886 = '".$_POST["G2704_C52886"]."'";
                    $LsqlI .= $separador."G2704_C52886";
                    $LsqlV .= $separador."'".$_POST["G2704_C52886"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2704_C52871 = $numero;
                    $LsqlU .= ", G2704_C52871 = ".$G2704_C52871."";
                    $LsqlI .= ", G2704_C52871";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2704_Usuario ,  G2704_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2704_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2704 WHERE  G2704_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
