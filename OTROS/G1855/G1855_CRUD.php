<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_GET["EnviarCalificacion"])) {
        $SC = $_POST["IdGuion"];
        $G = $_POST["IdGestion"];

        $P = "SELECT GUION__ConsInte__PREGUN_Pri_b AS P, GUION__ConsInte__PREGUN_Sec_b AS S FROM ".$BaseDatos_systema.". GUION_ 
              WHERE GUION__ConsInte__b = 1855;";
        $P = $mysqli->query($P);
        $P = $P->fetch_array();

        $upGCE = "UPDATE ".$BaseDatos.".G1855
                  SET G1855_C30874 = -201
                  WHERE G1855_ConsInte__b = ".$_POST["IdGestion"];           
        $upGCE = $mysqli->query($upGCE);

        $gestion = "SELECT * 
                    FROM ".$BaseDatos.".G1855 
                    WHERE G1855_ConsInte__b = ".$_POST["IdGestion"];
        $gestion = $mysqli->query($gestion);
        $gestion = $gestion->fetch_array();

        if (is_null($gestion["G1855_C30873"]) || $gestion["G1855_C30873"] == "") {
            $valCal = "NULL";
        }else{
            $valCal = $gestion["G1855_C30873"];
        }

        if (is_null($gestion["G1855_C30875"]) || $gestion["G1855_C30875"] == "") {
            $valCom = "NULL";
        }else{
            $valCom = $gestion["G1855_C30875"];
        }

        $histCalidad = "INSERT INTO ".$BaseDatos_systema.".CALHIS 
                        (CALHIS_ConsInte__GUION__b,CALHIS_IdGestion_b,CALHIS_FechaGestion_b,CALHIS_ConsInte__USUARI_Age_b,CALHIS_DatoPrincipalScript_b,CALHIS_DatoSecundarioScript_b,CALHIS_FechaEvaluacion_b,CALHIS_ConsInte__USUARI_Cal_b,CALHIS_Calificacion_b,CALHIS_ComentCalidad_b)
                        VALUES
                        (".$_POST["IdGuion"].",".$_POST["IdGestion"].",'".$gestion["G1855_FechaInsercion"]."',".$gestion["G1855_Usuario"].",'".$gestion["G1855_C".$P["P"]]."','".$gestion["G1855_C".$P["S"]]."','".date('Y-m-d H:i:s')."',".$_POST["IdCal"].",".$valCal.",'".$valCom."')";

        if ($mysqli->query($histCalidad)) {
            $H = $mysqli->insert_id;

            $URL = "interno.dyalogo.cloud/QA/index.php?SC=".$SC."&G=".$G."&H=".$H;
        }else{
            $URL="";
        }

        $HTML = "<!DOCTYPE html><html><head><title>HTML</title></head><body><div><h3>Añadir un comentario : </h3><a href = '".$URL."'>".$URL."</a></div><div>";

        //JDBD - obtenemos las secciones del formulario.
        $Secciones = "SELECT SECCIO_ConsInte__b AS id, 
                             SECCIO_TipoSecc__b AS tipo, 
                             SECCIO_Nombre____b AS nom 
                      FROM ".$BaseDatos_systema.".SECCIO 
                      WHERE SECCIO_ConsInte__GUION__b = 1855 
                      AND SECCIO_TipoSecc__b <> 4 ORDER BY FIELD(SECCIO_TipoSecc__b,2) DESC, 
                               SECCIO_ConsInte__b DESC;";

        $email = "SELECT USUARI_Correo___b AS email
                  FROM ".$BaseDatos_systema.".USUARI 
                  WHERE USUARI_ConsInte__b = ".$gestion["G1855_Usuario"];
        $email = $mysqli->query($email);
        $email = $email->fetch_array();

        $Secciones = $mysqli->query($Secciones);

        $itCal = 0;
        $itNor = 0;

        while ($s = $Secciones->fetch_object()) {
            if ($s->tipo == 2) {
                if ($itCal == 0) {
                    $HTML .= "<div><h1 style='color: #2D0080'>CALIFICACION DE LA LLAMADA</h1><div>";
                }

                $HTML .= "<em style='color: #11CFFF'><h3>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                        $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>"; 
                    }
                }

                if ($itCal == 0) {
                    $HTML .= "</div></div>";
                }
                $itCal ++;
            }else{
                if ($itNor == 0) {
                    $HTML .= "<h1 style='color: #2D0080'>INFORMACION DE LA GESTION DE LLAMADA</h1>";
                }

                $HTML .= "<div><em><h3 style='color: #11CFFF'>".$s->nom."</h3></em>";

                $columnas = "SELECT PREGUN_ConsInte__GUION__b AS G, 
                                    PREGUN_ConsInte__b AS C,
                                    PREGUN_Texto_____b AS nom,
                                    PREGUN_Tipo______b AS tipo
                             FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__SECCIO_b = ".$s->id." ORDER BY PREGUN_ConsInte__b;";

                $columnas = $mysqli->query($columnas);

                while ($c = $columnas->fetch_object()) {
                    if (isset($gestion["G".$c->G."_C".$c->C])) {
                         $HTML .= "<p><strong>".$c->nom." : </strong>".traductor($gestion["G".$c->G."_C".$c->C],$c->tipo)."</p>";  
                    }
                    
                }

                $HTML .= "</div>";

                $itNor ++;
            }
        }

        $HTML .= "</div></body></html>";
        
                $data = array(  
                    "strUsuario_t"              =>  "crm",
                    "strToken_t"                =>  "D43dasd321",
                    "strIdCfg_t"                =>  "18",
                    "strTo_t"                   =>  '"'.$email["email"].'"',
                    "strCC_t"                   =>  '"'.$_POST["Correos"].'"',
                    "strCCO_t"                  =>  null,
                    "strSubject_t"              =>  "Calificacion Llamada #". $gestion["G1855_ConsInte__b"],
                    "strMessage_t"              =>  $HTML,
                    "strListaAdjuntos_t"        =>  null
                ); 

                $data_string = json_encode($data); 

                $ch = curl_init("localhost:8080/dyalogocore/api/ce/correo/sendmailservice");
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(    
                        "Accept: application/json",                                                               
                        "Content-Type: application/json",                                                  
                        "Content-Length: ".strlen($data_string)
                    )                                                                      
                ); 
                $respuesta = curl_exec ($ch);
                $error = curl_error($ch);
                if (isset($respuesta)) {
                    echo json_encode($respuesta);
                }else{
                    echo json_encode($error);
                }
                curl_close ($ch);


        
    }
    
        if(isset($_GET["llenarBtnLlamada"])){// JDBD - Devolver link de la llamada
            $Con = "SELECT G1855_LinkContenido as url FROM ".$BaseDatos.".G1855 WHERE G1855_ConsInte__b = ".$_POST["idReg"];
            $result = $mysqli->query($Con);

            $url = $result->fetch_array();

            echo $url["url"];
        }                   

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1855_ConsInte__b, G1855_FechaInsercion , G1855_Usuario ,  G1855_CodigoMiembro  , G1855_PoblacionOrigen , G1855_EstadoDiligenciamiento ,  G1855_IdLlamada , G1855_C30592 as principal ,G1855_C30592,G1855_C30593,G1855_C30554,G1855_C30555,G1855_C30556,G1855_C30557,G1855_C30558,G1855_C30559,G1855_C30560,G1855_C30561,G1855_C30602,G1855_C30562,G1855_C30543,G1855_C30544,G1855_C30545,G1855_C30546,G1855_C30547,G1855_C30548,G1855_C30549,G1855_C30550,G1855_C30551,G1855_C30877,G1855_C30878,G1855_C30879,G1855_C30874,G1855_C30873,G1855_C30875,G1855_C30876 FROM '.$BaseDatos.'.G1855 WHERE G1855_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1855_C30592'] = $key->G1855_C30592;

                $datos[$i]['G1855_C30593'] = $key->G1855_C30593;

                $datos[$i]['G1855_C30554'] = $key->G1855_C30554;

                $datos[$i]['G1855_C30555'] = $key->G1855_C30555;

                $datos[$i]['G1855_C30556'] = $key->G1855_C30556;

                $datos[$i]['G1855_C30557'] = $key->G1855_C30557;

                $datos[$i]['G1855_C30558'] = $key->G1855_C30558;

                $datos[$i]['G1855_C30559'] = explode(' ', $key->G1855_C30559)[0];
  
                $hora = '';
                if(!is_null($key->G1855_C30560)){
                    $hora = explode(' ', $key->G1855_C30560)[1];
                }

                $datos[$i]['G1855_C30560'] = $hora;

                $datos[$i]['G1855_C30561'] = $key->G1855_C30561;

                $datos[$i]['G1855_C30602'] = $key->G1855_C30602;

                $datos[$i]['G1855_C30562'] = $key->G1855_C30562;

                $datos[$i]['G1855_C30543'] = $key->G1855_C30543;

                $datos[$i]['G1855_C30544'] = $key->G1855_C30544;

                $datos[$i]['G1855_C30545'] = explode(' ', $key->G1855_C30545)[0];
  
                $hora = '';
                if(!is_null($key->G1855_C30546)){
                    $hora = explode(' ', $key->G1855_C30546)[1];
                }

                $datos[$i]['G1855_C30546'] = $hora;

                $datos[$i]['G1855_C30547'] = $key->G1855_C30547;

                $datos[$i]['G1855_C30548'] = $key->G1855_C30548;

                $datos[$i]['G1855_C30549'] = $key->G1855_C30549;

                $datos[$i]['G1855_C30550'] = $key->G1855_C30550;

                $datos[$i]['G1855_C30551'] = $key->G1855_C30551;

                $datos[$i]['G1855_C30877'] = $key->G1855_C30877;

                $datos[$i]['G1855_C30878'] = $key->G1855_C30878;

                $datos[$i]['G1855_C30879'] = $key->G1855_C30879;

                $datos[$i]['G1855_C30874'] = $key->G1855_C30874;

                $datos[$i]['G1855_C30873'] = $key->G1855_C30873;

                $datos[$i]['G1855_C30875'] = $key->G1855_C30875;

                $datos[$i]['G1855_C30876'] = $key->G1855_C30876;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1855";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1855_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1855_ConsInte__b as id,  G1855_C30592 as camp1 , G1855_C30593 as camp2 
                     FROM ".$BaseDatos.".G1855  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1855_ConsInte__b as id,  G1855_C30592 as camp1 , G1855_C30593 as camp2  
                    FROM ".$BaseDatos.".G1855  JOIN ".$BaseDatos.".G1855_M".$_POST['muestra']." ON G1855_ConsInte__b = G1855_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1855_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1855_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1855_C30592 LIKE '%".$B."%' OR G1855_C30593 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1855_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G1855_C30562'])){
                                $Ysql = "SELECT G1849_ConsInte__b as id, G1849_C30490 as text FROM ".$BaseDatos.".G1849 WHERE G1849_C30490 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G1855_C30562"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G1849 WHERE G1849_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G1855_C30562"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1855");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1855_ConsInte__b, G1855_FechaInsercion , G1855_Usuario ,  G1855_CodigoMiembro  , G1855_PoblacionOrigen , G1855_EstadoDiligenciamiento ,  G1855_IdLlamada , G1855_C30592 as principal ,G1855_C30592,G1855_C30593,G1855_C30554,G1855_C30555,G1855_C30556,G1855_C30557,G1855_C30558,G1855_C30559,G1855_C30560, a.LISOPC_Nombre____b as G1855_C30561,G1855_C30602, G1849_C30490, b.LISOPC_Nombre____b as G1855_C30543, c.LISOPC_Nombre____b as G1855_C30544,G1855_C30545,G1855_C30546,G1855_C30547,G1855_C30548,G1855_C30549,G1855_C30550,G1855_C30551,G1855_C30877,G1855_C30878,G1855_C30879, d.LISOPC_Nombre____b as G1855_C30874,G1855_C30873,G1855_C30875,G1855_C30876 FROM '.$BaseDatos.'.G1855 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1855_C30561 LEFT JOIN '.$BaseDatos.'.G1849 ON G1849_ConsInte__b  =  G1855_C30562 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1855_C30543 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1855_C30544 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1855_C30874';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1855_C30546)){
                    $hora_a = explode(' ', $fila->G1855_C30546)[1];
                }

                $hora_b = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1855_C30560)){
                    $hora_b = explode(' ', $fila->G1855_C30560)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1855_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1855_ConsInte__b , ($fila->G1855_C30592) , ($fila->G1855_C30593) , ($fila->G1855_C30554) , ($fila->G1855_C30555) , ($fila->G1855_C30556) , ($fila->G1855_C30557) , ($fila->G1855_C30558) , explode(' ', $fila->G1855_C30559)[0] , $hora_a , ($fila->G1855_C30561) , ($fila->G1855_C30602) , ($fila->G1849_C30490) , ($fila->G1855_C30543) , ($fila->G1855_C30544) , explode(' ', $fila->G1855_C30545)[0] , $hora_b , ($fila->G1855_C30547) , ($fila->G1855_C30548) , ($fila->G1855_C30549) , ($fila->G1855_C30550) , ($fila->G1855_C30551) , ($fila->G1855_C30877) , ($fila->G1855_C30878) , ($fila->G1855_C30879) , ($fila->G1855_C30874) , ($fila->G1855_C30873) , ($fila->G1855_C30875) , ($fila->G1855_C30876) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1855 WHERE G1855_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1855";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1855_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1855_ConsInte__b as id,  G1855_C30592 as camp1 , G1855_C30593 as camp2  FROM '.$BaseDatos.'.G1855 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1855_ConsInte__b as id,  G1855_C30592 as camp1 , G1855_C30593 as camp2  
                    FROM ".$BaseDatos.".G1855  JOIN ".$BaseDatos.".G1855_M".$_POST['muestra']." ON G1855_ConsInte__b = G1855_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1855_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1855_C30592 LIKE "%'.$B.'%" OR G1855_C30593 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1855_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1855 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1855(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1855_C30592"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1855_C30592 = '".$_POST["G1855_C30592"]."'";
                $LsqlI .= $separador."G1855_C30592";
                $LsqlV .= $separador."'".$_POST["G1855_C30592"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1855_C30593"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1855_C30593 = '".$_POST["G1855_C30593"]."'";
                $LsqlI .= $separador."G1855_C30593";
                $LsqlV .= $separador."'".$_POST["G1855_C30593"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1855_C30554"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1855_C30554 = '".$_POST["G1855_C30554"]."'";
                $LsqlI .= $separador."G1855_C30554";
                $LsqlV .= $separador."'".$_POST["G1855_C30554"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1855_C30555"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1855_C30555 = '".$_POST["G1855_C30555"]."'";
                $LsqlI .= $separador."G1855_C30555";
                $LsqlV .= $separador."'".$_POST["G1855_C30555"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1855_C30556"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1855_C30556 = '".$_POST["G1855_C30556"]."'";
                $LsqlI .= $separador."G1855_C30556";
                $LsqlV .= $separador."'".$_POST["G1855_C30556"]."'";
                $validar = 1;
            }
             
  
            $G1855_C30557 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1855_C30557"])){
                if($_POST["G1855_C30557"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1855_C30557 = $_POST["G1855_C30557"];
                    $LsqlU .= $separador." G1855_C30557 = ".$G1855_C30557."";
                    $LsqlI .= $separador." G1855_C30557";
                    $LsqlV .= $separador.$G1855_C30557;
                    $validar = 1;
                }
            }
  
            $G1855_C30558 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1855_C30558"])){
                if($_POST["G1855_C30558"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1855_C30558 = $_POST["G1855_C30558"];
                    $LsqlU .= $separador." G1855_C30558 = ".$G1855_C30558."";
                    $LsqlI .= $separador." G1855_C30558";
                    $LsqlV .= $separador.$G1855_C30558;
                    $validar = 1;
                }
            }
 
            $G1855_C30559 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G1855_C30559"])){    
                if($_POST["G1855_C30559"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G1855_C30559"]);
                    if(count($tieneHora) > 1){
                        $G1855_C30559 = "'".$_POST["G1855_C30559"]."'";
                    }else{
                        $G1855_C30559 = "'".str_replace(' ', '',$_POST["G1855_C30559"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G1855_C30559 = ".$G1855_C30559;
                    $LsqlI .= $separador." G1855_C30559";
                    $LsqlV .= $separador.$G1855_C30559;
                    $validar = 1;
                }
            }
  
            $G1855_C30560 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
            if(isset($_POST["G1855_C30560"])){   
                if($_POST["G1855_C30560"] != '' && $_POST["G1855_C30560"] != 'undefined' && $_POST["G1855_C30560"] != 'null'){
                    $separador = "";
                    $fecha = date('Y-m-d');
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1855_C30560 = "'".$fecha." ".str_replace(' ', '',$_POST["G1855_C30560"])."'";
                    $LsqlU .= $separador." G1855_C30560 = ".$G1855_C30560."";
                    $LsqlI .= $separador." G1855_C30560";
                    $LsqlV .= $separador.$G1855_C30560;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1855_C30561"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1855_C30561 = '".$_POST["G1855_C30561"]."'";
                $LsqlI .= $separador."G1855_C30561";
                $LsqlV .= $separador."'".$_POST["G1855_C30561"]."'";
                $validar = 1;
            }
             
  
            $G1855_C30602 = 0;
            //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
            if(isset($_POST["G1855_C30602"])){
                if($_POST["G1855_C30602"] == 'Yes'){
                    $G1855_C30602 = 1;
                }else if($_POST["G1855_C30602"] == 'off'){
                    $G1855_C30602 = 0;
                }else if($_POST["G1855_C30602"] == 'on'){
                    $G1855_C30602 = 1;
                }else if($_POST["G1855_C30602"] == 'No'){
                    $G1855_C30602 = 1;
                }else{
                    $G1855_C30602 = $_POST["G1855_C30602"] ;
                }   

                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador." G1855_C30602 = ".$G1855_C30602."";
                $LsqlI .= $separador." G1855_C30602";
                $LsqlV .= $separador.$G1855_C30602;

                $validar = 1;
            }
  
            if(isset($_POST["G1855_C30562"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1855_C30562 = '".$_POST["G1855_C30562"]."'";
                $LsqlI .= $separador."G1855_C30562";
                $LsqlV .= $separador."'".$_POST["G1855_C30562"]."'";
                $validar = 1;
            }
             
 
            $G1855_C30543 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1855_C30543 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1855_C30543 = ".$G1855_C30543;
                    $LsqlI .= $separador." G1855_C30543";
                    $LsqlV .= $separador.$G1855_C30543;
                    $validar = 1;

                    
                }
            }
 
            $G1855_C30544 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1855_C30544 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1855_C30544 = ".$G1855_C30544;
                    $LsqlI .= $separador." G1855_C30544";
                    $LsqlV .= $separador.$G1855_C30544;
                    $validar = 1;
                }
            }
 
            $G1855_C30545 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1855_C30545 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1855_C30545 = ".$G1855_C30545;
                    $LsqlI .= $separador." G1855_C30545";
                    $LsqlV .= $separador.$G1855_C30545;
                    $validar = 1;
                }
            }
 
            $G1855_C30546 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1855_C30546 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1855_C30546 = ".$G1855_C30546;
                    $LsqlI .= $separador." G1855_C30546";
                    $LsqlV .= $separador.$G1855_C30546;
                    $validar = 1;
                }
            }
 
            $G1855_C30547 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1855_C30547 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1855_C30547 = ".$G1855_C30547;
                    $LsqlI .= $separador." G1855_C30547";
                    $LsqlV .= $separador.$G1855_C30547;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1855_C30548"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1855_C30548 = '".$_POST["G1855_C30548"]."'";
                $LsqlI .= $separador."G1855_C30548";
                $LsqlV .= $separador."'".$_POST["G1855_C30548"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1855_C30549"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1855_C30549 = '".$_POST["G1855_C30549"]."'";
                $LsqlI .= $separador."G1855_C30549";
                $LsqlV .= $separador."'".$_POST["G1855_C30549"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1855_C30550"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1855_C30550 = '".$_POST["G1855_C30550"]."'";
                $LsqlI .= $separador."G1855_C30550";
                $LsqlV .= $separador."'".$_POST["G1855_C30550"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1855_C30551"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1855_C30551 = '".$_POST["G1855_C30551"]."'";
                $LsqlI .= $separador."G1855_C30551";
                $LsqlV .= $separador."'".$_POST["G1855_C30551"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1855_C30552"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1855_C30552 = '".$_POST["G1855_C30552"]."'";
                $LsqlI .= $separador."G1855_C30552";
                $LsqlV .= $separador."'".$_POST["G1855_C30552"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1855_C30553"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1855_C30553 = '".$_POST["G1855_C30553"]."'";
                $LsqlI .= $separador."G1855_C30553";
                $LsqlV .= $separador."'".$_POST["G1855_C30553"]."'";
                $validar = 1;
            }
             
  
            $G1855_C30877 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1855_C30877"])){
                if($_POST["G1855_C30877"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1855_C30877 = $_POST["G1855_C30877"];
                    $LsqlU .= $separador." G1855_C30877 = ".$G1855_C30877."";
                    $LsqlI .= $separador." G1855_C30877";
                    $LsqlV .= $separador.$G1855_C30877;
                    $validar = 1;
                }
            }
  
            $G1855_C30878 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1855_C30878"])){
                if($_POST["G1855_C30878"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1855_C30878 = $_POST["G1855_C30878"];
                    $LsqlU .= $separador." G1855_C30878 = ".$G1855_C30878."";
                    $LsqlI .= $separador." G1855_C30878";
                    $LsqlV .= $separador.$G1855_C30878;
                    $validar = 1;
                }
            }
  
            $G1855_C30879 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1855_C30879"])){
                if($_POST["G1855_C30879"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1855_C30879 = $_POST["G1855_C30879"];
                    $LsqlU .= $separador." G1855_C30879 = ".$G1855_C30879."";
                    $LsqlI .= $separador." G1855_C30879";
                    $LsqlV .= $separador.$G1855_C30879;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1855_C30874"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1855_C30874 = '".$_POST["G1855_C30874"]."'";
                $LsqlI .= $separador."G1855_C30874";
                $LsqlV .= $separador."'".$_POST["G1855_C30874"]."'";
                $validar = 1;
            }
             
  
            $G1855_C30873 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G1855_C30873"])){
                if($_POST["G1855_C30873"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G1855_C30873 = $_POST["G1855_C30873"];
                    $LsqlU .= $separador." G1855_C30873 = ".$G1855_C30873."";
                    $LsqlI .= $separador." G1855_C30873";
                    $LsqlV .= $separador.$G1855_C30873;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1855_C30875"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1855_C30875 = '".$_POST["G1855_C30875"]."'";
                $LsqlI .= $separador."G1855_C30875";
                $LsqlV .= $separador."'".$_POST["G1855_C30875"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1855_C30876"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1855_C30876 = '".$_POST["G1855_C30876"]."'";
                $LsqlI .= $separador."G1855_C30876";
                $LsqlV .= $separador."'".$_POST["G1855_C30876"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1855_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G1855_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1855_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1855_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1855_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1855_Usuario , G1855_FechaInsercion, G1855_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1855_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1855 WHERE G1855_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
