<?php

    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
        
    //Inserciones o actualizaciones

    if(isset($_POST['getListaHija'])){
        $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
        $res = $mysqli->query($Lsql);
        echo "<option value='0'>Seleccione</option>";
        while($key = $res->fetch_object()){
            echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
        }
    }
        
    if(isset($_POST["oper"])){
            $str_Lsql  = '';

            $validar = 0;
            $str_LsqlU = "UPDATE ".$BaseDatos.".G1855 SET "; 
            $str_LsqlI = "INSERT INTO ".$BaseDatos.".G1855( G1855_FechaInsercion ,";
            $str_LsqlV = " VALUES ('".date('Y-m-d H:s:i')."',"; 
  
        if(isset($_POST["G1855_C30592"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1855_C30592 = '".$_POST["G1855_C30592"]."'";
            $str_LsqlI .= $separador."G1855_C30592";
            $str_LsqlV .= $separador."'".$_POST["G1855_C30592"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1855_C30593"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1855_C30593 = '".$_POST["G1855_C30593"]."'";
            $str_LsqlI .= $separador."G1855_C30593";
            $str_LsqlV .= $separador."'".$_POST["G1855_C30593"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1855_C30554"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1855_C30554 = '".$_POST["G1855_C30554"]."'";
            $str_LsqlI .= $separador."G1855_C30554";
            $str_LsqlV .= $separador."'".$_POST["G1855_C30554"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1855_C30555"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1855_C30555 = '".$_POST["G1855_C30555"]."'";
            $str_LsqlI .= $separador."G1855_C30555";
            $str_LsqlV .= $separador."'".$_POST["G1855_C30555"]."'";
            $validar = 1;
        }
         
  
        if(isset($_POST["G1855_C30556"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1855_C30556 = '".$_POST["G1855_C30556"]."'";
            $str_LsqlI .= $separador."G1855_C30556";
            $str_LsqlV .= $separador."'".$_POST["G1855_C30556"]."'";
            $validar = 1;
        }
         
  
        $G1855_C30557 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1855_C30557"])){
            if($_POST["G1855_C30557"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //$G1855_C30557 = $_POST["G1855_C30557"];
                $G1855_C30557 = str_replace(".", "", $_POST["G1855_C30557"]);
                $G1855_C30557 =  str_replace(",", ".", $G1855_C30557);
                $str_LsqlU .= $separador." G1855_C30557 = '".$G1855_C30557."'";
                $str_LsqlI .= $separador." G1855_C30557";
                $str_LsqlV .= $separador."'".$G1855_C30557."'";
                $validar = 1;
            }
        }
  
        $G1855_C30558 = NULL;
        //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
        if(isset($_POST["G1855_C30558"])){
            if($_POST["G1855_C30558"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                
                $G1855_C30558 = str_replace(".", "", $_POST["G1855_C30558"]);
                $G1855_C30558 =  str_replace(",", ".", $G1855_C30558);
                $str_LsqlU .= $separador." G1855_C30558 = '".$G1855_C30558."'";
                $str_LsqlI .= $separador." G1855_C30558";
                $str_LsqlV .= $separador."'".$G1855_C30558."'";
                $validar = 1;
            }
        }
 
        $G1855_C30559 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["G1855_C30559"])){    
            if($_POST["G1855_C30559"] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }
                $G1855_C30559 = "'".str_replace(' ', '',$_POST["G1855_C30559"])." 00:00:00'";
                $str_LsqlU .= $separador." G1855_C30559 = ".$G1855_C30559;
                $str_LsqlI .= $separador." G1855_C30559";
                $str_LsqlV .= $separador.$G1855_C30559;
                $validar = 1;
            }
        }
  
        $G1855_C30560 = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no o esta undefined
        if(isset($_POST["G1855_C30560"])){   
            if($_POST["G1855_C30560"] != '' && $_POST["G1855_C30560"] != 'undefined' && $_POST["G1855_C30560"] != 'null'){
                $separador = "";
                $fecha = date('Y-m-d');
                if($validar == 1){
                    $separador = ",";
                }

                $G1855_C30560 = "'".$fecha." ".str_replace(' ', '',$_POST["G1855_C30560"])."'";
                $str_LsqlU .= $separador." G1855_C30560 = ".$G1855_C30560."";
                $str_LsqlI .= $separador." G1855_C30560";
                $str_LsqlV .= $separador.$G1855_C30560;
                $validar = 1;
            }
        }
  
        if(isset($_POST["G1855_C30561"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1855_C30561 = '".$_POST["G1855_C30561"]."'";
            $str_LsqlI .= $separador."G1855_C30561";
            $str_LsqlV .= $separador."'".$_POST["G1855_C30561"]."'";
            $validar = 1;
        }
         
  
        $G1855_C30602 = 0;
        //este es tipo check primeo si viene y de acuerdo a su valor se pone 1 o 0
        if(isset($_POST["G1855_C30602"])){
            if($_POST["G1855_C30602"] == 'Yes'){
                $G1855_C30602 = 1;
            }else if($_POST["G1855_C30602"] == 'off'){
                $G1855_C30602 = 0;
            }else if($_POST["G1855_C30602"] == 'on'){
                $G1855_C30602 = 1;
            }else if($_POST["G1855_C30602"] == 'No'){
                $G1855_C30602 = 0;
            }else{
                $G1855_C30602 = $_POST["G1855_C30602"] ;
            }   

            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G1855_C30602 = ".$G1855_C30602."";
            $str_LsqlI .= $separador." G1855_C30602";
            $str_LsqlV .= $separador.$G1855_C30602;

            $validar = 1;
        }else{
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador." G1855_C30602 = ".$G1855_C30602."";
            $str_LsqlI .= $separador." G1855_C30602";
            $str_LsqlV .= $separador.$G1855_C30602;

            $validar = 1;
        }
  
        if(isset($_POST["G1855_C30562"])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1855_C30562 = '".$_POST["G1855_C30562"]."'";
            $str_LsqlI .= $separador."G1855_C30562";
            $str_LsqlV .= $separador."'".$_POST["G1855_C30562"]."'";
            $validar = 1;
        }
         
 
        $padre = NULL;
        //este es de tipo date hay que preguntar si esta vacia o no
        if(isset($_POST["padre"])){    
            if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                //primero hay que ir y buscar los campos
                $str_Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                $GuidRes = $mysqli->query($str_Lsql);
                $campo = null;
                while($ky = $GuidRes->fetch_object()){
                    $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                }
                $valorG = "G1855_C";
                $valorH = $valorG.$campo;
                $str_LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                $str_LsqlI .= $separador." ".$valorH;
                $str_LsqlV .= $separador.$_POST['padre'] ;
                $validar = 1;
            }
        }

        if(isset($_GET['id_gestion_cbx'])){
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }

            $str_LsqlU .= $separador."G1855_IdLlamada = '".$_GET['id_gestion_cbx']."'";
            $str_LsqlI .= $separador."G1855_IdLlamada";
            $str_LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
            $validar = 1;
        }


        if(isset($_POST['oper'])){
            if($_POST["oper"] == 'add' ){
                
                $str_Lsql = $str_LsqlI.")" . $str_LsqlV.")";
            }
        }

        //Si trae algo que insertar inserta

        //echo $str_Lsql;
        if($validar == 1){
            if ($mysqli->query($str_Lsql) === TRUE) {
                $ultimoResgistroInsertado = $mysqli->insert_id;
                //ahora toca ver lo de la muestra asi que toca ver que pasa 
                /* primero buscamos la campaña que nos esta llegando */
                $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b , CAMPAN_ActPobGui_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_POST["campana"];

                //echo $Lsql_Campan;

                $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
                $datoCampan = $res_Lsql_Campan->fetch_array();
                $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
                $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
                $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
                $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
                $int_CAMPAN_ActPo = $datoCampan['CAMPAN_ActPobGui_b'];


                if($int_CAMPAN_ActPo == '-1'){
                    /* toca hacer actualizacion desde Script */
                    
                    $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_POST["campana"];
                    $resultcampSql = $mysqli->query($campSql);
                    $Lsql = 'UPDATE '.$BaseDatos.'.'.$str_Pobla_Campan.' , '.$BaseDatos.'.G'.$int_Guion_Campan.' SET ';
                    $i=0;
                    while($key = $resultcampSql->fetch_object()){

                        if($i == 0){
                            $Lsql .= $key->CAMINC_NomCamPob_b . ' = '.$key->CAMINC_NomCamGui_b;
                        }else{
                            $Lsql .= " , ".$key->CAMINC_NomCamPob_b . ' = '.$key->CAMINC_NomCamGui_b;
                        }
                        $i++;
                    } 
                    $Lsql .= ' WHERE  G'.$int_Guion_Campan.'_ConsInte__b = '.$ultimoResgistroInsertado.' AND G'.$int_Guion_Campan.'_CodigoMiembro = '.$str_Pobla_Campan.'_ConsInte__b'; 
                    //echo "Esta ".$Lsql;
                    if($mysqli->query($Lsql) === TRUE ){

                    }else{
                        echo "NO sE ACTALIZO LA BASE DE DATOS ".$mysqli->error;
                    }
                }


                //Ahora toca actualizar la muestra
                $MuestraSql = "UPDATE ".$BaseDatos.".".$str_Pobla_Campan."_M".$int_Muest_Campan." SET 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_Estado____b = 3, 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b = ".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b + 1, 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_UltiGest__b = '-9' , 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_ConUltGes_b = 7,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecUltGes_b = '".date('Y-m-d H:i:s')."', 
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_EfeUltGes_b = 3,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_Comentari_b = 'No desea participar',
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecHorAge_b = NULL,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_GesMasImp_b = '-9',
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoGesMaIm_b = 7,
                        ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FeGeMaIm__b ='".date('Y-m-d H:i:s')."'";
                $MuestraSql .= " WHERE ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoInMiPo__b = ".$_POST['id'];
                // echo $MuestraSql;
                if($mysqli->query($MuestraSql) === true){

                }else{
                    echo "Error insertando la muesta => ".$mysqli->error;
                }
                
                header('Location:http://'.$_SERVER['HTTP_HOST'].'/crm_php/web_forms.php?web=MTg1NQ==&result=1');

            } else {
                echo "Error Hacieno el proceso los registros : " . $mysqli->error;
            }
        }
    }
    


?>
