
<?php date_default_timezone_set('America/Bogota'); ?>

<div class="modal fade-in" id="enviarCalificacion" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width: 50%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="CerrarCalificacion">&times;</button>
                <h4 class="modal-title">Enviar Calificacion</h4>
            </div>
            <div class="modal-body">
                <div class="row" style="margin-bottom: 10px;">
                    <div class="col-md-12">
                        <p >Para enviar la calificacion a otros correos, ingresarlos <strong>SEPARANDOLOS</strong>  por una coma ( , ).</p>
                        <input type="text" class="form-control" id="cajaCorreos" name="cajaCorreos" placeholder="Ejemplo1@ejem.com,Ejemplo2@ejem.com">
 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <img hidden id="loading" src="/crm_php/assets/plugins/loading.gif" width="30" height="30">&nbsp;&nbsp;&nbsp;
                        <button id="sendEmails" readonly class="btn btn-primary" >Enviar Calificacion</button>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
                
<input type="hidden" id="IdGestion">
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog" style="width:95%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G1905/G1905_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

    $PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

    if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G1905_ConsInte__b as id, G1905_C34964 as camp1 , G1905_C34966 as camp2 FROM ".$BaseDatos.".G1905  WHERE G1905_Usuario = ".$idUsuario." ORDER BY G1905_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G1905_ConsInte__b as id, G1905_C34964 as camp1 , G1905_C34966 as camp2 FROM ".$BaseDatos.".G1905  ORDER BY G1905_ConsInte__b DESC LIMIT 0, 50";
        }

        $muestra = 0;
        $tipoDistribucion = 0;
        $tareaBackoffice = 0;

        if(isset($_GET['tareabackoffice'])){
            $tareaBackoffice = 1;

            $tareaBsql = "SELECT TAREAS_BACKOFFICE_ConsInte__b as id, TAREAS_BACKOFFICE_ConsInte__ESTPAS_b as estpas, TAREAS_BACKOFFICE_TipoDistribucionTrabajo_b as tipoDist FROM ".$BaseDatos_systema.".TAREAS_BACKOFFICE WHERE TAREAS_BACKOFFICE_ConsInte__b = ".$_GET['tareabackoffice'];
            $tareaBQuery = $mysqli->query($tareaBsql);

            while ($key =  $tareaBQuery->fetch_object()) {
                $resultTareaB = $key;
            }

            $estpassql = "SELECT ESTPAS_ConsInte__MUESTR_b as muestr FROM ".$BaseDatos_systema.".ESTPAS WHERE ESTPAS_ConsInte__b = ".$resultTareaB->estpas;
            $estpasQuery = $mysqli->query($estpassql);

            while ($key =  $estpasQuery->fetch_object()) {
                $resultEstpas = $key;
            }

            $muestra = $resultEstpas->muestr;
            $tipoDistribucion = $resultTareaB->tipoDist;

            if($resultTareaB->tipoDist == 1){
                $Zsql = "SELECT G1905_ConsInte__b as id, G1905_C34964 as camp1 , G1905_C34966 as camp2 FROM ".$BaseDatos.".G1905 JOIN ".$BaseDatos.".G1905_M".$resultEstpas->muestr." ON G1905_ConsInte__b = G1905_M".$resultEstpas->muestr."_CoInMiPo__b ORDER BY G1905_ConsInte__b DESC LIMIT 0, 50";
            }else{
                $Zsql = "SELECT G1905_ConsInte__b as id, G1905_C34964 as camp1 , G1905_C34966 as camp2 FROM ".$BaseDatos.".G1905 JOIN ".$BaseDatos.".G1905_M".$resultEstpas->muestr." ON G1905_ConsInte__b = G1905_M".$resultEstpas->muestr."_CoInMiPo__b WHERE G1905_M".$resultEstpas->muestr."_ConIntUsu_b = ".$idUsuario." ORDER BY G1905_ConsInte__b DESC LIMIT 0, 50";
            }
            
        }

    }else{
        $Zsql = "SELECT G1905_ConsInte__b as id, G1905_C34964 as camp1 , G1905_C34966 as camp2 FROM ".$BaseDatos.".G1905  ORDER BY G1905_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>

<?php 

    include(__DIR__ ."/../cabecera.php");

?>

<?php
if(isset($_GET['user'])){


    $Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_Nombre____b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
    $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
    $datoCampan = $res_Lsql_Campan->fetch_array();
    $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
    $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
    $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
    $str_Nombr_Campan = $datoCampan['CAMPAN_Nombre____b'];


    $getPrincipales = "SELECT GUION__ConsInte__PREGUN_Pri_b FROM ".$BaseDatos_systema.".GUION_  WHERE GUION__ConsInte__b = ".$int_Pobla_Camp_2;
    $resLsql = $mysqli->query($getPrincipales);
    //echo $getPrincipales;
    $dato = $resLsql->fetch_array();

    $XLsql = "SELECT ".$str_Pobla_Campan."_C".$dato['GUION__ConsInte__PREGUN_Pri_b']." as nombre FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['user'].";";
    $nombre = $mysqli->query($XLsql);
    $nombreUsuario = NULL;
    //echo $XLsql;
    while ($key = $nombre->fetch_object()) {
        echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
        $nombreUsuario = $key->nombre;
        break;
    } 


    if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){


                    
        $data = array(  "strToken_t" => $_GET['token'], 
                        "strIdGestion_t" => $_GET['id_gestion_cbx'],
                        "strDatoPrincipal_t" => $nombreUsuario,
                        "strNombreCampanaCRM_t" => $str_Nombr_Campan);                                                                    
        $data_string = json_encode($data);    

        $ch = curl_init($IP_CONFIGURADA.'gestion/asignarDatoPrincipal');
        //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 
        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data_string))                                                                      
        ); 
        //recogemos la respuesta
        $respuesta = curl_exec ($ch);
        //o el error, por si falla
        $error = curl_error($ch);
        //y finalmente cerramos curl
        //echo "Respuesta =>  ". $respuesta;
        //echo "<br/>Error => ".$error;
        //include "Log.class.php";
        //$log = new Log("log", "./Log/");
        //$log->insert($error, $respuesta, false, true, false);
        //echo "nada";
        curl_close ($ch);
    }
}else{
    echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";    
}
?>

<?php if(isset($_GET['user'])){ ?>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="box">
            <div class="box-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th colspan="4">
                                Historico de gestiones
                            </th>
                        </tr>
                        <tr>
                            <th>Gesti&oacute;n</th>
                            <th>Comentarios</th>
                            <th>Fecha - hora</th>
                            <th>Agente</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            $Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


                            $res = $mysqli->query($Lsql);
                            while($key = $res->fetch_object()){
                                echo "<tr>";
                                echo "<td>".($key->MONOEF_Texto_____b)."</td>";
                                echo "<td>".$key->CONDIA_Observacio_b."</td>";
                                echo "<td>".$key->CONDIA_Fecha_____b."</td>";
                                echo "<td>".$key->USUARI_Nombre____b."</td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div  class="panel box box-primary" id="5035" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5035c">
                GUION DE ATENCION DE LLAMADA
            </a>
        </h4>
        
    </div>
    <div id="s_5035c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Buenos días/tardes, está comunicado con la Línea de Atención del Programa Aprende en Casa con MALOKA, mi nombre es (Nombre y Apellido del Guía) ¿Con quién tengo el gusto?</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Mucho gusto Sr./Sra. (Nombre del Acudiente), ¿podría por favor indicarme el número celular en el que recibió el mensaje que enviamos?</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">El motivo por el cual recibió el mensaje es indicarles que (Nombres y Apellidos del Estudiante) fue seleccionado como uno de los beneficiarios del Programa Aprende en Casa con MALOKA.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Este programa es gratuito y consiste en la entrega de una serie de recursos educativos impresos para educación inicial y básica primaria con el objetivo de apoyar el aprendizaje de los niños en la casa y desarrollar actividades que incentiven el gusto por la ciencia.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">El material será entregado en el domicilio de todos los estudiantes y les enviaremos 3 entregas durante el segundo semestre del año para que sean desarrolladas en compañía del acudiente.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Para continuar con el proceso, necesitamos que por favor nos confirme algunos datos para la entrega del material en el lugar en el que se encuentra el estudiante. ¿Está usted de acuerdo?</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Si la persona confirma que está de acuerdo, pasamos a la sección “Desarrollo”.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Si no está de acuerdo, se deben atender las preguntas u objeciones, de acuerdo con la orientación brindada en la “Tabla de Preguntas y Objeciones”</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="4999" style='display:none;'>
<h3 class="box box-title"></h3>

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34958" id="LblG1905_C34958">Agente</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34958" value="<?php echo getNombreUser($token);?>" readonly name="G1905_C34958"  placeholder="Agente">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34959" id="LblG1905_C34959">Fecha</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34959" value="<?php echo date('Y-m-d H:i:s');?>" readonly name="G1905_C34959"  placeholder="Fecha">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34960" id="LblG1905_C34960">Hora</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34960" value="<?php echo date('H:i:s');?>" readonly name="G1905_C34960"  placeholder="Hora">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34961" id="LblG1905_C34961">Campaña</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34961" value="<?php if(isset($_GET["campana_crm"])){ $cmapa = "SELECT CAMPAN_Nombre____b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
                $resCampa = $mysqli->query($cmapa);
                $dataCampa = $resCampa->fetch_array(); echo $dataCampa["CAMPAN_Nombre____b"]; } else { echo "NO TIENE CAMPAÑA";}?>" readonly name="G1905_C34961"  placeholder="Campaña">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


</div>

<div  class="panel box box-primary" id="5036" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5036c">
                DESARROLLO
            </a>
        </h4>
        
    </div>
    <div id="s_5036c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr./Sra. (Nombre del Acudiente), agradezco me confirme la siguiente información del lugar a donde haremos llegar el material:</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">a. Dirección exacta (Preguntar si vive en apartamento, piso específico, etc.)</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">b. Barrio</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">c. Localidad</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">d. Ciudad</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">e. Nombre del Acudiente que se encuentra con el estudiante durante la cuarentena</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">f. Pedir correo electrónico</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">g. (Solo para llamadas a teléfonos fijos): Número Celular del Acudiente</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">h. ¿Tiene computador y conexión a internet en la casa donde está el niño?</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">i. En caso de que el registro no traiga Grado, por favor preguntar cual es y actualizar</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Muchas gracias Sr./Sra. (Nombre del Acudiente). ¿Confirma usted que la información brindada es la que corresponde al Acudiente del Estudiante (Nombres y Apellidos del Estudiante) y autoriza al tratamiento de los datos para vincularlos en la inscripción, seguimiento y envío de material del Programa Aprende en Casa con MALOKA de acuerdo a nuestra Política de Tratamiento de Datos Personales?</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Si confirma y autoriza el Tratamiento de Datos, pasar a la sección “Salida</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Si no confirma o no autoriza el Tratamiento de Datos, se debe informar que no es posible incluir al Estudiante en el Programa.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5037" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5037c">
                SALIDA
            </a>
        </h4>
        
    </div>
    <div id="s_5037c" class="panel-collapse collapse ">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Si tenemos número de celular:</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Le confirmo que días antes de la entrega del primer paquete de guías, le estaremos enviando un mensaje de texto para que por favor esté atento(a) de la entrega.</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- lIBRETO O LABEL -->
                    <p style="text-align:justify;">Sr./Sra. (Nombre del Acudiente), muchas gracias por comunicarse con nosotros. Que tenga un(a) buen día/tarde/noche</p>
                    <!-- FIN LIBRETO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5038" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5038c">
                PREGUNTAS-OBJECIONES
            </a>
        </h4>
        
    </div>
    <div id="s_5038c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1905_C35153" id="LblG1905_C35153">Solución de dudas y objeciones</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1905_C35153" id="G1905_C35153">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1981 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1905_C35153" id="respuesta_LblG1905_C35153">Respuesta</label>
                        <textarea id="respuesta_G1905_C35153" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  id="4998" >
<h3 class="box box-title"></h3>

</div>

<div  class="panel box box-primary" id="4997" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_4997c">
                INFORMACIÓN DEL ESTUDIANTE
            </a>
        </h4>
        
    </div>
    <div id="s_4997c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34964" id="LblG1905_C34964">Nombre del Estudiante</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34964" value="<?php if (isset($_GET['G1905_C34964'])) {
                            echo $_GET['G1905_C34964'];
                        } ?>" readonly name="G1905_C34964"  placeholder="Nombre del Estudiante">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34965" id="LblG1905_C34965">Tipo de Documento</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34965" value="<?php if (isset($_GET['G1905_C34965'])) {
                            echo $_GET['G1905_C34965'];
                        } ?>" readonly name="G1905_C34965"  placeholder="Tipo de Documento">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34966" id="LblG1905_C34966">Número de Identificación</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34966" value="<?php if (isset($_GET['G1905_C34966'])) {
                            echo $_GET['G1905_C34966'];
                        } ?>" readonly name="G1905_C34966"  placeholder="Número de Identificación">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34967" id="LblG1905_C34967">Grado Actual</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34967" value="<?php if (isset($_GET['G1905_C34967'])) {
                            echo $_GET['G1905_C34967'];
                        } ?>"  name="G1905_C34967"  placeholder="Grado Actual">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34968" id="LblG1905_C34968">Colegio</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34968" value="<?php if (isset($_GET['G1905_C34968'])) {
                            echo $_GET['G1905_C34968'];
                        } ?>" readonly name="G1905_C34968"  placeholder="Colegio">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5000" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5000c">
                INFORMACION DEL ACUDIENTE
            </a>
        </h4>
        
    </div>
    <div id="s_5000c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34962" id="LblG1905_C34962">Nombre del Acudiente</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34962" value="<?php if (isset($_GET['G1905_C34962'])) {
                            echo $_GET['G1905_C34962'];
                        } ?>"  name="G1905_C34962"  placeholder="Nombre del Acudiente">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34963" id="LblG1905_C34963">Persona Autorizada para recibir</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34963" value="<?php if (isset($_GET['G1905_C34963'])) {
                            echo $_GET['G1905_C34963'];
                        } ?>"  name="G1905_C34963"  placeholder="Persona Autorizada para recibir">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C35095" id="LblG1905_C35095">Parentezco del acudiente</label>
                        <input type="text" class="form-control input-sm" id="G1905_C35095" value="<?php if (isset($_GET['G1905_C35095'])) {
                            echo $_GET['G1905_C35095'];
                        } ?>"  name="G1905_C35095"  placeholder="Parentezco del acudiente">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C35096" id="LblG1905_C35096">Parentezco de quien recibe</label>
                        <input type="text" class="form-control input-sm" id="G1905_C35096" value="<?php if (isset($_GET['G1905_C35096'])) {
                            echo $_GET['G1905_C35096'];
                        } ?>"  name="G1905_C35096"  placeholder="Parentezco de quien recibe">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5001" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5001c">
                INFORMACION DE CONTACTO
            </a>
        </h4>
        
    </div>
    <div id="s_5001c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34969" id="LblG1905_C34969">LUGAR DE ENTREGA (Casa/ Colegio)</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34969" value="<?php if (isset($_GET['G1905_C34969'])) {
                            echo $_GET['G1905_C34969'];
                        } ?>" readonly name="G1905_C34969"  placeholder="LUGAR DE ENTREGA (Casa/ Colegio)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34970" id="LblG1905_C34970">Dirección de Residencia Del Estudiante</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34970" value="<?php if (isset($_GET['G1905_C34970'])) {
                            echo $_GET['G1905_C34970'];
                        } ?>" readonly name="G1905_C34970"  placeholder="Dirección de Residencia Del Estudiante">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34971" id="LblG1905_C34971">ZONA SDP</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34971" value="<?php if (isset($_GET['G1905_C34971'])) {
                            echo $_GET['G1905_C34971'];
                        } ?>" readonly name="G1905_C34971"  placeholder="ZONA SDP">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1905_C34972" id="LblG1905_C34972">¿Se actualizo la dirección?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1905_C34972" id="G1905_C34972">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1970 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34973" id="LblG1905_C34973">Dirección Actualizada del Estudiante</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34973" value="<?php if (isset($_GET['G1905_C34973'])) {
                            echo $_GET['G1905_C34973'];
                        } ?>"  name="G1905_C34973"  placeholder="Dirección Actualizada del Estudiante">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34974" id="LblG1905_C34974">Especificaciones de Dirección (Torre, manzana etc)</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34974" value="<?php if (isset($_GET['G1905_C34974'])) {
                            echo $_GET['G1905_C34974'];
                        } ?>"  name="G1905_C34974"  placeholder="Especificaciones de Dirección (Torre, manzana etc)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34975" id="LblG1905_C34975">Ciudad</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34975" value="<?php if (isset($_GET['G1905_C34975'])) {
                            echo $_GET['G1905_C34975'];
                        } ?>"  name="G1905_C34975"  placeholder="Ciudad">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34976" id="LblG1905_C34976">Localidad</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34976" value="<?php if (isset($_GET['G1905_C34976'])) {
                            echo $_GET['G1905_C34976'];
                        } ?>"  name="G1905_C34976"  placeholder="Localidad">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34977" id="LblG1905_C34977">Barrio</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34977" value="<?php if (isset($_GET['G1905_C34977'])) {
                            echo $_GET['G1905_C34977'];
                        } ?>"  name="G1905_C34977"  placeholder="Barrio">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34978" id="LblG1905_C34978">Dirección de Trabajo del Acudiente (solo si aplica)</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34978" value="<?php if (isset($_GET['G1905_C34978'])) {
                            echo $_GET['G1905_C34978'];
                        } ?>"  name="G1905_C34978"  placeholder="Dirección de Trabajo del Acudiente (solo si aplica)">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34979" id="LblG1905_C34979">Correo Electrónico</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34979" value="<?php if (isset($_GET['G1905_C34979'])) {
                            echo $_GET['G1905_C34979'];
                        } ?>"  name="G1905_C34979"  placeholder="Correo Electrónico">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34983" id="LblG1905_C34983">Teléfono celular 1</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34983" value="<?php if (isset($_GET['G1905_C34983'])) {
                            echo $_GET['G1905_C34983'];
                        } ?>"  name="G1905_C34983"  placeholder="Teléfono celular 1">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34982" id="LblG1905_C34982">Teléfono Celular 2</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34982" value="<?php if (isset($_GET['G1905_C34982'])) {
                            echo $_GET['G1905_C34982'];
                        } ?>"  name="G1905_C34982"  placeholder="Teléfono Celular 2">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34980" id="LblG1905_C34980">Teléfono fijo 1</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34980" value="<?php if (isset($_GET['G1905_C34980'])) {
                            echo $_GET['G1905_C34980'];
                        } ?>"  name="G1905_C34980"  placeholder="Teléfono fijo 1">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-6 col-xs-6">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34981" id="LblG1905_C34981">Teléfono fijo 2</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34981" value="<?php if (isset($_GET['G1905_C34981'])) {
                            echo $_GET['G1905_C34981'];
                        } ?>"  name="G1905_C34981"  placeholder="Teléfono fijo 2">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->


        </div> <!-- AQUIFINSALDO1 -->


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5002" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5002c">
                INFORMACION ADICIONAL
            </a>
        </h4>
        
    </div>
    <div id="s_5002c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1905_C34984" id="LblG1905_C34984">¿Cuentan con conexión a internet en el hogar?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1905_C34984" id="G1905_C34984">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1970 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->


            <div class="col-md-6 col-xs-6">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1905_C34985" id="LblG1905_C34985">¿El/la estudiante cuenta con acceso a herramientas tecnológicas como computador o tablet?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1905_C34985" id="G1905_C34985">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1970 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div  class="panel box box-primary" id="5003" >
    <div class="box-header with-border">
        <h4 class="box-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#s_5003c">
                AUTORIZACION PROTECCION DE DATOS PERSONALES
            </a>
        </h4>
        
    </div>
    <div id="s_5003c" class="panel-collapse collapse in">
        <div class="box-body">

        <div class="row">
        

            <div class="col-md-12 col-xs-12">


                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1905_C34986" id="LblG1905_C34986">¿Confirma usted que la información brindada es la que corresponde al Acudiente del Estudiante (Nombres y Apellidos del Estudiante) y autoriza al tratamiento de los datos para vincularlos en la inscripción, seguimiento y envío de material del Programa Aprende en Casa con MALOKA de acuerdo a nuestra Política de Tratamiento de Datos Personales?</label>
                        <select  class="form-control input-sm select2"  style="width: 100%;" name="G1905_C34986" id="G1905_C34986">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1970 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34987" id="LblG1905_C34987">Nombre de la Persona que Actualizo y Confirmo los datos</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34987" value="<?php if (isset($_GET['G1905_C34987'])) {
                            echo $_GET['G1905_C34987'];
                        } ?>"  name="G1905_C34987"  placeholder="Nombre de la Persona que Actualizo y Confirmo los datos">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
                    <!-- CAMPO TIPO TEXTO -->
                    <div class="form-group">
                        <label for="G1905_C34988" id="LblG1905_C34988">Parentesco</label>
                        <input type="text" class="form-control input-sm" id="G1905_C34988" value="<?php if (isset($_GET['G1905_C34988'])) {
                            echo $_GET['G1905_C34988'];
                        } ?>"  name="G1905_C34988"  placeholder="Parentesco">
                    </div>
                    <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
                    <!-- CAMPO TIPO MEMO -->
                    <div class="form-group">
                        <label for="G1905_C34989" id="LblG1905_C34989">OBSERVACIONES </label>
                        <textarea class="form-control input-sm" name="G1905_C34989" id="G1905_C34989"  value="<?php if (isset($_GET['G1905_C34989'])) {
                            echo $_GET['G1905_C34989'];
                        } ?>" placeholder="OBSERVACIONES "></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO MEMO -->
  
            </div> <!-- AQUIFINCAMPO -->

  
        </div> 


        </div>
    </div> <!-- AQUIFINSECCION -->
</div>

<div class="row" style="background-color: #FAFAFA; ">
    <br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-7 col-xs-7">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1905_C34953">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 1969;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <div class="col-md-3 col-xs-3" style="text-align: center;">
        <button class="btn btn-success btn-block SaveHere" id="Save2" type="button" seguir="si">
            Guardar y Seguir en Llamada.
        </button>
    </div>

    <div class="col-md-2 col-xs-2" style="text-align: center;">
        <button class="btn btn-primary btn-block SaveHere" id="Save" seguir="no" type="button">
            Guardar y Colgar.
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G1905_C34953">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 1969;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."' >".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
            <input type="hidden" name="ContactoMonoEf" id="ContactoMonoEf" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; <?php if(isset($_GET['sentido']) && $_GET['sentido'] == '2'){ echo ""; } ?> ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G1905_C34954">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G1905_C34955" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G1905_C34956" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
</div>
<div class="row" style="background-color: #FAFAFA;">
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G1905_C34957" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->

<?php 

    include("pies.php");

?>
<script type="text/javascript" src="formularios/G1905/G1905_eventos.js"></script> 
<script type="text/javascript">
    $(function(){
    // JDBD Envio de calificacion por correo.
    //NBG - Esto es para mostrar la sección de calidad solo cuando se ingrese por esta
    //////////////////////////////////////////////////////////////////////////////////
        
    
<?php
    //JDBD - validamos que no estemos en la estacion
    if(!isset($_GET["id_gestion_cbx"])){
        //JDBD - validamos que estemos en el modulo calidad
        if(isset($_SESSION["QUALITY"]) && $_SESSION["QUALITY"] ==1){
            //JDBD - validamos que tenga permisos para acceder a calidad.
            if(isset($_SESSION["CARGO"]) && ($_SESSION["CARGO"] == "calidad" || $_SESSION["CARGO"] == "administrador" || $_SESSION["CARGO"] == "super-administrador")){?>
    <?php   }
        }
    }
?>      
    
    //JDBD - Esta seccion es solo para la interaccion con el formulario Padre
    /////////////////////////////////////////////////////////////////////////
    <?php if(isset($_GET["yourfather"]) && isset($_GET["idFather"]) && isset($_GET["pincheCampo"])){ ?>
        <?php if($_GET["yourfather"] != "NULL"){ ?>
            $("#G1905_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['yourfather'];?>");
        <?php }else{ ?>
            if(document.getElementById("G1905_C<?=$_GET['pincheCampo'];?>").type == "select-one"){
                $.ajax({
                    url      : '<?=$url_crud;?>?Combo_Guion_G<?php echo $_GET['formulario'];?>_C<?php echo $_GET['pincheCampo']; ?>=si',
                    type     : 'POST',
                    data     : { q : <?php echo $_GET["idFather"]; ?> },
                    success  : function(data){
                        $("#G<?php echo $_GET["formulario"]; ?>_C<?php echo $_GET["pincheCampo"]; ?>").html(data);
                    }
                });
            }else{
                $("#G1905_C<?=$_GET['pincheCampo'];?>").val("<?=$_GET['idFather'];?>");
            }
        <?php } ?>
    <?php } ?>
    /////////////////////////////////////////////////////////////////////////
    <?php if (!isset($_GET["view"])) {?>
        $("#add").click(function(){
                         
        });
    <?php } ?>;
    var meses = new Array(12);
    meses[0] = "01";
    meses[1] = "02";
    meses[2] = "03";
    meses[3] = "04";
    meses[4] = "05";
    meses[5] = "06";
    meses[6] = "07";
    meses[7] = "08";
    meses[8] = "09";
    meses[9] = "10";
    meses[10] = "11";
    meses[11] = "12";

    var d = new Date();
    var h = d.getHours();
    var horas = (h < 10) ? '0' + h : h;
    var dia = d.getDate();
    var dias = (dia < 10) ? '0' + dia : dia;
    var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
    $("#FechaInicio").val(fechaInicial);
            

    //Esta es por si lo llaman en modo formulario de edicion LigthBox
    <?php if(isset($_GET['registroId'])){ ?>
    $.ajax({
        url      : '<?=$url_crud;?>',
        type     : 'POST',
        data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
        dataType : 'json',
        success  : function(data){
            //recorrer datos y enviarlos al formulario
            $.each(data, function(i, item) {
                     
                $("#G1905_C34964").val(item.G1905_C34964); 
                $("#G1905_C34965").val(item.G1905_C34965); 
                $("#G1905_C34966").val(item.G1905_C34966); 
                $("#G1905_C34967").val(item.G1905_C34967); 
                $("#G1905_C34968").val(item.G1905_C34968); 
                $("#G1905_C34953").val(item.G1905_C34953).trigger("change");  
                $("#G1905_C34954").val(item.G1905_C34954).trigger("change");  
                $("#G1905_C34955").val(item.G1905_C34955); 
                $("#G1905_C34956").val(item.G1905_C34956); 
                $("#G1905_C34957").val(item.G1905_C34957); 
                $("#G1905_C34958").val(item.G1905_C34958); 
                $("#G1905_C34959").val(item.G1905_C34959); 
                $("#G1905_C34960").val(item.G1905_C34960); 
                $("#G1905_C34961").val(item.G1905_C34961); 
                $("#G1905_C34962").val(item.G1905_C34962); 
                $("#G1905_C34963").val(item.G1905_C34963); 
                $("#G1905_C35095").val(item.G1905_C35095); 
                $("#G1905_C35096").val(item.G1905_C35096); 
                $("#G1905_C34969").val(item.G1905_C34969); 
                $("#G1905_C34970").val(item.G1905_C34970); 
                $("#G1905_C34971").val(item.G1905_C34971); 
                $("#G1905_C34972").val(item.G1905_C34972).trigger("change");  
                $("#G1905_C34973").val(item.G1905_C34973); 
                $("#G1905_C34974").val(item.G1905_C34974); 
                $("#G1905_C34975").val(item.G1905_C34975); 
                $("#G1905_C34976").val(item.G1905_C34976); 
                $("#G1905_C34977").val(item.G1905_C34977); 
                $("#G1905_C34978").val(item.G1905_C34978); 
                $("#G1905_C34979").val(item.G1905_C34979); 
                $("#G1905_C34983").val(item.G1905_C34983); 
                $("#G1905_C34982").val(item.G1905_C34982); 
                $("#G1905_C34980").val(item.G1905_C34980); 
                $("#G1905_C34981").val(item.G1905_C34981); 
                $("#G1905_C34984").val(item.G1905_C34984).trigger("change");  
                $("#G1905_C34985").val(item.G1905_C34985).trigger("change");  
                $("#G1905_C34986").val(item.G1905_C34986).trigger("change");  
                $("#G1905_C34987").val(item.G1905_C34987); 
                $("#G1905_C34988").val(item.G1905_C34988); 
                $("#G1905_C34989").val(item.G1905_C34989);   
                if(item.G1905_C35127 == 1){
                    $("#G1905_C35127").attr('checked', true);
                }    
                if(item.G1905_C35128 == 1){
                    $("#G1905_C35128").attr('checked', true);
                }    
                if(item.G1905_C35129 == 1){
                    $("#G1905_C35129").attr('checked', true);
                }    
                if(item.G1905_C35130 == 1){
                    $("#G1905_C35130").attr('checked', true);
                }    
                if(item.G1905_C35131 == 1){
                    $("#G1905_C35131").attr('checked', true);
                }    
                if(item.G1905_C35132 == 1){
                    $("#G1905_C35132").attr('checked', true);
                }    
                if(item.G1905_C35133 == 1){
                    $("#G1905_C35133").attr('checked', true);
                }    
                if(item.G1905_C35134 == 1){
                    $("#G1905_C35134").attr('checked', true);
                }    
                if(item.G1905_C35135 == 1){
                    $("#G1905_C35135").attr('checked', true);
                }    
                if(item.G1905_C35136 == 1){
                    $("#G1905_C35136").attr('checked', true);
                }    
                if(item.G1905_C35137 == 1){
                    $("#G1905_C35137").attr('checked', true);
                }    
                if(item.G1905_C35138 == 1){
                    $("#G1905_C35138").attr('checked', true);
                }    
                if(item.G1905_C35139 == 1){
                    $("#G1905_C35139").attr('checked', true);
                }    
                if(item.G1905_C35140 == 1){
                    $("#G1905_C35140").attr('checked', true);
                }    
                if(item.G1905_C35141 == 1){
                    $("#G1905_C35141").attr('checked', true);
                }    
                if(item.G1905_C35142 == 1){
                    $("#G1905_C35142").attr('checked', true);
                }    
                if(item.G1905_C35143 == 1){
                    $("#G1905_C35143").attr('checked', true);
                }    
                if(item.G1905_C35144 == 1){
                    $("#G1905_C35144").attr('checked', true);
                }    
                if(item.G1905_C35145 == 1){
                    $("#G1905_C35145").attr('checked', true);
                }    
                if(item.G1905_C35146 == 1){
                    $("#G1905_C35146").attr('checked', true);
                }    
                if(item.G1905_C35147 == 1){
                    $("#G1905_C35147").attr('checked', true);
                }    
                if(item.G1905_C35148 == 1){
                    $("#G1905_C35148").attr('checked', true);
                }    
                if(item.G1905_C35149 == 1){
                    $("#G1905_C35149").attr('checked', true);
                }    
                if(item.G1905_C35150 == 1){
                    $("#G1905_C35150").attr('checked', true);
                }  
                $("#G1905_C35153").val(item.G1905_C35153).trigger("change"); 
                
                $("#h3mio").html(item.principal);

            });

            //Deshabilitar los campos 3

            //Habilitar todos los campos para edicion
            $('#FormularioDatos :input').each(function(){
                $(this).attr('disabled', true);
            });              

            //Habilidar los botones de operacion, add, editar, eliminar
            $("#add").attr('disabled', false);
            $("#edit").attr('disabled', false);
            $("#delete").attr('disabled', false);

            //Desahabiliatra los botones de salvar y seleccionar_registro
            $("#cancel").attr('disabled', true);
            $("#Save").attr('disabled', true);
        } 
    });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

        $("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
            /*
            vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
            idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G1905_C35153").select2();

    $("#G1905_C34972").select2();

    $("#G1905_C34984").select2();

    $("#G1905_C34985").select2();

    $("#G1905_C34986").select2();
                $("#G1905_C34953").change(function(){
                    $(".ReqForTip").closest(".form-group").removeClass("has-error");
                    $(".ReqForTip").removeClass("ReqForTip");
                        
                    if($(this).val() == "170208"){
                        $("#G1905_C34979").addClass("ReqForTip");
                    }
                    if($(this).val() == "170208"){
                        $("#G1905_C34972").addClass("ReqForTip");
                    }
                    if($(this).val() == "170208"){
                        $("#G1905_C34984").addClass("ReqForTip");
                    }
                    if($(this).val() == "170208"){
                        $("#G1905_C34985").addClass("ReqForTip");
                    }
                    if($(this).val() == "170208"){
                        $("#G1905_C34986").addClass("ReqForTip");
                    }
                    if($(this).val() == "170208"){
                        $("#G1905_C34987").addClass("ReqForTip");
                    }
                    if($(this).val() == "170208"){
                        $("#G1905_C34988").addClass("ReqForTip");
                    }
                    if($(this).val() == "170209"){
                        $("#G1905_C34979").addClass("ReqForTip");
                    }
                    if($(this).val() == "170209"){
                        $("#G1905_C34972").addClass("ReqForTip");
                    }
                    if($(this).val() == "170209"){
                        $("#G1905_C34984").addClass("ReqForTip");
                    }
                    if($(this).val() == "170209"){
                        $("#G1905_C34985").addClass("ReqForTip");
                    }
                    if($(this).val() == "170209"){
                        $("#G1905_C34986").addClass("ReqForTip");
                    }
                    if($(this).val() == "170209"){
                        $("#G1905_C34987").addClass("ReqForTip");
                    }
                    if($(this).val() == "170209"){
                        $("#G1905_C34988").addClass("ReqForTip");
                    }
                    if($(this).val() == "170210"){
                        $("#G1905_C34979").addClass("ReqForTip");
                    }
                    if($(this).val() == "170210"){
                        $("#G1905_C34972").addClass("ReqForTip");
                    }
                    if($(this).val() == "170210"){
                        $("#G1905_C34984").addClass("ReqForTip");
                    }
                    if($(this).val() == "170210"){
                        $("#G1905_C34985").addClass("ReqForTip");
                    }
                    if($(this).val() == "170210"){
                        $("#G1905_C34986").addClass("ReqForTip");
                    }
                    if($(this).val() == "170210"){
                        $("#G1905_C34987").addClass("ReqForTip");
                    }
                    if($(this).val() == "170210"){
                        $("#G1905_C34988").addClass("ReqForTip");
                    }
                    if($(this).val() == "170211"){
                        $("#G1905_C34979").addClass("ReqForTip");
                    }
                    if($(this).val() == "170211"){
                        $("#G1905_C34972").addClass("ReqForTip");
                    }
                    if($(this).val() == "170211"){
                        $("#G1905_C34984").addClass("ReqForTip");
                    }
                    if($(this).val() == "170211"){
                        $("#G1905_C34985").addClass("ReqForTip");
                    }
                    if($(this).val() == "170211"){
                        $("#G1905_C34986").addClass("ReqForTip");
                    }
                    if($(this).val() == "170211"){
                        $("#G1905_C34987").addClass("ReqForTip");
                    }
                    if($(this).val() == "170211"){
                        $("#G1905_C34988").addClass("ReqForTip");
                    }
                    if($(this).val() == "170212"){
                        $("#G1905_C34979").addClass("ReqForTip");
                    }
                    if($(this).val() == "170212"){
                        $("#G1905_C34972").addClass("ReqForTip");
                    }
                    if($(this).val() == "170212"){
                        $("#G1905_C34984").addClass("ReqForTip");
                    }
                    if($(this).val() == "170212"){
                        $("#G1905_C34985").addClass("ReqForTip");
                    }
                    if($(this).val() == "170212"){
                        $("#G1905_C34986").addClass("ReqForTip");
                    }
                    if($(this).val() == "170212"){
                        $("#G1905_C34987").addClass("ReqForTip");
                    }
                    if($(this).val() == "170212"){
                        $("#G1905_C34988").addClass("ReqForTip");
                    }
                });
        //datepickers
        

        $("#G1905_C34955").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Agenda', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G1905_C34956").wickedpicker(options);

        //Validaciones numeros Enteros
        


        //Validaciones numeros Decimales
        


        /* Si son d formulas */
        


        //Si tienen dependencias

        


    //function para Solución de dudas y objeciones 

    $("#G1905_C35153").change(function(){  
        //Esto es la parte de las listas dependientes
        var respuesta =  $("#G1905_C35153 option:selected").attr('respuesta');
        $("#respuesta_G1905_C35153").val(respuesta);
        

    });

    //function para ¿Se actualizo la dirección? 

    $("#G1905_C34972").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Cuentan con conexión a internet en el hogar? 

    $("#G1905_C34984").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿El/la estudiante cuenta con acceso a herramientas tecnológicas como computador o tablet? 

    $("#G1905_C34985").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Confirma usted que la información brindada es la que corresponde al Acudiente del Estudiante (Nombres y Apellidos del Estudiante) y autoriza al tratamiento de los datos para vincularlos en la inscripción, seguimiento y envío de material del Programa Aprende en Casa con MALOKA de acuerdo a nuestra Política de Tratamiento de Datos Personales? 

    $("#G1905_C34986").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
        
        //Funcionalidad del botob guardar
        


        $(".SaveHere").click(function(){
            var SaveHere = $(this).attr("seguir");
            var bol_respuesta = before_save();
            var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
                alertify.error("Es necesaria la tipificación!");
                valido = 1;
            }

            $(".saltoRequerido").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            valido = 1;
                        }
                    }
                }
            });

            $(".ReqForTip").each(function() {
                if ($(this).prop("disabled")==false) {
                    if (this.type == "select-one") {
                        if ($(this).val() == 0 || $(this).val() == null || $(this).val()== -1) {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("La lista debe ser diligenciada.");
                            valido = 1;
                        }
                    }else{
                        if ($(this).val()=="") {
                            $(this).closest(".form-group").addClass("has-error");
                            alertify.error("El campo debe ser diligenciado.");
                            valido = 1;
                        }
                    }
                }
            });

            if($(".reintento").val() == '2'){
                if($(".TxtFechaReintento").val().length < 1){
                    alertify.error("Es necesario llenar la fecha de reintento!");
                    $(".TxtFechaReintento").focus();
                    valido = 1;
                }

                if($(".TxtHoraReintento").val().length < 1){
                    alertify.error("Es necesario llenar la hora de reintento!");
                    $(".TxtHoraReintento").focus();
                    valido = 1;
                }
            }

            if(valido == '0'){
                $(this).attr("disabled",true);
                if(bol_respuesta){            
                    var form = $("#FormularioDatos");
                    //Se crean un array con los datos a enviar, apartir del formulario 
                    var formData = new FormData($("#FormularioDatos")[0]);
                    formData.append("SaveHere",SaveHere);
                    $.ajax({
                       url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; } else{ echo "0"; } ?>',  
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        //una vez finalizado correctamente
                        success: function(data){
                            if(data != '0'){
                                <?php if(!isset($_GET['campan'])){ ?>
                                    //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
                                    if($("#oper").val() == 'add'){
                                        idTotal = data;
                                    }else{
                                        idTotal= $("#hidId").val();
                                    }
                                   
                                    //Limpiar formulario
                                    form[0].reset();
                                    after_save();
                                    <?php if(isset($_GET['registroId'])){ ?>
                                        var ID = <?=$_GET['registroId'];?>
                                    <?php }else{ ?> 
                                        var ID = data
                                    <?php } ?>  
                                    $.ajax({
                                        url      : '<?=$url_crud;?>',
                                        type     : 'POST',
                                        data     : { CallDatos : 'SI', id : ID },
                                        dataType : 'json',
                                        success  : function(data){
                                            //recorrer datos y enviarlos al formulario
                                            $.each(data, function(i, item) {
                                            
 
                                                $("#G1905_C34964").val(item.G1905_C34964);
 
                                                $("#G1905_C34965").val(item.G1905_C34965);
 
                                                $("#G1905_C34966").val(item.G1905_C34966);
 
                                                $("#G1905_C34967").val(item.G1905_C34967);
 
                                                $("#G1905_C34968").val(item.G1905_C34968);
 
                    $("#G1905_C34953").val(item.G1905_C34953).trigger("change"); 
 
                    $("#G1905_C34954").val(item.G1905_C34954).trigger("change"); 
 
                                                $("#G1905_C34955").val(item.G1905_C34955);
 
                                                $("#G1905_C34956").val(item.G1905_C34956);
 
                                                $("#G1905_C34957").val(item.G1905_C34957);
 
                                                $("#G1905_C34958").val(item.G1905_C34958);
 
                                                $("#G1905_C34959").val(item.G1905_C34959);
 
                                                $("#G1905_C34960").val(item.G1905_C34960);
 
                                                $("#G1905_C34961").val(item.G1905_C34961);
 
                                                $("#G1905_C34962").val(item.G1905_C34962);
 
                                                $("#G1905_C34963").val(item.G1905_C34963);
 
                                                $("#G1905_C35095").val(item.G1905_C35095);
 
                                                $("#G1905_C35096").val(item.G1905_C35096);
 
                                                $("#G1905_C34969").val(item.G1905_C34969);
 
                                                $("#G1905_C34970").val(item.G1905_C34970);
 
                                                $("#G1905_C34971").val(item.G1905_C34971);
 
                    $("#G1905_C34972").val(item.G1905_C34972).trigger("change"); 
 
                                                $("#G1905_C34973").val(item.G1905_C34973);
 
                                                $("#G1905_C34974").val(item.G1905_C34974);
 
                                                $("#G1905_C34975").val(item.G1905_C34975);
 
                                                $("#G1905_C34976").val(item.G1905_C34976);
 
                                                $("#G1905_C34977").val(item.G1905_C34977);
 
                                                $("#G1905_C34978").val(item.G1905_C34978);
 
                                                $("#G1905_C34979").val(item.G1905_C34979);
 
                                                $("#G1905_C34983").val(item.G1905_C34983);
 
                                                $("#G1905_C34982").val(item.G1905_C34982);
 
                                                $("#G1905_C34980").val(item.G1905_C34980);
 
                                                $("#G1905_C34981").val(item.G1905_C34981);
 
                    $("#G1905_C34984").val(item.G1905_C34984).trigger("change"); 
 
                    $("#G1905_C34985").val(item.G1905_C34985).trigger("change"); 
 
                    $("#G1905_C34986").val(item.G1905_C34986).trigger("change"); 
 
                                                $("#G1905_C34987").val(item.G1905_C34987);
 
                                                $("#G1905_C34988").val(item.G1905_C34988);
 
                                                $("#G1905_C34989").val(item.G1905_C34989);
      
                                                if(item.G1905_C35127 == 1){
                                                   $("#G1905_C35127").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35128 == 1){
                                                   $("#G1905_C35128").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35129 == 1){
                                                   $("#G1905_C35129").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35130 == 1){
                                                   $("#G1905_C35130").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35131 == 1){
                                                   $("#G1905_C35131").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35132 == 1){
                                                   $("#G1905_C35132").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35133 == 1){
                                                   $("#G1905_C35133").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35134 == 1){
                                                   $("#G1905_C35134").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35135 == 1){
                                                   $("#G1905_C35135").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35136 == 1){
                                                   $("#G1905_C35136").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35137 == 1){
                                                   $("#G1905_C35137").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35138 == 1){
                                                   $("#G1905_C35138").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35139 == 1){
                                                   $("#G1905_C35139").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35140 == 1){
                                                   $("#G1905_C35140").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35141 == 1){
                                                   $("#G1905_C35141").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35142 == 1){
                                                   $("#G1905_C35142").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35143 == 1){
                                                   $("#G1905_C35143").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35144 == 1){
                                                   $("#G1905_C35144").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35145 == 1){
                                                   $("#G1905_C35145").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35146 == 1){
                                                   $("#G1905_C35146").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35147 == 1){
                                                   $("#G1905_C35147").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35148 == 1){
                                                   $("#G1905_C35148").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35149 == 1){
                                                   $("#G1905_C35149").attr('checked', true);
                                                } 
      
                                                if(item.G1905_C35150 == 1){
                                                   $("#G1905_C35150").attr('checked', true);
                                                } 
 
                    $("#G1905_C35153").val(item.G1905_C35153).trigger("change"); 
                                                $("#h3mio").html(item.principal);
                                            });

                                            //Deshabilitar los campos 2

                                            //Habilitar todos los campos para edicion
                                            $('#FormularioDatos :input').each(function(){
                                                $(this).attr('disabled', true);
                                            });

                                            //Habilidar los botones de operacion, add, editar, eliminar
                                            $("#add").attr('disabled', false);
                                            $("#edit").attr('disabled', false);
                                            $("#delete").attr('disabled', false);

                                            //Desahabiliatra los botones de salvar y seleccionar_registro
                                            $("#cancel").attr('disabled', true);
                                            $("#Save").attr('disabled', true);
                                        } 
                                    })
                                    $("#hidId").val(ID);  

                                <?php }else{ 
                                    if(!isset($_GET['formulario'])){
                                ?>

                                    $.ajax({
                                        url   : 'formularios/G1905/PHP_Ejecutar.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana_crm=<?php if(isset($_GET['campana_crm'])){ echo $_GET['campana_crm']; }else{ echo "0"; } ?><?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['id_llamada_original'])) { echo "&id_llamada_original=".$_GET['id_llamada_original']; }?>',
                                        type  : "post",
                                        data  : formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success : function(xt){
                                            console.log("|"+xt+"|");

                                            if (SaveHere == "si") {
                                                
                                                window.location.href = "formularios/G1905/quitar.php?token=<?=$token;?>&id_gestion_cbx=<?=$_GET['id_gestion_cbx'];?>&campana_crm=<?=$_GET['campana_crm'];?>&colgar=no&id_llamada_original="+xt;

                                            }else{

                                                window.location.href = "formularios/G1905/quitar.php";
                                                
                                            }
                                        }
                                    });
                                    
                
                                <?php } 
                                    }
                                ?>            
                            }else{
                                //Algo paso, hay un error
                                $("#Save").attr('disabled', false);
                                alertify.error('Un error ha ocurrido y no pudimos guardar la información');
                            }                
                        },
                        //si ha ocurrido un error
                        error: function(){
                            after_save_error();
                            $("#Save").attr('disabled', false);
                            alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
                        }
                    });
                }
            }
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        




    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','Nombre del Estudiante','Tipo de Documento','Número de Identificación','Grado Actual','Colegio','Agente','Fecha','Hora','Campaña','Nombre del Acudiente','Persona Autorizada para recibir','Parentezco del acudiente','Parentezco de quien recibe','LUGAR DE ENTREGA (Casa/ Colegio)','Dirección de Residencia Del Estudiante','ZONA SDP','¿Se actualizo la dirección?','Dirección Actualizada del Estudiante','Especificaciones de Dirección (Torre, manzana etc)','Ciudad','Localidad','Barrio','Dirección de Trabajo del Acudiente (solo si aplica)','Correo Electrónico','Teléfono celular 1','Teléfono Celular 2','Teléfono fijo 1','Teléfono fijo 2','¿Cuentan con conexión a internet en el hogar?','¿El/la estudiante cuenta con acceso a herramientas tecnológicas como computador o tablet?','¿Confirma usted que la información brindada es la que corresponde al Acudiente del Estudiante (Nombres y Apellidos del Estudiante) y autoriza al tratamiento de los datos para vincularlos en la inscripción, seguimiento y envío de material del Programa Aprende en Casa con MALOKA de acuerdo a nuestra Política de Tratamiento de Datos Personales?','Nombre de la Persona que Actualizo y Confirmo los datos','Parentesco','OBSERVACIONES ','Solución de dudas y objeciones'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

                    ,
                    { 
                        name:'G1905_C34964', 
                        index: 'G1905_C34964', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34965', 
                        index: 'G1905_C34965', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34966', 
                        index: 'G1905_C34966', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34967', 
                        index: 'G1905_C34967', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34968', 
                        index: 'G1905_C34968', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34958', 
                        index: 'G1905_C34958', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34959', 
                        index: 'G1905_C34959', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34960', 
                        index: 'G1905_C34960', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34961', 
                        index: 'G1905_C34961', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34962', 
                        index: 'G1905_C34962', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34963', 
                        index: 'G1905_C34963', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C35095', 
                        index: 'G1905_C35095', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C35096', 
                        index: 'G1905_C35096', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34969', 
                        index: 'G1905_C34969', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34970', 
                        index: 'G1905_C34970', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34971', 
                        index: 'G1905_C34971', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34972', 
                        index:'G1905_C34972', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1970&campo=G1905_C34972'
                        }
                    }

                    ,
                    { 
                        name:'G1905_C34973', 
                        index: 'G1905_C34973', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34974', 
                        index: 'G1905_C34974', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34975', 
                        index: 'G1905_C34975', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34976', 
                        index: 'G1905_C34976', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34977', 
                        index: 'G1905_C34977', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34978', 
                        index: 'G1905_C34978', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34979', 
                        index: 'G1905_C34979', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34983', 
                        index: 'G1905_C34983', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34982', 
                        index: 'G1905_C34982', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34980', 
                        index: 'G1905_C34980', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34981', 
                        index: 'G1905_C34981', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34984', 
                        index:'G1905_C34984', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1970&campo=G1905_C34984'
                        }
                    }

                    ,
                    { 
                        name:'G1905_C34985', 
                        index:'G1905_C34985', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1970&campo=G1905_C34985'
                        }
                    }

                    ,
                    { 
                        name:'G1905_C34986', 
                        index:'G1905_C34986', 
                        width:120 ,
                        editable: true, 
                        edittype:"select" , 
                        editoptions: {
                            dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=1970&campo=G1905_C34986'
                        }
                    }

                    ,
                    { 
                        name:'G1905_C34987', 
                        index: 'G1905_C34987', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34988', 
                        index: 'G1905_C34988', 
                        width:160, 
                        resizable:false, 
                        sortable:true , 
                        editable: true 
                    }

                    ,
                    { 
                        name:'G1905_C34989', 
                        index:'G1905_C34989', 
                        width:150, 
                        editable: true 
                    }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G1905_C34964',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //JDBD-2020-05-03 : Nueva funcion de filtro Avanzado y Scroll. 
    function llenarListaNavegacion(strScroll_p,intInicio_p,intFin_p){

        var strHTMLTr_t = "";
        var arrNumerosFiltros_t = new Array();

        $(".rows").each(function(i){
            arrNumerosFiltros_t[i]=$(this).attr("numero");
        });

        if (arrNumerosFiltros_t.length > 0) {

            var objFormFiltros_t = new FormData($("#forBusquedaAvanzada")[0]);
            objFormFiltros_t.append("arrNumerosFiltros_t",arrNumerosFiltros_t);
            objFormFiltros_t.append("CallDatosJson","SI");
            objFormFiltros_t.append("strScroll_t",strScroll_p);
            objFormFiltros_t.append("inicio_t",intInicio_p);
            objFormFiltros_t.append("fin_t",intFin_p);
            objFormFiltros_t.append("idUsuario",<?=$idUsuario;?>);
            objFormFiltros_t.append("tareaBackoffice",<?=$tareaBackoffice;?>);
            objFormFiltros_t.append("muestra",<?=$muestra;?>);
            objFormFiltros_t.append("tareaTipoDist",<?=$tipoDistribucion;?>);

            $.ajax({
                url         : '<?=$url_crud;?>',
                type        : 'POST',
                data        : objFormFiltros_t,
                cache       : false,
                contentType : false,
                processData : false,
                dataType    : 'json',
                success  : function(data){

                    $.each(data, function(i, item){
                        strHTMLTr_t += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                        strHTMLTr_t += "<td>";
                        strHTMLTr_t += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                        strHTMLTr_t += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                        strHTMLTr_t += "</td>";
                        strHTMLTr_t += "</tr>";
                    });


                    if (strScroll_p == "no") {
                        $("#tablaScroll").html(strHTMLTr_t);

                        //JDBD - Activamos el click a los nuevos <tr>.
                        busqueda_lista_navegacion();

                        if ( $("#"+idTotal).length > 0) {
                            //JDBD - Damos click al al registro siexiste.
                            $("#"+idTotal).click();
                            $("#"+idTotal).addClass('active'); 
                        }else{
                            //JDBD - Damos click al primer registro de la lista.
                            $(".CargarDatos :first").click();
                        }
                    }else{
                        $("#tablaScroll").append(strHTMLTr_t);
                        busqueda_lista_navegacion();
                    }
                }
            });

        }

    }

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(B,A=null,T=null,F=null,E=null){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', B : B, A : A, T : T, F : F, E : E, idUsuario : <?=$idUsuario;?>, tareaBackoffice: <?php echo $tareaBackoffice; ?>, muestra: <?php echo $muestra; ?>, tareaTipoDist: <?php echo $tipoDistribucion ?>},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');

            $("#IdGestion").val(id);

            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        

                        $("#G1905_C34964").val(item.G1905_C34964);

                        $("#G1905_C34965").val(item.G1905_C34965);

                        $("#G1905_C34966").val(item.G1905_C34966);

                        $("#G1905_C34967").val(item.G1905_C34967);

                        $("#G1905_C34968").val(item.G1905_C34968);
 
                    $("#G1905_C34953").val(item.G1905_C34953).trigger("change"); 
 
                    $("#G1905_C34954").val(item.G1905_C34954).trigger("change"); 

                        $("#G1905_C34955").val(item.G1905_C34955);

                        $("#G1905_C34956").val(item.G1905_C34956);

                        $("#G1905_C34957").val(item.G1905_C34957);

                        $("#G1905_C34958").val(item.G1905_C34958);

                        $("#G1905_C34959").val(item.G1905_C34959);

                        $("#G1905_C34960").val(item.G1905_C34960);

                        $("#G1905_C34961").val(item.G1905_C34961);

                        $("#G1905_C34962").val(item.G1905_C34962);

                        $("#G1905_C34963").val(item.G1905_C34963);

                        $("#G1905_C35095").val(item.G1905_C35095);

                        $("#G1905_C35096").val(item.G1905_C35096);

                        $("#G1905_C34969").val(item.G1905_C34969);

                        $("#G1905_C34970").val(item.G1905_C34970);

                        $("#G1905_C34971").val(item.G1905_C34971);
 
                    $("#G1905_C34972").val(item.G1905_C34972).trigger("change"); 

                        $("#G1905_C34973").val(item.G1905_C34973);

                        $("#G1905_C34974").val(item.G1905_C34974);

                        $("#G1905_C34975").val(item.G1905_C34975);

                        $("#G1905_C34976").val(item.G1905_C34976);

                        $("#G1905_C34977").val(item.G1905_C34977);

                        $("#G1905_C34978").val(item.G1905_C34978);

                        $("#G1905_C34979").val(item.G1905_C34979);

                        $("#G1905_C34983").val(item.G1905_C34983);

                        $("#G1905_C34982").val(item.G1905_C34982);

                        $("#G1905_C34980").val(item.G1905_C34980);

                        $("#G1905_C34981").val(item.G1905_C34981);
 
                    $("#G1905_C34984").val(item.G1905_C34984).trigger("change"); 
 
                    $("#G1905_C34985").val(item.G1905_C34985).trigger("change"); 
 
                    $("#G1905_C34986").val(item.G1905_C34986).trigger("change"); 

                        $("#G1905_C34987").val(item.G1905_C34987);

                        $("#G1905_C34988").val(item.G1905_C34988);

                        $("#G1905_C34989").val(item.G1905_C34989);
    
                        if(item.G1905_C35127 == 1){
                           $("#G1905_C35127").attr('checked', true);
                        } 
    
                        if(item.G1905_C35128 == 1){
                           $("#G1905_C35128").attr('checked', true);
                        } 
    
                        if(item.G1905_C35129 == 1){
                           $("#G1905_C35129").attr('checked', true);
                        } 
    
                        if(item.G1905_C35130 == 1){
                           $("#G1905_C35130").attr('checked', true);
                        } 
    
                        if(item.G1905_C35131 == 1){
                           $("#G1905_C35131").attr('checked', true);
                        } 
    
                        if(item.G1905_C35132 == 1){
                           $("#G1905_C35132").attr('checked', true);
                        } 
    
                        if(item.G1905_C35133 == 1){
                           $("#G1905_C35133").attr('checked', true);
                        } 
    
                        if(item.G1905_C35134 == 1){
                           $("#G1905_C35134").attr('checked', true);
                        } 
    
                        if(item.G1905_C35135 == 1){
                           $("#G1905_C35135").attr('checked', true);
                        } 
    
                        if(item.G1905_C35136 == 1){
                           $("#G1905_C35136").attr('checked', true);
                        } 
    
                        if(item.G1905_C35137 == 1){
                           $("#G1905_C35137").attr('checked', true);
                        } 
    
                        if(item.G1905_C35138 == 1){
                           $("#G1905_C35138").attr('checked', true);
                        } 
    
                        if(item.G1905_C35139 == 1){
                           $("#G1905_C35139").attr('checked', true);
                        } 
    
                        if(item.G1905_C35140 == 1){
                           $("#G1905_C35140").attr('checked', true);
                        } 
    
                        if(item.G1905_C35141 == 1){
                           $("#G1905_C35141").attr('checked', true);
                        } 
    
                        if(item.G1905_C35142 == 1){
                           $("#G1905_C35142").attr('checked', true);
                        } 
    
                        if(item.G1905_C35143 == 1){
                           $("#G1905_C35143").attr('checked', true);
                        } 
    
                        if(item.G1905_C35144 == 1){
                           $("#G1905_C35144").attr('checked', true);
                        } 
    
                        if(item.G1905_C35145 == 1){
                           $("#G1905_C35145").attr('checked', true);
                        } 
    
                        if(item.G1905_C35146 == 1){
                           $("#G1905_C35146").attr('checked', true);
                        } 
    
                        if(item.G1905_C35147 == 1){
                           $("#G1905_C35147").attr('checked', true);
                        } 
    
                        if(item.G1905_C35148 == 1){
                           $("#G1905_C35148").attr('checked', true);
                        } 
    
                        if(item.G1905_C35149 == 1){
                           $("#G1905_C35149").attr('checked', true);
                        } 
    
                        if(item.G1905_C35150 == 1){
                           $("#G1905_C35150").attr('checked', true);
                        } 
 
                    $("#G1905_C35153").val(item.G1905_C35153).trigger("change"); 
                        
            $("#h3mio").html(item.principal);
                        
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                },complete : function(data){
                    
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        <?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ ?>
                            document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                    <?php  
                        }   
                    }
                    
                } */  
            }
        ?>
        <?php if(isset($_GET['user'])){ ?>
            
            idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
        
    });
</script>
