<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G1905_ConsInte__b, G1905_FechaInsercion , G1905_Usuario ,  G1905_CodigoMiembro  , G1905_PoblacionOrigen , G1905_EstadoDiligenciamiento ,  G1905_IdLlamada , G1905_C34964 as principal ,G1905_C34964,G1905_C34965,G1905_C34966,G1905_C34967,G1905_C34968,G1905_C34953,G1905_C34954,G1905_C34955,G1905_C34956,G1905_C34957,G1905_C34958,G1905_C34959,G1905_C34960,G1905_C34961,G1905_C34962,G1905_C34963,G1905_C35095,G1905_C35096,G1905_C34969,G1905_C34970,G1905_C34971,G1905_C34972,G1905_C34973,G1905_C34974,G1905_C34975,G1905_C34976,G1905_C34977,G1905_C34978,G1905_C34979,G1905_C34983,G1905_C34982,G1905_C34980,G1905_C34981,G1905_C34984,G1905_C34985,G1905_C34986,G1905_C34987,G1905_C34988,G1905_C34989,G1905_C35153 FROM '.$BaseDatos.'.G1905 WHERE G1905_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G1905_C34964'] = $key->G1905_C34964;

                $datos[$i]['G1905_C34965'] = $key->G1905_C34965;

                $datos[$i]['G1905_C34966'] = $key->G1905_C34966;

                $datos[$i]['G1905_C34967'] = $key->G1905_C34967;

                $datos[$i]['G1905_C34968'] = $key->G1905_C34968;

                $datos[$i]['G1905_C34953'] = $key->G1905_C34953;

                $datos[$i]['G1905_C34954'] = $key->G1905_C34954;

                $datos[$i]['G1905_C34955'] = explode(' ', $key->G1905_C34955)[0];
  
                $hora = '';
                if(!is_null($key->G1905_C34956)){
                    $hora = explode(' ', $key->G1905_C34956)[1];
                }

                $datos[$i]['G1905_C34956'] = $hora;

                $datos[$i]['G1905_C34957'] = $key->G1905_C34957;

                $datos[$i]['G1905_C34958'] = $key->G1905_C34958;

                $datos[$i]['G1905_C34959'] = $key->G1905_C34959;

                $datos[$i]['G1905_C34960'] = $key->G1905_C34960;

                $datos[$i]['G1905_C34961'] = $key->G1905_C34961;

                $datos[$i]['G1905_C34962'] = $key->G1905_C34962;

                $datos[$i]['G1905_C34963'] = $key->G1905_C34963;

                $datos[$i]['G1905_C35095'] = $key->G1905_C35095;

                $datos[$i]['G1905_C35096'] = $key->G1905_C35096;

                $datos[$i]['G1905_C34969'] = $key->G1905_C34969;

                $datos[$i]['G1905_C34970'] = $key->G1905_C34970;

                $datos[$i]['G1905_C34971'] = $key->G1905_C34971;

                $datos[$i]['G1905_C34972'] = $key->G1905_C34972;

                $datos[$i]['G1905_C34973'] = $key->G1905_C34973;

                $datos[$i]['G1905_C34974'] = $key->G1905_C34974;

                $datos[$i]['G1905_C34975'] = $key->G1905_C34975;

                $datos[$i]['G1905_C34976'] = $key->G1905_C34976;

                $datos[$i]['G1905_C34977'] = $key->G1905_C34977;

                $datos[$i]['G1905_C34978'] = $key->G1905_C34978;

                $datos[$i]['G1905_C34979'] = $key->G1905_C34979;

                $datos[$i]['G1905_C34983'] = $key->G1905_C34983;

                $datos[$i]['G1905_C34982'] = $key->G1905_C34982;

                $datos[$i]['G1905_C34980'] = $key->G1905_C34980;

                $datos[$i]['G1905_C34981'] = $key->G1905_C34981;

                $datos[$i]['G1905_C34984'] = $key->G1905_C34984;

                $datos[$i]['G1905_C34985'] = $key->G1905_C34985;

                $datos[$i]['G1905_C34986'] = $key->G1905_C34986;

                $datos[$i]['G1905_C34987'] = $key->G1905_C34987;

                $datos[$i]['G1905_C34988'] = $key->G1905_C34988;

                $datos[$i]['G1905_C34989'] = $key->G1905_C34989;

                $datos[$i]['G1905_C35153'] = $key->G1905_C35153;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1905";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G1905_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G1905_ConsInte__b as id,  G1905_C34964 as camp1 , G1905_C34966 as camp2 
                     FROM ".$BaseDatos.".G1905  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G1905_ConsInte__b as id,  G1905_C34964 as camp1 , G1905_C34966 as camp2  
                    FROM ".$BaseDatos.".G1905  JOIN ".$BaseDatos.".G1905_M".$_POST['muestra']." ON G1905_ConsInte__b = G1905_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G1905_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G1905_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G1905_C34964 LIKE '%".$B."%' OR G1905_C34966 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G1905_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G1905");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G1905_ConsInte__b, G1905_FechaInsercion , G1905_Usuario ,  G1905_CodigoMiembro  , G1905_PoblacionOrigen , G1905_EstadoDiligenciamiento ,  G1905_IdLlamada , G1905_C34964 as principal ,G1905_C34964,G1905_C34965,G1905_C34966,G1905_C34967,G1905_C34968, a.LISOPC_Nombre____b as G1905_C34953, b.LISOPC_Nombre____b as G1905_C34954,G1905_C34955,G1905_C34956,G1905_C34957,G1905_C34958,G1905_C34959,G1905_C34960,G1905_C34961,G1905_C34962,G1905_C34963,G1905_C35095,G1905_C35096,G1905_C34969,G1905_C34970,G1905_C34971, c.LISOPC_Nombre____b as G1905_C34972,G1905_C34973,G1905_C34974,G1905_C34975,G1905_C34976,G1905_C34977,G1905_C34978,G1905_C34979,G1905_C34983,G1905_C34982,G1905_C34980,G1905_C34981, d.LISOPC_Nombre____b as G1905_C34984, e.LISOPC_Nombre____b as G1905_C34985, f.LISOPC_Nombre____b as G1905_C34986,G1905_C34987,G1905_C34988,G1905_C34989,G1905_C35153 FROM '.$BaseDatos.'.G1905 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G1905_C34953 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G1905_C34954 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G1905_C34972 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G1905_C34984 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G1905_C34985 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G1905_C34986';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G1905_C34956)){
                    $hora_a = explode(' ', $fila->G1905_C34956)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G1905_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G1905_ConsInte__b , ($fila->G1905_C34964) , ($fila->G1905_C34965) , ($fila->G1905_C34966) , ($fila->G1905_C34967) , ($fila->G1905_C34968) , ($fila->G1905_C34953) , ($fila->G1905_C34954) , explode(' ', $fila->G1905_C34955)[0] , $hora_a , ($fila->G1905_C34957) , ($fila->G1905_C34958) , ($fila->G1905_C34959) , ($fila->G1905_C34960) , ($fila->G1905_C34961) , ($fila->G1905_C34962) , ($fila->G1905_C34963) , ($fila->G1905_C35095) , ($fila->G1905_C35096) , ($fila->G1905_C34969) , ($fila->G1905_C34970) , ($fila->G1905_C34971) , ($fila->G1905_C34972) , ($fila->G1905_C34973) , ($fila->G1905_C34974) , ($fila->G1905_C34975) , ($fila->G1905_C34976) , ($fila->G1905_C34977) , ($fila->G1905_C34978) , ($fila->G1905_C34979) , ($fila->G1905_C34983) , ($fila->G1905_C34982) , ($fila->G1905_C34980) , ($fila->G1905_C34981) , ($fila->G1905_C34984) , ($fila->G1905_C34985) , ($fila->G1905_C34986) , ($fila->G1905_C34987) , ($fila->G1905_C34988) , ($fila->G1905_C34989) , ($fila->G1905_C35153) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G1905 WHERE G1905_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 1905";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G1905_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G1905_ConsInte__b as id,  G1905_C34964 as camp1 , G1905_C34966 as camp2  FROM '.$BaseDatos.'.G1905 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G1905_ConsInte__b as id,  G1905_C34964 as camp1 , G1905_C34966 as camp2  
                    FROM ".$BaseDatos.".G1905  JOIN ".$BaseDatos.".G1905_M".$_POST['muestra']." ON G1905_ConsInte__b = G1905_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G1905_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G1905_C34964 LIKE "%'.$B.'%" OR G1905_C34966 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G1905_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G1905 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G1905(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G1905_C34964"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34964 = '".$_POST["G1905_C34964"]."'";
                $LsqlI .= $separador."G1905_C34964";
                $LsqlV .= $separador."'".$_POST["G1905_C34964"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34965"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34965 = '".$_POST["G1905_C34965"]."'";
                $LsqlI .= $separador."G1905_C34965";
                $LsqlV .= $separador."'".$_POST["G1905_C34965"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34966"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34966 = '".$_POST["G1905_C34966"]."'";
                $LsqlI .= $separador."G1905_C34966";
                $LsqlV .= $separador."'".$_POST["G1905_C34966"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34967"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34967 = '".$_POST["G1905_C34967"]."'";
                $LsqlI .= $separador."G1905_C34967";
                $LsqlV .= $separador."'".$_POST["G1905_C34967"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34968"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34968 = '".$_POST["G1905_C34968"]."'";
                $LsqlI .= $separador."G1905_C34968";
                $LsqlV .= $separador."'".$_POST["G1905_C34968"]."'";
                $validar = 1;
            }
             
 
            $G1905_C34953 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1905_C34953 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G1905_C34953 = ".$G1905_C34953;
                    $LsqlI .= $separador." G1905_C34953";
                    $LsqlV .= $separador.$G1905_C34953;
                    $validar = 1;

                    
                }
            }
 
            $G1905_C34954 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1905_C34954 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G1905_C34954 = ".$G1905_C34954;
                    $LsqlI .= $separador." G1905_C34954";
                    $LsqlV .= $separador.$G1905_C34954;
                    $validar = 1;
                }
            }
 
            $G1905_C34955 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1905_C34955 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G1905_C34955 = ".$G1905_C34955;
                    $LsqlI .= $separador." G1905_C34955";
                    $LsqlV .= $separador.$G1905_C34955;
                    $validar = 1;
                }
            }
 
            $G1905_C34956 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1905_C34956 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G1905_C34956 = ".$G1905_C34956;
                    $LsqlI .= $separador." G1905_C34956";
                    $LsqlV .= $separador.$G1905_C34956;
                    $validar = 1;
                }
            }
 
            $G1905_C34957 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G1905_C34957 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G1905_C34957 = ".$G1905_C34957;
                    $LsqlI .= $separador." G1905_C34957";
                    $LsqlV .= $separador.$G1905_C34957;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G1905_C34958"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34958 = '".$_POST["G1905_C34958"]."'";
                $LsqlI .= $separador."G1905_C34958";
                $LsqlV .= $separador."'".$_POST["G1905_C34958"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34959"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34959 = '".$_POST["G1905_C34959"]."'";
                $LsqlI .= $separador."G1905_C34959";
                $LsqlV .= $separador."'".$_POST["G1905_C34959"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34960"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34960 = '".$_POST["G1905_C34960"]."'";
                $LsqlI .= $separador."G1905_C34960";
                $LsqlV .= $separador."'".$_POST["G1905_C34960"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34961"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34961 = '".$_POST["G1905_C34961"]."'";
                $LsqlI .= $separador."G1905_C34961";
                $LsqlV .= $separador."'".$_POST["G1905_C34961"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34962"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34962 = '".$_POST["G1905_C34962"]."'";
                $LsqlI .= $separador."G1905_C34962";
                $LsqlV .= $separador."'".$_POST["G1905_C34962"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34963"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34963 = '".$_POST["G1905_C34963"]."'";
                $LsqlI .= $separador."G1905_C34963";
                $LsqlV .= $separador."'".$_POST["G1905_C34963"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35095"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35095 = '".$_POST["G1905_C35095"]."'";
                $LsqlI .= $separador."G1905_C35095";
                $LsqlV .= $separador."'".$_POST["G1905_C35095"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35096"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35096 = '".$_POST["G1905_C35096"]."'";
                $LsqlI .= $separador."G1905_C35096";
                $LsqlV .= $separador."'".$_POST["G1905_C35096"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34969"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34969 = '".$_POST["G1905_C34969"]."'";
                $LsqlI .= $separador."G1905_C34969";
                $LsqlV .= $separador."'".$_POST["G1905_C34969"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34970"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34970 = '".$_POST["G1905_C34970"]."'";
                $LsqlI .= $separador."G1905_C34970";
                $LsqlV .= $separador."'".$_POST["G1905_C34970"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34971"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34971 = '".$_POST["G1905_C34971"]."'";
                $LsqlI .= $separador."G1905_C34971";
                $LsqlV .= $separador."'".$_POST["G1905_C34971"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34972"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34972 = '".$_POST["G1905_C34972"]."'";
                $LsqlI .= $separador."G1905_C34972";
                $LsqlV .= $separador."'".$_POST["G1905_C34972"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34973"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34973 = '".$_POST["G1905_C34973"]."'";
                $LsqlI .= $separador."G1905_C34973";
                $LsqlV .= $separador."'".$_POST["G1905_C34973"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34974"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34974 = '".$_POST["G1905_C34974"]."'";
                $LsqlI .= $separador."G1905_C34974";
                $LsqlV .= $separador."'".$_POST["G1905_C34974"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34975"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34975 = '".$_POST["G1905_C34975"]."'";
                $LsqlI .= $separador."G1905_C34975";
                $LsqlV .= $separador."'".$_POST["G1905_C34975"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34976"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34976 = '".$_POST["G1905_C34976"]."'";
                $LsqlI .= $separador."G1905_C34976";
                $LsqlV .= $separador."'".$_POST["G1905_C34976"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34977"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34977 = '".$_POST["G1905_C34977"]."'";
                $LsqlI .= $separador."G1905_C34977";
                $LsqlV .= $separador."'".$_POST["G1905_C34977"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34978"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34978 = '".$_POST["G1905_C34978"]."'";
                $LsqlI .= $separador."G1905_C34978";
                $LsqlV .= $separador."'".$_POST["G1905_C34978"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34979"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34979 = '".$_POST["G1905_C34979"]."'";
                $LsqlI .= $separador."G1905_C34979";
                $LsqlV .= $separador."'".$_POST["G1905_C34979"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34983"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34983 = '".$_POST["G1905_C34983"]."'";
                $LsqlI .= $separador."G1905_C34983";
                $LsqlV .= $separador."'".$_POST["G1905_C34983"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34982"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34982 = '".$_POST["G1905_C34982"]."'";
                $LsqlI .= $separador."G1905_C34982";
                $LsqlV .= $separador."'".$_POST["G1905_C34982"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34980"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34980 = '".$_POST["G1905_C34980"]."'";
                $LsqlI .= $separador."G1905_C34980";
                $LsqlV .= $separador."'".$_POST["G1905_C34980"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34981"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34981 = '".$_POST["G1905_C34981"]."'";
                $LsqlI .= $separador."G1905_C34981";
                $LsqlV .= $separador."'".$_POST["G1905_C34981"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34984"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34984 = '".$_POST["G1905_C34984"]."'";
                $LsqlI .= $separador."G1905_C34984";
                $LsqlV .= $separador."'".$_POST["G1905_C34984"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34985"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34985 = '".$_POST["G1905_C34985"]."'";
                $LsqlI .= $separador."G1905_C34985";
                $LsqlV .= $separador."'".$_POST["G1905_C34985"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34986"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34986 = '".$_POST["G1905_C34986"]."'";
                $LsqlI .= $separador."G1905_C34986";
                $LsqlV .= $separador."'".$_POST["G1905_C34986"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34987"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34987 = '".$_POST["G1905_C34987"]."'";
                $LsqlI .= $separador."G1905_C34987";
                $LsqlV .= $separador."'".$_POST["G1905_C34987"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34988"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C34988 = '".$_POST["G1905_C34988"]."'";
                $LsqlI .= $separador."G1905_C34988";
                $LsqlV .= $separador."'".$_POST["G1905_C34988"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C34989"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $_POST["G1905_C34989"] = preg_replace("/[\r\n|\n|\r]+/", " ", $_POST["G1905_C34989"]);

                $LsqlU .= $separador."G1905_C34989 = '".$_POST["G1905_C34989"]."'";
                $LsqlI .= $separador."G1905_C34989";
                $LsqlV .= $separador."'".$_POST["G1905_C34989"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35127"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35127 = '".$_POST["G1905_C35127"]."'";
                $LsqlI .= $separador."G1905_C35127";
                $LsqlV .= $separador."'".$_POST["G1905_C35127"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35128"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35128 = '".$_POST["G1905_C35128"]."'";
                $LsqlI .= $separador."G1905_C35128";
                $LsqlV .= $separador."'".$_POST["G1905_C35128"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35129"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35129 = '".$_POST["G1905_C35129"]."'";
                $LsqlI .= $separador."G1905_C35129";
                $LsqlV .= $separador."'".$_POST["G1905_C35129"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35130"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35130 = '".$_POST["G1905_C35130"]."'";
                $LsqlI .= $separador."G1905_C35130";
                $LsqlV .= $separador."'".$_POST["G1905_C35130"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35131"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35131 = '".$_POST["G1905_C35131"]."'";
                $LsqlI .= $separador."G1905_C35131";
                $LsqlV .= $separador."'".$_POST["G1905_C35131"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35132"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35132 = '".$_POST["G1905_C35132"]."'";
                $LsqlI .= $separador."G1905_C35132";
                $LsqlV .= $separador."'".$_POST["G1905_C35132"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35133"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35133 = '".$_POST["G1905_C35133"]."'";
                $LsqlI .= $separador."G1905_C35133";
                $LsqlV .= $separador."'".$_POST["G1905_C35133"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35134"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35134 = '".$_POST["G1905_C35134"]."'";
                $LsqlI .= $separador."G1905_C35134";
                $LsqlV .= $separador."'".$_POST["G1905_C35134"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35135"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35135 = '".$_POST["G1905_C35135"]."'";
                $LsqlI .= $separador."G1905_C35135";
                $LsqlV .= $separador."'".$_POST["G1905_C35135"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35136"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35136 = '".$_POST["G1905_C35136"]."'";
                $LsqlI .= $separador."G1905_C35136";
                $LsqlV .= $separador."'".$_POST["G1905_C35136"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35137"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35137 = '".$_POST["G1905_C35137"]."'";
                $LsqlI .= $separador."G1905_C35137";
                $LsqlV .= $separador."'".$_POST["G1905_C35137"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35138"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35138 = '".$_POST["G1905_C35138"]."'";
                $LsqlI .= $separador."G1905_C35138";
                $LsqlV .= $separador."'".$_POST["G1905_C35138"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35139"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35139 = '".$_POST["G1905_C35139"]."'";
                $LsqlI .= $separador."G1905_C35139";
                $LsqlV .= $separador."'".$_POST["G1905_C35139"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35140"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35140 = '".$_POST["G1905_C35140"]."'";
                $LsqlI .= $separador."G1905_C35140";
                $LsqlV .= $separador."'".$_POST["G1905_C35140"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35141"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35141 = '".$_POST["G1905_C35141"]."'";
                $LsqlI .= $separador."G1905_C35141";
                $LsqlV .= $separador."'".$_POST["G1905_C35141"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35142"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35142 = '".$_POST["G1905_C35142"]."'";
                $LsqlI .= $separador."G1905_C35142";
                $LsqlV .= $separador."'".$_POST["G1905_C35142"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35143"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35143 = '".$_POST["G1905_C35143"]."'";
                $LsqlI .= $separador."G1905_C35143";
                $LsqlV .= $separador."'".$_POST["G1905_C35143"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35144"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35144 = '".$_POST["G1905_C35144"]."'";
                $LsqlI .= $separador."G1905_C35144";
                $LsqlV .= $separador."'".$_POST["G1905_C35144"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35145"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35145 = '".$_POST["G1905_C35145"]."'";
                $LsqlI .= $separador."G1905_C35145";
                $LsqlV .= $separador."'".$_POST["G1905_C35145"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35146"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35146 = '".$_POST["G1905_C35146"]."'";
                $LsqlI .= $separador."G1905_C35146";
                $LsqlV .= $separador."'".$_POST["G1905_C35146"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35147"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35147 = '".$_POST["G1905_C35147"]."'";
                $LsqlI .= $separador."G1905_C35147";
                $LsqlV .= $separador."'".$_POST["G1905_C35147"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35148"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35148 = '".$_POST["G1905_C35148"]."'";
                $LsqlI .= $separador."G1905_C35148";
                $LsqlV .= $separador."'".$_POST["G1905_C35148"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35149"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35149 = '".$_POST["G1905_C35149"]."'";
                $LsqlI .= $separador."G1905_C35149";
                $LsqlV .= $separador."'".$_POST["G1905_C35149"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35150"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35150 = '".$_POST["G1905_C35150"]."'";
                $LsqlI .= $separador."G1905_C35150";
                $LsqlV .= $separador."'".$_POST["G1905_C35150"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G1905_C35153"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_C35153 = '".$_POST["G1905_C35153"]."'";
                $LsqlI .= $separador."G1905_C35153";
                $LsqlV .= $separador."'".$_POST["G1905_C35153"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G1905_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G1905_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G1905_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G1905_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G1905_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G1905_Usuario , G1905_FechaInsercion, G1905_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G1905_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G1905 WHERE G1905_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
