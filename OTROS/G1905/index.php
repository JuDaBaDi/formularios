<?php 
    /*
        Document   : index
        Created on : 2020-06-28 13:57:44
        Author     : Nicolas y su poderoso generador, La gloria sea Para DIOS 
        Url 	   : id = MTkwNQ==  
    */
    $url_crud =  "formularios/G1905/G1905_CRUD_web.php";
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    date_default_timezone_set('America/Bogota');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulario de contacto</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
    
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link rel="shortcut icon" href="assets/img/logo_dyalogo_mail.png"/>
        <style>
            
            .hed{
           
                font-family: 'Sansita One', cursive; 
                color:white;
            }

            .hed2{
                text-align:center;
                font-family: 'Sansita One', cursive; 
                font-size:25px; 
                color:#019CDE;
                margin-top: -9px;
            }
            .font_2 {
                font: normal normal normal 17px/1.4em Spinnaker,sans-serif;
                text-align:center;
            }
            
            .redonder {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                -webkit-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
            }

            [class^='select2'] {
                border-radius: 0px !important;
            }
        </style>
    </head>
    <?php  
        echo '<body class="hold-transition" >';
    ?>
    
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" >
                <div class="login-box">
                    <div class="login-logo hed">
                        <img src="assets/img/logo_dyalogo_mail.png"  alt="Dyalogo">
                    </div><!-- /.login-logo -->
                    <div class="login-box-body">
                        <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
                        <form action="formularios/G1905/G1905_CRUD_web.php" method="post" id="formLogin">
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Buenos días/tardes, está comunicado con la Línea de Atención del Programa Aprende en Casa con MALOKA, mi nombre es (Nombre y Apellido del Guía) ¿Con quién tengo el gusto?</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Mucho gusto Sr./Sra. (Nombre del Acudiente), ¿podría por favor indicarme el número celular en el que recibió el mensaje que enviamos?</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>El motivo por el cual recibió el mensaje es indicarles que (Nombres y Apellidos del Estudiante) fue seleccionado como uno de los beneficiarios del Programa Aprende en Casa con MALOKA.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Este programa es gratuito y consiste en la entrega de una serie de recursos educativos impresos para educación inicial y básica primaria con el objetivo de apoyar el aprendizaje de los niños en la casa y desarrollar actividades que incentiven el gusto por la ciencia.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>El material será entregado en el domicilio de todos los estudiantes y les enviaremos 3 entregas durante el segundo semestre del año para que sean desarrolladas en compañía del acudiente.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Para continuar con el proceso, necesitamos que por favor nos confirme algunos datos para la entrega del material en el lugar en el que se encuentra el estudiante. ¿Está usted de acuerdo?</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Si la persona confirma que está de acuerdo, pasamos a la sección “Desarrollo”.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Si no está de acuerdo, se deben atender las preguntas u objeciones, de acuerdo con la orientación brindada en la “Tabla de Preguntas y Objeciones”</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Sr./Sra. (Nombre del Acudiente), agradezco me confirme la siguiente información del lugar a donde haremos llegar el material:</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>a.	Dirección exacta (Preguntar si vive en apartamento, piso específico, etc.)</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>b.	Barrio</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>c.	Localidad</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>d.	Ciudad</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>e.	Nombre del Acudiente que se encuentra con el estudiante durante la cuarentena</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>f.	Pedir correo electrónico</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>g.	(Solo para llamadas a teléfonos fijos): Número Celular del Acudiente</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>h.	¿Tiene computador y conexión a internet en la casa donde está el niño?</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>i.	En caso de que el registro no traiga Grado, por favor preguntar cual es y actualizar</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Muchas gracias Sr./Sra. (Nombre del Acudiente). ¿Confirma usted que la información brindada es la que corresponde al Acudiente del Estudiante (Nombres y Apellidos del Estudiante) y autoriza al tratamiento de los datos para vincularlos en la inscripción, seguimiento y envío de material del Programa Aprende en Casa con MALOKA de acuerdo a nuestra Política de Tratamiento de Datos Personales?</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Si confirma y autoriza el Tratamiento de Datos, pasar a la sección “Salida</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Si no confirma o no autoriza el Tratamiento de Datos, se debe informar que no es posible incluir al Estudiante en el Programa.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Si tenemos número de celular:</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Le confirmo que días antes de la entrega del primer paquete de guías, le estaremos enviando un mensaje de texto para que por favor esté atento(a) de la entrega.</h3>
                            <!-- FIN LIBRETO -->
  
                            <!-- lIBRETO O LABEL -->
                            <h3>Sr./Sra. (Nombre del Acudiente), muchas gracias por comunicarse con nosotros. Que tenga un(a) buen día/tarde/noche</h3>
                            <!-- FIN LIBRETO -->

                    <!-- CAMPO DE TIPO LISTA / Respuesta -->
                    <div class="form-group">
                        <label for="G1905_C35153" id="LblG1905_C35153">Solución de dudas y objeciones</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1905_C35153" id="G1905_C35153">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, LISOPC_Respuesta_b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1981 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."' respuesta = '".$obje->LISOPC_Respuesta_b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="respuesta_G1905_C35153" id="respuesta_LblG1905_C35153">Respuesta</label>
                        <textarea id="respuesta_G1905_C35153" class="form-control" placeholder="Respuesta"></textarea>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA  / Respuesta -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34964" id="LblG1905_C34964">Nombre del Estudiante</label>
								<input type="text" class="form-control input-sm" id="G1905_C34964" value="" disabled name="G1905_C34964"  placeholder="Nombre del Estudiante">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34965" id="LblG1905_C34965">Tipo de Documento</label>
								<input type="text" class="form-control input-sm" id="G1905_C34965" value="" disabled name="G1905_C34965"  placeholder="Tipo de Documento">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34966" id="LblG1905_C34966">Número de Identificación</label>
								<input type="text" class="form-control input-sm" id="G1905_C34966" value="" disabled name="G1905_C34966"  placeholder="Número de Identificación">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34967" id="LblG1905_C34967">Grado Actual</label>
								<input type="text" class="form-control input-sm" id="G1905_C34967" value=""  name="G1905_C34967"  placeholder="Grado Actual">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34968" id="LblG1905_C34968">Colegio</label>
								<input type="text" class="form-control input-sm" id="G1905_C34968" value="" disabled name="G1905_C34968"  placeholder="Colegio">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34962" id="LblG1905_C34962">Nombre del Acudiente</label>
								<input type="text" class="form-control input-sm" id="G1905_C34962" value=""  name="G1905_C34962"  placeholder="Nombre del Acudiente">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34963" id="LblG1905_C34963">Persona Autorizada para recibir</label>
								<input type="text" class="form-control input-sm" id="G1905_C34963" value=""  name="G1905_C34963"  placeholder="Persona Autorizada para recibir">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C35095" id="LblG1905_C35095">Parentezco del acudiente</label>
								<input type="text" class="form-control input-sm" id="G1905_C35095" value=""  name="G1905_C35095"  placeholder="Parentezco del acudiente">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C35096" id="LblG1905_C35096">Parentezco de quien recibe</label>
								<input type="text" class="form-control input-sm" id="G1905_C35096" value=""  name="G1905_C35096"  placeholder="Parentezco de quien recibe">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34969" id="LblG1905_C34969">LUGAR DE ENTREGA (Casa/ Colegio)</label>
								<input type="text" class="form-control input-sm" id="G1905_C34969" value="" disabled name="G1905_C34969"  placeholder="LUGAR DE ENTREGA (Casa/ Colegio)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34970" id="LblG1905_C34970">Dirección de Residencia Del Estudiante</label>
								<input type="text" class="form-control input-sm" id="G1905_C34970" value="" disabled name="G1905_C34970"  placeholder="Dirección de Residencia Del Estudiante">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34971" id="LblG1905_C34971">ZONA SDP</label>
								<input type="text" class="form-control input-sm" id="G1905_C34971" value="" disabled name="G1905_C34971"  placeholder="ZONA SDP">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1905_C34972" id="LblG1905_C34972">¿Se actualizo la dirección?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1905_C34972" id="G1905_C34972">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1970 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34973" id="LblG1905_C34973">Dirección Actualizada del Estudiante</label>
								<input type="text" class="form-control input-sm" id="G1905_C34973" value=""  name="G1905_C34973"  placeholder="Dirección Actualizada del Estudiante">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34974" id="LblG1905_C34974">Especificaciones de Dirección (Torre, manzana etc)</label>
								<input type="text" class="form-control input-sm" id="G1905_C34974" value=""  name="G1905_C34974"  placeholder="Especificaciones de Dirección (Torre, manzana etc)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34975" id="LblG1905_C34975">Ciudad</label>
								<input type="text" class="form-control input-sm" id="G1905_C34975" value=""  name="G1905_C34975"  placeholder="Ciudad">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34976" id="LblG1905_C34976">Localidad</label>
								<input type="text" class="form-control input-sm" id="G1905_C34976" value=""  name="G1905_C34976"  placeholder="Localidad">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34977" id="LblG1905_C34977">Barrio</label>
								<input type="text" class="form-control input-sm" id="G1905_C34977" value=""  name="G1905_C34977"  placeholder="Barrio">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34978" id="LblG1905_C34978">Dirección de Trabajo del Acudiente (solo si aplica)</label>
								<input type="text" class="form-control input-sm" id="G1905_C34978" value=""  name="G1905_C34978"  placeholder="Dirección de Trabajo del Acudiente (solo si aplica)">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34979" id="LblG1905_C34979">Correo Electrónico</label>
								<input type="text" class="form-control input-sm" id="G1905_C34979" value=""  name="G1905_C34979"  placeholder="Correo Electrónico">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34983" id="LblG1905_C34983">Teléfono celular 1</label>
								<input type="text" class="form-control input-sm" id="G1905_C34983" value=""  name="G1905_C34983"  placeholder="Teléfono celular 1">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34982" id="LblG1905_C34982">Teléfono Celular 2</label>
								<input type="text" class="form-control input-sm" id="G1905_C34982" value=""  name="G1905_C34982"  placeholder="Teléfono Celular 2">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34980" id="LblG1905_C34980">Teléfono fijo 1</label>
								<input type="text" class="form-control input-sm" id="G1905_C34980" value=""  name="G1905_C34980"  placeholder="Teléfono fijo 1">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34981" id="LblG1905_C34981">Teléfono fijo 2</label>
								<input type="text" class="form-control input-sm" id="G1905_C34981" value=""  name="G1905_C34981"  placeholder="Teléfono fijo 2">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1905_C34984" id="LblG1905_C34984">¿Cuentan con conexión a internet en el hogar?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1905_C34984" id="G1905_C34984">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1970 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1905_C34985" id="LblG1905_C34985">¿El/la estudiante cuenta con acceso a herramientas tecnológicas como computador o tablet?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1905_C34985" id="G1905_C34985">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1970 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G1905_C34986" id="LblG1905_C34986">¿Confirma usted que la información brindada es la que corresponde al Acudiente del Estudiante (Nombres y Apellidos del Estudiante) y autoriza al tratamiento de los datos para vincularlos en la inscripción, seguimiento y envío de material del Programa Aprende en Casa con MALOKA de acuerdo a nuestra Política de Tratamiento de Datos Personales?</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G1905_C34986" id="G1905_C34986">
                            <option value="0">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 1970 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34987" id="LblG1905_C34987">Nombre de la Persona que Actualizo y Confirmo los datos</label>
								<input type="text" class="form-control input-sm" id="G1905_C34987" value=""  name="G1905_C34987"  placeholder="Nombre de la Persona que Actualizo y Confirmo los datos">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G1905_C34988" id="LblG1905_C34988">Parentesco</label>
								<input type="text" class="form-control input-sm" id="G1905_C34988" value=""  name="G1905_C34988"  placeholder="Parentesco">
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="G1905_C34989" id="LblG1905_C34989">OBSERVACIONES </label>
                                <textarea class="form-control input-sm" name="G1905_C34989" id="G1905_C34989"  value="" placeholder="OBSERVACIONES "></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->

                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="id" id="hidId" value='<?php if(isset($_GET['u'])){ echo $_GET['u']; }else{ echo "0"; } ?>'>
                            <input type="hidden" name="oper" id="oper" value='add'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formpadre" id="formpadre" value='<?php if(isset($_GET['formularioPadre'])){ echo $_GET['formularioPadre']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formhijo" id="formhijo" value='<?php if(isset($_GET['formulario'])){ echo $_GET['formulario']; }else{ echo "0"; }?>' >
                            <input type= "hidden" name="campana" id="campana" value="<?php if(isset($_GET['camp'])){ echo base64_decode($_GET['camp']); }else{ echo "0"; }?>">
                            <div class="row">
                                <div class="col-xs-2">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-xs-8">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar datos</button>
                                </div><!-- /.col -->
                            </div>
                        </form>
                    </div><!-- /.login-box-body -->
                </div><!-- /.login-box -->
            </div>
        </div>


        <!-- jQuery 2.2.3 -->
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>  
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/numeric.js"></script>

        <script type="text/javascript" src="formularios/G1905/G1905_eventos.js"></script>
        <script type="text/javascript">
            $.validator.setDefaults({
                submitHandler: function() { 
                     $("#formLogin").submit();
                }
            });

            $(function(){

                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };

                //str_Select2 estos son los guiones
                


                //datepickers
                

            $("#G1905_C34955").datepicker({
                language: "es",
                autoclose: true,
                todayHighlight: true
            });

                //Timepickers
                


            //Timepicker
            $("#G1905_C34956").timepicker({
                'timeFormat': 'H:i:s',
                'minTime': '08:00:00',
                'maxTime': '17:00:00',
                'setTime': '08:00:00',
                'step'  : '5',
                'showDuration': true
            });

                //Validaciones numeros Enteros
                


                //Validaciones numeros Decimales
               


               //Si tiene dependencias
               


    //function para Solución de dudas y objeciones 

    $("#G1905_C35153").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Se actualizo la dirección? 

    $("#G1905_C34972").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Cuentan con conexión a internet en el hogar? 

    $("#G1905_C34984").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿El/la estudiante cuenta con acceso a herramientas tecnológicas como computador o tablet? 

    $("#G1905_C34985").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });

    //function para ¿Confirma usted que la información brindada es la que corresponde al Acudiente del Estudiante (Nombres y Apellidos del Estudiante) y autoriza al tratamiento de los datos para vincularlos en la inscripción, seguimiento y envío de material del Programa Aprende en Casa con MALOKA de acuerdo a nuestra Política de Tratamiento de Datos Personales? 

    $("#G1905_C34986").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });
                

               <?php
                    if(isset($_GET['result'])){
                        if($_GET['result'] ==  1){
                ?>
                        swal({
                            title: "Exito!",
                            text: "Recibimos su solicitud, pronto estaremos en contacto",
                            type: "success",
                            confirmButtonText: "Ok"
                        },function(){

                        });
                        
                        
                <?php   }else{ ?>
                            swal({
                                title: "Error!",
                                text: 'Ocurrio un error, intenta mas tarde',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }
                    }
                ?>
            });
        </script>
        <Script type="text/javascript">
            $(document).ready(function() {
                <?php
                $campana = base64_decode($_GET['camp']);
                $Guion = 0;//id de la campaña
                $tabla = 0;// $_GET['u'];//ide del usuario
                $Lsql = "SELECT CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConsInte__GUION__Pob_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$campana;

                $result = $mysqli->query($Lsql);
                while($obj = $result->fetch_object()){
                    $Guion = $obj->CAMPAN_ConsInte__GUION__Gui_b;
                    $tabla = $obj->CAMPAN_ConsInte__GUION__Pob_b;
                } 
                //SELECT de la camic
                $campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b, CAMINC_ConsInte__CAMPO_Gui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$campana;
                
                $resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    
                    //Pregfuntar por el tipo de dato
                    $Lsql = "SELECT PREGUN_Tipo______b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__b = ".$key->CAMINC_ConsInte__CAMPO_Gui_b;
                    $res = $mysqli->query($Lsql);
                    $datos = $res->fetch_array();


                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['u'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                        while($objDatos = $resultDatosSql->fetch_object()){ 
                            if(!is_null($objDatos->campo) && $objDatos->campo != ''){

                                if($datos['PREGUN_Tipo______b'] != '8'){
                            ?>
                                    document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
                            <?php  
                                }else{
                                    if($objDatos->campo == '1'){
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , true);";
                                    }else{
                                        echo "$('#".$key->CAMINC_NomCamGui_b."').attr('checked' , false);";
                                    }
                                    
                                } 
                            }
                        }
                    }
                    
                }
                ?>
            });
        </script>
        
