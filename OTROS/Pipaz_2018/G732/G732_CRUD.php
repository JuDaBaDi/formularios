<?php
    ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G732_ConsInte__b, G732_FechaInsercion , G732_Usuario ,  G732_CodigoMiembro  , G732_PoblacionOrigen , G732_EstadoDiligenciamiento ,  G732_IdLlamada , G732_C9641 as principal ,G732_C9623,G732_C9625,G732_C9626,G732_C9627,G732_C9628,G732_C9629,G732_C9630,G732_C9631,G732_C9632,G732_C9633,G732_C9680,G732_C9636,G732_C9638,G732_C9639,G732_C9640,G732_C9641,G732_C9642,G732_C9643,G732_C9644,G732_C9645,G732_C9646,G732_C9647,G732_C9771,G732_C9772,G732_C9654,G732_C9655,G732_C9656,G732_C9657,G732_C9700 FROM '.$BaseDatos.'.G732 WHERE G732_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G732_C9623'] = $key->G732_C9623;

                $datos[$i]['G732_C9625'] = $key->G732_C9625;

                $datos[$i]['G732_C9626'] = $key->G732_C9626;

                $datos[$i]['G732_C9627'] = $key->G732_C9627;

                $datos[$i]['G732_C9628'] = $key->G732_C9628;

                $datos[$i]['G732_C9629'] = $key->G732_C9629;

                $datos[$i]['G732_C9630'] = $key->G732_C9630;

                $datos[$i]['G732_C9631'] = $key->G732_C9631;

                $datos[$i]['G732_C9632'] = $key->G732_C9632;

                $datos[$i]['G732_C9633'] = $key->G732_C9633;

                $datos[$i]['G732_C9680'] = $key->G732_C9680;

                $datos[$i]['G732_C9636'] = $key->G732_C9636;

                $datos[$i]['G732_C9638'] = $key->G732_C9638;

                $datos[$i]['G732_C9639'] = $key->G732_C9639;

                $datos[$i]['G732_C9640'] = $key->G732_C9640;

                $datos[$i]['G732_C9641'] = $key->G732_C9641;

                $datos[$i]['G732_C9642'] = $key->G732_C9642;

                $datos[$i]['G732_C9643'] = $key->G732_C9643;

                $datos[$i]['G732_C9644'] = $key->G732_C9644;

                $datos[$i]['G732_C9645'] = $key->G732_C9645;

                $datos[$i]['G732_C9646'] = $key->G732_C9646;

                $datos[$i]['G732_C9647'] = $key->G732_C9647;

                $datos[$i]['G732_C9771'] = explode(' ', $key->G732_C9771)[0];

                $datos[$i]['G732_C9772'] = $key->G732_C9772;

                $datos[$i]['G732_C9654'] = $key->G732_C9654;

                $datos[$i]['G732_C9655'] = explode(' ', $key->G732_C9655)[0];
  
                $hora = '';
                if(!is_null($key->G732_C9656)){
                    $hora = explode(' ', $key->G732_C9656)[1];
                }

                $datos[$i]['G732_C9657'] = $key->G732_C9657;

                $datos[$i]['G732_C9700'] = $key->G732_C9700;
      
				$datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){
            $Lsql = "SELECT G732_ConsInte__b as id,  G732_C9638 as camp2 , G732_C9641 as camp1 ";
            $Lsql .= " FROM ".$BaseDatos.".G732 ";
            if($_POST['Busqueda'] != '' && !is_null($_POST['Busqueda'])){
                $Lsql .= " WHERE G732_C9638 like '%".$_POST['Busqueda']."%' ";
                $Lsql .= " OR G732_C9641 like '%".$_POST['Busqueda']."%' ";
            }
            $Lsql .= " ORDER BY G732_ConsInte__b DESC LIMIT 0, 50 "; 
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G732");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G732_ConsInte__b, G732_FechaInsercion , G732_Usuario ,  G732_CodigoMiembro  , G732_PoblacionOrigen , G732_EstadoDiligenciamiento ,  G732_IdLlamada , G732_C9641 as principal ,G732_C9623, a.LISOPC_Nombre____b as G732_C9625,G732_C9626, b.LISOPC_Nombre____b as G732_C9627, c.LISOPC_Nombre____b as G732_C9628,G732_C9629,G732_C9630, d.LISOPC_Nombre____b as G732_C9631,G732_C9632,G732_C9633,G732_C9680, e.LISOPC_Nombre____b as G732_C9636,G732_C9638,G732_C9639,G732_C9640,G732_C9641,G732_C9642,G732_C9643,G732_C9644, f.LISOPC_Nombre____b as G732_C9645,G732_C9646,G732_C9647,G732_C9771, g.LISOPC_Nombre____b as G732_C9772,G732_C9654,G732_C9655,G732_C9656, h.LISOPC_Nombre____b as G732_C9657,G732_C9700 FROM '.$BaseDatos.'.G732 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G732_C9625 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G732_C9627 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G732_C9628 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G732_C9631 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G732_C9636 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G732_C9645 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as g ON g.LISOPC_ConsInte__b =  G732_C9772 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as h ON h.LISOPC_ConsInte__b =  G732_C9657';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G732_C9656)){
                    $hora_a = explode(' ', $fila->G732_C9656)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G732_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G732_ConsInte__b , ($fila->G732_C9623) , ($fila->G732_C9625) , ($fila->G732_C9626) , ($fila->G732_C9627) , ($fila->G732_C9628) , ($fila->G732_C9629) , ($fila->G732_C9630) , ($fila->G732_C9631) , ($fila->G732_C9632) , ($fila->G732_C9633) , ($fila->G732_C9680) , ($fila->G732_C9636) , ($fila->G732_C9638) , ($fila->G732_C9639) , ($fila->G732_C9640) , ($fila->G732_C9641) , ($fila->G732_C9642) , ($fila->G732_C9643) , ($fila->G732_C9644) , ($fila->G732_C9645) , ($fila->G732_C9646) , ($fila->G732_C9647) , explode(' ', $fila->G732_C9771)[0] , ($fila->G732_C9772) , ($fila->G732_C9654) , explode(' ', $fila->G732_C9655)[0] , $hora_a , ($fila->G732_C9657) , ($fila->G732_C9700) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G732 WHERE G732_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){
            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];
            $Zsql = 'SELECT  G732_ConsInte__b as id,  G732_C9638 as camp2 , G732_C9641 as camp1  FROM '.$BaseDatos.'.G732 ORDER BY G732_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G732 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G732(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G732_C9622"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9622 = '".$_POST["G732_C9622"]."'";
                $LsqlI .= $separador."G732_C9622";
                $LsqlV .= $separador."'".$_POST["G732_C9622"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9623"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9623 = '".$_POST["G732_C9623"]."'";
                $LsqlI .= $separador."G732_C9623";
                $LsqlV .= $separador."'".$_POST["G732_C9623"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9624"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9624 = '".$_POST["G732_C9624"]."'";
                $LsqlI .= $separador."G732_C9624";
                $LsqlV .= $separador."'".$_POST["G732_C9624"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9625"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9625 = '".$_POST["G732_C9625"]."'";
                $LsqlI .= $separador."G732_C9625";
                $LsqlV .= $separador."'".$_POST["G732_C9625"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9626"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9626 = '".$_POST["G732_C9626"]."'";
                $LsqlI .= $separador."G732_C9626";
                $LsqlV .= $separador."'".$_POST["G732_C9626"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9627"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9627 = '".$_POST["G732_C9627"]."'";
                $LsqlI .= $separador."G732_C9627";
                $LsqlV .= $separador."'".$_POST["G732_C9627"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9628"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9628 = '".$_POST["G732_C9628"]."'";
                $LsqlI .= $separador."G732_C9628";
                $LsqlV .= $separador."'".$_POST["G732_C9628"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9629"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9629 = '".$_POST["G732_C9629"]."'";
                $LsqlI .= $separador."G732_C9629";
                $LsqlV .= $separador."'".$_POST["G732_C9629"]."'";
                $validar = 1;
            }
             
  
            $G732_C9630 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G732_C9630"])){
                if($_POST["G732_C9630"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G732_C9630 = $_POST["G732_C9630"];
                    $LsqlU .= $separador." G732_C9630 = ".$G732_C9630."";
                    $LsqlI .= $separador." G732_C9630";
                    $LsqlV .= $separador.$G732_C9630;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G732_C9631"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9631 = '".$_POST["G732_C9631"]."'";
                $LsqlI .= $separador."G732_C9631";
                $LsqlV .= $separador."'".$_POST["G732_C9631"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9632"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9632 = '".$_POST["G732_C9632"]."'";
                $LsqlI .= $separador."G732_C9632";
                $LsqlV .= $separador."'".$_POST["G732_C9632"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9633"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9633 = '".$_POST["G732_C9633"]."'";
                $LsqlI .= $separador."G732_C9633";
                $LsqlV .= $separador."'".$_POST["G732_C9633"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9680"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9680 = '".$_POST["G732_C9680"]."'";
                $LsqlI .= $separador."G732_C9680";
                $LsqlV .= $separador."'".$_POST["G732_C9680"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9634"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9634 = '".$_POST["G732_C9634"]."'";
                $LsqlI .= $separador."G732_C9634";
                $LsqlV .= $separador."'".$_POST["G732_C9634"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9635"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9635 = '".$_POST["G732_C9635"]."'";
                $LsqlI .= $separador."G732_C9635";
                $LsqlV .= $separador."'".$_POST["G732_C9635"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9636"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9636 = '".$_POST["G732_C9636"]."'";
                $LsqlI .= $separador."G732_C9636";
                $LsqlV .= $separador."'".$_POST["G732_C9636"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9637"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9637 = '".$_POST["G732_C9637"]."'";
                $LsqlI .= $separador."G732_C9637";
                $LsqlV .= $separador."'".$_POST["G732_C9637"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9638"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9638 = '".$_POST["G732_C9638"]."'";
                $LsqlI .= $separador."G732_C9638";
                $LsqlV .= $separador."'".$_POST["G732_C9638"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9639"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9639 = '".$_POST["G732_C9639"]."'";
                $LsqlI .= $separador."G732_C9639";
                $LsqlV .= $separador."'".$_POST["G732_C9639"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9640"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9640 = '".$_POST["G732_C9640"]."'";
                $LsqlI .= $separador."G732_C9640";
                $LsqlV .= $separador."'".$_POST["G732_C9640"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9641"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9641 = '".$_POST["G732_C9641"]."'";
                $LsqlI .= $separador."G732_C9641";
                $LsqlV .= $separador."'".$_POST["G732_C9641"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9642"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9642 = '".$_POST["G732_C9642"]."'";
                $LsqlI .= $separador."G732_C9642";
                $LsqlV .= $separador."'".$_POST["G732_C9642"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9643"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9643 = '".$_POST["G732_C9643"]."'";
                $LsqlI .= $separador."G732_C9643";
                $LsqlV .= $separador."'".$_POST["G732_C9643"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9644"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9644 = '".$_POST["G732_C9644"]."'";
                $LsqlI .= $separador."G732_C9644";
                $LsqlV .= $separador."'".$_POST["G732_C9644"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9645"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9645 = '".$_POST["G732_C9645"]."'";
                $LsqlI .= $separador."G732_C9645";
                $LsqlV .= $separador."'".$_POST["G732_C9645"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9646"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9646 = '".$_POST["G732_C9646"]."'";
                $LsqlI .= $separador."G732_C9646";
                $LsqlV .= $separador."'".$_POST["G732_C9646"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9647"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9647 = '".$_POST["G732_C9647"]."'";
                $LsqlI .= $separador."G732_C9647";
                $LsqlV .= $separador."'".$_POST["G732_C9647"]."'";
                $validar = 1;
            }
             
 
            $G732_C9771 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G732_C9771"])){    
                if($_POST["G732_C9771"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G732_C9771 = "'".str_replace(' ', '',$_POST["G732_C9771"])." 00:00:00'";
                    $LsqlU .= $separador." G732_C9771 = ".$G732_C9771;
                    $LsqlI .= $separador." G732_C9771";
                    $LsqlV .= $separador.$G732_C9771;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G732_C9772"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9772 = '".$_POST["G732_C9772"]."'";
                $LsqlI .= $separador."G732_C9772";
                $LsqlV .= $separador."'".$_POST["G732_C9772"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9649"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9649 = '".$_POST["G732_C9649"]."'";
                $LsqlI .= $separador."G732_C9649";
                $LsqlV .= $separador."'".$_POST["G732_C9649"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9650"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9650 = '".$_POST["G732_C9650"]."'";
                $LsqlI .= $separador."G732_C9650";
                $LsqlV .= $separador."'".$_POST["G732_C9650"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9651"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9651 = '".$_POST["G732_C9651"]."'";
                $LsqlI .= $separador."G732_C9651";
                $LsqlV .= $separador."'".$_POST["G732_C9651"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9652"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9652 = '".$_POST["G732_C9652"]."'";
                $LsqlI .= $separador."G732_C9652";
                $LsqlV .= $separador."'".$_POST["G732_C9652"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G732_C9653"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9653 = '".$_POST["G732_C9653"]."'";
                $LsqlI .= $separador."G732_C9653";
                $LsqlV .= $separador."'".$_POST["G732_C9653"]."'";
                $validar = 1;
            }
             
  
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_C9654 = '".getNombreUser($_GET['token'])."'";
                $LsqlI .= $separador."G732_C9654";
                $LsqlV .= $separador."'".getNombreUser($_GET['token'])."'";
                $validar = 1;
            
             
 
            $G732_C9655 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }
            $G732_C9655 = "'".date('Y-m-d H:i:s')."'";
            $LsqlU .= $separador." G732_C9655 = ".$G732_C9655;
            $LsqlI .= $separador." G732_C9655";
            $LsqlV .= $separador.$G732_C9655;
            $validar = 1;
           
 
            $G732_C9656 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            $separador = "";
            if($validar == 1){
                $separador = ",";
            }
            $G732_C9656 = "'".date('Y-m-d H:i:s')."'";
            $LsqlU .= $separador." G732_C9656 = ".$G732_C9656;
            $LsqlI .= $separador." G732_C9656";
            $LsqlV .= $separador.$G732_C9656;
            $validar = 1;
           
 
            $G732_C9657 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G732_C9657 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G732_C9657 = ".$G732_C9657;
                    $LsqlI .= $separador." G732_C9657";
                    $LsqlV .= $separador.$G732_C9657;
                    $validar = 1;
                }
            }
 
            $G732_C9700 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G732_C9700 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G732_C9700 = ".$G732_C9700;
                    $LsqlI .= $separador." G732_C9700";
                    $LsqlV .= $separador.$G732_C9700;
                    $validar = 1;
                }
            }

			if(isset($_GET['id_gestion_cbx'])){
				$separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G732_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G732_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
			}


			$padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND 
GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G732_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }

            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G732_Usuario , G732_FechaInsercion, G732_CodigoMiembro, G732_PoblacionOrigen";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro']." , 733";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G732_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM G732 WHERE G732_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo "Error Hacieno el proceso los registros : " . $mysqli->error;
                }
            }
        }
    }
  

  
?>
