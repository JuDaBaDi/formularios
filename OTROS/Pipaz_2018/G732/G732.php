
<div class="modal fade-in" id="editarDatos" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="refrescarGrillas">&times;</button>
                <h4 class="modal-title">Edicion</h4>
            </div>
            <div class="modal-body">
                <iframe id="frameContenedor" src="" style="width: 100%; height: 900px;"  marginheight="0" marginwidth="0" noresize  frameborder="0">
                  
                </iframe>
            </div>
        </div>
    </div>
</div>
<?php
   //SECCION : Definicion urls
   $url_crud = "formularios/G732/G732_CRUD.php";
   //SECCION : CARGUE DATOS LISTA DE NAVEGACIÓN

	$PEOBUS_Escritur__b = 1 ;
    $PEOBUS_Adiciona__b = 1 ;
    $PEOBUS_Borrar____b = 1 ;

	if(!isset($_GET['view'])){
        $idUsuario = getIdentificacionUser($token);
        $peobus = "SELECT * FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$idUsuario." AND PEOBUS_ConsInte__GUION__b = ".$_GET['formulario'];
        $query = $mysqli->query($peobus);
        $PEOBUS_VeRegPro__b = 0 ;
        
        while ($key =  $query->fetch_object()) {
            $PEOBUS_VeRegPro__b = $key->PEOBUS_VeRegPro__b ;
            $PEOBUS_Escritur__b = $key->PEOBUS_Escritur__b ;
            $PEOBUS_Adiciona__b = $key->PEOBUS_Adiciona__b ;
            $PEOBUS_Borrar____b = $key->PEOBUS_Borrar____b ;
        }

        if($PEOBUS_VeRegPro__b != 0){
            $Zsql = "SELECT G732_ConsInte__b as id, G732_C9638 as camp2 , G732_C9641 as camp1 FROM ".$BaseDatos.".G732  WHERE G732_Usuario = ".$idUsuario." ORDER BY G732_ConsInte__b DESC LIMIT 0, 50";
        }else{
            $Zsql = "SELECT G732_ConsInte__b as id, G732_C9638 as camp2 , G732_C9641 as camp1 FROM ".$BaseDatos.".G732  ORDER BY G732_ConsInte__b DESC LIMIT 0, 50";
        }
    }else{
        $Zsql = "SELECT G732_ConsInte__b as id, G732_C9638 as camp2 , G732_C9641 as camp1 FROM ".$BaseDatos.".G732  ORDER BY G732_ConsInte__b DESC LIMIT 0, 50";
    }

   $result = $mysqli->query($Zsql);

?>
<?php include(__DIR__ ."/../cabecera.php");?>

<?php
if(isset($_GET['user'])){
	$XLsql = "SELECT G732_C9641 as nombre FROM ".$BaseDatos.".G732 WHERE G732_CodigoMiembro = ".$_GET['user'].";";
	$nombre = $mysqli->query($XLsql);
	$nombreUsuario = NULL;
	while ($key = $nombre->fetch_object()) {
	 	echo "<h3 style='color: rgb(110, 197, 255);'>".$key->nombre."</h3>";  
	 	$nombreUsuario = $key->nombre;
	 	break;
	} 

	if(isset($_GET['token']) && isset($_GET['id_gestion_cbx'])){
					
		$data = array(	"strToken_t" => $_GET['token'], 
						"strIdGestion_t" => $_GET['id_gestion_cbx'],
						"strDatoPrincipal_t" => $nombreUsuario,
						"strNombreCampanaCRM_t" => 'PIPAZ TD');                                                                    
		$data_string = json_encode($data);    

		$ch = curl_init('127.0.0.1:8080/dyalogocbx/api/gestion/asignarDatoPrincipal');
		//especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 

		//le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                      
		); 
		//recogemos la respuesta
		$respuesta = curl_exec ($ch);
		//o el error, por si falla
		$error = curl_error($ch);
		//y finalmente cerramos curl
		//echo "Respuesta =>  ". $respuesta;
		//echo "<br/>Error => ".$error;
		//include "Log.class.php";
		//$log = new Log("log", "./Log/");
		//$log->insert($error, $respuesta, false, true, false);
		//echo "nada";
		curl_close ($ch);
	}
}else{
	echo "<h3 id='h3mio' style='color : rgb(110, 197, 255);'></h3>";	
}
?>

<?php if(isset($_GET['user'])){ ?>
<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="box">
			<div class="box-body">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
                            <th colspan="4">
                                Historico de gestiones
                            </th>
                        </tr>
						<tr>
							<th>Gesti&oacute;n</th>
							<th>Comentarios</th>
							<th>Fecha - hora</th>
							<th>Agente</th>
						</tr>
					</thead>
					<tbody>
						<?php

							$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
					        $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
					        $datoCampan = $res_Lsql_Campan->fetch_array();
					        $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
					        $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
					        $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
					        $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];

							$Lsql = "SELECT * FROM ".$BaseDatos_systema.".CONDIA JOIN ".$BaseDatos_systema.".USUARI ON CONDIA_ConsInte__USUARI_b = USUARI_ConsInte__b JOIN ".$BaseDatos_systema.".MONOEF ON CONDIA_ConsInte__MONOEF_b = MONOEF_ConsInte__b WHERE CONDIA_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND CONDIA_ConsInte__GUION__Gui_b = ".$int_Guion_Campan." AND CONDIA_ConsInte__GUION__Pob_b = ".$int_Pobla_Camp_2." AND CONDIA_ConsInte__MUESTR_b = ".$int_Muest_Campan." AND CONDIA_CodiMiem__b = ".$_GET['user']." ORDER BY CONDIA_Fecha_____b DESC LIMIT 5;";


							/*$Lsql = "SELECT * FROM ".$BaseDatos.".G733_M341 JOIN ".$BaseDatos_systema.".USUARI ON G733_M341_ConIntUsu_b = USUARI_ConsInte__b LEFT JOIN ".$BaseDatos_systema.".LISOPC ON LISOPC_ConsInte__b = G733_M341_UltiGest__b WHERE G733_M341_CoInMiPo__b = ".$_GET['user'];*/

							$res = $mysqli->query($Lsql);
							while($key = $res->fetch_object()){
								echo "<tr>";
								echo "<td>".($key->MONOEF_Texto_____b)."</td>";
								echo "<td>".$key->CONDIA_Observacio_b."</td>";
								echo "<td>".$key->CONDIA_Fecha_____b."</td>";
								echo "<td>".$key->USUARI_Nombre____b."</td>";
								echo "</tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?php } ?>


<div id="897">


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
			        <!-- lIBRETO O LABEL -->
			        <h3>Bienvenido a tutor doctor le habla(Nombre Agente) en que le puedo colaborar</h3>
			        <!-- FIN LIBRETO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G732_C9623" id="LblG732_C9623">¿Con gusto, con quien tengo el gusto de hablar?</label>
			            <input type="text" class="form-control input-sm" id="G732_C9623" value=""  name="G732_C9623"  placeholder="¿Con gusto, con quien tengo el gusto de hablar?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
			        <!-- lIBRETO O LABEL -->
			        <h3>Con mucho gusto Sr@ (Nombre del Cliente)</h3>
			        <!-- FIN LIBRETO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G732_C9625" id="LblG732_C9625">Ciudad</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G732_C9625" id="G732_C9625">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 420 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G732_C9626" id="LblG732_C9626">¿Indíqueme por favor en que sector desea tomar las tutorías?</label>
			            <input type="text" class="form-control input-sm" id="G732_C9626" value=""  name="G732_C9626"  placeholder="¿Indíqueme por favor en que sector desea tomar las tutorías?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G732_C9627" id="LblG732_C9627">Consultor</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G732_C9627" id="G732_C9627">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 444 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G732_C9628" id="LblG732_C9628">¿Porque medio se enteró de nosotros?</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G732_C9628" id="G732_C9628">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 445 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G732_C9629" id="LblG732_C9629">¿Las clases son para usted o para otra persona?</label>
			            <input type="text" class="form-control input-sm" id="G732_C9629" value=""  name="G732_C9629"  placeholder="¿Las clases son para usted o para otra persona?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO ENTERO -->
			        <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G732_C9630" id="LblG732_C9630">¿Qué edad tiene el estudiante?</label>
			            <input type="text" class="form-control input-sm Numerico" value=""  name="G732_C9630" id="G732_C9630" placeholder="¿Qué edad tiene el estudiante?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO ENTERO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G732_C9631" id="LblG732_C9631">¿Ha tomado tutorías anteriormente?</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G732_C9631" id="G732_C9631">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 170 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G732_C9632" id="LblG732_C9632">¿Qué colegio está el alumno?</label>
			            <input type="text" class="form-control input-sm" id="G732_C9632" value=""  name="G732_C9632"  placeholder="¿Qué colegio está el alumno?">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G732_C9633" id="LblG732_C9633">Otra Informacion</label>
			            <input type="text" class="form-control input-sm" id="G732_C9633" value=""  name="G732_C9633"  placeholder="Otra Informacion">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G732_C9680" id="LblG732_C9680">Origen</label>
			            <input type="text" class="form-control input-sm" id="G732_C9680" value=""  name="G732_C9680"  placeholder="Origen">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


</div>

<div id="898">


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
			        <!-- lIBRETO O LABEL -->
			        <h3>Con gusto, le comento como tal como es el proceso con nosotros: Nosotros inicialmente asignamos una consulta educativa, ésta es totalmente gratis y sin ningún compromiso ¿Cuál es la idea de la consulta? Queremos identificar el estilo de aprendizaje y necesidad académica del alumno. De esta manera, podemos proponer un programa de trabajo que se ajuste a las necesidades que tienen como familia. Como tal lo que nosotros ofrecemos son paquetes de horas, tenemos paquetes desde 12 horas hasta más 144 horas, y nuestros costos varían dependiendo el paquete de horas que elijan, entre más horas elijan dentro de un paquete más económica le queda la hora, nuestra hora tiene un costo entre 50 y 70 mil dependiendo el paquete de horas que elijan ¿De acuerdo?</h3>
			        <!-- FIN LIBRETO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
			        <!-- lIBRETO O LABEL -->
			        <h3>Ok sí señor, entonces como le indicaba todo nuestro proceso inicia asignando una consulta educativa que es totalmente gratis y sin ningún compromiso. En esta consulta es importante que esté presente el adulto responsable y el alumno.  No sé si de pronto tenga la disponibilidad de tiempo para atendernos el día</h3>
			        <!-- FIN LIBRETO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G732_C9636" id="LblG732_C9636">Agendar Cita</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G732_C9636" id="G732_C9636">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 170 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
			        <!-- lIBRETO O LABEL -->
			        <h3>Perfecto, vamos a enviar un correo confirmando la cita, agradecemos nos confirme recibido y que todos los datos estén en orden. (Este correo lo debe enviar ya cada consultor educativo al que le agendemos)</h3>
			        <!-- FIN LIBRETO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G732_C9638" id="LblG732_C9638">Nombres y Apellidos</label>
			            <input type="text" class="form-control input-sm" id="G732_C9638" value=""  name="G732_C9638"  placeholder="Nombres y Apellidos">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G732_C9639" id="LblG732_C9639">Apellido</label>
			            <input type="text" class="form-control input-sm" id="G732_C9639" value=""  name="G732_C9639"  placeholder="Apellido">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G732_C9640" id="LblG732_C9640">Email</label>
			            <input type="text" class="form-control input-sm" id="G732_C9640" value=""  name="G732_C9640"  placeholder="Email">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G732_C9641" id="LblG732_C9641">Telefono</label>
			            <input type="text" class="form-control input-sm" id="G732_C9641" value=""  name="G732_C9641"  placeholder="Telefono">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G732_C9642" id="LblG732_C9642">Codigo postal</label>
			            <input type="text" class="form-control input-sm" id="G732_C9642" value=""  name="G732_C9642"  placeholder="Codigo postal">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G732_C9643" id="LblG732_C9643">Agenda</label>
			            <input type="text" class="form-control input-sm" id="G732_C9643" value=""  name="G732_C9643"  placeholder="Agenda">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G732_C9644" id="LblG732_C9644">Fuente</label>
			            <input type="text" class="form-control input-sm" id="G732_C9644" value=""  name="G732_C9644"  placeholder="Fuente">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G732_C9645" id="LblG732_C9645">Otro</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G732_C9645" id="G732_C9645">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 418 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G732_C9646" id="LblG732_C9646">Barrio</label>
			            <input type="text" class="form-control input-sm" id="G732_C9646" value=""  name="G732_C9646"  placeholder="Barrio">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G732_C9647" id="LblG732_C9647">Consultor</label>
			            <input type="text" class="form-control input-sm" id="G732_C9647" value=""  name="G732_C9647"  placeholder="Consultor">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G732_C9771" id="LblG732_C9771">Fecha</label>
			            <input type="text" class="form-control input-sm Fecha" value=""  name="G732_C9771" id="G732_C9771" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">


			        <!-- CAMPO DE TIPO LISTA -->
			        <div class="form-group">
			            <label for="G732_C9772" id="LblG732_C9772">Motivos</label>
			            <select class="form-control input-sm select2"  style="width: 100%;" name="G732_C9772" id="G732_C9772">
			                <option value="0">Seleccione</option>
			                <?php
			                    /*
			                        SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
			                    */
			                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 446 ORDER BY LISOPC_Nombre____b ASC";

			                    $obj = $mysqli->query($Lsql);
			                    while($obje = $obj->fetch_object()){
			                        echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

			                    }    
			                    
			                ?>
			            </select>
			        </div>
			        <!-- FIN DEL CAMPO TIPO LISTA -->
  
            </div>

  
        </div>


</div>

<div id="899">


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
			        <!-- lIBRETO O LABEL -->
			        <h3>Objeciones de presupuesto

Sr@________  comprendemos la situación, pero recuerde que el servicio que nosotros ofrecemos es totalmente a domicilio y personalizado, adicional a esto la primera consulta de diagnóstico es totalmente gratis y sin compromiso, que le parece si agendamos la consulta educativa para podernos acercar un poco más al estudiante y de esta manera puedan tomar una decisión..</h3>
			        <!-- FIN LIBRETO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
			        <!-- lIBRETO O LABEL -->
			        <h3>Objeciones de pocas horas

Sr@________ recuerde que la idea con nosotros no es apagar el incendio, si no que el estudiante pueda tener un aprendizaje duradero y con bases firmes, que realmente pueda mejorar en_______ y adicional a esto crear hábitos de estudio en el estudiante.</h3>
			        <!-- FIN LIBRETO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
			        <!-- lIBRETO O LABEL -->
			        <h3>Objeción de certificado

Sr@_______ nosotros lo podemos capacitar de la mejor manera para que usted pueda sacar un buen nivel en un certificado internacional que le va a servir en cualquier país.</h3>
			        <!-- FIN LIBRETO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
			        <!-- lIBRETO O LABEL -->
			        <h3>Recuerde mi nombre es</h3>
			        <!-- FIN LIBRETO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
			        <!-- lIBRETO O LABEL -->
			        <h3>Gracias por comunicarse, que tenga un muy buen día…</h3>
			        <!-- FIN LIBRETO -->
  
            </div>

  
        </div>


</div>

<div id="900">


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

 
			        <!-- CAMPO TIPO TEXTO -->
			        <div class="form-group">
			            <label for="G732_C9654" id="LblG732_C9654">Agente</label>
			            <input type="text" class="form-control input-sm" id="G732_C9654" value="<?php echo getNombreUser($token);?>" disabled name="G732_C9654"  placeholder="Agente">
			        </div>
			        <!-- FIN DEL CAMPO TIPO TEXTO -->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
			        <!-- CAMPO TIPO FECHA -->
			        <!-- Estos campos siempre deben llevar Fecha en la clase asi class="form-control input-sm Fecha" , de l contrario seria solo un campo de texto mas -->
			        <div class="form-group">
			            <label for="G732_C9655" id="LblG732_C9655">Fecha Llamada</label>
			            <input type="text" class="form-control input-sm Fecha" value="<?php echo date('Y-m-d');?>" disabled name="G732_C9655" id="G732_C9655" placeholder="YYYY-MM-DD">
			        </div>
			        <!-- FIN DEL CAMPO TIPO FECHA-->
  
            </div>

  
        </div>


        <div class="row">
        

            <div class="col-md-12 col-xs-12">

  
			        <!-- CAMPO TIMEPICKER -->
			        <!-- Estos campos siempre deben llevar Hora en la clase asi class="form-control input-sm Hora" , de l contrario seria solo un campo de texto mas -->
			        <div class="bootstrap-timepicker">
			            <div class="form-group">
			                <label for="G732_C9656" id="LblG732_C9656">Hora Llamada</label>
			                <div class="input-group">
			                    <input type="text" class="form-control input-sm Hora" disabled name="G732_C9656" id="G732_C9656" placeholder="HH:MM:SS" >
			                    <div class="input-group-addon" id="TMP_G732_C9656">
			                        <i class="fa fa-clock-o"></i>
			                    </div>
			                </div>
			                <!-- /.input group -->
			            </div>
			            <!-- /.form group -->
			        </div>
			        <!-- FIN DEL CAMPO TIMEPICKER -->
  
            </div>

  
        </div>


</div>
<div class="row" style="background-color: #FAFAFA; ">
	<br/>
    <?php if(isset($_GET['user'])){ ?>
    <div class="col-md-10 col-xs-9">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G732_C9657">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 436;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."'>".($obje->OPCION_Nombre____b)."</option>";

                }          
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
        </div>
    </div>
   	<div class="col-md-2 col-xs-3" style="text-align: center;">
        <button class="btn btn-primary btn-block" id="Save" type="button">
            Cerrar Gesti&oacute;n
        </button>
        <a id="errorGestion" style="text-align: center; font-size: 12px; color: gray; cursor: pointer;">
            <u>Cambiar registro</u>
        </a>
    </div>
    <?php }else{ ?>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <select class="form-control input-sm tipificacion" name="tipificacion" id="G732_C9657">
                <option value="0">Tipificaci&oacute;n</option>
                <?php
                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b FROM ".$BaseDatos_systema.".LISOPC 
                        JOIN ".$BaseDatos_systema.".MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                        WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 436;";
                $obj = $mysqli->query($Lsql);
                while($obje = $obj->fetch_object()){
                    echo "<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."'>".($obje->OPCION_Nombre____b)."</option>";

                }            
                ?>
            </select>
            
            <input type="hidden" name="Efectividad" id="Efectividad" value="0">
            <input type="hidden" name="MonoEf" id="MonoEf" value="0">
            <input type="hidden" name="TipNoEF" id="TipNoEF" value="0">
            <input type="hidden" name="FechaInicio" id="FechaInicio" value="0">
            <input type="hidden" name="FechaFinal" id="FechaFinal" value="0">
            <input type="hidden" name="MonoEfPeso" id="MonoEfPeso" value="0">
        </div>
    </div>
    <?php } ?>
</div>
<div class="row" style="background-color: #FAFAFA; ">
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <select class="form-control input-sm reintento" name="reintento" id="G732_C">
                <option value="0">Reintento</option>
                <option value="1">REINTENTO AUTOMATICO</option>
                <option value="2">AGENDADO</option>
                <option value="3">NO REINTENTAR</option>
            </select>     
        </div>
    </div>
    <div class="col-md-4 col-xs-4">
        <div class="form-group">
            <input type="text" name="TxtFechaReintento" id="G732_C" class="form-control input-sm TxtFechaReintento" placeholder="Fecha Reintento"  >
        </div>
    </div>
    <div class="col-md-4 col-xs-4" style="text-align: left;">
        <div class="form-group">
            <input type="text" name="TxtHoraReintento" id="G732_C" class="form-control input-sm TxtHoraReintento" placeholder="Hora Reintento">
        </div>
    </div>
    <div class="col-md-12 col-xs-12">
        <div class="form-group">
            <textarea class="form-control input-sm textAreaComentarios" name="textAreaComentarios" id="G732_C9700" placeholder="Observaciones"></textarea>
        </div>
    </div>
</div>
<!-- SECCION : PAGINAS INCLUIDAS -->
<?php include(__DIR__ ."/../pies.php");?>
<script type="text/javascript" src="formularios/G732/G732_eventos.js"></script> 
<script type="text/javascript">
    $(function(){

    	var meses = new Array(12);
    	meses[0] = "01";
    	meses[1] = "02";
    	meses[2] = "03";
    	meses[3] = "04";
    	meses[4] = "05";
    	meses[5] = "06";
    	meses[6] = "07";
    	meses[7] = "08";
    	meses[8] = "09";
    	meses[9] = "10";
    	meses[10] = "11";
    	meses[11] = "12";

    	var d = new Date();
        var h = d.getHours();
        var horas = (h < 10) ? '0' + h : h;
        var dia = d.getDate();
        var dias = (dia < 10) ? '0' + dia : dia;
        var fechaInicial = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
        $("#FechaInicio").val(fechaInicial);
            

        //Esta es por si lo llaman en modo formulario de edicion LigthBox
        <?php if(isset($_GET['registroId'])){ ?>
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
            dataType : 'json',
            success  : function(data){
                //recorrer datos y enviarlos al formulario
                $.each(data, function(i, item) {
                    
   
                    if(item.G732_C9622 == 1){
                        $("#G732_C9622").attr('checked', true);
                    } 
 
                    $("#G732_C9623").val(item.G732_C9623);
   
                    if(item.G732_C9624 == 1){
                        $("#G732_C9624").attr('checked', true);
                    } 
 
                    $("#G732_C9625").val(item.G732_C9625);
 
                    $("#G732_C9626").val(item.G732_C9626);
 
                    $("#G732_C9627").val(item.G732_C9627);
 
                    $("#G732_C9628").val(item.G732_C9628);
 
                    $("#G732_C9629").val(item.G732_C9629);
 
                    $("#G732_C9630").val(item.G732_C9630);
 
                    $("#G732_C9631").val(item.G732_C9631);
 
                    $("#G732_C9632").val(item.G732_C9632);
 
                    $("#G732_C9633").val(item.G732_C9633);
 
                    $("#G732_C9680").val(item.G732_C9680);
   
                    if(item.G732_C9634 == 1){
                        $("#G732_C9634").attr('checked', true);
                    } 
   
                    if(item.G732_C9635 == 1){
                        $("#G732_C9635").attr('checked', true);
                    } 
 
                    $("#G732_C9636").val(item.G732_C9636);
   
                    if(item.G732_C9637 == 1){
                        $("#G732_C9637").attr('checked', true);
                    } 
 
                    $("#G732_C9638").val(item.G732_C9638);
 
                    $("#G732_C9639").val(item.G732_C9639);
 
                    $("#G732_C9640").val(item.G732_C9640);
 
                    $("#G732_C9641").val(item.G732_C9641);
 
                    $("#G732_C9642").val(item.G732_C9642);
 
                    $("#G732_C9643").val(item.G732_C9643);
 
                    $("#G732_C9644").val(item.G732_C9644);
 
                    $("#G732_C9645").val(item.G732_C9645);
 
                    $("#G732_C9646").val(item.G732_C9646);
 
                    $("#G732_C9647").val(item.G732_C9647);
 
                    $("#G732_C9771").val(item.G732_C9771);
 
                    $("#G732_C9772").val(item.G732_C9772);
   
                    if(item.G732_C9649 == 1){
                        $("#G732_C9649").attr('checked', true);
                    } 
   
                    if(item.G732_C9650 == 1){
                        $("#G732_C9650").attr('checked', true);
                    } 
   
                    if(item.G732_C9651 == 1){
                        $("#G732_C9651").attr('checked', true);
                    } 
   
                    if(item.G732_C9652 == 1){
                        $("#G732_C9652").attr('checked', true);
                    } 
   
                    if(item.G732_C9653 == 1){
                        $("#G732_C9653").attr('checked', true);
                    } 
 
                    $("#G732_C9654").val(item.G732_C9654);
 
                    $("#G732_C9655").val(item.G732_C9655);
 
                    $("#G732_C9656").val(item.G732_C9656);
 
                    $("#G732_C9657").val(item.G732_C9657);
 
                    $("#G732_C9700").val(item.G732_C9700);
					$("#h3mio").html(item.principal);
                });
                //Deshabilitar los campos

                //Habilitar todos los campos para edicion
                $('#FormularioDatos :input').each(function(){
                    $(this).attr('disabled', true);
                });

                              

                //Habilidar los botones de operacion, add, editar, eliminar
                $("#add").attr('disabled', false);
                $("#edit").attr('disabled', false);
                $("#delete").attr('disabled', false);

                //Desahabiliatra los botones de salvar y seleccionar_registro
                $("#cancel").attr('disabled', true);
                $("#Save").attr('disabled', true);
            } 
        });

        $("#hidId").val(<?php echo $_GET['registroId'];?>);
        idTotal = <?php echo $_GET['registroId'];?>;

     	$("#TxtFechaReintento").attr('disabled', true);
        $("#TxtHoraReintento").attr('disabled', true); 
        

        vamosRecargaLasGrillasPorfavor(<?php echo $_GET['registroId'];?>)

        <?php } ?>

        <?php if(isset($_GET['user'])){ ?>
        	/*
        	vamosRecargaLasGrillasPorfavor('<?php echo $_GET['user'];?>');
        	idTotal = <?php echo $_GET['user'];?>; */
        <?php } ?>

        $("#refrescarGrillas").click(function(){
            
            
        });

        //Esta es la funcionalidad de los Tabs
        
 
        //Select2 estos son los guiones
        


    $("#G732_C9625").select2();

    $("#G732_C9627").select2();

    $("#G732_C9628").select2();

    $("#G732_C9631").select2();

    $("#G732_C9636").select2();

    $("#G732_C9645").select2();

    $("#G732_C9772").select2();
        //datepickers
        

        $("#G732_C9771").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        $("#G732_C9655").datepicker({
            language: "es",
            autoclose: true,
            todayHighlight: true
        });

        //Timepickers
        


        //Timepicker
        var options = { //hh:mm 24 hour format only, defaults to current time
            twentyFour: true, //Display 24 hour format, defaults to false
            title: 'Hora Llamada', //The Wickedpicker's title,
            showSeconds: true, //Whether or not to show seconds,
            secondsInterval: 1, //Change interval for seconds, defaults to 1
            minutesInterval: 1, //Change interval for minutes, defaults to 1
            beforeShow: null, //A function to be called before the Wickedpicker is shown
            show: null, //A function to be called when the Wickedpicker is shown
            clearable: false, //Make the picker's input clearable (has clickable "x")
        }; 
        $("#G732_C9656").wickedpicker(options);

        //Validaciones numeros Enteros
        

        $("#G732_C9630").numeric();
        

        //Validaciones numeros Decimales
        

        
        //Funcionalidad del botob guardar
        


        $("#Save").click(function(){
        	var bol_respuesta = before_save();
        	var d = new Date();
            var h = d.getHours();
            var horas = (h < 10) ? '0' + h : h;
            var dia = d.getDate();
            var dias = (dia < 10) ? '0' + dia : dia;
            var fechaFinal = d.getFullYear() + '-' + meses[d.getMonth()] + '-' + dias + ' '+ horas +':'+d.getMinutes()+':'+d.getSeconds();
            $("#FechaFinal").val(fechaFinal);
            var valido = 0;
            
            if($(".tipificacion").val() == '0'){
            	alertify.error("Es necesaria la tipificación!");
            	valido = 1;
            }

            if($(".reintento").val() == '2'){
            	if($(".TxtFechaReintento").val().length < 1){
            		alertify.error("Es necesario llenar la fecha de reintento!");
            		$(".TxtFechaReintento").focus();
            		valido = 1;
            	}

            	if($(".TxtHoraReintento").val().length < 1){
            		alertify.error("Es necesario llenar la hora de reintento!");
            		$(".TxtHoraReintento").focus();
            		valido = 1;
            	}
            }

            if(valido == '0'){
	        	if(bol_respuesta){            
		            var form = $("#FormularioDatos");
		            //Se crean un array con los datos a enviar, apartir del formulario 
		            var formData = new FormData($("#FormularioDatos")[0]);
		            $.ajax({
		               url: '<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?><?php if(isset($_GET['id_gestion_cbx'])){ echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?><?php if(!empty($token)){ echo "&token=".$token; }?>',  
		                type: 'POST',
		                data: formData,
		                cache: false,
		                contentType: false,
		                processData: false,
		                //una vez finalizado correctamente
		                success: function(data){
		                    if(data){
		                    	<?php if(!isset($_GET['campan'])){ ?>
			                        //Si realizo la operacion ,perguntamos cual es para posarnos sobre el nuevo registro
			                        if($("#oper").val() == 'add'){
			                            idTotal = data;
			                        }else{
			                            idTotal= $("#hidId").val();
			                        }
			                       
			                        //Limpiar formulario
			                        form[0].reset();
			                        after_save();
			                        <?php if(isset($_GET['registroId'])){ ?>
			                        $.ajax({
			                            url      : '<?=$url_crud;?>',
			                            type     : 'POST',
			                            data     : { CallDatos : 'SI', id : <?php echo $_GET['registroId']; ?> },
			                            dataType : 'json',
			                            success  : function(data){
			                                //recorrer datos y enviarlos al formulario
			                                $.each(data, function(i, item) {
		                                    
      
			                                    if(item.G732_C9622 == 1){
			                                       $("#G732_C9622").attr('checked', true);
			                                    } 
 
		                                    	$("#G732_C9623").val(item.G732_C9623);
      
			                                    if(item.G732_C9624 == 1){
			                                       $("#G732_C9624").attr('checked', true);
			                                    } 
 
		                                    	$("#G732_C9625").val(item.G732_C9625);
 
		                                    	$("#G732_C9626").val(item.G732_C9626);
 
		                                    	$("#G732_C9627").val(item.G732_C9627);
 
		                                    	$("#G732_C9628").val(item.G732_C9628);
 
		                                    	$("#G732_C9629").val(item.G732_C9629);
 
		                                    	$("#G732_C9630").val(item.G732_C9630);
 
		                                    	$("#G732_C9631").val(item.G732_C9631);
 
		                                    	$("#G732_C9632").val(item.G732_C9632);
 
		                                    	$("#G732_C9633").val(item.G732_C9633);
 
		                                    	$("#G732_C9680").val(item.G732_C9680);
      
			                                    if(item.G732_C9634 == 1){
			                                       $("#G732_C9634").attr('checked', true);
			                                    } 
      
			                                    if(item.G732_C9635 == 1){
			                                       $("#G732_C9635").attr('checked', true);
			                                    } 
 
		                                    	$("#G732_C9636").val(item.G732_C9636);
      
			                                    if(item.G732_C9637 == 1){
			                                       $("#G732_C9637").attr('checked', true);
			                                    } 
 
		                                    	$("#G732_C9638").val(item.G732_C9638);
 
		                                    	$("#G732_C9639").val(item.G732_C9639);
 
		                                    	$("#G732_C9640").val(item.G732_C9640);
 
		                                    	$("#G732_C9641").val(item.G732_C9641);
 
		                                    	$("#G732_C9642").val(item.G732_C9642);
 
		                                    	$("#G732_C9643").val(item.G732_C9643);
 
		                                    	$("#G732_C9644").val(item.G732_C9644);
 
		                                    	$("#G732_C9645").val(item.G732_C9645);
 
		                                    	$("#G732_C9646").val(item.G732_C9646);
 
		                                    	$("#G732_C9647").val(item.G732_C9647);
 
		                                    	$("#G732_C9771").val(item.G732_C9771);
 
		                                    	$("#G732_C9772").val(item.G732_C9772);
      
			                                    if(item.G732_C9649 == 1){
			                                       $("#G732_C9649").attr('checked', true);
			                                    } 
      
			                                    if(item.G732_C9650 == 1){
			                                       $("#G732_C9650").attr('checked', true);
			                                    } 
      
			                                    if(item.G732_C9651 == 1){
			                                       $("#G732_C9651").attr('checked', true);
			                                    } 
      
			                                    if(item.G732_C9652 == 1){
			                                       $("#G732_C9652").attr('checked', true);
			                                    } 
      
			                                    if(item.G732_C9653 == 1){
			                                       $("#G732_C9653").attr('checked', true);
			                                    } 
 
		                                    	$("#G732_C9654").val(item.G732_C9654);
 
		                                    	$("#G732_C9655").val(item.G732_C9655);
 
		                                    	$("#G732_C9656").val(item.G732_C9656);
 
		                                    	$("#G732_C9657").val(item.G732_C9657);
 
		                                    	$("#G732_C9700").val(item.G732_C9700);
		              							$("#h3mio").html(item.principal);
			                                });

			                                //Deshabilitar los campos

			                                //Habilitar todos los campos para edicion
			                                $('#FormularioDatos :input').each(function(){
			                                    $(this).attr('disabled', true);
			                                });

			                                //Habilidar los botones de operacion, add, editar, eliminar
			                                $("#add").attr('disabled', false);
			                                $("#edit").attr('disabled', false);
			                                $("#delete").attr('disabled', false);

			                                //Desahabiliatra los botones de salvar y seleccionar_registro
			                                $("#cancel").attr('disabled', true);
			                                $("#Save").attr('disabled', true);
			                            } 
			                        })
			                        $("#hidId").val(<?php echo $_GET['registroId'];?>);
			                        <?php } else { ?>
			                            llenar_lista_navegacion('');
			                        <?php } ?>   

		                        <?php }else{ 
		                        	if(!isset($_GET['formulario'])){
		                        ?>

		                        	$.ajax({
		                        		url   : 'formularios/G733/G733_Funciones_Busqueda_Manual.php?action=EDIT&tiempo=<?php echo $tiempoDesdeInicio;?>&usuario=<?php echo getIdentificacionUser($token);?>&CodigoMiembro=<?php if(isset($_GET['user'])) { echo $_GET["user"]; }else{ echo "0";  } ?>&ConsInteRegresado='+data +'<?php if(isset($_GET['token'])) { echo "&token=".$_GET['token']; }?><?php if(isset($_GET['id_gestion_cbx'])) { echo "&id_gestion_cbx=".$_GET['id_gestion_cbx']; }?>&campana=372&script=732&muestra=341&poblacion=733<?php if(isset($_GET['predictiva'])) { echo "&predictiva=".$_GET['predictiva'];}?><?php if(isset($_GET['consinte'])) { echo "&consinte=".$_GET['consinte']; }?><?php if(isset($_GET['campana_crm'])) { echo "&campana_crm=".$_GET['campana_crm']; }?>',
		                        		type  : "post",
		                        		data  : formData,
		                    		 	cache: false,
					                    contentType: false,
					                    processData: false,
		                        		success : function(xt){
		                        			console.log(xt);
		                        			window.location.href = "quitar.php";
		                        		}
		                        	});
		                        	
				
		                        <?php } 
		                        	}
		                        ?>            
		                    }else{
		                        //Algo paso, hay un error
		                        alertify.error('Un error ha ocurrido');
		                    }                
		                },
		                //si ha ocurrido un error
		                error: function(){
		                    after_save_error();
		                    alertify.error('Ocurrio un error relacionado con la red, al momento de guardar, intenta mas tarde');
		                }
		            });
          		}
          	}
        });
    });

        //funcionalidad del boton Gestion botonCerrarErronea
        

    <?php if(!isset($_GET['view'])) { ?>
    //SECICON : CARGUE INFORMACION EN HOJA DE DATOS
    //Cargar datos de la hoja de datos
    function cargar_hoja_datos(){
        $.jgrid.defaults.width = '1225';
        $.jgrid.defaults.height = '650';
        $.jgrid.defaults.responsive = true;
        $.jgrid.defaults.styleUI = 'Bootstrap';
        var lastsel2;
        $("#tablaDatos").jqGrid({
            url:'<?=$url_crud;?>?CallDatosJson=si',
            datatype: 'json',
            mtype: 'POST',
            colNames:['id','¿Con gusto, con quien tengo el gusto de hablar?','Ciudad','¿Indíqueme por favor en que sector desea tomar las tutorías?','Consultor','¿Porque medio se enteró de nosotros?','¿Las clases son para usted o para otra persona?','¿Qué edad tiene el estudiante?','¿Ha tomado tutorías anteriormente?','¿Qué colegio está el alumno?','Otra Informacion','Origen','Agendar Cita','Nombres y Apellidos','Apellido','Email','Telefono','Codigo postal','Agenda','Fuente','Otro','Barrio','Consultor','Fecha','Motivos','Agente','Fecha Llamada','Hora Llamada'],
            colModel:[
                //Traigo los datos de la base de dtaos y los defino en que columna va cada uno, tambien definimos con es su forma de edicion, sea Tipo listas, tipo Textos, etc.
                {
                    name:'providerUserId',
                    index:'providerUserId', 
                    width:100,
                    editable:true, 
                    editrules:{
                        required:false, 
                        edithidden:true
                    },
                    hidden:true, 
                    editoptions:{ 
                        dataInit: function(element) {                     
                          $(element).attr("readonly", "readonly"); 
                        } 
                    }
                }

	                ,
	                { 
	                    name:'G732_C9623', 
	                    index: 'G732_C9623', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G732_C9625', 
	                    index:'G732_C9625', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=420&campo=G732_C9625'
	                    }
	                }

	                ,
	                { 
	                    name:'G732_C9626', 
	                    index: 'G732_C9626', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G732_C9627', 
	                    index:'G732_C9627', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=444&campo=G732_C9627'
	                    }
	                }

	                ,
	                { 
	                    name:'G732_C9628', 
	                    index:'G732_C9628', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=445&campo=G732_C9628'
	                    }
	                }

	                ,
	                { 
	                    name:'G732_C9629', 
	                    index: 'G732_C9629', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }
 
	                ,
	                {  
	                    name:'G732_C9630', 
	                    index:'G732_C9630', 
	                    width:80 ,
	                    editable: true, 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).numeric();
	                        }
	                    } 
	                }

	                ,
	                { 
	                    name:'G732_C9631', 
	                    index:'G732_C9631', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=170&campo=G732_C9631'
	                    }
	                }

	                ,
	                { 
	                    name:'G732_C9632', 
	                    index: 'G732_C9632', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G732_C9633', 
	                    index: 'G732_C9633', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G732_C9680', 
	                    index: 'G732_C9680', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G732_C9636', 
	                    index:'G732_C9636', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=170&campo=G732_C9636'
	                    }
	                }

	                ,
	                { 
	                    name:'G732_C9638', 
	                    index: 'G732_C9638', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G732_C9639', 
	                    index: 'G732_C9639', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G732_C9640', 
	                    index: 'G732_C9640', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G732_C9641', 
	                    index: 'G732_C9641', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G732_C9642', 
	                    index: 'G732_C9642', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G732_C9643', 
	                    index: 'G732_C9643', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G732_C9644', 
	                    index: 'G732_C9644', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G732_C9645', 
	                    index:'G732_C9645', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=418&campo=G732_C9645'
	                    }
	                }

	                ,
	                { 
	                    name:'G732_C9646', 
	                    index: 'G732_C9646', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                { 
	                    name:'G732_C9647', 
	                    index: 'G732_C9647', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                {  
	                    name:'G732_C9771', 
	                    index:'G732_C9771', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                { 
	                    name:'G732_C9772', 
	                    index:'G732_C9772', 
	                    width:120 ,
	                    editable: true, 
	                    edittype:"select" , 
	                    editoptions: {
	                        dataUrl: '<?=$url_crud;?>?CallDatosLisop_=si&idLista=446&campo=G732_C9772'
	                    }
	                }

	                ,
	                { 
	                    name:'G732_C9654', 
	                    index: 'G732_C9654', 
	                    width:160, 
	                    resizable:false, 
	                    sortable:true , 
	                    editable: true 
	                }

	                ,
	                {  
	                    name:'G732_C9655', 
	                    index:'G732_C9655', 
	                    width:120 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    searchoptions: {
	                        sopt: ['eq', 'ne', 'lt', 'le', 'gt', 'ge']
	                    }, 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            $(el).datepicker({
	                                language: "es",
	                                autoclose: true,
	                                todayHighlight: true
	                            });
	                        },
	                        defaultValue: function(){
	                            var currentTime = new Date();
	                            var month = parseInt(currentTime.getMonth() + 1);
	                            month = month <= 9 ? "0"+month : month;
	                            var day = currentTime.getDate();
	                            day = day <= 9 ? "0"+day : day;
	                            var year = currentTime.getFullYear();
	                            return year+"-"+month + "-"+day;
	                        }
	                    }
	                }

	                ,
	                {  
	                    name:'G732_C9656', 
	                    index:'G732_C9656', 
	                    width:70 ,
	                    editable: true ,
	                    formatter: 'text', 
	                    editoptions:{
	                        size:20,
	                        dataInit:function(el){
	                            //Timepicker
	                            var options = { 
	                                now: "15:00:00", //hh:mm 24 hour format only, defaults to current time
	                                twentyFour: true, //Display 24 hour format, defaults to false
	                                title: 'Hora Llamada', //The Wickedpicker's title,
	                                showSeconds: true, //Whether or not to show seconds,
	                                secondsInterval: 1, //Change interval for seconds, defaults to 1
	                                minutesInterval: 1, //Change interval for minutes, defaults to 1
	                                beforeShow: null, //A function to be called before the Wickedpicker is shown
	                                show: null, //A function to be called when the Wickedpicker is shown
	                                clearable: false, //Make the picker's input clearable (has clickable "x")
	                            }; 
	                            $(el).wickedpicker(options);
	                        }
	                    }
	                }
            ],
            pager: "#pager" ,
            beforeSelectRow: function(rowid){
                if(rowid && rowid!==lastsel2){
                    
                }
                lastsel2=rowid;
            },
            rowNum: 50,
            rowList:[50,100],
            loadonce: false,
            sortable: true,
            sortname: 'G732_C9641',
            sortorder: 'asc',
            viewrecords: true,
            caption: 'PRUEBAS',
            editurl:"<?=$url_crud;?>?insertarDatosGrilla=si&usuario=<?php echo getIdentificacionUser($token);?>",
            autowidth: true
            
        });

        $('#tablaDatos').navGrid("#pager", { add:false, del: true , edit: false });
        $('#tablaDatos').inlineNav('#pager',
        // the buttons to appear on the toolbar of the grid
        { 
            edit: true, 
            add: true, 
            cancel: true,
            editParams: {
                keys: true,
            },
            addParams: {
                keys: true
            }
        });
      
        //para cuando se Maximice o minimize la pantalla.
        $(window).bind('resize', function() {
            $("#tablaDatos").setGridWidth($(window).width());
        }).trigger('resize'); 
    }

    //SECCION  : Manipular Lista de Navegacion

    //buscar registro en la Lista de navegacion
    function llenar_lista_navegacion(x){
        var tr = '';
        $.ajax({
            url      : '<?=$url_crud;?>',
            type     : 'POST',
            data     : { CallDatosJson : 'SI', Busqueda : x},
            dataType : 'json',
            success  : function(data){
                //Cargar la lista con los datos obtenidos en la consulta
                $.each(data, function(i, item) {
                    tr += "<tr class='CargarDatos' id='"+data[i].id+"'>";
                    tr += "<td>";
                    tr += "<p style='font-size:14px;'><b>"+data[i].camp1+"</b></p>";
                    tr += "<p style='font-size:12px; margin-top:-10px;'>"+data[i].camp2+"</p>";
                    tr += "</td>";
                    tr += "</tr>";
                });
                $("#tablaScroll").html(tr);
                //aplicar funcionalidad a la Lista de navegacion
                busqueda_lista_navegacion();

                //SI el Id existe, entonces le damos click,  para traer sis datos y le damos la clase activa
                if ( $("#"+idTotal).length > 0) {
                    $("#"+idTotal).click();   
                    $("#"+idTotal).addClass('active'); 
                }else{
                    //Si el id no existe, se selecciona el primer registro de la Lista de navegacion
                    $(".CargarDatos :first").click();
                }

            } 
        });
    }

    //poner en el formulario de la derecha los datos del registro seleccionado a la izquierda, funcionalidad de la lista de navegacion
    function busqueda_lista_navegacion(){

        $(".CargarDatos").click(function(){
            //remover todas las clases activas de la lista de navegacion
            $(".CargarDatos").each(function(){
                $(this).removeClass('active');
            });
            
            //add la clase activa solo ala celda que le dimos click.
            $(this).addClass('active');
              
              
            var id = $(this).attr('id');
            
            //buscar los datos
            $.ajax({
                url      : '<?=$url_crud;?>',
                type     : 'POST',
                data     : { CallDatos : 'SI', id : id },
                dataType : 'json',
                success  : function(data){
                    //recorrer datos y enviarlos al formulario
                    $.each(data, function(i, item) {
                        
    
                        if(item.G732_C9622 == 1){
                           $("#G732_C9622").attr('checked', true);
                        } 

                        $("#G732_C9623").val(item.G732_C9623);
    
                        if(item.G732_C9624 == 1){
                           $("#G732_C9624").attr('checked', true);
                        } 

                        $("#G732_C9625").val(item.G732_C9625);

                        $("#G732_C9626").val(item.G732_C9626);

                        $("#G732_C9627").val(item.G732_C9627);

                        $("#G732_C9628").val(item.G732_C9628);

                        $("#G732_C9629").val(item.G732_C9629);

                        $("#G732_C9630").val(item.G732_C9630);

                        $("#G732_C9631").val(item.G732_C9631);

                        $("#G732_C9632").val(item.G732_C9632);

                        $("#G732_C9633").val(item.G732_C9633);

                        $("#G732_C9680").val(item.G732_C9680);
    
                        if(item.G732_C9634 == 1){
                           $("#G732_C9634").attr('checked', true);
                        } 
    
                        if(item.G732_C9635 == 1){
                           $("#G732_C9635").attr('checked', true);
                        } 

                        $("#G732_C9636").val(item.G732_C9636);
    
                        if(item.G732_C9637 == 1){
                           $("#G732_C9637").attr('checked', true);
                        } 

                        $("#G732_C9638").val(item.G732_C9638);

                        $("#G732_C9639").val(item.G732_C9639);

                        $("#G732_C9640").val(item.G732_C9640);

                        $("#G732_C9641").val(item.G732_C9641);

                        $("#G732_C9642").val(item.G732_C9642);

                        $("#G732_C9643").val(item.G732_C9643);

                        $("#G732_C9644").val(item.G732_C9644);

                        $("#G732_C9645").val(item.G732_C9645);

                        $("#G732_C9646").val(item.G732_C9646);

                        $("#G732_C9647").val(item.G732_C9647);

                        $("#G732_C9771").val(item.G732_C9771);

                        $("#G732_C9772").val(item.G732_C9772);
    
                        if(item.G732_C9649 == 1){
                           $("#G732_C9649").attr('checked', true);
                        } 
    
                        if(item.G732_C9650 == 1){
                           $("#G732_C9650").attr('checked', true);
                        } 
    
                        if(item.G732_C9651 == 1){
                           $("#G732_C9651").attr('checked', true);
                        } 
    
                        if(item.G732_C9652 == 1){
                           $("#G732_C9652").attr('checked', true);
                        } 
    
                        if(item.G732_C9653 == 1){
                           $("#G732_C9653").attr('checked', true);
                        } 

                        $("#G732_C9654").val(item.G732_C9654);

                        $("#G732_C9655").val(item.G732_C9655);

                        $("#G732_C9656").val(item.G732_C9656);

                        $("#G732_C9657").val(item.G732_C9657);

                        $("#G732_C9700").val(item.G732_C9700);
        				$("#h3mio").html(item.principal);
                    });

                    //Deshabilitar los campos

                    //Habilitar todos los campos para edicion
                    $('#FormularioDatos :input').each(function(){
                        $(this).attr('disabled', true);
                    });

                    //Habilidar los botones de operacion, add, editar, eliminar
                    $("#add").attr('disabled', false);
                    $("#edit").attr('disabled', false);
                    $("#delete").attr('disabled', false);

                    //Desahabiliatra los botones de salvar y seleccionar_registro
                    $("#cancel").attr('disabled', true);
                    $("#Save").attr('disabled', true);
                } 
            });

            $("#hidId").val(id);
            idTotal = $("#hidId").val();
        });
    }

    function seleccionar_registro(){
        //Seleccinar loos registros de la Lista de navegacion, 
        if ( $("#"+idTotal).length > 0) {
            $("#"+idTotal).click();   
            $("#"+idTotal).addClass('active'); 
            idTotal = 0;
        }else{
            $(".CargarDatos :first").click();
        } 
        
    } 

    <?php } ?>


    

    function vamosRecargaLasGrillasPorfavor(id){
        
    }
</script>
<script type="text/javascript">
	$(document).ready(function() {
		<?php
            if(isset($campSql)){
                //recorro la campaña para tener los datos que necesito
                /*$resultcampSql = $mysqli->query($campSql);
                while($key = $resultcampSql->fetch_object()){
                    

                    //consulta de datos del usuario
                    $DatosSql = " SELECT ".$key->CAMINC_NomCamPob_b." as campo FROM ".$BaseDatos.".G".$tabla." WHERE G".$tabla."_ConsInte__b=".$_GET['user'];

                    //echo $DatosSql;
                    //recorro la tabla de donde necesito los datos
                    $resultDatosSql = $mysqli->query($DatosSql);
                    if($resultDatosSql){
                		while($objDatos = $resultDatosSql->fetch_object()){ ?>
                        	document.getElementById("<?=$key->CAMINC_NomCamGui_b;?>").value = '<?=trim($objDatos->campo);?>';
             		<?php  
             			}	
                    }
                    
                } */  
            }
        ?>
    	<?php if(isset($_GET['user'])){ ?>
        	
        	idTotal = <?php echo $_GET['user'];?>; 
        <?php } ?>
		
	});
</script>
