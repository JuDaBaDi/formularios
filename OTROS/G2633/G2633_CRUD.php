<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2633_ConsInte__b, G2633_FechaInsercion , G2633_Usuario ,  G2633_CodigoMiembro  , G2633_PoblacionOrigen , G2633_EstadoDiligenciamiento ,  G2633_IdLlamada , G2633_C51560 as principal ,G2633_C51560,G2633_C51561,G2633_C51562,G2633_C51563,G2633_C51564,G2633_C51565,G2633_C51566,G2633_C51385,G2633_C51386,G2633_C51387,G2633_C51388,G2633_C51389,G2633_C51390,G2633_C51391,G2633_C51392,G2633_C51393,G2633_C51568,G2633_C51569,G2633_C51570,G2633_C51571,G2633_C51572,G2633_C51573,G2633_C51574,G2633_C51575,G2633_C51576,G2633_C51577,G2633_C51578,G2633_C51579 FROM '.$BaseDatos.'.G2633 WHERE G2633_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2633_C51560'] = $key->G2633_C51560;

                $datos[$i]['G2633_C51561'] = $key->G2633_C51561;

                $datos[$i]['G2633_C51562'] = explode(' ', $key->G2633_C51562)[0];

                $datos[$i]['G2633_C51563'] = $key->G2633_C51563;

                $datos[$i]['G2633_C51564'] = $key->G2633_C51564;

                $datos[$i]['G2633_C51565'] = $key->G2633_C51565;

                $datos[$i]['G2633_C51566'] = $key->G2633_C51566;

                $datos[$i]['G2633_C51385'] = $key->G2633_C51385;

                $datos[$i]['G2633_C51386'] = $key->G2633_C51386;

                $datos[$i]['G2633_C51387'] = explode(' ', $key->G2633_C51387)[0];
  
                $hora = '';
                if(!is_null($key->G2633_C51388)){
                    $hora = explode(' ', $key->G2633_C51388)[1];
                }

                $datos[$i]['G2633_C51388'] = $hora;

                $datos[$i]['G2633_C51389'] = $key->G2633_C51389;

                $datos[$i]['G2633_C51390'] = $key->G2633_C51390;

                $datos[$i]['G2633_C51391'] = $key->G2633_C51391;

                $datos[$i]['G2633_C51392'] = $key->G2633_C51392;

                $datos[$i]['G2633_C51393'] = $key->G2633_C51393;

                $datos[$i]['G2633_C51568'] = $key->G2633_C51568;

                $datos[$i]['G2633_C51569'] = $key->G2633_C51569;

                $datos[$i]['G2633_C51570'] = $key->G2633_C51570;

                $datos[$i]['G2633_C51571'] = $key->G2633_C51571;

                $datos[$i]['G2633_C51572'] = explode(' ', $key->G2633_C51572)[0];

                $datos[$i]['G2633_C51573'] = explode(' ', $key->G2633_C51573)[0];

                $datos[$i]['G2633_C51574'] = $key->G2633_C51574;

                $datos[$i]['G2633_C51575'] = $key->G2633_C51575;

                $datos[$i]['G2633_C51576'] = $key->G2633_C51576;

                $datos[$i]['G2633_C51577'] = $key->G2633_C51577;

                $datos[$i]['G2633_C51578'] = $key->G2633_C51578;

                $datos[$i]['G2633_C51579'] = $key->G2633_C51579;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2633";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2633_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2633_ConsInte__b as id,  G2633_C51560 as camp1 , G2633_C51561 as camp2 
                     FROM ".$BaseDatos.".G2633  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2633_ConsInte__b as id,  G2633_C51560 as camp1 , G2633_C51561 as camp2  
                    FROM ".$BaseDatos.".G2633  JOIN ".$BaseDatos.".G2633_M".$_POST['muestra']." ON G2633_ConsInte__b = G2633_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2633_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2633_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2633_C51560 LIKE '%".$B."%' OR G2633_C51561 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2633_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se hará la ordenación de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenación
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2633");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2633_ConsInte__b, G2633_FechaInsercion , G2633_Usuario ,  G2633_CodigoMiembro  , G2633_PoblacionOrigen , G2633_EstadoDiligenciamiento ,  G2633_IdLlamada , G2633_C51560 as principal ,G2633_C51560,G2633_C51561,G2633_C51562,G2633_C51563,G2633_C51564,G2633_C51565,G2633_C51566, a.LISOPC_Nombre____b as G2633_C51385, b.LISOPC_Nombre____b as G2633_C51386,G2633_C51387,G2633_C51388,G2633_C51389,G2633_C51390,G2633_C51391,G2633_C51392,G2633_C51393, c.LISOPC_Nombre____b as G2633_C51568, d.LISOPC_Nombre____b as G2633_C51569, e.LISOPC_Nombre____b as G2633_C51570,G2633_C51571,G2633_C51572,G2633_C51573, f.LISOPC_Nombre____b as G2633_C51574,G2633_C51575,G2633_C51576,G2633_C51577,G2633_C51578,G2633_C51579 FROM '.$BaseDatos.'.G2633 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2633_C51385 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2633_C51386 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2633_C51568 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2633_C51569 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as e ON e.LISOPC_ConsInte__b =  G2633_C51570 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as f ON f.LISOPC_ConsInte__b =  G2633_C51574';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2633_C51388)){
                    $hora_a = explode(' ', $fila->G2633_C51388)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2633_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2633_ConsInte__b , ($fila->G2633_C51560) , ($fila->G2633_C51561) , explode(' ', $fila->G2633_C51562)[0] , ($fila->G2633_C51563) , ($fila->G2633_C51564) , ($fila->G2633_C51565) , ($fila->G2633_C51566) , ($fila->G2633_C51385) , ($fila->G2633_C51386) , explode(' ', $fila->G2633_C51387)[0] , $hora_a , ($fila->G2633_C51389) , ($fila->G2633_C51390) , ($fila->G2633_C51391) , ($fila->G2633_C51392) , ($fila->G2633_C51393) , ($fila->G2633_C51568) , ($fila->G2633_C51569) , ($fila->G2633_C51570) , ($fila->G2633_C51571) , explode(' ', $fila->G2633_C51572)[0] , explode(' ', $fila->G2633_C51573)[0] , ($fila->G2633_C51574) , ($fila->G2633_C51575) , ($fila->G2633_C51576) , ($fila->G2633_C51577) , ($fila->G2633_C51578) , ($fila->G2633_C51579) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2633 WHERE G2633_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2633";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2633_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2633_ConsInte__b as id,  G2633_C51560 as camp1 , G2633_C51561 as camp2  FROM '.$BaseDatos.'.G2633 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2633_ConsInte__b as id,  G2633_C51560 as camp1 , G2633_C51561 as camp2  
                    FROM ".$BaseDatos.".G2633  JOIN ".$BaseDatos.".G2633_M".$_POST['muestra']." ON G2633_ConsInte__b = G2633_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2633_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2633_C51560 LIKE "%'.$B.'%" OR G2633_C51561 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2633_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2633 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2633(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2633_C51560"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51560 = '".$_POST["G2633_C51560"]."'";
                $LsqlI .= $separador."G2633_C51560";
                $LsqlV .= $separador."'".$_POST["G2633_C51560"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2633_C51561"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51561 = '".$_POST["G2633_C51561"]."'";
                $LsqlI .= $separador."G2633_C51561";
                $LsqlV .= $separador."'".$_POST["G2633_C51561"]."'";
                $validar = 1;
            }
             
 
            $G2633_C51562 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2633_C51562"])){    
                if($_POST["G2633_C51562"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2633_C51562"]);
                    if(count($tieneHora) > 1){
                        $G2633_C51562 = "'".$_POST["G2633_C51562"]."'";
                    }else{
                        $G2633_C51562 = "'".str_replace(' ', '',$_POST["G2633_C51562"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2633_C51562 = ".$G2633_C51562;
                    $LsqlI .= $separador." G2633_C51562";
                    $LsqlV .= $separador.$G2633_C51562;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2633_C51563"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51563 = '".$_POST["G2633_C51563"]."'";
                $LsqlI .= $separador."G2633_C51563";
                $LsqlV .= $separador."'".$_POST["G2633_C51563"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2633_C51564"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51564 = '".$_POST["G2633_C51564"]."'";
                $LsqlI .= $separador."G2633_C51564";
                $LsqlV .= $separador."'".$_POST["G2633_C51564"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2633_C51565"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51565 = '".$_POST["G2633_C51565"]."'";
                $LsqlI .= $separador."G2633_C51565";
                $LsqlV .= $separador."'".$_POST["G2633_C51565"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2633_C51566"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51566 = '".$_POST["G2633_C51566"]."'";
                $LsqlI .= $separador."G2633_C51566";
                $LsqlV .= $separador."'".$_POST["G2633_C51566"]."'";
                $validar = 1;
            }
             
 
            $G2633_C51385 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2633_C51385 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2633_C51385 = ".$G2633_C51385;
                    $LsqlI .= $separador." G2633_C51385";
                    $LsqlV .= $separador.$G2633_C51385;
                    $validar = 1;

                    
                }
            }
 
            $G2633_C51386 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2633_C51386 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2633_C51386 = ".$G2633_C51386;
                    $LsqlI .= $separador." G2633_C51386";
                    $LsqlV .= $separador.$G2633_C51386;
                    $validar = 1;
                }
            }
 
            $G2633_C51387 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2633_C51387 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2633_C51387 = ".$G2633_C51387;
                    $LsqlI .= $separador." G2633_C51387";
                    $LsqlV .= $separador.$G2633_C51387;
                    $validar = 1;
                }
            }
 
            $G2633_C51388 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2633_C51388 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2633_C51388 = ".$G2633_C51388;
                    $LsqlI .= $separador." G2633_C51388";
                    $LsqlV .= $separador.$G2633_C51388;
                    $validar = 1;
                }
            }
 
            $G2633_C51389 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2633_C51389 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2633_C51389 = ".$G2633_C51389;
                    $LsqlI .= $separador." G2633_C51389";
                    $LsqlV .= $separador.$G2633_C51389;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2633_C51390"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51390 = '".$_POST["G2633_C51390"]."'";
                $LsqlI .= $separador."G2633_C51390";
                $LsqlV .= $separador."'".$_POST["G2633_C51390"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2633_C51391"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51391 = '".$_POST["G2633_C51391"]."'";
                $LsqlI .= $separador."G2633_C51391";
                $LsqlV .= $separador."'".$_POST["G2633_C51391"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2633_C51392"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51392 = '".$_POST["G2633_C51392"]."'";
                $LsqlI .= $separador."G2633_C51392";
                $LsqlV .= $separador."'".$_POST["G2633_C51392"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2633_C51393"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51393 = '".$_POST["G2633_C51393"]."'";
                $LsqlI .= $separador."G2633_C51393";
                $LsqlV .= $separador."'".$_POST["G2633_C51393"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2633_C51568"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51568 = '".$_POST["G2633_C51568"]."'";
                $LsqlI .= $separador."G2633_C51568";
                $LsqlV .= $separador."'".$_POST["G2633_C51568"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2633_C51569"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51569 = '".$_POST["G2633_C51569"]."'";
                $LsqlI .= $separador."G2633_C51569";
                $LsqlV .= $separador."'".$_POST["G2633_C51569"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2633_C51570"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51570 = '".$_POST["G2633_C51570"]."'";
                $LsqlI .= $separador."G2633_C51570";
                $LsqlV .= $separador."'".$_POST["G2633_C51570"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2633_C51571"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51571 = '".$_POST["G2633_C51571"]."'";
                $LsqlI .= $separador."G2633_C51571";
                $LsqlV .= $separador."'".$_POST["G2633_C51571"]."'";
                $validar = 1;
            }
             
 
            $G2633_C51572 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2633_C51572"])){    
                if($_POST["G2633_C51572"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2633_C51572"]);
                    if(count($tieneHora) > 1){
                        $G2633_C51572 = "'".$_POST["G2633_C51572"]."'";
                    }else{
                        $G2633_C51572 = "'".str_replace(' ', '',$_POST["G2633_C51572"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2633_C51572 = ".$G2633_C51572;
                    $LsqlI .= $separador." G2633_C51572";
                    $LsqlV .= $separador.$G2633_C51572;
                    $validar = 1;
                }
            }
 
            $G2633_C51573 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["G2633_C51573"])){    
                if($_POST["G2633_C51573"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $tieneHora = explode(' ' , $_POST["G2633_C51573"]);
                    if(count($tieneHora) > 1){
                        $G2633_C51573 = "'".$_POST["G2633_C51573"]."'";
                    }else{
                        $G2633_C51573 = "'".str_replace(' ', '',$_POST["G2633_C51573"])." 00:00:00'";
                    }


                    $LsqlU .= $separador." G2633_C51573 = ".$G2633_C51573;
                    $LsqlI .= $separador." G2633_C51573";
                    $LsqlV .= $separador.$G2633_C51573;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2633_C51574"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51574 = '".$_POST["G2633_C51574"]."'";
                $LsqlI .= $separador."G2633_C51574";
                $LsqlV .= $separador."'".$_POST["G2633_C51574"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2633_C51575"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51575 = '".$_POST["G2633_C51575"]."'";
                $LsqlI .= $separador."G2633_C51575";
                $LsqlV .= $separador."'".$_POST["G2633_C51575"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2633_C51576"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51576 = '".$_POST["G2633_C51576"]."'";
                $LsqlI .= $separador."G2633_C51576";
                $LsqlV .= $separador."'".$_POST["G2633_C51576"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2633_C51577"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51577 = '".$_POST["G2633_C51577"]."'";
                $LsqlI .= $separador."G2633_C51577";
                $LsqlV .= $separador."'".$_POST["G2633_C51577"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2633_C51578"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51578 = '".$_POST["G2633_C51578"]."'";
                $LsqlI .= $separador."G2633_C51578";
                $LsqlV .= $separador."'".$_POST["G2633_C51578"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2633_C51579"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_C51579 = '".$_POST["G2633_C51579"]."'";
                $LsqlI .= $separador."G2633_C51579";
                $LsqlV .= $separador."'".$_POST["G2633_C51579"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2633_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2633_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2633_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2633_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2633_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2633_Usuario , G2633_FechaInsercion, G2633_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2633_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2633 WHERE G2633_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            echo $Lsql; die();
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
