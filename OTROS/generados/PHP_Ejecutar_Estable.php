<?php
	ini_set('display_errors', 'On');
	ini_set('display_errors', 1);
	include(__DIR__."/../../conexion.php");
	date_default_timezone_set('America/Bogota');
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    	if($_GET['action'] == 'ADD'){
			/* toca insertar un registro vacio y editarlo desde el script */
			
			$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b, CAMPAN_ConfDinam_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];
	        $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	        $datoCampan = $res_Lsql_Campan->fetch_array();
	        $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	        $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	        $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	        $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];
         	$rea_ConfD_Campan = $datoCampan['CAMPAN_ConfDinam_b'];

			$Lsql = "INSERT INTO ".$BaseDatos.".".$str_Pobla_Campan." (".$str_Pobla_Campan."_FechaInsercion) VALUES ('".date('Y-m-d H:i:s')."');";
			if ($mysqli->query($Lsql) === TRUE) {

                $resultado = $mysqli->insert_id;
                
                if($rea_ConfD_Campan == '-1'){

                	$InsertMuestra = "INSERT INTO ".$BaseDatos.".".$str_Pobla_Campan."_M".$int_Muest_Campan." (".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoInMiPo__b , ".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b) VALUES ($resultado, 0);";
                	if($mysqli->query($InsertMuestra) !== true){
                		echo "error muestra = > ".$mysqli->error;
                	}
                
                }else{

                	$muestraCompleta = $str_Pobla_Campan."_M".$int_Muest_Campan;

                	$Xlsql = "SELECT ASITAR_ConsInte__USUARI_b, COUNT(".$muestraCompleta."_ConIntUsu_b) AS total FROM     ".$BaseDatos_systema.".ASITAR LEFT JOIN ".$BaseDatos.".".$muestraCompleta." ON ASITAR_ConsInte__USUARI_b = ".$muestraCompleta."_ConIntUsu_b WHERE ASITAR_ConsInte__CAMPAN_b = ".$_GET["campana_crm"]." AND (".$muestraCompleta."_Estado____b <> 3 OR (".$muestraCompleta."_Estado____b IS NULL)) AND (ASITAR_Automaticos_b <> 0 OR (ASITAR_Automaticos_b IS NULL)) GROUP BY ASITAR_ConsInte__USUARI_b ORDER BY COUNT(".$muestraCompleta."_ConIntUsu_b) LIMIT 1;";
                	$res = $mysqli->query($Xlsql);
                	$datoLsql = $res->fetch_array();

            	 	$insertarMuestraLsql = "INSERT INTO  ".$BaseDatos.".".$muestraCompleta." (".$muestraCompleta."_CoInMiPo__b ,  ".$muestraCompleta."_NumeInte__b, ".$muestraCompleta."_Estado____b , ".$muestraCompleta."_ConIntUsu_b) VALUES (".$resultado.", 0 , 0, ".$datoLsql['ASITAR_ConsInte__USUARI_b'].");";
  
            		if($mysqli->query($insertarMuestraLsql) !== true){
                		echo "error muestra = > ".$mysqli->error;
                	}
                }
                
                echo $resultado;
            
            }

		}
	
		if($_GET['action'] == 'EDIT'){

			/* primero buscamos la campaña que nos esta llegando */
			$Lsql_Campan = "SELECT CAMPAN_ConsInte__GUION__Pob_b, CAMPAN_ConsInte__MUESTR_b, CAMPAN_ConsInte__GUION__Gui_b  FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b = ".$_GET["campana_crm"];

			//echo $Lsql_Campan;

	        $res_Lsql_Campan = $mysqli->query($Lsql_Campan);
	        $datoCampan = $res_Lsql_Campan->fetch_array();
	        $str_Pobla_Campan = "G".$datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	        $int_Pobla_Camp_2 = $datoCampan['CAMPAN_ConsInte__GUION__Pob_b'];
	        $int_Muest_Campan = $datoCampan['CAMPAN_ConsInte__MUESTR_b'];
	        $int_Guion_Campan = $datoCampan['CAMPAN_ConsInte__GUION__Gui_b'];



	        /* Aqui se hace la jugada de la actualizacion */
	        if(!isset($_GET['cerrarForzado'])){
	        	$ActualizaLsql = "SELECT CAMPAN_ActPobGui_b FROM ".$BaseDatos_systema.".CAMPAN WHERE CAMPAN_ConsInte__b =".$_GET['campana_crm']; 

		        $resultado = $mysqli->query($ActualizaLsql);
		        $datoArray = $resultado->fetch_array();
		        if($datoArray['CAMPAN_ActPobGui_b'] == '-1'){
		        	/* toca hacer actualizacion desde Script */
		        	
		        	$campSql = "SELECT CAMINC_NomCamPob_b, CAMINC_NomCamGui_b FROM ".$BaseDatos_systema.".CAMINC WHERE CAMINC_ConsInte__CAMPAN_b = ".$_GET["campana_crm"];
					$resultcampSql = $mysqli->query($campSql);
					$Lsql = 'UPDATE '.$BaseDatos.'.'.$str_Pobla_Campan.' , '.$BaseDatos.'.G'.$int_Guion_Campan.' SET ';
					$i=0;
			        while($key = $resultcampSql->fetch_object()){
                        $validoparaedicion = false;
                        $valorScript = $key->CAMINC_NomCamGui_b;

                        $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE Field = '".$key->CAMINC_NomCamPob_b."'";

                        $resultShow = $mysqli->query($LsqlShow);
                        if($resultShow->num_rows === 0){
                            //comentario el campo no existe
                            $validoparaedicion = false;
                        }else{
                            $validoparaedicion = true;
                        } 

                        $LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE Field = '".$key->CAMINC_NomCamGui_b."'";
                        //echo $LsqlShow;
                        $resultShow = $mysqli->query($LsqlShow);
                        if($resultShow->num_rows === 0 ){
                            //comentario el campo no existe
                            $validoparaedicion = false;
                        }else{
                            $validoparaedicion = true;
                        } 

                        $LsqlPAsaNull = "SELECT ".$key->CAMINC_NomCamGui_b." as Campo_valido FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE  G".$int_Guion_Campan.'_ConsInte__b = '.$_GET['ConsInteRegresado'];
                        $LsqlRes = $mysqli->query($LsqlPAsaNull);
                        if($LsqlRes){
                            $sata = $LsqlRes->fetch_array();
                            if($sata['Campo_valido'] != '' && $sata['Campo_valido'] != null){

                            }else{
                                $valorScript = 'NULL';
                            }
                        }

                        if($validoparaedicion){
                            if($i == 0){
                                $Lsql .= $key->CAMINC_NomCamPob_b . ' = '.$valorScript;
                            }else{
                                $Lsql .= " , ".$key->CAMINC_NomCamPob_b . ' = '.$valorScript;
                            }
                            $i++;    
                        }
                        
			        } 
			        $Lsql .= ' WHERE  G'.$int_Guion_Campan.'_ConsInte__b = '.$_GET['ConsInteRegresado'].' AND G'.$int_Guion_Campan.'_CodigoMiembro = '.$str_Pobla_Campan.'_ConsInte__b'; 
			        //echo "Esta ".$Lsql;
			        if($mysqli->query($Lsql) === TRUE ){

			        }else{
			        	echo "NO SE ACTALIZO LA BASE DE DATOS ".$mysqli->error;
			        }
		        }


		        /* ACTUALIZAMOS EL ESTADO DE LA POBLACION */
		        $LsqlMonoEf = "SELECT LISOPC_Valor____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__b = ".$_POST['tipificacion'];
		        $resMonoEf = $mysqli->query($LsqlMonoEf);
		        $datosMonoEf = $resMonoEf->fetch_array();

		        if($datosMonoEf['LISOPC_Valor____b'] != '' &&  $datosMonoEf['LISOPC_Valor____b'] != NULL && $datosMonoEf['LISOPC_Valor____b'] != '0'){
		        	//Toca preguntar que exista el cmapo de estados
		        	$LsqPregun  = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = ".$int_Pobla_Camp_2." AND PREGUN_Texto_____b = 'ESTADO_DY'";
		        	$resPregun  = $mysqli->query($LsqPregun);
		        	if($resPregun->num_rows > 0){
		        		//El campo esta en la tabla pregun por lo menos
		        		$datoPregun = $resPregun->fetch_array();
		        		$LsqlShow = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE Field = '".$str_Pobla_Campan."_C".$datoPregun['PREGUN_ConsInte__b']."'";
		                $resultShow = $mysqli->query($LsqlShow);
		                //echo $LsqlShow;
		                if($resultShow->num_rows === 0){
		                	//comentario el campo no existe
		                }else{
		                	//El campo esta creado y toca llenarlo con el valor de  
		                	$LsqlUpdate = "UPDATE ".$BaseDatos.".".$str_Pobla_Campan." SET ".$str_Pobla_Campan."_C".$datoPregun['PREGUN_ConsInte__b']." = ".$datosMonoEf['LISOPC_Valor____b']." WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['CodigoMiembro'];
		                	if($mysqli->query($LsqlUpdate) === true){
		                		//Lo logro
		                	}else{
		                		echo "Error Actualizando el estado_dy => ".$mysqli->error;
		                	}
		                }	
		        	}
		        }

	        }
	        
	        
			
	        $reintento = null;
	        if(isset($_POST['reintento'])){
	        	if($_POST['reintento'] == 0){
	        		$reintento = $_POST['TipNoEF'];	
	        	}else{
	        		$reintento = $_POST['reintento'];	
	        	}
	        }else{
	        	$reintento = $_POST['TipNoEF'];
	        }

	        
	        $fechaAgenda = null;
	        $gestionMIMP = $_POST['MonoEf'];
	        $fechasGMIMP = date('Y-m-d H:i:s');
	        $conatcto = 0;
	        $contactoMasImp = 0;

	        if(isset($_POST['ContactoMonoEf']) && $_POST['ContactoMonoEf'] != ''){
	        	$conatcto = $_POST['ContactoMonoEf'];
				$contactoMasImp = $_POST['ContactoMonoEf'];
	        }


	        $LsqlMuestraAver = "SELECT ".$str_Pobla_Campan."_M".$int_Muest_Campan."_GesMasImp_b, ".$str_Pobla_Campan."_M".$int_Muest_Campan."_FeGeMaIm__b, ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoGesMaIm_b FROM ".$BaseDatos.".".$str_Pobla_Campan."_M".$int_Muest_Campan." WHERE ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoInMiPo__b = ".$_GET['CodigoMiembro'];

	        $resu = $mysqli->query($LsqlMuestraAver);
	        $key = $resu->fetch_array();

        	if(!is_null($key[$str_Pobla_Campan."_M".$int_Muest_Campan."_CoGesMaIm_b"])){

        		if($key[$str_Pobla_Campan."_M".$int_Muest_Campan."_CoGesMaIm_b"] > $conatcto){

        			$gestionMIMP = $key[$str_Pobla_Campan."_M".$int_Muest_Campan."_GesMasImp_b"];
        			$fechasGMIMP = $key[$str_Pobla_Campan."_M".$int_Muest_Campan."_FeGeMaIm__b"];
        			$contactoMasImp = $key[$str_Pobla_Campan."_M".$int_Muest_Campan."_CoGesMaIm_b"];
        		}
        	}
	        
        	$fechaAgenda = "NULL";
	        if(isset($_POST['TxtFechaReintento']) && $_POST['TxtFechaReintento'] != '' && $_POST['TxtFechaReintento'] != null){
	        	$fechaAgenda = "'".$_POST['TxtFechaReintento']." ".str_replace(" ", "",$_POST['TxtHoraReintento'])."'";
	        }

	        $LmonoEfLSql = "SELECT MONOEF_CanHorProxGes__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
	        $resMonoEf = $mysqli->query($LmonoEfLSql);
	        $dataMonoEf = $resMonoEf->fetch_array();

	        $MuestraSql = "UPDATE ".$BaseDatos.".".$str_Pobla_Campan."_M".$int_Muest_Campan." SET 
	        				".$str_Pobla_Campan."_M".$int_Muest_Campan."_Estado____b = ".$reintento." , 
	        				".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b = ".$str_Pobla_Campan."_M".$int_Muest_Campan."_NumeInte__b + 1, 
	        				".$str_Pobla_Campan."_M".$int_Muest_Campan."_UltiGest__b = ".$_POST['MonoEf'].", 
	        				".$str_Pobla_Campan."_M".$int_Muest_Campan."_ConUltGes_b = ".$conatcto.",
	        				".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecUltGes_b = '".$_GET['tiempo']."', 
	        				".$str_Pobla_Campan."_M".$int_Muest_Campan."_EfeUltGes_b = '".$_POST['Efectividad']."',
	        				".$str_Pobla_Campan."_M".$int_Muest_Campan."_Comentari_b = '".$_POST['textAreaComentarios']."',
	        				".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecHorAge_b = ".$fechaAgenda.",
	        				".$str_Pobla_Campan."_M".$int_Muest_Campan."_GesMasImp_b = ".$gestionMIMP.",
	        				".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoGesMaIm_b = ".$contactoMasImp.",
	        				".$str_Pobla_Campan."_M".$int_Muest_Campan."_FeGeMaIm__b = '".$fechasGMIMP."'";

	        if($dataMonoEf['MONOEF_CanHorProxGes__b'] != 0 && $reintento == 2){
	        	$Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan."_M".$int_Muest_Campan." WHERE Field = '".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecHorMinProGes__b'";
                $result = $mysqli->query($Lsql);
                if($result->num_rows === 0){
                	//comentario el campo no existe
                }else{
                	//El campo esta creado y toca 
                	$fechaActual = date('Y-m-d H:i:s');
                	$fechaFinal = strtotime ( '+ '.$dataMonoEf['MONOEF_CanHorProxGes__b'].' hours' , strtotime ( $fechaActual ) );
					$fechaFinal = date ( 'Y-m-d H:i:s' , $fechaFinal );
                	$MuestraSql .= ",".$str_Pobla_Campan."_M".$int_Muest_Campan."_FecHorMinProGes__b = '".$fechaFinal."'";
                }
	        }

	        $MuestraSql .= " WHERE ".$str_Pobla_Campan."_M".$int_Muest_Campan."_CoInMiPo__b = ".$_GET['CodigoMiembro'];
	       // echo $MuestraSql;
        	if($mysqli->query($MuestraSql) === true){

        	}else{
        		echo "Error insertando la muesta => ".$mysqli->error;
        	}


            $fechaInicial = new DateTime($_GET['tiempo']);
            $fechaFinal = new DateTime($_POST['FechaFinal']);
            $duracion = $fechaInicial->diff($fechaFinal);

        	/* Actualizar los datos de las getsiones en la base de datos */ 
	        /*VALIDAMOS QUE LOS CMAPOS EXISTAN esto es para que no paresca crispeta */
	        $validoCampos = 0;
	        $LsqlUpdateCampos = "UPDATE ".$BaseDatos.".".$str_Pobla_Campan." SET ";
            $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE Field = '".$str_Pobla_Campan."_UltiGest__b'";
            $result = $mysqli->query($Lsql);
            if($result->num_rows === 0){
            	$validoCampos = 0;
            }else{
            	//El campo existe y lo podemos modificar
            	$validoCampos = 1;
            	$LsqlUpdateCampos .= $str_Pobla_Campan."_UltiGest__b =  ".$_POST['MonoEf'];
            }

            $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE Field = '".$str_Pobla_Campan."_FecUltGes_b'";
            $result = $mysqli->query($Lsql);
            if($result->num_rows === 0){
            	$validoCampos = 0;
            }else{
            	//El campo existe y lo podemos modificar
            	if($validoCampos == 1){
            		$LsqlUpdateCampos .= " , ";	
            	}
            	$validoCampos = 1;
            	$LsqlUpdateCampos .= $str_Pobla_Campan."_FecUltGes_b =  '".$_GET['tiempo']."'";
            }

            $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE Field = '".$str_Pobla_Campan."_GesMasImp_b'";
            $result = $mysqli->query($Lsql);
            if($result->num_rows === 0){
            	$validoCampos = 0;
            }else{
            	//El campo existe y lo podemos modificar
            	if($validoCampos == 1){
            		$LsqlUpdateCampos .= " , ";	
            	}
            	$validoCampos = 1;
            	$LsqlUpdateCampos .= $str_Pobla_Campan."_GesMasImp_b =  ".$gestionMIMP;
            }

            $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".".$str_Pobla_Campan." WHERE Field = '".$str_Pobla_Campan."_FeGeMaIm__b'";
            $result = $mysqli->query($Lsql);
            if($result->num_rows === 0){
            	$validoCampos = 0;
            }else{
            	//El campo existe y lo podemos modificar
            	if($validoCampos == 1){
            		$LsqlUpdateCampos .= " , ";	
            	}
            	$validoCampos = 1;
            	$LsqlUpdateCampos .= $str_Pobla_Campan."_FeGeMaIm__b =  '".$fechasGMIMP."'";
            }

            if($validoCampos == 1){
            	$LsqlUpdateCampos .= " WHERE ".$str_Pobla_Campan."_ConsInte__b = ".$_GET['CodigoMiembro'];
            	if($mysqli->query($LsqlUpdateCampos) === true){

            	}else{
            		echo "Error actualizando la poblacion => ".$mysqli->error;
            	}
            }


            $valorId_Gestion_Cbx = '';
            $valorId_Gestion_Cbx_2 = '';
            if(isset($_POST['id_gestion_cbx']) && $_POST['id_gestion_cbx'] != '' && $_POST['id_gestion_cbx'] != null){

            	$valorId_Gestion_Cbx = $_POST['id_gestion_cbx'];
                $valorId_Gestion_Cbx_2 = $_POST['id_gestion_cbx'];
                
                /* Toca averiguar lo del Coninte de la gestio */
                $valorSentido = explode('_', $valorId_Gestion_Cbx_2)[1];
                $LsqlXUnique = "SELECT unique_id FROM dyalogo_telefonia.dy_llamadas_salientes where id_dy_llamada = '".$valorSentido."';";
                $resPXunique = $mysqli->query($LsqlXUnique);
                if($resPXunique){
                    $estoUnique = $resPXunique->fetch_array();
                    if( $estoUnique['unique_id'] != null &&  $estoUnique['unique_id'] != ''){
                        $valorId_Gestion_Cbx = $estoUnique['unique_id'];  
                    }else{
                        $valorId_Gestion_Cbx = $valorSentido;
                    }
                }else{
                    $valorId_Gestion_Cbx = $valorSentido;
                }

            }else{
            	$valorId_Gestion_Cbx = $_GET['id_gestion_cbx'];
                $valorId_Gestion_Cbx_2 = $_GET['id_gestion_cbx'];
                
                /* Toca averiguar lo del Coninte de la gestio */
                $valorSentido = explode('_', $valorId_Gestion_Cbx_2)[1];
                $LsqlXUnique = "SELECT unique_id FROM dyalogo_telefonia.dy_llamadas_salientes where id_dy_llamada = '".$valorSentido."';";
                $resPXunique = $mysqli->query($LsqlXUnique);
                if($resPXunique){
                    $estoUnique = $resPXunique->fetch_array();
                    if( $estoUnique['unique_id'] != null &&  $estoUnique['unique_id'] != ''){
                        $valorId_Gestion_Cbx = $estoUnique['unique_id'];  
                    }else{
                        $valorId_Gestion_Cbx = $valorSentido;
                    }
                }else{
                    $valorId_Gestion_Cbx = $valorSentido;
                }
            }

            $sentidoX = NULL;
            $sentidoY = 0;
            $valorSentido = NULL;
            /* Rellenar los datos del Script */
            $validoCamposX = 0;
	        $LsqlUpdateCamposX = "UPDATE ".$BaseDatos.".G".$int_Guion_Campan." SET ";
            $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE Field = 'G".$int_Guion_Campan."_Sentido___b'";
            $result = $mysqli->query($Lsql);
            if($result->num_rows === 0){
            	$validoCamposX = 0;
            }else{
            	//El campo existe y lo podemos modificar
            	$validoCamposX = 1;
            	$sentidoX = '0';
            	if(isset($_POST['cbx_sentido']) && $_POST['cbx_sentido'] != 0){
                    $sentidoY = $_POST['cbx_sentido'];
            		if($_POST['cbx_sentido'] == '1'){
            			$sentidoX = 'Saliente';
            		}else{
            			$sentidoX = 'Entrante';
            		}
            	}else{
            		$sentidoX = 0;
            	}
            	$LsqlUpdateCamposX .= "G".$int_Guion_Campan."_Sentido___b =  '".$sentidoX."'";
            }

            $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE Field = 'G".$int_Guion_Campan."_Canal_____b'";
            $result = $mysqli->query($Lsql);
            if($result->num_rows === 0){
            	$validoCamposX = 0;
            }else{
            	//El campo existe y lo podemos modificar
            	if($validoCamposX == 1){
            		$LsqlUpdateCamposX .= " , ";	
            	}
            	$validoCamposX = 1;

            	$valorSentido = explode('_', $valorId_Gestion_Cbx_2)[0];

            	$LsqlUpdateCamposX .= "G".$int_Guion_Campan."_Canal_____b =  '".$valorSentido."'";
            }


            $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE Field = 'G".$int_Guion_Campan."_Duracion___b'";
            $result = $mysqli->query($Lsql);
            if($result->num_rows === 0){
                $edit_Lsql = "ALTER TABLE ".$BaseDatos.".G".$int_Guion_Campan." ADD G".$int_Guion_Campan."_Duracion___b TIME DEFAULT NULL";
                $mysqli->query($edit_Lsql);   

                if($validoCamposX == 1){
                    $LsqlUpdateCamposX .= " , ";    
                }

                $LsqlUpdateCamposX .= "G".$int_Guion_Campan."_Duracion___b =  '".$duracion->format("%H:%I:%S")."'";    
               

            }else{
                //El campo existe y lo podemos modificar
                $validoCamposX = 1;
                if($validoCamposX == 1){
                    $LsqlUpdateCamposX .= " , ";    
                }


                $LsqlUpdateCamposX .= "G".$int_Guion_Campan."_Duracion___b =  '".$duracion->format("%H:%I:%S")."'";

                $validoCamposX = 1;
            }

            if($validoCamposX == 1){

                $LsqlReintento = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_Texto_____b = 'Reintento' AND PREGUN_ConsInte__GUION__b = ".$int_Guion_Campan.";";
                $res = $mysqli->query($LsqlReintento);
                if($res){
                    $datoReintento = $res->fetch_array();
                    $Lsql = "SHOW COLUMNS FROM ".$BaseDatos.".G".$int_Guion_Campan." WHERE Field = 'G".$int_Guion_Campan."_C".$datoReintento['PREGUN_ConsInte__b']."' ";
                    $result = $mysqli->query($Lsql);
                    if($result->num_rows === 0){
                        
                    }else{

                        $LsqlUpdateCamposX .= " , G".$int_Guion_Campan."_C".$datoReintento['PREGUN_ConsInte__b']." =  '".$reintento."' ";
                    }
                }

                $LsqlUpdateCamposX .= " , G".$int_Guion_Campan."_IdLlamada =  '".$valorId_Gestion_Cbx."' , G".$int_Guion_Campan."_FechaInsercion = '".$_GET['tiempo']."' ";

            	$LsqlUpdateCamposX .= " WHERE G".$int_Guion_Campan."_ConsInte__b = ".$_GET['ConsInteRegresado'];

            	if($mysqli->query($LsqlUpdateCamposX) === true){

            	}else{
            		echo "Error actualizando el sentido y canal del Script => ".$mysqli->error;
            	}
            }

	        //Rellenar los datos de condia 
            

	        
            
	        

	       	$SqlGESTIEM = "SELECT GESTIEM_TiempoUP FROM ".$BaseDatos_systema.".GESTIEM WHERE GESTIEM_Id_Gestion_cbx = '".$valorId_Gestion_Cbx_2."'";   
	        $re = $mysqli->query($SqlGESTIEM);
	        $fechaTimeUp = NULL;
	        while($k = $re->fetch_object()){
	        	$fechaTimeUp = $k->GESTIEM_TiempoUP;	
	        }
       			
	        $CondiaSql = '';
	        if(!is_null($fechaTimeUp) && !is_null($fechaAgenda)){
		        //Insertar en CONDIA 
		        $CondiaSql = "INSERT INTO ".$BaseDatos_systema.".CONDIA (
			        	CONDIA_IndiEfec__b, 
			        	CONDIA_TipNo_Efe_b, 
			        	CONDIA_ConsInte__MONOEF_b, 
			        	CONDIA_TiemDura__b, 
			        	CONDIA_Fecha_____b, 
			        	CONDIA_ConsInte__CAMPAN_b, 
			        	CONDIA_ConsInte__USUARI_b, 
			        	CONDIA_ConsInte__GUION__Gui_b, 
			        	CONDIA_ConsInte__GUION__Pob_b, 
			        	CONDIA_ConsInte__MUESTR_b, 
			        	CONDIA_CodiMiem__b, 
			        	CONDIA_Observacio_b, 
			        	CONDIA_FechaAgenda_b,
			        	CONDIA_IdenLlam___b,
			        	CONDIA_TiemPrev__b,
			        	CONDIA_TiemUp____b, 
			        	CONDIA_Canal_b,
			        	CONDIA_Sentido___b,
                        CONDIA_UniqueId_b) 
			        	VALUES (
		        		'".$_POST['Efectividad']."', 
		        		'".$reintento."',
		        		'".$_POST['MonoEf']."',
		        		'".date('Y-m-d').' '.$duracion->format("%H:%I:%S")."',
		        		'".$_GET['tiempo']."',
		        		'".$_GET["campana_crm"]."',
		        		'".$_GET['usuario']."',
		        		'".$int_Guion_Campan."',
		        		'".$int_Pobla_Camp_2."',
		        		'".$int_Muest_Campan."',
		        		'".$_GET['CodigoMiembro']."',
		        		'".$_POST['textAreaComentarios']."' ,
		        		".$fechaAgenda.",
		        		'".$valorId_Gestion_Cbx."',
		        		0,
		        		'".$fechaTimeUp."',
		        		'".$valorSentido."',
		        		'".$sentidoY."',
                        '".$valorId_Gestion_Cbx."'
	        		)";
	        	
	        }else if(is_null($fechaTimeUp) && !is_null($fechaAgenda)){
	        	$CondiaSql = "INSERT INTO ".$BaseDatos_systema.".CONDIA (
			        	CONDIA_IndiEfec__b, 
			        	CONDIA_TipNo_Efe_b, 
			        	CONDIA_ConsInte__MONOEF_b, 
			        	CONDIA_TiemDura__b, 
			        	CONDIA_Fecha_____b, 
			        	CONDIA_ConsInte__CAMPAN_b, 
			        	CONDIA_ConsInte__USUARI_b, 
			        	CONDIA_ConsInte__GUION__Gui_b, 
			        	CONDIA_ConsInte__GUION__Pob_b, 
			        	CONDIA_ConsInte__MUESTR_b, 
			        	CONDIA_CodiMiem__b, 
			        	CONDIA_Observacio_b, 
			        	CONDIA_FechaAgenda_b,
			        	CONDIA_IdenLlam___b,
			        	CONDIA_TiemPrev__b, 
			        	CONDIA_Canal_b,
			        	CONDIA_Sentido___b,
                        CONDIA_UniqueId_b) 
			        	VALUES (
		        		'".$_POST['Efectividad']."', 
		        		'".$reintento."',
		        		'".$_POST['MonoEf']."',
		        		'".date('Y-m-d').' '.$duracion->format("%H:%I:%S")."',
		        		'".$_GET['tiempo']."',
		        		'".$_GET["campana_crm"]."',
		        		'".$_GET['usuario']."',
		        		'".$int_Guion_Campan."',
		        		'".$int_Pobla_Camp_2."',
		        		'".$int_Muest_Campan."',
		        		'".$_GET['CodigoMiembro']."',
		        		'".$_POST['textAreaComentarios']."' ,
		        		".$fechaAgenda.",
		        		'".$valorId_Gestion_Cbx."',
		        		0,
		        		'".$valorSentido."',
		        		'".$sentidoY."',
                        '".$valorId_Gestion_Cbx."'
	        		)";

	        }else if(!is_null($fechaTimeUp) && is_null($fechaAgenda)){
	        	$CondiaSql = "INSERT INTO ".$BaseDatos_systema.".CONDIA (
			        	CONDIA_IndiEfec__b, 
			        	CONDIA_TipNo_Efe_b, 
			        	CONDIA_ConsInte__MONOEF_b, 
			        	CONDIA_TiemDura__b, 
			        	CONDIA_Fecha_____b, 
			        	CONDIA_ConsInte__CAMPAN_b, 
			        	CONDIA_ConsInte__USUARI_b, 
			        	CONDIA_ConsInte__GUION__Gui_b, 
			        	CONDIA_ConsInte__GUION__Pob_b, 
			        	CONDIA_ConsInte__MUESTR_b, 
			        	CONDIA_CodiMiem__b, 
			        	CONDIA_Observacio_b, 
			        	CONDIA_IdenLlam___b,
			        	CONDIA_TiemPrev__b,
			        	CONDIA_TiemUp____b,
			        	CONDIA_Canal_b,
			        	CONDIA_Sentido___b,
                        CONDIA_UniqueId_b) 
			        	VALUES (
		        		'".$_POST['Efectividad']."', 
		        		'".$reintento."',
		        		'".$_POST['MonoEf']."',
		        		'".date('Y-m-d').' '.$duracion->format("%H:%I:%S")."',
		        		'".$_GET['tiempo']."',
		        		'".$_GET["campana_crm"]."',
		        		'".$_GET['usuario']."',
		        		'".$int_Guion_Campan."',
		        		'".$int_Pobla_Camp_2."',
		        		'".$int_Muest_Campan."',
		        		'".$_GET['CodigoMiembro']."',
		        		'".$_POST['textAreaComentarios']."' ,
		        		'".$valorId_Gestion_Cbx."',
		        		0,
		        		'".$fechaTimeUp."',
		        		'".$valorSentido."',
		        		'".$sentidoY."',
                        '".$valorId_Gestion_Cbx."'
	        		)";
	    
	        }else if(is_null($fechaTimeUp) && is_null($fechaAgenda)){
	        	$CondiaSql = "INSERT INTO ".$BaseDatos_systema.".CONDIA (
			        	CONDIA_IndiEfec__b, 
			        	CONDIA_TipNo_Efe_b, 
			        	CONDIA_ConsInte__MONOEF_b, 
			        	CONDIA_TiemDura__b, 
			        	CONDIA_Fecha_____b, 
			        	CONDIA_ConsInte__CAMPAN_b, 
			        	CONDIA_ConsInte__USUARI_b, 
			        	CONDIA_ConsInte__GUION__Gui_b, 
			        	CONDIA_ConsInte__GUION__Pob_b, 
			        	CONDIA_ConsInte__MUESTR_b, 
			        	CONDIA_CodiMiem__b, 
			        	CONDIA_Observacio_b, 
			        	CONDIA_IdenLlam___b,
			        	CONDIA_TiemPrev__b,
			        	CONDIA_Canal_b,
			        	CONDIA_Sentido___b,
                        CONDIA_UniqueId_b) 
			        	VALUES (
		        		'".$_POST['Efectividad']."', 
		        		'".$reintento."',
		        		'".$_POST['MonoEf']."',
		        		'".date('Y-m-d').' '.$duracion->format("%H:%I:%S")."',
		        		'".$_GET['tiempo']."',
		        		'".$_GET["campana_crm"]."',
		        		'".$_GET['usuario']."',
		        		'".$int_Guion_Campan."',
		        		'".$int_Pobla_Camp_2."',
		        		'".$int_Muest_Campan."',
		        		'".$_GET['CodigoMiembro']."',
		        		'".$_POST['textAreaComentarios']."' ,
		        		'".$valorId_Gestion_Cbx."',
		        		0,
		        		'".$valorSentido."',
		        		'".$sentidoY."',
                        '".$valorId_Gestion_Cbx."'
	        		)";
	 
	        }

	        if($mysqli->query($CondiaSql) === true){

	        }else{
	        	echo "Error insertando Condia => ".$mysqli->error;
	        }

	        if(isset($_GET['token']) && isset($_GET['id_gestion_cbx']) ){
	        	
	        	$data =  array();
	        	$conatacto = 0;
	        	if(isset($_POST['ContactoMonoEf']) && $_POST['ContactoMonoEf'] != '' ){
	        		$conatacto = $_POST['ContactoMonoEf'];
	        	}

	        	$consinte = -1;
	        	if(isset($_GET['consinte'])){
	        		$consinte = $_GET['consinte'];
	        	}

        		if(!isset($_POST['TxtFechaReintento'])){
	        		$data = array(	
	        					"strToken_t" => $_GET['token'], 
								"strIdGestion_t" => $valorId_Gestion_Cbx_2, 
								"intTipoReintento_t" => $reintento,
								"intConsInte_t"	=> $consinte,
								"strFechaHoraAgenda_t" => null,
								"booForzarCierre_t" => true,
								"intMonoefEfectiva_t" => $_POST['Efectividad'],
								"intConsInteTipificacion_t" => $_POST['MonoEf'],
								"boolFinalizacionDesdeBlend_t" => false,
								"intMonoefContacto_t" => $conatacto
							); 
				}else{
					
					$data = array(	
							"strToken_t" => $_GET['token'], 
							"strIdGestion_t" => $valorId_Gestion_Cbx_2, 
							"intTipoReintento_t" => $reintento,
							"strFechaHoraAgenda_t" => $_POST['TxtFechaReintento']." ".str_replace(" ", "",$_POST['TxtHoraReintento']),
							"intConsInte_t"	=> $consinte,
							"booForzarCierre_t" => true,
							"intMonoefEfectiva_t" => $_POST['Efectividad'],
							"intConsInteTipificacion_t" => $_POST['MonoEf'],
							"boolFinalizacionDesdeBlend_t" => false,
							"intMonoefContacto_t" => $conatacto
						); 
				}
	        	
		        $ch = curl_init($IP_CONFIGURADA.'gestion/finalizar');
				                                                                   
				$data_string = json_encode($data);  

				//echo $data_string;  
				//especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string); 

				//le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
				    'Content-Type: application/json',                                                                                
				    'Content-Length: ' . strlen($data_string))                                                                      
				); 
				//recogemos la respuesta
				$respuesta = curl_exec ($ch);
				//o el error, por si falla
				$error = curl_error($ch);
				//y finalmente cerramos curl
				/*echo "Respuesta =>  ". $respuesta;
				echo "<br/>Error => ".$error;*/

				curl_close ($ch);
			}
		}
	}