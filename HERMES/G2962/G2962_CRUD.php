<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2962_ConsInte__b, G2962_FechaInsercion , G2962_Usuario ,  G2962_CodigoMiembro  , G2962_PoblacionOrigen , G2962_EstadoDiligenciamiento ,  G2962_IdLlamada , G2962_C59173 as principal ,G2962_C59162,G2962_C59163,G2962_C59164,G2962_C59165,G2962_C59166,G2962_C59167,G2962_C59168,G2962_C59169,G2962_C59170,G2962_C59172,G2962_C59173,G2962_C59174,G2962_C59298,G2962_C59175,G2962_C59176,G2962_C59228,G2962_C59177,G2962_C59216,G2962_C59223 FROM '.$BaseDatos.'.G2962 WHERE G2962_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2962_C59162'] = $key->G2962_C59162;

                $datos[$i]['G2962_C59163'] = $key->G2962_C59163;

                $datos[$i]['G2962_C59164'] = explode(' ', $key->G2962_C59164)[0];
  
                $hora = '';
                if(!is_null($key->G2962_C59165)){
                    $hora = explode(' ', $key->G2962_C59165)[1];
                }

                $datos[$i]['G2962_C59165'] = $hora;

                $datos[$i]['G2962_C59166'] = $key->G2962_C59166;

                $datos[$i]['G2962_C59167'] = $key->G2962_C59167;

                $datos[$i]['G2962_C59168'] = $key->G2962_C59168;

                $datos[$i]['G2962_C59169'] = $key->G2962_C59169;

                $datos[$i]['G2962_C59170'] = $key->G2962_C59170;

                $datos[$i]['G2962_C59172'] = $key->G2962_C59172;

                $datos[$i]['G2962_C59173'] = $key->G2962_C59173;

                $datos[$i]['G2962_C59174'] = $key->G2962_C59174;

                $datos[$i]['G2962_C59298'] = $key->G2962_C59298;

                $datos[$i]['G2962_C59175'] = $key->G2962_C59175;

                $datos[$i]['G2962_C59176'] = $key->G2962_C59176;

                $datos[$i]['G2962_C59228'] = $key->G2962_C59228;

                $datos[$i]['G2962_C59177'] = $key->G2962_C59177;

                $datos[$i]['G2962_C59216'] = $key->G2962_C59216;

                $datos[$i]['G2962_C59223'] = $key->G2962_C59223;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2962";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2962_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2962_ConsInte__b as id,  G2962_C59172 as camp2 , G2962_C59173 as camp1 
                     FROM ".$BaseDatos.".G2962  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2962_ConsInte__b as id,  G2962_C59172 as camp2 , G2962_C59173 as camp1  
                    FROM ".$BaseDatos.".G2962  JOIN ".$BaseDatos.".G2962_M".$_POST['muestra']." ON G2962_ConsInte__b = G2962_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2962_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2962_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2962_C59172 LIKE '%".$B."%' OR G2962_C59173 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2962_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G2962_C59228'])){
                                $Ysql = "SELECT G2965_ConsInte__b as id, G2965_C59221 as text FROM ".$BaseDatos.".G2965 WHERE G2965_C59221 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G2962_C59228"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G2965 WHERE G2965_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G2962_C59228"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }

            if(isset($_GET['MostrarCombo_Guion_2961_C59227'])){
                echo '<select class="form-control input-sm"  name="2961_C59227" id="2961_C59227">';
                echo '<option >Buscar</option>';
                echo '</select>';
            }

            if(isset($_GET['CallDatosCombo_Guion_2961_C59227'])){
                $Ysql = "SELECT G2965_ConsInte__b as id,  G2965_C59221 as text FROM ".$BaseDatos.".G2965 WHERE G2965_C59221 LIKE '%".$_POST['q']."%'";
                $guion = $mysqli->query($Ysql);
                $i = 0;
                $datos = array();
                while($obj = $guion->fetch_object()){
                    $datos[$i]['id'] = $obj->id;
                    $datos[$i]['text'] = $obj->text;
                    $i++;
                } 
                echo json_encode($datos);
            }

            if(isset($_POST['dameValoresCamposDinamicos_Guion_2961_C59227'])){
                $Lsql = "SELECT  G2965_ConsInte__b as id , G2965_C59222 FROM ".$BaseDatos.".G2965 WHERE G2965_ConsInte__b = ".$_POST['dameValoresCamposDinamicos_Guion_2961_C59227'];
                $res = $mysqli->query($Lsql);
                $data = array();
                $i = 0;
                while ($key = $res->fetch_object()) {
                    
                    $data[$i]['G2961_C59158'] = $key->G2965_C59222;

                    $i++;
                }

                echo json_encode($data);
            }
            


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2962");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2962_ConsInte__b, G2962_FechaInsercion , G2962_Usuario ,  G2962_CodigoMiembro  , G2962_PoblacionOrigen , G2962_EstadoDiligenciamiento ,  G2962_IdLlamada , G2962_C59173 as principal , a.LISOPC_Nombre____b as G2962_C59162, b.LISOPC_Nombre____b as G2962_C59163,G2962_C59164,G2962_C59165,G2962_C59166,G2962_C59167,G2962_C59168,G2962_C59169,G2962_C59170,G2962_C59172,G2962_C59173,G2962_C59174,G2962_C59298,G2962_C59175,G2962_C59176, G2965_C59221,G2962_C59177, c.LISOPC_Nombre____b as G2962_C59216, d.LISOPC_Nombre____b as G2962_C59223 FROM '.$BaseDatos.'.G2962 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2962_C59162 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2962_C59163 LEFT JOIN '.$BaseDatos.'.G2965 ON G2965_ConsInte__b  =  G2962_C59228 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2962_C59216 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as d ON d.LISOPC_ConsInte__b =  G2962_C59223';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2962_C59165)){
                    $hora_a = explode(' ', $fila->G2962_C59165)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2962_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2962_ConsInte__b , ($fila->G2962_C59162) , ($fila->G2962_C59163) , explode(' ', $fila->G2962_C59164)[0] , $hora_a , ($fila->G2962_C59166) , ($fila->G2962_C59167) , ($fila->G2962_C59168) , ($fila->G2962_C59169) , ($fila->G2962_C59170) , ($fila->G2962_C59172) , ($fila->G2962_C59173) , ($fila->G2962_C59174) , ($fila->G2962_C59298) , ($fila->G2962_C59175) , ($fila->G2962_C59176) , ($fila->G2965_C59221) , ($fila->G2962_C59177) , ($fila->G2962_C59216) , ($fila->G2962_C59223) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2962 WHERE G2962_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2962";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2962_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2962_ConsInte__b as id,  G2962_C59172 as camp2 , G2962_C59173 as camp1  FROM '.$BaseDatos.'.G2962 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2962_ConsInte__b as id,  G2962_C59172 as camp2 , G2962_C59173 as camp1  
                    FROM ".$BaseDatos.".G2962  JOIN ".$BaseDatos.".G2962_M".$_POST['muestra']." ON G2962_ConsInte__b = G2962_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2962_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2962_C59172 LIKE "%'.$B.'%" OR G2962_C59173 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2962_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2962 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2962(";
            $LsqlV = " VALUES ("; 
 
            $G2962_C59162 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2962_C59162 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2962_C59162 = ".$G2962_C59162;
                    $LsqlI .= $separador." G2962_C59162";
                    $LsqlV .= $separador.$G2962_C59162;
                    $validar = 1;

                    
                }
            }
 
            $G2962_C59163 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2962_C59163 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2962_C59163 = ".$G2962_C59163;
                    $LsqlI .= $separador." G2962_C59163";
                    $LsqlV .= $separador.$G2962_C59163;
                    $validar = 1;
                }
            }
 
            $G2962_C59164 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2962_C59164 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2962_C59164 = ".$G2962_C59164;
                    $LsqlI .= $separador." G2962_C59164";
                    $LsqlV .= $separador.$G2962_C59164;
                    $validar = 1;
                }
            }
 
            $G2962_C59165 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2962_C59165 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2962_C59165 = ".$G2962_C59165;
                    $LsqlI .= $separador." G2962_C59165";
                    $LsqlV .= $separador.$G2962_C59165;
                    $validar = 1;
                }
            }
 
            $G2962_C59166 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2962_C59166 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2962_C59166 = ".$G2962_C59166;
                    $LsqlI .= $separador." G2962_C59166";
                    $LsqlV .= $separador.$G2962_C59166;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2962_C59167"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_C59167 = '".$_POST["G2962_C59167"]."'";
                $LsqlI .= $separador."G2962_C59167";
                $LsqlV .= $separador."'".$_POST["G2962_C59167"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2962_C59168"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_C59168 = '".$_POST["G2962_C59168"]."'";
                $LsqlI .= $separador."G2962_C59168";
                $LsqlV .= $separador."'".$_POST["G2962_C59168"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2962_C59169"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_C59169 = '".$_POST["G2962_C59169"]."'";
                $LsqlI .= $separador."G2962_C59169";
                $LsqlV .= $separador."'".$_POST["G2962_C59169"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2962_C59170"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_C59170 = '".$_POST["G2962_C59170"]."'";
                $LsqlI .= $separador."G2962_C59170";
                $LsqlV .= $separador."'".$_POST["G2962_C59170"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2962_C59171"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_C59171 = '".$_POST["G2962_C59171"]."'";
                $LsqlI .= $separador."G2962_C59171";
                $LsqlV .= $separador."'".$_POST["G2962_C59171"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2962_C59172"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_C59172 = '".$_POST["G2962_C59172"]."'";
                $LsqlI .= $separador."G2962_C59172";
                $LsqlV .= $separador."'".$_POST["G2962_C59172"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2962_C59173"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_C59173 = '".$_POST["G2962_C59173"]."'";
                $LsqlI .= $separador."G2962_C59173";
                $LsqlV .= $separador."'".$_POST["G2962_C59173"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2962_C59174"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_C59174 = '".$_POST["G2962_C59174"]."'";
                $LsqlI .= $separador."G2962_C59174";
                $LsqlV .= $separador."'".$_POST["G2962_C59174"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2962_C59298"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_C59298 = '".$_POST["G2962_C59298"]."'";
                $LsqlI .= $separador."G2962_C59298";
                $LsqlV .= $separador."'".$_POST["G2962_C59298"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2962_C59175"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_C59175 = '".$_POST["G2962_C59175"]."'";
                $LsqlI .= $separador."G2962_C59175";
                $LsqlV .= $separador."'".$_POST["G2962_C59175"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2962_C59176"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_C59176 = '".$_POST["G2962_C59176"]."'";
                $LsqlI .= $separador."G2962_C59176";
                $LsqlV .= $separador."'".$_POST["G2962_C59176"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2962_C59228"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_C59228 = '".$_POST["G2962_C59228"]."'";
                $LsqlI .= $separador."G2962_C59228";
                $LsqlV .= $separador."'".$_POST["G2962_C59228"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2962_C59177"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_C59177 = '".$_POST["G2962_C59177"]."'";
                $LsqlI .= $separador."G2962_C59177";
                $LsqlV .= $separador."'".$_POST["G2962_C59177"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2962_C59216"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_C59216 = '".$_POST["G2962_C59216"]."'";
                $LsqlI .= $separador."G2962_C59216";
                $LsqlV .= $separador."'".$_POST["G2962_C59216"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2962_C59223"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_C59223 = '".$_POST["G2962_C59223"]."'";
                $LsqlI .= $separador."G2962_C59223";
                $LsqlV .= $separador."'".$_POST["G2962_C59223"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2962_C59178"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_C59178 = '".$_POST["G2962_C59178"]."'";
                $LsqlI .= $separador."G2962_C59178";
                $LsqlV .= $separador."'".$_POST["G2962_C59178"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2962_C59179"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_C59179 = '".$_POST["G2962_C59179"]."'";
                $LsqlI .= $separador."G2962_C59179";
                $LsqlV .= $separador."'".$_POST["G2962_C59179"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2962_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2962_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2962_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2962_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2962_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2962_Usuario , G2962_FechaInsercion, G2962_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2962_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2962 WHERE G2962_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id']; 

        $SQL = "SELECT G2961_ConsInte__b, G2961_C59155, G2961_C59154, G2961_C59224, G2961_C59156, G2965_C59221 as G2961_C59227, G2961_C59158, g.LISOPC_Nombre____b as  G2961_C59159, G2961_C59160, i.LISOPC_Nombre____b as  G2961_C59161 FROM ".$BaseDatos.".G2961  LEFT JOIN ".$BaseDatos.".G2965 ON G2965_ConsInte__b  =  G2961_C59227 LEFT JOIN ".$BaseDatos_systema.".LISOPC as g ON g.LISOPC_ConsInte__b =  G2961_C59159 LEFT JOIN ".$BaseDatos_systema.".LISOPC as i ON i.LISOPC_ConsInte__b =  G2961_C59161 ";

        $SQL .= " WHERE G2961_C59155 = '".$numero."'"; 

        $SQL .= " ORDER BY G2961_C59155";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2961_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2961_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G2961_C59155)."</cell>";

                echo "<cell>". ($fila->G2961_C59154)."</cell>";

                echo "<cell>". ($fila->G2961_C59224)."</cell>";

                echo "<cell>". ($fila->G2961_C59156)."</cell>";

                echo "<cell>". ($fila->G2961_C59227)."</cell>";

                echo "<cell>". ($fila->G2961_C59158)."</cell>";

                echo "<cell>". ($fila->G2961_C59159)."</cell>";

                echo "<cell><![CDATA[". ($fila->G2961_C59160)."]]></cell>";

                echo "<cell>". ($fila->G2961_C59161)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2961 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2961(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G2961_C59154"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2961_C59154 = '".$_POST["G2961_C59154"]."'";
                    $LsqlI .= $separador."G2961_C59154";
                    $LsqlV .= $separador."'".$_POST["G2961_C59154"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2961_C59224"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2961_C59224 = '".$_POST["G2961_C59224"]."'";
                    $LsqlI .= $separador."G2961_C59224";
                    $LsqlV .= $separador."'".$_POST["G2961_C59224"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2961_C59156"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2961_C59156 = '".$_POST["G2961_C59156"]."'";
                    $LsqlI .= $separador."G2961_C59156";
                    $LsqlV .= $separador."'".$_POST["G2961_C59156"]."'";
                    $validar = 1;
                }

                                                                               
  
                if(isset($_POST["G2961_C59227"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2961_C59227 = '".$_POST["G2961_C59227"]."'";
                    $LsqlI .= $separador."G2961_C59227";
                    $LsqlV .= $separador."'".$_POST["G2961_C59227"]."'";
                    $validar = 1;
                }
                
 

                if(isset($_POST["G2961_C59158"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2961_C59158 = '".$_POST["G2961_C59158"]."'";
                    $LsqlI .= $separador."G2961_C59158";
                    $LsqlV .= $separador."'".$_POST["G2961_C59158"]."'";
                    $validar = 1;
                }

                                                                               
 
                if(isset($_POST["G2961_C59159"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2961_C59159 = '".$_POST["G2961_C59159"]."'";
                    $LsqlI .= $separador."G2961_C59159";
                    $LsqlV .= $separador."'".$_POST["G2961_C59159"]."'";
                    $validar = 1;
                }
  

                if(isset($_POST["G2961_C59160"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2961_C59160 = '".$_POST["G2961_C59160"]."'";
                    $LsqlI .= $separador."G2961_C59160";
                    $LsqlV .= $separador."'".$_POST["G2961_C59160"]."'";
                    $validar = 1;
                }
                                                                               
 
                if(isset($_POST["G2961_C59161"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2961_C59161 = '".$_POST["G2961_C59161"]."'";
                    $LsqlI .= $separador."G2961_C59161";
                    $LsqlV .= $separador."'".$_POST["G2961_C59161"]."'";
                    $validar = 1;
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2961_C59155 = $numero;
                    $LsqlU .= ", G2961_C59155 = ".$G2961_C59155."";
                    $LsqlI .= ", G2961_C59155";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2961_Usuario ,  G2961_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2961_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2961 WHERE  G2961_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
