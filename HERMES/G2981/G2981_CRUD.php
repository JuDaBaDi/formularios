<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (isset($_GET["adjunto"])) {
            $archivo=$_GET["adjunto"];
            $extensiones = explode(".", $archivo);
            // $ext = ["pdf","jpeg","jpg","png"];
            // if (in_array(end($extensiones),$ext)) {
                if (is_file("/mnt/disks/grabaciones/adjuntos/G2981/G2981/".$archivo)) {
                    $size = strlen("/mnt/disks/grabaciones/adjuntos/G2981/G2981/".$archivo);

                    if ($size>0) {
                        $nombre=basename("/mnt/disks/grabaciones/adjuntos/G2981/G2981/".$archivo);
                        $tamaÃ±o = filesize("/mnt/disks/grabaciones/adjuntos/G2981/G2981/".$archivo);
                        header("Content-Description: File Transfer");
                        header("Content-type: application/force-download");
                        header("Content-disposition: attachment; filename=".$nombre);
                        header("Content-Transfer-Encoding: binary");
                        header("Expires: 0");
                        header("Cache-Control: must-revalidate");
                        header("Pragma: public");
                        header("Content-Length: " . $tamaÃ±o);
                        ob_clean();
                        flush();
                        readfile("/mnt/disks/grabaciones/adjuntos/G2981/G2981/".$archivo);
                    }            
                }else{ 
                    // header("Location:" . getenv("HTTP_REFERER"));
                    echo "<h1>EL ARCHIVO NO EXISTE O LO HAN BORRADO...<br> !!VUELVE ATRAS</h1>";
                }            
            // }else{
                // header("Location:" . getenv("HTTP_REFERER"));
                // echo "Este archivo no se puede descargar.";
            // }

    }
        
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2981_ConsInte__b, G2981_FechaInsercion , G2981_Usuario ,  G2981_CodigoMiembro  , G2981_PoblacionOrigen , G2981_EstadoDiligenciamiento ,  G2981_IdLlamada , G2981_C59373 as principal ,G2981_C59372,G2981_C59373,G2981_C59374,G2981_C59375,G2981_C59376,G2981_C59377,G2981_C59378,G2981_C59379,G2981_C59380,G2981_C59369,G2981_C59370,G2981_C59371 FROM '.$BaseDatos.'.G2981 WHERE G2981_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2981_C59372'] = $key->G2981_C59372;

                $datos[$i]['G2981_C59373'] = $key->G2981_C59373;

                $datos[$i]['G2981_C59374'] = $key->G2981_C59374;

                $datos[$i]['G2981_C59375'] = $key->G2981_C59375;

                $datos[$i]['G2981_C59376'] = $key->G2981_C59376;

                $datos[$i]['G2981_C59377'] = $key->G2981_C59377;

                $datos[$i]['G2981_C59378'] = $key->G2981_C59378;

                $datos[$i]['G2981_C59379'] = $key->G2981_C59379;

                $datos[$i]['G2981_C59380'] = $key->G2981_C59380;

                $datos[$i]['G2981_C59369'] = $key->G2981_C59369;

                $datos[$i]['G2981_C59370'] = $key->G2981_C59370;

                $datos[$i]['G2981_C59371'] = $key->G2981_C59371;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2981";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2981_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2981_ConsInte__b as id,  G2981_C59373 as camp1 , G2981_C59374 as camp2 
                     FROM ".$BaseDatos.".G2981  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2981_ConsInte__b as id,  G2981_C59373 as camp1 , G2981_C59374 as camp2  
                    FROM ".$BaseDatos.".G2981  JOIN ".$BaseDatos.".G2981_M".$_POST['muestra']." ON G2981_ConsInte__b = G2981_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2981_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2981_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2981_C59373 LIKE '%".$B."%' OR G2981_C59374 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2981_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2981");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2981_ConsInte__b, G2981_FechaInsercion , G2981_Usuario ,  G2981_CodigoMiembro  , G2981_PoblacionOrigen , G2981_EstadoDiligenciamiento ,  G2981_IdLlamada , G2981_C59373 as principal , a.LISOPC_Nombre____b as G2981_C59372,G2981_C59373,G2981_C59374,G2981_C59375,G2981_C59376,G2981_C59377,G2981_C59378,G2981_C59379,G2981_C59380,G2981_C59369,G2981_C59370, b.LISOPC_Nombre____b as G2981_C59371 FROM '.$BaseDatos.'.G2981 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2981_C59372 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2981_C59371';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                
                $respuesta['rows'][$i]['id']=$fila->G2981_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2981_ConsInte__b , ($fila->G2981_C59372) , ($fila->G2981_C59373) , ($fila->G2981_C59374) , ($fila->G2981_C59375) , ($fila->G2981_C59376) , ($fila->G2981_C59377) , ($fila->G2981_C59378) , ($fila->G2981_C59379) , ($fila->G2981_C59380) , ($fila->G2981_C59369) , ($fila->G2981_C59370) , ($fila->G2981_C59371) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2981 WHERE G2981_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2981";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2981_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2981_ConsInte__b as id,  G2981_C59373 as camp1 , G2981_C59374 as camp2  FROM '.$BaseDatos.'.G2981 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2981_ConsInte__b as id,  G2981_C59373 as camp1 , G2981_C59374 as camp2  
                    FROM ".$BaseDatos.".G2981  JOIN ".$BaseDatos.".G2981_M".$_POST['muestra']." ON G2981_ConsInte__b = G2981_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2981_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2981_C59373 LIKE "%'.$B.'%" OR G2981_C59374 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2981_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2981 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2981(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G2981_C59372"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2981_C59372 = '".$_POST["G2981_C59372"]."'";
                $LsqlI .= $separador."G2981_C59372";
                $LsqlV .= $separador."'".$_POST["G2981_C59372"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2981_C59373"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2981_C59373 = '".$_POST["G2981_C59373"]."'";
                $LsqlI .= $separador."G2981_C59373";
                $LsqlV .= $separador."'".$_POST["G2981_C59373"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2981_C59374"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2981_C59374 = '".$_POST["G2981_C59374"]."'";
                $LsqlI .= $separador."G2981_C59374";
                $LsqlV .= $separador."'".$_POST["G2981_C59374"]."'";
                $validar = 1;
            }
             
  
            $G2981_C59375 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2981_C59375"])){
                if($_POST["G2981_C59375"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2981_C59375 = $_POST["G2981_C59375"];
                    $LsqlU .= $separador." G2981_C59375 = ".$G2981_C59375."";
                    $LsqlI .= $separador." G2981_C59375";
                    $LsqlV .= $separador.$G2981_C59375;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2981_C59376"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2981_C59376 = '".$_POST["G2981_C59376"]."'";
                $LsqlI .= $separador."G2981_C59376";
                $LsqlV .= $separador."'".$_POST["G2981_C59376"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2981_C59377"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2981_C59377 = '".$_POST["G2981_C59377"]."'";
                $LsqlI .= $separador."G2981_C59377";
                $LsqlV .= $separador."'".$_POST["G2981_C59377"]."'";
                $validar = 1;
            }
             
  
            if (isset($_FILES["FG2981_C59378"]["tmp_name"])) {
                $destinoFile = "/mnt/disks/grabaciones/adjuntos/G2981/G2981/";
                $fechUp = date("Y-m-d_H:i:s");
                if (!file_exists("/mnt/disks/grabaciones/adjuntos/G2981/G2981")){
                    mkdir("/mnt/disks/grabaciones/adjuntos/G2981/G2981", 0777);
                }
                if ($_FILES["FG2981_C59378"]["size"] != 0) {
                    $G2981_C59378 = $_FILES["FG2981_C59378"]["tmp_name"];
                    $nG2981_C59378 = $fechUp."_".$_FILES["FG2981_C59378"]["name"];
                    $rutaFinal = $destinoFile.$nG2981_C59378;
                    if (is_uploaded_file($G2981_C59378)) {
                        move_uploaded_file($G2981_C59378, $rutaFinal);
                    }

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2981_C59378 = '".$nG2981_C59378."'";
                    $LsqlI .= $separador."G2981_C59378";
                    $LsqlV .= $separador."'".$nG2981_C59378."'";
                    $validar = 1;
                }
            }
            
  
            if (isset($_FILES["FG2981_C59379"]["tmp_name"])) {
                $destinoFile = "/mnt/disks/grabaciones/adjuntos/G2981/G2981/";
                $fechUp = date("Y-m-d_H:i:s");
                if (!file_exists("/mnt/disks/grabaciones/adjuntos/G2981/G2981")){
                    mkdir("/mnt/disks/grabaciones/adjuntos/G2981/G2981", 0777);
                }
                if ($_FILES["FG2981_C59379"]["size"] != 0) {
                    $G2981_C59379 = $_FILES["FG2981_C59379"]["tmp_name"];
                    $nG2981_C59379 = $fechUp."_".$_FILES["FG2981_C59379"]["name"];
                    $rutaFinal = $destinoFile.$nG2981_C59379;
                    if (is_uploaded_file($G2981_C59379)) {
                        move_uploaded_file($G2981_C59379, $rutaFinal);
                    }

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2981_C59379 = '".$nG2981_C59379."'";
                    $LsqlI .= $separador."G2981_C59379";
                    $LsqlV .= $separador."'".$nG2981_C59379."'";
                    $validar = 1;
                }
            }
            
  
            if (isset($_FILES["FG2981_C59380"]["tmp_name"])) {
                $destinoFile = "/mnt/disks/grabaciones/adjuntos/G2981/G2981/";
                $fechUp = date("Y-m-d_H:i:s");
                if (!file_exists("/mnt/disks/grabaciones/adjuntos/G2981/G2981")){
                    mkdir("/mnt/disks/grabaciones/adjuntos/G2981/G2981", 0777);
                }
                if ($_FILES["FG2981_C59380"]["size"] != 0) {
                    $G2981_C59380 = $_FILES["FG2981_C59380"]["tmp_name"];
                    $nG2981_C59380 = $fechUp."_".$_FILES["FG2981_C59380"]["name"];
                    $rutaFinal = $destinoFile.$nG2981_C59380;
                    if (is_uploaded_file($G2981_C59380)) {
                        move_uploaded_file($G2981_C59380, $rutaFinal);
                    }

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2981_C59380 = '".$nG2981_C59380."'";
                    $LsqlI .= $separador."G2981_C59380";
                    $LsqlV .= $separador."'".$nG2981_C59380."'";
                    $validar = 1;
                }
            }
            
  

            if(isset($_POST["G2981_C59369"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2981_C59369 = '".$_POST["G2981_C59369"]."'";
                $LsqlI .= $separador."G2981_C59369";
                $LsqlV .= $separador."'".$_POST["G2981_C59369"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2981_C59370"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2981_C59370 = '".$_POST["G2981_C59370"]."'";
                $LsqlI .= $separador."G2981_C59370";
                $LsqlV .= $separador."'".$_POST["G2981_C59370"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2981_C59371"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2981_C59371 = '".$_POST["G2981_C59371"]."'";
                $LsqlI .= $separador."G2981_C59371";
                $LsqlV .= $separador."'".$_POST["G2981_C59371"]."'";
                $validar = 1;
            }
             

            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2981_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2981_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2981_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2981_Usuario , G2981_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2981_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2981 WHERE G2981_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $dato= $mysqli->insert_id;
                        echo $mysqli->insert_id;
                        $UpdContext="UPDATE ".$BaseDatos.".G2981 SET G2981_UltiGest__b =-14, G2981_GesMasImp_b =-14, G2981_TipoReintentoUG_b =0, G2981_TipoReintentoGMI_b =0, G2981_EstadoUG_b =-14, G2981_EstadoGMI_b =-14, G2981_CantidadIntentos =0, G2981_CantidadIntentosGMI_b =0 WHERE G2981_ConsInte__b = ".$dato;
                        $UpdContext=$mysqli->query($UpdContext);
                    }else if($_POST["oper"] == 'edit'){
                        // Hago que me devuelva el id para mostrar los datos editados
                        echo $_POST["id"];
                    }else{
                        
                        echo "1";           
                    }
                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
