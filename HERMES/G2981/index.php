<?php 
    /*
        Document   : index
        Created on : 2020-12-27 21:13:34
        Author     : Jose David y su poderoso generador, La gloria sea Para DIOS 
        Url 	   : id = Mjk4MQ==  
    */
    $url_crud =  "formularios/G2981/G2981_CRUD_web.php";
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Formulario cargue de solicitudes de autorización</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Date Picker -->
        <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="assets/timepicker/jquery.timepicker.css"/>
        <!-- Theme style -->
        <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">

        <link rel="stylesheet" type="text/css" href="assets/plugins/sweetalert/sweetalert.css">

        <link rel="stylesheet" href="assets/plugins/select2/select2.min.css" />
    
        <link href='//fonts.googleapis.com/css?family=Sansita+One' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <link rel="shortcut icon" href="assets/img/logo_dyalogo_mail.png"/>
        <style>
            
            .hed{
           
                font-family: 'Sansita One', cursive; 
                color:white;
            }

            .hed2{
                text-align:center;
                font-family: 'Sansita One', cursive; 
                font-size:25px; 
                color:#019CDE;
                margin-top: -9px;
            }
            .font_2 {
                font: normal normal normal 17px/1.4em Spinnaker,sans-serif;
                text-align:center;
            }
            
            .redonder {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
                -webkit-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                -moz-box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
                box-shadow: 7px 7px 17px -9px rgba(0,0,0,0.75);
            }

            [class^='select2'] {
                border-radius: 0px !important;
            }

            form#formLogin h3{
                font-size: 14px;
            }
        </style>
    </head>
    <?php  
        if(isset($_GET['v'])){
            
        }else{
            echo '<body class="hold-transition" >';
        }
    ?>
    
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6" >
                <div class="login-box">
                    <?php if(!isset($_GET['aceptaTerminos'])){ ?>
                    <div class="login-logo hed">
                        <?php if(!isset($_GET['v'])){ ?>
                        <img src="/img_usr/1.png" style="width:100%;max-width:400px;" alt="Dyalogo">
                        <?php }else{ 
                                
                            }
                        ?>

                    </div><!-- /.login-logo -->
                    <?php if(isset($_GET['v'])){ 
                        
                    }else { ?>
                    <div class="login-box-body">
                        <p class="login-box-msg font_2" >Formulario cargue de solicitudes de autorización</p>
					<?php } ?>
                        <form action="formularios/G2981/G2981_CRUD_web.php" method="post" id="formLogin" enctype="multipart/form-data">

                    <!-- CAMPO DE TIPO LISTA -->
                    <div class="form-group">
                        <label for="G2981_C59372" id="LblG2981_C59372">Tipo de Documento*</label>
                        <select class="form-control input-sm select2"  style="width: 100%;" name="G2981_C59372" id="G2981_C59372" required>
                            <option value="">Seleccione</option>
                            <?php
                                /*
                                    SE RECORRE LA CONSULTA QUE SE CARGO CON ANTERIORIDAD EN LA SECCION DE CARGUE LISTAS DESPLEGABLES
                                */
                                $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = 3609 ORDER BY LISOPC_Nombre____b ASC";

                                $obj = $mysqli->query($Lsql);
                                while($obje = $obj->fetch_object()){
                                    echo "<option value='".$obje->OPCION_ConsInte__b."'>".($obje->OPCION_Nombre____b)."</option>";

                                }    
                                
                            ?>
                        </select>
                    </div>
                    <!-- FIN DEL CAMPO TIPO LISTA -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2981_C59373" id="LblG2981_C59373">Número de documento del afiliado*</label>
								<input type="text" class="form-control input-sm" id="G2981_C59373" value=""  name="G2981_C59373"  placeholder="Número de documento del afiliado" required>
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
							<!-- CAMPO TIPO TEXTO -->
							<div class="form-group">
								<label for="G2981_C59374" id="LblG2981_C59374">Nombres y apellidos del afiliado*</label>
								<input type="text" class="form-control input-sm" id="G2981_C59374" value=""  name="G2981_C59374"  placeholder="Nombres y apellidos del afiliado" required>
							</div>
							<!-- FIN DEL CAMPO TIPO TEXTO -->
 
                            <!-- CAMPO TIPO ENTERO -->
                            <!-- Estos campos siempre deben llevar Numerico en la clase asi class="form-control input-sm Numerico" , de l contrario seria solo un campo de texto mas -->
                            <div class="form-group">
                                <label for="G2981_C59375" id="LblG2981_C59375">Teléfono celular*</label>
                                <input type="text" class="form-control input-sm Numerico" value=""  name="G2981_C59375" id="G2981_C59375" placeholder="Teléfono celular" required>
                            </div>
                            <!-- FIN DEL CAMPO TIPO ENTERO -->
 
                            <!-- CAMPO TIPO EMAIL -->
                            <div class="form-group">
                                <label for="G2981_C59376" id="LblG2981_C59376">Correo electrónico</label>
                                <input type="email" class="form-control input-sm" id="G2981_C59376" value=""  name="G2981_C59376"  placeholder="Correo electrónico" >
                            </div>
                            <!-- FIN DEL CAMPO TIPO TEXTO -->
  
                            <!-- CAMPO TIPO MEMO -->
                            <div class="form-group">
                                <label for="G2981_C59377" id="LblG2981_C59377">Describa brevemente su solicitud*</label>
                                <textarea class="form-control input-sm" name="G2981_C59377" id="G2981_C59377"  value="" placeholder="Solicitud" required></textarea>
                            </div>
                            <!-- FIN DEL CAMPO TIPO MEMO -->
 
                            <!-- CAMPO TIPO ADJUNTO -->
                            <div class="form-group">
                                <label for="FG2981_C59378" id="LblFG2981_C59378">Anexo historia clinica*</label>
                                <input type="file" class="adjuntos" id="FG2981_C59378" name="FG2981_C59378" required>
                                <p class="help-block">
                                    Peso maximo del archivo 9 MB
                                </p>
                            </div>
 
                            <!-- CAMPO TIPO ADJUNTO -->
                            <div class="form-group">
                                <label for="FG2981_C59379" id="LblFG2981_C59379">Anexo orden medica*</label>
                                <input type="file" class="adjuntos" id="FG2981_C59379" name="FG2981_C59379" required>
                                <p class="help-block">
                                    Peso maximo del archivo 9 MB
                                </p>
                            </div>
 
                            <!-- CAMPO TIPO ADJUNTO -->
                            <div class="form-group">
                                <label for="FG2981_C59380" id="LblFG2981_C59380">Otros documentos (fallo de tutela, patología, anexo 3, nota operatoria).</label>
                                <input type="file" class="adjuntos" id="FG2981_C59380" name="FG2981_C59380">
                                <p class="help-block">
                                    Peso maximo del archivo 9 MB
                                </p>
                            </div>

                            <!-- SECCION : PAGINAS INCLUIDAS -->
                            <input type="hidden" name="pasoId" id="pasoId" value="<?php if (isset($_GET["paso"])) {
                                echo $_GET["paso"];
                            }else{ echo "0"; } ?>">
                            <input type="hidden" name="id" id="hidId" value='0'>
                            <input type="hidden" name="oper" id="oper" value='add'>
                            <input type="hidden" name="padre" id="padre" value='<?php if(isset($_GET['yourfather'])){ echo $_GET['yourfather']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formpadre" id="formpadre" value='<?php if(isset($_GET['formularioPadre'])){ echo $_GET['formularioPadre']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="formhijo" id="formhijo" value='<?php if(isset($_GET['formulario'])){ echo $_GET['formulario']; }else{ echo "0"; }?>' >
                            <input type="hidden" name="ORIGEN_DY_WF" id="ORIGEN_DY_WF" value='<?php if(isset($_GET['origen'])){ echo $_GET['origen']; }else{ echo "webForm"; }?>' >
                            
                            <?php if (isset($_GET['v'])){ ?>
                                    
                            <?php }else{ ?>
                                    <input type= "hidden" name="OPTIN_DY_WF" id="OPTIN_DY_WF" value="SIMPLE">
                            <?php }?>

                            <div class="row">
                                <div class="col-xs-2">
                                    &nbsp;
                                </div><!-- /.col -->
                                <div class="col-xs-8">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar datos</button>
                                </div><!-- /.col -->
                            </div>
                        </form>
                    </div><!-- /.login-box-body -->
                    <?php 
                        }else{ ?>
                            <?php if(isset($_GET['v'])){ 
                                
                            }else { ?>
                            <div class="login-box-body">
                                <p class="login-box-msg font_2" >FORMULARIO DE CONTACTO</p>
                            <?php } ?>
                    <?php 
                            if($_GET['aceptaTerminos'] == 'acepto'){
                                $ultimoResgistroInsertado = base64_decode($_GET['cons']);

                                $OPTIN_DY_WF = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 2981 AND PREGUN_Texto_____b = 'OPTIN_DY_WF'";
                                $res_OPTIN_DY_WF = $mysqli->query($OPTIN_DY_WF);
                                if($res_OPTIN_DY_WF->num_rows > 0){


                                    $Sel = "SELECT * FROM ".$BaseDatos.".G2981 WHERE G2981_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." IS NULL AND G871_ConsInte__b =".$ultimoResgistroInsertado;
                                    $resSel = $mysqli->query($Sel);
                                    if($resSel->num_rows > 0){

                                        $dataOPTIN_DY_WF = $res_OPTIN_DY_WF->fetch_array();
                                        $Lsql = "UPDATE ".$BaseDatos.".G2981 SET G2981_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." = 'CONFIRMADO' WHERE G2981_ConsInte__b =".$ultimoResgistroInsertado;
                                        if ($mysqli->query($Lsql) === TRUE) {
                                            /* Acepto toca meterlo en la muestra  G626_M285*/

                                            

                                            echo "<div class=\"alert alert-info\">Gracias por aceptar nuestra <a href='#'>Politica de tratamiento de datos</a>, nos pondremos en contacto pronto!</div>";

                                        }
                                    }else{
                                        echo "<div class=\"alert alert-info\">Gracias por aceptar nuestra <a href='#'>Politica de tratamiento de datos</a>, nos pondremos en contacto pronto!</div>";   
                                    }


                                }

                            }else{
                                /* NO CONFIRMAN */
                                $ultimoResgistroInsertado = base64_decode($_GET['cons']);
                                $OPTIN_DY_WF = "SELECT PREGUN_ConsInte__b FROM ".$BaseDatos_systema.".PREGUN WHERE PREGUN_ConsInte__GUION__b = 2981 AND PREGUN_Texto_____b = 'OPTIN_DY_WF'";
                                $res_OPTIN_DY_WF = $mysqli->query($OPTIN_DY_WF);
                                if($res_OPTIN_DY_WF->num_rows > 0){
                                    $dataOPTIN_DY_WF = $res_OPTIN_DY_WF->fetch_array();
                                    $Sel = "SELECT * FROM ".$BaseDatos.".G2981 WHERE G2981_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." IS NULL AND G871_ConsInte__b =".$ultimoResgistroInsertado;
                                    $resSel = $mysqli->query($Sel);
                                    if($resSel->num_rows > 0){

                                        $Lsql = "UPDATE ".$BaseDatos.".G2981 SET G2981_C".$dataOPTIN_DY_WF['PREGUN_ConsInte__b']." = 'NO CONFIRMADO' WHERE G2981_ConsInte__b =".$ultimoResgistroInsertado;

                                        if ($mysqli->query($Lsql) === TRUE) {
                                            echo "<div class=\"alert alert-info\">Hemos eliminado su información de la base de datos.</div>";
                                        }
                                    }else{
                                        echo "<div class=\"alert alert-info\">Hemos eliminado su información de la base de datos.</div>";            
                                    }

                                }
                            }

                                
                        } ?>


                </div><!-- /.login-box -->

            </div>
        </div>


        <!-- jQuery 2.2.3 -->
        <script src="assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/select2/select2.full.min.js"></script>
        <!-- datepicker -->
        <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- FastClick -->
        <script src="assets/plugins/fastclick/fastclick.js"></script>
        <script src="assets/timepicker/jquery.timepicker.js"></script>
        <script src="assets/plugins/sweetalert/sweetalert.min.js"></script>  
        <script src="assets/plugins/iCheck/icheck.min.js"></script>
        <script src="assets/js/jquery.validate.js"></script>
        <script src="assets/js/numeric.js"></script>

        <script type="text/javascript" src="formularios/G2981/G2981_eventos.js"></script>
        <script type="text/javascript">

            $("#formLogin").submit(function(event) {
                
            });
            $(function(){
                $(".adjuntos").change(function(){
                    var imax = $(this).attr("valor");
                    var imagen = this.files[0];

                    // if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png" && imagen["type"] != "application/pdf"){
                    //     swal({
                    //         title : "Error al subir el archivo",
                    //         text  : "El archivo debe estar en formato PNG , JPG, PDF",
                    //         type  : "error",
                    //         confirmButtonText : "Cerrar"
                    //     });
                    // }else 
                    if(imagen["size"] > 9000000 ) {
                        $(this).val("");
                        swal({
                            title : "Error al subir el archivo",
                            text  : "El archivo no debe pesar mas de 9 MB",
                            type  : "error",
                            confirmButtonText : "Cerrar"
                        });
                    }
                });
                $.fn.datepicker.dates['es'] = {
                    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
                    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
                    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
                    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                    today: "Today",
                    clear: "Clear",
                    format: "yyyy-mm-dd",
                    titleFormat: "yyyy-mm-dd", 
                    weekStart: 0
                };

                //str_Select2 estos son los guiones
                


                //datepickers
                

                //Timepickers
                


                //Validaciones numeros Enteros
                

            $("#G2981_C59375").numeric();
            

                //Validaciones numeros Decimales
               


               /* Si tiene dependiendias */
               


    //function para Tipo de Documento 

    $("#G2981_C59372").change(function(){  
        //Esto es la parte de las listas dependientes
        

    });


               <?php
                    if(isset($_GET['result'])){
                        if($_GET['result'] ==  1){
                ?>
                        swal({
                            title: "Exito!",
                            text: "Recibimos su solicitud, pronto estaremos en contacto",
                            type: "success",
                            confirmButtonText: "Ok"
                        },function(){
                            <?php 
                            if(isset($_GET['v'])) {
                                
                            } 
                            ?>
                        });
                        
                        
                <?php   }else{ ?>
                            swal({
                                title: "Error!",
                                text: 'Ocurrio un error, intenta mas tarde',
                                type: "error",
                                confirmButtonText: "Ok"
                            });
                <?php      
                        }
                    }
                ?>
            });
        </script>

        <?php 
        if(isset($_GET['v'])) {
            
        } 
        ?>
        
