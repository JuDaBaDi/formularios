<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    if (isset($_POST["INCTB"])) {
                            if ($mysqli->query("UPDATE ".$BaseDatos_systema.".CONTADORES SET CONTADORES_Valor_b = (CONTADORES_Valor_b+1) WHERE CONTADORES_ConsInte__PREGUN_b = 59511")){
                                $Lsql = $mysqli->query("SELECT CONTADORES_Valor_b FROM ".$BaseDatos_systema.".CONTADORES WHERE CONTADORES_ConsInte__PREGUN_b = 59511");
                                echo $Lsql->fetch_array()["CONTADORES_Valor_b"];
                            }else{echo 0;}
                        }
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2980_ConsInte__b, G2980_FechaInsercion , G2980_Usuario ,  G2980_CodigoMiembro  , G2980_PoblacionOrigen , G2980_EstadoDiligenciamiento ,  G2980_IdLlamada , G2980_C59366 as principal ,G2980_C59503,G2980_C59366,G2980_C59506,G2980_C59511,G2980_C59354,G2980_C59355,G2980_C59356,G2980_C59357,G2980_C59358,G2980_C59359,G2980_C59360,G2980_C59361,G2980_C59362 FROM '.$BaseDatos.'.G2980 WHERE G2980_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2980_C59503'] = $key->G2980_C59503;

                $datos[$i]['G2980_C59366'] = $key->G2980_C59366;

                $datos[$i]['G2980_C59506'] = $key->G2980_C59506;

                $datos[$i]['G2980_C59511'] = $key->G2980_C59511;

                $datos[$i]['G2980_C59354'] = $key->G2980_C59354;

                $datos[$i]['G2980_C59355'] = $key->G2980_C59355;

                $datos[$i]['G2980_C59356'] = explode(' ', $key->G2980_C59356)[0];
  
                $hora = '';
                if(!is_null($key->G2980_C59357)){
                    $hora = explode(' ', $key->G2980_C59357)[1];
                }

                $datos[$i]['G2980_C59357'] = $hora;

                $datos[$i]['G2980_C59358'] = $key->G2980_C59358;

                $datos[$i]['G2980_C59359'] = $key->G2980_C59359;

                $datos[$i]['G2980_C59360'] = $key->G2980_C59360;

                $datos[$i]['G2980_C59361'] = $key->G2980_C59361;

                $datos[$i]['G2980_C59362'] = $key->G2980_C59362;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2980";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2980_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2980_ConsInte__b as id,  G2980_C59366 as camp1 , G2980_C59366 as camp2 
                     FROM ".$BaseDatos.".G2980  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2980_ConsInte__b as id,  G2980_C59366 as camp1 , G2980_C59366 as camp2  
                    FROM ".$BaseDatos.".G2980  JOIN ".$BaseDatos.".G2980_M".$_POST['muestra']." ON G2980_ConsInte__b = G2980_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2980_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2980_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2980_C59366 LIKE '%".$B."%' OR G2980_C59366 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2980_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2980");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2980_ConsInte__b, G2980_FechaInsercion , G2980_Usuario ,  G2980_CodigoMiembro  , G2980_PoblacionOrigen , G2980_EstadoDiligenciamiento ,  G2980_IdLlamada , G2980_C59366 as principal ,G2980_C59503,G2980_C59366,G2980_C59506,G2980_C59511, a.LISOPC_Nombre____b as G2980_C59354, b.LISOPC_Nombre____b as G2980_C59355,G2980_C59356,G2980_C59357,G2980_C59358,G2980_C59359,G2980_C59360,G2980_C59361,G2980_C59362 FROM '.$BaseDatos.'.G2980 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2980_C59354 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2980_C59355';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2980_C59357)){
                    $hora_a = explode(' ', $fila->G2980_C59357)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2980_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2980_ConsInte__b , ($fila->G2980_C59503) , ($fila->G2980_C59366) , ($fila->G2980_C59506) , ($fila->G2980_C59511) , ($fila->G2980_C59354) , ($fila->G2980_C59355) , explode(' ', $fila->G2980_C59356)[0] , $hora_a , ($fila->G2980_C59358) , ($fila->G2980_C59359) , ($fila->G2980_C59360) , ($fila->G2980_C59361) , ($fila->G2980_C59362) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2980 WHERE G2980_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    //echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2980";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2980_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2980_ConsInte__b as id,  G2980_C59366 as camp1 , G2980_C59366 as camp2  FROM '.$BaseDatos.'.G2980 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2980_ConsInte__b as id,  G2980_C59366 as camp1 , G2980_C59366 as camp2  
                    FROM ".$BaseDatos.".G2980  JOIN ".$BaseDatos.".G2980_M".$_POST['muestra']." ON G2980_ConsInte__b = G2980_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2980_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2980_C59366 LIKE "%'.$B.'%" OR G2980_C59366 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2980_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2980 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2980(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G2980_C59503"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2980_C59503 = '".$_POST["G2980_C59503"]."'";
                $LsqlI .= $separador."G2980_C59503";
                $LsqlV .= $separador."'".$_POST["G2980_C59503"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2980_C59366"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2980_C59366 = '".$_POST["G2980_C59366"]."'";
                $LsqlI .= $separador."G2980_C59366";
                $LsqlV .= $separador."'".$_POST["G2980_C59366"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2980_C59506"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2980_C59506 = '".$_POST["G2980_C59506"]."'";
                $LsqlI .= $separador."G2980_C59506";
                $LsqlV .= $separador."'".$_POST["G2980_C59506"]."'";
                $validar = 1;
            }
             
  
            $G2980_C59511 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2980_C59511"])){
                if($_POST["G2980_C59511"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2980_C59511 = $_POST["G2980_C59511"];
                    $LsqlU .= $separador." G2980_C59511 = ".$G2980_C59511."";
                    $LsqlI .= $separador." G2980_C59511";
                    $LsqlV .= $separador.$G2980_C59511;
                    $validar = 1;
                }
            }
 
            $G2980_C59354 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2980_C59354 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2980_C59354 = ".$G2980_C59354;
                    $LsqlI .= $separador." G2980_C59354";
                    $LsqlV .= $separador.$G2980_C59354;
                    $validar = 1;

                    
                }
            }
 
            $G2980_C59355 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2980_C59355 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2980_C59355 = ".$G2980_C59355;
                    $LsqlI .= $separador." G2980_C59355";
                    $LsqlV .= $separador.$G2980_C59355;
                    $validar = 1;
                }
            }
 
            $G2980_C59356 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2980_C59356 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2980_C59356 = ".$G2980_C59356;
                    $LsqlI .= $separador." G2980_C59356";
                    $LsqlV .= $separador.$G2980_C59356;
                    $validar = 1;
                }
            }
 
            $G2980_C59357 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2980_C59357 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2980_C59357 = ".$G2980_C59357;
                    $LsqlI .= $separador." G2980_C59357";
                    $LsqlV .= $separador.$G2980_C59357;
                    $validar = 1;
                }
            }
 
            $G2980_C59358 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2980_C59358 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2980_C59358 = ".$G2980_C59358;
                    $LsqlI .= $separador." G2980_C59358";
                    $LsqlV .= $separador.$G2980_C59358;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2980_C59359"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2980_C59359 = '".$_POST["G2980_C59359"]."'";
                $LsqlI .= $separador."G2980_C59359";
                $LsqlV .= $separador."'".$_POST["G2980_C59359"]."'";
                $validar = 1;
            }
             
  
            //JDBD - Fecha oculta Script
            if(isset($_POST["G2980_C59360"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $strFecha_t = date("Y-m-d H:i:s");

                $LsqlU .= $separador."G2980_C59360 = '".$strFecha_t."'";
                $LsqlI .= $separador."G2980_C59360";
                $LsqlV .= $separador."'".$strFecha_t."'";
                $validar = 1;
            }
             
  
            //JDBD - Hora oculta Script
            if(isset($_POST["G2980_C59361"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $strHora_t = date("H:i:s");

                $LsqlU .= $separador."G2980_C59361 = '".$strHora_t."'";
                $LsqlI .= $separador."G2980_C59361";
                $LsqlV .= $separador."'".$strHora_t."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2980_C59362"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2980_C59362 = '".$_POST["G2980_C59362"]."'";
                $LsqlI .= $separador."G2980_C59362";
                $LsqlV .= $separador."'".$_POST["G2980_C59362"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2980_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2980_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2980_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2980_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2980_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2980_Usuario , G2980_FechaInsercion, G2980_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2980_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2980 WHERE G2980_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"]; echo $UltimoID;
                        }
                        //echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

    if(isset($_GET["callDatosSubgrilla_0"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G2980_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G2980 WHERE G2980_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G2988_ConsInte__b, G2988_C59496, G2988_C59498, G2988_C59500, G2988_C59499, e.LISOPC_Nombre____b as  G2988_C59501, G2988_C59502, G2988_C59504 FROM ".$BaseDatos.".G2988  LEFT JOIN ".$BaseDatos_systema.".LISOPC as e ON e.LISOPC_ConsInte__b =  G2988_C59501 ";

        $SQL .= " WHERE G2988_C59504 = '".$numero."'"; 

        $SQL .= " ORDER BY G2988_C59496";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2988_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2988_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G2988_C59496)."</cell>";

                echo "<cell>". ($fila->G2988_C59498)."</cell>";

                echo "<cell><![CDATA[". ($fila->G2988_C59500)."]]></cell>";

                echo "<cell>". ($fila->G2988_C59499)."</cell>";

                echo "<cell>". ($fila->G2988_C59501)."</cell>";

                if($fila->G2988_C59502 != ''){
                    echo "<cell>". explode(' ', $fila->G2988_C59502)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G2988_C59504)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

    if(isset($_GET["callDatosSubgrilla_1"])){

        $numero = $_GET['id'];
        if(isset($_GET['idBd'])){
            $sqlMiembro=$mysqli->query("SELECT G2980_CodigoMiembro AS miembro FROM DYALOGOCRM_WEB.G2980 WHERE G2980_ConsInte__b={$numero}");
            if($sqlMiembro && $sqlMiembro-> num_rows ==1){
                $sqlMiembro=$sqlMiembro->fetch_object();
                $numero=$sqlMiembro->miembro;            
            }
        }

        $SQL = "SELECT G2988_ConsInte__b, G2988_C59496, G2988_C59498, G2988_C59500, G2988_C59499, e.LISOPC_Nombre____b as  G2988_C59501, G2988_C59502, G2988_C59504 FROM ".$BaseDatos.".G2988  LEFT JOIN ".$BaseDatos_systema.".LISOPC as e ON e.LISOPC_ConsInte__b =  G2988_C59501 ";

        $SQL .= " WHERE G2988_C59498 = '".$numero."'"; 

        $SQL .= " ORDER BY G2988_C59496";

        // echo $SQL;
        if ( stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml") ) { 
            header("Content-type: application/xhtml+xml;charset=utf-8"); 
        } else { 
            header("Content-type: text/xml;charset=utf-8"); 
        } 

        $et = ">"; 
        echo "<?xml version='1.0' encoding='utf-8'?$et\n"; 
        echo "<rows>"; // be sure to put text data in CDATA
        $result = $mysqli->query($SQL);
        while( $fila = $result->fetch_object() ) {
            echo "<row asin='".$fila->G2988_ConsInte__b."'>"; 
            echo "<cell>". ($fila->G2988_ConsInte__b)."</cell>"; 
            

                echo "<cell>". ($fila->G2988_C59496)."</cell>";

                echo "<cell>". ($fila->G2988_C59498)."</cell>";

                echo "<cell><![CDATA[". ($fila->G2988_C59500)."]]></cell>";

                echo "<cell>". ($fila->G2988_C59499)."</cell>";

                echo "<cell>". ($fila->G2988_C59501)."</cell>";

                if($fila->G2988_C59502 != ''){
                    echo "<cell>". explode(' ', $fila->G2988_C59502)[0]."</cell>";
                }else{
                    echo "<cell></cell>";
                }

                echo "<cell>". ($fila->G2988_C59504)."</cell>";
            echo "</row>"; 
        } 
        echo "</rows>"; 
    }

  
    if(isset($_GET["insertarDatosSubgrilla_0"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2988 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2988(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G2988_C59496"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2988_C59496 = '".$_POST["G2988_C59496"]."'";
                    $LsqlI .= $separador."G2988_C59496";
                    $LsqlV .= $separador."'".$_POST["G2988_C59496"]."'";
                    $validar = 1;
                }

                                                                               
 

                if(isset($_POST["G2988_C59498"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2988_C59498 = '".$_POST["G2988_C59498"]."'";
                    $LsqlI .= $separador."G2988_C59498";
                    $LsqlV .= $separador."'".$_POST["G2988_C59498"]."'";
                    $validar = 1;
                }

                                                                               
  

                if(isset($_POST["G2988_C59500"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2988_C59500 = '".$_POST["G2988_C59500"]."'";
                    $LsqlI .= $separador."G2988_C59500";
                    $LsqlV .= $separador."'".$_POST["G2988_C59500"]."'";
                    $validar = 1;
                }
                                                                               
 

                if(isset($_POST["G2988_C59499"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2988_C59499 = '".$_POST["G2988_C59499"]."'";
                    $LsqlI .= $separador."G2988_C59499";
                    $LsqlV .= $separador."'".$_POST["G2988_C59499"]."'";
                    $validar = 1;
                }

                                                                               
 
                if(isset($_POST["G2988_C59501"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2988_C59501 = '".$_POST["G2988_C59501"]."'";
                    $LsqlI .= $separador."G2988_C59501";
                    $LsqlV .= $separador."'".$_POST["G2988_C59501"]."'";
                    $validar = 1;
                }

                $G2988_C59502 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2988_C59502"])){    
                    if($_POST["G2988_C59502"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2988_C59502 = "'".str_replace(' ', '',$_POST["G2988_C59502"])." 00:00:00'";
                        $LsqlU .= $separador." G2988_C59502 = ".$G2988_C59502;
                        $LsqlI .= $separador." G2988_C59502";
                        $LsqlV .= $separador.$G2988_C59502;
                        $validar = 1;
                    }
                }

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2988_C59504 = $numero;
                    $LsqlU .= ", G2988_C59504 = ".$G2988_C59504."";
                    $LsqlI .= ", G2988_C59504";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2988_Usuario ,  G2988_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2988_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2988 WHERE  G2988_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
    if(isset($_GET["insertarDatosSubgrilla_1"])){
        
        if(isset($_POST["oper"])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2988 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2988(";
            $LsqlV = " VALUES ("; 
 

                if(isset($_POST["G2988_C59496"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2988_C59496 = '".$_POST["G2988_C59496"]."'";
                    $LsqlI .= $separador."G2988_C59496";
                    $LsqlV .= $separador."'".$_POST["G2988_C59496"]."'";
                    $validar = 1;
                }

                                                                               
  

                if(isset($_POST["G2988_C59500"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2988_C59500 = '".$_POST["G2988_C59500"]."'";
                    $LsqlI .= $separador."G2988_C59500";
                    $LsqlV .= $separador."'".$_POST["G2988_C59500"]."'";
                    $validar = 1;
                }
                                                                               
 

                if(isset($_POST["G2988_C59499"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2988_C59499 = '".$_POST["G2988_C59499"]."'";
                    $LsqlI .= $separador."G2988_C59499";
                    $LsqlV .= $separador."'".$_POST["G2988_C59499"]."'";
                    $validar = 1;
                }

                                                                               
 
                if(isset($_POST["G2988_C59501"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2988_C59501 = '".$_POST["G2988_C59501"]."'";
                    $LsqlI .= $separador."G2988_C59501";
                    $LsqlV .= $separador."'".$_POST["G2988_C59501"]."'";
                    $validar = 1;
                }

                $G2988_C59502 = NULL;
                //este es de tipo date hay que preguntar si esta vacia o no
                if(isset($_POST["G2988_C59502"])){    
                    if($_POST["G2988_C59502"] != ''){
                        $separador = "";
                        if($validar == 1){
                            $separador = ",";
                        }

                        $G2988_C59502 = "'".str_replace(' ', '',$_POST["G2988_C59502"])." 00:00:00'";
                        $LsqlU .= $separador." G2988_C59502 = ".$G2988_C59502;
                        $LsqlI .= $separador." G2988_C59502";
                        $LsqlV .= $separador.$G2988_C59502;
                        $validar = 1;
                    }
                }
 

                if(isset($_POST["G2988_C59504"])){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2988_C59504 = '".$_POST["G2988_C59504"]."'";
                    $LsqlI .= $separador."G2988_C59504";
                    $LsqlV .= $separador."'".$_POST["G2988_C59504"]."'";
                    $validar = 1;
                }

                                                                               

            if(isset($_POST["Padre"])){
                if($_POST["Padre"] != ''){
                    //esto es porque el padre es el entero
                    $numero = $_POST["Padre"];

                    $G2988_C59498 = $numero;
                    $LsqlU .= ", G2988_C59498 = ".$G2988_C59498."";
                    $LsqlI .= ", G2988_C59498";
                    $LsqlV .= ",".$_POST["Padre"];
                }
            }  



            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ",  G2988_Usuario ,  G2988_FechaInsercion";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."'";
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2988_ConsInte__b =".$_POST["providerUserId"]; 
                }else if($_POST['oper'] == 'del'){
                    $Lsql = "DELETE FROM  ".$BaseDatos.".G2988 WHERE  G2988_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }

            if($validar == 1){
                // echo $Lsql;
                if ($mysqli->query($Lsql) === TRUE) {
                    echo $mysqli->insert_id;
                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                    echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }  
            }  
        }
    }
?>
