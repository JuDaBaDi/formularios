<?php session_start(); ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="tipo_contenido" content="text/html;" http-equiv="content-type" charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dyalogo CRM</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../assets/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../../assets/ionicons-master/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../assets/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../assets/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../../assets/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="../../assets/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="../../assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="../../assets/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../../assets/plugins/daterangepicker/daterangepicker.css">
    <!-- Bootstrap time Picker -->
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="../../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="../../assets/css/alertify.core.css">

    <link rel="stylesheet" href="../../assets/css/alertify.default.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../../assets/Guriddo_jqGrid_/css/ui.jqgrid-bootstrap.css" />
    <link rel="stylesheet" href="../../assets/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="../../assets/plugins/sweetalert/sweetalert.css" />
    <script src="../../assets/plugins/sweetalert/sweetalert.min.js"></script>

    <script src="../../assets/plugins/jQuery/jquery-2.2.3.min.js"></script>

    <link rel="stylesheet" href="../../assets/plugins/datatables/dataTables.bootstrap.css">
    <script src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../../assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../assets/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- FastClick -->
    <script src="../../assets/plugins/fastclick/fastclick.js"></script>
    <link rel="stylesheet" href="../../assets/plugins/WinPicker/dist/wickedpicker.min.css">
    <script type="text/javascript" src="../../assets/plugins/WinPicker/dist/wickedpicker.min.js"></script>
    <script src="https://malsup.github.io/jquery.blockUI.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.5/jquery.inputmask.min.js"></script>
    <script src="../../assets/js/validator.js"></script>
    <script src="../../assets/js/alertify.js"></script>

    <script type="text/javascript" src="../../assets/plugins/select2/select2.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../../assets/plugins/select2/select2.min.css" />
</head>
<section class='content' style="padding:0% 5%;margin-top:30px;">
    <div class='box box-primary'>
        <div class='box-body' style="padding:2% 0%">
            <div class="row" style="width:90%;margin:auto">
                <form action="webform.php" method="post" id="formBusqueda">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">CAMPAÑA</label>
                            <select name="campan" id="campan" class="form-control input-sm" required opt="<?php if(isset($_POST['campan'])){ echo $_POST['campan'];} ?>">
                                <option value="">Seleccione</option>
                                <option value="60">WEBFORM</option>
                                <option value="14">SERVICIOS</option>
                                <option value="13">AFILIACIONES</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">CEDULA</label>
                            <input type="text" class="form-control input-sm" name="cedula" id="cedula" value="<?php if(isset($_POST['cedula'])){ echo $_POST['cedula'];} ?>">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">ASUNTO</label>
                            <input type="text" class="form-control input-sm" name="asunto" id="asunto" value="<?php if(isset($_POST['asunto'])){ echo $_POST['asunto'];} ?>">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">DE</label>
                            <input type="text" class="form-control input-sm" name="de" id="de" value="<?php if(isset($_POST['de'])){ echo $_POST['de'];} ?>">
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label for=""> </label>
                            <input type="submit" class="btn btn-primary btn-block" value="BUSCAR">
                        </div>
                    </div>
                </form>
            </div>
            <div class="row" id="mails" style="width:90%;margin:auto">

                <?php
    if(isset($_POST) && isset($_POST['campan']) && $_POST['campan']>0){
        include("../../conexion.php");
        $campana=$_POST['campan'];
        if($campana=='60' && isset($_POST['cedula'])){
            $sql=$mysqli->query("select a.*,G2981_ConsInte__b,G2981_C59372,G2981_C59373,G2981_C59374,G2981_C59375,G2981_C59376,G2981_C59377,G2981_C59378,G2981_C59379,G2981_C59380,USUARI_Nombre____b from dyalogo_canales_electronicos.dy_ce_entrantes a join DYALOGOCRM_WEB.G2981 ON de=G2981_C59376 join DYALOGOCRM_SISTEMA.USUARI ON G2981_UsuarioUG_b=USUARI_ConsInte__b where G2981_C59373={$_POST['cedula']} order by fecha_hora_recibido_servidor desc");
            if($sql && $sql->num_rows>0){
                $html='';
                $fila=0;
                while($mail =$sql->fetch_object()){
                    $html.="<form class='form_fila' id='formulario_{$fila}' action='web_crud.php?fila={$fila}' method='post' enctype='multipart/form-data'><div class=\"panel box box-primary box-solid\" style=\"margin-bottom:10px;\">
                    <div class=\"box-header with-border\" style=\"height:30px\">
                    <h3 class=\"box-title\" style=\"width:100%\">
                    <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#fila_{$fila}\" style=\"display:flex;justify-content:space-between;font-size:14px\">
                    <span>Fecha: {$mail->fecha_hora_recibido_servidor}</span>
                    <span>Asunto: {$mail->asunto}</span>
                    </a>
                    </h3>
                    </div>
                    <div id=\"fila_{$fila}\" class=\"panel-collapse collapse\">
                    <div class=\"box-body\">
                    <div class=\"row\">
                    <div class=\"col-md-6 col-xs-6\">
                    <div class=\"form-group\"><label for='{$fila}_G2981_C59372' id='{$fila}_LblG2981_C59372'>Tipo de Documento</label><select opt=\"{$mail->G2981_C59372}\" class='form-control input-sm' style='width: 100%;' name='{$fila}_G2981_C59372' id='{$fila}_G2981_C59372' tabindex='-1' aria-hidden='true' disabled='disabled'><option value='0' ";
                    if($mail->G2981_C59372=='0'){ $html.="selected='true'";}
                    $html.=">Seleccione</option><option value='44613'";
                    if($mail->G2981_C59372=='44613'){$html.="selected='true'";}
                    $html.=">Cédula de ciudadanía</option><option value='44614' ";
                    if($mail->G2981_C59372=='44614'){$html.="selected='true'";}
                    $html.=">Cédula de extranjería</option><option value='44615' ";
                    if($mail->G2981_C59372=='44615'){$html.="selected='true'";}
                    $html.=">Pasaporte</option><option value='44616' ";
                    if($mail->G2981_C59372=='44616'){$html.="selected='true'";}
                    $html.=">Registro Civil</option><option value='44617' ";
                    if($mail->G2981_C59372=='44617'){$html.="selected='true'";}
                    $html.=">Tarjeta de identidad</option></select></div></div><div class=\"col-md-6 col-xs-6\"><div class=\"form-group\"><label for=\"{$fila}_G2981_C59373\" id=\"{$fila}_LblG2981_C59373\">Número de documento del afiliado</label><input type=\"text\" class=\"form-control input-sm\" id=\"{$fila}_G2981_C59373\" value=\"{$mail->G2981_C59373}\" name=\"{$fila}_G2981_C59373\" placeholder=\"Número de documento del afiliado\" readonly></div></div></div><div class=\"row\"><div class=\"col-md-6 col-xs-6\"><div class=\"form-group\"><label for=\"{$fila}_G2981_C59374\" id=\"{$fila}_LblG2981_C59374\">Nombres y apellidos del afiliado</label><input type=\"text\" class=\"form-control input-sm\" id=\"{$fila}_G2981_C59374\" value=\"{$mail->G2981_C59374}\" name=\"{$fila}_G2981_C59374\"  placeholder=\"Nombres y apellidos del afiliado\" readonly></div></div><div class=\"col-md-6 col-xs-6\"><div class=\"form-group\"><label for=\"{$fila}_G2981_C59375\" id=\"{$fila}_LblG2981_C59375\">Teléfono celular</label><input type=\"text\" class=\"form-control input-sm Numerico\" value=\"{$mail->G2981_C59375}\" name=\"{$fila}_G2981_C59375\" id=\"{$fila}_G2981_C59375\" placeholder=\"Teléfono celular\" readonly></div></div></div><div class=\"row\"><div class=\"col-md-6 col-xs-6\"><div class=\"form-group\"><label for=\"{$fila}_G2981_C59376\" id=\"{$fila}_LblG2981_C59376\">Correo electrónico</label><input type=\"email\" class=\"form-control input-sm\" id=\"G2981_C59376\" value=\"{$mail->G2981_C59376}\" name=\"{$fila}_G2981_C59376\" placeholder=\"Correo electrónico\" readonly></div></div><div class=\"col-md-6 col-xs-6\"><div class=\"form-group\"><label for=\"{$fila}_G2981_C59377\" id=\"{$fila}_LblG2981_C59377\">Solicitud</label><textarea class=\"form-control input-sm\" name=\"{$fila}_G2981_C59377\" id=\"{$fila}_G2981_C59377\" placeholder=\"Solicitud\" readonly>{$mail->G2981_C59377}</textarea></div></div></div><div class=\"row\"><div class=\"col-md-6 col-xs-6\"><div class=\"form-group\"><label for=\"{$fila}_FG2981_C59378\" id=\"{$fila}_LblFG2981_C59378\">Anexo historia clinica</label><input name=\"{$fila}_G2981_C59378\" value=\"{$mail->G2981_C59378}\" class=\"btn btn-primary btn-sm form-control input-sm\" id=\"{$fila}_G2981_C59378\" onclick=\"location.href='web_crud.php?adjunto='+this.value\"></div></div><div class=\"col-md-6 col-xs-6\"><div class=\"form-group\"><label for=\"{$fila}_FG2981_C59379\" id=\"{$fila}_LblFG2981_C59379\">Anexo orden medica</label><input name=\"{$fila}_G2981_C59379\" value=\"{$mail->G2981_C59379}\" class=\"btn btn-primary btn-sm form-control input-sm\" id=\"{$fila}_G2981_C59379\" onclick=\"location.href='web_crud.php?adjunto='+this.value\"></div></div></div><div class=\"row\"><div class=\"col-md-6 col-xs-6\"><div class=\"form-group\"><label for=\"{$fila}_FG2981_C59380\" id=\"{$fila}_LblFG2981_C59380\">Otros documentos (fallo de tutela, patología, anexo 3, nota operatoria).</label><input name=\"{$fila}_G2981_C59380\" value=\"{$mail->G2981_C59380}\" class=\"btn btn-primary btn-sm form-control input-sm\" id=\"{$fila}_G2981_C59380\" onclick=\"location.href='web_crud.php?adjunto='+this.value\"</div></div></div></div>";
                    if($mail->leido=='0' && $mail->estado_gestion=='1' && $mail->es_respuesta=='0'){
                        $html.="<div class='row'><div class='col-md-3 col-xs-3'><div class='form-group'><select required class=\"form-control input-sm\" name=\"{$fila}_agente\" id=\"{$fila}_agente\" fila='{$fila}' ><option value=\"\">Agente</option><option value=\"3684\">Experiencia del usuario</option><option value=\"3685\">Escucha del usuario</option><option value=\"3686\">Relacionamiento con la red</option></select></div></div><div class='col-md-3 col-xs-3'><div class='form-group'><input type='text' class='form-control input-sm' name='{$fila}_cedula' id='{$fila}_cedula' placeholder='Cedula de quien gestiona' required></div></div><div class='col-md-3 col-xs-3'><div class='form-group'><select required class=\"form-control input-sm tipificacion\" name=\"{$fila}_tipificacion\" id=\"{$fila}_tipificacion\" fila='{$fila}' ><option value=\"\">Tipificación</option>";

                        $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM DYALOGOCRM_SISTEMA.LISOPC 
                                JOIN DYALOGOCRM_SISTEMA.MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                                WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 3622;";
                        $obj = $mysqli->query($Lsql);
                        while($obje = $obj->fetch_object()){
                            $html.="<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";
                        }          

                        $html.="</select></div></div><div class='col-md-3 col-xs-3'><input type='submit' class=\"btn btn-primary btn-block\" id=\"Save\" value='Cerrar Gestión'></div></div>";
                        $html.='<input type="hidden" name="'.$fila.'_selected" id="'.$fila.'_selected" value="'.$mail->G2981_C59372.'">';
                        $html.='<input type="hidden" name="Efectividad" id="Efectividad" value="0">
                        <input type="hidden" name="'.$fila.'_MonoEf" id="'.$fila.'_MonoEf" value="0">
                        <input type="hidden" name="'.$fila.'_TipNoEF" id="'.$fila.'_TipNoEF" value="0">
                        <input type="hidden" name="'.$fila.'_FechaInicio" id="'.$fila.'_FechaInicio" value="0">
                        <input type="hidden" name="'.$fila.'_FechaFinal" id="'.$fila.'_FechaFinal" value="0">
                        <input type="hidden" name="'.$fila.'_MonoEfPeso" id="'.$fila.'_MonoEfPeso" value="0">
                        <input type="hidden" name="'.$fila.'_ContactoMonoEf" id="'.$fila.'_ContactoMonoEf" value="0">
                        <input type="hidden" name="'.$fila.'_reintento" id="'.$fila.'_reintento" value="0" value="0">
                        <input type="hidden" name="'.$fila.'_correoId" id="'.$fila.'_correoId" value="'.$mail->id.'">
                        <input type="hidden" name="'.$fila.'_miembro" id="'.$fila.'_miembro" value="'.$mail->G2981_ConsInte__b.'">
                        <input type="hidden" name="'.$fila.'_cola" id="'.$fila.'_cola" value="60">';   
                    }else{
                        $html.="<span style='color:gray'>Registro gestionado por: <strong> {$mail->USUARI_Nombre____b} </strong></span>";
                    }
                    $html.="</div></div></div></form>";
                    $fila++;
                }
                echo $html;

            }else{
                $html="<h1>No se encontraron registros<h1>";
                echo $html;
            }      
        }else{
            if(isset($_POST['de']) || isset($_POST['asunto'])){
                if($_POST['de'] !='' || $_POST['asunto'] !=''){
                    $de=$_POST['de'];
                    $asunto=$_POST['asunto'];
                    $srtde='';
                    $strasunto='';
                    if($de !=''){
                        $srtde=" and a.de='{$de}'";
                    }
                    
                    if($asunto != ''){
                        $strasunto=" and a.asunto like '%{$asunto}%'";
                    }
                    
                    $sql=$mysqli->query("select a.*,b.nombre from dyalogo_canales_electronicos.dy_ce_entrantes a join dyalogo_telefonia.dy_agentes b ON id_agente_asignado=b.id where a.id_cola_distribucion={$campana} {$srtde} {$strasunto} order by a.fecha_hora_recibido_servidor desc");
                    if($sql){
                        if($sql->num_rows>0){
                            $html='';
                            $fila=0;
                            while($mail =$sql->fetch_object()){
                                $html.="<form class='form_fila' id='formulario_{$fila}' action='web_crud.php?fila={$fila}' method='post' enctype='multipart/form-data'><div class=\"panel box box-primary box-solid\" style=\"margin-bottom:10px;\"><div class=\"box-header with-border\" style=\"height:30px\"><h3 class=\"box-title\" style=\"width:100%\"><a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#fila_{$fila}\" style=\"display:flex;justify-content:space-between;font-size:14px\"><span>Fecha: {$mail->fecha_hora}</span><span>Asunto: {$mail->asunto}</span></a></h3></div><div id=\"fila_{$fila}\" class=\"panel-collapse collapse\"><div class=\"box-body\"><div class=\"row\"><div class=\"col-md-12 col-xs-12 form-group\"><div class=\"form-group\"><div style=\"display:flex;justify-content:space-between\"><div><strong>Fecha: </strong><span>{$mail->fecha_hora}</span></div></div></div><div class=\"form-group\"><div style=\"display:flex;justify-content:space-between\"><div><strong>Asunto: </strong><span>{$mail->asunto}</span></div></div></div><div class=\"form-group\"><div style=\"display:flex;justify-content:space-between\"><div><strong>De: </strong><span>{$mail->de}</span></div></div></div><div class=\"form-group\"><table class=\"table table-bordered\"><thead><tr><th>Cuerpo del mail recibido</th></tr></thead><tbody><tr><td>{$mail->cuerpo}</td></tr></tbody></table></div>";
                                $adjuntos=$mysqli->query("SELECT nombre,ruta_fisica FROM dyalogo_canales_electronicos.dy_ce_entrante_adjuntos WHERE id_ce_entrante={$mail->id}");
                                if($adjuntos && $adjuntos->num_rows > 0){
                                    $html.="<div class=\"form-group\"><table class=\"table table-bordered\"><thead><tr><th>Adjuntos</th></tr></thead><tbody>";
                                    while($adjunto = $adjuntos->fetch_object()){
                                        $html.="<tr><td><a href='#' onclick=\"location.href='web_crud.php?cola=14&adjunto={$adjunto->nombre}&id={$mail->id}'\">{$adjunto->nombre}</a></td></tr>";
                                    }
                                    $html.="</tbody></table></div></div></div>";
                                }else{
                                    $html.="</div></div>";
                                }
                                if($mail->leido=='0' && $mail->estado_gestion=='1' && $mail->es_respuesta=='0'){
                                    $html.="<div class='row'><div class='col-md-3 col-xs-3'><div class='form-group'><select required class=\"form-control input-sm\" name=\"{$fila}_agente\" id=\"{$fila}_agente\" fila='{$fila}' ><option value=\"\">Agente</option><option value=\"3684\">Experiencia del usuario</option><option value=\"3685\">Escucha del usuario</option><option value=\"3686\">Relacionamiento con la red</option></select></div></div><div class='col-md-3 col-xs-3'><div class='form-group'><input type='text' class='form-control input-sm' name='{$fila}_cedula' id='{$fila}_cedula' placeholder='Cedula de quien gestiona' required></div></div><div class='col-md-3 col-xs-3'><div class='form-group'><select required class=\"form-control input-sm tipificacion\" name=\"{$fila}_tipificacion\" id=\"{$fila}_tipificacion\" fila='{$fila}' ><option value=\"\">Tipificación</option>";

                                    $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b, MONOEF_EFECTIVA__B,  MONOEF_ConsInte__b, MONOEF_TipNo_Efe_b, MONOEF_Importanc_b, LISOPC_CambRepr__b , MONOEF_Contacto__b FROM DYALOGOCRM_SISTEMA.LISOPC 
                                            JOIN DYALOGOCRM_SISTEMA.MONOEF ON MONOEF.MONOEF_ConsInte__b = LISOPC.LISOPC_Clasifica_b
                                            WHERE LISOPC.LISOPC_ConsInte__OPCION_b = 3622;";
                                    $obj = $mysqli->query($Lsql);
                                    while($obje = $obj->fetch_object()){
                                        $html.="<option value='".$obje->OPCION_ConsInte__b."' efecividad = '".$obje->MONOEF_EFECTIVA__B."' monoef='".$obje->MONOEF_ConsInte__b."' TipNoEF = '".$obje->MONOEF_TipNo_Efe_b."' cambio='".$obje->LISOPC_CambRepr__b."' importancia = '".$obje->MONOEF_Importanc_b."' contacto='".$obje->MONOEF_Contacto__b."'>".($obje->OPCION_Nombre____b)."</option>";
                                    }          

                                    $html.="</select></div></div><div class='col-md-3 col-xs-3'><input type='submit' class=\"btn btn-primary btn-block\" id=\"Save\" value='Cerrar Gestión'></div></div>";
                                    $html.='<input type="hidden" name="Efectividad" id="Efectividad" value="0">
                                    <input type="hidden" name="'.$fila.'_MonoEf" id="'.$fila.'_MonoEf" value="0">
                                    <input type="hidden" name="'.$fila.'_TipNoEF" id="'.$fila.'_TipNoEF" value="0">
                                    <input type="hidden" name="'.$fila.'_FechaInicio" id="'.$fila.'_FechaInicio" value="0">
                                    <input type="hidden" name="'.$fila.'_FechaFinal" id="'.$fila.'_FechaFinal" value="0">
                                    <input type="hidden" name="'.$fila.'_MonoEfPeso" id="'.$fila.'_MonoEfPeso" value="0">
                                    <input type="hidden" name="'.$fila.'_ContactoMonoEf" id="'.$fila.'_ContactoMonoEf" value="0">
                                    <input type="hidden" name="'.$fila.'_reintento" id="'.$fila.'_reintento" value="0" value="0">
                                    <input type="hidden" name="'.$fila.'_correoId" id="'.$fila.'_correoId" value="'.$mail->id.'">
                                    <input type="hidden" name="'.$fila.'_cola" id="'.$fila.'_cola" value="14">
                                    <input type="hidden" name="'.$fila.'_correo" id="'.$fila.'_correo" value="'.$mail->de.'">';  
                                }else{
                                    $html.="<span style='color:gray'>Registro gestionado por: <strong> {$mail->nombre} </strong></span>";
                                }
                                $html.="</div></div></div></form>";                            
                                $fila++;
                            }
                            echo $html;
                        }else{
                            $html="<h1>No se encontraron registros<h1>";
                            echo $html;
                        }
                    }else{
                        $html="<h1>No se pudo ontener la información solicitada<h1>";
                    }
                }else{
                    $html="<h1>Los datos enviados no son validos<h1>";
                }
            }else{
                $html="<h1>Los datos enviados no son validos<h1>";
            }
        }
    }
?>
<script>
    <?php 
        if(isset($_SESSION['result'])){
        if($_SESSION['result'] == '1'){
        unset($_POST);
    ?>
        alertify.success('Guardado Exitoso');
    <?php
        }else{
        unset($_POST);
    ?>
        alertify.error('No se ha guardado la información');
    <?php
        }
        unset($_SESSION['result']);
        }
    ?>
    $(function() {
        $(".form_fila").submit(function(e) {
            e.preventDefault();
            var id=$(this).attr('id');
            swal({
                html: true,
                title: "Está seguro de marcarlo como gestionado ?",
                text: 'recuerde que si lo hace este registro ya no va a ser gestionado por ninguna otra persona.',
                type: "warning",
                confirmButtonText: "Gestionar",
                cancelButtonText: "Cancelar",
                showCancelButton: true,
                closeOnConfirm: true
            },
                function(isconfirm) {
                    if (isconfirm) {
                        $("#"+id).submit();
                    }
                }
            );
        });

        $(".tipificacion").change(function() {
            var id = $(this).attr('id');
            var fila = $(this).attr('fila');
            var valor = $("#" + id + " option:selected").attr('efecividad');
            var monoef = $("#" + id + " option:selected").attr('monoef');
            var TipNoEF = $("#" + id + " option:selected").attr('TipNoEF');
            var cambio = $("#" + id + " option:selected").attr('cambio');
            var importancia = $("#" + id + " option:selected").attr('importancia');
            var contacto = $("#" + id + " option:selected").attr('contacto');
            $("#" + fila + "_reintento").val(TipNoEF).change();
            $("#" + fila + "_Efectividad").val(valor);
            $("#" + fila + "_MonoEf").val(monoef);
            $("#" + fila + "_TipNoEF").val(TipNoEF);
            $("#" + fila + "_MonoEfPeso").val(importancia);
            $("#" + fila + "_ContactoMonoEf").val(contacto);
        });

        $("#campan").change(function() {
            if ($(this).val() == '60') {
                $("#de").attr('disabled', true).val('');
                $("#asunto").attr('disabled', true).val('');
                $("#cedula").attr('disabled', false);
            } else {
                if ($(this).val() == '14' || $(this).val() == '13') {
                    $("#cedula").attr('disabled', true).val('');
                    $("#asunto").attr('disabled', false);
                    $("#de").attr('disabled', false);
                } else {
                    $("#de").attr('disabled', true).val('');
                    $("#asunto").attr('disabled', true).val('');
                    $("#cedula").attr('disabled', true).val('');
                }
            }
        });

        $("#campan").val($("#campan").attr('opt')).trigger('change');
    });

</script>
