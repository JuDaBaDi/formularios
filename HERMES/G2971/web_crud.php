<?php

session_start();
include("../../conexion.php");

if (isset($_GET["adjunto"])) {
    if(isset($_GET['cola'])){
        $nombre=$_GET['adjunto'];
        $id=$_GET['id'];
        $archivo=$mysqli->query("SELECT nombre,ruta_fisica FROM dyalogo_canales_electronicos.dy_ce_entrante_adjuntos WHERE id_ce_entrante={$id} and nombre='{$nombre}'");
        if($archivo && $archivo->num_rows ==1){
            $archivo=$archivo->fetch_object();
            $ruta=$archivo->ruta_fisica;
            if (is_file($ruta)) {
                $size = strlen($nombre);
                if ($size>0) {
                    $nombre=basename($ruta);
                    $tamano = filesize($ruta);
                    header('Content-Description: File Transfer');
                    header("Content-type: application/force-download");
                    header("Content-disposition: attachment; filename=".$nombre);
                    header("Content-Transfer-Encoding: binary");
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header("Content-Length: " . $tamano);
                    ob_clean();
                    flush();
                    readfile($ruta);
                }            
            }else{ 
                // header('Location:' . getenv('HTTP_REFERER'));
                echo "<h1>EL ARCHIVO NO EXISTE O LO HAN BORRADO...<br> !!VUELVE ATRAS</h1>";
            }
        }
    }else{
        $archivo=$_GET["adjunto"];
        $extensiones = explode(".", $archivo);
        if (is_file("/mnt/disks/grabaciones/adjuntos/G2981/G2981/".$archivo)) {
            $size = strlen("/mnt/disks/grabaciones/adjuntos/G2981/G2981/".$archivo);

            if ($size>0) {
                $nombre=basename("/mnt/disks/grabaciones/adjuntos/G2981/G2981/".$archivo);
                $tamano = filesize("/mnt/disks/grabaciones/adjuntos/G2981/G2981/".$archivo);
                header("Content-Description: File Transfer");
                header("Content-type: application/force-download");
                header("Content-disposition: attachment; filename=".$nombre);
                header("Content-Transfer-Encoding: binary");
                header("Expires: 0");
                header("Cache-Control: must-revalidate");
                header("Pragma: public");
                header("Content-Length: " . $tamano);
                ob_clean();
                flush();
                readfile("/mnt/disks/grabaciones/adjuntos/G2981/G2981/".$archivo);
            }            
        }else{
            echo "<h1>EL ARCHIVO NO EXISTE O LO HAN BORRADO...<br> !!VUELVE ATRAS</h1>";
        }
    }
}

if(isset($_GET['fila'])){
    $fila=$_GET['fila'];
    if(isset($_POST[$fila.'_cola'])){
        $id=isset($_POST[$fila.'_correoId']) ? $_POST[$fila.'_correoId'] : false;
        $agente=isset($_POST[$fila.'agente']) ? $_POST[$fila.'agente'] : false;
        $cedula_agente=isset($_POST[$fila.'cedula']) ? $_POST[$fila.'cedula'] : false;
        $contacto=isset($_POST[$fila.'_ContactoMonoEf']) ? $_POST[$fila.'_ContactoMonoEf'] : 'NULL';
        $tipificacion=isset($_POST[$fila.'_G2990_C59529']) ? $_POST[$fila.'_G2990_C59529'] : 'NULL';
        $reintento=isset($_POST[$fila.'_reintento']) ? $_POST[$fila.'_reintento'] : 'NULL';
        $monoEf=isset($_POST[$fila.'_MonoEf']) ? $_POST[$fila.'_MonoEf'] : 'NULL';
        $MonoEfPeso=isset($_POST[$fila.'__MonoEfPeso']) ? $_POST[$fila.'__MonoEfPeso'] : 'NULL';
        
        if($_POST[$fila.'_cola']=='60'){
            $miembro=isset($_POST[$fila.'_miembro']) ? $_POST[$fila.'_miembro'] : false;
            $tipo_documento=isset($_POST[$fila.'_selected']) ? $_POST[$fila.'_selected'] : 'NULL';
            $documento=isset($_POST[$fila.'_G2981_C59373']) ? $_POST[$fila.'_G2981_C59373'] : 'NULL';
            $nombres=isset($_POST[$fila.'_G2981_C59374']) ? $_POST[$fila.'_G2981_C59374'] : 'NULL';
            $telefono=isset($_POST[$fila.'_G2981_C59375']) ? $_POST[$fila.'_G2981_C59375'] : 'NULL';
            $correo=isset($_POST[$fila.'_G2981_C59376']) ? $_POST[$fila.'_G2981_C59376'] : 'NULL';
            $solicitud=isset($_POST[$fila.'_G2981_C59377']) ? $_POST[$fila.'_G2981_C59377'] : 'NULL';
            $historia=isset($_POST[$fila.'_G2981_C59378']) ? $_POST[$fila.'_G2981_C59378'] : 'NULL';
            $orden=isset($_POST[$fila.'_G2981_C59379']) ? $_POST[$fila.'_G2981_C59379'] : 'NULL';
            $otros=isset($_POST[$fila.'_G2981_C59380']) ? $_POST[$fila.'_G2981_C59380'] : 'NULL';
            if($id && $agente){
                //ACTUALIZAR EL CORREO COMO GESTIONADO
                $sql=$mysqli->query("update dyalogo_canales_electronicos.dy_ce_entrantes set leido=true, estado_gestion=2, estado = 3, cco = 'Gestionado por link' where id ={$id}");
                //echo "update dyalogo_canales_electronicos.dy_ce_entrantes set leido=true, estado_gestion=2, estado = 3, cco = 'Gestionado por link' where id ={$id}"."<br>";

                //BORRAR DE DY_CE_ESPERA
                $sql=$mysqli->query("delete from dyalogo_canales_electronicos.dy_ce_espera where id_entrante ={$id}");
                //echo "delete from dyalogo_canales_electronicos.dy_ce_espera where id_entrante ={$id}"."<br>";

                //ACTUALIZAR EL SCRIPT
                $sql=$mysqli->query("insert into DYALOGOCRM_WEB.G2990 (G2990_FechaInsercion,G2990_Usuario,G2990_CodigoMiembro,G2990_Origen_b,G2990_IdLlamada,G2990_Sentido___b,G2990_Canal_____b,G2990_Paso,G2990_Clasificacion,G2990_Duracion___b,G2990_C59529,G2990_C59530,G2990_C59535,G2990_C59536,G2990_C59537,G2990_C59540,G2990_C59541,G2990_C59542,G2990_C59543,G2990_C59544,G2990_C59545,G2990_C59546,G2990_C59547,G2990_C59548,G2990_C59806) values(now(),{$agente},{$miembro},'normal',{$id},'Entrante','email',68,{$contacto},'00:03:00',{$tipificacion},{$reintento},now(),date_format(now(), '%H:%i:%s'),'EXPERIENCIA WEBFORM',{$tipo_documento},'{$documento}','{$nombres}','{$telefono}','{$correo}','{$solicitud}','{$historia}','{$orden}','{$otros}','{$cedula_agente}')");
                //echo "insert into DYALOGOCRM_WEB.G2990 (G2990_FechaInsercion,G2990_Usuario,G2990_CodigoMiembro,G2990_Origen_b,G2990_IdLlamada,G2990_Sentido___b,G2990_Canal_____b,G2990_Paso,G2990_Clasificacion,G2990_Duracion___b,G2990_C59529,G2990_C59530,G2990_C59535,G2990_C59536,G2990_C59537,G2990_C59540,G2990_C59541,G2990_C59542,G2990_C59543,G2990_C59544,G2990_C59545,G2990_C59546,G2990_C59547,G2990_C59548) values(now(),{$agente},{$miembro},'normal',{$id},'Entrante','email',68,{$contacto},'00:03:00',{$tipificacion},{$reintento},now(),date_format(now(), '%H:%i:%s'),'EXPERIENCIA WEBFORM',{$tipo_documento},'{$documento}','{$nombres}','{$telefono}','{$correo}','{$solicitud}','{$historia}','{$orden}','{$otros}')"."<br>";

                //INSERTAR EN CONDIA
                $sql=$mysqli->query("insert into DYALOGOCRM_SISTEMA.CONDIA (CONDIA_IndiEfec__b,CONDIA_TipNo_Efe_b,CONDIA_ConsInte__MONOEF_b,CONDIA_TiemDura__b,CONDIA_Fecha_____b,CONDIA_ConsInte__CAMPAN_b,CONDIA_ConsInte__USUARI_b,CONDIA_ConsInte__GUION__Gui_b,CONDIA_ConsInte__GUION__Pob_b,CONDIA_ConsInte__MUESTR_b,CONDIA_CodiMiem__b,CONDIA_Sentido___b,CONDIA_IdenLlam___b,CONDIA_UniqueId_b,CONDIA_Canal_b) VALUES(0,{$reintento},{$monoEf},concat(curdate(),' ','00:03:00'),now(),'60','{$agente}','2990','2981','2343','{$miembro}','2','{$correo}','{$correo}','email')");

                //echo "insert into DYALOGOCRM_SISTEMA.CONDIA (CONDIA_IndiEfec__b,CONDIA_TipNo_Efe_b,CONDIA_ConsInte__MONOEF_b,CONDIA_TiemDura__b,CONDIA_Fecha_____b,CONDIA_ConsInte__CAMPAN_b,CONDIA_ConsInte__USUARI_b,CONDIA_ConsInte__GUION__Gui_b,CONDIA_ConsInte__GUION__Pob_b,CONDIA_ConsInte__MUESTR_b,CONDIA_CodiMiem__b,CONDIA_Sentido___b,CONDIA_IdenLlam___b,CONDIA_UniqueId_b,CONDIA_Canal_b) VALUES(0,{$reintento},{$monoEf},concat(curdate(),' ','00:03:00'),now(),'60','{$agente}','2990','2981','2343','{$miembro}','2','{$id}','{$id}','email')"."<br>";

                //ACTUALIZAR BD
                $sql=$mysqli->query("update DYALOGOCRM_WEB.G2981 set G2981_IdLlamada={$id},G2981_UltiGest__b={$monoEf},G2981_FecUltGes_b=now(),G2981_TipoReintentoUG_b={$reintento},G2981_ClasificacionUG_b={$MonoEfPeso},G2981_EstadoUG_b={$contacto},G2981_UsuarioUG_b={$agente},G2981_Canal_____b='email',G2981_Sentido___b='Entrante',G2981_CantidadIntentos=(G2981_CantidadIntentos+1),G2981_PasoUG_b=68,G2981_C59803='{$cedula_agente}' where G2981_ConsInte__b={$miembro}");
                //echo "update DYALOGOCRM_WEB.G2981 set G2981_IdLlamada={$id},G2981_UltiGest__b={$monoEf},G2981_FecUltGes_b=now(),G2981_TipoReintentoUG_b={$reintento},G2981_ClasificacionUG_b={$MonoEfPeso},G2981_EstadoUG_b={$contacto},G2981_UsuarioUG_b={$agente},G2981_Canal_____b='email',G2981_Sentido___b='Entrante',G2981_CantidadIntentos=(G2981_CantidadIntentos+1),G2981_PasoUG_b=68 where G2981_ConsInte__b={$miembro}"."<br>";

                header('Location:https://'.$_SERVER['HTTP_HOST'].'/crm_php/formularios/G2981/web/webform.php');
                //header('Location:webform.php');
                $_SESSION['result']='1';
            }else{
                header('Location:https://'.$_SERVER['HTTP_HOST'].'/crm_php/formularios/G2981/web/webform.php');
                //header('Location:webform.php');
                $_SESSION['result']='0';
            }
        }else{
            $correo=isset($_POST[$fila.'_correo']) ? $_POST[$fila.'_correo'] : 'NULL';
            if($id && $agente){
                $cola=$_POST[$fila.'_cola']=='13' ? '13': '14';
                $strcola=$_POST[$fila.'_cola']=='13' ? 'EMSSANAR EXP - UsuariosEPSAfiliacion20201125' : 'EMSSANAR EXP - UsuariosEPSServicios20201125';
                //ACTUALIZAR EL CORREO COMO GESTIONADO
                $sql=$mysqli->query("update dyalogo_canales_electronicos.dy_ce_entrantes set leido=true, estado_gestion=2, estado = 3, cco = 'Gestionado por link' where id ={$id}");
                //echo "update dyalogo_canales_electronicos.dy_ce_entrantes set leido=true, estado_gestion=2, estado = 3, cco = 'Gestionado por link' where id ={$id}"."<br>";

                //BORRAR DE DY_CE_ESPERA
                $sql=$mysqli->query("delete from dyalogo_canales_electronicos.dy_ce_espera where id_entrante ={$id}");
                //echo "delete from dyalogo_canales_electronicos.dy_ce_espera where id_entrante ={$id}"."<br>";
                
                //INSERTAR O ACTUALIZAR BD
                if($correo){
                    $strCorreo=$mysqli->query("select * from DYALOGOCRM_WEB.G2970 where G2970_C59282='{$correo}' limit 1");
                    if($strCorreo && $strCorreo->num_rows>0){
                        $strCorreo=$strCorreo->fetch_object();
                        $miembro=$strCorreo->G2970_ConsInte__b;
                        //ACTUALIZAR BD
                        $sql=$mysqli->query("update DYALOGOCRM_WEB.G2970 set G2970_IdLlamada={$id},G2970_UltiGest__b={$monoEf},G2970_FecUltGes_b=now(),G2970_TipoReintentoUG_b={$reintento},G2970_ClasificacionUG_b={$MonoEfPeso},G2970_EstadoUG_b={$contacto},G2970_UsuarioUG_b={$agente},G2970_Canal_____b='email',G2970_Sentido___b='Entrante',G2970_CantidadIntentos=(G2970_CantidadIntentos+1),G2970_PasoUG_b='{$cola}',G2970_C59804='{$cedula_agente}' where G2970_ConsInte__b={$miembro}");
                        //echo "update DYALOGOCRM_WEB.G2970 set G2970_IdLlamada={$id},G2970_UltiGest__b={$monoEf},G2970_FecUltGes_b=now(),G2970_TipoReintentoUG_b={$reintento},G2970_ClasificacionUG_b={$MonoEfPeso},G2970_EstadoUG_b={$contacto},G2970_UsuarioUG_b={$agente},G2970_Canal_____b='email',G2970_Sentido___b='Entrante',G2970_CantidadIntentos=(G2970_CantidadIntentos+1),G2970_PasoUG_b=14 where G2970_ConsInte__b={$miembro}"."<br>";
                    }else{
                        $sql=$mysqli->query("insert into DYALOGOCRM_WEB.G2970 (G2970_FechaInsercion,G2970_CantidadIntentos,G2970_C59282) values(now(),0,'{$correo}')");
                        $miembro=$mysqli->insert_id;
                        $sql=$mysqli->query("update DYALOGOCRM_WEB.G2970 set G2970_IdLlamada={$id},G2970_UltiGest__b={$monoEf},G2970_FecUltGes_b=now(),G2970_TipoReintentoUG_b={$reintento},G2970_ClasificacionUG_b={$MonoEfPeso},G2970_EstadoUG_b={$contacto},G2970_UsuarioUG_b={$agente},G2970_Canal_____b='email',G2970_Sentido___b='Entrante',G2970_CantidadIntentos=(G2970_CantidadIntentos+1),G2970_PasoUG_b='{$cola}',G2970_C59804='{$cedula_agente}' where G2970_ConsInte__b={$miembro}");
                        //echo "update DYALOGOCRM_WEB.G2970 set G2970_IdLlamada={$id},G2970_UltiGest__b={$monoEf},G2970_FecUltGes_b=now(),G2970_TipoReintentoUG_b={$reintento},G2970_ClasificacionUG_b={$MonoEfPeso},G2970_EstadoUG_b={$contacto},G2970_UsuarioUG_b={$agente},G2970_Canal_____b='email',G2970_Sentido___b='Entrante',G2970_CantidadIntentos=(G2970_CantidadIntentos+1),G2970_PasoUG_b=14 where G2970_ConsInte__b={$miembro}"."<br>";                       
                    }
                    
                    //ACTUALIZAR EL SCRIPT
                    $sql=$mysqli->query("insert into DYALOGOCRM_WEB.G2971 (G2971_FechaInsercion,G2971_Usuario,G2971_CodigoMiembro,G2971_Origen_b,G2971_IdLlamada,G2971_Sentido___b,G2971_Canal_____b,G2971_Paso,G2971_Clasificacion,G2971_Duracion___b,G2971_C59283,G2971_C59284,G2971_C59289,G2971_C59290,G2971_C59291,G2971_C59295,G2971_C59805) values(now(),{$agente},{$miembro},'normal',{$id},'Entrante','email','{$cola}',{$contacto},'00:03:00',{$tipificacion},{$reintento},now(),date_format(now(), '%H:%i:%s'),'{$strcola}','{$correo}','{$cedula_agente}')");
                    //echo "insert into DYALOGOCRM_WEB.G2971 (G2971_FechaInsercion,G2971_Usuario,G2971_CodigoMiembro,G2971_Origen_b,G2971_IdLlamada,G2971_Sentido___b,G2971_Canal_____b,G2971_Paso,G2971_Clasificacion,G2971_Duracion___b,G2971_C59283,G2971_C59284,G2971_C59289,G2971_C59290,G2971_C59291,G2971_C59295) values(now(),{$agente},{$miembro},'normal',{$id},'Entrante','email',14,{$contacto},'00:03:00',{$tipificacion},{$reintento},now(),date_format(now(), '%H:%i:%s'),'EMSSANAR EXP - UsuariosEPSServicios20201125','{$correo}')"."<br>";
                    
                    //INSERTAR EN CONDIA
                    $sql=$mysqli->query("insert into DYALOGOCRM_SISTEMA.CONDIA (CONDIA_IndiEfec__b,CONDIA_TipNo_Efe_b,CONDIA_ConsInte__MONOEF_b,CONDIA_TiemDura__b,CONDIA_Fecha_____b,CONDIA_ConsInte__CAMPAN_b,CONDIA_ConsInte__USUARI_b,CONDIA_ConsInte__GUION__Gui_b,CONDIA_ConsInte__GUION__Pob_b,CONDIA_ConsInte__MUESTR_b,CONDIA_CodiMiem__b,CONDIA_Sentido___b,CONDIA_IdenLlam___b,CONDIA_UniqueId_b,CONDIA_Canal_b) VALUES(0,{$reintento},{$monoEf},concat(curdate(),' ','00:03:00'),now(),'{$cola}','{$agente}','2971','2970','2292','{$miembro}','2','{$id}','{$id}','email')");

                    //echo "insert into DYALOGOCRM_SISTEMA.CONDIA (CONDIA_IndiEfec__b,CONDIA_TipNo_Efe_b,CONDIA_ConsInte__MONOEF_b,CONDIA_TiemDura__b,CONDIA_Fecha_____b,CONDIA_ConsInte__CAMPAN_b,CONDIA_ConsInte__USUARI_b,CONDIA_ConsInte__GUION__Gui_b,CONDIA_ConsInte__GUION__Pob_b,CONDIA_ConsInte__MUESTR_b,CONDIA_CodiMiem__b,CONDIA_Sentido___b,CONDIA_IdenLlam___b,CONDIA_UniqueId_b,CONDIA_Canal_b) VALUES(0,{$reintento},{$monoEf},concat(curdate(),' ','00:03:00'),now(),'14','{$agente}','2971','2970','2292','{$miembro}','2','{$id}','{$id}','email')"."<br>";                    
                }
                header('Location:https://'.$_SERVER['HTTP_HOST'].'/crm_php/formularios/G2981/web/webform.php');
                //header('Location:webform.php');
                $_SESSION['result']='1';
                
            }else{
                header('Location:https://'.$_SERVER['HTTP_HOST'].'/crm_php/formularios/G2981/web/webform.php');
                //header('Location:webform.php');
                $_SESSION['result']='0';
            }
        }
    }  
}
?>