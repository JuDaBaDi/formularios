<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2964_ConsInte__b, G2964_FechaInsercion , G2964_Usuario ,  G2964_CodigoMiembro  , G2964_PoblacionOrigen , G2964_EstadoDiligenciamiento ,  G2964_IdLlamada , G2964_C59207 as principal ,G2964_C59209,G2964_C59210,G2964_C59211,G2964_C59213,G2964_C59214,G2964_C59229,G2964_C59212,G2964_C59198,G2964_C59199,G2964_C59200,G2964_C59201,G2964_C59202,G2964_C59203,G2964_C59204,G2964_C59205,G2964_C59206,G2964_C59207,G2964_C59208 FROM '.$BaseDatos.'.G2964 WHERE G2964_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2964_C59209'] = $key->G2964_C59209;

                $datos[$i]['G2964_C59210'] = $key->G2964_C59210;

                $datos[$i]['G2964_C59211'] = $key->G2964_C59211;

                $datos[$i]['G2964_C59213'] = $key->G2964_C59213;

                $datos[$i]['G2964_C59214'] = $key->G2964_C59214;

                $datos[$i]['G2964_C59229'] = $key->G2964_C59229;

                $datos[$i]['G2964_C59212'] = $key->G2964_C59212;

                $datos[$i]['G2964_C59198'] = $key->G2964_C59198;

                $datos[$i]['G2964_C59199'] = $key->G2964_C59199;

                $datos[$i]['G2964_C59200'] = explode(' ', $key->G2964_C59200)[0];
  
                $hora = '';
                if(!is_null($key->G2964_C59201)){
                    $hora = explode(' ', $key->G2964_C59201)[1];
                }

                $datos[$i]['G2964_C59201'] = $hora;

                $datos[$i]['G2964_C59202'] = $key->G2964_C59202;

                $datos[$i]['G2964_C59203'] = $key->G2964_C59203;

                $datos[$i]['G2964_C59204'] = $key->G2964_C59204;

                $datos[$i]['G2964_C59205'] = $key->G2964_C59205;

                $datos[$i]['G2964_C59206'] = $key->G2964_C59206;

                $datos[$i]['G2964_C59207'] = $key->G2964_C59207;

                $datos[$i]['G2964_C59208'] = $key->G2964_C59208;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2964";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2964_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2964_ConsInte__b as id,  G2964_C59209 as camp2 , G2964_C59207 as camp1 
                     FROM ".$BaseDatos.".G2964  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2964_ConsInte__b as id,  G2964_C59209 as camp2 , G2964_C59207 as camp1  
                    FROM ".$BaseDatos.".G2964  JOIN ".$BaseDatos.".G2964_M".$_POST['muestra']." ON G2964_ConsInte__b = G2964_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2964_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2964_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2964_C59209 LIKE '%".$B."%' OR G2964_C59207 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2964_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        
                            if(isset($_GET['CallDatosCombo_Guion_G2964_C59229'])){
                                $Ysql = "SELECT G2965_ConsInte__b as id, G2965_C59221 as text FROM ".$BaseDatos.".G2965 WHERE G2965_C59221 LIKE '%".$_POST['q']."%'";
                                $guion = $mysqli->query($Ysql);
                                $i = 0;
                                $datos = array();
                                while($obj = $guion->fetch_object()){
                                    $datos[$i]['id'] = $obj->id;
                                    $datos[$i]['text'] = $obj->text;
                                    $i++;
                                } 
                                echo json_encode($datos);
                            }
                            if(isset($_POST["dameValoresCamposDinamicos_Guion_G2964_C59229"])){
                                 $strSQLOpt_t = "SELECT * FROM ".$BaseDatos.".G2965 WHERE G2965_ConsInte__b = ".$_POST["dameValoresCamposDinamicos_Guion_G2964_C59229"];
                                $resSQLOpt_t = $mysqli->query($strSQLOpt_t);

                                if ($resSQLOpt_t) {
                                    if ($resSQLOpt_t->num_rows > 0) {
                                        $objSQLOpt_t = $resSQLOpt_t->fetch_object();
                                        echo json_encode($objSQLOpt_t);
                                    }
                                }

                            }


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2964");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2964_ConsInte__b, G2964_FechaInsercion , G2964_Usuario ,  G2964_CodigoMiembro  , G2964_PoblacionOrigen , G2964_EstadoDiligenciamiento ,  G2964_IdLlamada , G2964_C59207 as principal ,G2964_C59209,G2964_C59210,G2964_C59211, a.LISOPC_Nombre____b as G2964_C59213,G2964_C59214, G2965_C59221,G2964_C59212, b.LISOPC_Nombre____b as G2964_C59198, c.LISOPC_Nombre____b as G2964_C59199,G2964_C59200,G2964_C59201,G2964_C59202,G2964_C59203,G2964_C59204,G2964_C59205,G2964_C59206,G2964_C59207,G2964_C59208 FROM '.$BaseDatos.'.G2964 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2964_C59213 LEFT JOIN '.$BaseDatos.'.G2965 ON G2965_ConsInte__b  =  G2964_C59229 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2964_C59198 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2964_C59199';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2964_C59201)){
                    $hora_a = explode(' ', $fila->G2964_C59201)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2964_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2964_ConsInte__b , ($fila->G2964_C59209) , ($fila->G2964_C59210) , ($fila->G2964_C59211) , ($fila->G2964_C59213) , ($fila->G2964_C59214) , ($fila->G2965_C59221) , ($fila->G2964_C59212) , ($fila->G2964_C59198) , ($fila->G2964_C59199) , explode(' ', $fila->G2964_C59200)[0] , $hora_a , ($fila->G2964_C59202) , ($fila->G2964_C59203) , ($fila->G2964_C59204) , ($fila->G2964_C59205) , ($fila->G2964_C59206) , ($fila->G2964_C59207) , ($fila->G2964_C59208) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2964 WHERE G2964_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2964";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2964_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2964_ConsInte__b as id,  G2964_C59209 as camp2 , G2964_C59207 as camp1  FROM '.$BaseDatos.'.G2964 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2964_ConsInte__b as id,  G2964_C59209 as camp2 , G2964_C59207 as camp1  
                    FROM ".$BaseDatos.".G2964  JOIN ".$BaseDatos.".G2964_M".$_POST['muestra']." ON G2964_ConsInte__b = G2964_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2964_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2964_C59209 LIKE "%'.$B.'%" OR G2964_C59207 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2964_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2964 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2964(";
            $LsqlV = " VALUES ("; 
  
            if(isset($_POST["G2964_C59209"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2964_C59209 = '".$_POST["G2964_C59209"]."'";
                $LsqlI .= $separador."G2964_C59209";
                $LsqlV .= $separador."'".$_POST["G2964_C59209"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2964_C59210"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2964_C59210 = '".$_POST["G2964_C59210"]."'";
                $LsqlI .= $separador."G2964_C59210";
                $LsqlV .= $separador."'".$_POST["G2964_C59210"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2964_C59211"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2964_C59211 = '".$_POST["G2964_C59211"]."'";
                $LsqlI .= $separador."G2964_C59211";
                $LsqlV .= $separador."'".$_POST["G2964_C59211"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2964_C59213"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2964_C59213 = '".$_POST["G2964_C59213"]."'";
                $LsqlI .= $separador."G2964_C59213";
                $LsqlV .= $separador."'".$_POST["G2964_C59213"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2964_C59214"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2964_C59214 = '".$_POST["G2964_C59214"]."'";
                $LsqlI .= $separador."G2964_C59214";
                $LsqlV .= $separador."'".$_POST["G2964_C59214"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2964_C59229"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2964_C59229 = '".$_POST["G2964_C59229"]."'";
                $LsqlI .= $separador."G2964_C59229";
                $LsqlV .= $separador."'".$_POST["G2964_C59229"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2964_C59212"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2964_C59212 = '".$_POST["G2964_C59212"]."'";
                $LsqlI .= $separador."G2964_C59212";
                $LsqlV .= $separador."'".$_POST["G2964_C59212"]."'";
                $validar = 1;
            }
             
 
            $G2964_C59198 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2964_C59198 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2964_C59198 = ".$G2964_C59198;
                    $LsqlI .= $separador." G2964_C59198";
                    $LsqlV .= $separador.$G2964_C59198;
                    $validar = 1;

                    
                }
            }
 
            $G2964_C59199 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2964_C59199 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2964_C59199 = ".$G2964_C59199;
                    $LsqlI .= $separador." G2964_C59199";
                    $LsqlV .= $separador.$G2964_C59199;
                    $validar = 1;
                }
            }
 
            $G2964_C59200 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2964_C59200 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2964_C59200 = ".$G2964_C59200;
                    $LsqlI .= $separador." G2964_C59200";
                    $LsqlV .= $separador.$G2964_C59200;
                    $validar = 1;
                }
            }
 
            $G2964_C59201 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2964_C59201 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2964_C59201 = ".$G2964_C59201;
                    $LsqlI .= $separador." G2964_C59201";
                    $LsqlV .= $separador.$G2964_C59201;
                    $validar = 1;
                }
            }
 
            $G2964_C59202 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2964_C59202 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2964_C59202 = ".$G2964_C59202;
                    $LsqlI .= $separador." G2964_C59202";
                    $LsqlV .= $separador.$G2964_C59202;
                    $validar = 1;
                }
            }
  
            if(isset($_POST["G2964_C59203"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2964_C59203 = '".$_POST["G2964_C59203"]."'";
                $LsqlI .= $separador."G2964_C59203";
                $LsqlV .= $separador."'".$_POST["G2964_C59203"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2964_C59204"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2964_C59204 = '".$_POST["G2964_C59204"]."'";
                $LsqlI .= $separador."G2964_C59204";
                $LsqlV .= $separador."'".$_POST["G2964_C59204"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2964_C59205"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2964_C59205 = '".$_POST["G2964_C59205"]."'";
                $LsqlI .= $separador."G2964_C59205";
                $LsqlV .= $separador."'".$_POST["G2964_C59205"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2964_C59206"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2964_C59206 = '".$_POST["G2964_C59206"]."'";
                $LsqlI .= $separador."G2964_C59206";
                $LsqlV .= $separador."'".$_POST["G2964_C59206"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2964_C59207"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2964_C59207 = '".$_POST["G2964_C59207"]."'";
                $LsqlI .= $separador."G2964_C59207";
                $LsqlV .= $separador."'".$_POST["G2964_C59207"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2964_C59208"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2964_C59208 = '".$_POST["G2964_C59208"]."'";
                $LsqlI .= $separador."G2964_C59208";
                $LsqlV .= $separador."'".$_POST["G2964_C59208"]."'";
                $validar = 1;
            }
             
  
            if(isset($_POST["G2964_C59215"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2964_C59215 = '".$_POST["G2964_C59215"]."'";
                $LsqlI .= $separador."G2964_C59215";
                $LsqlV .= $separador."'".$_POST["G2964_C59215"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2964_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2964_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2964_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2964_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2964_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2964_Usuario , G2964_FechaInsercion, G2964_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2964_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2964 WHERE G2964_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"];
                        }
                        echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
