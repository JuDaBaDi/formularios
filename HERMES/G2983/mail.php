<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="tipo_contenido"  content="text/html;" http-equiv="content-type" charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dyalogo CRM</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="../../assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../../assets/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="../../assets/ionicons-master/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../../assets/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="../../assets/css/skins/_all-skins.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="../../assets/plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="../../assets/plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="../../assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="../../assets/plugins/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="../../assets/plugins/daterangepicker/daterangepicker.css">
         <!-- Bootstrap time Picker -->
               <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="../../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <link rel="stylesheet" href="../../assets/css/alertify.core.css">

        <link rel="stylesheet" href="../../assets/css/alertify.default.css">
        <link rel="stylesheet" type="text/css" media="screen" href="../../assets/Guriddo_jqGrid_/css/ui.jqgrid-bootstrap.css" />
        <link rel="stylesheet" href="../../assets/plugins/select2/select2.min.css" />
        <link rel="stylesheet" href="../../assets/plugins/sweetalert/sweetalert.css" />
        <script src="../../assets/plugins/sweetalert/sweetalert.min.js"></script>
        
        <script src="../../assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        
        <link rel="stylesheet" href="../../assets/plugins/datatables/dataTables.bootstrap.css">
        <script src="../../assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../../assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
        <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
    </head>
<?php 
    include("../../conexion.php");
    include('../../funciones.php');
?>    
    <section class="content" style="padding:0% 5%">
        <div class="box box-primary">
            <div class="box-body" style="padding:2% 0%">
                <div class="row" id="mails" style="width:90%;margin:auto">
                    <?php
                    if(isset($_GET['correo']) && $_GET['correo'] != ''){
                        $sql=$mysqli->query("SELECT a.de,a.asunto,a.cuerpo,a.fecha_hora,b.fecha_hora AS fecha_enviado,b.cuerpo AS cuerpo_enviado,b.nombre_usuario FROM dyalogo_canales_electronicos.dy_ce_entrantes a JOIN dyalogo_canales_electronicos.dy_ce_rastreo b ON a.id=b.id_ce_entrante WHERE a.de='{$_GET['correo']}' ORDER BY a.fecha_hora DESC LIMIT 100");
                        if($sql){
                            if($sql->num_rows>0){
                                $html='';
                                $fila=0;
                                while($mail =$sql->fetch_object()){
                                    $html.="<div class=\"panel box box-primary box-solid\" style=\"margin-bottom:10px;\"><div class=\"box-header with-border\" style=\"height:30px\"><h3 class=\"box-title\" style=\"width:100%\"><a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#fila_{$fila}\" style=\"display:flex;justify-content:space-between;font-size:14px\"><span>Fecha: {$mail->fecha_hora}</span><span>Asunto: {$mail->asunto}</span></a></h3></div><div id=\"fila_{$fila}\" class=\"panel-collapse collapse\"><div class=\"box-body\"><div class=\"row\"><div class=\"col-md-12 col-xs-12 form-group\"><div class=\"form-group\"><div style=\"display:flex;justify-content:space-between\"><div><strong>Fecha: </strong><span>{$mail->fecha_hora}</span></div><div><strong>Fecha respuesta: </strong><span>{$mail->fecha_enviado}</span></div></div></div><div class=\"form-group\"><div style=\"display:flex;justify-content:space-between\"><div><strong>Asunto: </strong><span>{$mail->asunto}</span></div><div><strong>Agente: </strong><span>{$mail->nombre_usuario}</span></div></div></div><div class=\"form-group\"><table class=\"table table-bordered\"><thead><tr><th>Cuerpo del mail recibido</th><th>Cuerpo del mail Enviado</th></tr></thead><tbody><tr><td>{$mail->cuerpo}</td><td>{$mail->cuerpo_enviado}</td></tr></tbody></table></div></div></div></div></div></div>";
                                    $fila++;
                                }
                                echo $html;
                            }else{
                                $html="<h1>Aún no se han recibido correos electrónicos del correo {$_GET['correo']}<h1>";
                                echo $html;
                            }
                        }
                    }
                    ?>                   
                </div>
            </div>
        </div>
    </section>
</html>