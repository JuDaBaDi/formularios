<?php
    ini_set('display_errors', 'On');
    ini_set('display_errors', 1);
    include(__DIR__."/../../conexion.php");
    include(__DIR__."/../../funciones.php");
    date_default_timezone_set('America/Bogota');
    if (isset($_GET["adjunto"])) {
            $archivo=$_GET["adjunto"];
            $extensiones = explode(".", $archivo);
            // $ext = ["pdf","jpeg","jpg","png"];
            // if (in_array(end($extensiones),$ext)) {
                if (is_file("/mnt/disks/grabaciones/adjuntos/G2981/G2981/".$archivo)) {
                    $size = strlen("/mnt/disks/grabaciones/adjuntos/G2981/G2981/".$archivo);

                    if ($size>0) {
                        $nombre=basename("/mnt/disks/grabaciones/adjuntos/G2981/G2981/".$archivo);
                        $tamaÃ±o = filesize("/mnt/disks/grabaciones/adjuntos/G2981/G2981/".$archivo);
                        header("Content-Description: File Transfer");
                        header("Content-type: application/force-download");
                        header("Content-disposition: attachment; filename=".$nombre);
                        header("Content-Transfer-Encoding: binary");
                        header("Expires: 0");
                        header("Cache-Control: must-revalidate");
                        header("Pragma: public");
                        header("Content-Length: " . $tamaÃ±o);
                        ob_clean();
                        flush();
                        readfile("/mnt/disks/grabaciones/adjuntos/G2981/G2981/".$archivo);
                    }            
                }else{ 
                    // header("Location:" . getenv("HTTP_REFERER"));
                    echo "<h1>EL ARCHIVO NO EXISTE O LO HAN BORRADO...<br> !!VUELVE ATRAS</h1>";
                }            
            // }else{
                // header("Location:" . getenv("HTTP_REFERER"));
                // echo "Este archivo no se puede descargar.";
            // }

    }
        
    // JDBD - se envia calificacion de la gestion al agente que la creo.
    
    
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      //Datos del formulario
      if(isset($_POST['CallDatos'])){
          
            $Lsql = 'SELECT G2990_ConsInte__b, G2990_FechaInsercion , G2990_Usuario ,  G2990_CodigoMiembro  , G2990_PoblacionOrigen , G2990_EstadoDiligenciamiento ,  G2990_IdLlamada , G2990_C59541 as principal ,G2990_C59540,G2990_C59541,G2990_C59542,G2990_C59543,G2990_C59544,G2990_C59545,G2990_C59546,G2990_C59547,G2990_C59548,G2990_C59529,G2990_C59530,G2990_C59531,G2990_C59532,G2990_C59533,G2990_C59534,G2990_C59535,G2990_C59536,G2990_C59537 FROM '.$BaseDatos.'.G2990 WHERE G2990_ConsInte__b ='.$_POST['id'];
            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;

            while($key = $result->fetch_object()){

                $datos[$i]['G2990_C59540'] = $key->G2990_C59540;

                $datos[$i]['G2990_C59541'] = $key->G2990_C59541;

                $datos[$i]['G2990_C59542'] = $key->G2990_C59542;

                $datos[$i]['G2990_C59543'] = $key->G2990_C59543;

                $datos[$i]['G2990_C59544'] = $key->G2990_C59544;

                $datos[$i]['G2990_C59545'] = $key->G2990_C59545;

                $datos[$i]['G2990_C59546'] = $key->G2990_C59546;

                $datos[$i]['G2990_C59547'] = $key->G2990_C59547;

                $datos[$i]['G2990_C59548'] = $key->G2990_C59548;

                $datos[$i]['G2990_C59529'] = $key->G2990_C59529;

                $datos[$i]['G2990_C59530'] = $key->G2990_C59530;

                $datos[$i]['G2990_C59531'] = explode(' ', $key->G2990_C59531)[0];
  
                $hora = '';
                if(!is_null($key->G2990_C59532)){
                    $hora = explode(' ', $key->G2990_C59532)[1];
                }

                $datos[$i]['G2990_C59532'] = $hora;

                $datos[$i]['G2990_C59533'] = $key->G2990_C59533;

                $datos[$i]['G2990_C59534'] = $key->G2990_C59534;

                $datos[$i]['G2990_C59535'] = $key->G2990_C59535;

                $datos[$i]['G2990_C59536'] = $key->G2990_C59536;

                $datos[$i]['G2990_C59537'] = $key->G2990_C59537;
      
                $datos[$i]['principal'] = $key->principal;
                $i++;
            }
            echo json_encode($datos);
        }


        //JDBD-2020-05-03 : Datos de la lista de la izquierda
        if(isset($_POST['CallDatosJson'])){

            $strLimit_t = " LIMIT 0, 50";

            //JDBD-2020-05-03 : Preguntamos si esta funcion es llamada por el boton de (Buscar o lupa) o por el scroll.
            if (isset($_POST["strScroll_t"])) {
                if ($_POST["strScroll_t"] == "si") {
                    $strLimit_t = " LIMIT ".$_POST["inicio_t"].", ".$_POST["fin_t"];
                }
            }

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2990";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {

                $objRegPro_t = $resRegPro_t->fetch_object();
                
                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = " AND G2990_Usuario = ".$_POST["idUsuario"]." ";
                }else{
                    $strRegProp_t = "";
                }

            }else{
                $strRegProp_t = "";
            }

            //JDBD-2020-05-03 : Consulta estandar de los registros del guion.
            $Lsql = "SELECT G2990_ConsInte__b as id,  G2990_C59541 as camp1 , G2990_C59542 as camp2 
                     FROM ".$BaseDatos.".G2990  WHERE TRUE ".$strRegProp_t;

            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Lsql = "SELECT G2990_ConsInte__b as id,  G2990_C59541 as camp1 , G2990_C59542 as camp2  
                    FROM ".$BaseDatos.".G2990  JOIN ".$BaseDatos.".G2990_M".$_POST['muestra']." ON G2990_ConsInte__b = G2990_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Lsql .= " AND G2990_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            if (isset($_POST["arrNumerosFiltros_t"])) {

                //JDBD-2020-05-03 : Busqueda Avanzada.

                $arrNumerosFiltros_t = explode(",", $_POST["arrNumerosFiltros_t"]);

                $intNumerosFiltros_t = count($arrNumerosFiltros_t);

                if ($intNumerosFiltros_t > 0) {
                    $Lsql .= " AND (";
                    foreach ($arrNumerosFiltros_t as $key => $filtro) {
                        if (is_numeric($_POST["selCampo_".$filtro])) {
                            $Lsql .= operadorYFiltro("G2990_C".$_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }else{
                            $Lsql .= operadorYFiltro($_POST["selCampo_".$filtro],$_POST["selOperador_".$filtro],$_POST["tipo_".$filtro],$_POST["valor_".$filtro]);
                        }

                        if (array_key_exists(($key+1),$arrNumerosFiltros_t)) {
                            if (isset($_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])])) {
                                $Lsql .= $_POST["selCondicion_".($arrNumerosFiltros_t[$key+1])]." ";
                            }
                        }
                    }
                    $Lsql .= ") ";
                }

            }else{

                //JDBD-2020-05-03 : Busqueda Sencilla por la Lupa.

                $B = $_POST["B"];

                if ($B != "" && $B != NULL) {
                    $Lsql .= " AND (G2990_C59541 LIKE '%".$B."%' OR G2990_C59542 LIKE '%".$B."%') ";
                }

            }


            $Lsql .= " ORDER BY G2990_ConsInte__b DESC".$strLimit_t; 

            $result = $mysqli->query($Lsql);
            $datos = array();
            $i = 0;
            while($key = $result->fetch_object()){
                $datos[$i]['camp1'] = strtoupper(($key->camp1));
                $datos[$i]['camp2'] = strtoupper(($key->camp2));
                $datos[$i]['id'] = $key->id;
                $i++;
            }
            echo json_encode($datos);
        }

        if(isset($_POST['getListaHija'])){
            $Lsql = "SELECT LISOPC_ConsInte__b , LISOPC_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__LISOPC_Depende_b = ".$_POST['idPadre']." AND LISOPC_ConsInte__OPCION_b = ".$_POST['opcionID'];
            $res = $mysqli->query($Lsql);
            echo "<option value='0'>Seleccione</option>";
            while($key = $res->fetch_object()){
                echo "<option value='".$key->LISOPC_ConsInte__b."'>".$key->LISOPC_Nombre____b."</option>";
            }
        }


        //Esto ya es para cargar los combos en la grilla

        if(isset($_GET['CallDatosLisop_'])){
            $lista = $_GET['idLista'];
            $comboe = $_GET['campo'];
            $Lsql = "SELECT LISOPC_ConsInte__b AS OPCION_ConsInte__b, LISOPC_Nombre____b AS OPCION_Nombre____b FROM ".$BaseDatos_systema.".LISOPC WHERE LISOPC_ConsInte__OPCION_b = ".$lista." ORDER BY LISOPC_Nombre____b";
            
            $combo = $mysqli->query($Lsql);
            echo '<select class="form-control input-sm"  name="'.$comboe.'" id="'.$comboe.'">';
            echo '<option value="0">Seleccione</option>';
            while($obj = $combo->fetch_object()){
                echo "<option value='".$obj->OPCION_ConsInte__b."'>".$obj->OPCION_Nombre____b."</option>";
            }   
            echo '</select>'; 
        } 

        


        // esto carga los datos de la grilla CallDatosJson
        if(isset($_GET['CallDatosJson'])){
            $page = $_POST['page'];  // Almacena el numero de pagina actual
            $limit = $_POST['rows']; // Almacena el numero de filas que se van a mostrar por pagina
            $sidx = $_POST['sidx'];  // Almacena el indice por el cual se harÃ¡ la ordenaciÃ³n de los datos
            $sord = $_POST['sord'];  // Almacena el modo de ordenaciÃ³n
            if(!$sidx) $sidx =1;
            //Se hace una consulta para saber cuantos registros se van a mostrar
            $result = $mysqli->query("SELECT COUNT(*) AS count FROM ".$BaseDatos.".G2990");
            // Se obtiene el resultado de la consulta
            $fila = $result->fetch_array();
            $count = $fila['count'];
            //En base al numero de registros se obtiene el numero de paginas
            if( $count >0 ) {
                $total_pages = ceil($count/$limit);
            } else {
                $total_pages = 0;
            }
            if ($page > $total_pages)
                $page=$total_pages;

            //Almacena numero de registro donde se va a empezar a recuperar los registros para la pagina
            $start = $limit*$page - $limit; 
            //Consulta que devuelve los registros de una sola pagina

            $Lsql = 'SELECT G2990_ConsInte__b, G2990_FechaInsercion , G2990_Usuario ,  G2990_CodigoMiembro  , G2990_PoblacionOrigen , G2990_EstadoDiligenciamiento ,  G2990_IdLlamada , G2990_C59541 as principal , a.LISOPC_Nombre____b as G2990_C59540,G2990_C59541,G2990_C59542,G2990_C59543,G2990_C59544,G2990_C59545,G2990_C59546,G2990_C59547,G2990_C59548, b.LISOPC_Nombre____b as G2990_C59529, c.LISOPC_Nombre____b as G2990_C59530,G2990_C59531,G2990_C59532,G2990_C59533,G2990_C59534,G2990_C59535,G2990_C59536,G2990_C59537 FROM '.$BaseDatos.'.G2990 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as a ON a.LISOPC_ConsInte__b =  G2990_C59540 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as b ON b.LISOPC_ConsInte__b =  G2990_C59529 LEFT JOIN '.$BaseDatos_systema.'.LISOPC as c ON c.LISOPC_ConsInte__b =  G2990_C59530';
            if ($_REQUEST["_search"] == "false") {
                $where = " where 1";
            } else {
                $operations = array(
                    'eq' => "= '%s'",            // Equal
                    'ne' => "<> '%s'",           // Not equal
                    'lt' => "< '%s'",            // Less than
                    'le' => "<= '%s'",           // Less than or equal
                    'gt' => "> '%s'",            // Greater than
                    'ge' => ">= '%s'",           // Greater or equal
                    'bw' => "like '%s%%'",       // Begins With
                    'bn' => "not like '%s%%'",   // Does not begin with
                    'in' => "in ('%s')",         // In
                    'ni' => "not in ('%s')",     // Not in
                    'ew' => "like '%%%s'",       // Ends with
                    'en' => "not like '%%%s'",   // Does not end with
                    'cn' => "like '%%%s%%'",     // Contains
                    'nc' => "not like '%%%s%%'", // Does not contain
                    'nu' => "is null",           // Is null
                    'nn' => "is not null"        // Is not null
                ); 
                $value = $mysqli->real_escape_string($_REQUEST["searchString"]);
                $where = sprintf(" where %s ".$operations[$_REQUEST["searchOper"]], $_REQUEST["searchField"], $value);
            }
            $Lsql .= $where.' ORDER BY '.$sidx.' '.$sord.' LIMIT '.$start.','.$limit;
            $result = $mysqli->query($Lsql);
            $respuesta = array();
            $respuesta['page'] = $page;
            $respuesta['total'] = $total_pages;
            $respuesta['records'] = $count;
            $i=0;
            while( $fila = $result->fetch_object() ) {  
                

                $hora_a = '';
                //esto es para todo los tipo fecha, para que no muestre la parte de la hora
                if(!is_null($fila->G2990_C59532)){
                    $hora_a = explode(' ', $fila->G2990_C59532)[1];
                }
                $respuesta['rows'][$i]['id']=$fila->G2990_ConsInte__b;
                $respuesta['rows'][$i]['cell']=array($fila->G2990_ConsInte__b , ($fila->G2990_C59540) , ($fila->G2990_C59541) , ($fila->G2990_C59542) , ($fila->G2990_C59543) , ($fila->G2990_C59544) , ($fila->G2990_C59545) , ($fila->G2990_C59546) , ($fila->G2990_C59547) , ($fila->G2990_C59548) , ($fila->G2990_C59529) , ($fila->G2990_C59530) , explode(' ', $fila->G2990_C59531)[0] , $hora_a , ($fila->G2990_C59533) , ($fila->G2990_C59534) , ($fila->G2990_C59535) , ($fila->G2990_C59536) , ($fila->G2990_C59537) );
                $i++;
            }
            // La respuesta se regresa como json
            echo json_encode($respuesta);
        }

        if(isset($_POST['CallEliminate'])){
            if($_POST['oper'] == 'del'){
                $Lsql = "DELETE FROM ".$BaseDatos.".G2990 WHERE G2990_ConsInte__b = ".$_POST['id'];
                if ($mysqli->query($Lsql) === TRUE) {
                    //echo "1";
                } else {
                    echo "Error eliminado los registros : " . $mysqli->error;
                }
            }
        }

        if(isset($_POST['callDatosNuevamente'])){

            //JDBD-2020-05-03 : Averiguamos si el usuario en session solo puede ver sus propios registros.
            $strRegPro_t = "SELECT PEOBUS_VeRegPro__b AS reg FROM ".$BaseDatos_systema.".PEOBUS WHERE PEOBUS_ConsInte__USUARI_b = ".$_POST["idUsuario"]." AND PEOBUS_ConsInte__GUION__b = 2990";

            $resRegPro_t = $mysqli->query($strRegPro_t);

            if ($resRegPro_t->num_rows > 0) {
                
                $objRegPro_t = $resRegPro_t->fetch_object();

                if ($objRegPro_t->reg != 0) {
                    $strRegProp_t = ' AND G2990_Usuario = '.$_POST["idUsuario"].' ';
                }else{
                    $strRegProp_t = '';
                }
                
            }else{
                $strRegProp_t = '';
            }


            $inicio = $_POST['inicio'];
            $fin = $_POST['fin'];

            $B = "";

            if (isset($_POST["B"])) {
                $B = $_POST["B"];
            }

            //JDBD-2020-05-03 : Consulta estandar para los registros del guion.
            $Zsql = 'SELECT  G2990_ConsInte__b as id,  G2990_C59541 as camp1 , G2990_C59542 as camp2  FROM '.$BaseDatos.'.G2990 WHERE TRUE'.$strRegProp_t;
            
            // Si lo que estamos consultando es de tareas de backoffice cambia la consulta
            if(isset($_POST['tareaBackoffice']) && $_POST['tareaBackoffice'] == 1 && isset($_POST['muestra']) && $_POST['muestra'] != 0){

                $Zsql = "SELECT G2990_ConsInte__b as id,  G2990_C59541 as camp1 , G2990_C59542 as camp2  
                    FROM ".$BaseDatos.".G2990  JOIN ".$BaseDatos.".G2990_M".$_POST['muestra']." ON G2990_ConsInte__b = G2990_M".$_POST['muestra']."_CoInMiPo__b WHERE TRUE";

                if($_POST['tareaTipoDist'] != 1){
                    $Zsql .= " AND G2990_M".$_POST['muestra']."_ConIntUsu_b = ".$_POST["idUsuario"]." ";
                }
            }

            //JDBD-2020-05-03 : Este es el campo de busqueda sencilla que esta al lado de la lupa.
            if ($B != "") {
                $Zsql .= ' AND (G2990_C59541 LIKE "%'.$B.'%" OR G2990_C59542 LIKE "%'.$B.'%") ';
            }

            $Zsql .= ' ORDER BY G2990_ConsInte__b DESC LIMIT '.$inicio.' , '.$fin;
            
            $result = $mysqli->query($Zsql);
            while($obj = $result->fetch_object()){
                echo "<tr class='CargarDatos' id='".$obj->id."'>
                    <td>
                        <p style='font-size:14px;'><b>".strtoupper(($obj->camp1))."</b></p>
                        <p style='font-size:12px; margin-top:-10px;'>".strtoupper(($obj->camp2))."</p>
                    </td>
                </tr>";
            } 
        }
              
        //Inserciones o actualizaciones
        if(isset($_POST["oper"]) && isset($_GET['insertarDatosGrilla'])){
            $Lsql  = '';

            $validar = 0;
            $LsqlU = "UPDATE ".$BaseDatos.".G2990 SET "; 
            $LsqlI = "INSERT INTO ".$BaseDatos.".G2990(";
            $LsqlV = " VALUES ("; 
  

            if(isset($_POST["G2990_C59540"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2990_C59540 = '".$_POST["G2990_C59540"]."'";
                $LsqlI .= $separador."G2990_C59540";
                $LsqlV .= $separador."'".$_POST["G2990_C59540"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2990_C59541"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2990_C59541 = '".$_POST["G2990_C59541"]."'";
                $LsqlI .= $separador."G2990_C59541";
                $LsqlV .= $separador."'".$_POST["G2990_C59541"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2990_C59542"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2990_C59542 = '".$_POST["G2990_C59542"]."'";
                $LsqlI .= $separador."G2990_C59542";
                $LsqlV .= $separador."'".$_POST["G2990_C59542"]."'";
                $validar = 1;
            }
             
  
            $G2990_C59543 = NULL;
            //este es de tipo numero no se deja ir asi '', si est avacio lo mejor es no mandarlo
            if(isset($_POST["G2990_C59543"])){
                if($_POST["G2990_C59543"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $G2990_C59543 = $_POST["G2990_C59543"];
                    $LsqlU .= $separador." G2990_C59543 = ".$G2990_C59543."";
                    $LsqlI .= $separador." G2990_C59543";
                    $LsqlV .= $separador.$G2990_C59543;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2990_C59544"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2990_C59544 = '".$_POST["G2990_C59544"]."'";
                $LsqlI .= $separador."G2990_C59544";
                $LsqlV .= $separador."'".$_POST["G2990_C59544"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2990_C59545"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2990_C59545 = '".$_POST["G2990_C59545"]."'";
                $LsqlI .= $separador."G2990_C59545";
                $LsqlV .= $separador."'".$_POST["G2990_C59545"]."'";
                $validar = 1;
            }
             
  
            // if (isset($_FILES["FG2990_C59546"]["tmp_name"])) {
            //     $destinoFile = "/Dyalogo/tmp/G2990/";
            //     $fechUp = date("Y-m-d_H:i:s");
            //     if (!file_exists("/Dyalogo/tmp/G2990")){
            //         mkdir("/Dyalogo/tmp/G2990", 0777);
            //     }
            //     if ($_FILES["FG2990_C59546"]["size"] != 0) {
            //         $G2990_C59546 = $_FILES["FG2990_C59546"]["tmp_name"];
            //         $nG2990_C59546 = $fechUp."_".$_FILES["FG2990_C59546"]["name"];
            //         $rutaFinal = $destinoFile.$nG2990_C59546;
            //         if (is_uploaded_file($G2990_C59546)) {
            //             move_uploaded_file($G2990_C59546, $rutaFinal);
            //         }

            //         $separador = "";
            //         if($validar == 1){
            //             $separador = ",";
            //         }

            //         $LsqlU .= $separador."G2990_C59546 = '".$nG2990_C59546."'";
            //         $LsqlI .= $separador."G2990_C59546";
            //         $LsqlV .= $separador."'".$nG2990_C59546."'";
            //         $validar = 1;
            //     }
            // }

            // Esto es para no perder el formulario
            if(isset($_POST["G2990_C59546"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2990_C59546 = '".$_POST["G2990_C59546"]."'";
                $LsqlI .= $separador."G2990_C59546";
                $LsqlV .= $separador."'".$_POST["G2990_C59546"]."'";
                $validar = 1;
            }
            
  
            // if (isset($_FILES["FG2990_C59547"]["tmp_name"])) {
            //     $destinoFile = "/Dyalogo/tmp/G2990/";
            //     $fechUp = date("Y-m-d_H:i:s");
            //     if (!file_exists("/Dyalogo/tmp/G2990")){
            //         mkdir("/Dyalogo/tmp/G2990", 0777);
            //     }
            //     if ($_FILES["FG2990_C59547"]["size"] != 0) {
            //         $G2990_C59547 = $_FILES["FG2990_C59547"]["tmp_name"];
            //         $nG2990_C59547 = $fechUp."_".$_FILES["FG2990_C59547"]["name"];
            //         $rutaFinal = $destinoFile.$nG2990_C59547;
            //         if (is_uploaded_file($G2990_C59547)) {
            //             move_uploaded_file($G2990_C59547, $rutaFinal);
            //         }

            //         $separador = "";
            //         if($validar == 1){
            //             $separador = ",";
            //         }

            //         $LsqlU .= $separador."G2990_C59547 = '".$nG2990_C59547."'";
            //         $LsqlI .= $separador."G2990_C59547";
            //         $LsqlV .= $separador."'".$nG2990_C59547."'";
            //         $validar = 1;
            //     }
            // }

            // Esto es para no perder el formulario
            if(isset($_POST["G2990_C59547"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2990_C59547 = '".$_POST["G2990_C59547"]."'";
                $LsqlI .= $separador."G2990_C59547";
                $LsqlV .= $separador."'".$_POST["G2990_C59547"]."'";
                $validar = 1;
            }
            
  
            // if (isset($_FILES["FG2990_C59548"]["tmp_name"])) {
            //     $destinoFile = "/Dyalogo/tmp/G2990/";
            //     $fechUp = date("Y-m-d_H:i:s");
            //     if (!file_exists("/Dyalogo/tmp/G2990")){
            //         mkdir("/Dyalogo/tmp/G2990", 0777);
            //     }
            //     if ($_FILES["FG2990_C59548"]["size"] != 0) {
            //         $G2990_C59548 = $_FILES["FG2990_C59548"]["tmp_name"];
            //         $nG2990_C59548 = $fechUp."_".$_FILES["FG2990_C59548"]["name"];
            //         $rutaFinal = $destinoFile.$nG2990_C59548;
            //         if (is_uploaded_file($G2990_C59548)) {
            //             move_uploaded_file($G2990_C59548, $rutaFinal);
            //         }

            //         $separador = "";
            //         if($validar == 1){
            //             $separador = ",";
            //         }

            //         $LsqlU .= $separador."G2990_C59548 = '".$nG2990_C59548."'";
            //         $LsqlI .= $separador."G2990_C59548";
            //         $LsqlV .= $separador."'".$nG2990_C59548."'";
            //         $validar = 1;
            //     }
            // }

            // Esto es para no perder el formulario
            if(isset($_POST["G2990_C59548"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2990_C59548 = '".$_POST["G2990_C59548"]."'";
                $LsqlI .= $separador."G2990_C59548";
                $LsqlV .= $separador."'".$_POST["G2990_C59548"]."'";
                $validar = 1;
            }
            
 
            $G2990_C59529 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["tipificacion"])){    
                if($_POST["tipificacion"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2990_C59529 = str_replace(' ', '',$_POST["tipificacion"]);
                    $LsqlU .= $separador." G2990_C59529 = ".$G2990_C59529;
                    $LsqlI .= $separador." G2990_C59529";
                    $LsqlV .= $separador.$G2990_C59529;
                    $validar = 1;

                    
                }
            }
 
            $G2990_C59530 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["reintento"])){    
                if($_POST["reintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2990_C59530 = str_replace(' ', '',$_POST["reintento"]);
                    $LsqlU .= $separador." G2990_C59530 = ".$G2990_C59530;
                    $LsqlI .= $separador." G2990_C59530";
                    $LsqlV .= $separador.$G2990_C59530;
                    $validar = 1;
                }
            }
 
            $G2990_C59531 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtFechaReintento"])){    
                if($_POST["TxtFechaReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2990_C59531 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." 00:00:00'";
                    $LsqlU .= $separador." G2990_C59531 = ".$G2990_C59531;
                    $LsqlI .= $separador." G2990_C59531";
                    $LsqlV .= $separador.$G2990_C59531;
                    $validar = 1;
                }
            }
 
            $G2990_C59532 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["TxtHoraReintento"])){    
                if($_POST["TxtHoraReintento"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2990_C59532 = "'".str_replace(' ', '',$_POST["TxtFechaReintento"])." ".str_replace(' ', '',$_POST["TxtHoraReintento"])."'";
                    $LsqlU .= $separador." G2990_C59532 = ".$G2990_C59532;
                    $LsqlI .= $separador." G2990_C59532";
                    $LsqlV .= $separador.$G2990_C59532;
                    $validar = 1;
                }
            }
 
            $G2990_C59533 = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["textAreaComentarios"])){    
                if($_POST["textAreaComentarios"] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }
                    $G2990_C59533 = "'".$_POST["textAreaComentarios"]."'";
                    $LsqlU .= $separador." G2990_C59533 = ".$G2990_C59533;
                    $LsqlI .= $separador." G2990_C59533";
                    $LsqlV .= $separador.$G2990_C59533;
                    $validar = 1;
                }
            }
  

            if(isset($_POST["G2990_C59534"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2990_C59534 = '".$_POST["G2990_C59534"]."'";
                $LsqlI .= $separador."G2990_C59534";
                $LsqlV .= $separador."'".$_POST["G2990_C59534"]."'";
                $validar = 1;
            }
             
  
            //JDBD - Fecha oculta Script
            if(isset($_POST["G2990_C59535"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $strFecha_t = date("Y-m-d H:i:s");

                $LsqlU .= $separador."G2990_C59535 = '".$strFecha_t."'";
                $LsqlI .= $separador."G2990_C59535";
                $LsqlV .= $separador."'".$strFecha_t."'";
                $validar = 1;
            }
             
  
            //JDBD - Hora oculta Script
            if(isset($_POST["G2990_C59536"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $strHora_t = date("H:i:s");

                $LsqlU .= $separador."G2990_C59536 = '".$strHora_t."'";
                $LsqlI .= $separador."G2990_C59536";
                $LsqlV .= $separador."'".$strHora_t."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2990_C59537"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2990_C59537 = '".$_POST["G2990_C59537"]."'";
                $LsqlI .= $separador."G2990_C59537";
                $LsqlV .= $separador."'".$_POST["G2990_C59537"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2990_C59538"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2990_C59538 = '".$_POST["G2990_C59538"]."'";
                $LsqlI .= $separador."G2990_C59538";
                $LsqlV .= $separador."'".$_POST["G2990_C59538"]."'";
                $validar = 1;
            }
             
  

            if(isset($_POST["G2990_C59539"])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2990_C59539 = '".$_POST["G2990_C59539"]."'";
                $LsqlI .= $separador."G2990_C59539";
                $LsqlV .= $separador."'".$_POST["G2990_C59539"]."'";
                $validar = 1;
            }
             

                //JDBD - Llenado de Reintento y Clasificacion.
                if(isset($_POST["MonoEf"])){
                    $LmonoEfLSql = "SELECT MONOEF_Contacto__b FROM ".$BaseDatos_systema.".MONOEF WHERE MONOEF_ConsInte__b = ".$_POST['MonoEf'];
                    $resMonoEf = $mysqli->query($LmonoEfLSql);
                    $dataMonoEf = $resMonoEf->fetch_object();

                    $conatcto = $dataMonoEf->MONOEF_Contacto__b;

                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    $LsqlU .= $separador."G2990_Clasificacion = ".$conatcto;
                    $LsqlI .= $separador."G2990_Clasificacion";
                    $LsqlV .= $separador.$conatcto;
                    $validar = 1;
                }            
                
            if(isset($_GET['id_gestion_cbx'])){
                $separador = "";
                if($validar == 1){
                    $separador = ",";
                }

                $LsqlU .= $separador."G2990_IdLlamada = '".$_GET['id_gestion_cbx']."'";
                $LsqlI .= $separador."G2990_IdLlamada";
                $LsqlV .= $separador."'".$_GET['id_gestion_cbx']."'";
                $validar = 1;
            }


            $padre = NULL;
            //este es de tipo date hay que preguntar si esta vacia o no
            if(isset($_POST["padre"])){    
                if($_POST["padre"] != '0' && $_POST['padre'] != ''){
                    $separador = "";
                    if($validar == 1){
                        $separador = ",";
                    }

                    //primero hay que ir y buscar los campos
                    $Lsql = "SELECT GUIDET_ConsInte__PREGUN_De1_b FROM ".$BaseDatos_systema.".GUIDET WHERE GUIDET_ConsInte__GUION__Mae_b = ".$_POST['formpadre']." AND GUIDET_ConsInte__GUION__Det_b = ".$_POST['formhijo'];

                    $GuidRes = $mysqli->query($Lsql);
                    $campo = null;
                    while($ky = $GuidRes->fetch_object()){
                        $campo = $ky->GUIDET_ConsInte__PREGUN_De1_b;
                    }
                    $valorG = "G2990_C";
                    $valorH = $valorG.$campo;
                    $LsqlU .= $separador." " .$valorH." = ".$_POST["padre"];
                    $LsqlI .= $separador." ".$valorH;
                    $LsqlV .= $separador.$_POST['padre'] ;
                    $validar = 1;
                }
            }
            if(isset($_POST['oper'])){
                if($_POST["oper"] == 'add' ){
                    $LsqlI .= ", G2990_Usuario , G2990_FechaInsercion, G2990_CodigoMiembro";
                    $LsqlV .= ", ".$_GET['usuario']." , '".date('Y-m-d H:i:s')."', ".$_GET['CodigoMiembro'];
                    $Lsql = $LsqlI.")" . $LsqlV.")";
                }else if($_POST["oper"] == 'edit' ){
                    $Lsql = $LsqlU." WHERE G2990_ConsInte__b =".$_POST["id"]; 
                }else if($_POST["oper"] == 'del' ){
                    $Lsql = "DELETE FROM ".$BaseDatos.".G2990 WHERE G2990_ConsInte__b = ".$_POST['id'];
                    $validar = 1;
                }
            }
            //si trae algo que insertar inserta

            //echo $Lsql;
            if($validar == 1){
                if ($mysqli->query($Lsql) === TRUE) {
                    if($_POST["oper"] == 'add' ){
                        $UltimoID = $mysqli->insert_id;
                        echo $mysqli->insert_id;
                    }else{
                        if(isset($_POST["id"]) && $_POST["id"] != '0' ){
                             $UltimoID = $_POST["id"]; echo $UltimoID;
                        }
                        //echo "1";           
                    }

                    

                } else {
                    echo '0';
                    $queryCondia="INSERT INTO DYALOGOCRM_WEB.log_yorman_temporal (sqlGenerado,errorGenerado,comentario)
                    VALUES(\"".$Lsql."\",\"".$mysqli->error."\",'Insercion Script')";
                    $mysqli->query($queryCondia);                    
                   // echo "Error Haciendo el proceso los registros : " . $mysqli->error;
                }
            }        

        }
    }
  

  
?>
